//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/extensions/ERC1155SupplyUpgradeable.sol";
import "./IRandom.sol";
import "../mission_control/IAssetManager.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";
import "../interfaces/ITrustedMintable.sol";

/// @title ERC-1155 token representing various in game assets
/// @dev This contract contains a lot of bespoke functionality specific to the minting of badges.
contract NFT is ERC1155SupplyUpgradeable, OwnableUpgradeable, IAssetManager, ITrustedMintable {
    using SafeERC20Upgradeable for IERC20Upgradeable;
    using ECDSAUpgradeable for bytes32;

    /**
     * @notice Emitted when a badge is minted through _mintRandom()
     * @dev Note that this event is only emitted when badges are minted
     * @param account The owner of the new badge
     * @param tokenId The ID of the badge
     */
    event Minted(address account, uint256 tokenId);
    /**
     * @notice Event emitted when badges are combined to a badge of a higher tier
     * @param account The owner of the new, higher tier badge
     * @param tokenId The id of the higher tier badge
     * @param amount The number of badges upgraded in this transaction
     */
    event MintedHigherTier(address account, uint256 tokenId, uint256 amount);
    /**
     * @notice Emitted specifically when trustedMint() is  called
     * @dev Redundant with the transfer events emitted by ERC-1155
     * @param account The account the tokens have been minted to
     * @param tokenId The tokenId which has been minted
     * @param amount The amount of tokens that have been minted
     */
    event MintedTo(address account, uint256 tokenId, uint256 amount);

    string public name;
    string public symbol;

    bool public isPaused;
    address public moderator;
    mapping(uint256 => string) public uris;
    mapping(uint256 => uint256) public maxSupplies;
    mapping(uint256 => uint256) public prices;
    mapping(uint256 => uint8) public rarities; // [2%, 18%, 80%]

    address public weth;
    address public rng; // random number generator

    mapping(address => bool) public trustedAddresses;

    mapping(uint256 => bool) public usedNonce;
    mapping(uint256 => bool) public blockedTransfers;

    modifier onlyGov() {
        require(msg.sender == owner() || msg.sender == moderator, "NFT: NOT_GOVERNANCE");
        _;
    }

    modifier onlyMissionControl() {
        require(trustedAddresses[msg.sender] == true, "NFT: NOT_MISSION_CONTROL");
        _;
    }

    /**
     * @notice Initialiser for this contract
     * @param name_ The tokens name
     * @param symbol_ The tokens symbol
     */
    function initialize(string memory name_, string memory symbol_) public initializer {
        __ERC1155_init("");
        __Ownable_init();

        moderator = msg.sender;
        isPaused = true;
        name = name_;
        symbol = symbol_;
    }

    /**
     * @notice Used to set the name of this token
     * @param name_ The name to-be
     */
    function setName(string memory name_) external onlyGov {
        name = name_;
    }

    /**
     * @notice Used to set the symbol of this token
     * @param symbol_ The symbol to be
     */
    function setSymbol(string memory symbol_) external onlyGov {
        symbol = symbol_;
    }

    /**
     * @notice Used to set a contract as trusted for the purposes of IAssetManager
     * @param address_ The address of the contract which should be trusted
     */
    function setTrustedContract(address address_) external onlyGov {
        trustedAddresses[address_] = true;
    }

    /**
     * @notice Used to block token transfers
     * @param tokenId_ The token ID for which we preforme action
     * @param isBlocked_ The action
     */
    function blockTransfers(uint256 tokenId_, bool isBlocked_) external onlyGov {
        blockedTransfers[tokenId_] = isBlocked_;
    }

    /**
     * @notice Withdraws tokens from this contract
     * @param tokens Array of addresses corresponding to the tokens which should be withdrawn
     * @dev The zero address is used for eth/matic
     */
    function withdraw(address[] calldata tokens) external onlyOwner {
        for (uint256 i; i < tokens.length; i += 1) {
            IERC20Upgradeable token = IERC20Upgradeable(tokens[i]);
            if (tokens[i] == address(0)) {
                // solhint-disable-next-line avoid-low-level-calls
                (bool success, ) = msg.sender.call{value: address(this).balance}("");
                require(success, "Pix: WITHDRAW_FAILED");
            } else if (token.balanceOf(address(this)) > 0) {
                token.safeTransfer(msg.sender, token.balanceOf(address(this)));
            }
        }
    }

    /**
     * @notice Used to flip between paused and unpaused
     */
    function toggleStatus() external onlyGov {
        isPaused = !isPaused;
    }

    /**
     * @notice Used to set a moderator for this contract
     * @param moderator_ The address of the moderator
     */
    function setModerator(address moderator_) external onlyOwner {
        require(moderator_ != address(0), "NFT: ZERO_ADDRESS");
        moderator = moderator_;
    }

    /**
     * @notice Used to set the max supply for a token id
     * @param id The tokenId of the token
     * @param maxSupply The max supply
     * @dev Will not check if the current supply exceeds maxSupply
     */
    function setMaxSupply(uint256 id, uint256 maxSupply) external onlyOwner {
        maxSupplies[id] = maxSupply;
    }

    /**
     * @notice Sets the price for a token
     * @param id The token id
     * @param price The price denoted in weth
     */
    function setPrice(uint256 id, uint256 price) external onlyOwner {
        prices[id] = price;
    }

    /**
     * @notice Sets the uri for a token
     * @param id The token id
     * @param uri_ The uri
     */
    function setURI(uint256 id, string memory uri_) external onlyGov {
        uris[id] = uri_;
    }

    function uri(uint256 id) public view override returns (string memory) {
        return uris[id];
    }

    /**
     * @notice Called by a user to mint/purchase a random AoC badge
     * @param amount The number of badges to be purchased
     */
    function mint(uint256 amount) external payable {
        require(!isPaused, "NFT: SALE_PAUSED");
        require(amount > 0 && amount <= 50, "NFT: INVALID_MINT_AMOUNT");
        require(
            totalSupply(1) + totalSupply(2) + totalSupply(3) + amount <
                maxSupplies[1] + maxSupplies[2] + maxSupplies[3],
            "NFT: AMOUNT_TOO_BIG"
        );
        if (
            totalSupply(1) >= maxSupplies[1] &&
            totalSupply(2) >= maxSupplies[2] &&
            totalSupply(3) >= maxSupplies[3]
        ) revert("NFT: AOC_SOLD_OUT");
        uint256 totalWETHCost = 0;
        for (uint256 i = 0; i < amount; i++) {
            totalWETHCost += prices[1];
            _mintRandom();
        }
        IERC20Upgradeable(weth).safeTransferFrom(msg.sender, address(this), totalWETHCost);
    }


    function _verifySignature(bytes memory message, bytes memory signature) private view returns (bool) {
        address signer = keccak256(message).toEthSignedMessageHash().recover(signature);
        return trustedAddresses[signer];
    }

    function mintWithSignature(
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts,
        uint256 _signatureNonce,
        bytes memory _signature
    ) external {
        require(_tokenIds.length == _amounts.length, "NFT: INVALID_LENGTHS");
        require(!usedNonce[_signatureNonce], "NFT: USED_NONCE");
        require(_verifySignature(abi.encode(msg.sender, _tokenIds, _amounts, _signatureNonce), _signature), "NFT: INVALID_SIGNATURE");

        usedNonce[_signatureNonce] = true;

        _mintBatch(msg.sender, _tokenIds, _amounts, "");
    }


    /**
     * @notice Called by a user to combine multiple AoC badges into one of a higher tier
     * @param tier The desired tier
     * @param amount The number of higher tier badges to mint
     * @dev Tier and token id are used interchangeably here
     */
    function mintHigherTier(uint256 tier, uint256 amount) external {
        require(tier > 0 && tier < 3, "NFT: INVALID_TIER");
        require(amount > 0, "NFT: INVALID_AMOUNT");

        // burn 5X bronze/silver to mint silver/gold
        _burn(msg.sender, tier + 1, amount * 5);

        // mint silver/gold
        _mint(msg.sender, tier, amount, "");

        // event
        emit MintedHigherTier(msg.sender, tier, amount);
    }

    /// @inheritdoc IAssetManager
    function trustedMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) external override(IAssetManager, ITrustedMintable) onlyMissionControl {
        require(bytes(uris[_tokenId]).length > 0, "NFT: INVALID_TOKEN");
        _mint(_to, _tokenId, _amount, "");
        emit MintedTo(_to, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override(IAssetManager, ITrustedMintable) onlyMissionControl {
        _mintBatch(_to, _tokenIds, _amounts, "");
    }

    /// @inheritdoc IAssetManager
    function trustedBurn(
        address _from,
        uint256 _tokenId,
        uint256 _amount
    ) external override onlyMissionControl {
        require(bytes(uris[_tokenId]).length > 0, "NFT: INVALID_TOKEN");
        _burn(_from, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchBurn(
        address _from,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override onlyMissionControl {
        _burnBatch(_from, _tokenIds, _amounts);
    }

    /**
     * @notice Used to manually airdrop tokens
     * @param recipients Array of recipient addresses
     * @param ids The ids that the recipients should receive
     * @param amounts The amounts which the recipients should receive
     */
    function airdrop(
        address[] calldata recipients,
        uint256[] calldata ids,
        uint256[] calldata amounts
    ) external onlyGov {
        require(
            recipients.length == ids.length && recipients.length == amounts.length,
            "NFT: INVALID_ARGUMENTS"
        );
        for (uint256 i; i < recipients.length; i += 1) _mint(recipients[i], ids[i], amounts[i], "");
    }

    /**
     * @notice Sets the address to the weth token
     * @param weth_ The address of the weth token
     */
    function setWETH(address weth_) external onlyGov {
        weth = weth_;
    }

    /**
     * @notice Sets the rng provider
     * @param rng_ The address of the rng provider
     */
    function setRNG(address rng_) external onlyGov {
        rng = rng_;
    }

    /**
     * @notice Used to set probabilities for the AoC badge minting
     * @param id The token id
     * @param rarity The tokens rarity (In what units?)
     */
    function setRarities(uint256 id, uint8 rarity) external onlyGov {
        rarities[id] = rarity;
    }

    /**
     * @notice Mints a AoC badge
     * @param tier The tier of the badge
     * @dev Tier and token id are used interchangeably in this context
     */
    function _mintAOC(uint8 tier) private {
        _mint(msg.sender, tier, 1, "");
    }

    /**
     * @notice Mints a random AoC badge to the sender
     * @dev Calls IRandom
     */
    function _mintRandom() private {
        uint256 rarity = IRandom(rng).r100(totalSupply(1) + totalSupply(2) + totalSupply(3));
        uint8 tier = 3;
        if (rarity < rarities[1]) {
            tier = 1;
        } else if (rarity < rarities[2]) {
            tier = 2;
        }

        if (tier == 1) {
            if (totalSupply(1) < maxSupplies[1]) {
                _mintAOC(1);
            } else if (totalSupply(2) < maxSupplies[2]) {
                _mintAOC(2);
            } else _mintAOC(3);
        } else if (tier == 2) {
            if (totalSupply(2) < maxSupplies[2]) {
                _mintAOC(2);
            } else if (totalSupply(1) < maxSupplies[1]) {
                _mintAOC(1);
            } else _mintAOC(3);
        } else {
            if (totalSupply(3) < maxSupplies[3]) {
                _mintAOC(3);
            } else if (totalSupply(2) < maxSupplies[2]) {
                _mintAOC(2);
            } else if (totalSupply(1) < maxSupplies[1]) {
                _mintAOC(1);
            }
        }

        emit Minted(msg.sender, tier);
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) internal virtual override {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);

        for (uint256 i; i < ids.length; i += 1) {
            if(msg.sender != moderator && !trustedAddresses[msg.sender])
                require(!blockedTransfers[ids[i]], "NFT: Transfers blocked for tokenID");
        }
    }
}
