//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

/// @title An interface of a randomness provider
interface IRandom {
    /**
    * @notice Used to fetch random numbers
    * @param _tokenId Used as a random seed
    * @return A random integer less than 100
    * @dev The parameter name should be changed
    */
    function r100(uint _tokenId) external view returns (uint);
    function random(uint _tokenId, uint _number) external view returns (uint);
}
