// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

/**
* @title Implementation of IRandom
* @dev Does not ACTUALLY implement IRandom, but 'manually' conforms
*/
contract Random {

    /**
    * @notice Used to fetch random numbers
    * @param _tokenId Used as a random seed
    * @return A random integer less than 100
    * @dev The parameter name should be changed
    */
    function r100(uint _tokenId) external view returns (uint) {
        return random(_tokenId, 100);
    }

    /**
    * @notice Generates a random number from a uniform distribution
    * @param _tokenId Used as a seed
    * @param _number The upper limit to the number
    * @dev The return is \in U(0, _number)
    */
    function random(uint _tokenId, uint _number) public view returns (uint) {
        return _seed(_tokenId) % _number;
    }

    /**
    * @notice Used to fetch a random integer
    * @param input The seed
    * @return A random uint256
    * @dev A little superfluous in my opinion
    */
    function _random(string memory input) internal pure returns (uint256) {
        return uint256(keccak256(abi.encodePacked(input)));
    }

    /**
    * @notice Used to fetch a random integer
    * @param _tokenId The seed
    * @return rand A random uint256
    */
    function _seed(uint _tokenId) internal view returns (uint rand) {
        rand = _random(
            string(
                abi.encodePacked(
                    block.timestamp,
                    blockhash(block.number - 1),
                    _tokenId,
                    msg.sender
                )
            )
        );
    }
}
