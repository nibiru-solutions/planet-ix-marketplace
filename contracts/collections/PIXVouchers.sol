//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/extensions/ERC1155SupplyUpgradeable.sol";
import "../interfaces/ITrustedMintable.sol";
import "../mission_control/IAssetManager.sol";

/// @title ERC-1155 token representing Planet IX Vauchers
contract PIXVouchers is ERC1155SupplyUpgradeable, OwnableUpgradeable, IAssetManager, ITrustedMintable {
    /**
     * @notice Emitted specifically when trustedMint() is  called
     * @dev Redundant with the transfer events emitted by ERC-1155
     * @param _address The address tokens have been minted to
     * @param _tokenId The tokenId which has been minted
     * @param _amount The amount of tokens that have been minted
     */
    event MintedTo(address _address, uint256 _tokenId, uint256 _amount);

    /* State Variables */
    string private s_name;
    string private s_symbol;

    mapping(uint256 => string) public s_uris;
    mapping(address => bool) public s_trustedAddresses;
    mapping(address => bool) public s_whitelistedTraders;    
    mapping(uint256 => bool) public s_blockedTransfers;

    modifier onlyTrusted() {
        require(s_trustedAddresses[msg.sender] == true, "VA: NOT_TRUSTED");
        _;
    }

    /**
     * @notice Initialiser for this contract
     * @param _name The tokens name
     * @param _symbol The tokens symbol
     */
    function initialize(string memory _name, string memory _symbol) public initializer {
        __ERC1155_init("");
        __Ownable_init();

        s_name = _name;
        s_symbol = _symbol;
    }

    /**
     * @notice Used to get the collection name
     * @return Name
     */
    function name() public view returns (string memory) {
        return s_name;
    }

    /**
     * @notice Used to get the collection symbol
     * @return Symbol
     */
    function symbol() public view returns (string memory) {
        return s_symbol;
    }

    /**
     * @notice Used to block token transfers
     * @param tokenId_ The token ID for which we preforme action
     * @param isBlocked_ The action
     */
    function blockTransfers(uint256 tokenId_, bool isBlocked_) external onlyOwner {
        s_blockedTransfers[tokenId_] = isBlocked_;
    }

    /**
     * @notice Used to set a contract as trusted for the purposes of IAssetManager
     * @param _address The address of the contract which should be trusted
     */
    function setTrustedContract(address _address) external onlyOwner {
        s_trustedAddresses[_address] = true;
    }

    /**
     * @notice Used to whitelist a contract as trader
     * @param _address The address of the contract which should be whitelisted
     */
    function whitelistTraderContract(address _address) external onlyOwner {
        s_whitelistedTraders[_address] = true;
    }

    /**
     * @notice Sets the uri for a token
     * @param _tokenId The token id
     * @param _uri The uri
     */
    function setURI(uint256 _tokenId, string memory _uri) external onlyOwner {
        s_uris[_tokenId] = _uri;
    }

    /// @inheritdoc ERC1155Upgradeable
    function uri(uint256 _tokenId) public view override returns (string memory) {
        require(_tokenId > 0, "VA: NOT_EXISTING");
        return s_uris[_tokenId];
    }

    function _safeMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) internal {
        _mint(_to, _tokenId, _amount, "");
        emit MintedTo(_to, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) external override(IAssetManager, ITrustedMintable) onlyTrusted {
        _safeMint(_to, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override(IAssetManager, ITrustedMintable) onlyTrusted {
        uint256 length = _tokenIds.length;
        for (uint256 i = 0; i < length; i++) {
            _safeMint(_to, _tokenIds[i], _amounts[i]);
        }
    }

    /// @inheritdoc IAssetManager
    function trustedBurn(
        address _from,
        uint256 _tokenId,
        uint256 _amount
    ) external override onlyTrusted {
        _burn(_from, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchBurn(
        address _from,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override onlyTrusted {
        _burnBatch(_from, _tokenIds, _amounts);
    }

    /// @inheritdoc ERC1155Upgradeable
    function setApprovalForAll(address operator, bool approved) public virtual override(ERC1155Upgradeable) {
        if (approved && isContract(operator)) {
            require(s_whitelistedTraders[operator], "VA: NON_WHITELISTED_TRADER");
        }
        _setApprovalForAll(msg.sender, operator, approved);
    }

    /**
     * @notice Returns true if address passed is contract
     * @param _address Address to be checked whether it is contract 
     */
    function isContract(address _address) public view returns (bool) {
        uint256 size;
        assembly { size := extcodesize(_address) }
        return size > 0 ? true : false;
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) internal virtual override {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);

        for (uint256 i; i < ids.length; i += 1) {
            if(!s_trustedAddresses[msg.sender] && to != address(0))
                require(!s_blockedTransfers[ids[i]], "NFT: Transfers blocked for tokenID");
        }
    }
}
