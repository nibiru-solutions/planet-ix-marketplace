//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/ITrustedMintable.sol";
import "../mission_control/IAssetManager.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/StringsUpgradeable.sol";

/**
 *    @title ERC721 token representing AOC Badges
 */
contract AOCBadge is ERC721Upgradeable, OwnableUpgradeable, ITrustedMintable, IAssetManager {
    using StringsUpgradeable for uint256;

    mapping(address => bool) public s_isTrusted;
    uint256 public s_totalSupply;
    string public s_baseURI;
    IAssetManager public pixAssets;

    mapping(uint256 => string) uris;

    /* Modifiers */
    modifier onlyTrusted() {
        require(s_isTrusted[msg.sender], "AOC: NOT_TRUSTED");
        _;
    }

    /**
     * @notice Initialiser for this contract
     * @param _name The tokens name
     * @param _symbol The tokens symbol
     */
    function initialize(string memory _name, string memory _symbol) public initializer {
        __ERC721_init(_name, _symbol);
        __Ownable_init();
    }

    function _mintAOC(address _to, uint256 _amount) internal {
        for (uint256 i; i < _amount; i++) {
            unchecked {
                ++s_totalSupply;
            }
            _safeMint(_to, s_totalSupply);
        }
    }

    /// @inheritdoc IAssetManager
    function trustedMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) external override(ITrustedMintable, IAssetManager) onlyTrusted {
        _mintAOC(_to, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override(ITrustedMintable, IAssetManager) onlyTrusted {
        for (uint256 i; i < _tokenIds.length; i++) {
            require(!_exists(_tokenIds[i]), "AOC: TOKEN_ID_MINTED");
            _safeMint(_to, _tokenIds[i]);
        }
    }

    /// @inheritdoc IAssetManager
    function trustedBurn(
        address _from,
        uint256 _tokenId,
        uint256 _amount
    ) external override onlyTrusted {
        _burn(_tokenId);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchBurn(
        address _from,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override onlyTrusted {
        for (uint256 i; i < _tokenIds.length; i++) {
            _burn(_tokenIds[i]);
        }
    }

    /**
     * @notice For burning 5 AOC badges in order to get token id 3 on pix assets
     * @param _tokenIds list of tokenIds the user wishes to burn
     */
    function burnBadges(uint256[] memory _tokenIds) external {
        require(_tokenIds.length == 10, "AOC: INVALID_BURN_AMOUNT");
        for (uint256 i; i < _tokenIds.length; i++) {
            require(msg.sender == ownerOf(_tokenIds[i]), "AOC: WRONG_OWNER");
            _burn(_tokenIds[i]);
        }
        IAssetManager(pixAssets).trustedMint(msg.sender, uint256(IAssetManager.AssetIds.BronzeBadge), 1);
    }

    /**
     * @notice Used to set a contract as trusted for the purposes of IAssetManager
     * @param _sender The address of the contract which should be trusted
     * @param _isTrusted The boolean representing seting or removing trusted contract
     */
    function setTrusted(address _sender, bool _isTrusted) external onlyOwner {
        s_isTrusted[_sender] = _isTrusted;
    }

    function setPixAssets(IAssetManager _pixAssets) external onlyOwner {
        pixAssets = _pixAssets;
    }

    /**
     * @notice Used to set set URIs
     * @param _indices The indices for uris to be set
     * @param _aocURIs The list of uris to be set
     */
    function setURIs(uint256[] calldata _indices, string[] memory _aocURIs) external onlyOwner {
        require(_indices.length == _aocURIs.length, "AOC: INVALID_ARGUMENTS");
        for (uint256 i; i < _indices.length; i += 1) {
            uris[_indices[i]] = _aocURIs[i];
        }
    }

    /// @inheritdoc IERC721MetadataUpgradeable
    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        require(_exists(tokenId), "ERC721: invalid token ID");
        return bytes(uris[tokenId / 3000]).length > 0 ? string(abi.encodePacked(uris[tokenId / 3000], tokenId.toString())) : "";
    }

    /**
    * @notice Performs a batch transfer of AOC Badge
    * @param _from The address to transfer the AOC Badge from
    * @param _to The address to transfer the AOC Badge to
    * @param _tokenIds The Ids of the AOC Badge to be transferred
    */
    function safeBatchTransferFrom(
        address _from,
        address _to,
        uint256[] calldata _tokenIds
    ) external {
        uint256 len = _tokenIds.length;
        for (uint256 i; i < len; i += 1) {
            _transfer(_from, _to, _tokenIds[i]);
        }
    }
}
