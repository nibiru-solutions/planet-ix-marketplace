//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/extensions/ERC1155SupplyUpgradeable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "../interfaces/ITrustedMintable.sol";
import "../mission_control/IAssetManager.sol";

/// @title ERC-1155 token representing Corporation Avatars assets
contract CorporationAvatars is ERC1155SupplyUpgradeable, OwnableUpgradeable, IAssetManager, ITrustedMintable {
    using Strings for uint256;

    /**
     * @notice Emitted specifically when trustedMint() is  called
     * @dev Redundant with the transfer events emitted by ERC-1155
     * @param account The account tokens have been minted to
     * @param tokenId The tokenId which has been minted
     * @param amount The amount of tokens that have been minted
     */
    event MintedTo(address account, uint256 tokenId, uint256 amount);

    /**
     * @notice Emitted specifically when burn() is called
     * @dev Used for the "Claim all" functionality
     * @param account The account tokens have been burned from
     * @param tokenId The tokenId which has been burned
     */
    event AvatarBurned(address account, uint256 tokenId, uint256 amount);

    /* State Variables */
    string private s_name;
    string private s_symbol;
    string private s_baseURIExtended;
    uint256[] private s_tokensByRarity;
    uint256[] private s_tokenRarityIndices;

    address public s_moderator;
    mapping(uint256 => uint256) public s_maxSupplies;
    mapping(address => bool) public s_trustedAddresses;

    /* Modifiers */
    modifier onlyGov() {
        require(msg.sender == owner() || msg.sender == s_moderator, "CA: NOT_GOVERNANCE");
        _;
    }

    modifier onlyTrusted() {
        require(s_trustedAddresses[msg.sender] == true, "CA: NOT_TRUSTED");
        _;
    }

    /**
     * @notice Initialiser for this contract
     * @param _name The tokens name
     * @param _symbol The tokens symbol
     */
    function initialize(string memory _name, string memory _symbol) public initializer {
        __ERC1155_init("");
        __Ownable_init();

        s_moderator = msg.sender;
        s_name = _name;
        s_symbol = _symbol;
    }

    /**
     * @notice Used to get the collection name
     * @return Name
     */
    function name() public view returns (string memory) {
        return s_name;
    }

    /**
     * @notice Used to get the collection symbol
     * @return Symbol
     */
    function symbol() public view returns (string memory) {
        return s_symbol;
    }

    /**
     * @notice Used to set a contract as trusted for the purposes of IAssetManager
     * @param _address The address of the contract which should be trusted
     */
    function setTrustedContract(address _address) external onlyGov {
        s_trustedAddresses[_address] = true;
    }

    /**
     * @notice Used to set a list of tokens ordered by rarity
     * @param _tokensByRarity The list of token ids sorted by token rarity
     */
    function setTokensByRarity(uint256[] calldata _tokensByRarity) external onlyOwner {
        s_tokensByRarity = _tokensByRarity;
    }

    /**
     * @notice Used to set a list of indices of token rarities
     * @param _tokenRarityIndices The list of token ids sorted by token rarity
     */
    function setTokenRarityIndices(uint256[] calldata _tokenRarityIndices) external onlyOwner {
        s_tokenRarityIndices = _tokenRarityIndices;
    }

    /**
     * @notice Used to set a moderator for this contract
     * @param _moderator The address of the moderator
     */
    function setModerator(address _moderator) external onlyOwner {
        require(_moderator != address(0), "CA: ZERO_ADDRESS");
        s_moderator = _moderator;
    }

    /**
     * @notice Used to set the max supply for a token id
     * @param _tokenIds The list of tokenIds
     * @param _maxSupplies The list of max supplies
     * @dev Will not check if the current supply exceeds maxSupply
     */
    function setMaxSupplies(uint256[] calldata _tokenIds, uint256[] calldata _maxSupplies) external onlyOwner {
        require(_tokenIds.length == _maxSupplies.length, "CA: INVALID_ARGUMENTS");
        uint256[] memory tokenIdsMem = _tokenIds;
        uint256[] memory suppliesMem = _maxSupplies;
        for (uint256 i; i < suppliesMem.length; i++) {
            require(totalSupply(tokenIdsMem[i]) <= suppliesMem[i], "CA: MAX_SUPPLY_TOO_SMALL");
            s_maxSupplies[tokenIdsMem[i]] = suppliesMem[i];
        }
    }

    /**
     * @notice Sets the  base uri for a token
     * @param _baseURI The uri
     */
    function setBaseURI(string memory _baseURI) external onlyOwner {
        s_baseURIExtended = _baseURI;
    }

    function uri(uint256 _tokenId) public view override returns (string memory) {
        require(_tokenId > 0, "CA: NOT_EXISTING");
        return string(abi.encodePacked(s_baseURIExtended, _tokenId.toString()));
    }

    function _safeMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) internal {
        uint256 indexOfTokenId = s_tokenRarityIndices[_tokenId];
        uint256 idToMint;
        bool shouldMint;
        for (uint256 i = indexOfTokenId; i < s_tokensByRarity.length; i++) {
            uint256 mintedSupply = totalSupply(s_tokensByRarity[i]);
            uint256 maxSupply = s_maxSupplies[s_tokensByRarity[i]];
            if (mintedSupply + _amount <= maxSupply) {
                idToMint = s_tokensByRarity[i];
                shouldMint = true;
                break;
            }
        }
        if (shouldMint) {
            _mint(_to, idToMint, _amount, "");
            emit MintedTo(_to, idToMint, _amount);
        }
    }

    /// @inheritdoc IAssetManager
    function trustedMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) external override(IAssetManager, ITrustedMintable) onlyTrusted {
        _safeMint(_to, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override(IAssetManager, ITrustedMintable) onlyTrusted {
        uint256 length = _tokenIds.length;
        for (uint256 i = 0; i < length; i++) {
            _safeMint(_to, _tokenIds[i], _amounts[i]);
        }
    }

    /// @inheritdoc IAssetManager
    function trustedBurn(
        address _from,
        uint256 _tokenId,
        uint256 _amount
    ) external override onlyTrusted {
        _burn(_from, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchBurn(
        address _from,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override onlyTrusted {
        _burnBatch(_from, _tokenIds, _amounts);
    }

    /**
     * @notice Used to burn an avatar from a user for "Claim all" functionality
     * @param _tokenId The token id
     * @param _amount The amount to burn
     */
    function avatarBurn(uint256 _tokenId, uint256 _amount) external {
        _burn(msg.sender, _tokenId, _amount);
        emit AvatarBurned(msg.sender, _tokenId, _amount);
    }

    /**
     * @notice Used to batch burn avatars
     * @param _tokenIds The token ids
     * @param _amounts The amounts to burn
     */
    function avatarBatchBurn(uint256[] calldata _tokenIds, uint256[] calldata _amounts) external {
        _burnBatch(msg.sender, _tokenIds, _amounts);
        for (uint256 i; i < _tokenIds.length; i++) {
            emit AvatarBurned(msg.sender, _tokenIds[i], _amounts[i]);
        }
    }
}
