//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

error NonExistentProduct();
error SaleLimitExceeded();
error ProductPriceInvalid();
error InvalidArgumetSize();

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@fluencr/fluencr-protocol/contracts/Fluencr.sol";
import "../interfaces/ITrustedMintable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../spinner/ISpinner.sol";

interface IERC20Extented {
    function decimals() external view returns (uint8);
}

/**
 * @title Fluencr Sale
 * @notice This contract is for managing PlanetIX asset sales on Fluencr
 */
contract FluencrSale is OwnableUpgradeable, PausableUpgradeable, Fluencr {
    /**
     * @param contractAddress - contract address for the product
     * @param tokenId - token id for the product
     * @param amount - amount of tokens minted for the product
     */    
    struct ContractInfo {
        address contractAddress;
        uint256 tokenId;
        uint256 amount;
    }

    event ProductCreated(uint256 indexed id, string url, uint256 price);
    event ProductSold(uint256 indexed id, address buyer, address contractAddress, uint256 tokenId, uint256 amount);
    event LimitSet(uint256 productId, uint256 limit);
    event AlternativeLanguageSet(uint256 productId, string language, string uri);

    uint public totalProducts;
    mapping(uint256 => Product) public products;
    mapping(uint256 => ContractInfo) public contractInfos;
    mapping(uint256 => uint256) public sales;
    mapping(uint256 => uint256) public saleLimits;
    mapping(uint256 => mapping(string => string)) public alternativeLanguageUris;


    address public spinnerTreasury;
    mapping(uint => bool) public idToSwap;
    mapping(uint => uint) public idToPriceCutBps;
    uint256 constant public denominator = 10_000;
    
    mapping(string => uint256[]) public productIdsPerRegion;

    /**
     * @notice Initializer for this contract
     * @param _fluencrAddress Fluencr contract address
     */
    function initialize(address _fluencrAddress) public initializer {
        __Ownable_init();
        _pause();
        Fluencr._init(_fluencrAddress);
    }

    /**
     * @notice Create new product sale
     * @param _productId The product id
     * @param _url The asset metadata url
     * @param _price Product price
     * @param _contractAddress Product contract address
     * @param _tokenId Product token id
     * @param _amount The amount of tokens minted on each sale
     * @param _region Region in which the product will be sold
     */
    function createProduct(
        uint256 _productId,
        string memory _url,
        uint256 _price,
        address _contractAddress,
        uint256 _tokenId,
        uint256 _amount,
        string memory _region
    ) external onlyOwner {
        if (_price == 0) {
            revert ProductPriceInvalid();
        }
        unchecked {
            ++totalProducts;
        }
        Product memory product = Product(_productId, _url, _price);
        products[_productId] = product;
        ContractInfo memory contractInfo = ContractInfo(_contractAddress, _tokenId, _amount);
        contractInfos[_productId] = contractInfo;

        productIdsPerRegion[_region].push(_productId);

        emit ProductCreated(_productId, _url, _price);
    }

    /**
     * @notice Change product sale
     * @param _productId Product contract address
     * @param _newProductId The new product id
     * @param _url The asset metadata url
     * @param _price Product price
     * @param _contractAddress NFT contract address
     * @param _tokenId Token Id to be minted
     * @param _amount Amount of token Id to be minted
     * @param _region Region in which the product will be sold
     */
    function changeProduct(
        uint256 _productId,
        uint256 _newProductId,
        string memory _url,
        uint256 _price,
        address _contractAddress,
        uint256 _tokenId,
        uint256 _amount,
        string memory _region
    ) external onlyOwner {
        if (keccak256(abi.encodePacked(products[_productId].url)) == keccak256(abi.encodePacked(""))) {
            revert NonExistentProduct();
        }

        if (_price == 0) {
            revert ProductPriceInvalid();
        }
        Product memory product = Product(_newProductId, _url, _price);
        products[_newProductId] = product;
        ContractInfo memory contractInfo = ContractInfo(_contractAddress, _tokenId, _amount);
        contractInfos[_newProductId] = contractInfo;
        sales[_newProductId] = sales[_productId];
        
        productIdsPerRegion[_region].push(_newProductId);
        
        if (_productId != _newProductId) {
            delete products[_productId];
            delete contractInfos[_productId];  
        }
    }

    function removeLastElement(string memory language) external onlyOwner {
        productIdsPerRegion[language].pop();
    }

    /**
     * @notice Sets alternative language metadata uri for given product id 
     * @param _productId Product id
     * @param _languages Corresponding language tag (https://en.wikipedia.org/wiki/IETF_language_tag)
     * @param _uris The uri in alternative language
     */
    function setAlternativeLanguageUris(uint256 _productId, string[] calldata _languages, string[] calldata _uris) external onlyOwner {
        if (_languages.length != _uris.length) {
            revert InvalidArgumetSize();
        }
        for (uint256 i; i < _languages.length; i += 1) {
            alternativeLanguageUris[_productId][_languages[i]] = _uris[i];
            emit AlternativeLanguageSet(_productId, _languages[i], _uris[i]);
        }
    }

    /**
     * @notice Sets limits for the amount of products that can be sold
     * @param _productId Product id
     * @param _limit The maximum amount of products that could be minted
     */
    function setSaleLimits(uint256 _productId, uint256 _limit) external onlyOwner {
        saleLimits[_productId] = _limit;

        emit LimitSet(_productId, _limit);
    }

    /**
     * @notice Used to flip between paused and unpaused
     */
    function toggleStatus() external onlyOwner {
        if(paused()) _unpause();
        else _pause();
    }

    /// @inheritdoc IFluencr
    function getProduct(uint256 _productId) external view override(IFluencr) returns (Product memory) {
        Product memory product = products[_productId];
        if (keccak256(abi.encodePacked(products[_productId].url)) == keccak256(abi.encodePacked(""))) {
            revert NonExistentProduct();
        }
        return product;
    }

    /// @inheritdoc Fluencr
    function _sell(address buyer, uint256 id, address tokenAddress) internal override(Fluencr) whenNotPaused {
        if (keccak256(abi.encodePacked(products[id].url)) == keccak256(abi.encodePacked(""))) {
            revert NonExistentProduct();
        }

        ContractInfo memory contractInfo = contractInfos[id];

        if (saleLimits[id] < sales[id] + 1) {
            revert SaleLimitExceeded();
        }

        sales[id] += 1;
        ITrustedMintable(contractInfo.contractAddress).trustedMint(buyer, contractInfo.tokenId, contractInfo.amount);
        if(idToSwap[id]) {
            uint price = ((products[id].price * 10**IERC20Extented(tokenAddress).decimals()) * idToPriceCutBps[id]) / denominator;
            ISpinner(spinnerTreasury).addSwapOrder(buyer, tokenAddress, price);
        }

        emit ProductSold(id, buyer, contractInfo.contractAddress, contractInfo.tokenId, contractInfo.amount);
    }

    /// @inheritdoc IFluencr
    function listProducts(string memory language) external view override(IFluencr) returns (Product[] memory) {
        uint productIdsLength = productIdsPerRegion[language].length;
        Product[] memory productList = new Product[](productIdsLength);

        for (uint256 i; i < productIdsLength; i++) {
            productList[i] = products[productIdsPerRegion[language][i]];
        }
        return productList;
    }

    function setSpinnerTreasury(address _spinnerTreasury) external onlyOwner {
        spinnerTreasury = _spinnerTreasury;
    }

    function setIdToSwap(uint _id, bool _shouldSwap) external onlyOwner {
        idToSwap[_id] = _shouldSwap;
    }

    function setIdToPriceCutBps(uint _productId, uint _priceCutBps) external onlyOwner{
        idToPriceCutBps[_productId] = _priceCutBps;
    }
}
