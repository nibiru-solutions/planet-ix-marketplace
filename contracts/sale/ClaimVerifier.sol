// SPDX-License-Identifier: MIT

pragma solidity ^0.8.10;

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "../interfaces/ITrustedMintable.sol";

error InvalidNotarization();
error NonceReused();

contract ClaimVerifier is OwnableUpgradeable {
    address public notary;
    address cg3;
    uint256 cg3TokenId;

    mapping(uint256 => bool) public usedNonces;

    event Claim(address indexed user, uint256 indexed nonce, uint256 amount);
    event SetNotary(address indexed oldNotary, address indexed notary);

    function initialize(address _cg3, uint256 _cg3TokenId) public initializer {
        __Ownable_init();
        setNotary(msg.sender);
        setCG3(_cg3, _cg3TokenId);
    }

    /// @notice allows a user to mint an nft
    /// @param amount the amount of nfts to mint
    /// @param nonce codeID from the backend
    /// @param signature a signature from the notary
    function mint(
        uint256 amount,
        uint256 nonce,
        bytes memory signature
    ) public {
        bytes32 hash = ECDSA.toEthSignedMessageHash(keccak256(abi.encode(amount, msg.sender, nonce)));

        if (notary != ECDSA.recover(hash, signature)) {
            revert InvalidNotarization();
        }
        if (usedNonces[nonce]) {
            revert NonceReused();
        }

        usedNonces[nonce] = true;
        ITrustedMintable(cg3).trustedMint(msg.sender, cg3TokenId, amount);

        emit Claim(msg.sender, nonce, amount);
    }

    // ADMIN FUNCTIONS
    function setNotary(address _notary) public onlyOwner {
        address oldNotary = notary;
        notary = _notary;
        emit SetNotary(oldNotary, _notary);
    }

    function setCG3(address _cg3, uint256 _cg3TokenId) public onlyOwner {
        cg3 = _cg3;
        cg3TokenId = _cg3TokenId;
    }
}
