// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;
import "./ILockProvider.sol";
import "./IMissionControl.sol";
import "./IMissionControlStaking.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import {IConnext} from "@connext/nxtp-contracts/contracts/core/connext/interfaces/IConnext.sol";
import {IXReceiver} from "@connext/nxtp-contracts/contracts/core/connext/interfaces/IXReceiver.sol";

contract MCCrosschainServices is OwnableUpgradeable, IXReceiver {
    uint32 public originDomain;
    address public source;
    IConnext public connext;
    IMissionControl public missionControl;
    address public aGold;
    address public aGoldLite;

    mapping(address => uint256) lockedNewlandsGenesis; // user => staked
    mapping(address => uint256) lockedYGenesis; // user => staked
    mapping(address => bool) public validSource;

    address public ygLock;
    address public ngLock;
    address public elgLock;
    mapping(address => uint256) lockedEternaLabsGenesis; // user => staked

    modifier onlySource(address _originSender, uint32 _origin) {
        require(
            _origin == originDomain && validSource[_originSender] == true && msg.sender == address(connext),
            "Expected source contract on origin domain called by Connext"
        );
        _;
    }

    function initialize() external initializer {
        __Ownable_init();
    }

    function xReceive(
        bytes32 _transferId,
        uint256 _amount,
        address _asset,
        address _originSender,
        uint32 _origin,
        bytes memory _callData
    ) external onlySource(_originSender, _origin) returns (bytes memory) {
        (address _user, uint256 _lockedBalance) = abi.decode(_callData, (address, uint256));

        if (_originSender == ngLock) {
            lockedNewlandsGenesis[_user] = _lockedBalance;

            if (_lockedBalance == 0) {
                // delete based on what rings the NG provides for free
                missionControl.deleteRentTiles(aGold, _user);
                missionControl.deleteRentTiles(aGoldLite, _user);
            }
        }
        if (_originSender == ygLock) {
            lockedYGenesis[_user] = _lockedBalance;
        }
        if (_originSender == elgLock) {
            lockedEternaLabsGenesis[_user] = _lockedBalance;
        }
    }

    function getNewlandsGenesisBalance(address _user) external view returns (uint256) {
        return lockedNewlandsGenesis[_user];
    }

    function getYGenesisBalance(address _user) external view returns (uint256) {
        return lockedYGenesis[_user];
    }

    function getEternaLabsGenesisBalance(address _user) external view returns (uint256) {
        return lockedEternaLabsGenesis[_user];
    }

    function setOriginDomain(uint32 _originDomain) external onlyOwner {
        originDomain = _originDomain;
    }

    function setSource(address _source) external onlyOwner {
        source = _source;
    }

    function setConnext(address _connext) external onlyOwner {
        connext = IConnext(_connext);
    }

    function setMissionControl(IMissionControl _missionControl) external onlyOwner {
        require(address(_missionControl) != address(0), "MCCS: ADDRESS ZERO");
        missionControl = _missionControl;
    }

    function setAGold(address _aGold) external onlyOwner {
        require(_aGold != address(0), "MCCS: ADDRESS ZERO");
        aGold = _aGold;
    }

    function setAGoldLite(address _aGoldLite) external onlyOwner {
        require(_aGoldLite != address(0), "MCCS: ADDRESS ZERO");
        aGoldLite = _aGoldLite;
    }

    function setValidSource(address _source, bool _isValid) external onlyOwner {
        validSource[_source] = _isValid;
    }

    function setYLock(address _lock) external onlyOwner {
        ygLock = _lock;
    }

    function setNLock(address _lock) external onlyOwner {
        ngLock = _lock;
    }

    function setELLock(address _lock) external onlyOwner {
        elgLock = _lock;
    }

    function setNewlandsBalance(address[] memory _user, uint[] memory _balance) external onlyOwner {
        for (uint i; i < _user.length; i++) lockedNewlandsGenesis[_user[i]] = _balance[i];
    }

    function setYBalance(address[] memory _user, uint[] memory _balance) external onlyOwner {
        for (uint i; i < _user.length; i++) lockedYGenesis[_user[i]] = _balance[i];
    }

    function setEternaLabsGenesisBalance(address[] memory _user, uint[] memory _balance) external onlyOwner {
        for (uint i; i < _user.length; i++) lockedEternaLabsGenesis[_user[i]] = _balance[i];
    }
}
