// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./IAssetManager.sol";

contract SafeMetashareTransfer is OwnableUpgradeable {

    event keyUsed(string _key);

    mapping(address => bool) public admins;
    mapping(string => bool) public usedKeys;

    address assetManager;

    uint public tokenId;
    uint public maxSupply;
    uint public minted;

    function initialize(address _assetManager) public initializer {
        __Ownable_init();
        admins[msg.sender] = true;
        assetManager = _assetManager;
    }

    modifier onlyAdmin() {
        if(!admins[msg.sender]) revert("Not Admin");
        _;
    }

    function setAssetManager (address _assetManager) external onlyAdmin {
        assetManager = _assetManager;
    }

    function setAdmin(address _user, bool _admin) external onlyOwner {
        admins[_user] = _admin;
    }

    function setMaxSupply(uint _amount) external onlyOwner {
        maxSupply = _amount;
    }

    function setTokenId(uint _tokenId) external onlyOwner {
        tokenId = _tokenId;
    }


    /**
    * @notice Mint function that takes a unique key which can only be used once.
    * @param _key, unique key from backend. Can only be used once.
    * @param _to, address that will receive tokens.
    * @param _amount, amount of tokens to be sent to receiver.
    */

    function mintWithOneTimeKey(string calldata _key, address _to, uint _amount) external onlyAdmin {
        if(usedKeys[_key]) revert('This key has already been used.');
        if(minted + _amount > maxSupply) revert('Supply limit exceeded');

        usedKeys[_key] = true;
        minted += _amount;
        IAssetManager(assetManager).trustedMint(_to, tokenId, _amount);

        emit keyUsed(_key);
    } 
}