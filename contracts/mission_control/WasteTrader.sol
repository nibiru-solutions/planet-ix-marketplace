// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.8;

// Trade a minimum of 25 Waste tokens for Astro Credits (NFT) at 1:1 ratio with a 5% fee going to the GWS Wallet and burn Waste Token

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "./IWasteToCash.sol";
import "./IAssetManager.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../util/Burnable.sol";
import "./IBaseLevel.sol";
/* error codes */
error WasteTrader__NotGovernance();
error WasteTrader__ZeroAddress();
error WasteTrader__MinimumOrderNotMet(uint256 _required);
error WasteTrader__MaximumOrderAmountExceeded(uint256 _tokenOrderAmount);
error WasteTrader__NoOrdersPlaced(address _player);
error WasteTrader__InvalidId(uint256 _id);
error WasteTrader__OrderNotYetCompleted(address _player, uint256 _d);
error WasteTrader__SpeedUpUnitsExceedClaimAmount(uint256 _mintableAmount);
error WasteTrader__IndexOutOfBounds(uint256 _index);
error WasteTrader__NoClaimableOrders(address player);
error WasteTrader__speedUpNotAvailable(uint orderId);
error WasteTrader__InvalidOrderId(uint orderId);

/**@title Waste Trading
 * @author @Haltoshi
 * @notice This contract is for trading Waste NFTs for Astro Credits
 * @dev This uses the NFT contract for minting and burning Polygon Asset NFTs
 */
contract WasteTrader is OwnableUpgradeable, ReentrancyGuardUpgradeable, IWasteToCash {
    /* State Variables */
    uint256 public nextOrderId;
    address private s_signer;
    IAssetManager private s_iAssetManager;
    address private s_moderator;
    address private s_feeWallet;
    uint256 public s_numerator;
    uint256 public s_denominator;
    uint256 public s_gwsTime;
    uint256 public s_speedupTime;
    uint256 public s_gwsMinimumOrder;
    uint256 public s_gwsTax;
    uint8 public s_gwsMax; //disabled
    uint8 public s_gwsSpeedupCost;
    mapping(address => Order[]) private s_wasteOrders;

    string public constant MINT_MESSAGE = "mission control";
    uint256 constant WASTE_ID = uint256(IAssetManager.AssetIds.Waste);
    uint256 constant ASTRO_CREDIT_ID = uint256(IAssetManager.AssetIds.AstroCredit);
    uint256 public constant MAXIMUM_BASIS_POINTS = 10_000;

    address public pixt;
    address beneficiary;
    uint public pixtSpeedupSplitBps;
    uint public pixtSpeedupCost;
    address public baseLevelAddress;

    /* Events */
    event WasteOrderPlaced(address indexed player, uint256 amount);
    event MintedMissionControl(address indexed player, uint256 indexed tokenId, uint256 amount);
    event WasteOrderSpedUp(address indexed player, uint256 amount, uint orderId);
    event WasteOrderCompleted(address indexed player, uint256 orderId);

    /* Modifiers */
    modifier onlyGov() {
        if (msg.sender != owner() || msg.sender != s_moderator) revert WasteTrader__NotGovernance();
        _;
    }

    /* Functions */
    /**
     * @notice Initializer for the WasteTrader contract
     * @param assetManagerImpl address of the implementation 
        contract for the assetManager interface
     * @param gwsWalletAddress gws wallet address for the fees
     * @param numerator exchange rate numerator
     * @param denominator exchange rate denominator
     * @param gwsTime duration of time to convert waste to Astro Credits
     * @param speedupTime the conversion reduction time to convert
        Waste tokens to Astro Credits
     * @param gwsMinimumOrder the minimum amount of Waste tokens to burn
     * @param gwsTax fee represented as basis points e.g. 500 == 5 pct
     * @param gwsMax maximum amount of waste tokens per 
        conversion to Astro Credits
     * @param gwsSpeedupCost amount of Astro Credits to speed up 
        conversion of Waste tokens
     */
    function initialize(
        address assetManagerImpl,
        address gwsWalletAddress,
        uint256 numerator,
        uint256 denominator,
        uint256 gwsTime,
        uint256 speedupTime,
        uint256 gwsMinimumOrder,
        uint256 gwsTax,
        uint8 gwsMax,
        uint8 gwsSpeedupCost
    ) public initializer {
        __Ownable_init();
        __ReentrancyGuard_init();
        s_iAssetManager = IAssetManager(assetManagerImpl);
        s_feeWallet = gwsWalletAddress;
        s_numerator = numerator;
        s_denominator = denominator;
        s_gwsTime = gwsTime;
        s_speedupTime = speedupTime;
        s_gwsMinimumOrder = gwsMinimumOrder;
        s_gwsTax = gwsTax;
        s_gwsMax = gwsMax;
        s_gwsSpeedupCost = gwsSpeedupCost;
        s_moderator = msg.sender;
    }

    /// @inheritdoc IWasteToCash
    function placeWasteOrder(uint256 _wasteAmount) external nonReentrant {
        if (s_gwsTime > 0) {
            Order[] memory wasteOrders = s_wasteOrders[msg.sender];
            if (wasteOrders.length == 0) {
                createNewOrder(_wasteAmount, 0);
            } else {
                uint256 totalOrderAmount;
                for (uint orderIndex; orderIndex < wasteOrders.length; ) {
                    if(!isFinished(wasteOrders[orderIndex])){
                        totalOrderAmount += wasteOrders[orderIndex].orderAmount;
                    }
                    unchecked {
                        ++orderIndex;
                    }
                }
                createNewOrder(_wasteAmount, totalOrderAmount);
            }
        } else {
            burnAndMintMissionControlToken(msg.sender, WASTE_ID, _wasteAmount);
        }
    }

    /** @notice places a new Waste order
     *  @param _wasteAmount amount of waste tokens
     */
    function createNewOrder(uint256 _wasteAmount, uint _currentWasteAmount) internal {
        if (baseLevelAddress == address(0)) revert("baseLevelAddress not set");
        if (_wasteAmount + _currentWasteAmount > IBaseLevel(baseLevelAddress).getOrderCapacity(address(this), msg.sender) * 25) revert WasteTrader__MaximumOrderAmountExceeded(_wasteAmount + _currentWasteAmount);
        if (_wasteAmount < s_gwsMinimumOrder)
            revert WasteTrader__MinimumOrderNotMet(s_gwsMinimumOrder);
        uint256 orderId = nextOrderId;
        nextOrderId++;

        s_iAssetManager.trustedBurn(msg.sender, WASTE_ID, _wasteAmount);
        s_wasteOrders[msg.sender].push(Order({
            orderId: orderId,
            orderAmount: _wasteAmount,
            createdAt: block.timestamp,
            speedUpDeductedAmount: 0,
            totalCompletionTime: s_gwsTime}));
        emit WasteOrderPlaced(msg.sender, _wasteAmount);
    }

    /** @notice removes a Waste order from the set of orders
     *  @param _index the orderId to remove
     */
    function _removeOrder(uint _index) internal {
        if (_index < s_wasteOrders[msg.sender].length -1)
            s_wasteOrders[msg.sender][_index] = s_wasteOrders[msg.sender][s_wasteOrders[msg.sender].length-1];
        s_wasteOrders[msg.sender].pop();
    }

    /// @inheritdoc ITrader
    function claimOrder(uint _orderId) external override {
        Order[] storage wasteOrders = s_wasteOrders[msg.sender];
        if (wasteOrders.length == 0) revert WasteTrader__NoOrdersPlaced(msg.sender);

        for(uint256 i=0; i < wasteOrders.length; i++) {
            if (wasteOrders[i].orderId == _orderId){
                if (isFinished(wasteOrders[i])) {
                    mintMissionControlToken(msg.sender, WASTE_ID, wasteOrders[i].orderAmount);
                    emit WasteOrderCompleted(msg.sender, _orderId);
                    _removeOrder(i);
                    return;
                }
                revert WasteTrader__OrderNotYetCompleted(msg.sender, _orderId);
            }
        }
        revert WasteTrader__InvalidOrderId(_orderId);
    }

    /// @inheritdoc ITrader
    function claimBatchOrder() external override {
        Order[] memory wasteOrders = s_wasteOrders[msg.sender];
        if (wasteOrders.length == 0) revert WasteTrader__NoOrdersPlaced(msg.sender);

        for(uint i=wasteOrders.length; i > 0; i--) {
            if (isFinished(wasteOrders[i-1])) {
                mintMissionControlToken(msg.sender, WASTE_ID, wasteOrders[i-1].orderAmount);
                emit WasteOrderCompleted(msg.sender, wasteOrders[i-1].orderId);
                _removeOrder(i-1);
            }
        }
    }

    /// @inheritdoc IWasteToCash
    function getExchangeRate() external view returns (uint256 _numerator, uint256 _denominator) {
        return (s_numerator, s_denominator);
    }

    /// @inheritdoc ITrader
    function speedUpOrder(uint _numSpeedUps, uint _orderId) external override nonReentrant {
        Order[] storage wasteOrders = s_wasteOrders[msg.sender];
        if (wasteOrders.length == 0) revert WasteTrader__NoOrdersPlaced(msg.sender);
        for (uint256 orderIndex; orderIndex < wasteOrders.length; ) {
            if(wasteOrders[orderIndex].orderId == _orderId) {
                if (!isFinished(wasteOrders[orderIndex])) {
                    uint256 mintableAmount = (s_numerator * wasteOrders[orderIndex].orderAmount) / s_denominator;
                    uint256 feeTokenAmount = calculateFee(s_gwsTax, mintableAmount);
                    uint256 previousSpeedUpPayed = (wasteOrders[orderIndex].speedUpDeductedAmount / s_speedupTime) * s_gwsSpeedupCost;

                    if ( previousSpeedUpPayed + (_numSpeedUps * s_gwsSpeedupCost) >= (mintableAmount - feeTokenAmount))
                        revert WasteTrader__SpeedUpUnitsExceedClaimAmount(mintableAmount);

                    // Speed up order
                    wasteOrders[orderIndex].speedUpDeductedAmount += _numSpeedUps * s_speedupTime;
                    uint completionTime = int(s_gwsTime) - int(wasteOrders[orderIndex].speedUpDeductedAmount) > 0 ? s_gwsTime - wasteOrders[orderIndex].speedUpDeductedAmount : 0;
                    wasteOrders[orderIndex].totalCompletionTime = completionTime;

                    emit WasteOrderSpedUp(msg.sender, wasteOrders[orderIndex].orderAmount, _orderId);
                    // burn AC payments
                    s_iAssetManager.trustedBurn(msg.sender, getMissionControlTokenToMint(WASTE_ID), _numSpeedUps * s_gwsSpeedupCost);
                    return;
                } else revert WasteTrader__speedUpNotAvailable(_orderId);
            }
            unchecked {
                ++orderIndex;
            }
        }
        revert WasteTrader__InvalidOrderId(_orderId);
    }

    function IXTSpeedUpOrder(uint _numSpeedUps, uint _orderId) external override {
        require(pixt != address (0), "IXT address not set");
        Order[] storage wasteOrders = s_wasteOrders[msg.sender];
        if (wasteOrders.length == 0) revert WasteTrader__NoOrdersPlaced(msg.sender);
        for (uint256 orderIndex; orderIndex < wasteOrders.length; ) {
            if(wasteOrders[orderIndex].orderId == _orderId) {
                if (!isFinished(wasteOrders[orderIndex])) {
                    uint256 mintableAmount = (s_numerator * wasteOrders[orderIndex].orderAmount) / s_denominator;
                    uint256 feeTokenAmount = calculateFee(s_gwsTax, mintableAmount);

                    // Speed up order
                    wasteOrders[orderIndex].speedUpDeductedAmount += _numSpeedUps * s_speedupTime;
                    uint completionTime = int(s_gwsTime) - int(wasteOrders[orderIndex].speedUpDeductedAmount) > 0 ? s_gwsTime - wasteOrders[orderIndex].speedUpDeductedAmount : 0;
                    wasteOrders[orderIndex].totalCompletionTime = completionTime;

                    require(IERC20(pixt).transferFrom(msg.sender, address(this), pixtSpeedupCost * _numSpeedUps), "Transfer of funds failed");
                    Burnable(pixt).burn((pixtSpeedupCost * _numSpeedUps * pixtSpeedupSplitBps) / 10000);
                    emit WasteOrderSpedUp(msg.sender, wasteOrders[orderIndex].orderAmount, _orderId);
                    return;
                } else revert WasteTrader__speedUpNotAvailable(_orderId);
            }
            unchecked {
                ++orderIndex;
            }
        }
        revert WasteTrader__InvalidOrderId(_orderId);
    }

    function setIXTSpeedUpParams(address _pixt, address _beneficiary, uint _pixtSpeedupSplitBps, uint _pixtSpeedupCost) external override onlyOwner{
        pixt = _pixt;
        beneficiary = _beneficiary;
        pixtSpeedupSplitBps = _pixtSpeedupSplitBps;
        pixtSpeedupCost = _pixtSpeedupCost;
    }

    /** @notice mint mission control token(s)
     *  @param from origin address
     *  @param tokenId to be burnt
     *  @param amount number of tokens to be minted
     */
    function mintMissionControlToken(
        address from,
        uint256 tokenId,
        uint256 amount
    ) internal {
        uint256 tokenIdToMint = getMissionControlTokenToMint(tokenId);

        // calculate exchange amount
        uint256 mintableAmount = (s_numerator * amount) / s_denominator;
        // calculate mintFee token amount
        uint256 feeTokenAmount = calculateFee(s_gwsTax, mintableAmount);
        // mint mission control token(s) to the player/caller
        s_iAssetManager.trustedMint(from, tokenIdToMint, mintableAmount - feeTokenAmount);
        // mint fee amount of tokens to the fee wallet
        s_iAssetManager.trustedMint(s_feeWallet, tokenIdToMint, feeTokenAmount);
        emit MintedMissionControl(from, tokenIdToMint, mintableAmount);
    }

    /** @notice burn and mint mission control token(s)
     *  @param from origin address
     *  @param tokenId to be burnt
     *  @param amount number of tokens to be burnt
     */
    function burnAndMintMissionControlToken(
        address from,
        uint256 tokenId,
        uint256 amount
    ) internal {
        uint256 tokenIdToMint = getMissionControlTokenToMint(tokenId);

        // calculate exchange amount
        uint256 mintableAmount = (s_numerator * amount) / s_denominator;
        // calculate mintFee token amount
        uint256 feeTokenAmount = calculateFee(s_gwsTax, mintableAmount);

        // burn mission control token(s) e.g. Waste Token at tokenId=7
        s_iAssetManager.trustedBurn(from, tokenId, amount);
        // mint mission control token(s) to the player/caller
        s_iAssetManager.trustedMint(from, tokenIdToMint, mintableAmount - feeTokenAmount);
        // mint fee amount of tokens to the fee wallet
        s_iAssetManager.trustedMint(s_feeWallet, tokenIdToMint, feeTokenAmount);
        emit MintedMissionControl(from, tokenIdToMint, mintableAmount);
    }

    /** @notice retrieves the mintable tokenId for a given 
        burnable tokenId for mission control tokens
     *  @param tokenIdToBurn tokenId of the mission control 
        token that gets burnt
     */
    function getMissionControlTokenToMint(uint256 tokenIdToBurn)
        internal
        pure
        returns (uint256 tokenIdToMint)
    {
        if (tokenIdToBurn == WASTE_ID) {
            return ASTRO_CREDIT_ID;
        }
    }

    /** @notice calculates the amount of tokens determined as a fee
     *  @param bpsFee fee amount as a bps (basis points) e.g. 500 == 5 pct
     *  @param amount number of tokens to be minted
     */
    function calculateFee(uint256 bpsFee, uint256 amount) internal pure returns (uint256) {
        return (amount * bpsFee) / MAXIMUM_BASIS_POINTS;
    }

    /** @notice verifies the message hash and signature of the signer
     *  @param hash message hash
     *  @param signature message signature
     *  @param to mint to address
     *  @param amount mint amount
     *  @param nonce sequential minter nonce
     */
    function checkMintValidity(
        bytes32 hash,
        bytes memory signature,
        address to,
        uint256 amount,
        uint256 nonce
    ) public view returns (bool) {
        bytes32 payloadHash = ECDSAUpgradeable.toEthSignedMessageHash(
            keccak256(abi.encodePacked(to, amount, MINT_MESSAGE, nonce))
        );
        bytes32 ethSignedMessageHash = ECDSAUpgradeable.toEthSignedMessageHash(hash);

        require(payloadHash == ethSignedMessageHash, "NFT: INVALID_HASH");
        require(
            ECDSAUpgradeable.recover(ethSignedMessageHash, signature) == s_signer,
            "NFT: INVALID_SIGNATURE"
        );
        return true;
    }

    function isFinished(Order memory order) internal view returns(bool _isFinished){
        _isFinished = (order.createdAt + order.totalCompletionTime) < block.timestamp;
    }
    /** @notice change signer
     *  @param _signer new signer
     */
    function setSigner(address _signer) external onlyGov {
        s_signer = _signer;
    }

    /** @notice retreive signer
     */
    function getSigner() public view returns (address) {
        return s_signer;
    }

    /** @notice change Moderator
     *  @param _moderator new moderator
     */
    function setModerator(address _moderator) external onlyOwner {
        if (_moderator == address(0)) revert WasteTrader__ZeroAddress();
        s_moderator = _moderator;
    }

    /** @notice retreive Moderator
     */
    function getModerator() public view returns (address) {
        return s_moderator;
    }

    /** @notice change Fee Wallet Address
     *  @param _feeWallet new fee wallet address
     */
    function setFeeWallet(address _feeWallet) external onlyGov {
        if (_feeWallet == address(0)) revert WasteTrader__ZeroAddress();
        s_feeWallet = _feeWallet;
    }

    /** @notice retreive Fee Wallet Address
     */
    function getFeeWallet() public view returns (address) {
        return s_feeWallet;
    }

    /** @notice change exchange rate
     *  @param _numerator numerator
     *  @param _denominator denominator
     */
    function setExchangeRate(uint256 _numerator, uint256 _denominator) external onlyGov {
        s_numerator = _numerator;
        s_denominator = _denominator;
    }

    /** @notice change duration of time to convert waste to Astro Credits
     *  @param _gwsTime converting time duration
     */
    function setGwsTime(uint256 _gwsTime) external onlyGov {
        s_gwsTime = _gwsTime;
    }

    /** @notice retreive duration of time to convert waste to Astro Credits
     */
    function getGwsTime() public view returns (uint256) {
        return s_gwsTime;
    }

    /** @notice change amount of Astro Credits to speed up 
        conversion of Waste tokens
     *  @param _gwsSpeedupCost Astro Credits amount
     */
    function setGwsSpeedupCost(uint8 _gwsSpeedupCost) external onlyGov {
        s_gwsSpeedupCost = _gwsSpeedupCost;
    }

    /** @notice retreive amount of Astro Credits to speed up 
        conversion of Waste tokens
     */
    function getGwsSpeedupCost() public view returns (uint8) {
        return s_gwsSpeedupCost;
    }

    /** @notice change the conversion reduction time to convert
        Waste tokens to Astro Credits
     *  @param _speedupTime conversion reduction time amount
     */
    function setSpeedupTime(uint256 _speedupTime) external onlyGov {
        s_speedupTime = _speedupTime;
    }

    /** @notice retreive the conversion reduction time to convert
        Waste tokens to Astro Credits
     */
    function getSpeedupTime() public view returns (uint256) {
        return s_speedupTime;
    }

    /** @notice change the minimum amount of Waste tokens to burn
        to redeem Astro Credits
     *  @param _gwsMinimumOrder minimum amount of Waste tokens
     */
    function setGwsMinimumOrder(uint16 _gwsMinimumOrder) external onlyGov {
        s_gwsMinimumOrder = _gwsMinimumOrder;
    }

    /** @notice retreive the minimum amount of Waste tokens to burn
        to redeem Astro Credits
     */
    function getGwsMinimumOrder() public view returns (uint256) {
        return s_gwsMinimumOrder;
    }

    /** @notice change basis points for the fee
     *  @param _gwsTax fee represented as basis points e.g. 500 == 5 pct
     */
    function setGwsTax(uint16 _gwsTax) external onlyGov {
        s_gwsTax = _gwsTax;
    }

    /** @notice retreive basis points for the fee
     */
    function getGwsTax() public view returns (uint256) {
        return s_gwsTax;
    }

    /** @notice change the implementation address for the iAssetManager
     *  @param _iAssetManager implementation address
     */
    function setIAssetManager(address _iAssetManager) external onlyGov {
        s_iAssetManager = IAssetManager(_iAssetManager);
    }

    /** @notice returns the iAssetManager
     */
    function getIAssetManager() public view returns (IAssetManager) {
        return s_iAssetManager;
    }

    function setBaseLevelAddress(address _baseLevelAddress) external override onlyOwner {
        baseLevelAddress = _baseLevelAddress;
    }

    /// @inheritdoc ITrader
    function getOrders(address _player) external view returns (Order[] memory) {
        Order[] memory wasteOrders = s_wasteOrders[_player];
        return wasteOrders;
    }
}
