// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./IBaseLevel.sol";

interface ISFStream {
    function getFlowRate(address superToken, address player) external view returns (int96);
}

interface IMCCrosschainServices {
    function getNewlandsGenesisBalance(address _user) external view returns (uint256);
}

error BaseLevel__TraderNotSet(address _trader);
error BaseLevel__SuperAddressNotSetUp();

contract BaseLevel is IBaseLevel, OwnableUpgradeable {
    mapping(address => mapping(uint => uint)) public traderCapacity;
    address public superAppAddress;
    address public superTokenAddress;
    address public superTokenLiteAddress;
    uint public superFluidPerLevel;
    address public crosschainServicesAddress;
    uint genesisLevel;

    function initialize() public initializer {
        __Ownable_init();
    }

    /**
     * @notice get base level for user.
     * @param _user to get base level for.
     * @return _level returns level of user.
     */
    function getBaseLevel(address _user) public view returns (uint _level) {
        if (superFluidPerLevel == 0) revert("superFluidPerLevel not set");
        if (superAppAddress == address(0) || superTokenAddress == address(0) || superTokenLiteAddress == address(0))
            revert BaseLevel__SuperAddressNotSetUp();
        uint constantLevel = 6;
        if (IMCCrosschainServices(crosschainServicesAddress).getNewlandsGenesisBalance(_user) != 0) {
            return genesisLevel;
        }
        int96 sfLevel = ISFStream(superAppAddress).getFlowRate(superTokenAddress, _user);
        int96 sfLevelLite = ISFStream(superAppAddress).getFlowRate(superTokenLiteAddress, _user);
        if (sfLevel != 0 || sfLevelLite != 0)
            return (uint(uint96(sfLevel + sfLevelLite)) / superFluidPerLevel) + constantLevel;
        return constantLevel;
    }

    /// @inheritdoc IBaseLevel
    function getOrderCapacity(address _trader, address _user) external view override returns (uint _extraOrders) {
        if (traderCapacity[_trader][5] == 0) revert BaseLevel__TraderNotSet(_trader);
        uint userLevel = getBaseLevel(_user);
        return _getCapacity(userLevel, _trader);
    }

    /**
     * @notice Internal function to get order amount for ITrader depending on level.
     * @param _baseLevel order amount for input base level.
     * @param _trader ITrader address.
     * @return return order amount.
     */
    function _getCapacity(uint _baseLevel, address _trader) internal view returns (uint) {
        for (uint i = _baseLevel; i > 0; i--) {
            if (traderCapacity[_trader][i] != 0) {
                return traderCapacity[_trader][i];
            }
        }
        revert("invalid baseLevel");
    }

    /// @inheritdoc IBaseLevel
    function setOrderCapacity(
        address _trader,
        uint _fromLevel,
        uint _toLevel,
        uint _additionalOrders
    ) external override onlyOwner {
        if (_fromLevel > _toLevel) revert("_fromLevel is greater than _toLevel");

        for (uint i = _fromLevel; i <= _toLevel; i++) {
            traderCapacity[_trader][i] = _additionalOrders;
        }
    }

    /// @inheritdoc IBaseLevel
    function setSuperAppAddress(address _superAppAddress) external override onlyOwner {
        superAppAddress = _superAppAddress;
    }

    /// @inheritdoc IBaseLevel
    function setSuperTokens(address _superToken, address _superTokenLite) external override onlyOwner {
        superTokenAddress = _superToken;
        superTokenLiteAddress = _superTokenLite;
    }

    /// @inheritdoc IBaseLevel
    function setSuperFluidPerLevel(uint _superFluidPerLevel) external override onlyOwner {
        superFluidPerLevel = _superFluidPerLevel;
    }

    function setCrosschainServicesAddress(address _crosschainServices) external onlyOwner {
        crosschainServicesAddress = _crosschainServices;
    }

    function setGenesisLevel(uint _level) external onlyOwner {
        genesisLevel = _level;
    }
}
