// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./ISFStream.sol";


contract MockSFStream is ISFStream {

    mapping(address => uint) public streamers;
    int96 flowLevel = 385802469135;

    function setStream(address _user, uint _amount) external {
        streamers[_user] = _amount;
    }

    function getFlowRate(address superToken, address player) override external view returns (int96){
        return int96(uint96(streamers[player])) * flowLevel;
    }
}
