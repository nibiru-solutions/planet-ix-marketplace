// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/// @title Handle batches of vrf requests for prospecting.
interface IOrderAutomation {

    function addRequestToBatch(uint _orderId) external;

    function getRandomNumberForOrder(uint _orderId) external view returns(uint);
}
