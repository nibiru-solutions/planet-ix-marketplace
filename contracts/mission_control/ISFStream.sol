// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface ISFStream {
    function getFlowRate(address superToken, address player) external view returns (int96);
}