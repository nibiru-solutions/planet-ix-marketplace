// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IMissionControlStakeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/utils/ERC721HolderUpgradeable.sol";
import "@prb/math/contracts/PRBMathSD59x18.sol";

import "hardhat/console.sol";

/**
 * @title Implementation of IMissionControlStakeable to handle ERC721 tokens
 */
contract ERC721MCStakeableAdapter is IMissionControlStakeable, OwnableUpgradeable, ERC721HolderUpgradeable {
    event ParametersUpdated(address _stakeable, uint256 _id, uint256 _ev, uint256 _cap, bool _isBase);

    struct TokenInfo {
        bool isBase;
        bool isRaidable;
        uint256 resourceEV;
        uint256 resourceId;
        uint256 resourceCap;
    }

    mapping(uint256 => uint256) seeds;
    mapping(address => mapping(uint256 => uint256)) tokenIds;
    mapping(int256 => mapping(int256 => int256)) maxCutoff;
    address missionControl;
    IERC721 erc721Token;
    int256 constant MAX_SCALE = 1e6;
    int256 constant MAX_HALF_SCALE = 5e5;
    int256 constant MAX_LOG2_E = 1_442695;
    uint256 constant PROBA_RES = 1000;
    uint256 constant CUTOFF_COEF = 115293;
    uint256 constant CUTOFF_INTERCEPT = 17322;
    uint256 constant DAY_IN_SEC = 86400;
    mapping(int256 => mapping(int256 => TokenInfo)) tokenInfo;

    /**
     * @notice Initializer for this contract
     */
    function initialize() external initializer {
        __Ownable_init();
    }

    /*
    IMissionControlStakeable implementation
    */

    modifier onlyMissionControl() {
        require(msg.sender == missionControl, "MCS_ADAPTER: Sender not mission control");
        _;
    }

    function setResourceParameters(
        uint256 _resourceId,
        uint256 _resourceEV,
        uint256 _resourceCap,
        bool _isBase,
        bool _isRaidable,
        int256 _x,
        int256 _y
    ) external onlyOwner {
        // This check is needed, otherwise we get division by zero
        require(_resourceEV > 0, "MCS_ADAPTER: Expected value is zero");
        tokenInfo[_x][_y].resourceId = _resourceId;
        tokenInfo[_x][_y].resourceEV = _resourceEV;
        tokenInfo[_x][_y].resourceCap = _resourceCap;
        tokenInfo[_x][_y].isBase = _isBase;
        tokenInfo[_x][_y].isRaidable = _isRaidable;
        if (_resourceEV > 0) {
            maxCutoff[_x][_y] = int256(CUTOFF_COEF * (_resourceCap / _resourceEV) + CUTOFF_INTERCEPT);
        }
        emit ParametersUpdated(address(this), _resourceId, _resourceEV, _resourceCap, _isBase);
    }

    function setMissionControl(address _missionControl) external onlyOwner {
        missionControl = _missionControl;
        emit MissionControlSet(_missionControl);
    }

    function setERC721(address _erc721) external onlyOwner {
        require(address(erc721Token) == address(0), "MCS_ADAPTER: ERC721 already set");
        erc721Token = IERC721(_erc721);
    }

    /// @inheritdoc IMissionControlStakeable
    function reset(address _userAddress, uint256 _nonce) external override onlyMissionControl {
        require(seeds[tokenIds[_userAddress][_nonce]] > 0, "MCS_ADAPTER: No such token staked");
        seeds[tokenIds[_userAddress][_nonce]] = block.timestamp;
    }

    /// @inheritdoc IMissionControlStakeable
    function checkTile(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external view onlyMissionControl returns (uint256, uint256) {
        return __checkTile(tokenIds[_userAddress][_nonce], _pausedAt, _x, _y);
    }

    /// @inheritdoc IMissionControlStakeable
    function onCollect(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external onlyMissionControl returns (uint256, uint256) {
        uint256 tokenId = tokenIds[_userAddress][_nonce];
        (uint256 amount, uint256 id) = __checkTile(tokenId, _pausedAt, _x, _y);
        seeds[tokenId] = _pausedAt > 0 ? _pausedAt : block.timestamp;
        return (amount, id);
    }

    /// @inheritdoc IMissionControlStakeable
    function isBase(uint256 _tokenId, int256 _x, int256 _y) external view override returns (bool) {
        return tokenInfo[_x][_y].isBase;
    }

    /// @inheritdoc IMissionControlStakeable
    function isRaidable(uint256 _tokenId, int256 _x, int256 _y) external view override returns (bool) {
        return tokenInfo[_x][_y].isRaidable;
    }

    /// @inheritdoc IMissionControlStakeable
    function onStaked(
        address _currentOwner,
        uint256 _tokenId,
        uint256 _nonce,
        address _underlyingToken,
        uint256 _underlyingTokenId,
        address _besideToken,
        uint256 _besideTokenId,
        uint256 _besideNonce,
        int256 _x,
        int256 _y
    ) external override onlyMissionControl {
        require(tokenIds[_currentOwner][_nonce] == 0, "MCS_ADAPTER: Invalid nonce");
        erc721Token.safeTransferFrom(_currentOwner, address(this), _tokenId);
        seeds[_tokenId] = block.timestamp;
        tokenIds[_currentOwner][_nonce] = _tokenId;
        emit TokenClaimed(_currentOwner, address(this), _tokenId);
    }

    function onResume(address _renter, uint256 _nonce) external {
        uint256 tokenId = tokenIds[_renter][_nonce];
        seeds[tokenId] = block.timestamp;
    }

    /// @inheritdoc IMissionControlStakeable
    function onUnstaked(
        address _newOwner,
        uint256 _nonce,
        uint256 /* _besideNonce */
    ) external override onlyMissionControl {
        uint256 _tokenId = tokenIds[_newOwner][_nonce];
        erc721Token.safeTransferFrom(address(this), _newOwner, _tokenId);
        seeds[_tokenId] = 0;
        emit TokenReturned(_newOwner, address(this), _tokenId);
    }

    function __checkTile(
        uint256 tokenId,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) internal view virtual returns (uint256, uint256) {
        TokenInfo memory _info = tokenInfo[_x][_y];
        if (_info.resourceEV == 0) {
            return (0, _info.resourceId);
        }
        int256 localSeed = int256(seeds[tokenId]);
        require(localSeed > 0, "MCS_ADAPTER: No such token staked");
        int256 blockTime = _pausedAt > 0 ? int256(_pausedAt) : int256(block.timestamp);
        if (blockTime - localSeed >= maxCutoff[_x][_y]) {
            return (_info.resourceCap, _info.resourceId);
        }
        uint256 ret = 0;
        int256 T = _getT(tokenId, localSeed, _info.resourceEV);
        while (localSeed + T < blockTime && ret < _info.resourceCap) {
            ret++;
            localSeed = localSeed + T;
            T = _getT(tokenId, localSeed, _info.resourceEV);
        }
        return (ret, _info.resourceId);
    }

    /// @inheritdoc IMissionControlStakeable
    function checkTimestamp(
        address _userAddress,
        uint256 _nonce
    ) external view override onlyMissionControl returns (uint256 _timestamp) {
        _timestamp = seeds[tokenIds[_userAddress][_nonce]];
    }

    /// @inheritdoc IMissionControlStakeable
    function timeLeft(
        address _userAddress,
        uint256 _nonce,
        int256 _x,
        int256 _y
    ) external view override onlyMissionControl returns (int256 _timeLeft) {
        int256 localSeed = int256(seeds[tokenIds[_userAddress][_nonce]]);
        require(localSeed > 0, "MCS_ADAPTER: No such token staked");
        _timeLeft = -1;
    }

    /*
    Poisson distribution implementation
    */

    function _getT(uint256 tokenId, int256 seed, uint256 resourceEV) internal view returns (int256 T) {
        require(resourceEV > 0, "PIX: Expected value is zero");
        T =
            (_max_div(int256(DAY_IN_SEC) * MAX_SCALE, int256(resourceEV) * MAX_SCALE) * _exponential_2(tokenId, seed)) /
            (MAX_SCALE * MAX_SCALE);
    }

    function _uniform(uint256 tokenId, int256 seed) internal pure returns (uint256 uniform_sample) {
        uniform_sample = uint256(keccak256(abi.encode(tokenId, seed))) % PROBA_RES;
    }

    function _exponential_2(uint256 tokenId, int256 seed) internal pure returns (int256 exponential_sample) {
        exponential_sample = -_max_ln(
            MAX_SCALE - _max_div(MAX_SCALE * int256(_uniform(tokenId, seed)), MAX_SCALE * int256(PROBA_RES))
        );
    }

    function _max_ln(int256 x) internal pure returns (int256 result) {
        result = (_max_log2(x) * MAX_SCALE) / MAX_LOG2_E;
    }

    function _max_log2(int256 x) internal pure returns (int256 result) {
        require(x > 0, "X has to be bigger than zero");
        unchecked {
            // This works because log2(x) = -log2(1/x).
            int256 sign;
            if (x >= MAX_SCALE) {
                sign = 1;
            } else {
                sign = -1;
                // Do the fixed-point inversion inline to save gas. The numerator is SCALE * SCALE.
                x = _max_div(MAX_SCALE, x);
            }

            // Calculate the integer part of the logarithm and add it to the result and finally calculate y = x * 2^(-n).
            uint256 n = PRBMath.mostSignificantBit(uint256(x / MAX_SCALE));

            // The integer part of the logarithm as a signed 59.18-decimal fixed-point number. The operation can't overflow
            // because n is maximum 255, SCALE is 1e18 and sign is either 1 or -1.
            result = int256(n) * MAX_SCALE;

            // This is y = x * 2^(-n).
            int256 y = x >> n;

            // If y = 1, the fractional part is zero.
            if (y == MAX_SCALE) {
                return result * sign;
            }

            // Calculate the fractional part via the iterative approximation.
            // The "delta >>= 1" part is equivalent to "delta /= 2", but shifting bits is faster.
            for (int256 delta = int256(MAX_HALF_SCALE); delta > 0; delta >>= 1) {
                y = (y * y) / MAX_SCALE;

                // Is y^2 > 2 and so in the range [2,4)?
                if (y >= 2 * MAX_SCALE) {
                    // Add the 2^(-m) factor to the logarithm.
                    result += delta;

                    // Corresponds to z/2 on Wikipedia.
                    y >>= 1;
                }
            }
            result *= sign;
        }
    }

    function _max_div(int256 x, int256 y) internal pure returns (int256 result) {
        // Get hold of the absolute values of x and y.
        uint256 ax;
        uint256 ay;
        unchecked {
            ax = x < 0 ? uint256(-x) : uint256(x);
            ay = y < 0 ? uint256(-y) : uint256(y);
        }

        // Compute the absolute value of (x*SCALE)÷y. The result must fit within int256.
        uint256 rAbs = PRBMath.mulDiv(ax, uint256(MAX_SCALE), ay);
        // Get the signs of x and y.
        uint256 sx;
        uint256 sy;
        assembly {
            sx := sgt(x, sub(0, 1))
            sy := sgt(y, sub(0, 1))
        }

        // XOR over sx and sy. This is basically checking whether the inputs have the same sign. If yes, the result
        // should be positive. Otherwise, it should be negative.
        result = sx ^ sy == 1 ? -int256(rAbs) : int256(rAbs);
    }
}
