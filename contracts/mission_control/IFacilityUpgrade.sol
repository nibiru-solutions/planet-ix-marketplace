// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/ITrader.sol";

interface IFacilityUpgrade is ITrader {
    error FacilityUpgrade__AlreadyUpgrading(uint256 currentOrder);
    error FacilityUpgrade__BaseLevelUnderMinimum();
    error FacilityUpgrade__CooldownNotFinished();
    error FacilityUpgrade__NoOrders();
    error FacilityUpgrade__InvalidSpeedupAmount();
    error FacilityUpgrade__NoSpeedUpAvailable();
    error FacilityUpgrade__InvalidOrderId(uint256 orderId);
    error FacilityUpgrade__ArrayLengthsDontMatch();
    error FacilityUpgrade__FacilityNotUpgradeable();
    error FacilityUpgrade__WrongAdditionalEnergy();
    error FacilityUpgrade__AlreadyClaimed();
    error FacilityUpgrade__OrderNotFinished();
    error FacilityUpgrade__CooldownAlreadyFinished();
    error FacilityUpgrade__NotOracle();

    event PlaceFacilityUpgradeOrder(address indexed player, uint256 indexed orderId, uint256 facilityTokenId, uint256 orderDoubleUpgradeChance);
    event SpeedUpCooldown(address indexed player, uint256 numSpeedUps, uint256 cooldownEndtimestamp);
    event FacilityUpgradeFinished(address indexed player, uint256 indexed orderId, uint256 upgradedFacilityId);
    event ClaimOrder(address indexed player, uint256 indexed orderId, uint256 requestId, uint256 cooldownEndTimestamp);
    event SpeedUpOrder(address indexed player, uint256 orderId, uint256 numSpeedups);
}