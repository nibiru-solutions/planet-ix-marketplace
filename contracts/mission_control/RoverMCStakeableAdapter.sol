// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../rover/VRFCoordinatorV2Interface.sol";

import "./IMissionControlStakeable.sol";
import "./IMissionControl.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/utils/ERC721HolderUpgradeable.sol";
import "../libraries/VRFConsumerBaseV2Upgradable.sol";

interface IVRFManager {
    function getRandomNumber(uint32 numWords) external returns (uint256 requestId);
}

interface IRNGConsumer {
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] calldata randomWords) external;
}

/// @title Interface for the Rover ERC721 contract
interface IRover {
    function rovers(uint256 tokenId) external view returns (string memory, uint256, uint256, uint256);

    function breakdown(uint256 tokenId, uint256 to) external;
}

interface IM3tam0d {
    function trustedMint(address _to, uint256 _tokenId, uint256 _amount) external;
}

interface IPIXAssetStakeableAdapter {
    function getDroneTier(uint256 _droneId) external returns (uint256);

    function applyBuff(address _user, uint _nonce, bool _applied) external;
}

/**
 * @title Implementation of IMissionControlStakeable to handle ERC721 tokens
 */
contract RoverMCStakeableAdapter is
    IMissionControlStakeable,
    OwnableUpgradeable,
    ERC721HolderUpgradeable,
    VRFConsumerBaseV2Upgradable,
    IRNGConsumer
{
    struct TokenInfo {
        bool isBase;
        bool isRaidable;
        uint256 resourceEV;
        uint256 resourceId;
        uint256 resourceCap;
    }

    struct OnCollectVars {
        uint256 tokenId;
        uint256 status;
        uint256 localSeed;
        uint256 fromTime;
    }
    event ParametersUpdated(address _stakeable, uint256 _id, uint256 _ev, uint256 _cap, bool _isBase);
    event M3tam0dMinted(address _recipient, uint256 _id, uint256 _amount);

    mapping(uint => mapping(int256 => mapping(int256 => TokenInfo))) public tokenInfo;

    mapping(uint256 => uint256) seeds;
    mapping(address => mapping(uint256 => uint256)) tokenIds;
    uint256 public maxCutoff;
    address public missionControl;
    address public erc721Token;

    VRFCoordinatorV2Interface public COORDINATOR;
    IM3tam0d public m3tam0d;
    uint256 public m3tam0dTokenId;

    bytes32 keyHash;
    uint32 callbackGasLimit;
    uint16 requestConfirmations;
    uint64 subscriptionId;

    address pixAssetAdapter;

    mapping(uint256 => Request) public s_requests;
    mapping(uint256 => mapping(uint256 => TokenInfo)) public tokenInfoByRing;
    mapping(int => mapping(int => uint)) public tileToRing;
    bool public paused;

    IVRFManager public vrfManager;

    /**
     * @notice Initializer for this contract
     */
    function initialize(
        address _vrfCoordinator,
        address _m3tam0d,
        uint256 _m3tam0dTokenId,
        bytes32 _keyHash,
        uint32 _callbackGasLimit,
        uint16 _requestConfirmations,
        uint64 _subscriptionId
    ) external initializer {
        COORDINATOR = VRFCoordinatorV2Interface(_vrfCoordinator);
        __VRFConsumerBaseV2_init(_vrfCoordinator);

        keyHash = _keyHash;
        callbackGasLimit = _callbackGasLimit;
        requestConfirmations = _requestConfirmations;
        subscriptionId = _subscriptionId;

        m3tam0d = IM3tam0d(_m3tam0d);
        m3tam0dTokenId = _m3tam0dTokenId;
        maxCutoff = 10 days;
        __Ownable_init();
    }

    /*
    IMissionControlStakeable implementation
    */

    modifier onlyMissionControl() {
        require(msg.sender == missionControl, "MCRover_ADAPTER: Sender not mission control");
        _;
    }

    modifier onlyOracle() {
        require(msg.sender == address(vrfManager), "MCRover_ADAPTER: Sender not VRF manager");
        _;
    }

    modifier whenNotPaused() {
        require(!paused, "MCRover_ADAPTER: Paused");
        _;
    }

    function setResourceParameters(
        uint _tier,
        uint256 _resourceEV,
        uint256 _resourceId,
        uint256 _resourceCap,
        bool _isBase,
        bool _isRaidable,
        uint256 _ring
    ) public onlyOwner {
        tokenInfoByRing[_tier][_ring].resourceEV = _resourceEV;
        tokenInfoByRing[_tier][_ring].resourceId = _resourceId;
        tokenInfoByRing[_tier][_ring].resourceCap = _resourceCap;
        tokenInfoByRing[_tier][_ring].isBase = _isBase;
        tokenInfoByRing[_tier][_ring].isRaidable = _isRaidable;
        emit ParametersUpdated(address(this), _resourceId, _resourceEV, _resourceCap, _isBase);
    }

    function setResourceParametersBulk(ResourceParams[] memory param) external onlyOwner {
        for (uint i; i < param.length; i++)
            setResourceParameters(
                param[i].tier,
                param[i].resourceEV,
                param[i].resourceId,
                param[i].resourceCap,
                param[i].isBase,
                param[i].isRaidable,
                param[i].ring
            );
    }

    function setRings(RingParams[] memory ringParams) external onlyOwner {
        for (uint i; i < ringParams.length; i++) {
            tileToRing[ringParams[i].x][ringParams[i].y] = ringParams[i].ring;
        }
    }

    function setMissionControl(address _missionControl) external onlyOwner {
        missionControl = _missionControl;
        emit MissionControlSet(_missionControl);
    }

    function setPixAssetAdapter(address _pixAssetAdapter) external onlyOwner {
        pixAssetAdapter = _pixAssetAdapter;
    }

    function setERC721(address _erc721) external onlyOwner {
        require(_erc721 != address(0), "MCRover_ADAPTER: Address Zero");
        erc721Token = _erc721;
    }

    /// @inheritdoc IMissionControlStakeable
    function reset(address _userAddress, uint256 _nonce) external override onlyMissionControl {
        require(seeds[tokenIds[_userAddress][_nonce]] > 0, "MCRover_ADAPTER: No such token staked");
        seeds[tokenIds[_userAddress][_nonce]] = block.timestamp;
    }

    /// @inheritdoc IMissionControlStakeable
    function checkTile(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external view override onlyMissionControl returns (uint256, uint256) {
        (uint256 amount, uint256 id, ) = __checkTile(_userAddress, _nonce, _pausedAt, _x, _y);
        return (amount, id);
    }

    /// @inheritdoc IMissionControlStakeable
    function onCollect(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external override onlyMissionControl whenNotPaused returns (uint256, uint256) {
        OnCollectVars memory vars;

        vars.tokenId = tokenIds[_userAddress][_nonce];
        (, , vars.status, ) = IRover(erc721Token).rovers(vars.tokenId);
        vars.localSeed = seeds[vars.tokenId];
        vars.fromTime = _pausedAt > 0 ? _pausedAt : block.timestamp;
        require(vars.localSeed != 0, "MCRover_ADAPTER: No such token staked");
        if (vars.status == 3) return (0, 7);
        if (vars.fromTime - vars.localSeed < maxCutoff) return (0, 7);

        (uint256 amount, uint256 id, uint256 m3tam0dChance) = __checkTile(_userAddress, _nonce, _pausedAt, _x, _y);
        seeds[vars.tokenId] = vars.fromTime;

        // vrf request random word for m3tam0dChance
        require(keyHash != bytes32(0), "Must have valid key hash");
        uint256 requestId = vrfManager.getRandomNumber(1);

        s_requests[requestId] = Request({user: _userAddress, m3tam0dChance: m3tam0dChance, tokenId: vars.tokenId});
        return (amount, id);
    }

    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords) internal override {}

    /**
     * @notice Function for satisfying randomness requests from minting
     * @param requestId The particular request being serviced
     * @param randomWords Array of the random numbers requested
     */
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external override onlyOracle {
        Request memory request = s_requests[requestId];
        (, , uint256 status, ) = IRover(erc721Token).rovers(request.tokenId);
        uint256 randomWord = randomWords[0] % 10_000;
        if (randomWord < request.m3tam0dChance) {
            m3tam0d.trustedMint(request.user, m3tam0dTokenId, 1);
            emit M3tam0dMinted(request.user, m3tam0dTokenId, 1);
        }

        // second random word for rover breakdown
        uint256 randomWord2 = randomWords[0] % 1000;
        if (randomWord2 > 500) {
            IRover(erc721Token).breakdown(request.tokenId, status + 1);
        } else if (randomWord2 == 500) {
            if (status < 2) {
                IRover(erc721Token).breakdown(request.tokenId, status + 2);
            } else {
                IRover(erc721Token).breakdown(request.tokenId, status + 1);
            }
        }
        delete s_requests[requestId];
    }

    /// @inheritdoc IMissionControlStakeable
    function isBase(uint256 _tier, int256 _x, int256 _y) external view override returns (bool) {
        return tokenInfoByRing[_tier][tileToRing[_x][_y]].isBase;
    }

    /// @inheritdoc IMissionControlStakeable
    function isRaidable(uint256 _tier, int256 _x, int256 _y) external view override returns (bool) {
        return tokenInfoByRing[_tier][tileToRing[_x][_y]].isRaidable;
    }

    function _isDroneId(uint256 _besideTokenId) internal pure returns (bool) {
        return (_besideTokenId == 5 || _besideTokenId == 4 || _besideTokenId == 33);
    }

    /// @inheritdoc IMissionControlStakeable
    function onStaked(
        address _currentOwner,
        uint256 _tokenId,
        uint256 _nonce,
        address /* _underlyingToken */,
        uint256 /* _underlyingTokenId */,
        address _besideToken,
        uint256 _besideTokenId,
        uint256 _besideNonce,
        int256 _x,
        int256 _y
    ) external override onlyMissionControl {
        require(tokenIds[_currentOwner][_nonce] == 0, "MCRover_ADAPTER: Invalid nonce");

        bool droneStaked = (_besideToken == pixAssetAdapter && _isDroneId(_besideTokenId));

        require(_besideToken == address(0) || droneStaked, "MCRover_ADAPTER: Rover can be staked only besides drone");

        if (droneStaked) {
            (, uint256 roverRank, , ) = IRover(erc721Token).rovers(_tokenId);
            if (roverRank == IPIXAssetStakeableAdapter(pixAssetAdapter).getDroneTier(_besideTokenId)) {
                IPIXAssetStakeableAdapter(pixAssetAdapter).applyBuff(_currentOwner, _besideNonce, true);
            }
        }

        seeds[_tokenId] = block.timestamp;
        tokenIds[_currentOwner][_nonce] = _tokenId;
        IERC721(erc721Token).safeTransferFrom(_currentOwner, address(this), _tokenId);
        emit TokenClaimed(_currentOwner, address(this), _tokenId);
    }

    function onResume(address _renter, uint256 _nonce) external {
        uint256 tokenId = tokenIds[_renter][_nonce];
        seeds[tokenId] = block.timestamp;
    }

    /// @inheritdoc IMissionControlStakeable
    function onUnstaked(address _newOwner, uint256 _nonce, uint256 _besideNonce) external override onlyMissionControl {
        uint256 _tokenId = tokenIds[_newOwner][_nonce];

        if (_besideNonce != 0) IPIXAssetStakeableAdapter(pixAssetAdapter).applyBuff(_newOwner, _besideNonce, false);

        IERC721(erc721Token).safeTransferFrom(address(this), _newOwner, _tokenId);
        seeds[_tokenId] = 0;
        emit TokenReturned(_newOwner, address(this), _tokenId);
    }

    function __checkTile(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) internal view virtual returns (uint256, uint256, uint256) {
        CheckTileInputs memory vars;
        vars.tokenId = tokenIds[_userAddress][_nonce];
        (, uint256 color, , ) = IRover(erc721Token).rovers(vars.tokenId);
        TokenInfo memory _info = tokenInfoByRing[color][tileToRing[_x][_y]];
        if (_info.resourceEV == 0) {
            return (0, _info.resourceId, 0);
        }
        vars.localSeed = seeds[vars.tokenId];
        require(vars.localSeed != 0, "MCRover_ADAPTER: No such token staked");
        (uint256 rate, uint256 m3tam0dChance) = getRoverRate(vars.tokenId, _info.resourceEV);
        vars.blockTime = _pausedAt > 0 ? _pausedAt : block.timestamp;
        vars.elapsed = vars.blockTime - vars.localSeed;

        if (vars.elapsed > maxCutoff) {
            vars.ret = rate;
        } else {
            vars.ret = (rate * vars.elapsed) / maxCutoff;
        }
        return (vars.ret, tokenInfoByRing[color][tileToRing[_x][_y]].resourceId, m3tam0dChance);
    }

    /**
     * @dev returns the amount of waste earned by the rover in a 24 hour period
     * @param tokenId the id of the rover
     * @return rate amount of waste earned by the rover in a 10 day period (max)
     * @return m3tam0dChance chance of getting a m3tam0d out of 10_000
     */
    function getRoverRate(
        uint256 tokenId,
        uint256 resourceEV
    ) public view returns (uint256 rate, uint256 m3tam0dChance) {
        (, uint256 color, uint256 status, ) = IRover(erc721Token).rovers(tokenId);
        if (color == 0) {
            m3tam0dChance = 500;
        } else if (color == 1) {
            m3tam0dChance = 50;
        } else {
            m3tam0dChance = 5;
        }
        rate = resourceEV;
        if (status == 1) {
            rate = (rate * 900) / 1000;
        } else if (status == 2) {
            rate = (rate * 600) / 1000;
        } else if (status == 3) {
            rate = 0;
        }
    }

    function getResourceParams(uint _tier, int _maxRad) external view returns (ResourceParamOut[] memory outParam) {
        uint count;
        outParam = new ResourceParamOut[](18);
        for (int256 x = -_maxRad; x <= _maxRad; x++) {
            for (int256 y = -_maxRad; y <= _maxRad; y++) {
                for (int256 z = -_maxRad; z <= _maxRad; z++) {
                    int256 sum = x + y + z;
                    uint256 rad = (abs(x) + abs(y) + abs(z)) / 2;
                    if (rad == uint(_maxRad) && rad > 0 && sum == 0) {
                        TokenInfo memory info = tokenInfoByRing[_tier][tileToRing[x][y]];
                        outParam[count].x = x;
                        outParam[count].y = y;
                        outParam[count].z = z;
                        outParam[count].isBase = info.isBase;
                        outParam[count].isRaidable = info.isRaidable;
                        outParam[count].resourceEV = info.resourceEV;
                        outParam[count].resourceId = info.resourceId;
                        outParam[count].resourceCap = info.resourceCap;
                        ++count;
                    }
                }
            }
        }
    }

    /// @inheritdoc IMissionControlStakeable
    function checkTimestamp(address _userAddress, uint256 _nonce) external view override returns (uint256 _timestamp) {
        _timestamp = seeds[tokenIds[_userAddress][_nonce]];
    }

    /// @inheritdoc IMissionControlStakeable
    function timeLeft(
        address _userAddress,
        uint256 _nonce,
        int256 /* _x */,
        int256 /* _y */
    ) external view override onlyMissionControl returns (int256) {
        uint256 tokenId = tokenIds[_userAddress][_nonce];
        uint256 localSeed = seeds[tokenId];
        require(localSeed != 0, "MCRover_ADAPTER: No such token staked");
        uint256 completionTime = localSeed + uint256(maxCutoff);
        int256 _timeLeft = int256(completionTime) - int256(block.timestamp);
        return _timeLeft > 0 ? _timeLeft : int256(0);
    }

    /**
     * @notice Verifies that a rover is staked with a particular tier
     * @param _userAddress The address of the user
     * @param _nonce The nonce of the rover
     * @param _tier The required tier of the rover
     * @return true if the rover is staked with the required tier
     */
    function roverStakedWithTier(address _userAddress, uint256 _nonce, uint256 _tier) external view returns (bool) {
        uint256 tokenId = tokenIds[_userAddress][_nonce];
        (, uint256 color, , ) = IRover(erc721Token).rovers(tokenId);
        return color == _tier;
    }

    /** ADMIN FUNCTIONS **/
    /**
     * @notice set keyHash by owner
     */
    function setKeyHash(bytes32 _keyHash) external onlyOwner {
        keyHash = _keyHash;
    }

    /**
     * @notice set callbackGasLimit by owner
     */
    function setCallbackGasLimit(uint32 _callbackGasLimit) external onlyOwner {
        callbackGasLimit = _callbackGasLimit;
    }

    /**
     * @notice set subscriptionId by owner
     */
    function setSubscriptionId(uint64 _subscriptionId) external onlyOwner {
        subscriptionId = _subscriptionId;
    }

    /**
     * @notice set requestConfirmations by owner
     */
    function setRequestConfirmations(uint16 _requestConfirmations) external onlyOwner {
        requestConfirmations = _requestConfirmations;
    }

    /**
     * @notice set maxCutoff by owner
     */
    function setMaxCutoff(uint256 _maxCutoff) external onlyOwner {
        maxCutoff = _maxCutoff;
    }

    function setPaused(bool _paused) external onlyOwner {
        paused = _paused;
    }

    function setVrfManager(address _vrfManager) external onlyOwner {
        vrfManager = IVRFManager(_vrfManager);
    }

    /**
     * @notice Calculates the absolute value of an integer
     * @param _value The integer
     * @return Its absolute value
     */
    function abs(int256 _value) private pure returns (uint256) {
        return _value < 0 ? uint256(-_value) : uint256(_value);
    }
}