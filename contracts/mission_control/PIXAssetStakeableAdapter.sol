// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IMissionControlStakeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@prb/math/contracts/PRBMathSD59x18.sol";
import "../interfaces/IPIX.sol";
import "hardhat/console.sol";

interface IRover {
    function rovers(
        uint256 tokenId
    ) external view returns (string memory, uint256, uint256, uint256);
}

contract PIXAssetStakeableAdapter is
    IMissionControlStakeable,
    OwnableUpgradeable,
    ERC1155HolderUpgradeable
{
    enum Distribution {
        Poisson,
        LinearDeterministic,
        Facility
    }

    event FacilityStakeRequirementsUpdated(uint256 _tokenId, uint256 _pixRarity, bool _allowed);

    event RarityProviderUpdated(address rarityProvider, address PIX);

    event TokenInfoUpdated(
        bool isStakeable,
        bool isBase,
        bool isRaidable,
        Distribution class,
        uint256 resourceEV,
        uint256 resourceId,
        uint256 resourceCap
    );

    struct TokenInfo {
        bool isStakeable;
        bool isBase;
        bool isRaidable;
        Distribution class;
        uint256 resourceEV;
        uint256 resourceId;
        uint256 resourceCap;
    }

    struct TokenInfoParams {
        uint256 tokenId;
        bool isStakeable;
        bool isBase;
        bool isRaidable;
        Distribution class;
        uint256 resourceEv;
        uint256 resourceId;
        uint256 resourceCap;
        uint256 ring;
    }

    mapping(address => mapping(uint256 => uint256)) private seeds;
    mapping(address => mapping(uint256 => uint256)) private tokenIds;
    mapping(address => mapping(uint256 => uint256)) public facilityOutput;
    mapping(address => mapping(uint256 => uint256)) public facilityLeadTime;
    mapping(uint256 => mapping(int256 => mapping(int256 => TokenInfo))) public tokenInfo;
    mapping(uint256 => mapping(uint256 => bool)) public facilityStakeReq;
    IERC1155 erc1155Token;
    IPIX pix;
    address private pixAddress;
    address private missionControl;
    uint256[] public leadTimes;
    uint256[] public prodAmounts;

    int256 private constant MAX_SCALE = 1e6;
    int256 private constant MAX_HALF_SCALE = 5e5;
    int256 private constant MAX_LOG2_E = 1_442695;
    uint256 private constant PROBA_RES = 1000;
    uint256 private constant CUTOFF_COEF = 115293;
    uint256 private constant CUTOFF_INTERCEPT = 17322;
    uint256 private constant STRUCTURE_BONUS_DENOMINATOR = 10000;
    uint256 private constant DRONE_BUFF = 12000;
    uint256 private constant DRONE_BUFF_DENOMINATOR = 10000;

    mapping(uint256 => mapping(int256 => mapping(int256 => uint256))) private cutoffs;
    mapping(int256 => mapping(int256 => uint256)) private structureBonuses;
    mapping(address => mapping(uint256 => bool)) public droneBuffApplied;
    address public rover;
    mapping(uint256 => uint256) public droneTier;
    address public roverAdapter;
    mapping(uint256 => mapping(uint256 => uint256)) private cutoffsByRing;
    mapping(uint256 => mapping(uint256 => TokenInfo)) public tokenInfoByRing;
    mapping(int => mapping(int => uint)) public tileToRing;
    mapping(uint256 => uint256) public leadTimeBonusByRing;

    uint256 private constant DAY_IN_SEC = 86400;

    modifier onlyMissionControl() {
        require(msg.sender == missionControl, "MCS_ADAPTER: Sender not mission control");
        _;
    }

    modifier onlyStakedTokens(address _userAddress, uint256 _nonce) {
        require(seeds[_userAddress][_nonce] != 0, "MCS1155_ADAPTER: No such token staked");
        _;
    }

    modifier onlyRoverAdapter() {
        require(msg.sender == roverAdapter, "MCS1155_ADAPTER: Not Rover Adapter");
        _;
    }

    function initialize() external initializer {
        __Ownable_init();
    }

    function setTokenInfo(
        uint256 _tokenId,
        bool _isStakeable,
        bool _isBase,
        bool _isRaidable,
        Distribution _class,
        uint256 _resourceEv,
        uint256 _resourceId,
        uint256 _resourceCap,
        uint256 _ring
    ) public onlyOwner {
        tokenInfoByRing[_tokenId][_ring].isStakeable = _isStakeable;
        tokenInfoByRing[_tokenId][_ring].isBase = _isBase;
        tokenInfoByRing[_tokenId][_ring].isRaidable = _isRaidable;
        tokenInfoByRing[_tokenId][_ring].class = _class;
        tokenInfoByRing[_tokenId][_ring].resourceEV = _resourceEv;
        tokenInfoByRing[_tokenId][_ring].resourceId = _resourceId;
        tokenInfoByRing[_tokenId][_ring].resourceCap = _resourceCap;
        //cutoff should update by tile
        __update_cutoff(_tokenId, _resourceEv, _resourceCap, _ring);
        emit TokenInfoUpdated(
            _isStakeable,
            _isBase,
            _isRaidable,
            _class,
            _resourceEv,
            _resourceId,
            _resourceCap
        );
    }

    function setTokenInfoBulk(TokenInfoParams[] calldata params) external onlyOwner {
        for (uint i; i < params.length; i++) {
            setTokenInfo(
                params[i].tokenId,
                params[i].isStakeable,
                params[i].isBase,
                params[i].isRaidable,
                params[i].class,
                params[i].resourceEv,
                params[i].resourceId,
                params[i].resourceCap,
                params[i].ring
            );
        }
    }

    function setRings(RingParams[] memory ringParams) external onlyOwner {
        for (uint i; i < ringParams.length; i++) {
            tileToRing[ringParams[i].x][ringParams[i].y] = ringParams[i].ring;
        }
    }

    function setStructureBonuses(int256 _x, int256 _y, uint256 _bonus) external onlyOwner {
        structureBonuses[_x][_y] = _bonus;
    }

    function setLeadTimeBonuses(
        uint256[] memory _ring,
        uint256[] memory _bonus
    ) external onlyOwner {
        for (uint i; i < _ring.length; i++) leadTimeBonusByRing[_ring[i]] = _bonus[i];
    }

    function setFacilityStats(uint256[] memory _lead, uint256[] memory _amt) external onlyOwner {
        leadTimes = _lead;
        prodAmounts = _amt;
    }

    function __update_cutoff(uint256 _id, uint256 _ev, uint256 _cap, uint256 _ring) private {
        cutoffsByRing[_id][_ring] = CUTOFF_COEF * (_cap / _ev) + CUTOFF_INTERCEPT;
    }

    function setFacilityStakeRequirements(
        uint256 _tokenId,
        uint256 _pixRarity,
        bool _allowed
    ) external onlyOwner {
        facilityStakeReq[_tokenId][_pixRarity] = _allowed;
        emit FacilityStakeRequirementsUpdated(_tokenId, _pixRarity, _allowed);
    }

    function setRarityProvider(address _provider, address _pixAddress) external onlyOwner {
        pix = IPIX(_provider);
        pixAddress = _pixAddress;
        emit RarityProviderUpdated(_provider, _pixAddress);
    }

    function setMissionControl(address _missionControl) external onlyOwner {
        missionControl = _missionControl;
        emit MissionControlSet(_missionControl);
    }

    function setERC1155(address _erc1155) external onlyOwner {
        require(
            address(erc1155Token) == address(0),
            "MC1155_ADAPTER: Erc1155 has already been set"
        );
        erc1155Token = IERC1155(_erc1155);
    }

    function setRover(address _rover) external onlyOwner {
        rover = _rover;
    }

    function setRoverAdpater(address _adapter) external onlyOwner {
        roverAdapter = _adapter;
    }

    function setDroneTier(
        uint256[] memory _droneId,
        uint256[] memory _droneTier
    ) external onlyOwner {
        for (uint256 i; i < _droneId.length; i++) droneTier[_droneId[i]] = _droneTier[i];
    }

    function getDroneTier(uint256 _droneId) external view returns (uint256) {
        return droneTier[_droneId];
    }

    /// @inheritdoc IMissionControlStakeable
    function reset(address _userAddress, uint256 _nonce) external override onlyMissionControl {
        require(seeds[_userAddress][_nonce] > 0, "MCS1155_ADAPTER: No such token staked");
        seeds[_userAddress][_nonce] = block.timestamp;
    }

    /**
     * @notice Verifies that a rover is staked with a particular tier
     * @param _tier The required tier of the rover
     * @return true if the rover is staked with the required tier
     */
    function roverStakedWithTier(uint256 _roverTokenId, uint256 _tier) public view returns (bool) {
        (, uint256 color, , ) = IRover(rover).rovers(_roverTokenId);
        return color == _tier;
    }

    function applyBuff(address _user, uint _nonce, bool _applied) external onlyRoverAdapter {
        droneBuffApplied[_user][_nonce] = _applied;
    }

    /// @inheritdoc IMissionControlStakeable
    function checkTile(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external view override returns (uint256 _amount, uint256 _retTokenId) {
        return __check_tile(_userAddress, _nonce, _pausedAt, _x, _y);
    }

    /// @inheritdoc IMissionControlStakeable
    function onCollect(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external override onlyMissionControl returns (uint256, uint256) {
        (uint256 available, uint256 id) = __check_tile(_userAddress, _nonce, _pausedAt, _x, _y);
        seeds[_userAddress][_nonce] = _pausedAt > 0 ? _pausedAt : block.timestamp;
        return (available, id);
    }

    /// @inheritdoc IMissionControlStakeable
    function isBase(
        uint256 _tokenId,
        int256 _x,
        int256 _y
    ) external view override returns (bool _isBase) {
        _isBase = tokenInfoByRing[_tokenId][tileToRing[_x][_y]].isBase;
    }

    /// @inheritdoc IMissionControlStakeable
    function isRaidable(
        uint256 _tokenId,
        int256 _x,
        int256 _y
    ) external view override returns (bool _isRaidable) {
        _isRaidable = tokenInfoByRing[_tokenId][tileToRing[_x][_y]].isRaidable;
    }

    /// @inheritdoc IMissionControlStakeable
    function onStaked(
        address _currentOwner,
        uint256 _tokenId,
        uint256 _nonce,
        address _underlyingToken,
        uint256 _underlyingTokenId,
        address _besideToken,
        uint256 _besideTokenId,
        uint256 _besideNonce,
        int256 _x,
        int256 _y
    ) external override onlyMissionControl {
        require(
            tokenInfoByRing[_tokenId][tileToRing[_x][_y]].isStakeable,
            "MCS1155_ADAPTER: Token not stakeable"
        );
        require(seeds[_currentOwner][_nonce] == 0, "MCS1155_ADAPTER: Nonce already used");
        require(
            _besideToken != address(this),
            "MCS1155_ADAPTER: Cannot stake 2 things from pixAdapter"
        );
        if (tokenInfoByRing[_tokenId][tileToRing[_x][_y]].class == Distribution.Facility) {
            require(
                __handleFacilitySetup(
                    _tokenId,
                    _currentOwner,
                    _underlyingToken,
                    _underlyingTokenId,
                    _nonce
                ),
                "MCS1155_ADAPTER: Facility must be staked on correct tier"
            );
            require(
                _besideToken == address(0),
                "MCS1155_ADAPTER: Facility Cannot Be Staked alongside anything else"
            );
        }
        require(
            tokenInfoByRing[_besideTokenId][tileToRing[_x][_y]].class != Distribution.Facility,
            "MCS1155_ADAPTER: Facility Cannot Be Staked alongside anything else"
        );
        seeds[_currentOwner][_nonce] = block.timestamp;
        tokenIds[_currentOwner][_nonce] = _tokenId;

        if (
            (_tokenId == 5 || _tokenId == 4 || _tokenId == 33) &&
            _besideToken == roverAdapter &&
            roverStakedWithTier(_besideTokenId, droneTier[_tokenId])
        ) {
            droneBuffApplied[_currentOwner][_nonce] = true;
        }

        erc1155Token.safeTransferFrom(_currentOwner, address(this), _tokenId, 1, "");
        emit TokenClaimed(_currentOwner, address(this), _tokenId);
    }

    function onResume(address _currentOwner, uint256 _nonce) external onlyMissionControl {
        seeds[_currentOwner][_nonce] = block.timestamp;
    }

    /// @inheritdoc IMissionControlStakeable
    function checkTimestamp(
        address _userAddress,
        uint256 _nonce
    ) external view override onlyMissionControl returns (uint256 _timestamp) {
        _timestamp = seeds[_userAddress][_nonce];
    }

    /// @inheritdoc IMissionControlStakeable
    function onUnstaked(
        address _newOwner,
        uint256 _nonce,
        uint256 /* _besideNonce */
    ) external override onlyMissionControl onlyStakedTokens(_newOwner, _nonce) {
        droneBuffApplied[_newOwner][_nonce] == false;
        seeds[_newOwner][_nonce] = 0;
        erc1155Token.safeTransferFrom(address(this), _newOwner, tokenIds[_newOwner][_nonce], 1, "");
        emit TokenReturned(_newOwner, address(this), tokenIds[_newOwner][_nonce]);
    }

    /// @inheritdoc IMissionControlStakeable
    function timeLeft(
        address _userAddress,
        uint256 _nonce,
        int256 _x,
        int256 _y
    ) external view override returns (int256 _timeLeft) {
        _timeLeft = __timeLeft(_userAddress, _nonce, _x, _y);
    }

    function __timeLeft(
        address _userAddress,
        uint256 _nonce,
        int256 _x,
        int256 _y
    ) private view returns (int256 _timeLeft) {
        uint256 tokenId = tokenIds[_userAddress][_nonce];
        Distribution class = tokenInfoByRing[tokenId][tileToRing[_x][_y]].class;
        if (class == Distribution.Poisson) {
            return -1;
        }
        if (class == Distribution.LinearDeterministic) {
            return __timeLeft_deterministic_linear(_userAddress, tokenId, _nonce, _x, _y);
        }
        if (class == Distribution.Facility) {
            return __timeLeft_facility(_userAddress, tokenId, _nonce, _x, _y);
        }
        revert("Critical: Distribution class not supported, blame whoever implemented it");
    }

    function __timeLeft_deterministic_linear(
        address _userAddress,
        uint256 tokenId,
        uint256 _nonce,
        int256 _x,
        int256 _y
    ) internal view virtual returns (int256) {
        uint256 localSeed = seeds[_userAddress][_nonce];
        require(localSeed != 0, "MCS1155_ADAPTER: No such token staked");
        uint256 completionTime = (DAY_IN_SEC /
            tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceEV) *
            tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceCap +
            localSeed;
        int256 timeRemaining = int256(completionTime) - int256(block.timestamp);
        return timeRemaining > 0 ? timeRemaining : int256(0);
    }

    function __timeLeft_facility(
        address _userAddress,
        uint256 tokenId,
        uint256 _nonce,
        int256 _x,
        int256 _y
    ) internal view virtual returns (int256) {
        uint256 localSeed = seeds[_userAddress][_nonce];
        require(localSeed != 0, "MCS1155_ADAPTER: No such token staked");

        uint256 output = facilityOutput[_userAddress][_nonce] *
            tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceEV;

        uint256 effectiveLeadTime = (facilityLeadTime[_userAddress][_nonce] *
            leadTimeBonusByRing[tileToRing[_x][_y]]) / STRUCTURE_BONUS_DENOMINATOR;

        uint256 completionTime = (tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceCap *
            effectiveLeadTime) /
            output +
            localSeed;
        int256 timeRemaining = int256(completionTime) - int256(block.timestamp);
        return timeRemaining > 0 ? timeRemaining : int256(0);
    }

    function __handleFacilitySetup(
        uint256 _tokenId,
        address _owner,
        address _underlyingToken,
        uint256 _underlyingTokenId,
        uint256 _nonce
    ) private returns (bool) {
        IPIX.PIXInfo memory info = pix.getInfo(_underlyingTokenId);
        facilityLeadTime[_owner][_nonce] = leadTimes[uint256(info.category)];
        facilityOutput[_owner][_nonce] = prodAmounts[uint256(info.size)];
        return facilityStakeReq[_tokenId][uint256(info.category)] && _underlyingToken == pixAddress;
    }

    function __check_tile(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) private view returns (uint256, uint256) {
        uint256 tokenId = tokenIds[_userAddress][_nonce];
        Distribution class = tokenInfoByRing[tokenId][tileToRing[_x][_y]].class;
        if (class == Distribution.Poisson) {
            return __checkTile_Poisson(_userAddress, tokenId, _nonce, _pausedAt, _x, _y);
        }
        if (class == Distribution.LinearDeterministic) {
            return
                __checkTile_deterministic_linear(_userAddress, tokenId, _nonce, _pausedAt, _x, _y);
        }
        if (class == Distribution.Facility) {
            return __checkTile_facility(_userAddress, tokenId, _nonce, _pausedAt, _x, _y);
        }
        revert("Critical: Distribution class not supported, blame whoever implemented it");
    }

    function __checkTile_facility(
        address _userAddress,
        uint256 tokenId,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) internal view virtual returns (uint256, uint256) {
        uint256 localSeed = seeds[_userAddress][_nonce];
        require(localSeed != 0, "MCS1155_ADAPTER: No such token staked");
        uint256 untilTimestamp = _pausedAt > 0 ? _pausedAt : block.timestamp;

        uint effectiveLeadTime = (facilityLeadTime[_userAddress][_nonce] *
            leadTimeBonusByRing[tileToRing[_x][_y]]) / STRUCTURE_BONUS_DENOMINATOR;

        uint256 ret = tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceEV *
            (facilityOutput[_userAddress][_nonce] *
                ((untilTimestamp - localSeed) / effectiveLeadTime));

        if (ret > tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceCap) {
            ret = tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceCap;
        }

        return (ret, tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceId);
    }

    function __checkTile_deterministic_linear(
        address _userAddress,
        uint256 tokenId,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) internal view virtual returns (uint256, uint256) {
        uint256 localSeed = seeds[_userAddress][_nonce];
        require(localSeed != 0, "MCS1155_ADAPTER: No such token staked");
        uint256 untilTimestamp = _pausedAt > 0 ? _pausedAt : block.timestamp;
        uint256 ret = (tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceEV *
            (untilTimestamp - localSeed)) / DAY_IN_SEC;

        if (ret > tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceCap) {
            ret = tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceCap;
        }

        if (droneBuffApplied[_userAddress][_nonce])
            ret = (ret * DRONE_BUFF) / DRONE_BUFF_DENOMINATOR;

        return (ret, tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceId);
    }

    function __checkTile_Poisson(
        address _player_address,
        uint256 tokenId,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) internal view virtual returns (uint256, uint256) {
        int256 localSeed = int256(seeds[_player_address][_nonce]);
        require(localSeed != 0, "MCS1155_ADAPTER: No such token staked");
        uint256 ret = 0;
        if (tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceEV == 0) {
            return (ret, tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceId);
        }
        uint256 untilTimestamp = _pausedAt > 0 ? _pausedAt : block.timestamp;
        if (
            uint256(int256(untilTimestamp) - localSeed) > cutoffsByRing[tokenId][tileToRing[_x][_y]]
        ) {
            return (
                tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceCap,
                tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceId
            );
        }
        uint256 localEV = tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceEV;
        uint256 localCap = tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceCap;
        int256 T = _getT(_player_address, tokenId, localSeed, localEV);
        while (localSeed + T < int256(untilTimestamp) && ret < localCap) {
            ret++;
            localSeed = localSeed + T;
            T = _getT(_player_address, tokenId, localSeed, localEV);
        }
        return (ret, tokenInfoByRing[tokenId][tileToRing[_x][_y]].resourceId);
    }

    /*
    Poisson distribution implementation
    */

    function _getT(
        address _playerAddress,
        uint256 tokenId,
        int256 seed,
        uint256 ev
    ) internal pure returns (int256 T) {
        require(ev > 0, "PIX: Expected value is zero");
        T =
            (_max_div(int256(DAY_IN_SEC) * MAX_SCALE, int256(ev) * MAX_SCALE) *
                _exponential_2(_playerAddress, tokenId, seed)) /
            (MAX_SCALE * MAX_SCALE);
    }

    function _uniform(
        address _playerAddress,
        uint256 tokenId,
        int256 seed
    ) internal pure returns (uint256 uniform_sample) {
        uniform_sample = uint256(keccak256(abi.encode(_playerAddress, tokenId, seed))) % PROBA_RES;
    }

    function _exponential_2(
        address _playerAddress,
        uint256 tokenId,
        int256 seed
    ) internal pure returns (int256 exponential_sample) {
        exponential_sample = -_max_ln(
            MAX_SCALE -
                _max_div(
                    MAX_SCALE * int256(_uniform(_playerAddress, tokenId, seed)),
                    MAX_SCALE * int256(PROBA_RES)
                )
        );
    }

    function _max_ln(int256 x) internal pure returns (int256 result) {
        result = (_max_log2(x) * MAX_SCALE) / MAX_LOG2_E;
    }

    function _max_log2(int256 x) internal pure returns (int256 result) {
        require(x > 0, "X has to be bigger than zero");
        unchecked {
            // This works because log2(x) = -log2(1/x).
            int256 sign;
            if (x >= MAX_SCALE) {
                sign = 1;
            } else {
                sign = -1;
                // Do the fixed-point inversion inline to save gas. The numerator is SCALE * SCALE.
                x = _max_div(MAX_SCALE, x);
            }

            // Calculate the integer part of the logarithm and add it to the result and finally calculate y = x * 2^(-n).
            uint256 n = PRBMath.mostSignificantBit(uint256(x / MAX_SCALE));

            // The integer part of the logarithm as a signed 59.18-decimal fixed-point number. The operation can't overflow
            // because n is maximum 255, SCALE is 1e18 and sign is either 1 or -1.
            result = int256(n) * MAX_SCALE;

            // This is y = x * 2^(-n).
            int256 y = x >> n;

            // If y = 1, the fractional part is zero.
            if (y == MAX_SCALE) {
                return result * sign;
            }

            // Calculate the fractional part via the iterative approximation.
            // The "delta >>= 1" part is equivalent to "delta /= 2", but shifting bits is faster.
            for (int256 delta = int256(MAX_HALF_SCALE); delta > 0; delta >>= 1) {
                y = (y * y) / MAX_SCALE;

                // Is y^2 > 2 and so in the range [2,4)?
                if (y >= 2 * MAX_SCALE) {
                    // Add the 2^(-m) factor to the logarithm.
                    result += delta;

                    // Corresponds to z/2 on Wikipedia.
                    y >>= 1;
                }
            }
            result *= sign;
        }
    }

    function _max_div(int256 x, int256 y) internal pure returns (int256 result) {
        // Get hold of the absolute values of x and y.
        uint256 ax;
        uint256 ay;
        unchecked {
            ax = x < 0 ? uint256(-x) : uint256(x);
            ay = y < 0 ? uint256(-y) : uint256(y);
        }

        // Compute the absolute value of (x*SCALE)÷y. The result must fit within int256.
        uint256 rAbs = PRBMath.mulDiv(ax, uint256(MAX_SCALE), ay);
        // Get the signs of x and y.
        uint256 sx;
        uint256 sy;
        assembly {
            sx := sgt(x, sub(0, 1))
            sy := sgt(y, sub(0, 1))
        }

        // XOR over sx and sy. This is basically checking whether the inputs have the same sign. If yes, the result
        // should be positive. Otherwise, it should be negative.
        result = sx ^ sy == 1 ? -int256(rAbs) : int256(rAbs);
    }

    function emergencyWithdrawTo(
        address _to,
        address _token,
        uint256 _tokenId,
        uint256 _amount
    ) external onlyOwner {
        IERC1155(_token).safeTransferFrom(address(this), _to, _tokenId, _amount, "");
    }
}
