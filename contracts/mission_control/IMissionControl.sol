// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IMissionControlStakeable.sol";

/// @title Interface defining the mission control
/// @dev This defines only essential functionality and not admin functionality
interface IMissionControl {
    error MC__InvalidCoordinates();
    error MC__NotWhitelisted();
    error MC__InvalidSigner();
    error MC__InvalidStreamer();
    error MC__TileNotRented();
    error MC__TilePaused();
    error MC__NoTileContractStaked();
    error MC__InvalidSuperToken();
    error MC__InvalidFlow();
    error MC__TileRentedWithDifferentSuperToken();
    error MC__SenderNotStreamerNorCrosschain();
    error MC__SenderNotTCLockProvider();
    error MC__TileRented();
    error MC__BaseStaked();
    error MC__InvalidPlacement();
    error MC__DuplicateTech();
    error MC__TileEmpty();
    error MC__TopStaked();
    error MC__ZeroRadius();
    error MC__ZeroAddress();

    /**
     * @notice This is a struct which contains information regarding a tile element
     * @param steakeable Address for the stakeable token on the tile
     * @param tokenId Id of the staked token
     * @param nonce Staking nonce
     * @param staked Boolean defining whether the token is actively staked, used as opposed to clearing the previous two on unstaking.
     */
    struct TileElement {
        address stakeable;
        uint256 tokenId;
        uint256 nonce;
    }

    struct TileRentalInfo {
        bool isRented;
        uint256 pausedAt;
        address rentalToken;
    }

    struct TileRequirements {
        uint256 price;
        uint256 tileContractId;
    }

    /**
     * @notice Struct to hold instructions for tile resource collection
     * @param x X-coordinate of tile
     * @param y Y-coordinate of tile
     * @param z Z-coordinate of tile
     */
    struct CollectOrder {
        int256 x;
        int256 y;
        int256 z;
    }

    /**
     * @notice Struct to hold instructions for placing NFTs on tiles
     * @param x X-coordinate of tile
     * @param y Y-coordinate of tile
     * @param z Z-coordinate of tile
     * @param tokenId Token ID of NFT
     * @param tokenAddress Address of NFT, which is presumed to implement the IMissionControlStakeable interface
     */
    struct PlaceOrder {
        CollectOrder order;
        uint256 tokenId;
        address tokenAddress;
    }

    struct HandleTopPlacementArgs {
        int256 x;
        int256 y;
        uint256 tokenId;
        address stakeable;
        uint256 nonce;
        address staker;
    }

    struct RemoveNFTVars {
        uint256 zeroOrOne;
        uint256 tokenId;
        address stakeable;
    }

    struct TotalTileInfo {
        CollectOrder order;
        bool isRented;
        uint256 flowRate;
        address rentalToken;
        int256 timeLeftBottom;
        int256 timeLeftTop1;
        int256 timeLeftTop2;
        TileElement base;
        TileElement top1;
        TileElement top2;
    }

    struct TotalCheckTileInfo {
        CollectOrder order;
        CheckTileOutputs out;
    }

    struct CheckTileInputs {
        address user;
        int256 x;
        int256 y;
        int256 z;
    }

    struct CheckTileOutputs {
        uint256 bottomAmount;
        uint256 bottomId;
        uint256 topAmount1;
        uint256 topId1;
        uint256 topAmount2;
        uint256 topId2;
    }

    struct HandleRaidCollectInputs {
        address defender;
        int256 x;
        int256 y;
    }

    struct HandleRaidCollectVars {
        TileElement tileElement;
        TileRentalInfo tileRentalInfo;
        IMissionControlStakeable iStakeable;
    }

    struct HandleCollectInputs {
        int256 x;
        int256 y;
        int256 z;
        uint256 position;
    }

    struct HandleCollectVars {
        TileElement tileElement;
        TileRentalInfo tileRentalInfo;
    }

    struct UpdateRentTileVars {
        int96 requiredFlow;
        uint256 index;
        uint256 size;
        uint256 removeTileLength;
        TileRentalInfo tile;
        CollectOrder removeTileItem;
        CollectOrder order;
    }

    struct GetAllCheckTileInfoVars {
        uint256 amount;
        uint256 tokenId;
        uint256 count;
        uint256 totalTiles;
        int256 radius;
    }

    struct CollectFromTilesVars {
        uint256 arrayLen;
        uint256[] ids;
        uint256[] amounts;
        uint256 cId;
        uint256 cAmount;
    }

    /**
     * @notice Stakes multiple NFTs on various tiles in one transaction
     * @param _placeOrders Array of structs containing information regarding where to place what
     * @dev Any downside to using structs as an argument?
     */
    function placeNFTs(PlaceOrder[] calldata _placeOrders) external;

    /**
     * @notice Removes multiple NFTs on various tiles in one transaction
     * @param orders Array of structs containing information regarding where to remove something
     * @dev Token id, address are ignored
     */
    function removeNFTs(
        CollectOrder[] memory orders,
        uint256[] memory positions,
        uint8 _v,
        bytes32 _r,
        bytes32 _s
    ) external;

    /**
     * @notice Queries a tile regarding the resources which are able to collected from it at the current moment
     * @dev Add support for multiple types of tokens, i.e return would be an array?
     */
    function checkTile(CheckTileInputs memory c) external view returns (CheckTileOutputs memory out);

    /**
     * @notice Collects (mints) tokens from tiles
     * @dev An alternative worth considering would be to have an alternative mint function
     * @param _orders Array containing collect orders
     * @dev Any downside to using structs as an argument?
     */
    function collectFromTiles(
        CollectOrder[] calldata _orders,
        uint256[] memory _positions,
        uint8 _v,
        bytes32 _r,
        bytes32 _s
    ) external;

    /**
     * @notice Used to check what a user has staked on a tile
     * @param _user The user whose Mission Control we should check
     * @param _x X-coordinate of tile
     * @param _y Y-coordinate of tile
     * @param _z Z-coordinate of tile
     */
    function checkStakedOnTile(
        address _user,
        int256 _x,
        int256 _y,
        int256 _z
    ) external view returns (TileElement memory _base, TileElement memory _top1, TileElement memory _top2);

    /**
     * @notice Returns when the tile was last updated
     * @param _user The user whose Mission Control we should check
     * @param _x X-coordinate of tile
     * @param _y Y-coordinate of tile
     * @param _z Z-coordinate of tile
     * @return _timestamp The time in blockchain seconds
     */
    function checkLastUpdated(
        address _user,
        int256 _x,
        int256 _y,
        int256 _z,
        uint256 _position
    ) external view returns (uint256 _timestamp);

    /**
     * @notice returns the remaining time until a tile is filled
     * @param _user The user whose Mission Control we should check
     * @param _x X-coordinate of tile
     * @param _y Y-coordinate of tile
     * @param _z Z-coordinate of tile
     */
    function timeLeft(
        address _user,
        int256 _x,
        int256 _y,
        int256 _z
    ) external view returns (int256 _timeLeftBottom, int256 _timeLeftTop1, int256 _timeLeftTop2);

    // user start streaming to the game
    function createRentTiles(
        address supertoken,
        address renter,
        CollectOrder[] memory tiles, // changed to PlaceOrder so that it inclues tokenId and token Address
        int96 flowRate
    ) external;

    // user is streaming and change the rented tiles
    function updateRentTiles(
        address supertoken,
        address renter,
        CollectOrder[] memory addTiles,
        CollectOrder[] memory removeTiles,
        int96 oldFlowRate,
        int96 flowRate
    ) external;

    function toggleTile(address _renter, int256 _x, int256 _y, uint256 rad, bool _shouldPause) external;

    // user stop streaming to the game
    function deleteRentTiles(address supertoken, address renter) external;

    function getAllowedRadius() external view returns (uint256);

    function getTileRentalInfo(address _user, int256 _x, int256 _y) external view returns (TileRentalInfo memory _info);

    function getTileRequirements(int256 _x, int256 _y) external view returns (TileRequirements memory _requirements);
}
