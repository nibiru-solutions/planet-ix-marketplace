// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface ILootChestOpener {

    /**
    * @notice Event emitted when a chest is opened
    * @param _opener The address of the opener
    * @param _contentIds Id's for the content
    * @param _contentAmounts The amounts of each token id, corresponds by index to _contentIds
    */
    event ChestOpened(address _opener, uint[] _contentIds, uint[] _contentAmounts);

    /**
    * @notice Opens loot-chests
    * @param _numChests The number of chests to open
    */
    function openChests(uint _numChests) external;
}
