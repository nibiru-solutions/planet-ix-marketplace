// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@chainlink/contracts/src/v0.8/automation/interfaces/AutomationCompatibleInterface.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./IOrderAutomation.sol";
import "../util/VRFManagerV2.sol";


interface IVRFConsumerBaseV2 {
    function getRandomNumber(uint32 _num_words) external returns (uint256 requestID);
}

contract OrderAutomation is AutomationCompatibleInterface, OwnableUpgradeable, IOrderAutomation {

    address prospecting;
    IVRFConsumerBaseV2 private s_randNumOracle;

    struct BatchOrder {
        uint timestamp;
        bool pending;
        uint[] orderIds;
    }

    mapping(uint => uint) reqIdToBatchId;
    mapping(uint256 => BatchOrder) public queue;
    uint public batchMaxSize;
    uint public interval;
    uint256 public first;
    uint256 public last;

    mapping(uint => uint) orderIdToBatchIndex;
    mapping(uint => uint) batchIndexToRandomNumber;

    address public s_randNumOracleV2;


    // queue functions
    function enqueue(BatchOrder memory data) internal {
        last += 1;
        queue[last] = data;
    }

    function dequeue() internal {
        require(last >= first);
        // non-empty queue

        first += 1;
    }

    function queueLength() public view returns (uint) {
        return 1 + last - first;
    }

    function getFirstBatch() public view returns (BatchOrder memory){
        if (queueLength() < 1) revert("No batch in queue");
        return queue[first];
    }


    function initialize(uint _updateInterval, uint _batchMaxSize, address _prospecting) public initializer {
        __Ownable_init();
        interval = _updateInterval;
        prospecting = _prospecting;
        batchMaxSize = _batchMaxSize;
        first = 1;
        last = 0;
    }

    // Modifiers
    modifier onlyProspecting() {
        if (msg.sender != prospecting) revert("not prospecting");
        _;
    }

    modifier onlyOracle() {
        if (msg.sender != address(s_randNumOracleV2) && msg.sender != owner()) revert("Not oracle");
        _;
    }

    /** @notice Add new order id to correct position in the batch queue.
     *  @param _orderId order id to be added in a vrf batch request.
     */
    function addRequestToBatch(uint _orderId) override external onlyProspecting {
        if (!queue[last].pending && last >= first && queue[last].orderIds.length < batchMaxSize) {
            queue[last].orderIds.push(_orderId);
        }
        else {
            BatchOrder memory batch;
            batch.timestamp = block.timestamp;
            batch.pending = false;
            enqueue(batch);
            queue[last].orderIds.push(_orderId);
        }
        orderIdToBatchIndex[_orderId] = last;
    }

    /** @notice Will be called on by every block to see if performUpkeep tx should be executed.
     *  @param upkeepNeeded return true if tx should be performed.
     */
    function checkUpkeep(
        bytes calldata /* checkData */
    )
    external
    view
    override
    returns (bool upkeepNeeded, bytes memory /* performData */)
    {
        upkeepNeeded = shouldPerformUpkeep();
    }

    /** @notice Check if batch should be processed.
    */
    function shouldPerformUpkeep() public view returns (bool){
        if (!queue[first].pending && last >= first && (block.timestamp - queue[first].timestamp > interval || queue[first].orderIds.length >= batchMaxSize)) {
            return true;
        }
        return false;
    }

    function performUpkeep(bytes calldata) external override {

        if (!shouldPerformUpkeep()) {
            revert("reveal criteria not met");
        }
        if (address(s_randNumOracleV2) == address(0)) {
            revert("Oracle not set");
        }
        queue[first].pending = true;

        uint256 reqId = VRFManagerV2(s_randNumOracleV2).getRandomNumber(1);
        reqIdToBatchId[reqId] = first;
    }

    function manualDequeue(uint256[] memory randomWords)
    external
    onlyOwner
    {
        batchIndexToRandomNumber[first] = randomWords[0];
        dequeue();
    }

    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords)
    external
    onlyOracle
    {
        uint batchIndex = reqIdToBatchId[requestId];
        if (batchIndex == 0) revert("invalid requestId");
        batchIndexToRandomNumber[first] = randomWords[0];
        dequeue();
    }

    function getRandomNumberForOrder(uint _orderId) override external view returns (uint){
        uint randNum = batchIndexToRandomNumber[orderIdToBatchIndex[_orderId]];
        if (randNum == 0) revert("Random Number not fulfilled");
        return uint256(keccak256(abi.encode(randNum, _orderId)));
    }

    // Admin setters
    function setTimeInterval(uint _interval) external onlyOwner {
        interval = _interval;
    }

    function setProspecting(address _prospecting) external onlyOwner {
        prospecting = _prospecting;
    }

    function setOracle(address _oracle) external onlyOwner {
        if (_oracle == address(0)) revert("Can't be zero address");
        s_randNumOracle = IVRFConsumerBaseV2(_oracle);
    }

    function setOracleV2(address _oracle) external onlyOwner {
        if (_oracle == address(0)) revert("Can't be zero address");
        s_randNumOracleV2 = _oracle;
    }

    function setBatchMaxSize(uint _batchMaxSize) external onlyOwner {
        batchMaxSize = _batchMaxSize;
    }

    function getReqIdToBatchId(uint _reqId) external view returns (uint) {
        return reqIdToBatchId[_reqId];
    }

    function getOrderIdToBatchIndex(uint _orderId) external view returns (uint) {
        return orderIdToBatchIndex[_orderId];
    }

    function getBatchIndexToRandomNumber(uint _batchIndex) external view returns (uint) {
        return batchIndexToRandomNumber[_batchIndex];
    }

    function getQueue(uint _index) external view returns (BatchOrder memory) {
        return queue[_index];
    }
}