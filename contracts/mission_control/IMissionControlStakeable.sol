// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

/// @title Interface defining a contract which can be staked upon a "tile" in the mission control
interface IMissionControlStakeable {
    enum Rings {
        Apollo,
        Artemis,
        Chronos,
        Helios
    }

    struct CheckTileInputs {
        uint256 tokenId;
        uint256 localSeed;
        uint256 blockTime;
        uint256 elapsed;
        uint256 ret;
    }

    struct ResourceParams {
        uint tier;
        uint256 resourceEV;
        uint256 resourceId;
        uint256 resourceCap;
        bool isBase;
        bool isRaidable;
        uint256 ring;
    }

    struct ResourceParamOut {
        int256 x;
        int256 y;
        int256 z;
        bool isBase;
        bool isRaidable;
        uint256 resourceEV;
        uint256 resourceId;
        uint256 resourceCap;
    }

    struct RingParams {
        int x;
        int y;
        uint ring;
    }

    struct Request {
        address user;
        uint256 m3tam0dChance;
        uint256 tokenId;
    }
    /**
     * @notice Event emitted when a token is staked
     * @param _userAddress address of the staker
     * @param _stakeable Address to either the IMCStakeable implementation or the possibly the underlying token?
     * @param _tokenId Token id which has been staked
     */
    event TokenClaimed(address _userAddress, address _stakeable, uint256 _tokenId);
    /**
     * @notice Event emitted when a token is unstaked
     * @param _userAddress address of the staker
     * @param _stakeable Address to either the IMCStakeable implementation or the possibly the underlying token?
     * @param _tokenId Token id which has been unstaked
     */
    event TokenReturned(address _userAddress, address _stakeable, uint256 _tokenId);

    /**
     * @notice Event emitted when the mission control is set
     * @param missionControl The address of the mission control
     */
    event MissionControlSet(address missionControl);

    /**
     * @notice Resets the seed of a staked token. A cheaper alternative to onCollect when you do not need the number of harvested tokens
     * @param _userAddress The players address
     * @param _nonce Tile-staking nonce
     */
    function reset(address _userAddress, uint256 _nonce) external;

    /**
     * @notice Function to check the number of tokens ready to be harvested from this tile
     * @param _userAddress The players address
     * @param _nonce Tile-staking nonce
     * @return _amount Number of tokens that can be collected
     * @return _retTokenId The tokenId of the tokens that can be collected
     */
    function checkTile(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external view returns (uint256 _amount, uint256 _retTokenId);

    /**
     * @notice Used to fetch new seed time upon collection some resources
     * @param _userAddress The players address
     * @param _nonce Tile-staking nonce
     * @return _amount Number of tokens that can be collected
     * @return _retTokenId The tokenId of the tokens that can be collected
     * @dev It is vital for this function to update the rng
     */
    function onCollect(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external returns (uint256 _amount, uint256 _retTokenId);

    /**
     * @notice Used to see if the token can be used as the base of a tile, or if it is meant to be staked upon another token
     * @param _tokenId The id of the token
     * @return _isBase Whether this token is a base or not
     */
    function isBase(uint256 _tokenId, int256 _x, int256 _y) external view returns (bool _isBase);

    function isRaidable(uint256 _tokenId, int256 _x, int256 _y) external view returns (bool _isRaidable);

    /**
     * @notice transfers ownership of a token from the player to the stakeable contract
     * @param _currentOwner Address of the current owner
     * @param _tokenId The id of the token
     * @param _nonce Tile-staking nonce
     * @param _underlyingToken todo
     * @param _underlyingTokenId todo
     */
    function onStaked(
        address _currentOwner,
        uint256 _tokenId,
        uint256 _nonce,
        address _underlyingToken,
        uint256 _underlyingTokenId,
        address _besideToken,
        uint256 _besideTokenId,
        uint256 _besideNonce,
        int256 _x,
        int256 _y
    ) external;

    function onResume(address _renter, uint256 _nonce) external;

    /**
     * @notice transfers ownership of a token from the stakeable contract back to the player
     * @param _newOwner Address of the soon-to-be owner
     * @param _nonce Tile-staking nonce
     */
    function onUnstaked(address _newOwner, uint256 _nonce, uint256 _besideNonce) external;

    function checkTimestamp(address _userAddress, uint256 _nonce) external view returns (uint256 _timestamp);

    /**
     * @notice returns the remaining time until a tile is filled
     * @param _userAddress The players address
     * @param _nonce Tile-staking nonce
     * @return _timeLeft The time left, -1 if the time can not be predicted
     */
    // note: removed tokenId as a param. was unused
    function timeLeft(
        address _userAddress,
        uint256 _nonce,
        int256 _x,
        int256 _y
    ) external view returns (int256 _timeLeft);
}
