// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/ITrader.sol";

/// @title The Prospector allows the user to "prospect" their waste
interface IProspector is ITrader {
    /**
     * @notice Places orders for "prospecting"
     * @param _numOrders How many orders to place simultaneously
     */
    function placeProspectingOrders(uint256 _numOrders) external;

    /**
     * @notice Used to fetch the price of prospecting
     * @return _wastePrice The amount of waste one has to pay to prospect
     */
    function getProspectingPrice() external view returns (uint256 _wastePrice);

    /**
     * @notice Used to fetch weights for each token.
     * @return _weights base amount. Can be used to calculate probability for each token.
     */
    function getProspectingWeights() external view returns (uint256[] memory _weights);

    function getProspectingMaxOrders() external view returns (uint256 _maxOrders);
}
