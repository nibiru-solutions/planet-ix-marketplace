// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "./IAssetManager.sol";
import "./IAssetsConverter.sol";


contract AssetsConverter is IAssetsConverter, OwnableUpgradeable, ReentrancyGuardUpgradeable {
    //Storage
    mapping(uint => mapping(address => uint)) userMintSupply; //recipeId -> user -> amount of mints.
    mapping(uint => uint) recipeIdToConversions;
    mapping(address => bool) public admins;
    mapping(uint => Recipe) public recipes;
    IAssetManager public assetManager;
    uint public nonce;

    function initialize() public initializer {
        __Ownable_init();
        __ReentrancyGuard_init();
        setAdmin(msg.sender, true);
    }

    //Admin functionality
    /** @notice Only admins can use this function.
     */
    modifier onlyAdmins {
        if (!admins[msg.sender]) revert AssetsConverter__NotAdmin(msg.sender);
        _;
    }

    /** @notice Owner can set and remove admins.
     *  @param _admin address of admin.
     *  @param _shouldBeAdmin true to add admin false to remove admin.
     */
    function setAdmin(address _admin, bool _shouldBeAdmin) public onlyOwner {
        admins[_admin] = _shouldBeAdmin;
    }

    /** @notice change the implementation address for iAssetManager.
     *  @param _assetManager implementation address
     */
    function setAssetManager(address _assetManager) external onlyAdmins {
        assetManager = IAssetManager(_assetManager);
    }

    /** @notice delete an active recipe.
     *  @param _recipeId recipe to be deleted.
     */
    function deleteAssetConversionRecipe(uint _recipeId) external onlyAdmins {
        Recipe memory recipe;
        recipes[_recipeId] = recipe;
    }

    /** @notice Pause and unpause recipe from use.
     *  @param _recipeId id of recipe to be paused/unpause.
     *  @param _paused true for paused, false to unpause.
     */
    function setRecipePausedState(uint _recipeId, bool _paused) external onlyAdmins {
        recipes[_recipeId].paused = _paused;
    }

    /** @notice creates new asset conversion recipes.
     *  @param _fromToken token that will be burned for _toToken.
     *  @param _toToken tokens that will be minted to user on conversion.
     *  @param _prices cost of x amount of _fromToken
     */
    function createAssetConversionRecipe(uint[] memory _fromToken, uint[] memory _toToken, uint[] memory _prices, uint _maxConversionsPerWallet, uint _maxTotalConversions) override external onlyAdmins {
        if (_fromToken.length != _prices.length) revert AssetsConverter__InvalidRecipes();
        for (uint i; i < _prices.length; i++) {
            if (_prices[i] < 1) revert AssetsConverter__InvalidRecipes();
        }
        nonce++;
        Recipe memory recipe = Recipe({
        fromToken : _fromToken,
        toToken : _toToken,
        prices : _prices,
        id : nonce,
        maxConversionsPerWallet : _maxConversionsPerWallet,
        maxTotalConversions : _maxTotalConversions,
        paused : false
        });
        recipes[nonce] = recipe;
        emit AssetsConverter__NewRecipe(_fromToken, _toToken, _prices, nonce, _maxConversionsPerWallet, _maxTotalConversions);
    }

    //User functionality
    /** @notice Convert assets with specified recipe id.
     *  @param _recipeId recipe to used.
     *  @param _amount desired amount of uses of recipe.
     */
    function convertAsset(uint _recipeId, uint _amount) override nonReentrant external {
        Recipe memory recipe = recipes[_recipeId];
        if (_amount < 1) revert AssetsConverter__InvalidAmount();
        if (recipe.prices.length < 1) revert AssetsConverter__InvalidRecipeId();
        if (recipe.paused) revert AssetsConverter__RecipePaused();

        if (recipe.maxConversionsPerWallet != 0) {
            uint userPrevConversions = userMintSupply[_recipeId][msg.sender];
            if (userPrevConversions + _amount > recipe.maxConversionsPerWallet) revert AssetsConverter__MaximumConversionsExceeded();
            userMintSupply[_recipeId][msg.sender] += _amount;
        }
        if (recipe.maxTotalConversions != 0) {
            if (_amount + recipeIdToConversions[_recipeId] > recipe.maxTotalConversions) revert AssetsConverter__TotalConversionSupplyExceeded();
            recipeIdToConversions[_recipeId] += _amount;
        }

        uint fromTokenLength = recipe.fromToken.length;
        uint[] memory tempPrices = new uint[](fromTokenLength);
        for (uint i; i < fromTokenLength; i++) {
            tempPrices[i] = (recipe.prices[i] * _amount);
        }
        assetManager.trustedBatchBurn(msg.sender, recipe.fromToken, tempPrices);
        for (uint i; i < recipe.toToken.length; i++) {
            assetManager.trustedMint(msg.sender, recipe.toToken[i], _amount);
        }
        emit AssetsConverter__ConversionComplete(msg.sender, _recipeId, _amount);
    }

    //View
    /** @notice get tokens and prices for recipe.
     *  @param _recipeId recipe to get info for.
     */
    function getRecipeFormula(uint _recipeId) view external returns (uint[] memory fromToken, uint[] memory toToken, uint[] memory prices) {
        fromToken = recipes[_recipeId].fromToken;
        toToken = recipes[_recipeId].toToken;
        prices = recipes[_recipeId].prices;
    }
}
