// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

interface IAssetsConverter {

    event AssetsConverter__NewRecipe(uint[] fromToken, uint[] toToken, uint[] prices, uint id, uint maxConversionsPerWallet, uint maxTotalConversions);
    event AssetsConverter__ConversionComplete(address user, uint recipeId, uint amount);

    error AssetsConverter__NotAdmin(address);
    error AssetsConverter__InvalidRecipes();
    error AssetsConverter__InvalidAmount();
    error AssetsConverter__InvalidRecipeId();
    error AssetsConverter__MaximumConversionsExceeded();
    error AssetsConverter__TotalConversionSupplyExceeded();
    error AssetsConverter__RecipePaused();

    struct Recipe {
        uint[] fromToken;
        uint[] toToken;
        uint[] prices;
        uint id;
        uint maxConversionsPerWallet;
        uint maxTotalConversions;
        bool paused;
    }

    function convertAsset(uint _recipeId, uint _amount) external;

    function createAssetConversionRecipe(uint[] memory _fromToken, uint[] memory _toToken, uint[] memory _prices, uint _maxConversionsPerWallet, uint _maxTotalConversions) external;

}
