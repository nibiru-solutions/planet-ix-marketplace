// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";

import "./ILootChestOpener.sol";
import "./IAssetManager.sol";
import "../util/VRFManagerV2.sol";

/// @title Implementation of ILootChestOpener
contract LootChestOpener is ILootChestOpener, OwnableUpgradeable, IRNGConsumer {
    uint256 rngNonceA;
    uint256 rngNonceB;

    IAssetManager public pixAssets;
    uint256 public chestTokenId;

    uint256[] rewardAssetIds;
    uint256[] rewardAssetAmounts;
    /*
        Truncated normal dist. to approximate N(20,8)
        AC amounts
        [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]
        AC weights
        [2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2]
        AC weights if AC total weight = 500 i.e a 100x of all weights
        [15.0, 18.0, 20.0, 23.0, 25.0, 27.0, 29.0, 30.0, 31.0, 32.0, 32.0, 31.0, 30.0, 29.0, 27.0, 25.0, 23.0, 20.0, 18.0, 15.0]

    */
    uint256[] assetWeights;
    uint256 totalAssetWeight;
    uint256[] contentAmounts;
    uint256[] contentWeights;
    uint256 totalContentWeight;
    address private vrfCoordinator;
    mapping(uint256 => address) requests;

    error OnlyCoordinatorCanFulfill(address have, address want);

    modifier onlyOracle() {
        if (msg.sender != vrfCoordinator) revert OnlyCoordinatorCanFulfill(msg.sender, vrfCoordinator);
        _;
    }

    /**
     * @notice Initializer for this contract
     * @param _pixAssets Address of the IAssetManager
     * @param _rewardAssetIds Asset ids for crate contents
     * @param _rewardAssetAmounts Amounts, corresponding by index to _rewardAssetIds
     * @param _assetWeights Weights, corresponding by index to _rewardAssetIds
     * @param _contentAmounts Content amounts i.e the amount of items in a chest
     * @param _contentWeights Content weights, corresponding by index to _contentAmounts
     */
    function initialize(
        address _pixAssets,
        uint256[] calldata _rewardAssetIds,
        uint256[] calldata _rewardAssetAmounts,
        uint256[] calldata _assetWeights,
        uint256[] calldata _contentAmounts,
        uint256[] calldata _contentWeights
    ) public initializer {
        __Ownable_init();
        pixAssets = IAssetManager(_pixAssets);

        rewardAssetIds = _rewardAssetIds;
        rewardAssetAmounts = _rewardAssetAmounts;
        assetWeights = _assetWeights;
        totalAssetWeight = _sumArray(_assetWeights);

        contentAmounts = _contentAmounts;
        contentWeights = _contentWeights;
        totalContentWeight = _sumArray(_contentWeights);
    }

    /**
     * @notice Updates the chest probability distribution
     * @param _rewardAssetIds Asset ids for crate contents
     * @param _rewardAssetAmounts Amounts, corresponding by index to _rewardAssetIds
     * @param _assetWeights Weights, corresponding by index to _rewardAssetIds
     * @param _contentAmounts Content amounts i.e the amount of items in a chest
     * @param _contentWeights Content weights, corresponding by index to _contentAmounts
     */
    function updateProbabilities(
        uint256[] calldata _rewardAssetIds,
        uint256[] calldata _rewardAssetAmounts,
        uint256[] calldata _assetWeights,
        uint256[] calldata _contentAmounts,
        uint256[] calldata _contentWeights
    ) external onlyOwner {
        rewardAssetIds = _rewardAssetIds;
        rewardAssetAmounts = _rewardAssetAmounts;
        assetWeights = _assetWeights;
        totalAssetWeight = _sumArray(_assetWeights);
        contentAmounts = _contentAmounts;
        contentWeights = _contentWeights;
        totalContentWeight = _sumArray(_contentWeights);
    }

    /// @inheritdoc ILootChestOpener
    function openChests(uint256 _numChests) external {
        pixAssets.trustedBurn(msg.sender, uint256(IAssetManager.AssetIds.LootCrate), _numChests);
        for (uint256 i = 0; i < _numChests; i++) {
            // Explicit reference :(
            uint256 reqId = VRFManagerV2(vrfCoordinator).getRandomNumber(1);
            requests[reqId] = msg.sender;
        }
    }

    /// @notice Handles the business logic of opening 1 chest
    function _rollChest(address _recipient, uint256[] memory randomWords) internal {
        uint256 content_amt = _drawContentAmt(randomWords[0]);
        uint256[] memory treasuresToMint = new uint256[](content_amt);
        uint256[] memory amountList = new uint256[](content_amt);
        for (uint256 i; i < content_amt; i++) {
            (treasuresToMint[i], amountList[i]) = _drawTreasure(randomWords[i + 1]);
        }
        pixAssets.trustedBatchMint(_recipient, treasuresToMint, amountList);
        emit ChestOpened(_recipient, treasuresToMint, amountList);
    }

    /**
     * @notice Draws how many items are contained in a chest
     * @return _amount The number of items
     */
    function _drawContentAmt(uint256 _randomWord) internal view returns (uint256 _amount) {
        uint256 uniformSample = _randomWord % totalContentWeight;
        uint256 sum;
        for (uint256 i; i < contentWeights.length; i++) {
            sum += contentWeights[i];
            if (uniformSample <= sum) {
                return contentAmounts[i];
            }
        }
        revert("This should never happen");
    }

    /**
     * @notice Draws a singular treasure
     * @return _assetId Token id of the treasure
     * @return _assetAmount The amount of the treasure
     */
    function _drawTreasure(uint256 _randomWord) internal view returns (uint256 _assetId, uint256 _assetAmount) {
        uint256 uniformSample = _randomWord % totalAssetWeight;
        uint256 sum;
        for (uint256 i; i < assetWeights.length; i++) {
            sum += assetWeights[i];
            if (uniformSample <= sum) {
                return (rewardAssetIds[i], rewardAssetAmounts[i]);
            }
        }
        revert("This should never happen");
    }

    // Maths

    /**
     * @notice Sums an uint array
     * @param _array The array
     * @return _sum The sum of its elements
     */
    function _sumArray(uint256[] calldata _array) internal pure returns (uint256 _sum) {
        for (uint256 i; i < _array.length; i++) {
            _sum += _array[i];
        }
    }

    function setVRF(address _vrf) external onlyOwner {
        vrfCoordinator = _vrf;
    }

    /**
     * @notice fulfillRandomness handles the VRF response.
     * @param requestId The Id initially returned by requestRandomness
     * @param randomWords the VRF output expanded to the requested number of words
     */
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external virtual onlyOracle {
        uint256[] memory expandedValues = new uint256[](6);
        for (uint256 i = 0; i < 6; i++) {
            expandedValues[i] = uint256(keccak256(abi.encode(randomWords[0], i)));
        }
        _rollChest(requests[requestId], expandedValues);
        delete requests[requestId];
    }
}