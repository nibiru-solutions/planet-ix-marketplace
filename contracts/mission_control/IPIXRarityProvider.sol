pragma solidity ^0.8.0;

interface IPIXRarityProvider {
    // Deprecated interface do not use
    function getPIXRarity(uint _tokenId) external returns (uint);
}
