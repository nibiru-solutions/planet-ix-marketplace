// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "./IProspector.sol";
import "./IAssetManager.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../util/Burnable.sol";
import "./IBaseLevel.sol";
import "./IOrderAutomation.sol";
import "./IMCCrosschainServices.sol";

interface IVRFConsumerBaseV2 {
    function getRandomNumber(uint32 _num_words) external returns (uint256 requestID);
}

error Prospector__InvalidOrderId(uint orderId);
error Prospector__MaxOrdersExceeded(uint256 amount);
error Prospector__InvalidId(uint256 id);
error Prospector__NoOrders(address player);
error Prospector__NoClaimableOrders(address player);
error Prospector__ZeroAddress();
error Prospector__NonmatchingArrays();
error Prospector__InvalidArray();
error Prospecting__OrderNotYetCompleted(address player, uint256 id);
error Prospector__IndexOutOfBounds(uint256 index);
error Prospector__InvalidSpeedupAmount();
error Prospector__NotOracle();
error Prospector__NoSpeedUpAvailable(uint orderId);
error Prospector__NotOrderAutomation();
error Prospector__RandNumNotFulfilled();
error Prospector__IsPaused();

contract ProspectingV2 is OwnableUpgradeable, ReentrancyGuardUpgradeable, IProspector {
    struct ProspectingOrder {
        uint256 effectiveStartTime;
    }

    IAssetManager private s_assetManager;
    IVRFConsumerBaseV2 private s_randNumOracle;
    address private s_moderator;
    address private s_feeWallet;

    uint256 public s_prospectorFee;
    uint256 public s_prospectingTime;
    uint256 public s_prospectorMaxOrders; //disabled
    uint256 public s_speedupTime;
    uint256 public s_speedupCost;
    uint256 public s_prospectorTax;

    IAssetManager.AssetIds[] public s_biomodTypes;
    uint256[] public s_biomodWeights;
    uint256 public s_totalBiomodWeights;
    uint256 public s_randNonce;

    mapping(address => Order[]) s_prospectingOrders;
    mapping(uint256 => address) s_requestIds;

    uint256 constant WASTE_ID = uint256(IAssetManager.AssetIds.Waste);
    uint256 constant ASTRO_CREDIT_ID = uint256(IAssetManager.AssetIds.AstroCredit);
    uint256 public nextOrderId;

    address public pixt;
    address beneficiary;
    uint public pixtSpeedupSplitBps;
    uint public pixtSpeedupCost;

    mapping(uint => uint) s_reqIdToOrder;
    mapping(uint => uint) s_randomResult;

    address public baseLevelAddress;
    address public orderAutomation;

    bool public isPaused;

    IMCCrosschainServices private crosschainServices;
    uint256[] public s_boostedBiomodWeights;
    uint256 public s_totalBoostedBiomodWeights;
    /*mapping(uint => uint) orderIdToBatchIndex;

    function setBatchIndex(uint _orderId, uint _batchIndex) onlyOrderAutomation {
        orderIdToBatchIndex[_orderId] = _batchIndex;
    }*/

    event ProspectingOrderSpedUp(address indexed player, uint256 amount, uint orderId);
    event ProspectingOrderPlaced(address indexed player, uint256 amount);
    event ProspectingOrderCompleted(address indexed player, uint256 id);
    event RandomBiomodMinted(address indexed player, uint256 tokenId, uint256 orderId);

    modifier onlyOracle() {
        if (msg.sender != address(s_randNumOracle)) revert Prospector__NotOracle();
        _;
    }
    modifier onlyOrderAutomation() {
        if (msg.sender != orderAutomation) revert Prospector__NotOrderAutomation();
        _;
    }

    modifier whenNotPaused() {
        if (isPaused == true) revert Prospector__IsPaused();
        _;
    }

    function initialize(
        address _feeWallet,
        uint256 _prospectorFee,
        uint256 _prospectingTime,
        uint256 _prospectingMaxOrders,
        uint256 _speedupTime,
        uint256 _speedupCost,
        uint256 _prospectingTax
    ) public initializer {
        __Ownable_init();
        __ReentrancyGuard_init();
        s_moderator = msg.sender;
        s_feeWallet = _feeWallet;
        s_prospectorFee = _prospectorFee;
        s_prospectingTime = _prospectingTime;
        s_prospectorMaxOrders = _prospectingMaxOrders;
        s_speedupTime = _speedupTime;
        s_speedupCost = _speedupCost;
        s_prospectorTax = _prospectingTax;
    }

    /// @inheritdoc IProspector
    function placeProspectingOrders(uint256 _numOrders) external nonReentrant whenNotPaused {
        Order[] memory orders = s_prospectingOrders[msg.sender];
        if (orders.length + _numOrders > IBaseLevel(baseLevelAddress).getOrderCapacity(address(this), msg.sender))
            revert Prospector__MaxOrdersExceeded(orders.length + _numOrders);

        for (uint256 i; i < _numOrders; ) {
            uint256 _orderId = nextOrderId;
            nextOrderId++;
            s_prospectingOrders[msg.sender].push(
                Order({
                    orderId: _orderId,
                    orderAmount: 1,
                    createdAt: block.timestamp,
                    speedUpDeductedAmount: 0,
                    totalCompletionTime: s_prospectingTime
                })
            );
            _burnAssets();
            IOrderAutomation(orderAutomation).addRequestToBatch(_orderId);
            unchecked {
                ++i;
            }
        }
        emit ProspectingOrderPlaced(msg.sender, _numOrders);
    }

    /// @inheritdoc ITrader
    function claimOrder(uint _orderId) external override nonReentrant {
        Order[] storage orders = s_prospectingOrders[msg.sender];
        if (orders.length == 0) revert Prospector__NoOrders(msg.sender);
        for (uint256 i = 0; i < orders.length; i++) {
            if (orders[i].orderId == _orderId) {
                if (isFinished(orders[i])) {
                    _mintBiomod(_orderId);
                    _removeOrder(i);
                    return;
                } else revert Prospecting__OrderNotYetCompleted(msg.sender, _orderId);
            }
        }
        revert Prospector__InvalidId(_orderId);
    }

    /// @inheritdoc ITrader
    function claimBatchOrder() external override nonReentrant {
        Order[] memory orders = s_prospectingOrders[msg.sender];
        if (orders.length <= 0) revert Prospector__NoOrders(msg.sender);
        for(uint i=orders.length; i > 0; i--) {
            if ((orders[i-1].createdAt + orders[i-1].totalCompletionTime) < block.timestamp) {
                _mintBiomod(orders[i-1].orderId);
                _removeOrder(i-1);
            }
        }
    }

    function isFinished(Order memory order) internal view returns (bool _isFinished) {
        _isFinished = (order.createdAt + order.totalCompletionTime) < block.timestamp;
    }

    /// @inheritdoc ITrader
    function speedUpOrder(uint _numSpeedUps, uint _orderId) external override {
        Order[] storage orders = s_prospectingOrders[msg.sender];
        if (orders.length == 0) revert Prospector__NoOrders(msg.sender);
        if (_numSpeedUps == 0) revert Prospector__InvalidSpeedupAmount();

        for (uint256 orderIndex; orderIndex < orders.length; orderIndex++) {
            if (orders[orderIndex].orderId == _orderId) {
                if (isFinished(orders[orderIndex])) revert Prospector__NoSpeedUpAvailable(_orderId);

                orders[orderIndex].speedUpDeductedAmount += s_speedupTime * _numSpeedUps;
                orders[orderIndex].totalCompletionTime = int(s_prospectingTime) -
                    int(orders[orderIndex].speedUpDeductedAmount) >
                    0
                    ? s_prospectingTime - orders[orderIndex].speedUpDeductedAmount
                    : 0;

                s_assetManager.trustedBurn(
                    msg.sender,
                    ASTRO_CREDIT_ID,
                    s_speedupCost * _numSpeedUps
                );
                emit ProspectingOrderSpedUp(msg.sender, _numSpeedUps, _orderId);
                return;
            }
        }
        revert Prospector__InvalidOrderId(_orderId);
    }

    function IXTSpeedUpOrder(uint _numSpeedUps, uint _orderId) external override {
        // add in interface and add override.
        require(pixt != address(0), "IXT address not set");
        Order[] storage orders = s_prospectingOrders[msg.sender];
        if (orders.length == 0) revert Prospector__NoOrders(msg.sender);
        if (_numSpeedUps == 0) revert Prospector__InvalidSpeedupAmount();

        for (uint256 orderIndex; orderIndex < orders.length; orderIndex++) {
            if (orders[orderIndex].orderId == _orderId) {
                if (isFinished(orders[orderIndex])) revert Prospector__NoSpeedUpAvailable(_orderId);

                orders[orderIndex].speedUpDeductedAmount += s_speedupTime * _numSpeedUps;
                orders[orderIndex].totalCompletionTime = int(s_prospectingTime) -
                    int(orders[orderIndex].speedUpDeductedAmount) >
                    0
                    ? s_prospectingTime - orders[orderIndex].speedUpDeductedAmount
                    : 0;

                require(
                    IERC20(pixt).transferFrom(
                        msg.sender,
                        address(this),
                        pixtSpeedupCost * _numSpeedUps
                    ),
                    "Transfer of funds failed"
                ); // transfer 100% to this contract.
                Burnable(pixt).burn((pixtSpeedupCost * _numSpeedUps * pixtSpeedupSplitBps) / 10000);
                emit ProspectingOrderSpedUp(msg.sender, _numSpeedUps, _orderId);
                return;
            }
        }
        revert Prospector__InvalidOrderId(_orderId);
    }

    function speedupAllAC(uint _numSpeedUps) external {
        Order[] storage orders = s_prospectingOrders[msg.sender];
        if (orders.length == 0) revert Prospector__NoOrders(msg.sender);
        if (_numSpeedUps == 0) revert Prospector__InvalidSpeedupAmount();

        uint speedupsApplied;
        for (uint256 orderIndex; orderIndex < orders.length; orderIndex++) {
            if (isFinished(orders[orderIndex])) continue;

            orders[orderIndex].speedUpDeductedAmount += s_speedupTime * _numSpeedUps;
            orders[orderIndex].totalCompletionTime = int(s_prospectingTime) -
                int(orders[orderIndex].speedUpDeductedAmount) >
                0
                ? s_prospectingTime - orders[orderIndex].speedUpDeductedAmount
                : 0;

            unchecked {
                ++speedupsApplied;
            }
            emit ProspectingOrderSpedUp(msg.sender, _numSpeedUps, orders[orderIndex].orderId);
        }
        s_assetManager.trustedBurn(
            msg.sender,
            ASTRO_CREDIT_ID,
            s_speedupCost * _numSpeedUps * speedupsApplied
        );
    }

    function speedupAllIXT(uint _numSpeedUps) external {
        Order[] storage orders = s_prospectingOrders[msg.sender];
        if (orders.length == 0) revert Prospector__NoOrders(msg.sender);
        if (_numSpeedUps == 0) revert Prospector__InvalidSpeedupAmount();

        uint speedupsApplied;
        for (uint256 orderIndex; orderIndex < orders.length; orderIndex++) {
            if (isFinished(orders[orderIndex])) continue;

            orders[orderIndex].speedUpDeductedAmount += s_speedupTime * _numSpeedUps;
            orders[orderIndex].totalCompletionTime = int(s_prospectingTime) -
                int(orders[orderIndex].speedUpDeductedAmount) >
                0
                ? s_prospectingTime - orders[orderIndex].speedUpDeductedAmount
                : 0;

            unchecked {
                ++speedupsApplied;
            }
            emit ProspectingOrderSpedUp(msg.sender, _numSpeedUps, orders[orderIndex].orderId);
        }
        require(
            IERC20(pixt).transferFrom(
                msg.sender,
                address(this),
                pixtSpeedupCost * _numSpeedUps * speedupsApplied
            ),
            "Transfer of funds failed"
        ); // transfer 100% to this contract.
        Burnable(pixt).burn(
            (pixtSpeedupCost * _numSpeedUps * pixtSpeedupSplitBps * speedupsApplied) / 10000
        );
    }

    function setIXTSpeedUpParams(
        address _pixt,
        address _beneficiary,
        uint _pixtSpeedupSplitBps,
        uint _pixtSpeedupCost
    ) external override onlyOwner {
        pixt = _pixt;
        beneficiary = _beneficiary;
        pixtSpeedupSplitBps = _pixtSpeedupSplitBps;
        pixtSpeedupCost = _pixtSpeedupCost;
    }

    function _burnAssets() internal {
        uint256 taxPayable = (s_prospectorFee * s_prospectorTax) / 10000;

        s_assetManager.trustedBurn(msg.sender, WASTE_ID, s_prospectorFee);
        s_assetManager.trustedMint(s_feeWallet, WASTE_ID, taxPayable);
    }

    function _mintBiomod(uint _orderId) internal {
        if (s_biomodTypes.length == 0 || s_biomodWeights.length == 0)
            revert Prospector__InvalidArray();
        uint random;
        if(s_randomResult[_orderId] != 0) {
            random = s_randomResult[_orderId];
        }
        else{
            random = IOrderAutomation(orderAutomation).getRandomNumberForOrder(_orderId) ;
        }
        address player = msg.sender;
        uint256 weight;
        IAssetManager.AssetIds resultantBiomod;

        if (crosschainServices.getEternaLabsGenesisBalance(msg.sender) > 0) {
            for (uint256 i; i < s_boostedBiomodWeights.length; i++) {
                weight += s_boostedBiomodWeights[i];
                if (random % s_totalBoostedBiomodWeights <= weight) {
                    resultantBiomod = s_biomodTypes[i];
                    break;
                }
            }
        }
        else {
            for (uint256 i; i < s_biomodWeights.length; i++) {
                weight += s_biomodWeights[i];
                if (random % s_totalBiomodWeights <= weight) {
                    resultantBiomod = s_biomodTypes[i];
                    break;
                }
            }
        }
        ///@dev Using Asset ID 0 to mean no mint
        if (uint256(resultantBiomod) > 0) {
            s_assetManager.trustedMint(player, uint256(resultantBiomod), 1);
            emit RandomBiomodMinted(player, uint256(resultantBiomod), _orderId);
        } else {
            emit RandomBiomodMinted(player, 0, _orderId);
        }
    }

    function _removeOrder(uint _index) internal {
        if (_index < s_prospectingOrders[msg.sender].length -1)
            s_prospectingOrders[msg.sender][_index] = s_prospectingOrders[msg.sender][s_prospectingOrders[msg.sender].length-1];
        s_prospectingOrders[msg.sender].pop();
    }

    function setBiomodWeights(
        IAssetManager.AssetIds[] calldata _biomodTypes,
        uint256[] calldata _biomodWeights
    ) external onlyOwner {
        if (_biomodTypes.length == 0 || _biomodWeights.length == 0)
            revert Prospector__InvalidArray();
        if (_biomodTypes.length != _biomodWeights.length) revert Prospector__NonmatchingArrays();
        s_biomodTypes = _biomodTypes;
        s_biomodWeights = _biomodWeights;

        uint256 sum;
        for (uint256 i; i < _biomodWeights.length; i++) {
            sum += _biomodWeights[i];
        }
        s_totalBiomodWeights = sum;
    }

    function setBoostedBiomodWeights(
        uint256[] calldata _boostedBiomodWeights
    ) external onlyOwner {
        if (_boostedBiomodWeights.length != 7)
            revert Prospector__InvalidArray();
        s_boostedBiomodWeights = _boostedBiomodWeights;

        uint256 sum;
        for (uint256 i; i < _boostedBiomodWeights.length; i++) {
            sum += _boostedBiomodWeights[i];
        }
        s_totalBoostedBiomodWeights = sum;
    }

    /** @notice change Moderator
     *  @param _moderator new moderator
     */
    function setModerator(address _moderator) external onlyOwner {
        if (_moderator == address(0)) revert Prospector__ZeroAddress();
        s_moderator = _moderator;
    }

    function setOracle(address _oracle) external onlyOwner {
        if (_oracle == address(0)) revert Prospector__ZeroAddress();
        s_randNumOracle = IVRFConsumerBaseV2(_oracle);
    }

    function setNextOrderId(uint256 _orderId) external onlyOwner {
        nextOrderId = _orderId;
    }

    function setPaused(bool _isPaused) external onlyOwner {
        isPaused = _isPaused;
    }

    function setOrderAutomation(address _orderAutomation) external onlyOwner {
        orderAutomation = _orderAutomation;
    }

    /** @notice retreive Moderator
     */
    function getModerator() public view returns (address) {
        return s_moderator;
    }

    function setProspectingPrice(uint256 price) external onlyOwner {
        s_prospectorFee = price;
    }

    /// @inheritdoc IProspector
    function getProspectingPrice() external view returns (uint256 _prospectorFee) {
        _prospectorFee = s_prospectorFee;
    }

    /// @inheritdoc IProspector
    function getProspectingWeights() external view returns (uint256[] memory _weights) {
        _weights = s_biomodWeights;
    }

    /// @inheritdoc IProspector
    function getProspectingMaxOrders() external view returns (uint256 _maxOrders) {
        _maxOrders = s_prospectorMaxOrders;
    }

    /** @notice change Fee Wallet Address
     *  @param _feeWallet new fee wallet address
     */

    function setFeeWallet(address _feeWallet) external onlyOwner {
        if (_feeWallet == address(0)) revert Prospector__ZeroAddress();
        s_feeWallet = _feeWallet;
    }

    /** @notice retreive Fee Wallet Address
     */
    function getFeeWallet() public view returns (address) {
        return s_feeWallet;
    }

    /** @notice change duration of time to prospect
     *  @param _prospectingTime converting time duration
     */
    function setProspectingTime(uint256 _prospectingTime) external onlyOwner {
        s_prospectingTime = _prospectingTime;
    }

    /** @notice retreive duration of time prospect
     */
    function getProspectingTime() public view returns (uint256) {
        return s_prospectingTime;
    }

    /** @notice change amount of Astro Credits to speed up
        prospecting
     *  @param _speedupCost Astro Credits amount
     */
    function setSpeedupCost(uint256 _speedupCost) external onlyOwner {
        s_speedupCost = _speedupCost;
    }

    /** @notice retreive amount of Astro Credits to speed up
        prospecting
     */
    function getSpeedupCost() public view returns (uint256) {
        return s_speedupCost;
    }

    /** @notice change the reduction time to prospect
     *  @param _speedupTime reduction time amount
     */
    function setSpeedupTime(uint256 _speedupTime) external onlyOwner {
        s_speedupTime = _speedupTime;
    }

    /** @notice retreive the reduction time to prospect
     */
    function getSpeedupTime() public view returns (uint256) {
        return s_speedupTime;
    }

    /** @notice change basis points for the fee
     *  @param _tax fee represented as basis points e.g. 500 == 5 pct
     */
    function setProspectorTax(uint16 _tax) external onlyOwner {
        s_prospectorTax = _tax;
    }

    /** @notice retreive basis points for the fee
     */
    function getProspectorTax() public view returns (uint256) {
        return s_prospectorTax;
    }

    /** @notice change the implementation address for the iAssetManager
     *  @param _iAssetManager implementation address
     */
    function setIAssetManager(address _iAssetManager) external onlyOwner {
        s_assetManager = IAssetManager(_iAssetManager);
    }

    function setBaseLevelAddress(address _baseLevelAddress) external override onlyOwner {
        baseLevelAddress = _baseLevelAddress;
    }

    function setCrosschainServices(IMCCrosschainServices _crosschainServices) external onlyOwner {
        if (address(_crosschainServices) == address(0)) revert Prospector__ZeroAddress();
        crosschainServices = _crosschainServices;
    }

    /** @notice returns the iAssetManager
     */
    function getIAssetManager() public view returns (IAssetManager) {
        return s_assetManager;
    }

    /// @inheritdoc ITrader
    function getOrders(address _player) external view returns (Order[] memory) {
        return s_prospectingOrders[_player];
    }
}