// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/ITrader.sol";

interface IFacilityStore is ITrader {
    /**
    * @notice Struct containing weights for single order.
    * @param totalFacilityProbabilityWeights weights depending on biomods placed for order.
    */
    struct NewFacilityOrder {
        uint256[] totalFacilityProbabilityWeights;
    }

    /**
    * @notice Place an order for a facility
    * @param _additionalTokensAmounts Amounts of additional Bio-mods to be expended of each type
    */
    function placeFacilityOrder(uint[] calldata _additionalTokensAmounts) external;

    /**
    * @notice Used to fetch the part of facility price paid in tokens which are not biomods
    * @return _tokenIds Ids of tokens to be paid
    * @return _tokenAmounts Amounts of tokens to be paid
    */
    function getFacilityFixedTokensPrice() external view returns (uint[] memory _tokenIds, uint[] memory _tokenAmounts);

    /**
    * @notice Used to fetch the facility price paid in biomod tokens
    * @return _biomodTokenIds Ids of tokens to be paid
    * @return _biomodTokenAmounts Amounts of tokens to be paid
    */
    function getFacilityBiomodTokensPrice() external view returns (uint[] memory _biomodTokenIds, uint[] memory _biomodTokenAmounts);

    function getMaxOrders() external view returns(uint256 _maxOrders);

}