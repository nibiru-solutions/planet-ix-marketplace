pragma solidity ^0.8.0;

import "abdk-libraries-solidity/ABDKMathQuad.sol";
import "hardhat/console.sol";
import "./IMissionControlStakeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";


contract PoissonResourceGenerator is OwnableUpgradeable {

    mapping(uint => uint) seeds;
    // Needing this mapping is an unfortunate side effect of the specs
    mapping(uint => uint) remainingWaste;
    uint wasteEV = 12;
    uint wasteCap = 3;
    uint constant DAY_IN_SEC = 86400;

    function _uniform(uint tokenId, uint seed, address sender) pure public returns (uint uniform_sample){
        uniform_sample = uint(keccak256(abi.encode(tokenId, seed, sender))) % 10000;
    }

    function _exponential(uint tokenId, uint seed, address sender) pure public returns (bytes16 exponential_sample){
        exponential_sample = ABDKMathQuad.neg(
            ABDKMathQuad.ln(
                ABDKMathQuad.sub(
                    ABDKMathQuad.fromInt(1),
                    ABDKMathQuad.div(
                        ABDKMathQuad.fromUInt(
                            _uniform(tokenId, seed, sender)),
                        ABDKMathQuad.fromInt(10000)
                    )
                )
            )
        );
    }

    function getT(uint tokenId, uint seed, address sender) view public returns (uint T){
        bytes16 sample = _exponential(tokenId, seed, sender);
        T = ABDKMathQuad.toUInt(
            ABDKMathQuad.mul(
                ABDKMathQuad.div(ABDKMathQuad.fromUInt(DAY_IN_SEC), ABDKMathQuad.fromUInt(wasteEV)),
                sample)
        );
    }

    function init_token(uint tokenId) external {
        seeds[tokenId] = block.timestamp;
        console.log("init seed", seeds[tokenId]);
    }

    function checkTile(uint tokenId) public view returns (uint, uint){
        uint ret = remainingWaste[tokenId];
        uint localSeed = seeds[tokenId];
        uint blockTime = block.timestamp;
        uint T = getT(tokenId, localSeed, msg.sender);
        console.log("check tile called:", localSeed, blockTime, T);
        while (localSeed + T < blockTime && ret < wasteCap) {
            ret++;
            localSeed = localSeed + T;
            T = getT(tokenId, localSeed, msg.sender);
            console.log("Check tile loop, ret, T, seed: ", ret, T, localSeed);
        }
        console.log("returning: ", ret);
        return (ret, 1);
    }

    function onCollect(uint tokenId, uint numCollected) external {
        (uint check,) = checkTile(tokenId);
        remainingWaste[tokenId] = check - numCollected;
        seeds[tokenId] = block.timestamp;
    }


}
