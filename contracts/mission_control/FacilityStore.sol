// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./IFacilityStore.sol";
import "./IAssetManager.sol";
import "../util/VRFManagerV2.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../util/Burnable.sol";
import "./IBaseLevel.sol";

/* Errors */
error OnlyCoordinatorCanFulfill(address have, address want);
error OnlyGovCanCall();
error AdditionalBiomodsWrongLength(uint256 have, uint256 want);
error MaxOpenOrdersReached();
error MsgSenderIsNotOrderOwner(address have, address want);
error OrderIsNotFinished();
error OrderClaimIsAlreadyRequested();
error BiomodTokenIdsAndPriceLengthsAreDifferent();
error BiomodTokensAndFacilityTokensLengthsAreDifferent();
error FacilityTokenIdsAndWeightsLengthsAreDifferent();
error NoOrders(address player);
error NoClaimableOrders(address player);
error InvalidSpeedupAmount();
error NoPossibleSpeedUps();
error InvalidOrderId(uint orderId);
error OrderNotClaimed();

struct InitializeParams {
    address _assetManager;
    address _feeWallet;
    uint256 _facilityTime;
    uint256 _speedupPriceTokenId;
    uint256 _speedupPrice;
    uint256 _speedupTime;
    uint256 _maxOrders;
    uint256[] _fixedPriceTokenIds;
    uint256[] _fixedPriceTokenAmounts;
    uint256[] _biomodTokenIds;
    uint256[] _biomodPriceAmounts;
    uint256[] _facilityTokenIds;
    uint256[] _facilityProbabilityWeights;
    address _vrfCoordinator;
}

contract FacilityStore is IFacilityStore, OwnableUpgradeable, IRNGConsumer {
    /* Type Declarations */
    uint256 public nextOrderId;
    mapping(address => Order[]) s_facilityOrders;
    mapping(uint256 => NewFacilityOrder) s_facilityOrderWeights;
    mapping(uint256 => address) s_requestIds;
    mapping(uint256 => uint256) requestToOrderId;
    mapping(address => uint256[]) userOrders; // delete this when deploying new

    IAssetManager public assetManager;

    uint256 public facilityTime;
    uint256 public speedupPriceTokenId;
    uint256 public speedupPrice;
    uint256 public speedupTime;

    uint256 public maxOrders; //disabled

    address public feeWallet;
    uint256 public feePercentage;  // denominator is 10000
    uint256 public feeTokenId;     // same as fixedPriceTokenIds[0]
    uint256 public feeTokenAmount; // calculated based on feePercentage and fixedPriceTokenAmounts[0]

    uint256[] public fixedPriceTokenIds;
    uint256[] public fixedPriceTokenAmounts;

    uint256[] public biomodTokenIds;
    uint256[] public biomodPriceAmounts;
    uint256[] public facilityTokenIds;
    uint256[] public facilityProbabilityWeights;

    address public vrfCoordinator;
    mapping(uint256 => uint256) public vrfRequests; // map requestId -> facility orderId

    address public moderator;

    address public pixt;
    address beneficiary;
    uint public pixtSpeedupSplitBps;
    uint public pixtSpeedupCost;
    address public baseLevelAddress;

    /* Events */
    event FacilityOrderPlaced(address indexed user, uint256 indexed orderId);
    event SpeedUpConstruction(address indexed user, uint256 indexed orderId, uint256 _numSpeedups);
    event RequestClaimFacility(address indexed user, uint256 indexed orderId);
    event ReceiveFacility(address indexed user, uint256 orderId, uint256 facilityTokenId);

    /* Modifiers */
    modifier onlyGov() {
        if (msg.sender != owner() || msg.sender != moderator) revert OnlyGovCanCall();
        _;
    }

    modifier onlyOracle() {
        if (msg.sender != vrfCoordinator) revert OnlyCoordinatorCanFulfill(msg.sender, vrfCoordinator);
        _;
    }

    function initialize(
        InitializeParams memory _initParams
    ) public initializer {
        __Ownable_init();

        if (_initParams._biomodTokenIds.length != _initParams._biomodPriceAmounts.length) {
            revert BiomodTokenIdsAndPriceLengthsAreDifferent();
        }
        if (_initParams._facilityTokenIds.length != _initParams._facilityProbabilityWeights.length) {
            revert FacilityTokenIdsAndWeightsLengthsAreDifferent();
        }
        if (_initParams._biomodTokenIds.length != _initParams._facilityTokenIds.length) {
            revert BiomodTokensAndFacilityTokensLengthsAreDifferent();
        }

        assetManager = IAssetManager(_initParams._assetManager);
        feeWallet = _initParams._feeWallet;
        facilityTime = _initParams._facilityTime;
        speedupPriceTokenId = _initParams._speedupPriceTokenId;
        speedupPrice = _initParams._speedupPrice;
        speedupTime = _initParams._speedupTime;
        maxOrders = _initParams._maxOrders;
        fixedPriceTokenIds = _initParams._fixedPriceTokenIds;
        fixedPriceTokenAmounts = _initParams._fixedPriceTokenAmounts;
        biomodTokenIds = _initParams._biomodTokenIds;
        biomodPriceAmounts = _initParams._biomodPriceAmounts;
        facilityTokenIds = _initParams._facilityTokenIds;
        facilityProbabilityWeights = _initParams._facilityProbabilityWeights;
        vrfCoordinator = _initParams._vrfCoordinator;

        feeTokenId = fixedPriceTokenIds[0];
        feeTokenAmount = _calculateFee(feePercentage, fixedPriceTokenAmounts[0]);

        moderator = msg.sender;
    }

    function _createNewOrder(address _user) internal returns(uint _orderId) {
        uint256 orderId = nextOrderId;
        nextOrderId++;

        s_facilityOrders[_user].push(Order({
            orderId: orderId,
            orderAmount: 1,
            createdAt: block.timestamp,
            speedUpDeductedAmount: 0,
            totalCompletionTime: facilityTime
        }));
        return orderId;
    }

    /// @inheritdoc IFacilityStore
    function placeFacilityOrder(
        uint256[] calldata _additionalBiomodAmounts
    ) override external {
        if (_additionalBiomodAmounts.length != biomodTokenIds.length) {
            revert AdditionalBiomodsWrongLength(_additionalBiomodAmounts.length, biomodTokenIds.length);
        }
        if (userOpenOrdersAmount(msg.sender) >= IBaseLevel(baseLevelAddress).getOrderCapacity(address(this), msg.sender)) {
            revert MaxOpenOrdersReached();
        }
        uint orderId = _createNewOrder(msg.sender);
        uint256[] memory totalBiomodAmounts = biomodPriceAmounts;

        for(uint256 i=0; i<biomodTokenIds.length; i++) {
            totalBiomodAmounts[i] += _additionalBiomodAmounts[i];
            s_facilityOrderWeights[orderId].totalFacilityProbabilityWeights.push(facilityProbabilityWeights[i] * (1 + _additionalBiomodAmounts[i]));
        }

        assetManager.trustedBatchBurn(msg.sender, fixedPriceTokenIds, fixedPriceTokenAmounts);
        assetManager.trustedBatchBurn(msg.sender, biomodTokenIds, totalBiomodAmounts);
        // mint fee to feeWallet
        assetManager.trustedMint(feeWallet, feeTokenId, feeTokenAmount);
        emit FacilityOrderPlaced(msg.sender, orderId);
    }

    /// @inheritdoc ITrader
    function speedUpOrder(uint _numSpeedUps, uint _orderId) external override {
        Order[] storage orders = s_facilityOrders[msg.sender];
        if (orders.length == 0) revert NoOrders(msg.sender);
        if (_numSpeedUps == 0) revert InvalidSpeedupAmount();
        for (uint256 orderIndex; orderIndex < orders.length; orderIndex++) {
            if (orders[orderIndex].orderId == _orderId) {
                if (isFinished(orders[orderIndex])) revert NoPossibleSpeedUps();
                orders[orderIndex].speedUpDeductedAmount += speedupTime * _numSpeedUps;
                orders[orderIndex].totalCompletionTime = int(facilityTime) - int(orders[orderIndex].speedUpDeductedAmount) > 0 ? facilityTime - orders[orderIndex].speedUpDeductedAmount : 0;

                emit SpeedUpConstruction(msg.sender, orders[orderIndex].orderId, _numSpeedUps);
                assetManager.trustedBurn(msg.sender, speedupPriceTokenId, _numSpeedUps * speedupPrice);
                return;
            }
        }
        revert InvalidOrderId(_orderId);
    }

    function IXTSpeedUpOrder(uint _numSpeedUps, uint _orderId) external override{
        require(pixt != address (0), "IXT address not set");
        Order[] storage orders = s_facilityOrders[msg.sender];
        if (orders.length == 0) revert NoOrders(msg.sender);
        if (_numSpeedUps == 0) revert InvalidSpeedupAmount();
        for (uint256 orderIndex; orderIndex < orders.length; orderIndex++) {
            if (orders[orderIndex].orderId == _orderId) {
                if (isFinished(orders[orderIndex])) revert NoPossibleSpeedUps();
                orders[orderIndex].speedUpDeductedAmount += speedupTime * _numSpeedUps;
                orders[orderIndex].totalCompletionTime = int(facilityTime) - int(orders[orderIndex].speedUpDeductedAmount) > 0 ? facilityTime - orders[orderIndex].speedUpDeductedAmount : 0;

                require(IERC20(pixt).transferFrom(msg.sender, address(this), pixtSpeedupCost * _numSpeedUps), "Transfer of funds failed");
                Burnable(pixt).burn((pixtSpeedupCost * _numSpeedUps * pixtSpeedupSplitBps) / 10000);
                emit SpeedUpConstruction(msg.sender, orders[orderIndex].orderId, _numSpeedUps);
                return;
            }
        }
        revert InvalidOrderId(_orderId);
    }

    function setIXTSpeedUpParams(address _pixt, address _beneficiary, uint _pixtSpeedupSplitBps, uint _pixtSpeedupCost) external override onlyOwner{
        pixt = _pixt;
        beneficiary = _beneficiary;
        pixtSpeedupSplitBps = _pixtSpeedupSplitBps;
        pixtSpeedupCost = _pixtSpeedupCost;
    }

    function _requestOrder(uint _orderId) internal {
        uint reqId = VRFManagerV2(vrfCoordinator).getRandomNumber(1);
        s_requestIds[reqId] = msg.sender;
        requestToOrderId[reqId] = _orderId;
    }

    function userOpenOrdersAmount(address _user) view public returns(uint256) {
        uint256 openOrdersCount = 0;
        Order[] storage orders = s_facilityOrders[_user];
        for(uint256 i=0; i<orders.length; i++) {
            if (block.timestamp > orders[i].createdAt + orders[i].totalCompletionTime) {
                openOrdersCount++;
            }
        }
        return openOrdersCount;
    }

    /// @inheritdoc ITrader
    function claimOrder(uint _orderId) external override {
        Order[] storage orders = s_facilityOrders[msg.sender];
        if (orders.length == 0) revert NoOrders(msg.sender);
        for(uint256 i=0; i < orders.length; i++) {
            if (orders[i].orderId == _orderId){
                if ((orders[i].createdAt + orders[i].totalCompletionTime) < block.timestamp) {
                    _requestOrder(_orderId);
                    _removeOrder(i);
                    emit RequestClaimFacility(msg.sender, _orderId);
                    return;
                }
                else revert OrderIsNotFinished();
            }
        }
        revert InvalidOrderId(_orderId);
    }

    /// @inheritdoc ITrader
    function claimBatchOrder() external override {
        Order[] storage orders = s_facilityOrders[msg.sender];
        if (orders.length == 0) revert NoOrders(msg.sender);
        for(uint i=orders.length; i > 0; i--) {
            if ((orders[i-1].createdAt + orders[i-1].totalCompletionTime) < block.timestamp) {
                _requestOrder(orders[i-1].orderId);
                emit RequestClaimFacility(msg.sender, orders[i-1].orderId);
                _removeOrder(i-1);
            }
        }
    }

    function _removeOrder(uint _index) internal {
        if (_index < s_facilityOrders[msg.sender].length -1)
            s_facilityOrders[msg.sender][_index] = s_facilityOrders[msg.sender][s_facilityOrders[msg.sender].length-1];
        s_facilityOrders[msg.sender].pop();
    }

    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external virtual onlyOracle {
        address player = s_requestIds[requestId];
        uint orderId = requestToOrderId[requestId];
        delete requestToOrderId[requestId];
        delete s_requestIds[requestId];

        uint256 facilityIndex = _draw(randomWords[0], s_facilityOrderWeights[orderId].totalFacilityProbabilityWeights);

        assetManager.trustedMint(player, facilityTokenIds[facilityIndex], 1);
        emit ReceiveFacility(player,orderId, facilityTokenIds[facilityIndex]);
    }

    /* Check if order is finished and claimable */
    function isFinished(Order memory order) internal view returns(bool _isFinished){
        _isFinished = (order.createdAt + order.totalCompletionTime) < block.timestamp;
    }

    /* Math functions */
    function _calculateFee(uint256 bpsFee, uint256 amount) internal pure returns (uint256) {
        return (amount * bpsFee) / 10000;
    }

    function _draw(uint256 _sample, uint256[] storage _weights) view internal returns (uint256 resultIndex) {
        uint uniformSample = _sample % _sumArray(_weights);
        uint256 sum = 0;

        for(uint256 i=0; i<_weights.length; i++) {
            sum += _weights[i];
            if (uniformSample < sum) {
                return i;
            }
        }
    }

    function _sumArray(uint256[] storage _array) internal view returns (uint256 _sum){
        for (uint256 i; i < _array.length; i++) {
            _sum += _array[i];
        }
    }

    /* Get functions */
    /// @inheritdoc IFacilityStore
    function getFacilityFixedTokensPrice() override external view returns (uint[] memory _tokenIds, uint[] memory _tokenAmounts) {
        return(fixedPriceTokenIds, fixedPriceTokenAmounts);
    }
    /// @inheritdoc IFacilityStore
    function getFacilityBiomodTokensPrice() override external view returns (uint[] memory _tokenIds, uint[] memory _tokenAmounts) {
        return(biomodTokenIds, biomodPriceAmounts);
    }

    /// @inheritdoc ITrader
    function getOrders(address _player) external view returns (Order[] memory){
        return s_facilityOrders[_player];
    }

    /* Set functions */
    function setAssetManager(address _assetManager) external onlyGov() {
        assetManager = IAssetManager(_assetManager);
    }

    function setFacilityTime(uint256 _facilityTime) external onlyGov() {
        facilityTime = _facilityTime;
    }

    function setSpeedupParameters(
        uint256 _speedupPriceTokenId,
        uint256 _speedupPrice,
        uint256 _speedupTime
    ) external onlyGov() {
        speedupPriceTokenId = _speedupPriceTokenId;
        speedupPrice = _speedupPrice;
        speedupTime = _speedupTime;
    }

    function setMaxOrders(uint256 _maxOrders) external onlyGov() {
        maxOrders = _maxOrders;
    }

    /// @inheritdoc IFacilityStore
    function getMaxOrders() external view override returns(uint256 _maxOrders){
        _maxOrders = maxOrders;
    }

    function setFeeWallet(address _feeWallet) external onlyGov() {
        feeWallet = _feeWallet;
    }

    /**
    * @dev feeTokenAmount will be recalculated based on new fee percentage
    */
    function setFeePercentage(uint256 _feePercentage) external onlyGov() {
        feePercentage = _feePercentage;
        feeTokenAmount = _calculateFee(feePercentage, fixedPriceTokenAmounts[0]);
    }

    /**
    * @dev first parameter is used always as fee token, feeTokenId and feeTokenAmount will be recalculated
    */
    function setFixedPriceTokenIds(
        uint256[] memory _fixedPriceTokenIds,
        uint256[] memory _fixedPriceTokenAmounts
    ) external onlyGov() {
        fixedPriceTokenIds = _fixedPriceTokenIds;
        fixedPriceTokenAmounts = _fixedPriceTokenAmounts;

        feeTokenId = fixedPriceTokenIds[0];
        feeTokenAmount = _calculateFee(feePercentage, fixedPriceTokenAmounts[0]);
    }

    function setBiomodTokenPrices(
        uint256[] memory _biomodTokenIds,
        uint256[] memory _biomodPriceAmounts
    ) external onlyGov() {
        if (_biomodTokenIds.length != _biomodPriceAmounts.length) {
            revert BiomodTokenIdsAndPriceLengthsAreDifferent();
        }

        biomodTokenIds = _biomodTokenIds;
        biomodPriceAmounts = _biomodPriceAmounts;
    }

    function setFacilityTokenIdsAndWeights(
        uint256[] memory _facilityTokenIds,
        uint256[] memory _facilityProbabilityWeights
    ) external onlyGov() {

        if (_facilityTokenIds.length != _facilityProbabilityWeights.length) {
            revert FacilityTokenIdsAndWeightsLengthsAreDifferent();
        }
        if (_facilityTokenIds.length != biomodTokenIds.length) {
            revert BiomodTokensAndFacilityTokensLengthsAreDifferent();
        }

        facilityTokenIds = _facilityTokenIds;
        facilityProbabilityWeights = _facilityProbabilityWeights;
    }

    function setVrfCoordinator(address _vrfCoordinator) external onlyGov() {
        vrfCoordinator = _vrfCoordinator;
    }

    function setModerator(address _moderator) external onlyGov() {
        moderator = _moderator;
    }

    function setBaseLevelAddress(address _baseLevelAddress) external override onlyOwner {
        baseLevelAddress = _baseLevelAddress;
    }

}