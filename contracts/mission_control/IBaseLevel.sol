// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;


interface IBaseLevel {

    /**
    * @notice Set super app address.
    * @param _superAppAddress address of MissionControlStream.
    */
    function setSuperAppAddress(address _superAppAddress) external;

    /**
    * @notice Set sf flow per month per level.
    * @param _superFluidPerLevel flow per level.
    */
    function setSuperFluidPerLevel(uint _superFluidPerLevel) external;

    /**
    * @notice Set numbers of orders for a ITrader depending users level.
    * @param _trader address of ITrader.
    * @param _fromLevel from this level to _toLevel will be set to _additionalOrders.
    * @param _toLevel from this level to _fromLevel  will be set to _additionalOrders.
    * @param _additionalOrders number of orders a trader will have for input level range.
    */
    function setOrderCapacity(address _trader, uint _fromLevel, uint _toLevel, uint _additionalOrders) external;

    /**
    * @notice Set address for super token and super token lite.
    * @param _superToken address for super token.
    * @param _superTokenLite address for super token lite.
    */
    function setSuperTokens(address _superToken, address _superTokenLite) external;

    /**
    * @notice Get number of orders for a user for a ITrader.
    * @param _trader address of ITrader.
    * @param _user user to look up capacity for.
    */
    function getOrderCapacity(address _trader, address _user) external view returns(uint _extraOrders);

    /**
    * @notice get base level for user.
     * @param _user to get base level for.
     * @return _level returns level of user.
     */
    function getBaseLevel(address _user) external view returns (uint _level);
}