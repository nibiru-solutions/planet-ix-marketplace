// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;
import "./ILockProvider.sol";
import "./IMissionControl.sol";
import "./IMissionControlStaking.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165Upgradeable.sol";

contract TileContractLockProvider is ILockProvider, OwnableUpgradeable, ERC165Upgradeable {
    struct Coords {
        int256 x;
        int256 y;
        int256 z;
    }

    IMissionControlStaking public missionControlStaking;
    IMissionControl public missionControl;

    mapping(uint256 => Coords[]) idToRing;

    modifier onlyMCStaking() {
        require(msg.sender == address(missionControlStaking), "LOCK_PROVIDER: MC STAKING ONLY");
        _;
    }

    function initialize() external initializer {
        __Ownable_init();
    }

    function onTokenLocked(
        address _user,
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _amount,
        uint256 _relayerFee
    ) external payable onlyMCStaking {
        Coords[] memory ring = idToRing[_tokenId];
        for (uint256 i; i < ring.length; i++) {
            uint256 rad = (abs(ring[i].x) + abs(ring[i].y) + abs(ring[i].z)) / 2;
            missionControl.toggleTile(_user, ring[i].x, ring[i].y, rad, false);
        }
    }

    function onTokenUnlocked(
        address _user,
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _amount,
        uint256 _relayerFee
    ) external payable onlyMCStaking {
        Coords[] memory ring = idToRing[_tokenId];
        if (_amount == 0) {
            for (uint256 i; i < ring.length; i++) {
                uint256 rad = (abs(ring[i].x) + abs(ring[i].y) + abs(ring[i].z)) / 2;
                missionControl.toggleTile(_user, ring[i].x, ring[i].y, rad, true);
            }
        }
    }

    function setMCStaking(address _mcStaking) external onlyOwner {
        require(_mcStaking != address(0), "LOCK_PROVIDER: ADDRESS ZERO");
        missionControlStaking = IMissionControlStaking(_mcStaking);
    }

    function setMissionControl(address _missionControl) external onlyOwner {
        require(_missionControl != address(0), "LOCK_PROVIDER: ADDRESS ZERO");
        missionControl = IMissionControl(_missionControl);
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC165Upgradeable) returns (bool) {
        return interfaceId == type(ILockProvider).interfaceId || super.supportsInterface(interfaceId);
    }

    function addCoords(uint256 id, Coords[] memory coords) external onlyOwner {
        for (uint256 i; i < coords.length; i++) {
            idToRing[id].push(coords[i]);
        }
    }

    function abs(int256 _value) private pure returns (uint256) {
        return _value < 0 ? uint256(-_value) : uint256(_value);
    }
}
