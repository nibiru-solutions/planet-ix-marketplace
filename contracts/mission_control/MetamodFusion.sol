// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../util/Burnable.sol";
import "./IMetamodFusion.sol";
import "./IAssetManager.sol";

// todo add IMetamodFusion
contract MetamodFusion is OwnableUpgradeable, ReentrancyGuardUpgradeable, IMetamodFusion {

    uint256 constant METAMOD_ID = 0;
    uint256 constant ASTRO_CREDIT_ID = uint256(IAssetManager.AssetIds.AstroCredit);

    mapping(uint256 => Order) public fusionOrders;
    mapping(address => uint256) public currentFusionOrder;
    uint256 public nextOrderId;

    uint256 public fusionOrderTime;

    address public pixt;
    uint256 public speedupTime;
    uint256 public acSpeedupCost;
    uint256 public pixtSpeedupCost;
    uint256 public pixtSpeedupSplitBps;
    uint256 public constant pixtSpeedupSplitBpsDenominator = 100_00;

    uint256[] public costTokenIds;
    uint256[] public costTokenAmounts;
    uint256 public metamodFusionAmount;

    IAssetManager public assetManager;

    function initialize(
      address _assetManager,
      address _pixt
    ) public initializer {
      __Ownable_init();

      assetManager = IAssetManager(_assetManager);
      pixt = _pixt;

      nextOrderId = 1;

      metamodFusionAmount = 1;

      fusionOrderTime = 5 days;

      speedupTime = 1 hours;
      acSpeedupCost = 100;
      pixtSpeedupCost = 0.5 ether;
      pixtSpeedupSplitBps = 50_00;
    }

    function setBaseLevelAddress(address _baseLevelAddress) external override onlyOwner {
      // @dev base level is not needed but it's in ITrader interaface
    }

    function setAssetManagerAddress(address _assetManagerAddress) external onlyOwner {
       assetManager = IAssetManager(_assetManagerAddress);
    }

    function setIXTSpeedUpParams(
        address _pixt,
        address /* _beneficiary */,
        uint _pixtSpeedupSplitBps,
        uint _pixtSpeedupCost
    ) external override onlyOwner {
      pixt = _pixt;
      pixtSpeedupSplitBps = _pixtSpeedupSplitBps;
      pixtSpeedupCost = _pixtSpeedupCost;
    }

    function setSpeedUpParams(uint256 _speedupTime, uint256 _acSpeedupCost) external onlyOwner {
      speedupTime = _speedupTime;
      acSpeedupCost = _acSpeedupCost;
    }

    function setFusionCost(uint256[] calldata _costTokenIds, uint256[] calldata _costTokenAmounts) external onlyOwner {
      costTokenIds = _costTokenIds;
      costTokenAmounts = _costTokenAmounts;
    }

    function setMetamodFusionAmount(uint256 _metamodFusionAmount) external onlyOwner {
      metamodFusionAmount = _metamodFusionAmount;
    }

    function placeFusionOrder() external {
      if (currentFusionOrder[msg.sender] != 0) revert MetamodFusion__MaxOrdersReached();

      assetManager.trustedBatchBurn(msg.sender, costTokenIds, costTokenAmounts);

      fusionOrders[nextOrderId] =
        Order({
            orderId: nextOrderId,
            orderAmount: 1,
            createdAt: block.timestamp,
            speedUpDeductedAmount: 0,
            totalCompletionTime: fusionOrderTime
        });

      currentFusionOrder[msg.sender] = nextOrderId;

      emit PlaceMetamodFusionOrder(msg.sender, nextOrderId);

      nextOrderId++;
    }

    function getOrders(address _player) external view override returns (Order[] memory) {
        if (currentFusionOrder[_player] == 0) {
            return new Order[](0);
        } else {
            Order[] memory order = new Order[](1);
            order[0] = fusionOrders[currentFusionOrder[_player]];
            return order;
        }
    }

    function _speedupOrder(Order storage order, uint256 _numSpeedUps) internal {
      if (_isFinished(order)) revert MetamodFusion__NoSpeedUpAvailable();

      order.speedUpDeductedAmount += speedupTime * _numSpeedUps;
      order.totalCompletionTime = 
          fusionOrderTime > order.speedUpDeductedAmount ? fusionOrderTime - order.speedUpDeductedAmount : 0;
    }

    function speedUpOrder(uint _numSpeedUps, uint _orderId) external override {
      if (currentFusionOrder[msg.sender] != _orderId || _orderId == 0) revert MetamodFusion__InvalidOrder(_orderId);

      uint256 totalCost = acSpeedupCost * _numSpeedUps;
      assetManager.trustedBurn(msg.sender, ASTRO_CREDIT_ID, totalCost);

      _speedupOrder(fusionOrders[currentFusionOrder[msg.sender]], _numSpeedUps);

      emit SpeedUpOrder(msg.sender, _orderId, _numSpeedUps);
    }

    function IXTSpeedUpOrder(uint _numSpeedUps, uint _orderId) external override {
      if (currentFusionOrder[msg.sender] != _orderId || _orderId == 0) revert MetamodFusion__InvalidOrder(_orderId);

      uint256 totalCost = pixtSpeedupCost * _numSpeedUps;
      require(
          IERC20(pixt).transferFrom(msg.sender, address(this), totalCost),
          "Transfer of funds failed"
      );
      Burnable(pixt).burn(totalCost * pixtSpeedupSplitBps / pixtSpeedupSplitBpsDenominator);

      _speedupOrder(fusionOrders[currentFusionOrder[msg.sender]], _numSpeedUps);

      emit SpeedUpOrderIXT(msg.sender, _orderId, _numSpeedUps);
    }

    function claimOrder(uint _orderId) public override {
      if (currentFusionOrder[msg.sender] != _orderId || _orderId == 0) revert MetamodFusion__InvalidOrder(_orderId);
      Order storage order = fusionOrders[currentFusionOrder[msg.sender]];
      
      if (!_isFinished(order)) revert MetamodFusion__OrderNotFinished();

      currentFusionOrder[msg.sender] = 0;

      assetManager.trustedMint(msg.sender, METAMOD_ID, metamodFusionAmount);
      
      emit ClaimOrder(msg.sender, _orderId);
    }

    function claimBatchOrder() external override {
      if (currentFusionOrder[msg.sender] == 0) revert MetamodFusion__NoOrders();
      
      claimOrder(currentFusionOrder[msg.sender]);
    }

    function _isFinished(Order storage order) internal view returns (bool) {
        return (order.createdAt + order.totalCompletionTime) < block.timestamp;
    }
    
}