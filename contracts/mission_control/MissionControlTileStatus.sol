// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IMissionControl.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract MissionControlTileStatus is OwnableUpgradeable {
    IMissionControl public missionControl;

    function initialize() external initializer {
        __Ownable_init();
    }

    function setMC(IMissionControl _mc) external onlyOwner {
        require(address(_mc) != address(0));
        missionControl = _mc;
    }

    function getAllTileInfo(address _renter)
        external
        view
        returns (IMissionControl.TotalTileInfo[] memory _totalTileInfo)
    {
        uint256 _count;
        uint256 _totalTiles;
        uint256 allowedRadius = missionControl.getAllowedRadius();
        int256 radius = int256(allowedRadius);
        for (uint256 i = 1; i <= allowedRadius; i++) _totalTiles += i * 6;
        _totalTileInfo = new IMissionControl.TotalTileInfo[](_totalTiles);
        for (int256 x = -radius; x <= radius; x++) {
            for (int256 y = -radius; y <= radius; y++) {
                for (int256 z = -radius; z <= radius; z++) {
                    int256 sum = x + y + z;
                    uint256 rad = (abs(x) + abs(y) + abs(z)) / 2;
                    if (rad <= allowedRadius && rad > 0 && sum == 0) {
                        (
                            _totalTileInfo[_count].base,
                            _totalTileInfo[_count].top1,
                            _totalTileInfo[_count].top2
                        ) = missionControl.checkStakedOnTile(_renter, x, y, z);
                        (
                            _totalTileInfo[_count].timeLeftBottom,
                            _totalTileInfo[_count].timeLeftTop1,
                            _totalTileInfo[_count].timeLeftTop2
                        ) = missionControl.timeLeft(_renter, x, y, z);
                        _totalTileInfo[_count].order = IMissionControl.CollectOrder(x, y, z);
                        IMissionControl.TileRentalInfo memory rentInfo = missionControl.getTileRentalInfo(
                            _renter,
                            x,
                            y
                        );
                        _totalTileInfo[_count].isRented = rentInfo.isRented;
                        _totalTileInfo[_count].rentalToken = rentInfo.rentalToken;
                        _totalTileInfo[_count].flowRate = missionControl.getTileRequirements(x, y).price;
                        _count++;
                    }
                }
            }
        }
    }

    function getAllCheckTileInfo(address _renter)
        external
        view
        returns (IMissionControl.TotalCheckTileInfo[] memory _totalCheckTileInfo)
    {
        IMissionControl.GetAllCheckTileInfoVars memory v;
        uint256 allowedRadius = missionControl.getAllowedRadius();
        v.radius = int256(allowedRadius);
        for (uint256 i = 1; i <= allowedRadius; i++) v.totalTiles += i * 6;
        _totalCheckTileInfo = new IMissionControl.TotalCheckTileInfo[](v.totalTiles);
        for (int256 x = -v.radius; x <= v.radius; x++) {
            for (int256 y = -v.radius; y <= v.radius; y++) {
                for (int256 z = -v.radius; z <= v.radius; z++) {
                    int256 sum = x + y + z;
                    uint256 rad = (abs(x) + abs(y) + abs(z)) / 2;
                    if (rad <= allowedRadius && rad > 0 && sum == 0) {
                        _totalCheckTileInfo[v.count].order = IMissionControl.CollectOrder(x, y, z);
                        _totalCheckTileInfo[v.count].out = missionControl.checkTile(
                            IMissionControl.CheckTileInputs(_renter, x, y, z)
                        );
                        v.count++;
                    }
                }
            }
        }
    }

    /**
     * @notice Calculates the absolute value of an integer
     * @param _value The integer
     * @return Its absolute value
     */
    function abs(int256 _value) private pure returns (uint256) {
        return _value < 0 ? uint256(-_value) : uint256(_value);
    }
}
