// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IMissionControlStaking.sol";
import "./IMissionControl.sol";
import "./IMissionControlStakeable.sol";
import "./IMCCrosschainServices.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./IAssetManager.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/draft-EIP712Upgradeable.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

import "hardhat/console.sol";

/// @title Implementation of IMissionControl
contract MissionControl is IMissionControl, OwnableUpgradeable, EIP712Upgradeable {
    /**
     * @notice Event emitted when an NFT is placed on a tile
     * @param user The users address
     * @param tokenAddress The NFT address
     * @param tokenId The id of the token
     * @param x x-coordinate
     * @param y y-coordinate
     */
    event NFTPlaced(address user, address tokenAddress, uint256 tokenId, int256 x, int256 y);

    /**
     * @notice Event emitted when an NFT is removed from a tile
     * @param user The users address
     * @param tokenAddress The address of the removed token
     * @param underlyingAddress The address of the underlying (base) token, zero address if there is none
     * @param tokenId The token ID of the removed token
     * @param underlyingTokenId The tokenId of the underlying (base) token, zero if there is none
     * @param x x-coordinate
     * @param y y-coordinate
     */
    event NFTRemoved(
        address user,
        address tokenAddress,
        address underlyingAddress,
        uint256 tokenId,
        uint256 underlyingTokenId,
        int256 x,
        int256 y
    );

    /**
     * @notice Emitted when a loot crate is found
     * @param user The user that found the crate
     */
    event LootCrateFound(address user);

    /**
     * @notice Event emitted when the whitelist is set
     * @param token The token address
     * @param whitelisted Whether the token is whitelisted or not
     */
    event WhitelistSet(address token, bool whitelisted);

    /**
     * @notice Event emitted when the mint is set
     * @param newMint The address of the mint
     */
    event MintSet(address newMint);

    /**
     * @notice Event emitted when the radius is set
     * @param newRadius The new radius
     */
    event RadiusSet(uint256 newRadius);

    event Raid(address defender, uint256 raidId, uint256 booty, uint256 timeStamp);

    // Mapping storing the information regarding staked tokens
    mapping(address => mapping(int256 => mapping(int256 => TileElement[3]))) public tiles;
    mapping(address => uint256) stakeNonces;
    mapping(uint256 => bool) raidSpent;
    // Mapping of whitelisted tokens
    mapping(address => bool) public whiteList;
    // Mapping of addresses allowed to sign raids
    mapping(address => bool) signers;

    // The contract responsible for minting rewards
    IAssetManager mint;
    // The currently allowed max radius of the mission control
    uint256 public allowedRadius;
    // raid params
    int256 raidTimeout;

    // Constants
    uint256 constant WASTE_ID = uint256(IAssetManager.AssetIds.Waste);
    bytes32 private constant RAID_MESSAGE =
        keccak256("RaidMessage(address _defender,uint256 _timestamp,uint256 _raidId)");

    // out of order so that i can upgrade on testnet for now
    mapping(address => address) tokenAliases;
    bool public enableSignature;
    mapping(address => uint256) public nonces;
    // This allows players to get a sig and then use it later but it's no big deal
    bytes32 private constant COLLECT_MESSAGE = keccak256("CollectMessage(address _user,uint256 _nonce)");

    address public streamer;
    address private aGold;
    address private aGoldLite;
    address private missionControlStaking;
    address private tileContracts;
    address tileContractLockProvider;
    IMCCrosschainServices private crosschainServices;

    mapping(address => mapping(address => CollectOrder[])) private userRentedTiles;
    mapping(address => mapping(address => mapping(int256 => mapping(int256 => uint256))))
        private userRentedTilesIndices;

    mapping(address => mapping(int256 => mapping(int256 => TileRentalInfo))) public tileRentalInfo;
    mapping(int256 => mapping(int256 => TileRequirements)) public tileRequirements;

    /**
     * @notice Modifier to hinder calls with invalid coordinates
     * @param _x X-coordinate of tile
     * @param _y Y-coordinate of tile
     * @param _z Z-coordinate of tile
     */
    modifier validCoordinates(
        int256 _x,
        int256 _y,
        int256 _z
    ) {
        int256 sum = _x + _y + _z;
        uint256 rad = (abs(_x) + abs(_y) + abs(_z)) / 2;
        if (rad > allowedRadius || rad == 0 || sum != 0) revert MC__InvalidCoordinates();
        _;
    }

    /**
     * @notice Modifier to only allow whitelisted tokens
     * @param _tokenAddress The address of the token to be checked against the whitelist
     */
    modifier onWhitelist(address _tokenAddress) {
        if (!whiteList[_tokenAddress] && !whiteList[tokenAliases[_tokenAddress]]) revert MC__NotWhitelisted();
        _;
    }

    /**
     *
     */
    modifier checkSignature(
        uint8 _v,
        bytes32 _r,
        bytes32 _s
    ) {
        if (enableSignature) {
            address signer = ECDSA.recover(
                _hashTypedDataV4(keccak256(abi.encode(COLLECT_MESSAGE, msg.sender, nonces[msg.sender]++))),
                _v,
                _r,
                _s
            );
            if (!signers[signer]) revert MC__InvalidSigner();
        }
        _;
    }

    modifier onlyStreamer() {
        if (msg.sender != streamer) revert MC__InvalidStreamer();
        _;
    }

    /**
     * @notice Initialiser for this contract
     * @param _radius The mission control radius
     * @param _mint The address of the mint
     */
    function initialize(uint256 _radius, address _mint) external initializer {
        allowedRadius = _radius;
        mint = IAssetManager(_mint);
        __Ownable_init();
        __EIP712_init("Mission Control", "1");
    }

    /// @inheritdoc IMissionControl
    function placeNFTs(PlaceOrder[] memory _placeOrders) external override {
        for (uint256 i; i < _placeOrders.length; i++) {
            uint256 rad = (abs(_placeOrders[i].order.x) + abs(_placeOrders[i].order.y) + abs(_placeOrders[i].order.z)) /
                2;
            if (rad > 1) {
                TileRentalInfo storage tile = tileRentalInfo[msg.sender][_placeOrders[i].order.x][
                    _placeOrders[i].order.y
                ];
                if (!tile.isRented) revert MC__TileNotRented();
                if (tile.pausedAt != 0) revert MC__TilePaused();
            }
            TileRequirements memory _tileRequirements = tileRequirements[_placeOrders[i].order.x][
                _placeOrders[i].order.y
            ];
            if (
                IMissionControlStaking(missionControlStaking).getUserStakedBalance(
                    msg.sender,
                    tileContracts,
                    _tileRequirements.tileContractId
                ) == 0
            ) revert MC__NoTileContractStaked();
            __placeNFT(_placeOrders[i].order, _placeOrders[i].tokenId, _placeOrders[i].tokenAddress, msg.sender);
        }
    }

    /// @inheritdoc IMissionControl
    function removeNFTs(
        CollectOrder[] memory _orders,
        uint256[] memory _positions,
        uint8 _v,
        bytes32 _r,
        bytes32 _s
    ) external override checkSignature(_v, _r, _s) {
        CollectFromTilesVars memory vars;
        vars.arrayLen = _orders.length;
        vars.ids = new uint256[](vars.arrayLen);
        vars.amounts = new uint256[](vars.arrayLen);
        for (uint256 i; i < _orders.length; i++) {
            (vars.amounts[i], vars.ids[i]) = __collectFromTile(_orders[i], _positions[i]);
            __removeNFT(_orders[i], msg.sender, _positions[i]);
        }
        mint.trustedBatchMint(msg.sender, vars.ids, vars.amounts);
    }

    function __collectFromTile(CollectOrder memory _order, uint256 _position) private returns (uint cAmount, uint cId) {
        (cAmount, cId) = _handleCollect(HandleCollectInputs(_order.x, _order.y, _order.z, _position));
    }

    /** @inheritdoc IMissionControl
     * @dev This code is absolute cancer, but is this way to only require 1 cross contract call
     */
    function collectFromTiles(
        CollectOrder[] memory _orders,
        uint256[] memory _positions,
        uint8 _v,
        bytes32 _r,
        bytes32 _s
    ) public override checkSignature(_v, _r, _s) {
        CollectFromTilesVars memory vars;

        vars.arrayLen = _orders.length;
        vars.ids = new uint256[](vars.arrayLen);
        vars.amounts = new uint256[](vars.arrayLen);
        for (uint256 i; i < _orders.length; i++)
            (vars.amounts[i], vars.ids[i]) = __collectFromTile(_orders[i], _positions[i]);

        mint.trustedBatchMint(msg.sender, vars.ids, vars.amounts);
    }

    // user start streaming to the game
    // TODO remember to check if coords valid before renting
    function createRentTiles(
        address _supertoken,
        address _renter,
        CollectOrder[] memory _order,
        int96 _flowRate
    ) external onlyStreamer {
        if (!(_supertoken == aGold || _supertoken == aGoldLite)) revert MC__InvalidSuperToken();
        uint256 requiredFlow;

        requiredFlow += _handleTileRentals(_supertoken, _renter, _order);
        if (crosschainServices.getNewlandsGenesisBalance(_renter) > 0) requiredFlow = 0;

        if (_flowRate != int96(uint96(requiredFlow))) revert MC__InvalidFlow();
    }

    // user is streaming and change the rented tiles
    function updateRentTiles(
        address _supertoken,
        address _renter,
        CollectOrder[] memory _order,
        CollectOrder[] memory _removeTiles,
        int96 _oldFlowRate,
        int96 _flowRate
    ) external onlyStreamer {
        if (!(_supertoken == aGold || _supertoken == aGoldLite)) revert MC__InvalidSuperToken();
        UpdateRentTileVars memory vars;

        vars.requiredFlow += int96(uint96(_handleTileRentals(_supertoken, _renter, _order)));

        vars.removeTileLength = _removeTiles.length;
        for (uint256 i; i < vars.removeTileLength; i++) {
            vars.removeTileItem = _removeTiles[i];

            if (!tileRentalInfo[_renter][vars.removeTileItem.x][vars.removeTileItem.y].isRented)
                revert MC__TileNotRented();
            if (tileRentalInfo[_renter][vars.removeTileItem.x][vars.removeTileItem.y].rentalToken != _supertoken)
                revert MC__TileRentedWithDifferentSuperToken();

            _removeRental(_renter, vars.removeTileItem.x, vars.removeTileItem.y);

            vars.requiredFlow -= int96(uint96(tileRequirements[vars.removeTileItem.x][vars.removeTileItem.y].price));

            vars.index = userRentedTilesIndices[_renter][_supertoken][vars.removeTileItem.x][vars.removeTileItem.y];
            vars.size = userRentedTiles[_renter][_supertoken].length;

            userRentedTiles[_renter][_supertoken][vars.index] = userRentedTiles[_renter][_supertoken][vars.size - 1];
            vars.order = userRentedTiles[_renter][_supertoken][vars.index];
            userRentedTilesIndices[_renter][_supertoken][vars.order.x][vars.order.y] = vars.index;
            userRentedTiles[_renter][_supertoken].pop();
        }

        if (crosschainServices.getNewlandsGenesisBalance(_renter) > 0) vars.requiredFlow = 0;
        if (_flowRate != _oldFlowRate + vars.requiredFlow) revert MC__InvalidFlow();
    }

    // user stop streaming to the game
    function deleteRentTiles(address _supertoken, address _renter) external {
        if (msg.sender != streamer && msg.sender != address(crosschainServices))
            revert MC__SenderNotStreamerNorCrosschain();
        if (_supertoken != aGold && _supertoken != aGoldLite) return;
        uint256 i;
        uint256 length = userRentedTiles[_renter][_supertoken].length;
        for (i; i < length; i++) {
            CollectOrder memory rental = userRentedTiles[_renter][_supertoken][i];
            _removeRental(_renter, rental.x, rental.y);
        }
        delete userRentedTiles[_renter][_supertoken];
    }

    function _removeRental(address _renter, int256 _x, int256 _y) internal {
        TileRentalInfo storage tile = tileRentalInfo[_renter][_x][_y];
        tile.pausedAt = block.timestamp;
        tile.isRented = false;
        tile.rentalToken = address(0);
    }

    function _handleTileRentals(
        address _supertoken,
        address _renter,
        CollectOrder[] memory _order
    ) internal returns (uint256 _requiredFlow) {
        uint256 i;
        uint256 length = _order.length;

        for (; i < length; i++) {
            CollectOrder memory orderItem = _order[i];
            TileRentalInfo storage _tileRentalInfo = tileRentalInfo[_renter][orderItem.x][orderItem.y];
            TileRequirements memory _tileRequirements = tileRequirements[orderItem.x][orderItem.y];
            int256 sum = orderItem.x + orderItem.y + orderItem.z;
            uint256 rad = (abs(orderItem.x) + abs(orderItem.y) + abs(orderItem.z)) / 2;
            if (rad > allowedRadius || rad == 0 || sum != 0) revert MC__InvalidCoordinates();
            if (
                IMissionControlStaking(missionControlStaking).getUserStakedBalance(
                    _renter,
                    tileContracts,
                    _tileRequirements.tileContractId
                ) == 0
            ) revert MC__NoTileContractStaked();

            if (_tileRentalInfo.pausedAt != 0) {
                _tileRentalInfo.pausedAt = 0;
                _tileRentalInfo.isRented = true;
                _tileRentalInfo.rentalToken = _supertoken;

                userRentedTiles[_renter][_supertoken].push(orderItem);
                userRentedTilesIndices[_renter][_supertoken][orderItem.x][orderItem.y] =
                    userRentedTiles[_renter][_supertoken].length -
                    1;

                TileElement[3] memory tileElements = tiles[_renter][orderItem.x][orderItem.y];
                if (tileElements[0].nonce != 0)
                    IMissionControlStakeable(tileElements[0].stakeable).onResume(_renter, tileElements[0].nonce);

                if (tileElements[1].nonce != 0)
                    IMissionControlStakeable(tileElements[1].stakeable).onResume(_renter, tileElements[1].nonce);

                if (tileElements[2].nonce != 0)
                    IMissionControlStakeable(tileElements[2].stakeable).onResume(_renter, tileElements[2].nonce);

                _requiredFlow += _tileRequirements.price;
            } else {
                if (_tileRentalInfo.isRented) revert MC__TileRented();
                _tileRentalInfo.isRented = true;
                _tileRentalInfo.rentalToken = _supertoken;

                userRentedTiles[_renter][_supertoken].push(orderItem);
                userRentedTilesIndices[_renter][_supertoken][orderItem.x][orderItem.y] =
                    userRentedTiles[_renter][_supertoken].length -
                    1;

                _requiredFlow += _tileRequirements.price;
            }
        }
    }

    function toggleTile(address _renter, int256 _x, int256 _y, uint256 rad, bool _shouldPause) external {
        if (msg.sender != tileContractLockProvider) revert MC__SenderNotTCLockProvider();
        TileRentalInfo storage tile = tileRentalInfo[_renter][_x][_y];
        uint256 timestamp = tile.pausedAt;

        if (_shouldPause == false) {
            if (timestamp != 0 && (rad == 1 || (rad > 1 && tile.isRented))) {
                TileElement[3] memory tileElements = tiles[_renter][_x][_y];
                if (tileElements[0].nonce != 0)
                    IMissionControlStakeable(tileElements[0].stakeable).onResume(_renter, tileElements[0].nonce);

                if (tileElements[1].nonce != 0)
                    IMissionControlStakeable(tileElements[1].stakeable).onResume(_renter, tileElements[1].nonce);

                if (tileElements[2].nonce != 0)
                    IMissionControlStakeable(tileElements[2].stakeable).onResume(_renter, tileElements[2].nonce);

                tile.pausedAt = 0;
            }
        }

        if (_shouldPause == true) tile.pausedAt = block.timestamp;
    }

    /// @inheritdoc IMissionControl
    function checkTile(
        CheckTileInputs memory args
    ) public view override validCoordinates(args.x, args.y, args.z) returns (CheckTileOutputs memory out) {
        TileElement[3] memory elements = tiles[args.user][args.x][args.y];

        if (elements[0].nonce != 0)
            (out.bottomAmount, out.bottomId) = IMissionControlStakeable(elements[0].stakeable).checkTile(
                args.user,
                elements[0].nonce,
                tileRentalInfo[args.user][args.x][args.y].pausedAt,
                args.x,
                args.y
            );

        if (elements[1].nonce != 0)
            (out.topAmount1, out.topId1) = IMissionControlStakeable(elements[1].stakeable).checkTile(
                args.user,
                elements[1].nonce,
                tileRentalInfo[args.user][args.x][args.y].pausedAt,
                args.x,
                args.y
            );

        if (elements[2].nonce != 0)
            (out.topAmount2, out.topId2) = IMissionControlStakeable(elements[2].stakeable).checkTile(
                args.user,
                elements[2].nonce,
                tileRentalInfo[args.user][args.x][args.y].pausedAt,
                args.x,
                args.y
            );
    }

    /// @inheritdoc IMissionControl
    function checkStakedOnTile(
        address _user,
        int256 _x,
        int256 _y,
        int256 _z
    )
        public
        view
        override
        validCoordinates(_x, _y, _z)
        returns (TileElement memory _base, TileElement memory _top1, TileElement memory _top2)
    {
        _top2 = tiles[_user][_x][_y][2];
        _top1 = tiles[_user][_x][_y][1];
        _base = tiles[_user][_x][_y][0];
    }

    /// @inheritdoc IMissionControl
    function timeLeft(
        address _user,
        int256 _x,
        int256 _y,
        int256 _z
    )
        public
        view
        override
        validCoordinates(_x, _y, _z)
        returns (int256 _timeLeftBottom, int256 _timeLeftTop1, int256 _timeLeftTop2)
    {
        TileElement[3] memory elements = tiles[_user][_x][_y];
        if (elements[0].nonce != 0)
            _timeLeftBottom = IMissionControlStakeable(elements[0].stakeable).timeLeft(
                _user,
                elements[0].nonce,
                _x,
                _y
            );
        if (elements[1].nonce != 0)
            _timeLeftTop1 = IMissionControlStakeable(elements[1].stakeable).timeLeft(_user, elements[1].nonce, _x, _y);
        if (elements[2].nonce != 0)
            _timeLeftTop2 = IMissionControlStakeable(elements[2].stakeable).timeLeft(_user, elements[2].nonce, _x, _y);
    }

    /// @inheritdoc IMissionControl
    function checkLastUpdated(
        address _user,
        int256 _x,
        int256 _y,
        int256 _z,
        uint256 _position
    ) external view override validCoordinates(_x, _y, _z) returns (uint256 _timestamp) {
        _timestamp = IMissionControlStakeable(tiles[_user][_x][_y][_position].stakeable).checkTimestamp(
            _user,
            tiles[_user][_x][_y][_position].nonce
        );
    }

    function getUserRentals(
        address _renter,
        address _supertoken
    ) external view returns (CollectOrder[] memory _orders) {
        _orders = userRentedTiles[_renter][_supertoken];
    }

    function getAllowedRadius() external view returns (uint256) {
        return allowedRadius;
    }

    function getTileRentalInfo(
        address _user,
        int256 _x,
        int256 _y
    ) external view returns (TileRentalInfo memory _info) {
        _info = tileRentalInfo[_user][_x][_y];
    }

    function getTileRequirements(int256 _x, int256 _y) external view returns (TileRequirements memory _requirements) {
        _requirements = tileRequirements[_x][_y];
    }

    /**
     * Private functions
     */

    /**
     * @notice Calculates the absolute value of an integer
     * @param _value The integer
     * @return Its absolute value
     */
    function abs(int256 _value) private pure returns (uint256) {
        return _value < 0 ? uint256(-_value) : uint256(_value);
    }

    /**
     * @notice Handles the placement of a tile if it is classified as a base
     * @param _x X-coordinate of tile
     * @param _y Y-coordinate of tile
     * @param _tokenId Token ID of NFT
     * @param _stakeable Interface towards the token contract
     */
    function _handleBasePlacement(
        int256 _x,
        int256 _y,
        uint256 _tokenId,
        address _stakeable,
        uint256 _nonce,
        address _staker
    ) private {
        if (tiles[_staker][_x][_y][0].nonce != 0) revert MC__BaseStaked();
        IMissionControlStakeable(_stakeable).onStaked(
            _staker,
            _tokenId,
            _nonce,
            address(0),
            0,
            address(0),
            0,
            0,
            _x,
            _y
        );
        // Could use this variable above but i think this saves on case in case of revert on the above line?
        TileElement storage tileElement = tiles[_staker][_x][_y][0];
        tileElement.nonce = _nonce;
        tileElement.tokenId = _tokenId;
        tileElement.stakeable = _stakeable;
    }

    /**
     * @notice Handles the placement of a tile if it is classified as a top
     */
    function _handleTopPlacement(HandleTopPlacementArgs memory args) private {
        if (
            !(tiles[args.staker][args.x][args.y][0].nonce != 0 &&
                (tiles[args.staker][args.x][args.y][1].nonce == 0 || tiles[args.staker][args.x][args.y][2].nonce == 0))
        ) revert MC__InvalidPlacement();

        uint256 freeSlot = tiles[args.staker][args.x][args.y][1].nonce == 0 ? 1 : 2;
        uint256 otherSlot = freeSlot == 1 ? 2 : 1;

        TileElement storage tileElement = tiles[args.staker][args.x][args.y][freeSlot];
        tileElement.nonce = args.nonce;
        tileElement.tokenId = args.tokenId;
        tileElement.stakeable = args.stakeable;

        IMissionControlStakeable(args.stakeable).onStaked(
            args.staker,
            args.tokenId,
            args.nonce,
            tiles[args.staker][args.x][args.y][0].stakeable,
            tiles[args.staker][args.x][args.y][0].tokenId,
            tiles[args.staker][args.x][args.y][otherSlot].stakeable,
            tiles[args.staker][args.x][args.y][otherSlot].tokenId,
            tiles[args.staker][args.x][args.y][otherSlot].nonce,
            args.x,
            args.y
        );
    }

    /**
     * @notice Handles overarching logic for placing a nft on a tile
     * @param _tokenId Token ID of NFT
     * @param _tokenAddress Address of NFT, which is presumed to implement the IMissionControlStakeable interface
     */
    function __placeNFT(
        CollectOrder memory order,
        uint256 _tokenId,
        address _tokenAddress,
        address _staker
    ) private validCoordinates(order.x, order.y, order.z) onWhitelist(_tokenAddress) {
        // If the sender is not the owner of the token, the transaction should revert here
        address tokenAddress = _tokenAddress;
        if (tokenAliases[_tokenAddress] != address(0)) {
            tokenAddress = tokenAliases[_tokenAddress];
        }
        if (IMissionControlStakeable(tokenAddress).isBase(_tokenId, order.x, order.y)) {
            _handleBasePlacement(order.x, order.y, _tokenId, tokenAddress, ++stakeNonces[_staker], _staker);
        } else {
            _handleTopPlacement(
                HandleTopPlacementArgs(order.x, order.y, _tokenId, tokenAddress, ++stakeNonces[_staker], _staker)
            );
        }

        emit NFTPlaced(_staker, tokenAddress, _tokenId, order.x, order.y);
    }

    /**
     * @notice Handles the logic for unstaking a token
     */
    function __removeNFT(CollectOrder memory order, address _renter, uint256 _position) private {
        RemoveNFTVars memory vars;
        TileElement[3] storage tileElements = tiles[_renter][order.x][order.y];

        if (tileElements[_position].nonce == 0) revert MC__TileEmpty();
        if (_position == 0 && (tileElements[1].nonce != 0 || tileElements[2].nonce != 0)) revert MC__TopStaked();

        uint256 _besideSlot;
        if (_position != 0) _besideSlot = _position == 1 ? 2 : 1;

        vars.tokenId = tileElements[_position].tokenId;
        vars.stakeable = tileElements[_position].stakeable;
        _handleReturn(tileElements[_position], tileElements[_besideSlot], _renter);
        if (_position == 1 || _position == 2) {
            IMissionControlStakeable(tileElements[0].stakeable).reset(_renter, tileElements[0].nonce);
        }
        emit NFTRemoved(
            _renter,
            vars.stakeable,
            _position == 1 || _position == 2 ? tileElements[0].stakeable : address(0),
            vars.tokenId,
            _position == 1 || _position == 2 ? tileElements[0].tokenId : 0,
            order.x,
            order.y
        );
    }

    function _handleReturn(TileElement storage element, TileElement storage besideElement, address _renter) private {
        IMissionControlStakeable(element.stakeable).onUnstaked(_renter, element.nonce, besideElement.nonce);
        element.nonce = 0;
        element.tokenId = 0;
        element.stakeable = address(0);
    }

    function _handleCollect(
        HandleCollectInputs memory args
    ) private validCoordinates(args.x, args.y, args.z) returns (uint256, uint256) {
        TileElement[3] storage tileElements = tiles[msg.sender][args.x][args.y];

        if (tileElements[args.position].nonce == 0) revert MC__TileEmpty();
        if (args.position == 0 && (tileElements[1].nonce != 0 || tileElements[2].nonce != 0)) revert MC__TopStaked();

        HandleCollectVars memory vars;
        vars.tileElement = tileElements[args.position];
        vars.tileRentalInfo = tileRentalInfo[msg.sender][args.x][args.y];
        return
            IMissionControlStakeable(vars.tileElement.stakeable).onCollect(
                msg.sender,
                vars.tileElement.nonce,
                vars.tileRentalInfo.pausedAt,
                args.x,
                args.y
            );
    }

    /**
     * Admin functions
     */

    /**
     * @notice Allows the owner to set a token to whitelisted
     * @param _token Address of token
     * @param _whitelisted Whether it should be whitelisted
     */
    function setWhitelist(address _token, bool _whitelisted) external onlyOwner {
        whiteList[_token] = _whitelisted;
        emit WhitelistSet(_token, _whitelisted);
    }

    /**
     * @notice Allows the owner to set the mint
     * @param _mint The address of the mint
     */
    function setMint(address _mint) external onlyOwner {
        mint = IAssetManager(_mint);
        emit MintSet(_mint);
    }

    /**
     * @notice Allows the owner to set the maximum radius
     * @param _radius The radius
     */
    function setRadius(uint256 _radius) external onlyOwner {
        if (_radius == 0) revert MC__ZeroRadius();
        allowedRadius = _radius;
        emit RadiusSet(_radius);
    }

    function setRaidSigner(address _address, bool _isSigner) external onlyOwner {
        signers[_address] = _isSigner;
    }

    function setStreamer(address _streamer) external onlyOwner {
        if (_streamer == address(0)) revert MC__ZeroAddress();
        streamer = _streamer;
    }

    function setAGold(address _aGold) external onlyOwner {
        if (_aGold == address(0)) revert MC__ZeroAddress();
        aGold = _aGold;
    }

    function setAGoldLite(address _aGoldLite) external onlyOwner {
        if (_aGoldLite == address(0)) revert MC__ZeroAddress();
        aGoldLite = _aGoldLite;
    }

    function setMissionControlStaking(address _staking) external onlyOwner {
        if (_staking == address(0)) revert MC__ZeroAddress();
        missionControlStaking = _staking;
    }

    function setTileContracts(address _tileContracts) external onlyOwner {
        if (_tileContracts == address(0)) revert MC__ZeroAddress();
        tileContracts = _tileContracts;
    }

    function setCrosschainServices(IMCCrosschainServices _crosschainServices) external onlyOwner {
        if (address(_crosschainServices) == address(0)) revert MC__ZeroAddress();
        crosschainServices = _crosschainServices;
    }

    function setSignature(bool _requiresSignature) external onlyOwner {
        enableSignature = _requiresSignature;
    }

    function setTileContractLockProvider(address _tileContractLockProvider) external onlyOwner {
        tileContractLockProvider = _tileContractLockProvider;
    }

    function setTileRequirements(int256 _x, int256 _y, uint256 _price, uint256 _tileContractId) external onlyOwner {
        TileRequirements storage _tileRequirements = tileRequirements[_x][_y];
        _tileRequirements.price = _price;
        _tileRequirements.tileContractId = _tileContractId;
    }
}
