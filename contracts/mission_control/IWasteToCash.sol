// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/ITrader.sol";

/// @title The global waste systems exchanges waste for Astro Credits, as defined per the specs.
interface IWasteToCash is ITrader{
    /**
     * @notice Places an order to have waste exchanged for Astro Credits
     * @param _wasteAmount The amount of waste to exchange
     */
    function placeWasteOrder(uint256 _wasteAmount) external;

    /**
     * @notice Checks the current exchange rate, AC = round((WASTE)*(_numerator/_denominator))
     * @dev (_numerator/_denominator) may round to zero if integer division is used, so be careful in implementing
     * @return _numerator Numerator of exchange rate
     * @return _denominator Denominator of exchange rate
     */
    function getExchangeRate() external view returns (uint256 _numerator, uint256 _denominator);
}
