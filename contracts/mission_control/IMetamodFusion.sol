// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/ITrader.sol";

/// @title The Metamod Fusion allows the user to make metamods
interface IMetamodFusion is ITrader {
    error MetamodFusion__MaxOrdersReached();
    error MetamodFusion__NoOrders();
    error MetamodFusion__InvalidOrder(uint256 _orderId);
    error MetamodFusion__OrderNotFinished();
    error MetamodFusion__NoSpeedUpAvailable();

    event ClaimOrder(address indexed player, uint256 indexed orderId);
    event SpeedUpOrder(address indexed player, uint256 indexed orderId, uint256 numSpeedUps);
    event SpeedUpOrderIXT(address indexed player, uint256 indexed orderId, uint256 numSpeedUps);
    event PlaceMetamodFusionOrder(address indexed player, uint256 indexed orderId);
}
