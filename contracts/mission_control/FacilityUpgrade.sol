// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IFacilityUpgrade.sol";
import "./IBaseLevel.sol";
import "./IAssetManager.sol";
import "../util/Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "abdk-libraries-solidity/ABDKMathQuad.sol";

interface IVRFManager {
    function getRandomNumber(uint32 numWords) external returns (uint256 requestId);
}

interface IRNGConsumer {
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external;
}

contract FacilityUpgrade is IFacilityUpgrade, IRNGConsumer, OwnableUpgradeable, ReentrancyGuardUpgradeable {

    struct UpgradeCost {
        uint256[] tokenIds;
        uint256[] amounts;
    }

    struct FacilityUpgradeOrder {
        address player;
        Order baseOrder;
        uint256 facilityTokenId;
        uint256 doubleUpgradeChance;
        uint256 vrfRequestId;
    }

    mapping(address => uint256) public cooldownEndTimestamp;
    uint256 public upgradingTime;
    uint256 public maxOrders;
    uint256 public minBaseLevel;
    uint256 public speedupTime;

    mapping(uint256 => uint256) public assetLevel;
    mapping(uint256 => uint256) public upgradeToAssetId;

    mapping(uint256 => UpgradeCost) private s_levelUpgradeCost;

    mapping(uint256 => uint256) public doubleUpgradeChanceForEnergy;
    uint256 public constant doubleUpgradeChanceDenominator = 100_00;

    address public pixt;
    uint256 public pixtSpeedupCost;
    uint256 public pixtSpeedupSplitBps;
    uint256 public constant pixtSpeedupSplitBpsDenominator = 100_00;

    uint256 public cooldownTimePerLevel;
    uint256 public cooldownSpeedupCost;
    uint256 public cooldownSpeedupTime;

    uint256 public nextOrderId;
    mapping(uint256 => FacilityUpgradeOrder) public upgradingOrders;
    mapping(address => uint256) public currentUpgradeOrder;
    mapping(uint256 => uint256) public orderForRequest; // requestId => orderId

    IAssetManager public assetManager;
    IBaseLevel public baseLevel;
    IVRFManager public vrfManager;
    IVRFManager public vrfManagerV2;

    // mapping(uint256 => uint256) public exponentDoubleUpgradeChanceForEnergy;

    // @dev after init function:
    // @dev setFacilityParams must be called to initialize levels
    // @dev setLevelUpgradeCost should be called for levels that can be upgraded
    // @dev calcExponentDoubleUpgradeChance should be called to calculate double upgrade chances

    /// @notice Makes sure function is called only by vrf oracle
    modifier onlyOracle() {
        if (msg.sender != address(vrfManagerV2)) revert FacilityUpgrade__NotOracle();
        _;
    }

    function initialize(
        address _assetManagerAddress,
        address _baseLevelAddress,
        address _vrfManagerAddress,
        address _pixt
    ) public initializer {
        __Ownable_init();

        assetManager = IAssetManager(_assetManagerAddress);
        baseLevel = IBaseLevel(_baseLevelAddress);
        vrfManager = IVRFManager(_vrfManagerAddress);
        pixt = _pixt;

        nextOrderId = 1;
        
        maxOrders = 1;

        minBaseLevel = 30;

        upgradingTime = 7 days;

        speedupTime = 1 hours;
        pixtSpeedupCost = 0.4 ether;
        pixtSpeedupSplitBps = 50_00;

        cooldownTimePerLevel = 7 days;
        cooldownSpeedupTime = 1 hours;
        cooldownSpeedupCost = 0.4 ether;
    }

    function setBaseLevelAddress(address _baseLevelAddress) external override onlyOwner {
        baseLevel = IBaseLevel(_baseLevelAddress);
    }

    function setAssetManagerAddress(address _assetManagerAddress) external onlyOwner {
       assetManager = IAssetManager(_assetManagerAddress);
    }

    function setVRFManager(address _vrfManagerAddress) external onlyOwner {
        vrfManager = IVRFManager(_vrfManagerAddress);
    }
    
    function setVRFManagerV2(address _vrfManagerAddress) external onlyOwner {
        vrfManagerV2 = IVRFManager(_vrfManagerAddress);
    }

    function setCooldownParams(
        uint256 _cooldownTimePerLevel,
        uint256 _cooldownSpeedupCost,
        uint256 _cooldownSpeedupTime
    ) external onlyOwner {
        cooldownTimePerLevel = _cooldownTimePerLevel;
        cooldownSpeedupCost = _cooldownSpeedupCost;
        cooldownSpeedupTime = _cooldownSpeedupTime;
    }

    function setSpeedUpTime(uint256 _speedUpTime) external onlyOwner {
        speedupTime = _speedUpTime;
    }

    function setIXTSpeedUpParams(
        address _pixt,
        address /*_beneficiary*/,
        uint256 _pixtSpeedupSplitBps,
        uint256 _pixtSpeedupCost
    ) external override {
        pixt = _pixt;
        pixtSpeedupSplitBps = _pixtSpeedupSplitBps;
        pixtSpeedupCost = _pixtSpeedupCost;
    }

    function setLevelUpgradeCost(uint256 level, UpgradeCost calldata costParams) external onlyOwner {
        s_levelUpgradeCost[level] = costParams;
    }

    function setDoubleUpgradeChanceForEnergy(uint256 _energy, uint256 _chance) external onlyOwner {
        doubleUpgradeChanceForEnergy[_energy] = _chance;
    }

    function setFacilityParams(
        uint256[] calldata assetIds,
        uint256[] calldata levels,
        uint256[] calldata upgradeToAssetIds
    ) external onlyOwner {
        if (assetIds.length != levels.length || assetIds.length != upgradeToAssetIds.length) {
            revert FacilityUpgrade__ArrayLengthsDontMatch();
        }

        for (uint256 i = 0; i < assetIds.length; i++) {
            assetLevel[assetIds[i]] = levels[i];
            upgradeToAssetId[assetIds[i]] = upgradeToAssetIds[i];
        }
    }

    function getLevelUpgradeCost(uint256 level) external view returns(UpgradeCost memory) {
        return s_levelUpgradeCost[level];
    }

    function placeFacilityUpgradeOrder(uint256 _facilityTokenId, uint256 _additionalEnergy) external {
        if (baseLevel.getBaseLevel(msg.sender) < minBaseLevel) revert FacilityUpgrade__BaseLevelUnderMinimum();

        if (currentUpgradeOrder[msg.sender] > 0) revert FacilityUpgrade__AlreadyUpgrading(currentUpgradeOrder[msg.sender]);

        if (block.timestamp < cooldownEndTimestamp[msg.sender]) revert FacilityUpgrade__CooldownNotFinished();

        UpgradeCost storage upgradeCost = s_levelUpgradeCost[assetLevel[_facilityTokenId]];
        if (upgradeCost.tokenIds.length == 0) revert FacilityUpgrade__FacilityNotUpgradeable();

        // burn tokens for upgrade cost
        assetManager.trustedBatchBurn(msg.sender, upgradeCost.tokenIds, upgradeCost.amounts);

        // burn facility
        assetManager.trustedBurn(msg.sender, _facilityTokenId, 1);

        // burn additional energy
        uint256 orderDoubleUpgradeChance = doubleUpgradeChanceForEnergy[0];
        if (_additionalEnergy > 0) {
            if (doubleUpgradeChanceForEnergy[_additionalEnergy] == 0) revert FacilityUpgrade__WrongAdditionalEnergy();

            orderDoubleUpgradeChance = doubleUpgradeChanceForEnergy[_additionalEnergy];

            assetManager.trustedBurn(msg.sender, uint256(IAssetManager.AssetIds.Energy), _additionalEnergy);
        }

        upgradingOrders[nextOrderId] = FacilityUpgradeOrder({
            player: msg.sender,
            baseOrder: Order({
                orderId: nextOrderId,
                orderAmount: 1,
                createdAt: block.timestamp,
                speedUpDeductedAmount: 0,
                totalCompletionTime: upgradingTime
            }),
            facilityTokenId: _facilityTokenId,
            doubleUpgradeChance: orderDoubleUpgradeChance,
            vrfRequestId: 0
        });

        currentUpgradeOrder[msg.sender] = nextOrderId;

        emit PlaceFacilityUpgradeOrder(msg.sender, nextOrderId, _facilityTokenId, orderDoubleUpgradeChance);

        nextOrderId++;
    }

    function getOrders(address _player) external view override returns (Order[] memory) {
        if (currentUpgradeOrder[_player] == 0) {
            return new Order[](0);
        } else {
            Order[] memory order = new Order[](1);
            order[0] = upgradingOrders[currentUpgradeOrder[_player]].baseOrder;
            return order;
        }
    }

    function speedUpCooldown(uint256 _numSpeedUps) external {
        if (cooldownEndTimestamp[msg.sender] < block.timestamp) 
            revert FacilityUpgrade__CooldownAlreadyFinished();

        uint256 totalCost = _numSpeedUps * cooldownSpeedupCost;
        require(
            IERC20(pixt).transferFrom(msg.sender, address(this), totalCost),
            "Transfer of funds failed"
        ); 
        Burnable(pixt).burn(totalCost * pixtSpeedupSplitBps / pixtSpeedupSplitBpsDenominator);

        cooldownEndTimestamp[msg.sender] -= cooldownSpeedupTime * _numSpeedUps;

        emit SpeedUpCooldown(msg.sender, _numSpeedUps, cooldownEndTimestamp[msg.sender]);
    }

    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external override onlyOracle {
        FacilityUpgradeOrder storage upgradeOrder = upgradingOrders[orderForRequest[requestId]];

        uint256 upgradedFacilityId = upgradeToAssetId[upgradeOrder.facilityTokenId];

        uint256 rnd = randomWords[0] % doubleUpgradeChanceDenominator;
        if (rnd < upgradeOrder.doubleUpgradeChance) {
            upgradedFacilityId = upgradeToAssetId[upgradedFacilityId];
        }

        assetManager.trustedMint(upgradeOrder.player, upgradedFacilityId, 1);

        emit FacilityUpgradeFinished(upgradeOrder.player, upgradeOrder.baseOrder.orderId, upgradedFacilityId);
    }

    function claimOrder(uint256 _orderId) public override {
        if (currentUpgradeOrder[msg.sender] != _orderId || _orderId == 0) 
            revert FacilityUpgrade__InvalidOrderId(_orderId);

        FacilityUpgradeOrder storage order = upgradingOrders[currentUpgradeOrder[msg.sender]];

        if (!_isFinished(order.baseOrder))
            revert FacilityUpgrade__OrderNotFinished();

        if (order.vrfRequestId != 0)
            revert FacilityUpgrade__AlreadyClaimed();

        uint256 reqId = vrfManagerV2.getRandomNumber(1);
        orderForRequest[reqId] = _orderId;
        order.vrfRequestId = reqId;

        currentUpgradeOrder[msg.sender] = 0;

        cooldownEndTimestamp[msg.sender] = 
            block.timestamp + cooldownTimePerLevel * assetLevel[order.facilityTokenId];

        emit ClaimOrder(msg.sender, _orderId, reqId, cooldownEndTimestamp[msg.sender]);
    }

    function claimBatchOrder() external override {
        if (currentUpgradeOrder[msg.sender] == 0)
            revert FacilityUpgrade__NoOrders();

        claimOrder(currentUpgradeOrder[msg.sender]);
    }

    function speedUpOrder(uint256 _numSpeedUps, uint256 _orderId) external override {
        IXTSpeedUpOrder(_numSpeedUps, _orderId);
    }

    function IXTSpeedUpOrder(uint256 _numSpeedUps, uint256 _orderId) public override {
        if (currentUpgradeOrder[msg.sender] != _orderId || _orderId == 0) 
            revert FacilityUpgrade__InvalidOrderId(_orderId);
        
        Order storage order = upgradingOrders[currentUpgradeOrder[msg.sender]].baseOrder;

        if (_isFinished(order))
            revert FacilityUpgrade__NoSpeedUpAvailable();

        order.speedUpDeductedAmount += speedupTime * _numSpeedUps;
        order.totalCompletionTime = 
            upgradingTime > order.speedUpDeductedAmount ? upgradingTime - order.speedUpDeductedAmount : 0;

        uint256 totalCost = pixtSpeedupCost * _numSpeedUps;

        require(
            IERC20(pixt).transferFrom(msg.sender, address(this), totalCost),
            "Transfer of funds failed"
        );
        Burnable(pixt).burn(totalCost * pixtSpeedupSplitBps / pixtSpeedupSplitBpsDenominator);

        emit SpeedUpOrder(msg.sender, _orderId, _numSpeedUps);
    }

    function _isFinished(Order memory order) internal view returns (bool) {
        return (order.createdAt + order.totalCompletionTime) < block.timestamp;
    }

    function calcExponentDoubleUpgradeChance(
        uint256 _maxEnergy, 
        uint256 _maxChanceParam,
        uint256 _step, 
        uint256 _expCParam
    ) external onlyOwner {

        bytes16 ONE = ABDKMathQuad.fromUInt(1);
        bytes16 STEP = ABDKMathQuad.fromUInt(_step);
        bytes16 MAX_CHANCE_PARAM = ABDKMathQuad.fromUInt(_maxChanceParam);

        // expc = - _expCParam/1000
        bytes16 EXPC = 
            ABDKMathQuad.neg(
                ABDKMathQuad.div(
                    ABDKMathQuad.fromUInt(_expCParam), 
                    ABDKMathQuad.fromUInt(1000)
                ));

        // chance = maxChanceParam * (1 - e^(expc * additionalEnergy/step))
        for(uint256 additionalEnergy = _step; additionalEnergy <= _maxEnergy; additionalEnergy += _step) {

            // chanceMul = (1 - e^(expc * additionalEnergy/step))
            bytes16 chanceMul = 
                ABDKMathQuad.sub(
                    ONE,
                    ABDKMathQuad.exp(
                        ABDKMathQuad.mul(
                            EXPC,
                            ABDKMathQuad.div(
                                ABDKMathQuad.fromUInt(additionalEnergy),
                                STEP
                            )
                        )
                    )
                );

            // res = maxChanceParam * chanceMul
            int256 res = ABDKMathQuad.toInt(
                ABDKMathQuad.mul(
                    MAX_CHANCE_PARAM,
                    chanceMul
                )
            );
            
            doubleUpgradeChanceForEnergy[additionalEnergy] = uint256(res);
        }
    }
}