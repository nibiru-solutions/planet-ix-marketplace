//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {PIXBaseGovernance} from "./PIXBaseGovernance.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";

contract PIXGovernance_ERC1155 is PIXBaseGovernance, ERC1155Holder {
    IERC1155 public pixAssets;
    uint256 public tokenID;

    function initialize(
        address _pixAssets,
        uint256 _tokenID,
        uint256 _period,
        uint256 _lockPeriod
    ) public initializer {
        _initializeBase(_period, _lockPeriod);
        require(_pixAssets != address(0), "GOV: INVALID_PIXTOKEN");

        pixAssets = IERC1155(_pixAssets);
        tokenID = _tokenID;
    }

    function _baseTokenTransfer(
        address _from,
        address _to,
        uint256 _amount
    ) internal override {
        pixAssets.safeTransferFrom(_from, _to, tokenID, _amount, "");
    }

    function canCreateProposal(address _user)
        internal
        view
        override
        returns (bool)
    {
        uint256 balance = pixAssets.balanceOf(_user, tokenID);
        return balance >= 1;
    }
}
