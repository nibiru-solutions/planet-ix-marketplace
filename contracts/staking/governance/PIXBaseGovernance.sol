//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

abstract contract PIXBaseGovernance is OwnableUpgradeable, PausableUpgradeable {
    using SafeERC20Upgradeable for IERC20Upgradeable;

    ////////////////////////////////////////////////////////////////////////////
    // STRUCT
    ////////////////////////////////////////////////////////////////////////////

    enum Status {
        InProduction,
        Active,
        Completed,
        Closed
    }

    struct Proposal {
        string title;
        string description;
        bool isModerator;
        bool isVoted;
        uint8 result;
        uint256 createdTime;
        uint256 power1;
        uint256 power2;
        uint256 replies;
        Status status;
    }

    uint256 public lockPeriod;

    ////////////////////////////////////////////////////////////////////////////
    // STATE
    ////////////////////////////////////////////////////////////////////////////

    /// proposal period
    uint256 public period;

    uint256 public pid;
    mapping(uint256 => Proposal) public proposals;
    mapping(address => bool) public moderators;

    mapping(address => uint256) public votingPower;
    mapping(address => uint256) public delegatedPower;
    mapping(address => uint256) public votingPowerStaked;
    mapping(address => uint256) public delegatedPowerStaked;
    mapping(address => uint256) public voterStakedAt;
    mapping(address => mapping(address => uint256)) public delegatedAmount;

    ////////////////////////////////////////////////////////////////////////////
    // EVENTS
    ////////////////////////////////////////////////////////////////////////////

    event ProposalCreated(uint256 indexed pid, address indexed user);
    event ProposalClosed(uint256 pid);
    event ProposalCompleted(uint256 pid, uint8 result);
    event Staked(address user, uint256 amount);
    event Voted(
        uint256 indexed pid,
        address indexed user,
        uint256 amount,
        bool status
    );
    event PowerDelegated(
        address indexed delegator,
        address indexed user,
        uint256 amount
    );
    event DelegateRemoved(address user, address delegate);
    event VotingPowerWithdrawn(address user);
    event Unstaked(address user, uint256 amount);

    ////////////////////////////////////////////////////////////////////////////
    // INITIALIZE
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @notice initialize the governance
     * @param _period proposal period
     * @param _lockPeriod lock period
     */
    function _initializeBase(uint256 _period, uint256 _lockPeriod)
        public
        initializer
    {
        period = _period;
        lockPeriod = _lockPeriod;
        require(period < lockPeriod, "GOV: INVALID_PERIODS");

        __Ownable_init();
    }

    ////////////////////////////////////////////////////////////////////////////
    /// ADMIN
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @notice pause contract by the owner
     */
    function pause() external onlyOwner whenNotPaused {
        return _pause();
    }

    /**
     * @notice unpause contract by the owner
     */
    function unpause() external onlyOwner whenPaused {
        return _unpause();
    }

    /**
     * @notice add/remove moderator
     * @param _moderator moderator address
     * @param _approved add/remove
     */
    function setModerator(address _moderator, bool _approved)
        external
        onlyOwner
        whenNotPaused
    {
        require(_moderator != address(0), "GOV: INVALID_MODERATOR");
        moderators[_moderator] = _approved;
    }

    /**
     * @notice shut down proposal
     * @param _pid proposal index
     */
    function closeProposal(uint256 _pid) external onlyOwner whenNotPaused {
        require(pid > _pid, "GOV: INVALID_PID");

        Proposal storage proposal = proposals[_pid];
        proposal.status = Status.Closed;

        emit ProposalClosed(_pid);
    }

    ////////////////////////////////////////////////////////////////////////////
    // PUBLIC
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @notice create a new proposal
     * @param _title title text
     * @param _description description text
     */
    function createProposal(
        string calldata _title,
        string calldata _description
    ) external whenNotPaused {
        require(canCreateProposal(msg.sender), "GOV: INSUFFICIENT_BALANCE");
        require(bytes(_title).length > 0, "GOV: INVALID_TITLE");
        require(bytes(_description).length > 0, "GOV: INVALID_DESCRIPTION");

        Proposal storage proposal = proposals[pid];
        if (moderators[msg.sender]) {
            proposal.isModerator = true;
        }
        proposal.title = _title;
        proposal.description = _description;
        proposal.createdTime = block.timestamp;

        emit ProposalCreated(pid, msg.sender);
        pid += 1;
    }

    /**
     * @notice complete proposal after it's ended
     * @param _pid proposal index
     */
    function completeProposal(uint256 _pid) external whenNotPaused {
        require(pid > _pid, "GOV: INVALID_PID");

        Proposal storage proposal = proposals[_pid];
        require(
            block.timestamp >= proposal.createdTime + period,
            "GOV: PERIOD_NOT_ENDED"
        );
        require(proposal.status == Status.Active, "GOV: NOT_ACTIVEproposal");

        proposal.status = Status.Completed;

        if (proposal.power1 > proposal.power2) {
            proposal.result = 1;
        } else {
            proposal.result = 2;
        }

        emit ProposalCompleted(_pid, proposal.result);
    }

    /**
     * @notice stakes tokens
     * @param _amount staking amount
     */
    function stake(uint256 _amount) external whenNotPaused {
        _baseTokenTransfer(msg.sender, address(this), _amount);
        votingPower[msg.sender] += _amount;

        emit Staked(msg.sender, _amount);
    }

    /**
     * @notice vote into the proposal
     * @param _pid proposal index
     * @param _status yes/no
     */
    function vote(uint256 _pid, bool _status) external whenNotPaused {
        require(pid > _pid, "GOV: INVALID_PID");

        Proposal storage proposal = proposals[_pid];
        require(
            proposal.createdTime + period >= block.timestamp,
            "GOV: PROPOSAL_ENDED"
        );

        if (proposal.isVoted) {
            proposal.replies += 1;
        }

        uint256 voteAmount = votingPower[msg.sender] +
            delegatedPower[msg.sender];

        if (_status) {
            proposal.power1 += voteAmount;
        } else {
            proposal.power2 += voteAmount;
        }
        proposal.status = Status.Active;
        proposal.isVoted = true;

        votingPowerStaked[msg.sender] += votingPower[msg.sender];
        votingPower[msg.sender] = 0;
        delegatedPowerStaked[msg.sender] += delegatedPower[msg.sender];
        delegatedPower[msg.sender] = 0;
        voterStakedAt[msg.sender] = block.timestamp;

        emit Voted(_pid, msg.sender, voteAmount, _status);
    }

    /**
     * @notice delegate voting power
     * @param _user delegate address to take power
     * @param _amount voting power (PIX token amount)
     */
    function delegatePower(address _user, uint256 _amount)
        external
        whenNotPaused
    {
        require(_user != address(0), "GOV: INVALID_USER");
        require(votingPower[msg.sender] >= _amount, "GOV: INVALID_AMOUNT");

        votingPower[msg.sender] -= _amount;
        delegatedPower[_user] += _amount;
        delegatedAmount[msg.sender][_user] += _amount;

        emit PowerDelegated(msg.sender, _user, _amount);
    }

    /**
     * @notice remove delegate
     * @param _user delegate
     */
    function removeDelegate(address _user) external {
        uint256 amountDelegated = delegatedAmount[msg.sender][_user];
        if (delegatedPower[_user] < amountDelegated) {
            uint256 stakedAt = voterStakedAt[_user];
            require(stakedAt + lockPeriod <= block.timestamp, "GOV: LOCKED");

            delegatedPowerStaked[_user] -= amountDelegated;
        } else {
            delegatedPower[_user] -= amountDelegated;
        }
        votingPower[msg.sender] += amountDelegated;
        delegatedAmount[msg.sender][_user] = 0;

        emit DelegateRemoved(msg.sender, _user);
    }

    /**
     * @notice withdraw voting power after lock period
     */
    function withdrawVotingPower() external {
        uint256 stakedAt = voterStakedAt[msg.sender];
        require(stakedAt + lockPeriod <= block.timestamp, "GOV: LOCKED");

        votingPower[msg.sender] += votingPowerStaked[msg.sender];
        votingPowerStaked[msg.sender] = 0;
        delegatedPower[msg.sender] += delegatedPowerStaked[msg.sender];
        delegatedPowerStaked[msg.sender] = 0;

        emit VotingPowerWithdrawn(msg.sender);
    }

    /**
     * @notice unstake PIX tokens
     * @param _amount amount
     */
    function unstake(uint256 _amount) external {
        require(votingPower[msg.sender] >= _amount, "GOV: INVALID_AMOUNT");
        votingPower[msg.sender] -= _amount;
        _baseTokenTransfer(address(this), msg.sender, _amount);

        emit Unstaked(msg.sender, _amount);
    }

    function _baseTokenTransfer(
        address _from,
        address _to,
        uint256 _amount
    ) internal virtual;

    function canCreateProposal(address _user)
        internal
        view
        virtual
        returns (bool);
}
