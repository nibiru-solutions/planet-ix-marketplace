//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {PIXBaseGovernance} from "./PIXBaseGovernance.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract PIXGovernance_ERC20 is PIXBaseGovernance {
    using SafeERC20Upgradeable for IERC20Upgradeable;

    IERC20Upgradeable public pixToken;

    function initialize(
        address _pixToken,
        uint256 _period,
        uint256 _lockPeriod
    ) public initializer {
        _initializeBase(_period, _lockPeriod);
        require(_pixToken != address(0), "GOV: INVALID_PIXTOKEN");

        pixToken = IERC20Upgradeable(_pixToken);
    }

    function _baseTokenTransfer(
        address _from,
        address _to,
        uint256 _amount
    ) internal override {
        if (_from == address(this)) {
            pixToken.safeTransfer(_to, _amount);
        } else {
            pixToken.safeTransferFrom(_from, _to, _amount);
        }
    }

    function canCreateProposal(address _user)
        internal
        view
        override
        returns (bool)
    {
        uint256 balance = pixToken.balanceOf(_user);
        return balance >= 1e21;
    }
}
