//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/IEnergy.sol";
import "../interfaces/IM3taCrystalIXTStaking.sol";
import "../mission_control/IAssetManager.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";

contract M3taCrystalIXTStaking is IM3taCrystalIXTStaking, OwnableUpgradeable, ReentrancyGuardUpgradeable {
    struct Stake {
        uint256 amount;
        uint256 stakedAt;
    }
    /// @notice latest epoch
    uint256 public s_epochCount;

    /// @notice wallet from which rewards will be added
    address private s_rewardDistributor;

    /// @notice IXT contract
    IERC20Upgradeable public pixt;
    /// @notice Asset Manager Contract
    IEnergy public asset_manager;

    /// @notice Amount a user currently has staked
    mapping(address => uint256) public userStakedAmount;

    /// @notice moderators
    mapping(address => bool) public moderators;

    bool public paused;

    mapping(address => Stake[]) public userIXTStakes;
    mapping(address => uint256) public userTotalIXTStaked;

    mapping(address => mapping(uint256 => uint256)) public userCrystalStaked;

    mapping(uint256 => RewardPool) public rewardPool;

    uint256 private constant M3TA_CRYSTAL_ID = 77;
    uint256 private constant LOCKUP_PERIOD = 30 days;

    /*------------------- MODIFIERS -------------------*/

    /// @notice allows calls from only reward distributor
    modifier onlyRewardDistributor() {
        require(msg.sender == s_rewardDistributor, "M3TA CRYSTAL STAKING: NON_DISTRIBUTOR");
        _;
    }

    /// @notice allows calls from only moderators
    modifier onlyGov() {
        require(msg.sender == owner() || moderators[msg.sender], "M3TA CRYSTAL STAKING: NON_MODERATOR");
        _;
    }

    ///@notice allows function calls only when not paused
    modifier whenNotPaused() {
        require(!paused, "M3TA CRYSTAL STAKING: FEATURE PAUSED");
        _;
    }

    /*------------------- INITIALIZER -------------------*/

    function initialize(IERC20Upgradeable _pixt, IEnergy _asset_manager) public initializer {
        __Ownable_init();
        __ReentrancyGuard_init();

        pixt = _pixt;
        asset_manager = _asset_manager;
    }

    /*---------------- END - USER FUNCTIONS ----------------*/

    /// @inheritdoc IM3taCrystalIXTStaking
    function stakeIXT(uint256 _amount) external nonReentrant whenNotPaused {
        require(_amount > 0, "M3TA CRYSTAL STAKING: CANT STAKE 0");
        userIXTStakes[msg.sender].push(Stake({amount: _amount, stakedAt: block.timestamp}));
        userTotalIXTStaked[msg.sender] += _amount;
        pixt.transferFrom(msg.sender, address(this), _amount * 1 ether);
        emit IXTStaked(msg.sender, _amount);
    }

    /// @inheritdoc IM3taCrystalIXTStaking
    function unstakeIXT(uint256 _amount) external nonReentrant {
        require(_amount > 0, "M3TA CRYSTAL STAKING: CANT UNSTAKE 0");
        require(userTotalIXTStaked[msg.sender] - userCrystalStaked[msg.sender][s_epochCount] >= _amount, "M3TA CRYSTAL STAKING: INSUFFICIENT AMOUNT STAKED");
        _updateStake(msg.sender, _amount);
        pixt.transfer(msg.sender, _amount * 1 ether);
        emit IXTUnstaked(msg.sender, _amount);
    }

    /// @inheritdoc IM3taCrystalIXTStaking
    function stakeCrystal(uint256 _amount) external nonReentrant {
        uint256 totalIXTStaked = getUserTotalIXTStaked(msg.sender);
        uint256 totalCrystalStaked = getUserTotalCrystalStaked(msg.sender);

        require(_amount > 0, "M3TA CRYSTAL STAKING: CANT STAKE 0");
        require(totalCrystalStaked + _amount <= totalIXTStaked, "M3TA CRYSTAL STAKING: NEED TO STAKE MORE IXT");

        _updateRewards(msg.sender, s_epochCount);
        rewardPool[s_epochCount].tokensStaked += _amount;

        userCrystalStaked[msg.sender][s_epochCount] += _amount;

        asset_manager.trustedBurn(msg.sender, M3TA_CRYSTAL_ID, _amount);
        emit M3taCrystalStaked(msg.sender, _amount);
    }

    /// @inheritdoc IM3taCrystalIXTStaking
    function claim() external nonReentrant {
        for (uint256 i = 1; i <= s_epochCount; i++) _claim(i);
    }

    function _claim(uint256 _epoch) internal {
        _updateRewards(msg.sender, _epoch);
        _proportionAndTransferRewards(_epoch);
    }

    /*---------------- STAKING UTILS ----------------*/

    function _updateStake(address _user, uint256 _amount) internal {
        Stake[] memory userStaked = userIXTStakes[_user];

        uint256 remaining = _amount;
        for (uint256 i; i < userStaked.length; i++) {
            if (block.timestamp > (userStaked[i].stakedAt + LOCKUP_PERIOD)) {
                if (remaining >= userStaked[i].amount) {
                    remaining -= userStaked[i].amount;
                    _removeFromArray(0, userIXTStakes[_user]);
                } else {
                    userIXTStakes[_user][0].amount -= remaining;
                    remaining = 0;
                }
            }
        }
        userTotalIXTStaked[_user] -= _amount;
        require(remaining == 0, "M3TA CRYSTAL STAKING: NOT_ENOUGH_TOKENS_UNLOCKED");
    }

    function _removeFromArray(uint256 _position, Stake[] storage _arr) internal {
        for (uint256 i = _position; i < _arr.length - 1; i++) {
            _arr[i] = _arr[i + 1];
        }
        _arr.pop();
    }

    function _proportionAndTransferRewards(uint256 _epoch) internal {
        uint256 rewardToPay;

        rewardToPay = rewardPool[_epoch].rewards[msg.sender];
        rewardPool[_epoch].rewards[msg.sender] = 0;
        
        pixt.transfer(msg.sender, rewardToPay);
    }

    /**
     * @notice Updates rewards within pool based on the number of IXT staked
     * @param _walletAddress address of a user whose rewards we wish to update
     */
    function _updateRewards(address _walletAddress, uint256 _epoch) internal {
        rewardPool[_epoch].rewardPerTokenStored = rewardPerTokenStored(_epoch);
        rewardPool[_epoch].lastUpdateTime = lastUpdateTimeApplicable(block.timestamp, _epoch);

        if (_walletAddress != address(0)) {
            rewardPool[_epoch].rewards[_walletAddress] = earned(_walletAddress, _epoch);
            rewardPool[_epoch].userRewardPerTokenPaid[_walletAddress] = rewardPool[_epoch].rewardPerTokenStored;
        }
    }

    /// @inheritdoc IM3taCrystalIXTStaking
    function earned(address _walletAddress, uint256 _epoch) public view returns (uint256) {
        return (userCrystalStaked[_walletAddress][_epoch] * (rewardPerTokenStored(_epoch) - rewardPool[_epoch].userRewardPerTokenPaid[_walletAddress])) + rewardPool[_epoch].rewards[_walletAddress];
    }


    /// @notice Finds the reward per token based on the number of staked tokens and the time that has passed
    function rewardPerTokenStored(uint256 _epoch) public view returns (uint256) {
        if (rewardPool[_epoch].tokensStaked == 0) {
            return rewardPool[_epoch].rewardPerTokenStored;
        }

        uint256 startTime = rewardPool[_epoch].lastUpdateTime;
        uint256 untilTime = lastUpdateTimeApplicable(block.timestamp, _epoch);
        uint256 reward = rewardPool[_epoch].rewardRate * (untilTime - startTime);

        return rewardPool[_epoch].rewardPerTokenStored + (reward / rewardPool[_epoch].tokensStaked);
    }

    /// @notice Finds the last update time that was applicable, does not allow it to go beyond the epoch finish time
    function lastUpdateTimeApplicable(uint256 _untilTimeStamp, uint256 _epoch) public view returns (uint256) {
        uint256 effectiveTimeStamp = _untilTimeStamp < rewardPool[_epoch].periodFinish ? _untilTimeStamp : rewardPool[_epoch].periodFinish;

        return effectiveTimeStamp;
    }

    /*---------------- ADMIN FUNCTIONS ----------------*/

    /// @inheritdoc IM3taCrystalIXTStaking
    function addCurrentEpochRewards(uint256 _poolTotalReward, uint256 _epochDuration) external onlyRewardDistributor {
        require(rewardPool[s_epochCount].periodFinish < block.timestamp, "M3TA CRYSTAL STAKING: EPOCH NOT YET FINISHED");

        unchecked {
            ++s_epochCount;
        }

        _updateRewards(address(0), s_epochCount);

        rewardPool[s_epochCount].rewardRate = _poolTotalReward / _epochDuration;

        rewardPool[s_epochCount].lastUpdateTime = block.timestamp;
        rewardPool[s_epochCount].periodFinish = block.timestamp + _epochDuration;

        pixt.transferFrom(s_rewardDistributor, address(this), _poolTotalReward);

        emit RewardAdded(_poolTotalReward, _epochDuration);
    }

    /// @inheritdoc IM3taCrystalIXTStaking
    function setRewardDistributor(address _distributor) external onlyGov {
        require(_distributor != address(0), "M3TA CRYSTAL STAKING: ZERO ADDRESS");
        s_rewardDistributor = _distributor;
    }

    /**
     * @notice sets or disapproves someone's moderator status
     * @param moderator moderator
     * @param approved whether or not they are approved
     */
    function setModerator(address moderator, bool approved) external onlyOwner {
        require(moderator != address(0), "M3TA CRYSTAL STAKING: ZERO ADDRESS");
        moderators[moderator] = approved;
    }

    /**
     * @notice sets asset_manager contract
     * @param _asset_manager Asset Manager Contract
     */
    function setAssetManager(IEnergy _asset_manager) external onlyGov {
        asset_manager = _asset_manager;
    }

    function setIXT(IERC20Upgradeable _pixt) external onlyGov {
        pixt = _pixt;
    }

    function setPaused(bool _paused) external onlyGov {
        paused = _paused;
    }

    /*---------------- VIEW FUNCTIONS ----------------*/

    /// @inheritdoc IM3taCrystalIXTStaking
    function getUserStakedAmount(address _walletAddress) external view returns (uint256) {
        return userStakedAmount[_walletAddress];
    }

    /// @notice Gets total reward per token
    function getRewardPerToken() external view returns (uint256) {
        return rewardPool[s_epochCount].rewardPerTokenStored;
    }

    /// @notice Gets current reward rate
    function getCurrentRewardRate() external view returns (uint256) {
        return rewardPool[s_epochCount].rewardRate;
    }

    /// @notice Gets total staked amount
    function getTotalStakedAmount() external view returns (uint256) {
        return rewardPool[s_epochCount].tokensStaked;
    }

    /// @notice Gets current epoch end time
    function getCurrentEpochEndTime() external view returns (uint256) {
        return rewardPool[s_epochCount].periodFinish;
    }

    /// @notice Gets last update time
    function getLastUpdateTime() external view returns (uint256) {
        return rewardPool[s_epochCount].lastUpdateTime;
    }

    /// @notice Gets user reward per token paid
    function getUserRewardPerTokenPaid(address _walletAddress) external view returns (uint256) {
        return rewardPool[s_epochCount].userRewardPerTokenPaid[_walletAddress];
    }

    function getUserIXTStakes(address _user) external view returns (Stake[] memory) {
        return userIXTStakes[_user];
    }

    function getUserTotalIXTStaked(address _user) public view returns (uint256) {
        return userStakedAmount[_user] + userTotalIXTStaked[_user];
    }

    function getUserTotalCrystalStaked(address _user) public view returns (uint256) {
        return userCrystalStaked[_user][s_epochCount];
    }

    function getUserCrystalStaked(address _user) public view returns (uint256) {
        return userCrystalStaked[_user][s_epochCount];
    }
}
