//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "../interfaces/IPIXCatRaffV2.sol";
import "../mission_control/IAssetManager.sol";

interface IVRFManager {
    function getRandomNumber(uint32 numWords) external returns (uint256 requestId);
}

interface IRNGConsumer {
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] calldata randomWords) external;
}

contract CrystalPIXCatRaff is IPIXCatRaffV2, IRNGConsumer, OwnableUpgradeable, PausableUpgradeable {
    using SafeERC20 for IERC20;

    /* ========== DATA ========== */
    struct WeeklyTicketInfo {
        uint256 prize;
        uint256 totalTickets;
        address[] players;
        uint256[] sum;
        mapping(address => uint256) playerTickets;
    }

    /* ========== STATE VARIABLES ========== */

    IERC20 public pixToken;

    address public treasury;
    address public orderAutomation;
    uint256 public weeklyPrize;
    uint256 public firstWeek;

    mapping(uint256 => address[]) public weeklyWinners;
    mapping(uint256 => uint256) public drawRequests;
    mapping(uint256 => WeeklyTicketInfo) public weeklyTickets;
    mapping(address => uint256) public prizes;

    mapping(uint256 => bool) private _drawn;

    uint256 public constant SECONDS_PER_WEEK = 1 weeks;
    uint8[] public poolPercent;

    IVRFManager public vrfManager;
    uint256 public constant CRYSTAL_ID = 77;
    address pixAssets;
    uint256 rewardReceiversCount;

    modifier onlyOracle() {
        if (msg.sender != address(vrfManager)) revert PIXCatRaffV2__NotOracle();
        _;
    }

    function initialize(
        address _pixToken,
        address _pixAssets,
        address _vrfManager,
        address _treasury,
        uint256 _weeklyPrize
    ) public initializer {
        __Ownable_init();
        __Pausable_init();
        require(_pixToken != address(0), "invalid token");
        require(_treasury != address(0), "invalid treasury");
        require(_weeklyPrize > 0, "invalid weekly prize");

        pixToken = IERC20(_pixToken);
        pixAssets = _pixAssets;
        treasury = _treasury;
        weeklyPrize = _weeklyPrize;

        vrfManager = IVRFManager(_vrfManager);

        firstWeek = block.timestamp / SECONDS_PER_WEEK;
        poolPercent = [20, 18, 16, 14, 12, 4, 4, 4, 4, 4];
        rewardReceiversCount = 10;
    }

    /* ========== MUTATIVE FUNCTIONS ========== */

    function addTickets(uint256 _tickets) external whenNotPaused {
        require(_tickets > 0, "tickets must be > 0");

        uint256 week = getCurrentWeek();
        require(!_drawn[week], "already drawn");

        WeeklyTicketInfo storage info = weeklyTickets[week];
        info.playerTickets[msg.sender] += _tickets;
        info.totalTickets += _tickets;
        info.players.push(msg.sender);
        info.sum.push(info.totalTickets);

        IAssetManager(pixAssets).trustedBurn(msg.sender, CRYSTAL_ID, _tickets);

        emit TicketsAdded(week, msg.sender, _tickets, block.timestamp);
    }

    function claimPrize() external {
        uint256 prize = prizes[msg.sender];
        require(prize > 0, "no prize");

        prizes[msg.sender] = 0;
        pixToken.safeTransferFrom(treasury, msg.sender, prize);

        emit PrizeClaimed(msg.sender, prize);
    }

    /* ========== RESTRICTED FUNCTIONS ========== */

    function setPoolPercent(uint8[] memory _poolPercent) external onlyOwner {
        require(_poolPercent.length > 0 && _poolPercent.length <= 50, "invalid length");
        uint256 totalPercent = 0;
        for (uint256 i = 0; i < _poolPercent.length; i++) {
            totalPercent += _poolPercent[i];
        }
        for (uint i = _poolPercent.length; i < rewardReceiversCount; i++) {
            poolPercent[i] = 0; 
        }
        require(totalPercent == 100, "invalid sum");

        poolPercent = _poolPercent;
        rewardReceiversCount = _poolPercent.length;
    }

    function addPrize(uint256 _prize) external onlyOwner whenNotPaused {
        uint256 week = getCurrentWeek();
        require(!_drawn[week], "already drawn");

        WeeklyTicketInfo storage info = weeklyTickets[week];
        info.prize += _prize;

        emit PrizeAdded(week, _prize, block.timestamp);
    }

    function draw(uint256 _week) external override whenNotPaused {
        require(msg.sender == orderAutomation, "invalid order automation");
        require((_week + firstWeek + 1) * SECONDS_PER_WEEK < block.timestamp, "invalid week");

        WeeklyTicketInfo storage info = weeklyTickets[_week];
        require(info.totalTickets > 0, "no tickets");

        require(!_drawn[_week], "draw already requested");
        _drawn[_week] = true;

        uint256 requestID = getRandomNumber();
        drawRequests[requestID] = _week;

        emit DrawRequested(requestID, _week, info.totalTickets, info.prize + weeklyPrize);
    }

    function setTreasury(address _treasury) public onlyOwner {
        require(_treasury != address(0), "invalid treasury");
        treasury = _treasury;
    }

    function setVRFManager(address _vrfManager) public onlyOwner {
        require(_vrfManager != address(0), "invalid vrfManager");
        vrfManager = IVRFManager(_vrfManager);
    }

    function setOrderAutomation(address _orderAutomation) external onlyOwner {
        require(_orderAutomation != address(0), "invalid order automation");
        orderAutomation = _orderAutomation;
    }

    function setWeeklyPrize(uint256 _weeklyPrize) external onlyOwner {
        require(_weeklyPrize > 0, "invalid weekly prize");
        weeklyPrize = _weeklyPrize;
    }

    function pause() external onlyOwner whenNotPaused {
        _pause();
    }

    function unpause() external onlyOwner whenPaused {
        _unpause();
    }

    /* ========== INTERNALS ========== */

    /**
     * @notice Request random words
     */
    function getRandomNumber() internal returns (uint256 requestID) {
        requestID = vrfManager.getRandomNumber(1);
    }

    /**
     * @notice Callback function used by ChainLink's VRF v2.5 Coordinator
     */
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] calldata randomWords) external override onlyOracle {
        uint256 week = drawRequests[requestId];
        WeeklyTicketInfo storage info = weeklyTickets[week];

        uint8 i;
        uint256 left;
        uint256 right;
        uint256 mid;
        uint256 rand;

        for (i = 0; i < rewardReceiversCount; i++) {
            rand = uint256(keccak256(abi.encode(randomWords[0], i))) % info.totalTickets;
            left = 0;
            right = info.players.length - 1;
            while (left < right) {
                mid = (left + right) / 2;
                if (rand >= info.sum[mid]) {
                    left = mid + 1;
                } else if (rand < info.sum[mid]) {
                    right = mid;
                }
            }

            address winner = info.players[left];
            weeklyWinners[week].push(winner);
            prizes[winner] += ((info.prize + weeklyPrize) * poolPercent[i]) / 100;
        }

        emit ResultDrawn(week);
    }

    /* ========== VIEWS ========== */

    function getCurrentWeek() public view override returns (uint256) {
        uint256 currentWeek = block.timestamp / SECONDS_PER_WEEK - firstWeek;
        return currentWeek;
    }

    function winnings(uint256 _week) external view returns (address[] memory, uint256[] memory) {
        WeeklyTicketInfo storage info = weeklyTickets[_week];
        uint256[] memory weeklyPrizes = new uint256[](weeklyWinners[_week].length);
        for (uint256 i; i < weeklyWinners[_week].length; i++) {
            weeklyPrizes[i] = ((info.prize + weeklyPrize) * poolPercent[i]) / 100;
        }
        return (weeklyWinners[_week], weeklyPrizes);
    }

    function drawable(uint256 _week) external view override returns (bool) {
        if (paused()) {
            return false;
        }
        if (_drawn[_week]) {
            return false;
        }
        if ((_week + firstWeek + 1) * SECONDS_PER_WEEK >= block.timestamp) {
            return false;
        }

        WeeklyTicketInfo storage info = weeklyTickets[_week];
        if (info.totalTickets == 0) {
            return false;
        }

        return true;
    }

    function ticketsOf(uint256 _week, address _player) external view returns (uint256) {
        return weeklyTickets[_week].playerTickets[_player];
    }

    function ticketsOf(address _player) external view returns (uint256) {
        uint256 week = getCurrentWeek();
        return weeklyTickets[week].playerTickets[_player];
    }

    function pastTicketsOf(address _player) external view returns (uint256[] memory) {
        uint256 week = getCurrentWeek() + 1;
        uint256[] memory pastTickets = new uint256[](week);

        for (uint256 i = 0; i < week; i++) {
            pastTickets[i] = weeklyTickets[i].playerTickets[_player];
        }

        return pastTickets;
    }
}
