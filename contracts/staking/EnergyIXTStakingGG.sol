//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/IEnergy.sol";
import "../gravity_grade/IGravityGrade.sol";
import "../interfaces/IEnergyIXTStakingGG.sol";
import "abdk-libraries-solidity/ABDKMathQuad.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";

/**
 * @title EnergyIXTStakingGG
 * @author Jourdan
 * @notice This is a contract for staking energy and IXT for Gravity Grade rewards
 */
contract EnergyIXTStakingGG is IEnergyIXTStakingGG, OwnableUpgradeable, ReentrancyGuardUpgradeable {
    /// @notice latest reward ID
    uint256 public s_rewardId;

    /// @notice Gravity Grade Contract
    IGravityGrade public gravityGrade;
    /// @notice IXT contract
    IERC20Upgradeable public pixt;
    /// @notice Energy contract (Asset Manager)
    IEnergy public energy;

    /// @notice gg reward specifications
    mapping(uint256 => GGRewards) public ggRewards;
    /// @notice array containing all rewards
    uint256[] public allRewards;
    /// @notice indices of rewards
    mapping(uint256 => uint256) public rewardIndices;

    /// @notice number of concurrent stakers
    mapping(uint256 => uint256) public concurrentStakerCount;
    /// @notice number of unique stakers
    mapping(uint256 => uint256) public uniqueStakerCount;
    /// @notice whether or not player has staked
    mapping(uint256 => mapping(address => bool)) public playerHasStaked;
    /// @notice time at which player staked
    mapping(uint256 => mapping(address => uint256)) public playerStakedAt;
    /// @notice whether or not player is currently staking
    mapping(uint256 => mapping(address => bool)) public playerCurrentlyStaking;

    /// @notice reward ids user is staking for
    mapping(address => uint256[]) private userStaked;
    /// @notice indices of reward ids user is staking for
    mapping(address => mapping(uint256 => uint256)) public userRewardIndices;

    /// @notice moderators
    mapping(address => bool) public moderators;

    modifier onlyMod() {
        require(moderators[msg.sender] || msg.sender == owner(), "STAKING: NON_MODERATOR");
        _;
    }

    function initialize(
        IGravityGrade _gravityGrade,
        IERC20Upgradeable _pixt,
        IEnergy _energy
    ) public initializer {
        __Ownable_init();
        __ReentrancyGuard_init();

        gravityGrade = _gravityGrade;
        pixt = _pixt;
        energy = _energy;
    }

    /*---------------- END - USER FUNCTIONS ----------------*/

    /// @inheritdoc IEnergyIXTStakingGG
    function stake(uint256 rewardId) external nonReentrant {
        GGRewards storage rewards = ggRewards[rewardId];

        require(!rewards.paused, "STAKING: REWARD PAUSED");
        require(!playerCurrentlyStaking[rewardId][msg.sender], "STAKING: USER CURRENTLY STAKING");
        require((concurrentStakerCount[rewardId] + 1) <= rewards.concurrentStakerCap, "STAKING: CAP REACHED");

        uint256 ixtCost = rewards.ixtCost;
        uint256 energyCost = rewards.energyCost;

        energy.trustedBurn(msg.sender, 24, energyCost);
        pixt.transferFrom(msg.sender, address(this), ixtCost * 1 ether);

        ++concurrentStakerCount[rewardId];

        if (!playerHasStaked[rewardId][msg.sender]) {
            require((uniqueStakerCount[rewardId] + 1) <= rewards.lifeTimePlayerCap, "STAKING: LIFETIME CAP REACHED");
            ++uniqueStakerCount[rewardId];
            playerHasStaked[rewardId][msg.sender] = true;
        }

        playerCurrentlyStaking[rewardId][msg.sender] = true;
        playerStakedAt[rewardId][msg.sender] = block.timestamp;

        userStaked[msg.sender].push(rewardId);
        userRewardIndices[msg.sender][rewardId] = userStaked[msg.sender].length - 1;

        emit EnergyStaked(msg.sender, rewardId);
    }

    /// @inheritdoc IEnergyIXTStakingGG
    function claim(uint256 rewardId) public nonReentrant {
        GGRewards storage rewards = ggRewards[rewardId];
        uint256 _playerStakedAt = playerStakedAt[rewardId][msg.sender];

        require(playerCurrentlyStaking[rewardId][msg.sender], "STAKING: PLAYER HAS NOT STAKED YET");
        require(block.timestamp > (_playerStakedAt + rewards.lockupTime), "STAKING: LOCKUP PERIOD NOT OVER");

        playerCurrentlyStaking[rewardId][msg.sender] = false;
        --concurrentStakerCount[rewardId];

        uint256 index = userRewardIndices[msg.sender][rewardId];
        for (uint256 i = index; i < userStaked[msg.sender].length - 1; i++) {
            userStaked[msg.sender][i] = userStaked[msg.sender][i + 1];
            userRewardIndices[msg.sender][userStaked[msg.sender][i]] = i;
        }
        userStaked[msg.sender].pop();

        uint256[] memory tokenIdArr = new uint256[](1);
        uint256[] memory amountArr = new uint256[](1);
        address[] memory recipientArr = new address[](1);

        tokenIdArr[0] = rewards.rewardTokenId;
        amountArr[0] = rewards.rewardTokenAmount;
        recipientArr[0] = msg.sender;

        pixt.transfer(msg.sender, rewards.ixtCost * 1 ether);
        gravityGrade.airdrop(recipientArr, tokenIdArr, amountArr);
    }

    /// @notice claims all rewards
    function claimAll() external {
        uint256 len = userStaked[msg.sender].length;
        uint256[] memory rewardIds = userStaked[msg.sender];
        for (uint256 i; i < len; i++) {
            claim(rewardIds[i]);
        }
    }

    /*---------------- ADMIN FUNCTIONS ----------------*/

    /**
     * @notice Adds gg rewards users can stake for
     * @notice emit {RewardAdded} event
     * @param _rewards param necessary for setting up rewards
     */
    function addReward(GGRewards memory _rewards) external onlyMod returns (uint256 _rewardId) {
        require(_rewards.rewardTokenAmount > 0, "STAKING: INVALID REWARD AMOUNT");
        require(_rewards.lockupTime > 0, "STAKING: LOCKUP TIME ZERO");
        require(_rewards.concurrentStakerCap > 0, "STAKING: STAKER CAP ZERO");
        require(_rewards.lifeTimePlayerCap > 0, "STAKING: LIFETIME PLAYER CAP ZERO");

        unchecked {
            _rewardId = ++s_rewardId;
        }

        ggRewards[s_rewardId] = GGRewards({
            rewardTokenId: _rewards.rewardTokenId,
            rewardTokenAmount: _rewards.rewardTokenAmount,
            energyCost: _rewards.energyCost,
            ixtCost: _rewards.ixtCost,
            lockupTime: _rewards.lockupTime,
            concurrentStakerCap: _rewards.concurrentStakerCap,
            lifeTimePlayerCap: _rewards.lifeTimePlayerCap,
            paused: _rewards.paused
        });
        allRewards.push(s_rewardId);

        emit RewardAdded(_rewards);
    }

    /// @notice sets moderator
    function setModerator(address _user, bool _isModerator) external onlyOwner {
        moderators[_user] = _isModerator;
    }

    /// @notice sets reward state
    function setRewardState(uint256 _rewardId, bool _paused) external onlyMod {
        ggRewards[_rewardId].paused = _paused;
    }

    /// @notice sets ixt price
    function setIXTPrice(uint256 _rewardId, uint256 _ixtCost) external onlyMod {
        ggRewards[_rewardId].ixtCost = _ixtCost;
    }

    /// @notice sets energy price
    function setEnergyPrice(uint256 _rewardId, uint256 _energyCost) external onlyMod {
        ggRewards[_rewardId].energyCost = _energyCost;
    }

    /// @notice sets lockup time
    function setLockupTime(uint256 _rewardId, uint256 _lockupTime) external onlyMod {
        ggRewards[_rewardId].lockupTime = _lockupTime;
    }

    /// @notice sets concurrent user cap
    function setConcurrentUserCap(uint256 _rewardId, uint256 _concurrentStakerCap) external onlyMod {
        ggRewards[_rewardId].concurrentStakerCap = _concurrentStakerCap;
    }

    /// @notice sets lifetime user cap
    function setLifetimeUserCap(uint256 _rewardId, uint256 _lifeTimePlayerCap) external onlyMod {
        ggRewards[_rewardId].lifeTimePlayerCap = _lifeTimePlayerCap;
    }

    /**
     * @notice sets energy contract
     * @param _energy Energy Contract
     */
    function setEnergy(IEnergy _energy) external onlyMod {
        energy = _energy;
    }

    /**
     * @notice sets ixt contract
     * @param _pixt IXT Contract
     */
    function setIXT(IERC20Upgradeable _pixt) external onlyMod {
        pixt = _pixt;
    }

    /*---------------- VIEW FUNCTIONS ----------------*/

    /// @notice gets rewards user is staking for
    function getUserStaked(address _user) external view returns (uint256[] memory) {
        return userStaked[_user];
    }

    function getAllRewards() external view returns (uint256[] memory) {
        return allRewards;
    }
}
