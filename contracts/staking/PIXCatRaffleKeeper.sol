
//SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@chainlink/contracts/src/v0.8/automation/interfaces/AutomationCompatibleInterface.sol";

import "../interfaces/IPIXCatRaffle.sol";

contract PIXCatRaffleKeeper is AutomationCompatibleInterface {
    IPIXCatRaffle public catRaffle;

    constructor(address _catRaffle) {
        catRaffle = IPIXCatRaffle(_catRaffle);
    }

    function checkUpkeep(bytes calldata)
        external
        view
        override
        returns (bool upkeepNeeded, bytes memory performData)
    {
        uint256 day = catRaffle.getCurrentDay() - 1;
        upkeepNeeded = catRaffle.drawable(day);
        performData = abi.encode(day);
    }

    function performUpkeep(bytes calldata performData) external override {
        uint256 day = abi.decode(performData, (uint256));

        catRaffle.draw(day);
    }
}
