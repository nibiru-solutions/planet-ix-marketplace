//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../interfaces/IMetasharesStaking.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";

import {IConnext} from "@connext/nxtp-contracts/contracts/core/connext/interfaces/IConnext.sol";
import {IXReceiver} from "@connext/nxtp-contracts/contracts/core/connext/interfaces/IXReceiver.sol";

contract MetashareStaking is IMetasharesStaking, OwnableUpgradeable, ERC1155HolderUpgradeable, ReentrancyGuardUpgradeable, IXReceiver {
    using SafeERC20Upgradeable for IERC20Upgradeable;

    address private s_rewardDistributor;

    /// @notice moderators
    mapping(address => bool) public moderators;

    /// @notice Amount a user currently has staked
    mapping(uint256 => RewardPool) private s_rewardPools;
    mapping(address => mapping(uint256 => uint256)) private s_stakedAmounts;
    mapping(address => uint256[]) private s_stakedIDs;

    IERC20Upgradeable private s_rewardToken;
    address private s_metashares;

    bool public paused;

    IConnext public connext;
    address ethereumStaking;

    mapping(uint256 => bool) public stakeable_ids;

    function initialize(
        address _pixt,
        address _metashares,
        address _connext
    ) public initializer {
        require(_pixt != address(0), "MetashareStaking: INVALID_PIXT");
        require(_metashares != address(0), "MetashareStaking: INVALID_METASHARES");
        require(_connext != address(0), "MetashareStaking: INVALID_CONNEXT");

        __Ownable_init();
        __ERC1155Holder_init();
        __ReentrancyGuard_init();

        s_rewardToken = IERC20Upgradeable(_pixt);
        s_metashares = _metashares;

        connext = IConnext(_connext);
    }

    /// @notice sends message to ethereum to claim tokens
    function claimOnEthereum(uint256 relayerFee, uint256 returnRelayerFee) external payable nonReentrant whenNotPaused {
        bytes memory _callData = abi.encode(msg.sender, returnRelayerFee);
        connext.xcall{value: relayerFee}(
            1, // _destination: Domain ID of the destination chain - Ethereum
            ethereumStaking, // _to: address of the target contract (staking contract on ethereum)
            address(0), // _asset: use address zero for 0-value transfers
            msg.sender, // _delegate: address that can revert or forceLocal on destination
            0, // _amount: 0 because no funds are being transferred
            0, // _slippage: can be anything between 0-10000 because no funds are being transferred
            _callData // _callData: the encoded calldata to send
        );
    }

    /// @notice receives message from connext and claims tokens
    function xReceive(
        bytes32 _transferId,
        uint256 _amount,
        address _asset,
        address _originSender,
        uint32 _origin,
        bytes memory _callData
    ) external onlySource(_originSender, _origin) returns (bytes memory) {
        (address recipient, uint256 reward) = abi.decode(_callData, (address, uint256));
        s_rewardToken.safeTransfer(recipient, reward);
        emit RewardClaimed(msg.sender, reward);
        return "";
    }

    /// @inheritdoc IMetasharesStaking
    function stake(uint256 _tokenId, uint256 _amount) external override nonReentrant whenNotPaused {
        require(stakeable_ids[_tokenId], "MetashareStaking: INVALID_TOKEN_ID");
        IERC1155Upgradeable(s_metashares).safeTransferFrom(msg.sender, address(this), _tokenId, _amount, "");
        updateReward(_tokenId, msg.sender);

        bool isTokenIdStaked;
        for (uint256 i; i < s_stakedIDs[msg.sender].length; i += 1) {
            if (_tokenId == s_stakedIDs[msg.sender][i]) {
                isTokenIdStaked = true;
                break;
            }
        }
        if (!isTokenIdStaked) s_stakedIDs[msg.sender].push(_tokenId);

        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        rewardPool.tokensStaked += _amount;
        s_stakedAmounts[msg.sender][_tokenId] += _amount;

        emit MetashareStaked(msg.sender, _tokenId, _amount);
    }

    /// @inheritdoc IMetasharesStaking
    function unstake(uint256 _tokenId, uint256 _amount) external override nonReentrant whenNotPaused {
        require(_tokenId > 0, "MetashareStaking: INVALID_TOKEN_ID");
        require(s_stakedAmounts[msg.sender][_tokenId] >= _amount, "MetashareStaking: NOT_ENOUGH");

        updateReward(_tokenId, msg.sender);

        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        rewardPool.tokensStaked -= _amount;
        s_stakedAmounts[msg.sender][_tokenId] -= _amount;

        if (s_stakedAmounts[msg.sender][_tokenId] == 0) _deleteFromStakedNFTs(msg.sender, _tokenId);

        uint256 reward = rewardPool.rewards[msg.sender];
        if (reward > 0) {
            rewardPool.rewards[msg.sender] = 0;
            s_rewardToken.safeTransfer(msg.sender, reward);
            emit RewardClaimed(msg.sender, reward);
        }

        IERC1155Upgradeable(s_metashares).safeTransferFrom(address(this), msg.sender, _tokenId, _amount, "");

        emit MetashareUnstaked(msg.sender, _tokenId, _amount);
    }

    /// @inheritdoc IMetasharesStaking
    function claim() external override whenNotPaused {
        uint256 count;
        for (uint256 i; i < s_stakedIDs[msg.sender].length; i += 1) {
            if (s_stakedAmounts[msg.sender][s_stakedIDs[msg.sender][i]] == 0) continue;
            count++;
        }
        uint256 k;
        uint256[] memory tokenIds = new uint256[](count);
        for (uint256 i; i < s_stakedIDs[msg.sender].length; i += 1) {
            if (s_stakedAmounts[msg.sender][s_stakedIDs[msg.sender][i]] == 0) continue;
            tokenIds[k++] = s_stakedIDs[msg.sender][i];
        }
        claimBatch(tokenIds);
    }

    /// @inheritdoc IMetasharesStaking
    function claimBatch(uint256[] memory tokenIds) public override {
        uint256 reward;
        for (uint256 i; i < tokenIds.length; i += 1) {
            RewardPool storage rewardPool = s_rewardPools[tokenIds[i]];
            updateReward(tokenIds[i], msg.sender);
            reward += rewardPool.rewards[msg.sender];
            rewardPool.rewards[msg.sender] = 0;
        }
        if (reward > 0) {
            s_rewardToken.safeTransfer(msg.sender, reward);
            emit RewardClaimed(msg.sender, reward);
        }
    }

    /*---------------- VIEW FUNCTIONS ----------------*/

    /// @inheritdoc IMetasharesStaking
    function rewardPerToken(uint256 _tokenId, uint256 _untilTimestamp) public view override returns (uint256) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        if (rewardPool.tokensStaked == 0) {
            return rewardPool.rewardPerTokenStored;
        }
        return rewardPool.rewardPerTokenStored + ((lastTimeRewardApplicable(_tokenId, _untilTimestamp) - rewardPool.lastUpdateTime) * rewardPool.rewardRate) / rewardPool.tokensStaked;
    }

    /// @notice calculates the reward per token for a given token id and a given timestamp
    function rewardPerTokenFromNow(uint256 _tokenId, uint256 _untilTimestamp) public view returns (uint256) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        if (rewardPool.tokensStaked == 0) {
            return rewardPool.rewardPerTokenStored;
        }
        return ((lastTimeRewardApplicable(_tokenId, _untilTimestamp) - block.timestamp) * rewardPool.rewardRate) / rewardPool.tokensStaked;
    }

    /// @inheritdoc IMetasharesStaking
    function lastTimeRewardApplicable(uint256 _tokenId, uint256 _untilTimestamp) public view override returns (uint256) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        return MathUpgradeable.min(_untilTimestamp, rewardPool.periodFinish);
    }

    /// @inheritdoc IMetasharesStaking
    function getStakedAmounts(address _walletAddress, uint256 _tokenId) external view override returns (uint256) {
        return s_stakedAmounts[_walletAddress][_tokenId];
    }

    /// @inheritdoc IMetasharesStaking
    function getTokensStakedPerPool(uint256 _tokenId) external view override returns (uint256) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        return rewardPool.tokensStaked;
    }

    /// @inheritdoc IMetasharesStaking
    function getStakedIDs(address _walletAddress) external view override returns (uint256[] memory) {
        return s_stakedIDs[_walletAddress];
    }

    /// @inheritdoc IMetasharesStaking
    function earned(address _walletAddress, uint256 _tokenId) public view override returns (uint256) {
        return earnedUntilTimestamp(_walletAddress, _tokenId, block.timestamp);
    }

    /// @inheritdoc IMetasharesStaking
    function earnedUntilTimestamp(
        address _walletAddress,
        uint256 _tokenId,
        uint256 _untilTimestamp
    ) public view override returns (uint256 reward) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        return (s_stakedAmounts[_walletAddress][_tokenId] * (rewardPerToken(_tokenId, _untilTimestamp) - rewardPool.userRewardPerTokenPaid[_walletAddress])) + rewardPool.rewards[_walletAddress];
    }

    /// @inheritdoc IMetasharesStaking
    function earnedBatch(address _walletAddress, uint256[] calldata _tokenIds) external view override returns (uint256[] memory) {
        uint256[] memory earneds = new uint256[](_tokenIds.length);
        for (uint256 i; i < _tokenIds.length; i += 1) {
            earneds[i] = earned(_walletAddress, _tokenIds[i]);
        }
        return earneds;
    }

    /// @inheritdoc IMetasharesStaking
    function earnedByAccount(address _walletAddress) external view override returns (uint256) {
        return earnedByAccountUntilTimestamp(_walletAddress, block.timestamp);
    }

    /// @inheritdoc IMetasharesStaking
    function earnedByAccountUntilTimestamp(address _walletAddress, uint256 _untilTimestamp) public view override returns (uint256 reward) {
        for (uint256 i; i < s_stakedIDs[_walletAddress].length; i += 1) {
            if (s_stakedAmounts[_walletAddress][s_stakedIDs[_walletAddress][i]] == 0) continue;
            reward += earnedUntilTimestamp(_walletAddress, s_stakedIDs[_walletAddress][i], _untilTimestamp);
        }
    }

    /*---------------- ADMIN FUNCTIONS ----------------*/

    function setStakeableIds(uint256[] calldata _tokenIds, bool _isValid) public onlyOwner {
        uint256 length = _tokenIds.length;
        for (uint256 i = 0; i < length; ) {
            stakeable_ids[_tokenIds[i]] = _isValid;
            unchecked {
                ++i;
            }
        }
    }

    function setPaused(bool _paused) external override onlyGov {
        paused = _paused;
    }

    /// @inheritdoc IMetasharesStaking
    function initializePools(
        uint256[] calldata _tokenIds,
        uint256[] calldata _poolRewards,
        uint256 _epochDuration
    ) external override onlyRewardDistributor {
        uint256 length = _tokenIds.length;
        require(length == _poolRewards.length, "MetashareStaking: INVALID_ARGUMENTS");
        uint256 totalRewards;
        for (uint256 i = 0; i < length; i++) {
            require(stakeable_ids[_tokenIds[i]], "MetashareStaking: INVALID_TOKEN_ID");
            updateReward(_tokenIds[i], address(0));
            RewardPool storage rewardPool = s_rewardPools[_tokenIds[i]];
            if (block.timestamp >= rewardPool.periodFinish) {
                rewardPool.rewardRate = _poolRewards[i] / _epochDuration;
            } else {
                uint256 remaining = rewardPool.periodFinish - block.timestamp;
                uint256 leftover = remaining * rewardPool.rewardRate;
                rewardPool.rewardRate = (_poolRewards[i] + leftover) / _epochDuration;
            }
            rewardPool.lastUpdateTime = block.timestamp;
            rewardPool.periodFinish = block.timestamp + _epochDuration;
            totalRewards += _poolRewards[i];
            emit RewardAdded(_tokenIds[i], _poolRewards[i]);
        }
        s_rewardToken.safeTransferFrom(msg.sender, address(this), totalRewards);
    }

    function setEthAddress(address _ethereumStaking) public onlyOwner {
        ethereumStaking = _ethereumStaking;
    }

    /// @inheritdoc IMetasharesStaking
    function setRewardDistributor(address _distributor) external override onlyOwner {
        require(_distributor != address(0), "MetashareStaking: INVALID_DISTRIBUTOR");
        s_rewardDistributor = _distributor;
    }

    function setRewardToken(address _rewardToken) external onlyOwner {
        s_rewardToken = IERC20Upgradeable(_rewardToken);
    }

    /*------------------- MODIFIERS -------------------*/

    /// @notice allows calls from only moderators
    modifier onlyGov() {
        require(msg.sender == owner() || moderators[msg.sender], "MetashareStaking: NON_MODERATOR");
        _;
    }

    ///@notice allows function calls only when not paused
    modifier whenNotPaused() {
        require(!paused, "MetashareStaking: FEATURE PAUSED");
        _;
    }

    modifier onlyRewardDistributor() {
        require(msg.sender == s_rewardDistributor, "MetashareStaking: NON_DISTRIBUTOR");
        _;
    }

    /** @notice A modifier for authenticated calls.
     *  This is an important security consideration. If the target contract
     *  function should be authenticated, it must check three things:
     *    1) The originating call comes from the expected origin domain.
     *    2) The originating call comes from the expected source contract.
     *    3) The call to this contract comes from Connext.
     */
    modifier onlySource(address _originSender, uint32 _origin) {
        require(_origin == 1 && _originSender == ethereumStaking && msg.sender == address(connext), "Expected source contract on origin domain called by Connext");
        _;
    }

    /*---------------- INTERNAL FUNCTIONS ----------------*/

    /**
     * @notice Internal function to delete address and tokenId from s_stakedIDs
     * @param _walletAddress The address to remove
     * @param _tokenId The tokenID to remove
     */
    function _deleteFromStakedNFTs(address _walletAddress, uint256 _tokenId) internal {
        for (uint256 i; i < s_stakedIDs[_walletAddress].length; i++) {
            if (s_stakedIDs[_walletAddress][i] == _tokenId) {
                s_stakedIDs[_walletAddress][i] = s_stakedIDs[_walletAddress][s_stakedIDs[_walletAddress].length - 1];
                s_stakedIDs[_walletAddress].pop();
            }
        }
    }

    function updateReward(uint256 _tokenId, address _walletAddress) internal {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        rewardPool.rewardPerTokenStored = rewardPerToken(_tokenId, block.timestamp);
        rewardPool.lastUpdateTime = lastTimeRewardApplicable(_tokenId, block.timestamp);
        if (_walletAddress != address(0)) {
            rewardPool.rewards[_walletAddress] = earned(_walletAddress, _tokenId);
            rewardPool.userRewardPerTokenPaid[_walletAddress] = rewardPool.rewardPerTokenStored;
        }
    }
}
