//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";
import "../rover/VRFCoordinatorV2Interface.sol";

import "../libraries/VRFConsumerBaseV2Upgradable.sol";

import "../interfaces/IPIXCatRaffle.sol";

import "../mission_control/IAssetManager.sol";

contract PIXCatRaffle is
    IPIXCatRaffle,
    VRFConsumerBaseV2Upgradable,
    OwnableUpgradeable,
    PausableUpgradeable,
    ERC1155HolderUpgradeable
{
    /* ========== EVENTS ========== */

    event TicketsAdded(
        uint256 indexed day,
        address indexed player,
        uint256 indexed ticketId,
        uint256 tickets
    );
    event PrizeClaimed(address indexed player, uint256[] ids, uint256[] amounts);
    event PrizeAdded(
        uint256 indexed day,
        uint256 indexed ticketId,
        uint256[] ids,
        uint256[] amounts
    );
    event DrawRequested(uint256 requestID, uint256 indexed day, uint256 ticketId, uint256 tickets);
    event ResultDrawn(uint256 indexed day);
    event RandomSeedReceived(uint256 indexed day, uint256 randomSeed);

    /* ========== DATA ========== */

    struct DailyTicketInfo {
        uint256 ticketId; // regular, premium, golden ticket
        uint256 maxEntriesPerWallet;
        uint256 maxTotalEntries;
        uint256 totalTickets;
        address[] players;
        uint256[] sum;
        mapping(address => uint256) playerTickets;
    }

    /* ========== STATE VARIABLES ========== */

    IERC1155 public landmark;
    IERC1155 public ticket;

    address public keeper;
    uint256 public firstDay;

    mapping(uint256 => address[]) public dailyWinners; // day => winners
    mapping(uint256 => uint256) public drawRequests; // requestId => day
    mapping(uint256 => DailyTicketInfo) public dailyTickets; // day => daily tickets
    mapping(uint256 => uint256[]) public raffleLandmarkIds; // day => landmark tokenIds
    mapping(uint256 => mapping(uint256 => uint256)) public raffleLandmarkAmounts; // day => landmark tokenId => amount
    mapping(address => uint256[]) public prizeLandmarkIds; // player => landmark tokenIds
    mapping(address => mapping(uint256 => uint256)) public prizeLandmarkAmounts; // player => landmark tokenId => amount

    mapping(uint256 => bool) private _drawn;

    mapping(address => bool) public moderators;

    VRFCoordinatorV2Interface public COORDINATOR;
    uint64 public subscriptionId;
    bytes32 public keyHash;
    uint32 public callbackGasLimit;
    uint16 public requestConfirmations;

    uint256 public constant SECONDS_PER_DAY = 1 days;

    /* ========== v2 STATE VARIABLES ========== */
    mapping(uint256 => uint256) public receivedRandomSeeds; // day => random seed

    /* ========== MODIFIER ========== */
    modifier onlyMod() {
        require(moderators[msg.sender], "non moderator");
        _;
    }

    /* ========== INITIALIZER ========== */

    function initialize(
        address _landmark,
        address _ticket,
        address _vrfCoordinator,
        bytes32 _keyHash,
        uint64 _subscriptionId
    ) external initializer {
        require(_landmark != address(0), "invalid landmark");
        require(_ticket != address(0), "invalid ticket");

        landmark = IERC1155(_landmark);
        ticket = IERC1155(_ticket);

        COORDINATOR = VRFCoordinatorV2Interface(_vrfCoordinator);
        keyHash = _keyHash;
        subscriptionId = _subscriptionId;
        firstDay = block.timestamp / SECONDS_PER_DAY;

        callbackGasLimit = 2000000;
        requestConfirmations = 3;

        moderators[msg.sender] = true;

        __Ownable_init();
        __ERC1155Holder_init();
        __VRFConsumerBaseV2_init(_vrfCoordinator);
    }

    /* ========== MUTATIVE FUNCTIONS ========== */

    /**
     * @notice enter using the tickets for the chance of winning Landmark shares
     * @param _tickets amount of the tickets
     */
    function addTickets(uint256 _tickets) external whenNotPaused {
        require(_tickets > 0, "tickets must be > 0");

        uint256 day = getCurrentDay();
        require(!_drawn[day], "already drawn");
        require(raffleLandmarkIds[day].length > 0, "no landmark");

        DailyTicketInfo storage info = dailyTickets[day];
        require(
            info.playerTickets[msg.sender] + _tickets <= info.maxEntriesPerWallet,
            "exceed max entries per wallet"
        );
        require(info.totalTickets + _tickets <= info.maxTotalEntries, "exceed max total entries");

        info.playerTickets[msg.sender] += _tickets;
        info.totalTickets += _tickets;
        info.players.push(msg.sender);
        info.sum.push(info.totalTickets);

        IAssetManager(address(ticket)).trustedBurn(msg.sender, info.ticketId, _tickets);

        emit TicketsAdded(day, msg.sender, info.ticketId, _tickets);
    }

    /**
     * @notice claim all of winning Landmark shares
     */
    function claimPrize() external whenNotPaused {
        uint256[] memory ids = prizeLandmarkIds[msg.sender];
        uint256 length = ids.length;
        require(length > 0, "no prize");

        uint256[] memory amounts = new uint256[](length);
        for (uint256 i = 0; i < length; i++) {
            amounts[i] = prizeLandmarkAmounts[msg.sender][ids[i]];

            delete prizeLandmarkAmounts[msg.sender][ids[i]];
        }

        delete prizeLandmarkIds[msg.sender];

        landmark.safeBatchTransferFrom(address(this), msg.sender, ids, amounts, "");

        emit PrizeClaimed(msg.sender, ids, amounts);
    }

    /* ========== RESTRICTED FUNCTIONS ========== */

    /**
     * @notice set keyHash by owner
     */
    function setKeyHash(bytes32 _keyHash) external onlyOwner {
        keyHash = _keyHash;
    }

    /**
     * @notice set callbackGasLimit by owner
     */
    function setCallbackGasLimit(uint32 _callbackGasLimit) external onlyOwner {
        callbackGasLimit = _callbackGasLimit;
    }

    /**
     * @notice set requestConfirmations by owner
     */
    function setRequestConfirmations(uint16 _requestConfirmations) external onlyOwner {
        requestConfirmations = _requestConfirmations;
    }

    /**
     * @notice set moderator by onwer
     */
    function setModerator(address _moderator, bool _approved) external onlyOwner {
        require(_moderator != address(0), "invalid moderator");
        moderators[_moderator] = _approved;
    }

    /**
     * @notice add Landmark NFTs for daily winners, set regular/ultra/golden ticket raffle 
               for players to win less-good/more-rare/seldomly Landmarks
     * @param _day number of day to add Landmark NFTs
     * @param _ticketId regular/ultra/golden ticket
     * @param _ids Landmark ids to add
     * @param _amounts Landmark amounts to add
     * @param _maxEntriesPerWallet max entries per wallet of the day
     * @param _maxTotalEntries max total entries of the day
     */
    function addPrize(
        uint256 _day,
        uint256 _ticketId,
        uint256[] memory _ids,
        uint256[] memory _amounts,
        uint256 _maxEntriesPerWallet,
        uint256 _maxTotalEntries
    ) external onlyMod whenNotPaused {
        require(_day >= getCurrentDay(), "invalid day");
        require(!_drawn[_day], "already drawn");

        uint256 length = _ids.length;
        require(length > 0 && length == _amounts.length, "invalid length");

        if (raffleLandmarkIds[_day].length == 0) {
            require(_ticketId >= 16 && _ticketId <= 18, "invalid ticketId");
            require(
                _maxEntriesPerWallet > 0 && _maxTotalEntries >= _maxEntriesPerWallet,
                "invalid max entries"
            );

            DailyTicketInfo storage info = dailyTickets[_day];
            info.ticketId = _ticketId;
            info.maxEntriesPerWallet = _maxEntriesPerWallet;
            info.maxTotalEntries = _maxTotalEntries;
        } else {
            require(
                dailyTickets[_day].ticketId == _ticketId,
                "invalid ticketId: TicketId for day already set"
            );
        }

        for (uint256 i = 0; i < length; i++) {
            uint256 id = _ids[i];
            uint256 amount = _amounts[i];
            require(amount > 0, "invalid amount");

            if (raffleLandmarkAmounts[_day][id] == 0) {
                raffleLandmarkIds[_day].push(id);
            }

            raffleLandmarkAmounts[_day][id] += amount;
        }

        landmark.safeBatchTransferFrom(msg.sender, address(this), _ids, _amounts, "");

        emit PrizeAdded(_day, _ticketId, _ids, _amounts);
    }

    /**
     * @notice add Landmark NFTs for daily winners, set regular/ultra/golden ticket raffle 
               for players to win less-good/more-rare/seldomly Landmarks
     * @param _ticketId regular/ultra/golden ticket
     * @param _ids Landmark ids to add
     * @param _amounts Landmark amounts to add
     * @param _maxEntriesPerWallet max entries per wallet of the day
     * @param _maxTotalEntries max total entries of the day
     */
    function addPrizeToCurrentDay(
        uint256 _ticketId,
        uint256[] memory _ids,
        uint256[] memory _amounts,
        uint256 _maxEntriesPerWallet,
        uint256 _maxTotalEntries
    ) external onlyMod whenNotPaused {
        uint256 day = getCurrentDay();
        require(!_drawn[day], "already drawn");

        uint256 length = _ids.length;
        require(length > 0 && length == _amounts.length, "invalid length");

        if (raffleLandmarkIds[day].length == 0) {
            require(_ticketId >= 16 && _ticketId <= 18, "invalid ticketId");
            require(
                _maxEntriesPerWallet > 0 && _maxTotalEntries >= _maxEntriesPerWallet,
                "invalid max entries"
            );

            DailyTicketInfo storage info = dailyTickets[day];
            info.ticketId = _ticketId;
            info.maxEntriesPerWallet = _maxEntriesPerWallet;
            info.maxTotalEntries = _maxTotalEntries;
        } else {
            require(
                dailyTickets[day].ticketId == _ticketId,
                "invalid ticketId: TicketId for day already set"
            );
        }

        for (uint256 i = 0; i < length; i++) {
            uint256 id = _ids[i];
            uint256 amount = _amounts[i];
            require(amount > 0, "invalid amount");

            if (raffleLandmarkAmounts[day][id] == 0) {
                raffleLandmarkIds[day].push(id);
            }

            raffleLandmarkAmounts[day][id] += amount;
        }

        landmark.safeBatchTransferFrom(msg.sender, address(this), _ids, _amounts, "");

        emit PrizeAdded(day, _ticketId, _ids, _amounts);
    }

    /**
     * @notice request to generate random numbers to pick winners
     * @dev chainlink keeper can execute it
     * @param _day number of day to pick winners
     */
    function draw(uint256 _day) external override whenNotPaused {
        require(msg.sender == keeper, "invalid keeper");
        require((_day + firstDay + 1) * SECONDS_PER_DAY < block.timestamp, "invalid day");

        require(!_drawn[_day], "draw already requested");
        _drawn[_day] = true;

        DailyTicketInfo storage info = dailyTickets[_day];

        if (info.totalTickets > 0) {
            uint256 requestID = getRandomNumber();
            drawRequests[requestID] = _day;

            emit DrawRequested(requestID, _day, info.ticketId, info.totalTickets);
        } else {
            uint256[] memory ids = raffleLandmarkIds[_day];
            uint256 length = ids.length;
            uint256[] memory amounts = new uint256[](length);

            for (uint256 i = 0; i < length; i++) {
                amounts[i] = raffleLandmarkAmounts[_day][ids[i]];
            }

            landmark.safeBatchTransferFrom(address(this), owner(), ids, amounts, "");
        }
    }

    /**
     * @notice resolves the winner for given day. 
     * @notice Can be called only when random seed is received from Chainlink 
     * @param _day day to resolve
     */
    function resolveDay(uint256 _day) external override whenNotPaused {
        uint256 randomSeed = receivedRandomSeeds[_day];
        uint256 randomIndex = 1;

        require(randomSeed != 0, "random seed not received yet");

        DailyTicketInfo storage info = dailyTickets[_day];

        uint8 i;
        uint8 j;
        uint256 id;
        uint256 left;
        uint256 right;
        uint256 mid;
        uint256 rand;
        
        for (i = 0; i < raffleLandmarkIds[_day].length; i++) {
            id = raffleLandmarkIds[_day][i];
            for (j = 0; j < raffleLandmarkAmounts[_day][id]; j++) {
                rand = generatePRNGfromSeed(randomSeed, randomIndex++) % info.totalTickets;
                left = 0;
                right = info.players.length - 1;
                while (left < right) {
                    mid = (left + right) / 2;
                    if (rand >= info.sum[mid]) {
                        left = mid + 1;
                    } else if (rand < info.sum[mid]) {
                        right = mid;
                    }
                }

                address winner = info.players[left];
                dailyWinners[_day].push(winner);
                if (prizeLandmarkAmounts[winner][id] == 0) {
                    prizeLandmarkIds[winner].push(id);
                }
                prizeLandmarkAmounts[winner][id] += 1;
            }
        }

        emit ResultDrawn(_day);
    }

    /**
     * @notice emergency redraw by owner
     * @param _day number of day
     */
    function emergencyRedraw(uint256 _day) external onlyOwner {
        require(_drawn[_day], "draw not requested");
        _drawn[_day] = false;
    }

    /**
     * @notice set keeper by owner
     */
    function setKeeper(address _keeper) external onlyMod {
        require(_keeper != address(0), "invalid keeper");
        keeper = _keeper;
    }

    /**
     * @notice pause
     */
    function pause() external onlyOwner whenNotPaused {
        _pause();
    }

    /**
     * @notice unpause
     */
    function unpause() external onlyOwner whenPaused {
        _unpause();
    }

    /* ========== INTERNALS ========== */

    /**
     * @notice request random words
     */
    function getRandomNumber() internal returns (uint256 requestID) {
        require(keyHash != bytes32(0), "Must have valid key hash");

        requestID = COORDINATOR.requestRandomWords(
            keyHash,
            subscriptionId,
            requestConfirmations,
            callbackGasLimit,
            uint32(1)
        );
    }

    /**
     * @notice Callback function used by ChainLink's VRF v2 Coordinator
     */
    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords) internal override {
        uint256 day = drawRequests[requestId];
        receivedRandomSeeds[day] = randomWords[0];

        emit RandomSeedReceived(day, randomWords[0]);
    }

    /**
     * @notice generate random number from the given random seed
     * @param _seed starting random seed
     * @param _index index of the random number
     */
    function generatePRNGfromSeed(uint256 _seed, uint256 _index) internal pure returns (uint256) {
        return uint256(keccak256((abi.encode(_seed, _index))));
    }

    /* ========== VIEWS ========== */

    /**
     * @notice return number of the day for current raffle
     */
    function getCurrentDay() public view override returns (uint256) {
        uint256 currentDay = block.timestamp / SECONDS_PER_DAY - firstDay;
        return currentDay;
    }

    /**
     * @notice raffle is finished to pick winners
     * @param _day number of the day
     */
    function drawable(uint256 _day) external view override returns (bool) {
        if (paused()) {
            return false;
        }

        if (_drawn[_day]) {
            return false;
        }
        if ((_day + firstDay + 1) * SECONDS_PER_DAY >= block.timestamp) {
            return false;
        }

        DailyTicketInfo storage info = dailyTickets[_day];
        if (info.totalTickets == 0) {
            return false;
        }

        return true;
    }

    /**
     * @notice winners' addresses
     * @param _day number of the day
     */
    function winnings(uint256 _day) external view returns (address[] memory) {
        return dailyWinners[_day];
    }

    /**
     * @notice winner's claimable Landmarks
     * @param _player winner address
     */
    function claimable(address _player) external view returns (uint256[] memory, uint256[] memory) {
        uint256 length = prizeLandmarkIds[_player].length;
        uint256[] memory amounts = new uint256[](length);

        for (uint256 i = 0; i < length; i++) {
            amounts[i] = prizeLandmarkAmounts[_player][prizeLandmarkIds[_player][i]];
        }

        return (prizeLandmarkIds[_player], amounts);
    }

    /**
     * @notice available tickets to add
     * @param _player palyer address
     */
    function availableTicketsOf(address _player) external view returns (uint256) {
        uint256 day = getCurrentDay();
        DailyTicketInfo storage info = dailyTickets[day];

        if (info.playerTickets[_player] >= info.maxEntriesPerWallet) {
            return 0;
        }
        if (info.totalTickets >= info.maxTotalEntries) {
            return 0;
        }

        return
            Math.min(
                info.maxEntriesPerWallet - info.playerTickets[_player],
                info.maxTotalEntries - info.totalTickets
            );
    }

    /**
     * @notice player's added tickets in a raffle
     * @param _day number of the day
     * @param _player player address
     */
    function ticketsOf(uint256 _day, address _player) external view returns (uint256, uint256) {
        return (dailyTickets[_day].ticketId, dailyTickets[_day].playerTickets[_player]);
    }

    /**
     * @notice player's added tickets in a raffle
     * @param _player player address
     */
    function ticketsOfCurrentDay(address _player) external view returns (uint256, uint256) {
        uint256 day = getCurrentDay();
        return (dailyTickets[day].ticketId, dailyTickets[day].playerTickets[_player]);
    }

    /**
     * @notice player's added tickets of past raffles
     * @param _player player address
     */
    function pastTicketsOf(address _player)
        external
        view
        returns (uint256[] memory, uint256[] memory)
    {
        uint256 day = getCurrentDay() + 1;
        uint256[] memory pastTicketIds = new uint256[](day);
        uint256[] memory pastTickets = new uint256[](day);

        for (uint256 i = 0; i < day; i++) {
            pastTicketIds[i] = dailyTickets[i].ticketId;
            pastTickets[i] = dailyTickets[i].playerTickets[_player];
        }

        return (pastTicketIds, pastTickets);
    }
}
