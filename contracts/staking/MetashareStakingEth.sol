//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../interfaces/IMetasharesStakingEth.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";

import {IConnext} from "@connext/nxtp-contracts/contracts/core/connext/interfaces/IConnext.sol";

contract MetashareStakingEth is IMetasharesStakingEth, OwnableUpgradeable, ERC1155HolderUpgradeable, ReentrancyGuardUpgradeable {
    using SafeERC20Upgradeable for IERC20Upgradeable;

    address private s_rewardDistributor;

    /// @notice moderators
    mapping(address => bool) public moderators;

    RewardPool private s_rewardPool;

    /// @notice Amount a user currently has staked
    mapping(address => uint256) private s_stakedAmounts;

    address private s_metashares;
    uint256 private stakeable_token_id;

    bool public paused;

    IConnext public connext;
    address polygonStaking;

    function initialize(
        uint256 _stakeable_token_id,
        address _metashares,
        address _connext,
        address _polygonStaking
    ) public initializer {
        require(_metashares != address(0), "MetashareStaking: INVALID_METASHARES");
        require(_connext != address(0), "MetashareStaking: INVALID_CONNEXT");

        __Ownable_init();
        __ERC1155Holder_init();
        __ReentrancyGuard_init();

        stakeable_token_id = _stakeable_token_id;
        s_metashares = _metashares;

        connext = IConnext(_connext);
        polygonStaking = _polygonStaking;
    }

    /// @inheritdoc IMetasharesStakingEth
    function claimOnPolygon(uint256 relayerFee) public payable override {
        _claimOnPolygon(msg.sender, relayerFee);
    }

    /// @notice Claim rewards for a user
    function _claimOnPolygon(address recipient, uint256 relayerFee) internal whenNotPaused {
        uint256 reward = _claim(recipient);
        _sendCrossChain(recipient, reward, relayerFee);
    }

    /// @notice receives message from connext and claims tokens
    function xReceive(
        bytes32 _transferId,
        uint256 _amount,
        address _asset,
        address _originSender,
        uint32 _origin,
        bytes memory _callData
    ) external onlySource(_originSender, _origin) returns (bytes memory) {
        (address recipient, uint256 returnRelayerFee) = abi.decode(_callData, (address, uint256));
        _claimOnPolygon(recipient, returnRelayerFee);
        return "";
    }

    /// @inheritdoc IMetasharesStakingEth
    function stake(uint256 _amount) external nonReentrant whenNotPaused {
        IERC1155Upgradeable(s_metashares).safeTransferFrom(msg.sender, address(this), stakeable_token_id, _amount, "");
        updateReward(msg.sender);

        RewardPool storage rewardPool = s_rewardPool;
        rewardPool.tokensStaked += _amount;
        s_stakedAmounts[msg.sender] += _amount;

        emit MetashareStaked(msg.sender, stakeable_token_id, _amount);
    }

    /// @inheritdoc IMetasharesStakingEth
    function unstake(uint256 _amount, uint256 relayerFee) external payable nonReentrant whenNotPaused {
        require(msg.value == relayerFee, "MetashareStaking: INVALID_RELAYER_FEE");
        require(s_stakedAmounts[msg.sender] >= _amount, "MetashareStaking: NOT_ENOUGH");

        updateReward(msg.sender);

        RewardPool storage rewardPool = s_rewardPool;
        rewardPool.tokensStaked -= _amount;
        s_stakedAmounts[msg.sender] -= _amount;

        uint256 reward = rewardPool.rewards[msg.sender];
        if (reward > 0) {
            rewardPool.rewards[msg.sender] = 0;
            _sendCrossChain(msg.sender, reward, relayerFee);
            emit RewardClaimed(msg.sender, reward);
        }

        IERC1155Upgradeable(s_metashares).safeTransferFrom(address(this), msg.sender, stakeable_token_id, _amount, "");

        emit MetashareUnstaked(msg.sender, stakeable_token_id, _amount);
    }

    function _claim(address recipient) internal whenNotPaused returns (uint256 reward) {
        RewardPool storage rewardPool = s_rewardPool;
        updateReward(recipient);
        reward += rewardPool.rewards[recipient];
        rewardPool.rewards[recipient] = 0;
        emit RewardClaimed(recipient, reward);
        return reward;
    }

    /*---------------- VIEW FUNCTIONS ----------------*/

    /// @inheritdoc IMetasharesStakingEth
    function rewardPerToken(uint256 _untilTimestamp) public view returns (uint256) {
        RewardPool storage rewardPool = s_rewardPool;
        if (rewardPool.tokensStaked == 0) {
            return rewardPool.rewardPerTokenStored;
        }
        return rewardPool.rewardPerTokenStored + ((lastTimeRewardApplicable(_untilTimestamp) - rewardPool.lastUpdateTime) * rewardPool.rewardRate) / rewardPool.tokensStaked;
    }

    /// @notice calculates the reward per token for a given token id and a given timestamp
    function rewardPerTokenFromNow(uint256 _untilTimestamp) public view returns (uint256) {
        RewardPool storage rewardPool = s_rewardPool;
        if (rewardPool.tokensStaked == 0) {
            return rewardPool.rewardPerTokenStored;
        }
        return ((lastTimeRewardApplicable(_untilTimestamp) - block.timestamp) * rewardPool.rewardRate) / rewardPool.tokensStaked;
    }

    /// @inheritdoc IMetasharesStakingEth
    function lastTimeRewardApplicable(uint256 _untilTimestamp) public view returns (uint256) {
        RewardPool storage rewardPool = s_rewardPool;
        return MathUpgradeable.min(_untilTimestamp, rewardPool.periodFinish);
    }

    /// @inheritdoc IMetasharesStakingEth
    function getStakedAmounts(address _walletAddress) external view returns (uint256) {
        return s_stakedAmounts[_walletAddress];
    }

    /// @inheritdoc IMetasharesStakingEth
    function getTokensStakedPerPool() external view returns (uint256) {
        RewardPool storage rewardPool = s_rewardPool;
        return rewardPool.tokensStaked;
    }

    /// @inheritdoc IMetasharesStakingEth
    function earned(address _walletAddress) public view returns (uint256) {
        return earnedUntilTimestamp(_walletAddress, block.timestamp);
    }

    /// @inheritdoc IMetasharesStakingEth
    function earnedUntilTimestamp(address _walletAddress, uint256 _untilTimestamp) public view returns (uint256 reward) {
        RewardPool storage rewardPool = s_rewardPool;
        return (s_stakedAmounts[_walletAddress] * (rewardPerToken(_untilTimestamp) - rewardPool.userRewardPerTokenPaid[_walletAddress])) + rewardPool.rewards[_walletAddress];
    }

    /// @inheritdoc IMetasharesStakingEth
    function earnedByAccount(address _walletAddress) external view returns (uint256) {
        return earnedByAccountUntilTimestamp(_walletAddress, block.timestamp);
    }

    /// @inheritdoc IMetasharesStakingEth
    function earnedByAccountUntilTimestamp(address _walletAddress, uint256 _untilTimestamp) public view returns (uint256 reward) {
        reward = earnedUntilTimestamp(_walletAddress, _untilTimestamp);
    }

    /*---------------- ADMIN FUNCTIONS ----------------*/

    function setPaused(bool _paused) external onlyGov {
        paused = _paused;
    }

    /// @inheritdoc IMetasharesStakingEth
    function initializePools(uint256 _poolRewards, uint256 _epochDuration) external onlyRewardDistributor {
        uint256 totalRewards;
        updateReward(address(0));
        RewardPool storage rewardPool = s_rewardPool;
        if (block.timestamp >= rewardPool.periodFinish) {
            rewardPool.rewardRate = _poolRewards / _epochDuration;
        } else {
            uint256 remaining = rewardPool.periodFinish - block.timestamp;
            uint256 leftover = remaining * rewardPool.rewardRate;
            rewardPool.rewardRate = (_poolRewards + leftover) / _epochDuration;
        }
        rewardPool.lastUpdateTime = block.timestamp;
        rewardPool.periodFinish = block.timestamp + _epochDuration;
        totalRewards += _poolRewards;
        emit RewardAdded(stakeable_token_id, _poolRewards);
    }

    function setRewardDistributor(address _distributor) external onlyOwner {
        require(_distributor != address(0), "Staking: INVALID_DISTRIBUTOR");
        s_rewardDistributor = _distributor;
    }

    /*------------------- MODIFIERS -------------------*/

    /// @notice allows calls from only moderators
    modifier onlyGov() {
        require(msg.sender == owner() || moderators[msg.sender], "ENERGY STAKING: NON_MODERATOR");
        _;
    }

    ///@notice allows function calls only when not paused
    modifier whenNotPaused() {
        require(!paused, "ENERGY STAKING: FEATURE PAUSED");
        _;
    }

    modifier onlyRewardDistributor() {
        require(msg.sender == s_rewardDistributor, "Staking: NON_DISTRIBUTOR");
        _;
    }

    /** @notice A modifier for authenticated calls.
     *  This is an important security consideration. If the target contract
     *  function should be authenticated, it must check three things:
     *    1) The originating call comes from the expected origin domain.
     *    2) The originating call comes from the expected source contract.
     *    3) The call to this contract comes from Connext.
     */
    modifier onlySource(address _originSender, uint32 _origin) {
        require(_origin == 137 && _originSender == polygonStaking && msg.sender == address(connext), "Expected source contract on origin domain called by Connext");
        _;
    }

    /*---------------- INTERNAL FUNCTIONS ----------------*/

    function updateReward(address _walletAddress) internal {
        RewardPool storage rewardPool = s_rewardPool;
        rewardPool.rewardPerTokenStored = rewardPerToken(block.timestamp);
        rewardPool.lastUpdateTime = lastTimeRewardApplicable(block.timestamp);
        if (_walletAddress != address(0)) {
            rewardPool.rewards[_walletAddress] = earned(_walletAddress);
            rewardPool.userRewardPerTokenPaid[_walletAddress] = rewardPool.rewardPerTokenStored;
        }
    }

    /// @notice Encodes calldata and sends rewards to polygon contract
    function _sendCrossChain(
        address recipient,
        uint256 reward,
        uint256 relayerFee
    ) internal whenNotPaused {
        bytes memory _callData = abi.encode(recipient, reward, address(this), relayerFee);
        connext.xcall{value: relayerFee}(
            137, // _destination: Domain ID of the destination chain - Polygon
            polygonStaking, // _to: address of the target contract (Polygon staking contract)
            address(0), // _asset: use address zero for 0-value transfers
            msg.sender, // _delegate: address that can revert or forceLocal on destination
            0, // _amount: 0 because no funds are being transferred
            0, // _slippage: can be anything between 0-10000 because no funds are being transferred
            _callData // _callData: the encoded calldata to send
        );
    }

    /// @notice receives ETH for connext
    receive() external payable {
        emit Received(msg.sender, msg.value);
    }
}
