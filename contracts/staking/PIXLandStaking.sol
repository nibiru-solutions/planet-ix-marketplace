//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/IPIXLandStaking.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/MathUpgradeable.sol";

contract PIXLandStaking is
    IPIXLandStaking,
    OwnableUpgradeable,
    ERC1155HolderUpgradeable,
    ReentrancyGuardUpgradeable
{
    using SafeERC20Upgradeable for IERC20Upgradeable;

    /* State Variables */
    address private s_pixLandmark;
    address private s_rewardDistributor;
    IERC20Upgradeable private s_rewardToken;

    mapping(uint256 => RewardPool) private s_rewardPools;
    mapping(address => uint256[]) private s_stakedIDs;
    mapping(address => mapping(uint256 => uint256)) private s_stakedAmounts;

    /* Modifiers */
    modifier onlyRewardDistributor() {
        require(msg.sender == s_rewardDistributor, "Staking: NON_DISTRIBUTOR");
        _;
    }

    /* Functions */
    /**
     * @notice Initializer for the PIXLandStaking contract
     * @param _pixt IXT contract
     * @param _pixLandmark Landmark contract
     */
    function initialize(address _pixt, address _pixLandmark) public initializer {
        require(_pixt != address(0), "LandStaking: INVALID_PIXT");
        require(_pixLandmark != address(0), "LandStaking: INVALID_PIX_LAND");

        __Ownable_init();
        __ERC1155Holder_init();
        __ReentrancyGuard_init();

        s_rewardToken = IERC20Upgradeable(_pixt);
        s_pixLandmark = _pixLandmark;
    }

    /**
     * @notice Get reward per token staked for Landmark share
     * @param _tokenId The Token to get reward
     * @param _untilTimestamp The timestamp by which the reward per token is requested
     */
    function rewardPerToken(uint256 _tokenId, uint256 _untilTimestamp) public view returns (uint256) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        if (rewardPool.tokensStaked == 0) {
            return rewardPool.rewardPerTokenStored;
        }
        return
            rewardPool.rewardPerTokenStored +
            ((lastTimeRewardApplicable(_tokenId, _untilTimestamp) - rewardPool.lastUpdateTime) * rewardPool.rewardRate ) / rewardPool.tokensStaked;
    }

    /**
     * @notice Used to get the latest time for which the reward is applicable
     * @param _tokenId The token for which the reward was added
     * @param _untilTimestamp The timestamp by which the last time reward is applicable
     */
    function lastTimeRewardApplicable(uint256 _tokenId, uint256 _untilTimestamp) public view returns (uint256) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        return MathUpgradeable.min(_untilTimestamp, rewardPool.periodFinish);
    }

    // @inheritdoc IPIXLandStaking
    function getStakedAmounts(address _walletAddress, uint256 _tokenId) external view override returns (uint256) {
        return s_stakedAmounts[_walletAddress][_tokenId];
    }

    // @inheritdoc IPIXLandStaking
    function getRewardRate(uint256 _tokenId) external view override returns (uint256) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        return rewardPool.rewardRate;
    }

    // @inheritdoc IPIXLandStaking
    function getTokensStakedPerPool(uint256 _tokenId) external view override returns (uint256) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        return rewardPool.tokensStaked;
    }

    // @inheritdoc IPIXLandStaking
    function getStakedIDs(address _walletAddress) external view override returns (uint256[] memory) {
        return s_stakedIDs[_walletAddress];
    }

    /**
     * @notice Used to get earned rewards for addess and token
     * @param _walletAddress The address to get earned rewards
     * @param _tokenId The token to be get earned rewards
     * @return Rewards accumulated by address for a given token
     */
    function earned(address _walletAddress, uint256 _tokenId) public view returns (uint256) {
        return earnedUntilTimestamp(_walletAddress, _tokenId, block.timestamp);
    }

    /**
     * @notice Used to get earned rewards for addess and token until timestamp
     * @param _walletAddress The address to get earned rewards
     * @param _tokenId The token to be get earned rewards
     * @param _untilTimestamp The timestamp by which the earned reward is requested
     * @return reward Rewards accumulated by address for a given token until timestamp
     */
    function earnedUntilTimestamp(address _walletAddress, uint256 _tokenId, uint256 _untilTimestamp) public view returns (uint256 reward) {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        return
            (s_stakedAmounts[_walletAddress][_tokenId] * (rewardPerToken(_tokenId, _untilTimestamp) - rewardPool.userRewardPerTokenPaid[_walletAddress]))  +
            rewardPool.rewards[_walletAddress];
    }

    // @inheritdoc IPIXLandStaking
    function earnedBatch(address _walletAddress, uint256[] calldata _tokenIds)
        external
        view
        override
        returns (uint256[] memory)
    {
        uint256[] memory earneds = new uint256[](_tokenIds.length);
        for (uint256 i; i < _tokenIds.length; i += 1) {
            earneds[i] = earned(_walletAddress, _tokenIds[i]);
        }
        return earneds;
    }

    // @inheritdoc IPIXLandStaking
    function earnedByAccount(address _walletAddress) external
        view
        override
        returns (uint256)
    {
        return earnedByAccountUntilTimestamp(_walletAddress, block.timestamp);
    }

    /**
     * @notice Used to get rewards earned by a wallet address until timestamp
     * @param _walletAddress Wallet address to get rewards
     * @param _untilTimestamp The timestamp by which the account earned reward is requested
     * @return reward Rewards accumulated by address until timestamp
     */
    function earnedByAccountUntilTimestamp(address _walletAddress, uint256 _untilTimestamp) public view returns (uint256 reward) {
        for (uint256 i; i < s_stakedIDs[_walletAddress].length; i += 1) {
            if (s_stakedAmounts[_walletAddress][s_stakedIDs[_walletAddress][i]] == 0) continue;
            reward += earnedUntilTimestamp(_walletAddress, s_stakedIDs[_walletAddress][i], _untilTimestamp);
        }
    }

    // @inheritdoc IPIXLandStaking
    function setRewardDistributor(address _distributor) external override onlyOwner {
        require(_distributor != address(0), "Staking: INVALID_DISTRIBUTOR");
        s_rewardDistributor = _distributor;
    }

    // @inheritdoc IPIXLandStaking
    function stake(uint256 _tokenId, uint256 _amount) external override nonReentrant {
        IERC1155Upgradeable(s_pixLandmark).safeTransferFrom(
            msg.sender,
            address(this),
            _tokenId,
            _amount,
            ""
        );
        updateReward(_tokenId, msg.sender);

        bool isTokenIdStaked;
        for (uint256 i; i < s_stakedIDs[msg.sender].length; i += 1) {
            if (_tokenId == s_stakedIDs[msg.sender][i]) {
                isTokenIdStaked = true;
                break;
            }
        }
        if (!isTokenIdStaked) s_stakedIDs[msg.sender].push(_tokenId);

        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        rewardPool.tokensStaked += _amount;
        s_stakedAmounts[msg.sender][_tokenId] += _amount;

        emit PIXLandStaked(msg.sender, _tokenId, _amount);
    }

    // @inheritdoc IPIXLandStaking
    function unstake(uint256 _tokenId, uint256 _amount) external override nonReentrant {
        require(_tokenId > 0, "LandStaking: INVALID_TOKEN_ID");
        require(s_stakedAmounts[msg.sender][_tokenId] >= _amount, "LandStaking: NOT_ENOUGH");

        updateReward(_tokenId, msg.sender);

        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        rewardPool.tokensStaked -= _amount;
        s_stakedAmounts[msg.sender][_tokenId] -= _amount;

        if (s_stakedAmounts[msg.sender][_tokenId] == 0)
            _deleteFromStakedNFTs(msg.sender, _tokenId);

        uint256 reward = rewardPool.rewards[msg.sender];
        if (reward > 0) {
            rewardPool.rewards[msg.sender] = 0;
            s_rewardToken.safeTransfer(msg.sender, reward);
            emit RewardClaimed(msg.sender, reward);
        }

        IERC1155Upgradeable(s_pixLandmark).safeTransferFrom(
            address(this),
            msg.sender,
            _tokenId,
            _amount,
            ""
        );

        emit PIXLandUnstaked(msg.sender, _tokenId, _amount);
    }

    /**
     * @notice Claim rewards for multiple Landmarks
     * @notice emit {RewardClaimed} event
     */
    function claimBatch(uint256[] memory tokenIds) public {
        uint256 reward;
        for (uint256 i; i < tokenIds.length; i += 1) {
            RewardPool storage rewardPool = s_rewardPools[tokenIds[i]];
            updateReward(tokenIds[i], msg.sender);
            reward += rewardPool.rewards[msg.sender];
            rewardPool.rewards[msg.sender] = 0;
        }
        if (reward > 0) {
            s_rewardToken.safeTransfer(msg.sender, reward);
            emit RewardClaimed(msg.sender, reward);
        }
    }

    // @inheritdoc IPIXLandStaking
    function claim() external override {
        uint256 count;
        for (uint256 i; i < s_stakedIDs[msg.sender].length; i += 1) {
            if (s_stakedAmounts[msg.sender][s_stakedIDs[msg.sender][i]] == 0) continue;
            count++;
        }
        uint256 k;
        uint256[] memory tokenIds = new uint256[](count);
        for (uint256 i; i < s_stakedIDs[msg.sender].length; i += 1) {
            if (s_stakedAmounts[msg.sender][s_stakedIDs[msg.sender][i]] == 0) continue;
            tokenIds[k++] = s_stakedIDs[msg.sender][i];
        }
        claimBatch(tokenIds);
    }

    // @inheritdoc IPIXLandStaking
    function initializePools(
        uint256[] calldata _tokenIds,
        uint256[] calldata _poolRewards,
        uint256 _epochDuration
    ) external override onlyRewardDistributor
    {
        uint256 length = _tokenIds.length;
        require(length == _poolRewards.length, "Staking: INVALID_ARGUMENTS");
        uint256 totalRewards;
        for(uint256 i=0; i<length; i++) {
            updateReward(_tokenIds[i], address(0));
            RewardPool storage rewardPool = s_rewardPools[_tokenIds[i]];
            if (block.timestamp >= rewardPool.periodFinish) {
                rewardPool.rewardRate = _poolRewards[i] / _epochDuration;
            } else {
                uint256 remaining = rewardPool.periodFinish - block.timestamp;
                uint256 leftover = remaining * rewardPool.rewardRate;
                rewardPool.rewardRate = (_poolRewards[i] + leftover) / _epochDuration;
            }
            rewardPool.lastUpdateTime = block.timestamp;
            rewardPool.periodFinish = block.timestamp + _epochDuration;
            totalRewards += _poolRewards[i];
            emit RewardAdded(_tokenIds[i], _poolRewards[i]);
        }
        s_rewardToken.safeTransferFrom(msg.sender, address(this), totalRewards);
    }

    /**
     * @notice Internal function to delete address and tokenId from s_stakedIDs
     * @param _walletAddress The address to remove
     * @param _tokenId The tokenID to remove
     */
    function _deleteFromStakedNFTs(address _walletAddress, uint256 _tokenId) internal {
        for (uint256 i; i < s_stakedIDs[_walletAddress].length; i++) {
            if (s_stakedIDs[_walletAddress][i] == _tokenId) {
                s_stakedIDs[_walletAddress][i] = s_stakedIDs[_walletAddress][s_stakedIDs[_walletAddress].length - 1];
                s_stakedIDs[_walletAddress].pop();
            }
        }
    }

    /**
     * @notice internal function to update rewards before staking/unstaking and claiming rewards
     * @param _tokenId The token to update pool
     * @param _walletAddress The wallet address to update pool
     */
    function updateReward(uint256 _tokenId, address _walletAddress) internal {
        RewardPool storage rewardPool = s_rewardPools[_tokenId];
        rewardPool.rewardPerTokenStored = rewardPerToken(_tokenId, block.timestamp);
        rewardPool.lastUpdateTime = lastTimeRewardApplicable(_tokenId, block.timestamp);
        if (_walletAddress != address(0)) {
            rewardPool.rewards[_walletAddress] = earned(_walletAddress, _tokenId);
            rewardPool.userRewardPerTokenPaid[_walletAddress] = rewardPool.rewardPerTokenStored;
        }
    }
}
