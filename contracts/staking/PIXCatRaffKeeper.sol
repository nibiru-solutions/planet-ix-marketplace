//SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@chainlink/contracts/src/v0.8/automation/interfaces/AutomationCompatibleInterface.sol";

import "../interfaces/IPIXCatRaff.sol";

contract PIXCatRaffKeeper is AutomationCompatibleInterface {
    IPIXCatRaff public catRaff;

    constructor(address _catRaff) {
        catRaff = IPIXCatRaff(_catRaff);
    }

    function checkUpkeep(bytes calldata)
        external
        view
        override
        returns (bool upkeepNeeded, bytes memory performData)
    {
        uint256 week = catRaff.getCurrentWeek() - 1;
        upkeepNeeded = catRaff.drawable(week);
        performData = abi.encode(week);
    }

    function performUpkeep(bytes calldata performData) external override {
        uint256 week = abi.decode(performData, (uint256));

        catRaff.draw(week);
    }
}