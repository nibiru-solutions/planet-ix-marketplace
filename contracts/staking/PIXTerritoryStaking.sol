//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/IERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/utils/ERC721HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "../interfaces/IPIX.sol";
import "../interfaces/IPIXTerritoryStaking.sol";
import "hardhat/console.sol";

struct Epoch {
    uint256 startTimestamp;
    uint256 endTimestamp;
    mapping(IPIX.PIXSize => mapping(IPIX.PIXCategory => uint256)) pushedRewards;
    mapping(IPIX.PIXSize => mapping(IPIX.PIXCategory => uint256)) rewardPerSecond;
}

struct RewardPool {
    uint256 rewardPercentage;
    uint256 tokensStaked;
    uint256 totalPoolRewardPerToken;
    uint256 totalPoolRewardLastUpdated;
    uint256 currentRewardPerSecondRate;
    mapping(uint256 => uint256) pushedRewards; //epochNumber => pushed amount from previous epochs
}

contract PIXTerritoryStaking is OwnableUpgradeable, ReentrancyGuardUpgradeable, ERC721HolderUpgradeable, IPIXTerritoryStaking {
    using SafeERC20Upgradeable for IERC20Upgradeable;

    mapping(uint256 => address) public stakers;
    mapping(address => uint256[]) public stakedNFTs;

    IERC20Upgradeable public rewardToken;

    address public pixNFT;
    uint256 public totalTiers;  // disabled

    uint256 public constant DURATION = 10 days; // disabled
    uint256 public periodFinish; // disabled
    uint256 public rewardRate; // disabled
    uint256 public lastUpdateTime; // disabled
    uint256 public rewardPerTierStored; // disabled
    address public rewardDistributor;
    mapping(uint256 => uint256) public nftRewardPerTierPaid; // disabled
    mapping(uint256 => uint256) public rewards; // disabled

    ///////////////////////////////////////////////////////////////////////////

    mapping(address => bool) public moderators;

    uint256 public currentEpoch;
    uint256 public currentEpochEndTimestamp;

    mapping(IPIX.PIXSize => mapping(IPIX.PIXCategory => RewardPool)) rewardPools;

    uint256[] public rewardPercentagePaidPerTokensStaked;
    mapping(IPIX.PIXSize => uint256[]) public pushRewardNextEpochsPercentages; // how much to push to the next epochs
    uint256 public rewardPercentagesDenominator; // = 10000

    // tokenId => totalPoolRewardPerToken at the moment when tokenId last claimed or staked
    mapping(uint256 => uint256) public tokenTotalRewardLastClaim; 
    bool public paused;

    function initialize(address _pixt, address _pixNFT) external initializer {
        require(_pixt != address(0), "Staking: INVALID_PIXT");
        require(_pixNFT != address(0), "Staking: INVALID_PIX");
        __Ownable_init();
        __ReentrancyGuard_init();
        __ERC721Holder_init();

        rewardToken = IERC20Upgradeable(_pixt);
        pixNFT = _pixNFT;
    }

    modifier onlyRewardDistributor() {
        require(msg.sender == rewardDistributor, "Staking: NON_DISTRIBUTOR");
        _;
    }

    modifier onlyMod() {
        require(moderators[msg.sender], "Staking: NON_MODERATOR");
        _;
    }

    modifier whenNotPaused() {
        require(paused == false, "Staking: CONTRACT_PAUSED");
        _;
    }

    modifier onlyEOA() {
        require(msg.sender == tx.origin, "Staking: ONLY_EOA");
        _;
    }

    function pause() external onlyOwner whenNotPaused {
        paused = true;
    }

    function unpause() external onlyOwner {
        require(paused == true, "Staking: CONTRACT_UNPAUSED");
        paused = false;
    }

    function setModerator(address moderator, bool approved) external override onlyOwner {
        require(moderator != address(0), "Staking: INVALID_MODERATOR");
        moderators[moderator] = approved;
        emit ModeratorUpdated(moderator, approved);
    }

    function setRewardToken(address rewardTokenAddress) external override onlyRewardDistributor {
        rewardToken = IERC20Upgradeable(rewardTokenAddress);
    }

    /**
     * @dev called by admin to initialize new pool rewards mechanics. For each mini pool (size+categoty) 
     *   we set percentage of total rewards which will go to the mini pool in that epoch.
     *   Also, we set number of currently staked tokens in the contract if we upgrade on existing one.
     * @param pixSize array of sizes
     * @param pixCategory array of categories
     * @param rewardPercentages corresponding percentage which mini pool should get
     * @param currentStakedTokens corresponding number of staked tokens
     * @param _rewardPercentagesDenominator percentage denominator
     */
    function initializePoolRewards(
        IPIX.PIXSize[] calldata pixSize, 
        IPIX.PIXCategory[] calldata pixCategory,
        uint256[] calldata rewardPercentages,
        uint256[] calldata currentStakedTokens,
        uint256 _rewardPercentagesDenominator
    ) external override onlyMod {
        uint256 length = pixSize.length;
        require(length == pixCategory.length, "Staking: ARRAY_LENGTH_NOT_EQUAL");
        require(length == rewardPercentages.length, "Staking: ARRAY_LENGTH_NOT_EQUAL");
        require(length == currentStakedTokens.length, "Staking: ARRAY_LENGTH_NOT_EQUAL");

        for(uint256 i=0; i<length; i++) {
            RewardPool storage rewardPool = rewardPools[pixSize[i]][pixCategory[i]];
            rewardPool.rewardPercentage = rewardPercentages[i];
            rewardPool.tokensStaked = currentStakedTokens[i];
        }

        rewardPercentagesDenominator = _rewardPercentagesDenominator;
    }

    function setRewardPercentagePaidPerTokensStaked(
        uint256[] calldata _rewardPercentagePaidPerTokensStaked
    ) external override onlyMod {
        rewardPercentagePaidPerTokensStaked = _rewardPercentagePaidPerTokensStaked;
    }

    function setPushRewardNextEpochsPercentages(
        IPIX.PIXSize pixSize, 
        uint256[] calldata _pushRewardNextEpochsPercentages
    ) external override onlyMod {
        pushRewardNextEpochsPercentages[pixSize] = _pushRewardNextEpochsPercentages;
    }

    function setRewardDistributor(address distributor) external override onlyOwner {
        require(distributor != address(0), "Staking: INVALID_DISTRIBUTOR");
        rewardDistributor = distributor;
    }

    function _rewardPerTokenFromLastUpdate(RewardPool storage rewardPool, uint256 untilTimestamp) internal view returns(uint256) {

        if (rewardPool.tokensStaked == 0) {
            return 0;
        }

        uint256 startTime = rewardPool.totalPoolRewardLastUpdated;
        uint256 untilTime = (untilTimestamp < currentEpochEndTimestamp) ? untilTimestamp : currentEpochEndTimestamp;

        if (untilTime <= startTime) return 0;

        uint256 reward = rewardPool.currentRewardPerSecondRate * (untilTime - startTime);
        uint256 rewardPerToken = reward / rewardPool.tokensStaked;

        return rewardPerToken;
    }

    function _updateRewards(RewardPool storage rewardPool) internal {
        rewardPool.totalPoolRewardPerToken += _rewardPerTokenFromLastUpdate(rewardPool, block.timestamp);
        rewardPool.totalPoolRewardLastUpdated = block.timestamp;
    }

    function stake(uint256 tokenId) public override nonReentrant whenNotPaused onlyEOA {
        IERC721Upgradeable(pixNFT).safeTransferFrom(msg.sender, address(this), tokenId);

        IPIX.PIXInfo memory info = IPIX(pixNFT).getInfo(tokenId);

        require(info.size != IPIX.PIXSize.Pix, "Staking: TERRITORY_ONLY");
        
        RewardPool storage rewardPool = rewardPools[info.size][info.category];

        _updateRewards(rewardPool);

        rewardPool.tokensStaked += 1;
        tokenTotalRewardLastClaim[tokenId] = rewardPool.totalPoolRewardPerToken;

        stakers[tokenId] = msg.sender;
        stakedNFTs[msg.sender].push(tokenId);
        
        emit PIXStaked(tokenId, msg.sender);
    }

    function _deleteFromStakedNFTs(address user, uint256 tokenId) internal {
        for (uint256 i; i < stakedNFTs[user].length; i++) {
            if (stakedNFTs[user][i] == tokenId) {
                stakedNFTs[user][i] = stakedNFTs[user][stakedNFTs[user].length - 1];
                stakedNFTs[user].pop();
            }
        }
    }

    function unstake(uint256 tokenId) external override nonReentrant whenNotPaused onlyEOA {
        require(tokenId > 0, "Staking: INVALID_TOKEN_ID");
        require(stakers[tokenId] == msg.sender, "Staking: NOT_STAKER");

        IPIX.PIXInfo memory info = IPIX(pixNFT).getInfo(tokenId);
        RewardPool storage rewardPool = rewardPools[info.size][info.category];

        _updateRewards(rewardPool);

        uint256 reward = _claimAndUpdateToken(tokenId);

        rewardPool.tokensStaked -= 1;

        delete stakers[tokenId];
        _deleteFromStakedNFTs(msg.sender, tokenId);

        if (reward > 0) {
            rewardToken.safeTransfer(msg.sender, reward);
            emit RewardClaimed(reward, msg.sender);
        }

        IERC721Upgradeable(pixNFT).safeTransferFrom(address(this), msg.sender, tokenId);
        emit PIXUnstaked(tokenId, msg.sender);
    }

    function _calcRewardsToPush(uint256 _tokensStaked, uint256 totalReward) internal view returns (uint256) {
        if (rewardPercentagePaidPerTokensStaked.length <= _tokensStaked) {
            return 0;
        }

        uint256 rewardStay = (totalReward * rewardPercentagePaidPerTokensStaked[_tokensStaked]) / rewardPercentagesDenominator;
        uint256 rewardPush = totalReward - rewardStay;

        return rewardPush;
    }

    function _pushRewards(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory, uint256 poolReward) internal returns (uint256) {

        RewardPool storage rewardPool = rewardPools[pixSize][pixCategory];
        
        uint256 rewardsToPush = _calcRewardsToPush(rewardPool.tokensStaked, poolReward);

        if (rewardsToPush > 0) {
            uint256 len = pushRewardNextEpochsPercentages[pixSize].length;
            for(uint256 i=1; i<len; i++) {
                rewardPool.pushedRewards[currentEpoch + i] +=
                     (rewardsToPush * pushRewardNextEpochsPercentages[pixSize][i]) / rewardPercentagesDenominator;
            }
        }

        return rewardsToPush;
    }

    function setDividendPayout(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory, uint256 poolReward) internal returns (uint256) {

        RewardPool storage rewardPool = rewardPools[pixSize][pixCategory];

        uint256 len = pushRewardNextEpochsPercentages[pixSize].length - 1;
        uint256 monthlyDividend = poolReward / len;
        if (len > 1) {
            for(uint256 i=1; i<len; i++) {
                rewardPool.pushedRewards[currentEpoch + i] += monthlyDividend;
            }
        }

        return monthlyDividend;
    }

    function addEpochRewards(uint256 epochTotalReward, uint256 epochDuration) external override onlyRewardDistributor {
        require(currentEpochEndTimestamp < block.timestamp, "Staking: EPOCH_NOT_FINISHED");

        currentEpoch++;

        for(uint256 size=1; size<=uint256(type(IPIX.PIXSize).max); size++) {
            IPIX.PIXSize pixSize = IPIX.PIXSize(size);
            for(uint256 category=0; category<=uint256(type(IPIX.PIXCategory).max); category++) {
                IPIX.PIXCategory pixCategory = IPIX.PIXCategory(category);

                RewardPool storage rewardPool = rewardPools[pixSize][pixCategory];
                _updateRewards(rewardPool);

                uint256 rewardForPool = rewardPool.pushedRewards[currentEpoch];
                uint256 totalRewardForPool = (epochTotalReward * rewardPool.rewardPercentage) / rewardPercentagesDenominator;
                uint256 dividendPayout = setDividendPayout(pixSize, pixCategory, totalRewardForPool);
                rewardForPool += dividendPayout;

                uint256 rewardsPushedToNextEpochs = _pushRewards(pixSize, pixCategory, rewardForPool);

                rewardForPool -= rewardsPushedToNextEpochs;

                rewardPool.currentRewardPerSecondRate = rewardForPool / epochDuration;
            }
        }

        currentEpochEndTimestamp = block.timestamp + epochDuration;

        rewardToken.safeTransferFrom(msg.sender, address(this), epochTotalReward);
        emit EpochAdded(currentEpoch, block.timestamp, currentEpochEndTimestamp, epochTotalReward);
    }

    function earnedReward(uint256 tokenId) public override view returns(uint256) {
        return earnedRewardUntilTimestamp(tokenId, block.timestamp);
    }

    function earnedRewardUntilTimestamp(uint256 tokenId, uint256 untilTimestamp) public override view returns(uint256) {
        if (!isStaked(tokenId)) {
            return 0;
        }
        IPIX.PIXInfo memory info = IPIX(pixNFT).getInfo(tokenId);
        RewardPool storage rewardPool = rewardPools[info.size][info.category];
        uint256 totalReward = 
           rewardPool.totalPoolRewardPerToken + _rewardPerTokenFromLastUpdate(rewardPool, untilTimestamp);
        return totalReward - tokenTotalRewardLastClaim[tokenId];
    }

    function earnedBatch(uint256[] memory tokenIds) public override view returns (uint256[] memory) {
        return earnedBatchUntilTimestamp(tokenIds, block.timestamp);
    }

    function earnedBatchUntilTimestamp(uint256[] memory tokenIds, uint256 untilTimestamp) public override view returns (uint256[] memory) {
        uint256[] memory earneds = new uint256[](tokenIds.length);
        for (uint256 i; i < tokenIds.length; i += 1) {
            earneds[i] = earnedRewardUntilTimestamp(tokenIds[i], untilTimestamp);
        }
        return earneds;
    }

    function earnedByAccount(address account) public override view returns (uint256) {
        return earnedByAccountUntilTimestamp(account, block.timestamp);
    }

    function earnedByAccountUntilTimestamp(address account, uint256 untilTimestamp) public override view returns (uint256) {
        uint256 len = stakedNFTs[account].length;
        uint256 reward = 0;

        for (uint256 i = 0; i < len; i++) {
            uint256 tokenId = stakedNFTs[account][i];
            if (stakers[tokenId] != account) continue;

            bool repeat = false;
            for(uint256 j=0; j<i && !repeat; j++) {
                if (stakedNFTs[account][j] == tokenId) {
                    repeat = true;
                }
            }

            if (!repeat) {
                reward += earnedRewardUntilTimestamp(tokenId, untilTimestamp);
            }
        }

        return reward;
    }

    function _claimAndUpdateToken(uint256 tokenId) internal returns(uint256) {
        require(stakers[tokenId] == msg.sender, "Staking: NOT_STAKER");
        IPIX.PIXInfo memory info = IPIX(pixNFT).getInfo(tokenId);
        RewardPool storage rewardPool = rewardPools[info.size][info.category];

        _updateRewards(rewardPool);

        uint256 reward = earnedRewardUntilTimestamp(tokenId, block.timestamp);
        tokenTotalRewardLastClaim[tokenId] = rewardPool.totalPoolRewardPerToken;

        emit RewardClaimedToken(msg.sender, tokenId, reward);

        return reward;
    }

    function claimBatch(uint256[] memory tokenIds) public override whenNotPaused onlyEOA {
        uint256 reward;
        for (uint256 i; i < tokenIds.length; i += 1) {
            reward += _claimAndUpdateToken(tokenIds[i]);
        }

        if (reward > 0) {
            rewardToken.safeTransfer(msg.sender, reward);
            emit RewardClaimed(reward, msg.sender);
        }
    }

    function claimAll() external override whenNotPaused onlyEOA {
        uint256 reward;
        uint256 len = stakedNFTs[msg.sender].length;
        for (uint256 i = 0; i < len; i += 1) {
            uint256 tokenId = stakedNFTs[msg.sender][i];

            // delete old tokens which are not staked anymore
            while(len > i && stakers[tokenId] != msg.sender) {
                tokenId = stakedNFTs[msg.sender][len - 1];
                stakedNFTs[msg.sender].pop();
                len--;
            }

            if (i < len) {
                stakedNFTs[msg.sender][i] = tokenId;
                reward += _claimAndUpdateToken(tokenId); 
            }       
        }

        if (reward > 0) {
            rewardToken.safeTransfer(msg.sender, reward);
            emit RewardClaimed(reward, msg.sender);
        }
    }

    function isStaked(uint256 tokenId) public override view returns (bool) {
        return stakers[tokenId] != address(0);
    }

    function getStakedNFTsLength(address user) external override view returns(uint256) {
        return stakedNFTs[user].length;
    }

    function getStakedNFTs(address user) external override view returns(uint256[] memory) {
        return stakedNFTs[user];
    }

    function getPushedRewards(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory, uint256 epochId) external override view returns(uint256) {
        return rewardPools[pixSize][pixCategory].pushedRewards[epochId];
    }

    function getRewardPerToken(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external override view returns(uint256) {
        return rewardPools[pixSize][pixCategory].totalPoolRewardPerToken;
    }

    function getCurrentRewardPerSecond(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external override view returns(uint256) {
        return rewardPools[pixSize][pixCategory].currentRewardPerSecondRate;
    }

    function getTokensStaked(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external override view returns(uint256) {
        return rewardPools[pixSize][pixCategory].tokensStaked;
    }
}
