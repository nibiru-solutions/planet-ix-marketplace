//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "../interfaces/IPIXCatRaffV2.sol";

interface IVRFManager {
    function getRandomNumber(uint32 numWords) external returns (uint256 requestId);
}

interface IRNGConsumer {
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] calldata randomWords) external;
}

contract PIXCatRaffV2 is IPIXCatRaffV2, IRNGConsumer, OwnableUpgradeable, PausableUpgradeable {
    using SafeERC20 for IERC20;

    /* ========== DATA ========== */
    struct WeeklyTicketInfo {
        uint256 prize;
        uint256 totalTickets;
        address[] players;
        uint256[] sum;
        mapping(address => uint256) playerTickets;
    }

    /* ========== STATE VARIABLES ========== */

    IERC20 public pixToken;
    uint256 public ticketPrice;

    address public treasury;
    address public orderAutomation;
    uint256 public weeklyPrize;
    uint256 public firstWeek;

    mapping(uint256 => address[]) public weeklyWinners;
    mapping(uint256 => uint256) public drawRequests;
    mapping(uint256 => WeeklyTicketInfo) public weeklyTickets;
    mapping(address => uint256) public prizes;

    mapping(uint256 => bool) private _drawn;

    uint256 public constant SECONDS_PER_WEEK = 1 weeks;
    uint8[5] public POOL_PERCENT;

    IVRFManager public vrfManager;

    modifier onlyOracle() {
        if (msg.sender != address(vrfManager)) revert PIXCatRaffV2__NotOracle();
        _;
    }

    function initialize(
        address _pixToken,
        address _vrfManager,
        address _treasury,
        uint256 _ticketPrice,
        uint256 _weeklyPrize
    ) public initializer {
        __Ownable_init();
        __Pausable_init();
        require(_pixToken != address(0), "invalid token");
        require(_treasury != address(0), "invalid treasury");
        require(_ticketPrice > 0, "invalid ticket price");
        require(_weeklyPrize > 0, "invalid weekly prize");

        pixToken = IERC20(_pixToken);
        treasury = _treasury;
        ticketPrice = _ticketPrice;
        weeklyPrize = _weeklyPrize;

        vrfManager = IVRFManager(_vrfManager);

        firstWeek = block.timestamp / SECONDS_PER_WEEK;
        POOL_PERCENT = [40, 27, 19, 10, 4];
    }

    /* ========== MUTATIVE FUNCTIONS ========== */

    function addTickets(uint256 _tickets) external whenNotPaused {
        require(_tickets > 0, "tickets must be > 0");

        uint256 week = getCurrentWeek();
        require(!_drawn[week], "already drawn");

        WeeklyTicketInfo storage info = weeklyTickets[week];
        info.playerTickets[msg.sender] += _tickets;
        info.totalTickets += _tickets;
        info.players.push(msg.sender);
        info.sum.push(info.totalTickets);

        pixToken.safeTransferFrom(msg.sender, address(this), _tickets * ticketPrice);

        emit TicketsAdded(week, msg.sender, _tickets, block.timestamp);
    }

    function addOldTickets(
        uint256 _week,
        uint256 _tickets,
        bool _max
    ) external whenNotPaused {
        require(_drawn[_week], "not drawn");

        uint256 week = getCurrentWeek();
        require(!_drawn[week], "already drawn");

        WeeklyTicketInfo storage oldInfo = weeklyTickets[_week];
        uint256 tickets = oldInfo.playerTickets[msg.sender];
        if (!_max) {
            require(tickets >= _tickets, "invalid tickets");
            tickets = _tickets;
        }
        require(tickets > 0, "tickets must be > 0");
        oldInfo.playerTickets[msg.sender] -= tickets;

        WeeklyTicketInfo storage info = weeklyTickets[week];
        info.playerTickets[msg.sender] += tickets;
        info.totalTickets += tickets;
        info.players.push(msg.sender);
        info.sum.push(info.totalTickets);

        emit TicketsWithdrawn(_week, msg.sender, tickets);
        emit TicketsAdded(week, msg.sender, tickets, block.timestamp);
    }

    function withdrawOldTickets(
        uint256 _week,
        uint256 _tickets,
        bool _max
    ) external whenNotPaused {
        require(_drawn[_week], "not drawn");

        uint256 week = getCurrentWeek();
        require(_week + 1 < week, "locked");

        WeeklyTicketInfo storage info = weeklyTickets[_week];
        uint256 tickets = info.playerTickets[msg.sender];
        if (!_max) {
            require(tickets >= _tickets, "invalid tickets");
            tickets = _tickets;
        }
        require(tickets > 0, "tickets must be > 0");
        info.playerTickets[msg.sender] -= tickets;

        pixToken.safeTransfer(msg.sender, tickets * ticketPrice);

        emit TicketsWithdrawn(_week, msg.sender, tickets);
    }

    function claimPrize() external {
        uint256 prize = prizes[msg.sender];
        require(prize > 0, "no prize");

        prizes[msg.sender] = 0;
        pixToken.safeTransferFrom(treasury, msg.sender, prize);

        emit PrizeClaimed(msg.sender, prize);
    }

    /* ========== RESTRICTED FUNCTIONS ========== */

    function setPoolPercent(uint8[5] memory _poolPercent) external onlyOwner {
        require(
            _poolPercent[0] +
                _poolPercent[1] +
                _poolPercent[2] +
                _poolPercent[3] +
                _poolPercent[4] ==
                100,
            "invalid sum"
        );

        POOL_PERCENT[0] = _poolPercent[0];
        POOL_PERCENT[1] = _poolPercent[1];
        POOL_PERCENT[2] = _poolPercent[2];
        POOL_PERCENT[3] = _poolPercent[3];
        POOL_PERCENT[4] = _poolPercent[4];
    }

    function addPrize(uint256 _prize) external onlyOwner whenNotPaused {
        uint256 week = getCurrentWeek();
        require(!_drawn[week], "already drawn");

        WeeklyTicketInfo storage info = weeklyTickets[week];
        info.prize += _prize;

        emit PrizeAdded(week, _prize, block.timestamp);
    }

    function draw(uint256 _week) external override whenNotPaused {
        require(msg.sender == orderAutomation, "invalid order automation");
        require((_week + firstWeek + 1) * SECONDS_PER_WEEK < block.timestamp, "invalid week");

        WeeklyTicketInfo storage info = weeklyTickets[_week];
        require(info.totalTickets > 0, "no tickets");

        require(!_drawn[_week], "draw already requested");
        _drawn[_week] = true;

        uint256 requestID = getRandomNumber();
        drawRequests[requestID] = _week;

        emit DrawRequested(requestID, _week, info.totalTickets, info.prize + weeklyPrize);
    }

    function setTreasury(address _treasury) public onlyOwner {
        require(_treasury != address(0), "invalid treasury");
        treasury = _treasury;
    }

    function setOrderAutomation(address _orderAutomation) external onlyOwner {
        require(_orderAutomation != address(0), "invalid order automation");
        orderAutomation = _orderAutomation;
    }

    function setWeeklyPrize(uint256 _weeklyPrize) external onlyOwner {
        require(_weeklyPrize > 0, "invalid weekly prize");
        weeklyPrize = _weeklyPrize;
    }

    function pause() external onlyOwner whenNotPaused {
        _pause();
    }

    function unpause() external onlyOwner whenPaused {
        _unpause();
    }

    /* ========== INTERNALS ========== */

    /**
     * @notice Request random words
     */
    function getRandomNumber() internal returns (uint256 requestID) {
        requestID = vrfManager.getRandomNumber(5);
    }

    /**
     * @notice Callback function used by ChainLink's VRF v2.5 Coordinator
     */
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] calldata randomWords) external override onlyOracle {
        uint256 week = drawRequests[requestId];
        WeeklyTicketInfo storage info = weeklyTickets[week];

        uint8 i;
        uint256 left;
        uint256 right;
        uint256 mid;
        uint256 rand;

        for (i = 0; i < 5; i++) {
            rand = randomWords[i] % info.totalTickets;
            left = 0;
            right = info.players.length - 1;
            while (left < right) {
                mid = (left + right) / 2;
                if (rand >= info.sum[mid]) {
                    left = mid + 1;
                } else if (rand < info.sum[mid]) {
                    right = mid;
                }
            }

            address winner = info.players[left];
            weeklyWinners[week].push(winner);
            prizes[winner] += ((info.prize + weeklyPrize) * POOL_PERCENT[i]) / 100;
        }

        emit ResultDrawn(week);
    }

    /* ========== VIEWS ========== */

    function getCurrentWeek() public view override returns (uint256) {
        uint256 currentWeek = block.timestamp / SECONDS_PER_WEEK - firstWeek;
        return currentWeek;
    }

    function winnings(uint256 _week) external view returns (address[] memory, uint256[] memory) {
        WeeklyTicketInfo storage info = weeklyTickets[_week];
        uint256[] memory weeklyPrizes = new uint256[](weeklyWinners[_week].length);
        for (uint256 i; i < weeklyWinners[_week].length; i++) {
            weeklyPrizes[i] = ((info.prize + weeklyPrize) * POOL_PERCENT[i]) / 100;
        }
        return (weeklyWinners[_week], weeklyPrizes);
    }

    function drawable(uint256 _week) external view override returns (bool) {
        if (paused()) {
            return false;
        }
        if (_drawn[_week]) {
            return false;
        }
        if ((_week + firstWeek + 1) * SECONDS_PER_WEEK >= block.timestamp) {
            return false;
        }

        WeeklyTicketInfo storage info = weeklyTickets[_week];
        if (info.totalTickets == 0) {
            return false;
        }

        return true;
    }

    function ticketsOf(uint256 _week, address _player) external view returns (uint256) {
        return weeklyTickets[_week].playerTickets[_player];
    }

    function ticketsOf(address _player) external view returns (uint256) {
        uint256 week = getCurrentWeek();
        return weeklyTickets[week].playerTickets[_player];
    }

    function pastTicketsOf(address _player) external view returns (uint256[] memory) {
        uint256 week = getCurrentWeek() + 1;
        uint256[] memory pastTickets = new uint256[](week);

        for (uint256 i = 0; i < week; i++) {
            pastTickets[i] = weeklyTickets[i].playerTickets[_player];
        }

        return pastTickets;
    }
}
