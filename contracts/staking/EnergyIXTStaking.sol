//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/IEnergy.sol";
import "../interfaces/IEnergyIXTStaking.sol";
import "../mission_control/IAssetManager.sol";
import "abdk-libraries-solidity/ABDKMathQuad.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";

/**
 * @title EnergyIXTStaking
 * @author Jourdan
 * @notice This is a contract for staking energy and IXT
 */
contract EnergyIXTStaking is IEnergyIXTStaking, OwnableUpgradeable, ReentrancyGuardUpgradeable {
    struct Stake {
        uint256 amount;
        uint256 stakedAt;
    }
    /// @notice latest epoch
    uint256 public s_epochCount;
    /// @notice percentages of unused rewards to be pushed to later epochs
    uint256[] private s_pushRewardToNextEpochPercentages;
    /// @notice denominator used to calculate future epoch portions
    uint256 private s_rewardsPercentageDenominator;
    /// @notice percentage to be donated to amelia foundation
    uint256 private s_ameliaFoundationShare;
    /// @notice denominator used to calculate amelia foundation portion
    uint256 private s_ameliaFoundationMaxBps;

    /// @notice struct containing information on reward pool
    RewardPool private s_rewardPool;

    /// @notice wallet from which rewards will be added
    address private s_rewardDistributor;
    /// @notice amelia foundation wallet
    address private s_ameliaFoundation;

    /// @notice IXT contract
    IERC20Upgradeable public pixt;
    /// @notice Energy Contract(Asset Manager)
    IEnergy public energy;

    /// @notice Amount a user currently has staked
    mapping(address => uint256) public userStakedAmount;

    /// @notice moderators
    mapping(address => bool) public moderators;

    mapping(address => uint256) public userAmeliaCommitment;

    bool public paused;

    mapping(address => Stake[]) public userIXTStakes;
    mapping(address => uint256) public userTotalIXTStaked;

    mapping(address => mapping(uint256 => uint256)) public userEnergyStaked;
    mapping(address => mapping(uint256 => uint256)) public userEnergyStakedAmelia;

    mapping(uint256 => RewardPool) public rewardPool;

    uint256 private constant ENERGY_ID = 24;
    uint256 private constant LOCKUP_PERIOD = 30 days;

    /*------------------- MODIFIERS -------------------*/

    /// @notice allows calls from only reward distributor
    modifier onlyRewardDistributor() {
        require(msg.sender == s_rewardDistributor, "ENERGY STAKING: NON_DISTRIBUTOR");
        _;
    }

    /// @notice allows calls from only moderators
    modifier onlyGov() {
        require(msg.sender == owner() || moderators[msg.sender], "ENERGY STAKING: NON_MODERATOR");
        _;
    }

    ///@notice allows function calls only when not paused
    modifier whenNotPaused() {
        require(!paused, "ENERGY STAKING: FEATURE PAUSED");
        _;
    }

    /*------------------- INITIALIZER -------------------*/

    function initialize(IERC20Upgradeable _pixt, IEnergy _energy) public initializer {
        __Ownable_init();
        __ReentrancyGuard_init();

        pixt = _pixt;
        energy = _energy;
    }

    /*---------------- END - USER FUNCTIONS ----------------*/

    function stakeIXT(uint256 _amount) external nonReentrant whenNotPaused {
        require(_amount > 0, "ENERGY STAKING: CANT STAKE 0");
        userIXTStakes[msg.sender].push(Stake({amount: _amount, stakedAt: block.timestamp}));
        userTotalIXTStaked[msg.sender] += _amount;
        pixt.transferFrom(msg.sender, address(this), _amount * 1 ether);
    }

    /// @inheritdoc IEnergyIXTStaking
    function unstakeIXT(uint256 _amount) external nonReentrant {
        require(_amount > 0, "ENERGY STAKING: CANT UNSTAKE 0");
        require(userTotalIXTStaked[msg.sender] >= _amount, "ENERGY STAKING: INSUFFICIENT AMOUNT STAKED");

        _updateStake(msg.sender, _amount);
        pixt.transfer(msg.sender, _amount * 1 ether);
    }

    /// @inheritdoc IEnergyIXTStaking
    function stakeEnergy(uint256 _amount, bool _ameliaCommitment) external nonReentrant {
        uint256 totalIXTStaked = getUserTotalIXTStaked(msg.sender);
        uint256 totalEnergyStaked = getUserTotalEnergyStaked(msg.sender);

        require(_amount > 0, "ENERGY STAKING: CANT STAKE 0");
        require(totalEnergyStaked + _amount <= totalIXTStaked, "ENERGY STAKING: NEED TO STAKE MORE IXT");

        _updateRewards(msg.sender, s_epochCount);
        rewardPool[s_epochCount].tokensStaked += _amount;

        if (_ameliaCommitment) {
            userEnergyStakedAmelia[msg.sender][s_epochCount] += _amount;
        } else {
            userEnergyStaked[msg.sender][s_epochCount] += _amount;
        }

        energy.trustedBurn(msg.sender, ENERGY_ID, _amount);
    }

    /// @inheritdoc IEnergyIXTStaking
    function claim(bool _ameliaCommitment) external nonReentrant {
        _claimOld(_ameliaCommitment);
        for (uint256 i = 1; i <= s_epochCount; i++) _claim(_ameliaCommitment, i);
    }

    function _claim(bool _ameliaCommitment, uint256 _epoch) internal {
        _updateRewards(msg.sender, _epoch);
        _proportionAndTransferRewards(_ameliaCommitment, _epoch);
    }

    /*---------------- STAKING UTILS ----------------*/

    function _updateStake(address _user, uint256 _amount) internal {
        Stake[] memory userStaked = userIXTStakes[_user];

        uint256 remaining = _amount;
        for (uint256 i; i < userStaked.length; i++) {
            if (block.timestamp > (userStaked[i].stakedAt + LOCKUP_PERIOD)) {
                if (remaining >= userStaked[i].amount) {
                    remaining -= userStaked[i].amount;
                    _removeFromArray(0, userIXTStakes[_user]);
                } else {
                    userIXTStakes[_user][0].amount -= remaining;
                    remaining = 0;
                }
            }
        }
        userTotalIXTStaked[_user] -= _amount;
        require(remaining == 0, "ENERGY STAKING: NOT_ENOUGH_TOKENS_UNLOCKED");
    }

    function _removeFromArray(uint256 _position, Stake[] storage _arr) internal {
        for (uint256 i = _position; i < _arr.length - 1; i++) {
            _arr[i] = _arr[i + 1];
        }
        _arr.pop();
    }

    function _proportionAndTransferRewards(bool _ameliaCommitment, uint256 _epoch) internal {
        uint256 rewardToPay;
        if (_ameliaCommitment) {
            uint256 ameliaPortion = (rewardPool[_epoch].ameliaRewards[msg.sender] * s_ameliaFoundationShare) / s_ameliaFoundationMaxBps;
            rewardToPay = rewardPool[_epoch].ameliaRewards[msg.sender] - ameliaPortion;
            rewardPool[_epoch].ameliaRewards[msg.sender] = 0;
            pixt.transfer(s_ameliaFoundation, ameliaPortion);
        } else {
            rewardToPay = rewardPool[_epoch].rewards[msg.sender];
            rewardPool[_epoch].rewards[msg.sender] = 0;
        }
        pixt.transfer(msg.sender, rewardToPay);
    }

    /**
     * @notice Updates rewards within pool based on the number of IXT staked
     * @param _walletAddress address of a user whose rewards we wish to update
     */
    function _updateRewards(address _walletAddress, uint256 _epoch) internal {
        rewardPool[_epoch].rewardPerTokenStored = rewardPerTokenStored(_epoch);
        rewardPool[_epoch].lastUpdateTime = lastUpdateTimeApplicable(block.timestamp, _epoch);

        if (_walletAddress != address(0)) {
            rewardPool[_epoch].rewards[_walletAddress] = earned(_walletAddress, _epoch);
            rewardPool[_epoch].ameliaRewards[_walletAddress] = earnedAmelia(_walletAddress, _epoch);
            rewardPool[_epoch].userRewardPerTokenPaid[_walletAddress] = rewardPool[_epoch].rewardPerTokenStored;
        }
    }

    /// @inheritdoc IEnergyIXTStaking
    function earned(address _walletAddress, uint256 _epoch) public view returns (uint256) {
        return (userEnergyStaked[_walletAddress][_epoch] * (rewardPerTokenStored(_epoch) - rewardPool[_epoch].userRewardPerTokenPaid[_walletAddress])) + rewardPool[_epoch].rewards[_walletAddress];
    }

    function earnedAmelia(address _walletAddress, uint256 _epoch) public view returns (uint256) {
        return
            (userEnergyStakedAmelia[_walletAddress][_epoch] * (rewardPerTokenStored(_epoch) - rewardPool[_epoch].userRewardPerTokenPaid[_walletAddress])) +
            rewardPool[_epoch].ameliaRewards[_walletAddress];
    }

    /// @notice Finds the reward per token based on the number of staked tokens and the time that has passed
    function rewardPerTokenStored(uint256 _epoch) public view returns (uint256) {
        if (rewardPool[_epoch].tokensStaked == 0) {
            return rewardPool[_epoch].rewardPerTokenStored;
        }

        uint256 startTime = rewardPool[_epoch].lastUpdateTime;
        uint256 untilTime = lastUpdateTimeApplicable(block.timestamp, _epoch);
        uint256 reward = rewardPool[_epoch].rewardRate * (untilTime - startTime);

        return rewardPool[_epoch].rewardPerTokenStored + (reward / rewardPool[_epoch].tokensStaked);
    }

    /// @notice Finds the last update time that was applicable, does not allow it to go beyond the epoch finish time
    function lastUpdateTimeApplicable(uint256 _untilTimeStamp, uint256 _epoch) public view returns (uint256) {
        uint256 effectiveTimeStamp = _untilTimeStamp < rewardPool[_epoch].periodFinish ? _untilTimeStamp : rewardPool[_epoch].periodFinish;

        return effectiveTimeStamp;
    }

    /*---------------- ADMIN FUNCTIONS ----------------*/

    /// @inheritdoc IEnergyIXTStaking
    function addCurrentEpochRewards(uint256 _poolTotalReward, uint256 _epochDuration) external onlyRewardDistributor {
        require(rewardPool[s_epochCount].periodFinish < block.timestamp, "ENERGY STAKING: EPOCH NOT YET FINISHED");

        unchecked {
            ++s_epochCount;
        }

        _updateRewards(address(0), s_epochCount);

        uint256 unlockedRewards = _facilityCalculation(_poolTotalReward);
        uint256 lockedRewards = _poolTotalReward - unlockedRewards;

        uint256 totalRewardForThisEpoch = unlockedRewards + s_rewardPool.pushedRewards[s_epochCount];
        _pushLockedPoolRewardToNextEpochs(lockedRewards);

        rewardPool[s_epochCount].rewardRate = totalRewardForThisEpoch / _epochDuration;

        rewardPool[s_epochCount].lastUpdateTime = block.timestamp;
        rewardPool[s_epochCount].periodFinish = block.timestamp + _epochDuration;

        pixt.transferFrom(s_rewardDistributor, address(this), _poolTotalReward);

        emit RewardAdded(_poolTotalReward, _epochDuration);
    }

    function addRewardsToCurrentEpoch(uint256 _poolTotalReward, uint256 _epochDuration) external onlyRewardDistributor {
        require(rewardPool[s_epochCount].periodFinish > block.timestamp, "ENERGY STAKING: EPOCH FINISHED");

        _updateRewards(address(0), s_epochCount);

        uint256 unlockedRewards = _facilityCalculation(_poolTotalReward);
        uint256 lockedRewards = _poolTotalReward - unlockedRewards;

        uint256 totalRewardForThisEpoch = unlockedRewards;
        _pushLockedPoolRewardToNextEpochs(lockedRewards);

        rewardPool[s_epochCount].rewardRate += totalRewardForThisEpoch / _epochDuration;

        rewardPool[s_epochCount].lastUpdateTime = block.timestamp;
        rewardPool[s_epochCount].periodFinish = block.timestamp + _epochDuration;

        pixt.transferFrom(s_rewardDistributor, address(this), _poolTotalReward);

        emit RewardAdded(_poolTotalReward, _epochDuration);
    }

    /**
     * @notice calculates how much rewards should be unlocked in the current epoch based on the number of facilities in existence
     * @param _totalRewards total rewards
     */
    function _facilityCalculation(uint256 _totalRewards) public view returns (uint256) {
        uint256 facilityTotalSupply = energy.totalSupply(uint256(IAssetManager.AssetIds.FacilityOutlier)) +
            energy.totalSupply(uint256(IAssetManager.AssetIds.FacilityCommon)) +
            energy.totalSupply(uint256(IAssetManager.AssetIds.FacilityUncommon)) +
            energy.totalSupply(uint256(IAssetManager.AssetIds.FacilityRare)) +
            energy.totalSupply(uint256(IAssetManager.AssetIds.FacilityLegendary));

        bytes16 x = ABDKMathQuad.fromUInt(facilityTotalSupply - 1);
        bytes16 y = ABDKMathQuad.fromUInt(25000);

        bytes16 exponentPart = ABDKMathQuad.exp(ABDKMathQuad.div(x, y));
        bytes16 poolShare = ABDKMathQuad.sub(ABDKMathQuad.fromUInt(10000), ABDKMathQuad.div(ABDKMathQuad.fromUInt(9900), exponentPart));

        return (_totalRewards * ABDKMathQuad.toUInt(poolShare)) / 10000;
    }

    /**
     * @notice Pushes all locked rewards to next epochs in their occrent proportions
     * @param _lockedRewards locked rewards to be pushed to next epochs
     */
    function _pushLockedPoolRewardToNextEpochs(uint256 _lockedRewards) internal {
        uint256 len = s_pushRewardToNextEpochPercentages.length;
        for (uint256 i = 1; i < len + 1; i++) {
            s_rewardPool.pushedRewards[s_epochCount + i] += (_lockedRewards * s_pushRewardToNextEpochPercentages[i - 1]) / s_rewardsPercentageDenominator;
        }
    }

    /**
     * @notice Sets locked reward percentages to be pushed to later epochs
     * @param _pushRewardNextEpochsPercentages percentages to be used in future epoch pushing
     */
    function setPushRewardNextEpochsPercentages(uint256[] calldata _pushRewardNextEpochsPercentages) external onlyGov {
        s_pushRewardToNextEpochPercentages = _pushRewardNextEpochsPercentages;
    }

    /**
     * @notice Sets amelia foundation wallet
     * @param _walletAddress amelia foundation wallet
     */
    function setAmeliaFoundationWallet(address _walletAddress) external onlyGov {
        require(_walletAddress != address(0), "ENERGY STAKING: ZERO ADDRESS");
        s_ameliaFoundation = _walletAddress;
    }

    /**
     * @notice Sets amelia foundation share
     * @param _share share for amelia foundation ie. 300 for 3% if bps is 10000
     * @param _totalBps total bps to be used in calculations
     */
    function setAmeliaFoundationShare(uint256 _share, uint256 _totalBps) external onlyGov {
        s_ameliaFoundationShare = _share;
        s_ameliaFoundationMaxBps = _totalBps;
    }

    /// @inheritdoc IEnergyIXTStaking
    function setRewardDistributor(address _distributor) external onlyGov {
        require(_distributor != address(0), "ENERGY STAKING: ZERO ADDRESS");
        s_rewardDistributor = _distributor;
    }

    /**
     * @notice sets denominator to be used in reward percentage calculations
     * @param _denominator denominator
     */
    function setRewardPercentageDenominator(uint256 _denominator) external onlyGov {
        require(_denominator != 0, "ENERGY STAKING: ZERO ADDRESS");
        s_rewardsPercentageDenominator = _denominator;
    }

    /**
     * @notice sets or disapproves someone's moderator status
     * @param moderator moderator
     * @param approved whether or not they are approved
     */
    function setModerator(address moderator, bool approved) external onlyOwner {
        require(moderator != address(0), "ENERGY STAKING: ZERO ADDRESS");
        moderators[moderator] = approved;
    }

    /**
     * @notice sets energy contract
     * @param _energy Energy Contract
     */
    function setEnergy(IEnergy _energy) external onlyGov {
        energy = _energy;
    }

    function setIXT(IERC20Upgradeable _pixt) external onlyGov {
        pixt = _pixt;
    }

    function setPaused(bool _paused) external onlyGov {
        paused = _paused;
    }

    /*---------------- VIEW FUNCTIONS ----------------*/

    /// @inheritdoc IEnergyIXTStaking
    function getUserStakedAmount(address _walletAddress) external view returns (uint256) {
        return userStakedAmount[_walletAddress];
    }

    function getUserAmeliaCommitment(address _walletAddress) external view returns (uint256) {
        return userAmeliaCommitment[_walletAddress];
    }

    function getTotalStakedAmountOld() external view returns (uint256) {
        return s_rewardPool.tokensStaked;
    }

    /**
     * @notice Gets pushed rewards
     * @param _epoch epoch for which to get reward
     */
    function getPushedRewards(uint256 _epoch) external view returns (uint256) {
        return s_rewardPool.pushedRewards[_epoch];
    }

    /// @notice Gets total reward per token
    function getRewardPerToken() external view returns (uint256) {
        return rewardPool[s_epochCount].rewardPerTokenStored;
    }

    /// @notice Gets current reward rate
    function getCurrentRewardRate() external view returns (uint256) {
        return rewardPool[s_epochCount].rewardRate;
    }

    /// @inheritdoc IEnergyIXTStaking

    /// @notice Gets total staked amount
    function getTotalStakedAmount() external view returns (uint256) {
        return rewardPool[s_epochCount].tokensStaked;
    }

    /// @notice Gets current epoch end time
    function getCurrentEpochEndTime() external view returns (uint256) {
        return rewardPool[s_epochCount].periodFinish;
    }

    /// @notice Gets last update time
    function getLastUpdateTime() external view returns (uint256) {
        return rewardPool[s_epochCount].lastUpdateTime;
    }

    /// @notice Gets user reward per token paid
    function getUserRewardPerTokenPaid(address _walletAddress) external view returns (uint256) {
        return rewardPool[s_epochCount].userRewardPerTokenPaid[_walletAddress];
    }

    /// @notice Gets percentages for pushing to next epochs
    function getPushedRewardsNextEpochPercentages() external view returns (uint256[] memory) {
        return s_pushRewardToNextEpochPercentages;
    }

    function getUserIXTStakes(address _user) external view returns (Stake[] memory) {
        return userIXTStakes[_user];
    }

    function getUserTotalIXTStaked(address _user) public view returns (uint256) {
        return userStakedAmount[_user] + userAmeliaCommitment[_user] + userTotalIXTStaked[_user];
    }

    function getUserTotalEnergyStaked(address _user) public view returns (uint256) {
        return userEnergyStaked[_user][s_epochCount] + userEnergyStakedAmelia[_user][s_epochCount];
    }

    function getUserEnergyStaked(address _user) public view returns (uint256) {
        return userEnergyStaked[_user][s_epochCount];
    }

    function getUserTotalEnergyStakedAmelia(address _user) public view returns (uint256) {
        return userEnergyStakedAmelia[_user][s_epochCount];
    }

    /*---------------- STAKING UTILS (OLD) ----------------*/

    function unstakeOld(uint256 _amount, bool _ameliaCommitment) external nonReentrant {
        require(_amount > 0, "ENERGY STAKING: CANT UNSTAKE 0");
        if (_ameliaCommitment) require(userAmeliaCommitment[msg.sender] >= _amount, "ENERGY STAKING: INSUFFICIENT AMOUNT COMMITTED");
        else require(userStakedAmount[msg.sender] >= _amount, "ENERGY STAKING: INSUFFICIENT AMOUNT STAKED");

        _updateRewardsOld(msg.sender);

        s_rewardPool.tokensStaked -= _amount;

        if (_ameliaCommitment) {
            userAmeliaCommitment[msg.sender] -= _amount;
        } else {
            userStakedAmount[msg.sender] -= _amount;
        }

        _proportionAndTransferRewardsOld(_ameliaCommitment);

        pixt.transfer(msg.sender, _amount * 1 ether);
    }

    function _claimOld(bool _ameliaCommitment) internal {
        _updateRewardsOld(msg.sender);
        _proportionAndTransferRewardsOld(_ameliaCommitment);
    }

    function _proportionAndTransferRewardsOld(bool _ameliaCommitment) internal {
        uint256 rewardToPay;
        if (_ameliaCommitment) {
            uint256 ameliaPortion = (s_rewardPool.ameliaRewards[msg.sender] * s_ameliaFoundationShare) / s_ameliaFoundationMaxBps;
            rewardToPay = s_rewardPool.ameliaRewards[msg.sender] - ameliaPortion;
            s_rewardPool.ameliaRewards[msg.sender] = 0;
            pixt.transfer(s_ameliaFoundation, ameliaPortion);
        } else {
            rewardToPay = s_rewardPool.rewards[msg.sender];
            s_rewardPool.rewards[msg.sender] = 0;
        }
        pixt.transfer(msg.sender, rewardToPay);
    }

    /**
     * @notice Updates rewards within pool based on the number of IXT staked
     * @param _walletAddress address of a user whose rewards we wish to update
     */
    function _updateRewardsOld(address _walletAddress) internal {
        s_rewardPool.rewardPerTokenStored = rewardPerTokenStoredOld();
        s_rewardPool.lastUpdateTime = lastUpdateTimeApplicableOld(block.timestamp);

        if (_walletAddress != address(0)) {
            s_rewardPool.rewards[_walletAddress] = earnedOld(_walletAddress);
            s_rewardPool.ameliaRewards[_walletAddress] = earnedAmeliaOld(_walletAddress);
            s_rewardPool.userRewardPerTokenPaid[_walletAddress] = s_rewardPool.rewardPerTokenStored;
        }
    }

    function earnedOld(address _walletAddress) public view returns (uint256) {
        return (userStakedAmount[_walletAddress] * (rewardPerTokenStoredOld() - s_rewardPool.userRewardPerTokenPaid[_walletAddress])) + s_rewardPool.rewards[_walletAddress];
    }

    function earnedAmeliaOld(address _walletAddress) public view returns (uint256) {
        return (userAmeliaCommitment[_walletAddress] * (rewardPerTokenStoredOld() - s_rewardPool.userRewardPerTokenPaid[_walletAddress])) + s_rewardPool.ameliaRewards[_walletAddress];
    }

    /// @notice Finds the reward per token based on the number of staked tokens and the time that has passed
    function rewardPerTokenStoredOld() public view returns (uint256) {
        if (s_rewardPool.tokensStaked == 0) {
            return s_rewardPool.rewardPerTokenStored;
        }

        uint256 startTime = s_rewardPool.lastUpdateTime;
        uint256 untilTime = lastUpdateTimeApplicableOld(block.timestamp);
        uint256 reward = s_rewardPool.rewardRate * (untilTime - startTime);

        return s_rewardPool.rewardPerTokenStored + (reward / s_rewardPool.tokensStaked);
    }

    /// @notice Finds the last update time that was applicable, does not allow it to go beyond the epoch finish time
    function lastUpdateTimeApplicableOld(uint256 _untilTimeStamp) public view returns (uint256) {
        uint256 effectiveTimeStamp = _untilTimeStamp < s_rewardPool.periodFinish ? _untilTimeStamp : s_rewardPool.periodFinish;

        return effectiveTimeStamp;
    }
}
