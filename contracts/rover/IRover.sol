// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface IRover {
    error Rover__ZeroAddress();
    error Rover__NotOwner();
    error Rover__NotOracle();
    error Rover__IncorrectStatus(uint256 from);
    error Rover__InvalidSkin(uint256 skin);
    error Rover__MaxSupplyReached();
    error Rover__PaymentTooLow();
    error Rover__NotMC();
    error Rover__NoKeyHash();
    error Rover__SaleNotOpen();
    error Rover__WrongPayToken();
    error Rover__WrongPayTokenOrStatus();
    error Rover__InvalidArguments();

    enum Status {
        Pristine,
        Worn,
        Damaged,
        Wrecked
    }

    enum Color {
        Night,
        Genesis,
        Piercer
    }

    struct RoverInfo {
        string name;
        Color color;
        Status status;
        uint256 skin;
    }

    struct YGenesisRepairInfo {
        uint tokenId;
        int256 x;
        int256 y;
        int256 z;
    }

    enum PayableToken {
        IXT,
        AstroCredits,
        USDT,
        Matic
    }
}
