//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "./VRFCoordinatorV2Interface.sol";

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721EnumerableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/AddressUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../interfaces/IOracleManager.sol";
import "../interfaces/ITrustedMintable.sol";
import "../libraries/VRFConsumerBaseV2Upgradable.sol";
import "../mission_control/IMCCrosschainServices.sol";
import "../mission_control/IMissionControlStakeable.sol";
import "../mission_control/IMissionControl.sol";
import "./RoverHelper.sol";
import "./IRover.sol";

interface IVRFManager {
    function getRandomNumber(uint32 numWords) external returns (uint256 requestId);
}

interface IRNGConsumer {
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] calldata randomWords) external;
}

contract Rover is
    ERC721EnumerableUpgradeable,
    OwnableUpgradeable,
    ERC1155HolderUpgradeable,
    VRFConsumerBaseV2Upgradable,
    ITrustedMintable,
    IRover,
    IRNGConsumer
{
    IERC20Upgradeable public IXT;
    IERC1155Upgradeable public astroCredits;
    IERC20 public USDT;

    uint256 acTokenId;

    IOracleManager s_oracle;

    VRFCoordinatorV2Interface public COORDINATOR;
    mapping(uint256 => address) public s_requests;
    bytes32 keyHash;
    uint32 callbackGasLimit;
    uint16 requestConfirmations;
    uint64 subscriptionId;

    uint256 MAX_SUPPLY;

    string[] public uris;
    mapping(uint256 => uint256) public ixtSkinPrices; // not used
    mapping(uint256 => uint256) public acSkinPrices; // not used
    mapping(uint256 => RoverInfo) public rovers;

    uint256 ixtNamePrice; // not used
    uint256 acNamePrice; // not used

    uint256 ixtRepairWornPrice; // not used
    uint256 ixtRepairDamagedPrice; // not used

    uint256 acRepairWornPrice; // not used
    uint256 acRepairDamagedPrice; // not used

    address public missionControl;

    mapping(address => bool) trusted;

    bool public saleOpen;

    mapping(PayableToken => mapping(Status => uint256)) repairPrice;
    mapping(PayableToken => mapping(uint256 => uint256)) skinPrice;
    mapping(PayableToken => uint256) namePrice;

    uint256 public purchaseUSDTprice;
    IMCCrosschainServices public crosschainServices;
    IMissionControlStakeable public roverAdapter;
    RoverHelper public roverHelper;

    IVRFManager public vrfManager;

    /** Events **/
    event NewRover(address indexed to, uint256 indexed tokenId, Color color);
    event Repaired(uint256 indexed tokenId, Status status);
    event Named(uint256 indexed tokenId, string name);
    event NewSkin(uint256 indexed tokenId, uint256 _skin);
    event Breakdown(uint256 indexed tokenId, Status to);

    modifier trustedOnly() {
        if (!trusted[msg.sender]) revert TM__NotTrusted(msg.sender);
        _;
    }

    modifier onlyRoverAdapter() {
        if (msg.sender != address(roverAdapter)) revert Rover__NotMC();
        _;
    }

    modifier onlyOracle() {
        if (msg.sender != address(vrfManager)) revert Rover__NotOracle();
        _;
    }

    function initialize(
        string calldata uri,
        address _missionControl,
        address _s_oracle,
        address _vrfCoordinator,
        address _USDT,
        address _IXT,
        address _astroCredits,
        uint256 _acTokenId
    ) public initializer {
        __ERC721_init("Rover", "ROVER");
        __Ownable_init();
        __ERC1155Holder_init();
        uris.push(uri);

        if (_missionControl == address(0)) revert Rover__ZeroAddress();
        if (_USDT == address(0)) revert Rover__ZeroAddress();
        if (_IXT == address(0)) revert Rover__ZeroAddress();
        if (_astroCredits == address(0)) revert Rover__ZeroAddress();

        setOracleManager(_s_oracle);

        COORDINATOR = VRFCoordinatorV2Interface(_vrfCoordinator);
        __VRFConsumerBaseV2_init(_vrfCoordinator);

        USDT = IERC20(_USDT);
        IXT = IERC20Upgradeable(_IXT);
        astroCredits = IERC1155Upgradeable(_astroCredits);
        acTokenId = _acTokenId;

        // ixtNamePrice = 25 ether;
        // acNamePrice = 1000 ether;

        MAX_SUPPLY = 500_000;
    }

    /** Prices **/
    function _transferERCTokens(PayableToken payToken, uint256 amount) internal {
        if (payToken == PayableToken.IXT) {
            IXT.transferFrom(msg.sender, address(this), amount);
        } else if (payToken == PayableToken.AstroCredits) {
            astroCredits.safeTransferFrom(msg.sender, address(this), acTokenId, amount, "");
        } else if (payToken == PayableToken.USDT) {
            USDT.transferFrom(msg.sender, address(this), amount);
        } else {
            revert Rover__WrongPayToken();
        }
    }

    function setVrfManager(address _vrfManager) external onlyOwner {
        vrfManager = IVRFManager(_vrfManager);
    }

    function setRepairPrice(PayableToken _payToken, Status _roverStatus, uint256 _newPrice) external onlyOwner {
        repairPrice[_payToken][_roverStatus] = _newPrice;
    }

    function getRepairPrice(PayableToken _payToken, Status _roverStatus) public view returns (uint256) {
        if (repairPrice[_payToken][_roverStatus] == 0) {
            revert Rover__WrongPayTokenOrStatus();
        }
        return repairPrice[_payToken][_roverStatus];
    }

    function setPurchaseUSDTprice(uint256 _purchaseUSDTprice) external onlyOwner {
        purchaseUSDTprice = _purchaseUSDTprice;
    }

    function getPurchasePrice(PayableToken _payToken) external view returns (uint256) {
        if (_payToken == PayableToken.USDT) {
            return purchaseUSDTprice;
        } else if (_payToken == PayableToken.Matic) {
            return s_oracle.getAmountOutView(address(USDT), address(0), purchaseUSDTprice);
        } else if (_payToken == PayableToken.IXT) {
            return s_oracle.getAmountOutView(address(USDT), address(IXT), purchaseUSDTprice);
        } else {
            revert Rover__WrongPayToken();
        }
    }

    function _getPurchasePriceAndUpdate(PayableToken _payToken) internal returns (uint256) {
        if (_payToken == PayableToken.USDT) {
            return purchaseUSDTprice;
        } else if (_payToken == PayableToken.Matic) {
            return s_oracle.getAmountOut(address(USDT), address(0), purchaseUSDTprice);
        } else if (_payToken == PayableToken.IXT) {
            return s_oracle.getAmountOut(address(USDT), address(IXT), purchaseUSDTprice);
        } else {
            revert Rover__WrongPayToken();
        }
    }

    function setNamePrice(PayableToken _payToken, uint256 _newPrice) external onlyOwner {
        namePrice[_payToken] = _newPrice;
    }

    function getNamePrice(PayableToken _payToken) public view returns (uint256) {
        if (namePrice[_payToken] == 0) {
            revert Rover__WrongPayToken();
        }
        return namePrice[_payToken];
    }

    function setSkinPrice(PayableToken _payToken, uint256 _skinId, uint256 _newPrice) external onlyOwner {
        skinPrice[_payToken][_skinId] = _newPrice;
    }

    function getSkinPrice(PayableToken _payToken, uint256 _skinId) public view returns (uint256) {
        if (skinPrice[_payToken][_skinId] == 0) {
            revert Rover__InvalidSkin(_skinId);
        }
        return skinPrice[_payToken][_skinId];
    }

    function getTokensOfOwner(address owner) external view returns (uint256[] memory) {
        uint256 len = balanceOf(owner);
        uint256[] memory tokens = new uint256[](len);
        for (uint256 i = 0; i < len; i++) {
            tokens[i] = tokenOfOwnerByIndex(owner, i);
        }
        return tokens;
    }

    /** Minting **/
    /**
     * @notice Pay for rover with USDT and submit a VRF request
     * @param amount The number of rovers to mint
     */
    function purchaseWithUSDT(uint32 amount) public returns (uint256 requestId) {
        return _purchaseRover(PayableToken.USDT, amount);
    }

    /**
     * @notice Pay for rover with MATIC and submit a VRF request
     * @param amount The number of rovers to mint
     */
    function purchaseWithMatic(uint32 amount) public payable returns (uint256 requestId) {
        return _purchaseRover(PayableToken.Matic, amount);
    }

    /**
     * @notice Pay for rover with IXT and submit a VRF request
     * @param amount The number of rovers to mint
     */
    function purchaseWithIXT(uint32 amount) public returns (uint256 requestId) {
        return _purchaseRover(PayableToken.IXT, amount);
    }

    function _purchaseRover(PayableToken _payToken, uint32 amount) internal returns (uint256 requestId) {
        if (_payToken == PayableToken.Matic) {
            if (msg.value < amount * _getPurchasePriceAndUpdate(_payToken)) {
                revert Rover__PaymentTooLow();
            }
        } else {
            _transferERCTokens(_payToken, amount * _getPurchasePriceAndUpdate(_payToken));
        }

        return _vrfRequest(amount);
    }

    function _vrfRequest(uint32 amount) internal returns (uint256 requestId) {
        if (totalSupply() + amount > MAX_SUPPLY) {
            revert Rover__MaxSupplyReached();
        }
        if (!saleOpen) {
            revert Rover__SaleNotOpen();
        }
        if (keyHash == bytes32(0)) {
            revert Rover__NoKeyHash();
        }

        requestId = vrfManager.getRandomNumber(amount);
        s_requests[requestId] = msg.sender;
    }

    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords) internal override{}

    /**
     * @notice Function for satisfying randomness requests from minting
     * @param requestId The particular request being serviced
     * @param randomWords Array of the random numbers requested
     */
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external onlyOracle {
        address to = s_requests[requestId];
        uint256 length = randomWords.length;
        for (uint256 i = 0; i < length; ) {
            uint256 tokenId = totalSupply();
            uint256 randomWord = randomWords[i] % 100;
            Color color;
            if (randomWord < 89) {
                color = Color.Piercer;
            } else if (randomWord < 99) {
                color = Color.Genesis;
            } else {
                color = Color.Night;
            }
            rovers[tokenId] = RoverInfo({name: "", color: Color(color), status: Status.Pristine, skin: 0});
            _safeMint(to, tokenId);
            emit NewRover(to, tokenId, color);
            unchecked {
                ++i;
            }
        }
        delete s_requests[requestId];
    }

    /** Token URIs **/
    function description(Color color) public view returns (string memory) {
        return roverHelper.description(color);
    }

    function tokenURI(uint256 tokenId) public view override returns (string memory) {
        require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");
        RoverInfo memory rover = rovers[tokenId];
        return roverHelper.tokenURI(rover, uris[0], uris[rover.skin]);
    }

    function addUri(string calldata uri) public onlyOwner {
        uris.push(uri);
    }

    function addSkin(string memory uri, uint256 ixtPrice, uint256 acPrice) public onlyOwner {
        uris.push(uri);
        skinPrice[PayableToken.IXT][uris.length - 1] = ixtPrice;
        skinPrice[PayableToken.AstroCredits][uris.length - 1] = acPrice;
    }

    /** Repairs **/
    function _repairBatch(PayableToken payToken, uint256[] calldata _tokenIds) internal {
        uint256 totalPrice = 0;
        for (uint256 i = 0; i < _tokenIds.length; i++) {
            uint256 tokenId = _tokenIds[i];
            if (rovers[tokenId].status != Status.Worn && rovers[tokenId].status != Status.Damaged) {
                revert Rover__IncorrectStatus(uint256(rovers[tokenId].status));
            }
            totalPrice += getRepairPrice(payToken, rovers[tokenId].status);

            rovers[tokenId].status = Status.Pristine;
            emit Repaired(tokenId, Status.Pristine);
        }

        _transferERCTokens(payToken, totalPrice);
    }

    function repairBatchIXT(uint256[] calldata _tokenIds) external {
        _repairBatch(PayableToken.IXT, _tokenIds);
    }

    function repairBatchAC(uint256[] calldata _tokenIds) external {
        _repairBatch(PayableToken.AstroCredits, _tokenIds);
    }

    function repairBatchYGenesis(YGenesisRepairInfo[] memory repairs) external {
        require(crosschainServices.getYGenesisBalance(msg.sender) != 0, "ROVER: NO_Y_GENESIS_BALANCE");
        for (uint256 i = 0; i < repairs.length; i++) {
            uint tokenToRepair;
            if (ownerOf(repairs[i].tokenId) == msg.sender) {
                tokenToRepair = repairs[i].tokenId;
            } else {
                (, IMissionControl.TileElement memory top1, IMissionControl.TileElement memory top2) = IMissionControl(
                    missionControl
                ).checkStakedOnTile(msg.sender, repairs[i].x, repairs[i].y, repairs[i].z);

                if (top1.stakeable == address(roverAdapter)) tokenToRepair = top1.tokenId;
                else if (top2.stakeable == address(roverAdapter)) tokenToRepair = top2.tokenId;
                else revert("ROVER: NO_ROVER_ON_THIS_TILE");
            }

            if (rovers[tokenToRepair].status != Status.Worn && rovers[tokenToRepair].status != Status.Damaged) {
                revert Rover__IncorrectStatus(uint256(rovers[tokenToRepair].status));
            }
            rovers[tokenToRepair].status = Status.Pristine;
            emit Repaired(tokenToRepair, Status.Pristine);
        }
    }

    function breakdown(uint256 tokenId, uint256 to) external onlyRoverAdapter {
        if (to > uint(Status.Wrecked)) return;
        rovers[tokenId].status = Status(to);
        emit Breakdown(tokenId, Status(to));
    }

    /** Set Name **/
    function setNameWithIXT(uint256 tokenId, string memory name) public {
        _setName(PayableToken.IXT, tokenId, name);
    }

    function setNameWithAC(uint256 tokenId, string memory name) public {
        _setName(PayableToken.AstroCredits, tokenId, name);
    }

    function _setName(PayableToken _payToken, uint256 tokenId, string memory name) internal {
        if (ownerOf(tokenId) != msg.sender) revert Rover__NotOwner();
        _transferERCTokens(_payToken, getNamePrice(_payToken));
        rovers[tokenId].name = name;
        emit Named(tokenId, name);
    }

    /** Upgrade Skin **/
    function upgradeSkinIXT(uint256 tokenId, uint256 _skin) public {
        _upgradeSkin(PayableToken.IXT, tokenId, _skin);
    }

    function upgradeSkinAC(uint256 tokenId, uint256 _skin) public {
        _upgradeSkin(PayableToken.AstroCredits, tokenId, _skin);
    }

    function _upgradeSkin(PayableToken _payToken, uint256 tokenId, uint256 _skin) internal {
        if (ownerOf(tokenId) != msg.sender) revert Rover__NotOwner();
        if (_skin >= uris.length) revert Rover__InvalidSkin(_skin);
        _transferERCTokens(_payToken, getSkinPrice(_payToken, _skin));
        rovers[tokenId].skin = _skin;
        emit NewSkin(tokenId, _skin);
    }

    /** Burn **/
    function burn(uint256 tokenId) public {
        if (ownerOf(tokenId) != msg.sender) revert Rover__NotOwner();
        _burn(tokenId);
    }

    /** Admin Functions **/

    /**
     * @notice set keyHash by owner
     */
    function setKeyHash(bytes32 _keyHash) external onlyOwner {
        keyHash = _keyHash;
    }

    /**
     * @notice set callbackGasLimit by owner
     */
    function setCallbackGasLimit(uint32 _callbackGasLimit) external onlyOwner {
        callbackGasLimit = _callbackGasLimit;
    }

    /**
     * @notice set subscriptionId by owner
     */
    function setSubscriptionId(uint64 _subscriptionId) external onlyOwner {
        subscriptionId = _subscriptionId;
    }

    /**
     * @notice set requestConfirmations by owner
     */
    function setRequestConfirmations(uint16 _requestConfirmations) external onlyOwner {
        requestConfirmations = _requestConfirmations;
    }

    function setOracleManager(address _oracleManager) public onlyOwner {
        if (_oracleManager == address(0)) revert Rover__ZeroAddress();
        s_oracle = IOracleManager(_oracleManager);
    }

    function setCrosschain(IMCCrosschainServices _crosschain) external onlyOwner {
        crosschainServices = _crosschain;
    }

    function setRoverAdapter(IMissionControlStakeable _stakeable) external onlyOwner {
        roverAdapter = _stakeable;
    }

    function setMissionControl(IMissionControl _missionControl) external onlyOwner {
        missionControl = address(_missionControl);
    }

    function setRoverHelper(RoverHelper _roverHelper) external onlyOwner {
        roverHelper = _roverHelper;
    }

    function withdraw() public onlyOwner {
        IXT.transfer(msg.sender, IXT.balanceOf(address(this)));
        astroCredits.safeTransferFrom(
            address(this),
            msg.sender,
            acTokenId,
            astroCredits.balanceOf(address(this), acTokenId),
            ""
        );
        USDT.transfer(msg.sender, USDT.balanceOf(address(this)));
        require(payable(msg.sender).send(address(this).balance));
    }

    function setSaleOpen(bool _saleOpen) public onlyOwner {
        saleOpen = _saleOpen;
    }

    function setAstroCredits(address _astroCredits) external onlyOwner {
        astroCredits = IERC1155Upgradeable(_astroCredits);
    }

    /** Interface **/
    function supportsInterface(
        bytes4 interfaceId
    ) public view virtual override(ERC1155ReceiverUpgradeable, ERC721EnumerableUpgradeable) returns (bool) {
        return interfaceId == type(IERC721EnumerableUpgradeable).interfaceId || super.supportsInterface(interfaceId);
    }

    function setTrusted(address _trusted, bool _isTrusted) external onlyOwner {
        trusted[_trusted] = _isTrusted;
    }

    function trustedMint(address _to, uint256 _tokenId, uint256 _amount) external trustedOnly {
        for (uint256 i; i < _amount; i++) {
            uint256 tokenId = totalSupply();
            Color color;
            if (_tokenId == 3) {
                color = Color.Piercer;
            } else if (_tokenId == 2) {
                color = Color.Genesis;
            } else if (_tokenId == 1) {
                color = Color.Night;
            }
            rovers[tokenId] = RoverInfo({name: "", color: Color(color), status: Status.Pristine, skin: 0});
            _safeMint(_to, tokenId);
            emit NewRover(_to, tokenId, color);
        }
    }

    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external trustedOnly {}

    function tokensOfOwner(address _owner) external view returns (uint[] memory _tokens) {
        _tokens = new uint[](balanceOf(_owner));
        for (uint i; i < balanceOf(_owner); i++) {
            _tokens[i] = tokenOfOwnerByIndex(_owner, i);
        }
    }

    function airdrop(address[] memory _receipients, uint256[] memory _colours) external onlyOwner {
        uint256 length = _receipients.length;
        if (_colours.length != length) revert Rover__InvalidArguments();
        if (totalSupply() + length > MAX_SUPPLY) revert Rover__MaxSupplyReached();
        for (uint256 i; i < length; i++) {
            uint256 tokenId = totalSupply();
            Color color;
            if (_colours[i] == 3) {
                color = Color.Piercer;
            } else if (_colours[i] == 2) {
                color = Color.Genesis;
            } else if (_colours[i] == 1) {
                color = Color.Night;
            }
            rovers[tokenId] = RoverInfo({name: "", color: Color(color), status: Status.Pristine, skin: 0});
            _safeMint(_receipients[i], tokenId);
            emit NewRover(_receipients[i], tokenId, color);
        }
    }

    function safeBatchTransferFrom(
        address _from,
        address _to,
        uint256[] calldata _tokenIds
    ) external {
        uint256 len = _tokenIds.length;
        for (uint256 i; i < len; i += 1) {
            _transfer(_from, _to, _tokenIds[i]);
        }
    }
}
