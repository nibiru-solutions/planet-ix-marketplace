//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "./IRover.sol";
import "../libraries/Base64Url.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract RoverHelper is OwnableUpgradeable {

  function initialize() public initializer {
      __Ownable_init();
  }

  function description(IRover.Color color) public pure returns (string memory) {
    if (color == IRover.Color.Piercer) {
        return
            "The Piercer Rover is designed for the harsh terrain of Planet IX. Utilizing standard propulsion and power systems, it is able to collect waste in the immediate area. However, long-term use without maintenance will result in mechanical malfunction.";
    } else if (color == IRover.Color.Genesis) {
        return
            "The Genesis Rover is designed for the harsh terrains of Planet IX. Its advanced sensors and mapping technology allow it to locate and collect waste with pinpoint accuracy. However, regular maintenance is required to avoid potential system failure.";
    } else {
        return
            "The Night Rover is designed to navigate the rugged terrains of Planet IX. Equipped with cutting-edge thermal and infrared sensors, it efficiently collects waste in extremely low-light conditions. However, regular maintenance is required to avoid potential system failure.";
    }
  }

  function tokenURI(IRover.RoverInfo memory rover, string memory baseUri, string memory skinUri) public pure returns(string memory) {
    string memory image;
    string memory video;
    if (rover.status == IRover.Status.Wrecked) {
        image = string.concat(baseUri, "/3.gif");
        video = string.concat(baseUri, "/3.mp4");
    } else {
        if (rover.skin == 0) {
            image = string.concat(
                baseUri,
                "/",
                Strings.toString(uint256(rover.status)),
                "-",
                Strings.toString(uint256(rover.color)),
                ".gif"
            );
            video = string.concat(
                baseUri,
                "/",
                Strings.toString(uint256(rover.status)),
                "-",
                Strings.toString(uint256(rover.color)),
                ".mp4"
            );
        } else {
            image = skinUri;
            video = skinUri;
            // video = uris[rover.skin];
        }
    }

    string memory status;
    if (rover.status == IRover.Status.Pristine) {
        status = "Pristine";
    } else if (rover.status == IRover.Status.Worn) {
        status = "Worn";
    } else if (rover.status == IRover.Status.Damaged) {
        status = "Damaged";
    } else {
        status = "Wrecked";
    }
    string memory color;
    string memory roverType;
    if (rover.color == IRover.Color.Piercer) {
        roverType = "RVR Piercer";
        color = "White";
    } else if (rover.color == IRover.Color.Genesis) {
        roverType = "RVR Genesis";
        color = "Green";
    } else {
        roverType = "RVR Night";
        color = "Black";
    }
    string memory attributes = string.concat(
        '"attributes":[{"trait_type":"Corporation","value":"Y_"},{"trait_type":"Family","value":"Harvester Vehicle"},{"trait_type":"Type","value":"',
        roverType,
        '"},{"trait_type":"Color","value":"',
        color,
        '"},{"trait_type":"Material","value":"Hardened Bioplastics"},{"trait_type":"State","value":"',
        status,
        '"}]'
    );
    string memory json = string.concat(
        '{"name": "',
        bytes(rover.name).length == 0 ? roverType : rover.name,
        '", "description": "',
        description(rover.color),
        '", "image": "',
        image,
        '", "animation_url": "',
        video,
        '", ',
        attributes,
        "}"
    );
    string memory base = "data:application/json;base64,";

    return string.concat(base, Base64Url.encode(bytes(json)));
  }
}