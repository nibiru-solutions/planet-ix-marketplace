pragma solidity ^0.8.0;

import "../interfaces/ISyndicatesOfChange.sol";
import "../interfaces/IOracleManager.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

contract SyndicatesOfChange is
ISyndicatesOfChange,
ERC721Upgradeable,
OwnableUpgradeable,
ReentrancyGuardUpgradeable
{
    using SafeERC20Upgradeable for IERC20Upgradeable;

    /* TYPE DECLARATIONS */
    struct SoC {
        uint256 causeId;
        uint256 usdDonatedInCents;
        string metadata;
    }

    struct CauseMetadata {
        uint256[] _breakpoints;
        string[] _metadata;
    }

    /* STATE VARIABLES */
    mapping(address => uint256) private s_ethBalanceOf;
    mapping(address => mapping(address => uint256)) private s_erc20BalanceOf;

    mapping(address => bool) private s_whitelistedTokens;
    mapping(address => bool) private s_beneficiaries;

    mapping(uint256 => CauseInfo) private s_idToCause;
    mapping(uint256 => CauseMetadata) private s_idToCauseMetadata;
    uint256 public s_totalCauses;

    mapping(uint256 => SoC) private s_idToSoC;
    uint256 public s_totalSupply;

    uint256 public s_minimumDonation;

    IOracleManager s_oracle;
    address private s_usdToken;

    /* INITIALIZER */
    function initialize() public initializer {
        __ERC721_init("Syndicates Of Change", "SOC");
        __ReentrancyGuard_init();
        __Ownable_init();
    }

    /* USER FUNCTIONS */

    /// @inheritdoc ISyndicatesOfChange
    function donate(
        uint256 _causeId,
        address _paymentToken,
        uint256 _amount,
        uint256 _existingTokenId
    ) external payable nonReentrant returns (uint256 _newTokenId) {
        if (!s_whitelistedTokens[_paymentToken] && _paymentToken != address(0))
            revert SoC__TokenNotWhitelisted(_paymentToken);
        if (_causeId > s_totalCauses) revert SoC__InvalidCauseId(_causeId);
        if (s_idToCause[_causeId]._paused) revert SoC__CausePaused(_causeId);
        if (!_exists(_existingTokenId) && _existingTokenId != 0)
            revert SoC__NotOwnerOf(msg.sender, _existingTokenId);

        uint256 usdAmount;

        if (_paymentToken == address(0)) {
            usdAmount = _amountToUsd(msg.value, _paymentToken);
            if (usdAmount < s_minimumDonation) revert SoC__DonationTooSmall(usdAmount);
            s_ethBalanceOf[s_idToCause[_causeId]._beneficiary] += msg.value;
        } else {
            usdAmount = _amountToUsd(_amount, _paymentToken);
            if (usdAmount < s_minimumDonation) revert SoC__DonationTooSmall(usdAmount);
            IERC20Upgradeable(_paymentToken).safeTransferFrom(msg.sender, address(this), _amount);
            s_erc20BalanceOf[s_idToCause[_causeId]._beneficiary][_paymentToken] += _amount;
        }

        if (_existingTokenId > 0) {
            if (msg.sender != ownerOf(_existingTokenId))
                revert SoC__NotOwnerOf(msg.sender, _existingTokenId);

            SoC storage existingSoC = s_idToSoC[_existingTokenId];

            if (existingSoC.causeId != _causeId)
                revert SoC__DifferentCauses(_causeId, _existingTokenId);

            existingSoC.usdDonatedInCents += usdAmount;

            existingSoC.metadata = _createtokenURI(
                _causeId,
                existingSoC.usdDonatedInCents + usdAmount
            );
            _newTokenId = _existingTokenId;
        } else {
        unchecked {
            ++s_totalSupply;
        }
            s_idToSoC[s_totalSupply] = SoC({
            causeId : _causeId,
            usdDonatedInCents : usdAmount,
            metadata : _createtokenURI(_causeId, usdAmount)
            });
            _mint(msg.sender, s_totalSupply);
            _newTokenId = s_totalSupply;
        }

        s_idToCause[_causeId]._totalDonatedAmount += usdAmount;

        emit Donation(msg.sender, _causeId, _paymentToken, _amount);
    }

    /// @inheritdoc ISyndicatesOfChange
    function mergeTokens(uint256[] calldata _tokenIds) external {
        uint256 _causeId = s_idToSoC[_tokenIds[0]].causeId;
        uint256 finalDonation;

        uint256 len = _tokenIds.length;
        for (uint256 i = 0; i < len; i++) {
            if (!_exists(_tokenIds[i]) || msg.sender != ownerOf(_tokenIds[i]))
                revert SoC__NotOwnerOf(msg.sender, _tokenIds[i]);
            if (s_idToSoC[_tokenIds[i]].causeId != _causeId)
                revert SoC__DifferentCauses(_causeId, s_idToSoC[_tokenIds[i]].causeId);

            finalDonation += s_idToSoC[_tokenIds[i]].usdDonatedInCents;
            _burn(_tokenIds[i]);
        }

    unchecked {
        ++s_totalSupply;
    }

        s_idToSoC[s_totalSupply] = SoC({
        causeId : _causeId,
        usdDonatedInCents : finalDonation,
        metadata : _createtokenURI(_causeId, finalDonation)
        });
        _mint(msg.sender, s_totalSupply);
    }

    /**
     * @notice Functions for beneficiaries to withdraw their donations
     * @param _paymentToken The token the user wishes to withdraw
     *
     * Reverts with "USER HAS NO BALANCE" on empty token balance
     * Reverts with "FAILED TO SEND ETHER" on invalid token id
     * Reverts with "SoC__NotBeneficiary" when a non-beneficiary tries to withdraw
     */
    function withdraw(address _paymentToken) external nonReentrant {
        if (!s_beneficiaries[msg.sender]) revert SoC__NotBeneficiary(msg.sender);
        if (_paymentToken == address(0)) {
            uint256 bal = s_ethBalanceOf[msg.sender];
            require(bal > 0, "USER HAS NO BALANCE");

            s_ethBalanceOf[msg.sender] = 0;
            (bool sent,) = msg.sender.call{value : bal}("");
            require(sent, "Failed to send Ether");
            emit Withdrawal(msg.sender, address(0), bal);
        } else {
            uint256 bal = s_erc20BalanceOf[msg.sender][_paymentToken];
            require(bal > 0, "USER HAS NO BALANCE");

            s_erc20BalanceOf[msg.sender][_paymentToken] = 0;
            IERC20Upgradeable(_paymentToken).safeTransfer(msg.sender, bal);
            emit Withdrawal(msg.sender, _paymentToken, bal);
        }
    }

    /* GETTERS */

    /// @inheritdoc ISyndicatesOfChange
    function getDonatedAmount(uint256 _tokenId)
    external
    view
    returns (uint256 _donatedAmountInUSDCents)
    {
        require(_exists(_tokenId), "ERC721: token not minted");
        _donatedAmountInUSDCents = s_idToSoC[_tokenId].usdDonatedInCents;
    }

    /// @inheritdoc ISyndicatesOfChange
    function getCauseForToken(uint256 _tokenId) external view returns (uint256 _causeId) {
        require(_exists(_tokenId), "ERC721: token not minted");
        _causeId = s_idToSoC[_tokenId].causeId;
    }

    /// @inheritdoc ISyndicatesOfChange
    function getCauseInfo(uint256 _causeId) external view returns (CauseInfo memory _info) {
        if (_causeId > s_totalCauses) revert SoC__InvalidCauseId(_causeId);
        _info = s_idToCause[_causeId];
    }

    /// @inheritdoc ISyndicatesOfChange
    function getPendingBalance(address _paymentToken, address _beneficiary)
    external
    view
    returns (uint256)
    {
        if (_paymentToken == address(0)) {
            return s_ethBalanceOf[_beneficiary];
        } else {
            return s_erc20BalanceOf[_beneficiary][_paymentToken];
        }
    }

    /**
     * @notice Returns the URI for a particular tokenId
     * @param tokenId The tokenId that we want a URI for
     *
     * Reverts with "ERC721Metadata: URI query for nonexistent token" on tokenId not existing
     */
    function tokenURI(uint256 tokenId)
    public
    view
    override(ERC721Upgradeable, IERC721MetadataUpgradeable)
    returns (string memory)
    {
        require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");
        return s_idToSoC[tokenId].metadata;
    }

    /* ADMIN-ONLY FUNCTIONS */

    /// @inheritdoc ISyndicatesOfChange
    function registerCause(
        address _beneficiary,
        uint256[] calldata _breakpoints,
        string[] calldata _metadata
    ) external onlyOwner returns (uint256 _causeId) {
        if (_beneficiary == address(0)) revert SoC__InvalidBeneficiary(_beneficiary);
        if ((_metadata.length != _breakpoints.length + 1) || _metadata.length == 0)
            revert SoC__IncorrectLength(_breakpoints.length, _metadata.length);
        if (!_validateSorting(_breakpoints)) revert SoC__InputsNotSorted();

    unchecked {
        ++s_totalCauses;
    }

        s_idToCause[s_totalCauses] = CauseInfo({
        _beneficiary : _beneficiary,
        _totalDonatedAmount : 0,
        _paused : true
        });
        s_beneficiaries[_beneficiary] = true;

        uint256 len = _breakpoints.length;

        CauseMetadata memory causeMetadata = CauseMetadata({
        _breakpoints : new uint256[](len),
        _metadata : new string[](len + 1)
        });
        causeMetadata._metadata[0] = _metadata[0];

        if (len != 0) {
            for (uint256 i = 0; i < len; i++) {
                causeMetadata._breakpoints[i] = _breakpoints[i];
                causeMetadata._metadata[i + 1] = _metadata[i + 1];
            }
        }
        s_idToCauseMetadata[s_totalCauses] = causeMetadata;
        _causeId = s_totalCauses;

        emit CauseRegistered(_causeId, _beneficiary);
    }

    /// @inheritdoc ISyndicatesOfChange
    function modifyCause(
        uint256 _causeId,
        uint256[] calldata _breakpoints,
        string[] calldata _metadata
    ) external onlyOwner {
        if (_causeId > s_totalCauses) revert SoC__InvalidCauseId(_causeId);
        if ((_metadata.length != _breakpoints.length + 1) || _metadata.length == 0)
            revert SoC__IncorrectLength(_breakpoints.length, _metadata.length);
        if (!_validateSorting(_breakpoints)) revert SoC__InputsNotSorted();

        uint256 len = _breakpoints.length;

        CauseMetadata memory causeMetadata = CauseMetadata({
        _breakpoints : new uint256[](len),
        _metadata : new string[](len + 1)
        });
        causeMetadata._metadata[0] = _metadata[0];
        if (len != 0) {
            for (uint256 i = 0; i < len; i++) {
                causeMetadata._breakpoints[i] = _breakpoints[i];
                causeMetadata._metadata[i + 1] = _metadata[i + 1];
            }
        }
        s_idToCauseMetadata[_causeId] = causeMetadata;
    }

    /// @inheritdoc ISyndicatesOfChange
    function deleteCause(uint256 _causeId) external onlyOwner {
        if (_causeId > s_totalCauses) revert SoC__InvalidCauseId(_causeId);
        delete s_idToCause[_causeId];
        emit CauseDeleted(_causeId);
    }

    /// @inheritdoc ISyndicatesOfChange
    function setPaused(uint256 _causeId, bool _isPaused) external onlyOwner {
        if (_causeId > s_totalCauses) revert SoC__InvalidCauseId(_causeId);
        s_idToCause[_causeId]._paused = _isPaused;
    }

    /// @inheritdoc ISyndicatesOfChange
    function setTokenWhitelist(address _tokenAddress, bool _isWhitelisted) external onlyOwner {
        if (_tokenAddress == address(0)) revert SoC__NotERC20(_tokenAddress);
        s_whitelistedTokens[_tokenAddress] = _isWhitelisted;
        emit TokenWhitelisted(_tokenAddress, _isWhitelisted);
    }

    /**
     * @notice Sets the oracle manager to be used for getting price data
     * @param _oracleManager address of the oracle manager to use
     *
     */
    function setOracleManager(address _oracleManager) external onlyOwner {
        s_oracle = IOracleManager(_oracleManager);
    }

    /**
     * @notice Sets the address of the stablecoin for the oracle manager to use
     * @param _usdToken address of the stablecoin to use
     *
     */
    function setUsdToken(address _usdToken) external onlyOwner {
        s_usdToken = _usdToken;
    }

    /**
     * @notice Sets the smallest donation a user can provide
     * @param _minimumDonation minimum amount that can be donated for all causes
     *
     */
    function setMinimumDonation(uint256 _minimumDonation) external onlyOwner {
        s_minimumDonation = _minimumDonation;
    }

    /* INTERNAL/PRIVATE */

    /**
     * @notice Returns conversion from one token to another token
     * @param _amount The amount being converted
     * @param _paymentToken The token we are converting from
     * @return _amountOut The conversion amount
     *
     */
    function _amountToUsd(uint256 _amount, address _paymentToken)
    private
    returns (uint256 _amountOut)
    {
        _amountOut = s_oracle.getAmountOut(_paymentToken, s_usdToken, _amount);
    }

    /**
     * @notice Creates a new uri for an SoC token based on the amount that is donated to it and its cause
     * @param amount The amount being donated
     * @param causeId The cause being donated to
     *
     */
    function _createtokenURI(uint256 causeId, uint256 amount)
    internal
    view
    returns (string memory)
    {
        uint256 len = s_idToCauseMetadata[causeId]._breakpoints.length;
        if (amount < s_idToCauseMetadata[causeId]._breakpoints[0])
            return s_idToCauseMetadata[causeId]._metadata[0];
        for (uint256 i = 0; i < len; i++) {
            if (i < len - 1) {
                if (
                    amount >= s_idToCauseMetadata[causeId]._breakpoints[i] &&
                    amount < s_idToCauseMetadata[causeId]._breakpoints[i + 1]
                ) {
                    return s_idToCauseMetadata[causeId]._metadata[i + 1];
                }
            }
        }
        return s_idToCauseMetadata[causeId]._metadata[len];
    }

    /**
     * @notice Checks whether or not the array input is sorted in ascending order
     * @param _arr The array being sorted
     *
     */
    function _validateSorting(uint256[] calldata _arr) private pure returns (bool) {
        uint256[] memory arr = _arr;
        uint256 l = arr.length;
        for (uint256 i = 0; i < l; i++) {
            for (uint256 j = i + 1; j < l; j++) {
                if (arr[i] > arr[j]) {
                    uint256 temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        for (uint256 i = 0; i < l; i++) {
            if (arr[i] != _arr[i]) return false;
        }
        return true;
    }
}
