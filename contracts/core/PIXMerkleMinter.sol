//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/MerkleProofUpgradeable.sol";
import "../interfaces/IPIX.sol";

/**
* @title Implementation of the merkle minter which does not actually implement the interface
*/
contract PIXMerkleMinter is OwnableUpgradeable {
    mapping(bytes32 => bool) public merkleRoots;
    mapping(bytes32 => bool) public leafUsed;

    IPIX public pix;

    mapping(address => bool) public delegateMinters;

    address public moderator;
    
    mapping(address => mapping(address => bool)) public alternativeAddresses;   // disabled
    mapping(address => address) public alternativeToOwnerAddress;
    

    /**
    * @notice Initializer for this contract
    * @param _pix Address of the PIX contract
    */
    function initialize(address _pix) external initializer {
        require(_pix != address(0), "Pix: INVALID_PIX");
        __Ownable_init();

        pix = IPIX(_pix);
    }

    /**
    * @notice Used to set merkle roots
    * @param _merkleRoot The merkle root
    * @param add Whether it is valid
    */
    function setMerkleRoot(bytes32 _merkleRoot, bool add) external onlyOwner {
        merkleRoots[_merkleRoot] = add;
    }

    /**
    * @notice Sets the moderator of this contract
    * @param mod The address of the moderator
    */
    function setModerator(address mod) external onlyOwner {
        moderator = mod;
    }

    /**
    * @notice Sets an alternative/preferred address
    * @param original The base address
    * @param alternative The alternative address
    * @param toSet Whether to clear or to set, thus this function can also be used to clear
    */
    function setAlternativeAddress(address original, address alternative, bool toSet) external {
        require(msg.sender == moderator || msg.sender == owner(), "Pix: NOT_OWNER_MODERATOR");
        
        if (toSet == true) {
            alternativeToOwnerAddress[alternative] = original;
        } else {
            alternativeToOwnerAddress[alternative] = address(0);
        }
    }

    /**
    * @notice Fetches a merkle leaf
    * @param user The address of the user
    * @param info Information regarding the pix-to-be
    * @param merkleRoot The merkle root
    * @param merkleProofs The merkle proofs
    * @return The leaf
    */
    function _getMerkleLeaf(
        address user,
        IPIX.PIXInfo memory info,
        bytes32 merkleRoot,
        bytes32[] calldata merkleProofs
    ) internal view returns (bytes32) {

        bytes32 leaf = keccak256(abi.encode(user, info.pixId, info.category, info.size));
        if (MerkleProofUpgradeable.verify(merkleProofs, merkleRoot, leaf)) {
            return leaf;
        }

        if (alternativeToOwnerAddress[user] != address(0)) {
            leaf = keccak256(abi.encode(alternativeToOwnerAddress[user], info.pixId, info.category, info.size));
            if (MerkleProofUpgradeable.verify(merkleProofs, merkleRoot, leaf)) {
                return leaf;
            }
        }

        return bytes32(0);
    }
    /**
    * @notice Validate the proofs and then mints a PIX
    * @param to Recipient of the PIX
    * @param info Struct containing info regarding the PIX to be minted
    * @param merkleRoot The merkle root
    * @param merkleProofs The merkle proofs
    */
    function mintByProof(
        address to,
        IPIX.PIXInfo memory info,
        bytes32 merkleRoot,
        bytes32[] calldata merkleProofs
    ) public {
        require(merkleRoots[merkleRoot], "Pix: invalid root");
        require(to == msg.sender || alternativeToOwnerAddress[msg.sender] == to, "Pix: alternative address not set");

        bytes32 leaf = _getMerkleLeaf(msg.sender, info, merkleRoot, merkleProofs);
        require(leaf != bytes32(0), "Pix: invalid proof");
        require(!leafUsed[leaf], "Pix: already minted");
        leafUsed[leaf] = true;

        pix.safeMint(msg.sender, info);
    }

    /**
    * @notice Mints a batch of PIX by proof
    * @param to Recipient of the PIX
    * @param info Array containing PIXinfo structs
    * @param merkleRoot Array containing the merkle root
    * @param merkleProofs Array containing arrays of merkle proofs
    */
    function mintByProofInBatch(
        address to,
        IPIX.PIXInfo[] memory info,
        bytes32[] calldata merkleRoot,
        bytes32[][] calldata merkleProofs
    ) external {
        require(
            info.length == merkleRoot.length && info.length == merkleProofs.length,
            "Pix: invalid length"
        );
        uint256 len = info.length;
        for (uint256 i; i < len; i += 1) {
            mintByProof(to, info[i], merkleRoot[i], merkleProofs[i]);
        }
    }

    function setDelegateMinter(address _minter, bool enabled) external onlyOwner {
        delegateMinters[_minter] = enabled;
    }

    /**
    * @notice Mints a pix and sends it to a new owner
    * @dev This contract does not extend IPIXMerkleMinter, thus the documentation is copy-pasted
    * @param destination Address of new owner
    * @param oldOwner Address of previous owner
    * @param info Info regarding the pix
    * @param merkleRoot The merkle root
    * @param merkleProofs The merkle proofs
    * @return The token Id
    */
    function mintToNewOwner(
        address destination,
        address oldOwner,
        IPIX.PIXInfo memory info,
        bytes32 merkleRoot,
        bytes32[] calldata merkleProofs
    ) public returns (uint256) {
        require(delegateMinters[msg.sender], "Pix: not delegate minter");
        require(merkleRoots[merkleRoot], "Pix: invalid root");

        bytes32 leaf = _getMerkleLeaf(oldOwner, info, merkleRoot, merkleProofs);
        require(leaf != bytes32(0), "Pix: invalid proof");
        require(!leafUsed[leaf], "Pix: already minted");
        leafUsed[leaf] = true;

        pix.safeMint(destination, info);

        return pix.lastTokenId();
    }

    /**
    * @notice Mints multiple PIX and sends them to a new owner
    * @dev This contract does not extend IPIXMerkleMinter, thus the documentation is copy-pasted
    * @param destination Address of new owner
    * @param oldOwner Address of previous owner
    * @param info Array info structs regarding the pix
    * @param merkleRoot Array containing the merkle roots
    * @param merkleProofs Arrays containing the corresponding merkle proofs
    * @return The token Ids of minted PIX
    */
    function mintToNewOwnerInBatch(
        address destination,
        address oldOwner,
        IPIX.PIXInfo[] memory info,
        bytes32[] calldata merkleRoot,
        bytes32[][] calldata merkleProofs
    ) external returns (uint256[] memory) {
        require(
            info.length > 0 &&
                info.length == merkleRoot.length &&
                info.length == merkleProofs.length,
            "Pix: invalid length"
        );
        uint256 len = info.length;
        uint256[] memory newIds = new uint256[](len);
        for (uint256 i; i < len; i += 1) {
            newIds[i] = mintToNewOwner(
                destination,
                oldOwner,
                info[i],
                merkleRoot[i],
                merkleProofs[i]
            );
        }

        return newIds;
    }

    /**
    * @notice Disables a proof, permanently preventing that PIX from being minted by this contract
    * @param to The would-be recipient of the PIX
    * @param info Struct containing info regarding the PIX to be minted
    * @param merkleRoot The merkle root
    * @param merkleProofs The merkle proofs
    */
    function disableProof(
        address to,
        IPIX.PIXInfo memory info,
        bytes32 merkleRoot,
        bytes32[] calldata merkleProofs
    ) external onlyOwner {
        require(merkleRoots[merkleRoot], "Pix: invalid root");

        bytes32 leaf = _getMerkleLeaf(to, info, merkleRoot, merkleProofs);
        require(leaf != bytes32(0), "Pix: invalid proof");
        require(!leafUsed[leaf], "Pix: already minted");

        leafUsed[leaf] = true;
    }
}
