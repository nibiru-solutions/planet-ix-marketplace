//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

/**
* @title Contract defining the treasury for IXT
*/
contract PIXTreasury is Ownable {
    using SafeERC20 for IERC20;

    IERC20 public immutable pixToken;

    /**
    * @notice Constructor for this contract
    * @param pixt Address of the IXT contract
    */
    constructor(address pixt) {
        require(pixt != address(0), "Treasury: INVALID_PIXT");
        pixToken = IERC20(pixt);
    }

    /**
    * @notice Function to empty the treasury
    * @param to The address where the IXT should be deposited
    */
    function transfer(address to) external onlyOwner {
        pixToken.safeTransfer(to, pixToken.balanceOf(address(this)));
    }
}
