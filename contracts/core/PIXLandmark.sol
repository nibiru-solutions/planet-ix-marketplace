//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/IPIXLandmark.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/StringsUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/extensions/ERC1155SupplyUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../interfaces/IOracleManager.sol";
import "../interfaces/ISwapManager.sol";
import "../interfaces/ITrustedMintable.sol";

contract PIXLandmark is IPIXLandmark, ERC1155SupplyUpgradeable, OwnableUpgradeable, ITrustedMintable {
    using StringsUpgradeable for uint256;

    /* State Variables */
    string private s_name;
    string private s_symbol;
    string private s_baseURIExtended;

    mapping(uint256 => PIXCategory) private s_landCategories;
    mapping(uint256 => uint256) private s_maxSupplies;
    mapping(address => bool) private s_moderators;
    mapping(address => bool) public s_trusted;

    IOracleManager s_oracle;
    ISwapManager s_swapManager;

    uint256 s_price;
    bool s_profitState;
    address s_usdtToken;
    address s_ixtToken;

    uint256[] public s_tokensByRarity;
    mapping(uint256 => uint256) public s_tokenRarityIndices; // tokenId => index in array

    /* Modifiers */
    modifier onlyMod() {
        require(s_moderators[msg.sender], "Landmark: NON_MODERATOR");
        _;
    }

    modifier onlyTrusted() {
        require(s_trusted[msg.sender], "Landmark: NOT_TRUSTED");
        _;
    }

    /* Functions */
    /**
     * @notice Initializer for the PIXLandmark contract
     */
    function initialize() external initializer {
        __ERC1155Supply_init();
        __ERC1155_init("");
        __Ownable_init();

        s_moderators[msg.sender] = true;
        s_name = "PIX LANDMARK";
        s_symbol = "PIXL";
        s_profitState = true;
    }

    /**
     * @notice Used to get the collection name
     * @return Name
     */
    function name() public view returns (string memory) {
        return s_name;
    }

    /**
     * @notice Used to get the collection symbol
     * @return Symbol
     */
    function symbol() public view returns (string memory) {
        return s_symbol;
    }

    // @inheritdoc IPIXLandmark
    function getMaxSupply(uint256 _tokenId) external view override returns (uint256) {
        return s_maxSupplies[_tokenId];
    }

    // @inheritdoc IPIXLandmark
    function getLandCategory(uint256 _tokenId) external view override returns (PIXCategory) {
        return s_landCategories[_tokenId];
    }

    // @inheritdoc IPIXLandmark
    function isModerator(address _walletAddress) external view override returns (bool) {
        return s_moderators[_walletAddress];
    }

    // @inheritdoc ERC1155Upgradeable
    function uri(uint256 _tokenId) public view override returns (string memory) {
        require(_tokenId > 0, "Landmark: NOT_EXISTING");
        return string(abi.encodePacked(s_baseURIExtended, _tokenId.toString()));
    }

    // @inheritdoc IPIXLandmark
    function setModerator(address _moderator, bool _approved) external override onlyOwner {
        require(_moderator != address(0), "Landmark: INVALID_MODERATOR");
        s_moderators[_moderator] = _approved;
    }

    // @inheritdoc IPIXLandmark
    function setCategories(uint256[] calldata _tokenIds, PIXCategory[] calldata _categories) external override onlyOwner {
        require(_tokenIds.length == _categories.length, "Landmark: INVALID_ARGUMENTS");
        uint256[] memory tokenIdsMem = _tokenIds;
        PIXCategory[] memory categoriesMem = _categories;
        for (uint256 i; i < categoriesMem.length; i++) s_landCategories[tokenIdsMem[i]] = categoriesMem[i];
    }

    // @inheritdoc IPIXLandmark
    function setMaxSupplies(uint256[] calldata _tokenIds, uint256[] calldata _supplies) external override onlyOwner {
        require(_tokenIds.length == _supplies.length, "Landmark: INVALID_ARGUMENTS");
        uint256[] memory tokenIdsMem = _tokenIds;
        uint256[] memory suppliesMem = _supplies;
        for (uint256 i; i < suppliesMem.length; i++) {
            require(totalSupply(tokenIdsMem[i]) <= suppliesMem[i], "Landmark: AMOUNT_TOO_SMALL");
            s_maxSupplies[tokenIdsMem[i]] = suppliesMem[i];
        }
    }

    // @inheritdoc IPIXLandmark
    function setBaseURI(string memory _baseURI) external override onlyOwner {
        s_baseURIExtended = _baseURI;
    }

    // @inheritdoc IPIXLandmark
    function setOracleManager(address _oracleManager) external override onlyOwner {
        s_oracle = IOracleManager(_oracleManager);
    }

    // @inheritdoc IPIXLandmark
    function setSwapManager(address _swapManager) external override onlyOwner {
        s_swapManager = ISwapManager(_swapManager);
    }

    // @inheritdoc IPIXLandmark
    function setPrice(uint256 _price) external override onlyOwner {
        s_price = _price;
    }

    // @inheritdoc IPIXLandmark
    function setUsdtAddress(address _contractAddress) external override onlyOwner {
        s_usdtToken = _contractAddress;
    }

    // @inheritdoc IPIXLandmark
    function setIxtAddress(address _contractAddress) external override onlyOwner {
        s_ixtToken = _contractAddress;
    }

    // @inheritdoc IPIXLandmark
    function changeProfitState() external override onlyOwner {
        s_profitState = !s_profitState;
    }

    // @inheritdoc IPIXLandmark
    function safeMint(
        address _walletAddress,
        uint256 _tokenId,
        uint256 _amount
    ) external override onlyMod {
        require(totalSupply(_tokenId) + _amount <= s_maxSupplies[_tokenId], "Landmark: AMOUNT_TOO_BIG");
        _safeMint(_walletAddress, _tokenId, _amount);
    }

    // @inheritdoc IPIXLandmark
    function batchMint(
        address[] calldata _walletAddresses,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override onlyMod {
        require(_walletAddresses.length == _tokenIds.length && _walletAddresses.length == _amounts.length, "Landmark: INVALID_ARGUMENTS");
        address[] memory recipientsMem = _walletAddresses;
        uint256[] memory tokenIdsMem = _tokenIds;
        uint256[] memory amountsMem = _amounts;
        for (uint256 i; i < amountsMem.length; i++) {
            require(totalSupply(tokenIdsMem[i]) + amountsMem[i] <= s_maxSupplies[tokenIdsMem[i]], "Landmark: AMOUNT_TOO_BIG");
            _safeMint(recipientsMem[i], tokenIdsMem[i], amountsMem[i]);
        }
    }

    // @inheritdoc IPIXLandmark
    function mint(uint256 _amount, address _currency) external payable override {
        require(_amount > 0 && _amount <= 50, "Landmark: INVALID_MINT_AMOUNT");
        require(totalSupply(2) + _amount < s_maxSupplies[2], "Landmark: AMOUNT_TOO_BIG");
        require(_currency == address(0), "Landmark: INVALID_CURRENCY");

        uint256 totalPrice = s_price * _amount;

        if (_currency != s_usdtToken) {
            totalPrice = s_oracle.getAmountOut(s_usdtToken, _currency, totalPrice);
        }
        if (!IERC20(_currency).transferFrom(msg.sender, address(this), totalPrice)) revert("LANDMARK: TRANSDER_FAILED");
        if (!s_profitState && (_currency != s_usdtToken)) {
            IERC20(_currency).approve(address(s_swapManager), totalPrice);
            s_swapManager.swap(_currency, s_usdtToken, totalPrice, address(this));
        }

        _safeMint(msg.sender, 2, _amount);
    }

    // @inheritdoc IPIXLandmark
    function withdraw(address[] calldata tokens) external override onlyOwner {
        for (uint256 i; i < tokens.length; i += 1) {
            IERC20 token = IERC20(tokens[i]);
            if (token.balanceOf(address(this)) > 0) {
                token.transferFrom(address(this), msg.sender, token.balanceOf(address(this)));
            }
        }
    }

    /**
     * @notice Internal function to mint Landmarks
     * @param _to The account to receive Landmark
     * @param _tokenId The token id to be minted
     * @param _amount The amount of Landmark shares to be minted
     * @notice emit {LandmarkMinted} event
     */
    function _safeMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) internal onlyMod {
        _mint(_to, _tokenId, _amount, "");
        emit LandmarkMinted(_to, _tokenId, _amount, s_landCategories[_tokenId]);
    }

    /// @inheritdoc ITrustedMintable
    function trustedMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) external onlyTrusted {
        uint256 indexOfTokenId = s_tokenRarityIndices[_tokenId];
        uint256 idToMint;
        bool shouldMint;
        for (uint256 i = indexOfTokenId; i < s_tokensByRarity.length; i++) {
            uint256 mintedSupply = totalSupply(s_tokensByRarity[i]);
            uint256 maxSupply = s_maxSupplies[s_tokensByRarity[i]];
            if (mintedSupply + _amount <= maxSupply) {
                idToMint = s_tokensByRarity[i];
                shouldMint = true;
                break;
            }
        }
        if (shouldMint) {
            _mint(_to, idToMint, _amount, "");
            emit LandmarkMinted(_to, idToMint, _amount, s_landCategories[_tokenId]);
        }
    }

    function setTokensByRarity(uint256[] calldata _tokensByRarity) external onlyOwner {
        s_tokensByRarity = _tokensByRarity;
    }

    function setTokenRarityIndices(uint256[] calldata _tokenIds, uint256[] calldata _indices) external onlyOwner {
        require(_tokenIds.length == _indices.length, "LANDMARK: INVALIDE_ARRAY_LENGHTS");
        for (uint256 i; i < _tokenIds.length; i++) s_tokenRarityIndices[_tokenIds[i]] = _indices[i];
    }

    /// @inheritdoc ITrustedMintable
    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external onlyTrusted {
        require(_tokenIds.length == _amounts.length, "Landmark: INVALID_ARGUMENTS");
        uint256[] memory tokenIdsMem = _tokenIds;
        uint256[] memory amountsMem = _amounts;
        for (uint256 i; i < amountsMem.length; i++) {
            if (totalSupply(tokenIdsMem[i]) + amountsMem[i] > s_maxSupplies[tokenIdsMem[i]]) {
                continue;
            }
            _mint(_to, tokenIdsMem[i], amountsMem[i], "");
            emit LandmarkMinted(_to, tokenIdsMem[i], amountsMem[i], s_landCategories[tokenIdsMem[i]]);
        }
    }

    function setTrusted(address _addr, bool _isTrusted) external onlyMod {
        require(_addr != address(0), "Landmark: ZERO_ADDRESS");
        s_trusted[_addr] = _isTrusted;
    }
}
