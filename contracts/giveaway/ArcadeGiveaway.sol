// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/IArcadeGiveaway.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";

contract ArcadeGiveaway is IArcadeGiveaway, OwnableUpgradeable {
    using ECDSAUpgradeable for bytes32;

    event AddClaiming(uint256 indexed giveawayId, uint256 claimingId);
    event SetClaimingActive(uint256 indexed giveawayId, uint256 indexed claimingId, bool active);
    event Claimed(
        uint256 indexed withTokenId,
        uint256 indexed giveawayId,
        uint256 indexed claimingId
    );
    event ModeratorUpdated(address indexed moderator, bool approved);
    event SetMintingSinger(address mintingSigner);
    event CreateGiveaway(
        uint256 indexed giveawayCount,
        address indexed rewardToken,
        uint256 startTokenId,
        uint256 endTokenId,
        bool needsProof
    );

    address public mintingSigner;

    mapping(address => bool) public moderators;

    uint256 public giveawayCount;
    mapping(uint256 => Giveaway) public giveaways;

    mapping(uint256 => mapping(uint256 => mapping(uint256 => bool))) public claimed; //giveawayId => tokenId => claimingId

    modifier onlyMods() {
        require(msg.sender == owner() || moderators[msg.sender], "Giveaway: NOT_GOVERNANCE");
        _;
    }

    function initialize() public initializer {
        __Ownable_init();
    }

    /**
     * @notice Manage moderators of this contract
     * @param moderator Address to be managed
     * @param approved Whether or not this address should be a moderator
     */
    function setModerator(address moderator, bool approved) external onlyOwner {
        require(moderator != address(0), "Giveaway: INVALID_MODERATOR");
        moderators[moderator] = approved;
        emit ModeratorUpdated(moderator, approved);
    }

    /**
     * @notice Set a signer address for proofs
     * @param newMintingSigner new signer address
     */
    function setMintingSigner(address newMintingSigner) external onlyOwner {
        mintingSigner = newMintingSigner;
        emit SetMintingSinger(newMintingSigner);
    }

    /**
     * @notice Creates new giveaway in the contract
     * @param rewardToken ERC721 token which user should own to be able to claim reward
     * @param startTokenId minimum token id which counts for rewards
     * @param endTokenId maximum token id which counts for rewards
     * @param needsProof flag if it is crosschain giveaway and proof is needed
     * @param claimings data about each reward inside this giveaway
     */
    function createGiveaway(
        address rewardToken,
        uint256 startTokenId,
        uint256 endTokenId,
        bool needsProof,
        Claiming[] calldata claimings
    ) external onlyMods {
        giveawayCount++;

        giveaways[giveawayCount].rewardToken = rewardToken;
        giveaways[giveawayCount].startTokenId = startTokenId;
        giveaways[giveawayCount].endTokenId = endTokenId;
        giveaways[giveawayCount].needsProof = needsProof;
        addClaimingsToGiveaway(giveawayCount, claimings);

        emit CreateGiveaway(giveawayCount, rewardToken, startTokenId, endTokenId, needsProof);
    }

    function _getGiveaway(uint256 giveawayId) internal view returns (Giveaway storage) {
        require(giveawayId > 0 && giveawayId <= giveawayCount, "Giveaway: WRONG_GIVEAWAY_ID");
        return giveaways[giveawayId];
    }

    /**
     * @notice Adds new claimings (rewards) inside existing giveaway
     * @param giveawayId id of giveaway where claimings are added
     * @param claimings data about rewards
     */
    function addClaimingsToGiveaway(uint256 giveawayId, Claiming[] calldata claimings)
        public
        onlyMods
    {
        Giveaway storage giveaway = _getGiveaway(giveawayId);

        for (uint256 i = 0; i < claimings.length; i++) {
            giveaway.claimings.push(claimings[i]);
            emit AddClaiming(giveawayId, giveaway.claimings.length - 1);
        }
    }

    /**
     * @notice Disables or enables claiming inside of giveaway
     * @param giveawayId id of giveaway
     * @param claimingIds indexes of claimings inside of given giveaway
     * @param actives array of booleans, for coresponding claimingId shows if it should be enabled or disabled
     */
    function setClaimingsActive(
        uint256 giveawayId,
        uint256[] calldata claimingIds,
        bool[] calldata actives
    ) external onlyMods {
        require(claimingIds.length == actives.length, "Giveaway: NOT_SAME_LENGTH");
        Giveaway storage giveaway = _getGiveaway(giveawayId);

        for (uint256 i = 0; i < claimingIds.length; i++) {
            giveaway.claimings[claimingIds[i]].active = actives[i];
            emit SetClaimingActive(giveawayId, claimingIds[i], actives[i]);
        }
    }

    function _claim(
        uint256 giveawayId,
        uint256[] calldata withTokenIds,
        uint256[] calldata claimingIds
    ) internal {
        Giveaway storage giveaway = _getGiveaway(giveawayId);
        mapping(uint256 => mapping(uint256 => bool)) storage tokenClaims = claimed[giveawayId];

        for (uint256 i = 0; i < claimingIds.length; i++) {
            uint256 claimingId = claimingIds[i];

            require(claimingId < giveaway.claimings.length, "Giveaway: WRONG_CLAIMING_ID");

            Claiming storage claiming = giveaway.claimings[claimingId];
            require(claiming.active, "Giveaway: CLAIMING_NOT_ACTIVE");

            for (uint256 j = 0; j < withTokenIds.length; j++) {
                uint256 withTokenId = withTokenIds[j];

                require(
                    withTokenId >= giveaway.startTokenId && withTokenId <= giveaway.endTokenId,
                    "Giveaway: WRONG_TOKEN_ID"
                );

                require(!tokenClaims[withTokenId][claimingId], "Giveaway: ALREADY_CLAIMED");
                tokenClaims[withTokenId][claimingId] = true;

                emit Claimed(withTokenId, giveawayId, claimingId);
            }

            uint256 totalAmount = claiming.amount * withTokenIds.length;
            claiming.giveawayHanlder.handleGiveaway(msg.sender, claiming.tokenId, totalAmount);
        }
    }

    /**
     * @notice Claims rewards from giveaway. User must own token on this chain when calling this method.
     * @param giveawayId id of giveaway for which reward is claimed
     * @param withTokenIds token ids used to claim rewards
     * @param claimingIds indexes of claimings inside of giveaway for which user wants to claim rewards
     */
    function claim(
        uint256 giveawayId,
        uint256[] calldata withTokenIds,
        uint256[] calldata claimingIds
    ) external override {
        Giveaway storage giveaway = _getGiveaway(giveawayId);

        require(giveaway.needsProof == false, "Giveaway: NEEDS_PROOF");

        for (uint256 i = 0; i < withTokenIds.length; i++) {
            require(
                IERC721(giveaway.rewardToken).ownerOf(withTokenIds[i]) == msg.sender,
                "Giveaway: NOT_OWNER"
            );
        }

        _claim(giveawayId, withTokenIds, claimingIds);
    }

    function _verifySignature(bytes memory message, bytes memory signature)
        private
        view
        returns (bool)
    {
        address signer = keccak256(message).toEthSignedMessageHash().recover(signature);
        return (signer == mintingSigner);
    }

    /**
     * @notice Claims rewards from giveaway with given signature from backend. It means that base token is on other chain.
     * @param giveawayId id of giveaway for which reward is claimed
     * @param withTokenIds token ids used to claim rewards
     * @param claimingIds indexes of claimings inside of giveaway for which user wants to claim rewards
     * @param signatureProof signature obtained from backend
     * @param signatureTtl timestamp until signature is valid
     */
    function claimWithProof(
        uint256 giveawayId,
        uint256[] calldata withTokenIds,
        uint256[] calldata claimingIds,
        bytes calldata signatureProof,
        uint256 signatureTtl
    ) external override {
        require(block.timestamp <= signatureTtl, "Giveaway: TTL_PASSED");

        require(
            _verifySignature(
                abi.encode(msg.sender, giveawayId, withTokenIds, claimingIds, signatureTtl),
                signatureProof
            ),
            "Giveaway: INVALID_SIGNATURE"
        );

        Giveaway storage giveaway = _getGiveaway(giveawayId);

        require(giveaway.needsProof == true, "Giveaway: PROOF_NOT_NEEDED");

        _claim(giveawayId, withTokenIds, claimingIds);
    }

    /**
     * @notice Get all existing giveaways inside of this contract
     * @return retGiveaways list of all giveaways which exists
     */
    function getGiveaways() external view override returns (Giveaway[] memory) {
        Giveaway[] memory retGiveaways = new Giveaway[](giveawayCount);

        for (uint256 i = 1; i <= giveawayCount; i++) {
            retGiveaways[i - 1] = giveaways[i];
        }

        return retGiveaways;
    }

    /**
     * @notice Get all available claimings (rewards) for given giveaway id
     * @param giveawayId id of giveaway
     * @return claimings list of claimings inside of given giveaway
     */
    function getClaimings(uint256 giveawayId) external view override returns (Claiming[] memory) {
        Giveaway storage giveaway = _getGiveaway(giveawayId);
        return giveaway.claimings;
    }

    /**
     * @notice Gets list of claimed rewards inside giveaway for given token id
     * @param giveawayId id of giveaway
     * @param withTokenId id of token for which to check
     * @return bool[] returns list of booleans, for each claiming inside of giveaway it is true if given token already claimed
     */
    function getAlreadyClaimedForTokenId(uint256 giveawayId, uint256 withTokenId)
        external
        view
        override
        returns (bool[] memory)
    {
        Giveaway storage giveaway = _getGiveaway(giveawayId);
        bool[] memory alreadyClaimed = new bool[](giveaway.claimings.length);

        mapping(uint256 => bool) storage tokenClaims = claimed[giveawayId][withTokenId];

        for (uint256 i = 0; i < giveaway.claimings.length; i++) {
            alreadyClaimed[i] = tokenClaims[i];
        }

        return alreadyClaimed;
    }
}
