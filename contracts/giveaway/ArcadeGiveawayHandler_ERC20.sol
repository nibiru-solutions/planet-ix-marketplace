// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "hardhat/console.sol";

import "../interfaces/IArcadeGiveawayTokenHandler.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

contract ArcadeGiveawayHandler_ERC20 is IArcadeGiveawayTokenHandler, OwnableUpgradeable {
    using SafeERC20Upgradeable for IERC20Upgradeable;

    address public tokenAddress;
    address public arcadeGiveawayAddress;

    function initialize(address _tokenAddress, address _arcadeGiveawayAddress) public initializer {
        __Ownable_init();

        tokenAddress = _tokenAddress;
        arcadeGiveawayAddress = _arcadeGiveawayAddress;
    }

    modifier onlyArcadeGiveaway() {
        require(msg.sender == arcadeGiveawayAddress, "GiveawayHandler20: NOT_GIVEAWAY");
        _;
    }

    function handleGiveaway(address to, uint256 /*tokenId*/, uint256 amountTimes) external override onlyArcadeGiveaway {
        IERC20Upgradeable(tokenAddress).safeTransfer(to, amountTimes);
    }

    function withdrawTokens(uint256 amount) external onlyOwner {
        IERC20Upgradeable(tokenAddress).safeTransfer(
            msg.sender, 
            amount
        );
    }
}
