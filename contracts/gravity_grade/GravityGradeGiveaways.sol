pragma solidity ^0.8.0;

import "./IGravityGrade.sol";
import "./IGravityGradeGiveaways.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/draft-EIP712Upgradeable.sol";

contract GravityGradeGiveaways is IGravityGradeGiveaways, OwnableUpgradeable, EIP712Upgradeable {
    uint256 public s_totalGiveaways;
    mapping(uint256 => GiveawayInfo) private s_idToGiveaway;
    mapping(address => mapping(uint256 => bool)) private s_hasClaimed;
    mapping(address => mapping(uint256 => uint256)) public nonces;

    address private s_signer;
    IGravityGrade s_gravityGrade;

    bytes32 private constant AIRDROP_MESSAGE =
        keccak256("AirdropMessage(uint256 id,address sender,uint256 nonce)");

    modifier giveawayValid(uint256 _id) {
        if (s_idToGiveaway[_id].id == 0 || s_idToGiveaway[_id].id > s_totalGiveaways)
            revert GG_Giveaway_InvalidId(_id);
        _;
    }

    function initialize() public initializer {
        __Ownable_init();
        __EIP712_init("GG__Giveaway", "1");
    }

    function claim(
        uint256 _id,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external giveawayValid(_id) {
        GiveawayInfo memory giveawayInfo = s_idToGiveaway[_id];
        if (giveawayInfo.claimed >= giveawayInfo.cap) revert GG_Giveaway_MaxAmountExceeded(_id);
        if (s_hasClaimed[msg.sender][_id]) revert GG_Giveaway_AlreadyClaimed(msg.sender);

        uint256 nonce = nonces[msg.sender][_id]++;

        bytes32 structHash = keccak256(abi.encode(AIRDROP_MESSAGE, _id, msg.sender, nonce));
        bytes32 hash = _hashTypedDataV4(structHash);
        address signer = ECDSAUpgradeable.recover(hash, v, r, s);
        if (signer != s_signer) revert GG_Giveaway_InvalidSignature();

        address[] memory sender = new address[](1);
        uint256[] memory tokenId = new uint256[](1);
        uint256[] memory amount = new uint256[](1);

        sender[0] = msg.sender;
        tokenId[0] = giveawayInfo.tokenId;
        amount[0] = giveawayInfo.amount;

        IGravityGrade(s_gravityGrade).airdrop(sender, tokenId, amount);

        unchecked {
            ++giveawayInfo.claimed;
        }

        s_idToGiveaway[_id] = giveawayInfo;
        s_hasClaimed[msg.sender][_id] = true;

        emit GiveawayClaimed(msg.sender, _id);
    }

    function createGiveaway(
        uint256 _tokenId,
        uint256 _amount,
        uint256 _totalAmount
    ) external onlyOwner {
        unchecked {
            ++s_totalGiveaways;
        }

        s_idToGiveaway[s_totalGiveaways] = GiveawayInfo({
            id: s_totalGiveaways,
            tokenId: _tokenId,
            amount: _amount,
            claimed: 0,
            cap: _totalAmount
        });

        emit GiveawayCreated(s_totalGiveaways, _tokenId, _amount, _totalAmount);
    }

    function updateGiveaway(
        uint256 _id,
        uint256 _tokenId,
        uint256 _amount,
        uint256 _totalAmount
    ) external onlyOwner {
        GiveawayInfo storage giveawayInfo = s_idToGiveaway[_id];

        giveawayInfo.tokenId = _tokenId;
        giveawayInfo.amount = _amount;
        giveawayInfo.cap = _totalAmount;

        emit GiveawayUpdated(_id, _tokenId, _amount, _totalAmount);
    }

    function deleteGiveaway(uint256 _id) external onlyOwner giveawayValid(_id) {
        delete s_idToGiveaway[_id];
        emit GiveawayDeleted(_id);
    }

    function setGravityGrade(address _gravityGrade) external onlyOwner {
        s_gravityGrade = IGravityGrade(_gravityGrade);
        emit GravityGradeSet(_gravityGrade);
    }

    function setSigner(address _signer) external onlyOwner {
        s_signer = _signer;
        emit SignerSet(_signer);
    }

    function getGiveawayInfo(uint256 _id)
        external
        view
        giveawayValid(_id)
        returns (GiveawayInfo memory _info)
    {
        _info = s_idToGiveaway[_id];
    }

    function canClaim(address _user, uint256 _id)
        external
        view
        giveawayValid(_id)
        returns (bool _canClaim)
    {
        _canClaim = !s_hasClaimed[_user][_id];
    }
}
