// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.8;

import "./GravityGrade.sol";

interface IGravityGradeMintWrapper {
    function setGravityGradeContract(address _gravityGradeContract) external;

    function setTrusted(address _trusted, bool _isTrusted) external;

    function isTrusted(address _trusted) external view returns (bool);
}