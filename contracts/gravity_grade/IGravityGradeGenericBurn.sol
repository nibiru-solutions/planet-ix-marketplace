pragma solidity ^0.8.0;

import "./IGravityGradeBurn.sol";
import "../util/IGenericBurn.sol";

// @title Interface for a generic GG burn contract
// @dev IGenericBurn replaces the previous GG burn interface, though it's backwards compatible
interface IGravityGradeGenericBurn is IGenericBurn {
    /**
     * @notice Event emitted when the GG address is set
     * @param _address GG address
     */
    event GravityGradeSet(address _address);

    /**
     * @notice Sets the address to gravity grade
     * @param _gravityGrade The address
     *
     * Throws GB__NotGov on non gov call
     *
     * Emits GravityGradeSet
     */
    function setGravityGrade(address _gravityGrade) external;
}
