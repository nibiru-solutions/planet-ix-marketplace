// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

/// @title Interface defining a burn contract for Gravity Grade tokens
interface IGravityGradeBurn {
    /**
    * @notice Event emitted upon opening packs
    * @param _opener The address of the opener
    * @param _tokenId The tokenid that was "opened"
    * @param _numPacks The number of tokens that were "opened"
    */
    event PackOpened(address _opener, uint _tokenId, uint _numPacks);

    error GravityGradeBurn__InvalidTokenId(uint _tokenId);
    /**
     * @notice Burns Gravity Grade Token Packs
     * @param _tokenId The tokenId of the pack to burn
     * @param _amount The amount of tokens to burn
     */
    function burnPack(uint256 _tokenId, uint _amount) external;

}
