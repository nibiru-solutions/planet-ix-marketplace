// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./ISaleBonuses.sol";
import "../util/IConditionalProvider.sol";
import "../interfaces/ITrustedMintable.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/utils/introspection/IERC165.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165CheckerUpgradeable.sol";

///@notice Interface used for interacting with the VRF oracle
interface IVRFConsumerBaseV2 {
    /**
     * @notice Requests a random number from the vrf oracle
     * @param draws The number of draws requested
     * @return requestID the id of the random number request
     */
    function getRandomNumber(uint32 draws) external returns (uint256 requestID);
}

contract SaleBonuses is ISaleBonuses, OwnableUpgradeable {
    /*------------------- STATE VARIABLES -------------------*/

    /// @notice All category ids belonging to a tokenId, (tokenId => categoryIds)
    mapping(address => mapping(uint256 => uint256[])) private s_categoryIds;
    /// @notice Total categories belonging to a tokenId, (tokenId => total categories)
    mapping(address => mapping(uint256 => uint256)) private s_totalCategories;
    /// @notice All categories belonging to a tokenId, (tokenId => ContentCategory[])
    mapping(address => mapping(uint256 => ContentCategory[])) private s_categories;
    /// @notice Shows whether a category is active, (tokenId => categoryId => bool)
    mapping(address => mapping(uint256 => mapping(uint256 => bool))) private s_categoryActive;
    /// @notice Shows the index where a category can be found, (tokenId => categoryId => index)
    mapping(address => mapping(uint256 => mapping(uint256 => uint256))) private s_categoryIndex;
    /// @notice Contract with eligibility requirements for category, (tokenId => categoryId => eligibility)
    mapping(address => mapping(uint256 => mapping(uint256 => IConditionalProvider)))
        private s_eligibility;
    /// @notice Contract with eligibility requirements for bonus, (id => eligibility)
    mapping(address => mapping(uint256 => IConditionalProvider)) private s_bonusEligibility;
    /// @notice Max number of draws any category can have for a particular token (tokenId => maxDraws)
    mapping(address => mapping(uint256 => uint32)) maxDrawsPerCategory;

    /// @notice Oracle being used for randomness
    IVRFConsumerBaseV2 private s_randNumOracle;

    /// @notice TokenId for a VRF request
    mapping(uint256 => uint256) s_requestSaleId;
    /// @notice User for a VRF request
    mapping(uint256 => address) private s_requestUser;
    /// @notice Ids of categories a user didn't qualify for on a VRF request
    mapping(uint256 => uint256[]) private s_requestExcludedIds;
    /// @notice Number of openings for a VRF request
    mapping(uint256 => uint256) private s_requestOpenings;
    /// @notice Number of random words needed
    mapping(uint256 => uint256) private s_requestRandWords;
    mapping(uint256 => address) private s_requestAddr;

    /// @notice TokenIds which are whitelisted for burning
    mapping(uint256 => bool) private s_whitelistedTokens;
    /// @notice Governance
    address private s_governance;
    mapping(address => bool) private s_trusted;

    /*------------------- MODIFIERS -------------------*/

    modifier onlyOracle() {
        if (msg.sender != address(s_randNumOracle)) revert SB__NotOracle();
        _;
    }

    modifier userQualifies(
        address _addr,
        uint256 _id,
        address _recipient
    ) {
        IConditionalProvider provider = s_bonusEligibility[_addr][_id];
        if (!provider.isEligible(_recipient)) revert SB__Unauthorized(_recipient);
        _;
    }

    modifier onlyGov() {
        if (msg.sender != owner() && msg.sender != s_governance)
            revert SB__Unauthorized(msg.sender);
        _;
    }

    modifier onlyTrusted() {
        if (!s_trusted[msg.sender]) revert SB__Unauthorized(msg.sender);
        _;
    }

    /*------------------- INITIALIZER -------------------*/

    function initialize() public initializer {
        __Ownable_init();
    }

    /*------------------- ADMIN - ONLY FUNCTIONS -------------------*/

    /**
     * @notice Sets VRF consumer to be used
     * @param _vrfOracle The address of the oracle
     */
    function setVRFOracle(address _vrfOracle) external onlyGov {
        if (_vrfOracle == address(0)) revert SB__ZeroAddress();
        s_randNumOracle = IVRFConsumerBaseV2(_vrfOracle);
    }

    /**
     * @notice Sets governace
     * @param _governance The address of the oracle
     */
    function setGovernance(address _governance) external onlyGov {
        s_governance = _governance;
    }

    function setTrusted(address _client, bool _isTrusted) external onlyGov {
        s_trusted[_client] = _isTrusted;
    }

    function createContentCategory(address _addr, uint256 _id)
        external
        onlyGov
        returns (uint256 _categoryId)
    {
        unchecked {
            _categoryId = ++s_totalCategories[_addr][_id];
        }
        s_categories[_addr][_id].push(
            ContentCategory({
                id: _categoryId,
                contentAmountsTotalWeight: 0,
                contentsTotalWeight: 0,
                contentAmounts: new uint256[](0),
                contentAmountsWeights: new uint256[](0),
                tokenAmounts: new uint256[](0),
                tokenWeights: new uint256[](0),
                tokens: new address[](0),
                tokenIds: new uint256[](0)
            })
        );
        s_categoryIds[_addr][_id].push(_categoryId);
        s_categoryIndex[_addr][_id][_categoryId] = s_categories[_addr][_id].length - 1;
        s_categoryActive[_addr][_id][_categoryId] = true;

        emit CategoryCreated(_addr, _id, _categoryId);
    }

    function deleteContentCategory(
        address _addr,
        uint256 _id,
        uint256 _contentCategory
    ) external onlyGov {
        if (!s_categoryActive[_addr][_id][_contentCategory])
            revert SB__InvalidCategoryId(_contentCategory);

        uint256 index = s_categoryIndex[_addr][_id][_contentCategory];
        for (uint256 i = index; i < s_categories[_addr][_id].length - 1; ) {
            s_categories[_addr][_id][i] = s_categories[_addr][_id][i + 1];
            s_categoryIds[_addr][_id][i] = s_categoryIds[_addr][_id][i + 1];
            s_categoryIndex[_addr][_id][s_categories[_addr][_id][i].id] = i;
            unchecked {
                ++i;
            }
        }
        s_categoryActive[_addr][_id][_contentCategory] = false;
        s_categoryIds[_addr][_id].pop();
        s_categories[_addr][_id].pop();

        emit CategoryDeleted(_addr, _id, _contentCategory);
    }

    function setContentAmounts(
        address _addr,
        uint256 _id,
        uint256 _contentCategory,
        uint256[] calldata _amounts,
        uint256[] calldata _weights
    ) external onlyGov {
        if (!s_categoryActive[_addr][_id][_contentCategory])
            revert SB__InvalidCategoryId(_contentCategory);
        if (_amounts.length != _weights.length) revert SB__ArraysNotSameLength();

        uint256 sum;
        for (uint256 i = 0; i < _weights.length; i++) {
            if (_weights[i] == 0) revert SB__ZeroWeight();
            if (_amounts[i] > maxDrawsPerCategory[_addr][_id])
                revert SB__MaxDrawsExceeded(_amounts[i]);
            sum += _weights[i];
        }

        uint256 index = s_categoryIndex[_addr][_id][_contentCategory];

        s_categories[_addr][_id][index].contentAmounts = _amounts;
        s_categories[_addr][_id][index].contentAmountsWeights = _arrayToCumulative(_weights);
        s_categories[_addr][_id][index].contentAmountsTotalWeight = sum;

        emit ContentAmountsUpdated(_addr, _id, _contentCategory, _amounts, _weights);
    }

    function setContents(
        address _addr,
        uint256 _id,
        uint256 _contentCategory,
        ContentInputs calldata contents
    ) external onlyGov {
        if (!s_categoryActive[_addr][_id][_contentCategory])
            revert SB__InvalidCategoryId(_contentCategory);
        if (
            contents._amounts.length != contents._weights.length ||
            contents._amounts.length != contents._tokens.length ||
            contents._amounts.length != contents._ids.length
        ) revert SB__ArraysNotSameLength();

        uint256 sum;
        for (uint256 i = 0; i < contents._weights.length; i++) {
            if (contents._weights[i] == 0) revert SB__ZeroWeight();
            if (contents._amounts[i] == 0) revert SB__ZeroAmount();
            if (
                !ERC165CheckerUpgradeable.supportsInterface(
                    contents._tokens[i],
                    type(IERC721).interfaceId
                ) &&
                !ERC165CheckerUpgradeable.supportsInterface(
                    contents._tokens[i],
                    type(IERC1155).interfaceId
                )
            ) {
                revert SB__NotERC721or1155(contents._tokens[i]);
            }
            sum += contents._weights[i];
        }

        uint256 index = s_categoryIndex[_addr][_id][_contentCategory];

        s_categories[_addr][_id][index].tokenAmounts = contents._amounts;
        s_categories[_addr][_id][index].tokenWeights = _arrayToCumulative(contents._weights);
        s_categories[_addr][_id][index].contentsTotalWeight = sum;
        s_categories[_addr][_id][index].tokens = contents._tokens;
        s_categories[_addr][_id][index].tokenIds = contents._ids;

        emit ContentsUpdated(
            _addr,
            _id,
            _contentCategory,
            contents._tokens,
            contents._ids,
            contents._amounts,
            contents._weights
        );
    }

    /**
     * @notice Function for setting the maximum # of draws any category can have
     * @param _maxDraws The max number of draws any category can have
     */
    function setMaxDraws(
        address _addr,
        uint256 _id,
        uint32 _maxDraws
    ) external onlyGov {
        maxDrawsPerCategory[_addr][_id] = _maxDraws;
    }

    function setContentEligibility(
        address _addr,
        uint256 _id,
        uint256 _categoryId,
        address _conditionalProvider
    ) external onlyGov {
        if (
            !ERC165CheckerUpgradeable.supportsInterface(
                _conditionalProvider,
                type(IERC165Upgradeable).interfaceId
            ) ||
            !ERC165CheckerUpgradeable.supportsInterface(
                _conditionalProvider,
                type(IConditionalProvider).interfaceId
            )
        ) {
            revert SB__NotConditionalProvider(_conditionalProvider);
        }
        if (!s_categoryActive[_addr][_id][_categoryId]) revert SB__InvalidCategoryId(_categoryId);
        s_eligibility[_addr][_id][_categoryId] = IConditionalProvider(_conditionalProvider);

        emit CategoryEligibilitySet(_addr, _id, _categoryId, _conditionalProvider);
    }

    //TODO add events, error checks for sale existing
    function setBonusEligibility(
        address _addr,
        uint256 _id,
        address _conditionalProvider
    ) external virtual onlyGov {
        if (
            !ERC165CheckerUpgradeable.supportsInterface(
                _conditionalProvider,
                type(IERC165Upgradeable).interfaceId
            ) ||
            !ERC165CheckerUpgradeable.supportsInterface(
                _conditionalProvider,
                type(IConditionalProvider).interfaceId
            )
        ) {
            revert SB__NotConditionalProvider(_conditionalProvider);
        }
        s_bonusEligibility[_addr][_id] = IConditionalProvider(_conditionalProvider);
    }

    /*------------------- END - USER FUNCTIONS -------------------*/

    function claimBonusReward(
        uint256 _id,
        uint32 _amount,
        bool _optInConditionals,
        address _recipient
    ) external userQualifies(msg.sender, _id, _recipient) onlyTrusted {
        uint256[] memory categoryIds = s_categoryIds[msg.sender][_id];
        uint256[] memory excludedIds = new uint256[](categoryIds.length);
        uint256 numRequests;

        for (uint256 i; i < categoryIds.length; i++) {
            if (address(s_eligibility[msg.sender][_id][categoryIds[i]]) != address(0)) {
                IConditionalProvider conditionalProvider = s_eligibility[msg.sender][_id][
                    categoryIds[i]
                ];
                if (!_optInConditionals || !conditionalProvider.isEligible(_recipient)) {
                    excludedIds[numRequests] = categoryIds[i];
                    unchecked {
                        ++numRequests;
                    }
                }
            }
        }
        uint32 randWordsRequest = _amount > maxDrawsPerCategory[msg.sender][_id]
            ? _amount
            : maxDrawsPerCategory[msg.sender][_id];
        uint256 requestId = s_randNumOracle.getRandomNumber(1);
        s_requestUser[requestId] = _recipient;
        s_requestAddr[requestId] = msg.sender;
        s_requestSaleId[requestId] = _id;
        s_requestOpenings[requestId] = _amount;
        s_requestRandWords[requestId] = randWordsRequest;

        for (uint256 i; i < excludedIds.length; i++) {
            if (excludedIds[i] != 0) {
                s_requestExcludedIds[requestId].push(excludedIds[i]);
            }
        }
    }

    function getContentCategories(address _addr, uint256 _id)
        external
        view
        returns (ContentCategory[] memory _categories)
    {
        _categories = s_categories[_addr][_id];
    }

    /*------------------- INTERNAL FUNCTIONS -------------------*/

    /**
     * @notice Function for satisfying randomness requests from claimReward
     * @param requestId The particular request being serviced
     * @param randomWords Array of the random numbers requested
     */
    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords)
        external
        onlyOracle
    {
        RequestInputs memory req = RequestInputs({
            user: s_requestUser[requestId],
            saleId: s_requestSaleId[requestId],
            openings: s_requestOpenings[requestId],
            randWordsCount: s_requestRandWords[requestId],
            excludedIds: s_requestExcludedIds[requestId],
            addr: s_requestAddr[requestId]
        });

        uint256[] memory expandedValues = new uint256[](req.randWordsCount);
        for (uint256 i = 0; i < req.randWordsCount; i++) {
            expandedValues[i] = uint256(keccak256(abi.encode(randomWords[0], i)));
        }

        ContentCategory[] memory categories = s_categories[req.addr][req.saleId];
        for (uint256 i; i < categories.length; i++) {
            bool categoryExcluded;
            for (uint256 z; z < req.excludedIds.length; z++) {
                if (categories[i].id == req.excludedIds[z]) {
                    categoryExcluded = true;
                    break;
                }
            }
            if (categoryExcluded) continue;
            for (uint256 j; j < req.openings; j++) {
                uint256 target = expandedValues[j] % categories[i].contentAmountsTotalWeight;
                uint256 index = _binarySearch(categories[i].contentAmountsWeights, target);
                uint256 draws = categories[i].contentAmounts[index];
                for (uint256 k; k < draws; k++) {
                    uint256 targetContent = expandedValues[k] % categories[i].contentsTotalWeight;
                    uint256 indexContent = _binarySearch(categories[i].tokenWeights, targetContent);
                    _mintReward(req.user, categories[i], indexContent);
                }
            }
        }

        delete s_requestAddr[requestId];
        delete s_requestUser[requestId];
        delete s_requestSaleId[requestId];
        delete s_requestOpenings[requestId];
        delete s_requestRandWords[requestId];
        delete s_requestExcludedIds[requestId];
    }

    /**
     * @notice Mints appropriate rewards for the user
     * @param _user The address of the user
     * @param _category The category from which the reward should come
     * @param _index Index of the particular contents to mint
     */
    function _mintReward(
        address _user,
        ContentCategory memory _category,
        uint256 _index
    ) internal {
        ITrustedMintable(_category.tokens[_index]).trustedMint(
            _user,
            _category.tokenIds[_index],
            _category.tokenAmounts[_index]
        );
        emit RewardGranted(
            _category.tokens[_index],
            _category.tokenIds[_index],
            _category.tokenAmounts[_index]
        );
    }

    /**
     * @notice Converts an array of weights into a cumulative array
     * @param _arr The array to convert
     * @return _cumulativeArr The resultant cumulative array
     */
    function _arrayToCumulative(uint256[] memory _arr)
        private
        pure
        returns (uint256[] memory _cumulativeArr)
    {
        _cumulativeArr = new uint256[](_arr.length);
        _cumulativeArr[0] = _arr[0];
        for (uint256 i = 1; i < _arr.length; i++) {
            _cumulativeArr[i] = _cumulativeArr[i - 1] + _arr[i];
        }
    }

    /**
     * @notice Runs a binary search on an array
     * @param _arr The array to search
     * @param _target The target value
     * @return _location Index of the result
     */
    function _binarySearch(uint256[] memory _arr, uint256 _target)
        private
        pure
        returns (uint256 _location)
    {
        uint256 left;
        uint256 mid;
        uint256 right = _arr.length;
        while (left < right) {
            mid = Math.average(left, right);
            if (_target < _arr[mid]) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        if (left > 0 && _arr[left - 1] == _target) {
            return left - 1;
        } else {
            return left;
        }
    }
}
