// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

/// @title Paper.xyz extension for ERC1155 store
interface IERC1155StoreGeneric_Paper {
    struct PaperInputs {
        address buyer;
        address tokenAddress;
        uint256 tokenId;
        uint256 numPurchases;
        uint256 saleId;
        bool optInBonuses;
        bool optInCategories;
        bytes32 nonce;
        bytes signature;
    }

    struct PaperSaleInputs {
        address buyer;
        address tokenAddress;
        uint256 tokenId;
        uint256 numPurchases;
        uint256 saleId;
    }

    /**
     * @notice adaptor to allow purchases via Paper.xyz
     * @dev Price is calculated implicitly from _saleId, _numPurchases
     */
    function onPaper(
        PaperSaleInputs calldata _paperInputs,
        bytes32 _nonce,
        bytes calldata _signature
    ) external;

    /**
     * @notice eligibility function to check if the player can purchase a pack based upon
     *          token ownership discounts and purchase quantity
     * @param _buyer the buyers' EOA address
     * @param _tokenAddress The token address for the tokenId claimed to be owned (for rebates)
     * @param _tokenId The token id, ignored if ERC721 is provided for the token type
     * @param _numPurchases the number of packs to purchase
     * @param _saleId the sale ID of the pack to purchase
     * @return eligible Indicating if the pack can be purchased
     */
    function tokenClaimable(
        address _buyer,
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _numPurchases,
        uint256 _saleId
    ) external view returns (bool eligible);

    /**
     * @notice calculates the exact price with ownership based discounts applied
     * @param _tokenAddress The token address for the tokenId claimed to be owned (for rebates)
     * @param _tokenId The token id for the token claimed to be owned (for rebates)
     * @param _numPurchases the number of packs to purchase
     * @param _saleId the sale ID of the pack to purchase
     * @return price The unit price
     */
    function checkPrice(
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _numPurchases,
        uint256 _saleId
    ) external returns (uint256 price);

    /**
     * @notice Set the payment currency token for paper
     * @param _paperCurrency The address of the supported paper currency token
     */
    function setPaperCurrency(address _paperCurrency) external;
}
