// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.8;

import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";

/// @title Interface defining Gravity Grade
interface IGravityGrade is IERC1155Upgradeable {
    enum GravityGradeDrops {
        UNUSED,
        CargoDrop3,
        AnniversaryPackMystery,
        AnniversaryPackOutlier,
        AnniversaryPackCommon,
        AnniversaryPackUncommon,
        AnniversaryPackRare,
        AnniversaryPackLegendary,
        StarterPack,
        CoinBaseTier1,
        CoinBaseTier2,
        GG37Small,
        GG37Medium,
        StarterPack2,
        ArcadeMedium
    }

    /**
     * @notice Event emitted when a sales state is mutated
     * @param saleId The id of the sale
     * @param isPaused Whether the sale is paused or not
     */
    event SaleState(uint256 saleId, bool isPaused);
    /**
     * @notice Event emitted when a sale is deleted
     * @param saleId The sale id
     */
    event SaleDeleted(uint256 saleId);
    /**
     * @notice Event emitted when a sales parameters are updated
     * @param saleId The sale id
     * @param tokenId The token id that is being sold
     * @param salePrice The price, denoted in the default currency
     * @param totalSupply The cap on the total amount of units to be sold
     * @param userCap The cap per user on units that can be purchased
     * @param defaultCurrency The default currency for the sale.
     */
    event SaleInfoUpdated(uint256 saleId, uint256 tokenId, uint256 salePrice, uint256 totalSupply, uint256 userCap, address defaultCurrency);
    /**
     * @notice Event emitted when the beneficiaries are updated
     * @param beneficiaries Array of beneficiary addresses
     * @param basisPoints Array of basis points for each beneficiary (by index)
     */
    event BeneficiariesUpdated(address[] beneficiaries, uint256[] basisPoints);
    /**
     * @notice Event emitted when payment currencies are added
     * @param saleId The sale id
     * @param currencyAddresses The addresses of the currencies that have been added
     */
    event PaymentCurrenciesSet(uint256 saleId, address[] currencyAddresses);
    /**
     * @notice Event emitted when payment currencies are removed
     * @param saleId The sale id
     * @param currencyAddresses The addresses of the currencies that have been removed
     */
    event PaymentCurrenciesRevoked(uint256 saleId, address[] currencyAddresses);

    // Used to classify token types in the ownership rebate struct
    enum TokenType {
        ERC721,
        ERC1155
    }

    /**
     * @notice Used to provide specifics for ownership based discounts
     * @param tokenType The type of token
     * @param tokenAddress The address of the token contract
     * @param tokenId The token id, ignored if ERC721 is provided for the token type
     * @param basisPoints The discount in basis points
     */
    struct OwnershipRebate {
        TokenType tokenType;
        address tokenAddress;
        uint256 tokenId; // ignored if ERC721
        uint256 basisPoints;
    }

    /**
     * @notice Sets the TokenURI
     * @param _tokenId The tokenId to set for the URI
     * @param _uri The URI to set for the token
     */
    function setTokenUri(uint256 _tokenId, string memory _uri) external;

    /**
     * @notice Create new emissions/sales
     * @param _tokenId The ERC1155 tokenId to sell
     * @param _salePrice Price in US dollars
     * @param _totalSupplyAmountToSell Cap on total amount to be sold
     * @param _userCap A per-user cap
     * @param _defaultCurrency Default currency (contract address)
     * @param _profitState Whether all sale profits should be instantly exchanged
        for the default currency or stored as is (false to exchange, true otherwise)
     */
    function createNewSale(
        uint256 _tokenId,
        uint256 _salePrice,
        uint256 _totalSupplyAmountToSell,
        uint256 _userCap,
        address _defaultCurrency,
        bool _profitState
    ) external returns (uint256 saleId);

    /**
     * @notice Start and pause sales
     * @param _saleId The sale ID to set the status for
     * @param _paused The sale status
     */
    function setSaleState(uint256 _saleId, bool _paused) external;

    /**
     * @notice Modify sale
     * @param _saleId The sale ID to modify
     * @param _salePrice Price in US dollars
     * @param _totalSupplyAmountToSell Cap on total amount to be sold
     * @param _userCap A per-user cap
     * @param _defaultCurrency Default currency (contract address)
     * @param _profitState Whether all sale profits should be instantly exchanged
        for the default currency or stored as is (false to exchange, true otherwise)
     */
    function modifySale(
        uint256 _saleId,
        uint256 _salePrice,
        uint256 _totalSupplyAmountToSell,
        uint256 _userCap,
        address _defaultCurrency,
        bool _profitState
    ) external;

    /**
     * @notice Adds a bulk discount to a sale
     * @param _saleId The sale id
     * @param _breakpoint At what quantity the discount should be applied
     * @param _basisPoints The non cumulative discount in basis point
     */
    function addBulkDiscount(
        uint256 _saleId,
        uint256 _breakpoint,
        uint256 _basisPoints
    ) external;

    /**
     * @notice Adds a token ownership based discount to a sale
     * @param _saleId The sale id
     * @param _info Struct containing specifics regarding the discount
     */
    function addOwnershipDiscount(uint256 _saleId, OwnershipRebate calldata _info) external;

    /**
     * @notice Delete a sale
     * @param _saleId The sale ID to delete
     */
    function deleteSale(uint256 _saleId) external;

    /**
     * @notice Set the whitelist for allowed payment currencies on a per saleId basis
     * @param _saleId The sale ID to set
     * @param _currencyAddresses The addresses of permissible payment currencies
     */
    function setAllowedPaymentCurrencies(uint256 _saleId, address[] calldata _currencyAddresses)
    external;

    /**
     * @notice Set a swap manager to manage the means through which tokens are exchanged
     * @param _swapManager SwapManager address
     */
    function setSwapManager(address _swapManager) external;

    /**
     * @notice Set a oracle manager to manage the means through which token prices are fetched
     * @param _oracleManager OracleManager address
     */
    function setOracleManager(address _oracleManager) external;

    /**
     * @notice Set administrator
     * @param _moderatorAddress The addresse of an allowed admin
     */
    function setModerator(address _moderatorAddress) external;

    /**
     * @notice Adds a trusted party, which is allowed to mint tokens through the airdrop function
     * @param _trusted The address of the trusted party
     * @param _isTrusted Whether the party is trusted or not
     */
    function setTrusted(address _trusted, bool _isTrusted) external;

    /**
     * @notice Empty the treasury into the owners or an arbitrary wallet
     * @param _walletAddress The withdrawal EOA address
     * @param _currency ERC20 currency to withdraw, ZERO address implies MATIC
     */
    function withdraw(address _walletAddress, address _currency) external payable;

    /**
     * @notice  Set Fee Wallets and fee percentages from sales
     * @param _walletAddresses The withdrawal EOA addresses
     * @param _feeBps Represented as basis points e.g. 500 == 5 pct
     */
    function setFeeWalletsAndPercentages(address[] calldata _walletAddresses, uint256[] calldata _feeBps) external;

    /**
     * @notice Purchase any active sale in any whitelisted currency
     * @param _saleId The sale ID of the pack to purchase
     * @param _numPurchases The number of packs to purchase
     * @param _tokenId The tokenId claimed to be owned (for rebates)
     * @param _tokenAddress The token address for the tokenId claimed to be owned (for rebates)
     * @param _currency Address of currency to use, address(0) for matic
     */
    function buyPacks(
        uint256 _saleId,
        uint256 _numPurchases,
        uint256 _tokenId,
        address _tokenAddress,
        address _currency
    ) external payable;

    /**
     * @notice Airdrop tokens to arbitrary wallets
     * @param _recipients The recipient addresses
     * @param _tokenIds The tokenIds to mint
     * @param _amounts The amount of tokens to mint
     */
    function airdrop(
        address[] calldata _recipients,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external;

    /**
     * @notice used to burn tokens by trusted contracts
     * @param _from address to burn tokens from
     * @param _tokenId id of to-be-burnt tokens
     * @param _amount number of tokens to burn
     */
    function burn(
        address _from,
        uint256 _tokenId,
        uint256 _amount
    ) external;
}
