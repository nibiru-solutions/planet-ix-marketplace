// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../mission_control/IAssetManager.sol";
import "../util/IGenericBurnV2.sol";
import "../util/IConditionalProvider.sol";
import "../interfaces/ITrustedMintable.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/IERC165Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165CheckerUpgradeable.sol";

///@notice Interface used for interacting with the VRF oracle
interface IVRFConsumerBaseV2 {
    /**
     * @notice Requests a random number from the vrf oracle
     * @param draws The number of draws requested
     * @return requestID the id of the random number request
     */
    function getRandomNumber(uint32 draws) external returns (uint256 requestID);
}

/**
 * @title Gravity Grade Generic Burn
 * @author Jourdan
 * @notice This contract manages burning and custom rewards for any Gravity Grade token
 */
contract GenericBurnV2 is IGenericBurnV2, OwnableUpgradeable {
    event PIXAssetsSet(address _address);
    event AvatarClaimed(address _address, address _pack, uint256 _tokenId, uint256 _amount);

    /*------------------- STATE VARIABLES -------------------*/

    /// @notice All category ids belonging to a tokenId, (tokenId => categoryIds)
    mapping(address => mapping(uint256 => uint256[])) public s_tokenCategoryIds;
    /// @notice Total categories belonging to a tokenId, (tokenId => total categories)
    mapping(address => mapping(uint256 => uint256)) private s_tokenTotalCategories;
    /// @notice All categories belonging to a tokenId, (tokenId => ContentCategory[])
    mapping(address => mapping(uint256 => ContentCategory[])) public s_tokenCategories;
    /// @notice Shows whether a category is active, (tokenId => categoryId => bool)
    mapping(address => mapping(uint256 => mapping(uint256 => bool))) private s_tokenCategoryActive;
    /// @notice Shows the index where a category can be found, (tokenId => categoryId => index)
    mapping(address => mapping(uint256 => mapping(uint256 => uint256))) private s_tokenCategoryIndex;
    /// @notice Contract with eligibility requirements for category, (tokenId => categoryId => eligibility)
    mapping(address => mapping(uint256 => mapping(uint256 => IConditionalProvider))) public s_tokenEligibility;
    /// @notice Max number of draws any category can have for a particular token (tokenId => maxDraws)
    mapping(address => mapping(uint256 => uint32)) maxDrawsPerCategory;

    /// @notice Oracle being used for randomness
    IVRFConsumerBaseV2 private s_randNumOracle;

    mapping(uint256 => address) s_requestToken;
    /// @notice TokenId for a VRF request
    mapping(uint256 => uint256) s_requestTokenId;
    /// @notice User for a VRF request
    mapping(uint256 => address) private s_requestUser;
    /// @notice Ids of categories a user didn't qualify for on a VRF request
    mapping(uint256 => uint256[]) private s_requestExcludedIds;
    /// @notice Number of openings for a VRF request
    mapping(uint256 => uint256) private s_requestOpenings;
    /// @notice Number of random words needed
    mapping(uint256 => uint256) private s_requestRandWords;

    /// @notice TokenIds which are whitelisted for burning
    mapping(address => mapping(uint256 => bool)) private s_whitelistedTokens;
    /// @notice Gravity Grade Contract
    address private pixAssets;
    /// @notice Governance
    address private s_governance;

    mapping(address => mapping(uint256 => mapping(uint256 => uint256[]))) largeContentWeights;
    mapping(address => uint256) userAvatarSeed;
    mapping(address => RequestInputs) userVRFRequestParams;
    address avatarPack;

    mapping(address => mapping(uint256 => uint256)) tokenBurnLimit;
    mapping(address => mapping(uint256 => GuaranteedReward[])) public s_guaranteedRewards; // tokenId => guaranteed rewards

    uint256 constant AVATAR_PACK_ID = 32;

    IVRFConsumerBaseV2 private s_randNumOracleV2;

    /*------------------- MODIFIERS -------------------*/

    /// @notice Makes sure function is called only by governance
    modifier onlyGov() {
        if (msg.sender != owner() && msg.sender != s_governance) revert GB__NotGov(msg.sender);
        _;
    }

    /// @notice Makes sure function is called only by vrf oracle
    modifier onlyOracle() {
        if (msg.sender != address(s_randNumOracleV2)) revert GB__NotOracle();
        _;
    }

    /*------------------- INITIALIZER -------------------*/

    ///@notice Initializer
    function initialize() public initializer {
        __Ownable_init();
    }

    /*------------------- ADMIN - ONLY FUNCTIONS -------------------*/

    /// @inheritdoc IGenericBurnV2
    function whitelistToken(
        address[] calldata _token,
        uint256[] calldata _tokenId,
        bool[] calldata _isWhitelisted
    ) external onlyGov {
        for (uint i; i < _token.length; i++) {
            s_whitelistedTokens[_token[i]][_tokenId[i]] = _isWhitelisted[i];
            emit TokenWhitelisted(_tokenId[i], _isWhitelisted[i]);
        }
    }

    /**
     * @notice Sets VRF consumer to be used
     * @param _vrfOracle The address of the oracle
     */
    function setVRFOracle(address _vrfOracle) external onlyOwner {
        if (_vrfOracle == address(0)) revert GB__ZeroAddress();
        s_randNumOracle = IVRFConsumerBaseV2(_vrfOracle);
    }

    /**
     * @notice Sets VRF consumer to be used
     * @param _vrfOracle The address of the oracle
     */
    function setVRFOracleV2(address _vrfOracle) external onlyOwner {
        if (_vrfOracle == address(0)) revert GB__ZeroAddress();
        s_randNumOracleV2 = IVRFConsumerBaseV2(_vrfOracle);
    }

    function setPixAssets(address _pixAssets) external onlyGov {
        pixAssets = _pixAssets;
        emit PIXAssetsSet(_pixAssets);
    }

    function setAvatarPack(address _avatarPack) external onlyGov {
        avatarPack = _avatarPack;
    }

    /**
     * @notice Sets governace
     * @param _governance The address of the oracle
     */
    function setGovernance(address _governance) external onlyGov {
        s_governance = _governance;
    }

    function setTokenBurnLimit(
        address[] calldata _token,
        uint256[] calldata _tokenId,
        uint256[] calldata _limit
    ) external onlyGov {
        for (uint i; i < _token.length; i++) tokenBurnLimit[_token[i]][_tokenId[i]] = _limit[i];
    }

    /// @inheritdoc IGenericBurnV2
    function setContentEligibility(
        address _token,
        uint256 _tokenId,
        uint256 _categoryId,
        address _conditionalProvider
    ) external onlyGov {
        if (
            !ERC165CheckerUpgradeable.supportsInterface(_conditionalProvider, type(IERC165Upgradeable).interfaceId) ||
            !ERC165CheckerUpgradeable.supportsInterface(_conditionalProvider, type(IConditionalProvider).interfaceId)
        ) {
            revert GB__NotConditionalProvider(_conditionalProvider);
        }
        if (!s_tokenCategoryActive[_token][_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);
        s_tokenEligibility[_token][_tokenId][_categoryId] = IConditionalProvider(_conditionalProvider);
        emit CategoryEligibilitySet(_tokenId, _categoryId, _conditionalProvider);
    }

    /// @inheritdoc IGenericBurnV2
    function createContentCategory(address _token, uint256 _tokenId) external onlyGov returns (uint256 _categoryId) {
        unchecked {
            _categoryId = ++s_tokenTotalCategories[_token][_tokenId];
        }

        s_tokenCategories[_token][_tokenId].push(
            ContentCategory({
                id: _categoryId,
                contentAmountsTotalWeight: 0,
                contentsTotalWeight: 0,
                contentAmounts: new uint256[](0),
                contentAmountsWeights: new uint256[](0),
                tokenAmounts: new uint256[](0),
                tokenWeights: new uint256[](0),
                tokens: new address[](0),
                tokenIds: new uint256[](0)
            })
        );
        s_tokenCategoryIds[_token][_tokenId].push(_categoryId);
        s_tokenCategoryIndex[_token][_tokenId][_categoryId] = s_tokenCategories[_token][_tokenId].length - 1;
        s_tokenCategoryActive[_token][_tokenId][_categoryId] = true;

        emit CategoryCreated(_tokenId, _categoryId);
    }

    /// @inheritdoc IGenericBurnV2
    function deleteContentCategory(address _token, uint256 _tokenId, uint256 _categoryId) external onlyGov {
        if (!s_tokenCategoryActive[_token][_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);

        uint256 index = s_tokenCategoryIndex[_token][_tokenId][_categoryId];
        for (uint256 i = index; i < s_tokenCategories[_token][_tokenId].length - 1; ) {
            s_tokenCategories[_token][_tokenId][i] = s_tokenCategories[_token][_tokenId][i + 1];
            s_tokenCategoryIds[_token][_tokenId][i] = s_tokenCategoryIds[_token][_tokenId][i + 1];
            s_tokenCategoryIndex[_token][_tokenId][s_tokenCategories[_token][_tokenId][i].id] = i;

            unchecked {
                ++i;
            }
        }
        s_tokenCategoryActive[_token][_tokenId][_categoryId] = false;
        s_tokenCategoryIds[_token][_tokenId].pop();
        s_tokenCategories[_token][_tokenId].pop();

        emit CategoryDeleted(_tokenId, _categoryId);
    }

    /// @inheritdoc IGenericBurnV2
    function setContentAmounts(
        address _token,
        uint256 _tokenId,
        uint256 _categoryId,
        uint256[] calldata _amounts,
        uint256[] calldata _weights
    ) external onlyGov {
        if (!s_tokenCategoryActive[_token][_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);
        if (_amounts.length != _weights.length) revert GB__ArraysNotSameLength();

        uint256 sum;
        for (uint256 i = 0; i < _weights.length; i++) {
            if (_weights[i] == 0) revert GB__ZeroWeight();
            if (_amounts[i] > maxDrawsPerCategory[_token][_tokenId]) revert GB__MaxDrawsExceeded(_amounts[i]);
            sum += _weights[i];
        }

        uint256 index = s_tokenCategoryIndex[_token][_tokenId][_categoryId];

        s_tokenCategories[_token][_tokenId][index].contentAmounts = _amounts;
        s_tokenCategories[_token][_tokenId][index].contentAmountsWeights = _arrayToCumulative(_weights);
        s_tokenCategories[_token][_tokenId][index].contentAmountsTotalWeight = sum;

        emit ContentAmountsUpdated(_tokenId, _categoryId, _amounts, _weights);
    }

    /// @inheritdoc IGenericBurnV2
    function setContents(SetContentInputs calldata args) external onlyGov {
        if (!s_tokenCategoryActive[args.token][args.tokenId][args.categoryId])
            revert GB__InvalidCategoryId(args.categoryId);
        if (
            args.amounts.length != args.weights.length ||
            args.amounts.length != args.tokens.length ||
            args.amounts.length != args.tokenIds.length
        ) revert GB__ArraysNotSameLength();

        uint256 sum;
        for (uint256 i = 0; i < args.weights.length; i++) {
            if (args.weights[i] == 0) revert GB__ZeroWeight();
            if (args.amounts[i] == 0) revert GB_ZeroAmount();

            sum += args.weights[i];
        }

        uint256 index = s_tokenCategoryIndex[args.token][args.tokenId][args.categoryId];

        s_tokenCategories[args.token][args.tokenId][index].tokenAmounts = args.amounts;
        s_tokenCategories[args.token][args.tokenId][index].tokenWeights = _arrayToCumulative(args.weights);
        s_tokenCategories[args.token][args.tokenId][index].contentsTotalWeight = sum;
        s_tokenCategories[args.token][args.tokenId][index].tokens = args.tokens;
        s_tokenCategories[args.token][args.tokenId][index].tokenIds = args.tokenIds;

        emit ContentsUpdated(args.tokenId, args.categoryId, args.tokens, args.tokenIds, args.amounts, args.weights);
    }

    function setContentLarge(
        address _token,
        uint256 _tokenId,
        uint256 _categoryId,
        address _tokens,
        uint256 _tokenIds,
        uint256 _amounts,
        uint256 _weights
    ) external onlyGov {
        if (!s_tokenCategoryActive[_token][_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);

        if (_weights == 0) revert GB__ZeroWeight();
        if (_amounts == 0) revert GB_ZeroAmount();

        uint256 index = s_tokenCategoryIndex[_token][_tokenId][_categoryId];

        largeContentWeights[_token][_tokenId][index].push(_weights);

        s_tokenCategories[_token][_tokenId][index].tokens.push(_tokens);
        s_tokenCategories[_token][_tokenId][index].tokenIds.push(_tokenIds);
        s_tokenCategories[_token][_tokenId][index].tokenAmounts.push(_amounts);

        // emit ContentsUpdated(_tokenId, _categoryId, _tokens, _tokenIds, _amounts, _weights);
    }

    function setContentLargeWeights(address _token, uint256 _tokenId, uint256 _categoryId) external onlyGov {
        if (!s_tokenCategoryActive[_token][_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);
        uint256 index = s_tokenCategoryIndex[_token][_tokenId][_categoryId];

        uint256 sum;
        for (uint256 i = 0; i < largeContentWeights[_token][_tokenId][index].length; i++) {
            sum += largeContentWeights[_token][_tokenId][index][i];
        }

        s_tokenCategories[_token][_tokenId][index].tokenWeights = _arrayToCumulative(
            largeContentWeights[_token][_tokenId][index]
        );
        s_tokenCategories[_token][_tokenId][index].contentsTotalWeight = sum;
    }

    /**
     * @notice Function for setting the maximum # of draws any category can have
     * @param _maxDraws The max number of draws any category can have
     */
    function setMaxDraws(address _token, uint256 _tokenId, uint32 _maxDraws) external onlyGov {
        maxDrawsPerCategory[_token][_tokenId] = _maxDraws;
    }

    function setGuaranteedRewards(
        address _token,
        uint256 _tokenId,
        GuaranteedReward[] memory _rewards
    ) external onlyGov {
        for (uint256 i; i < _rewards.length; i++) {
            s_guaranteedRewards[_token][_tokenId].push(_rewards[i]);
        }
        emit GuaranteedRewardSet(_tokenId, _rewards);
    }

    function deleteGuaranteedRewards(address _token, uint256 _tokenId, uint256 _position) external onlyGov {
        uint256 last = s_guaranteedRewards[_token][_tokenId].length - 1;
        s_guaranteedRewards[_token][_tokenId][_position] = s_guaranteedRewards[_token][_tokenId][last];
        s_guaranteedRewards[_token][_tokenId].pop();
    }

    function deleteAllGuaranteedRewards(address _token, uint256 _tokenId) external onlyGov {
        delete s_guaranteedRewards[_token][_tokenId];
    }

    /*------------------- END - USER FUNCTIONS -------------------*/

    /// @inheritdoc IGenericBurnV2
    function burnPack(address _token, uint256 _tokenId, uint32 _amount, bool _optInConditionals) external {
        require(_amount <= tokenBurnLimit[_token][_tokenId], "PIX BURN: BURN_LIMIT_EXCEEDED");

        if (_token == pixAssets && _tokenId == AVATAR_PACK_ID)
            require(userAvatarSeed[msg.sender] == 0, "PIX_BURN: USER HAS AN AVATAR TO CLAIM");

        if (!s_whitelistedTokens[_token][_tokenId]) revert GB__TokenNotWhitelisted(_tokenId);

        _burnTokens(_token, _tokenId, _amount);

        if (s_tokenCategories[_token][_tokenId].length != 0) {
            uint256[] memory tokenCategoryIds = s_tokenCategoryIds[_token][_tokenId];
            uint256[] memory excludedIds = new uint256[](tokenCategoryIds.length);
            uint256 numRequests;

            for (uint256 i; i < tokenCategoryIds.length; i++) {
                if (address(s_tokenEligibility[_token][_tokenId][tokenCategoryIds[i]]) != address(0)) {
                    IConditionalProvider conditionalProvider = s_tokenEligibility[_token][_tokenId][
                        tokenCategoryIds[i]
                    ];
                    if (!_optInConditionals || !conditionalProvider.isEligible(msg.sender)) {
                        excludedIds[numRequests] = tokenCategoryIds[i];
                        unchecked {
                            ++numRequests;
                        }
                    }
                }
            }
            uint32 randWordsRequest = _amount > maxDrawsPerCategory[_token][_tokenId]
                ? _amount
                : maxDrawsPerCategory[_token][_tokenId];
            uint256 requestId = s_randNumOracleV2.getRandomNumber(1);
            s_requestUser[requestId] = msg.sender;
            s_requestToken[requestId] = _token;
            s_requestTokenId[requestId] = _tokenId;
            s_requestOpenings[requestId] = _amount;
            s_requestRandWords[requestId] = randWordsRequest;

            for (uint256 i; i < excludedIds.length; i++) {
                if (excludedIds[i] != 0) {
                    s_requestExcludedIds[requestId].push(excludedIds[i]);
                }
            }
        }

        GuaranteedReward[] memory rewards = s_guaranteedRewards[_token][_tokenId];
        for (uint256 i; i < rewards.length; i++) {
            ITrustedMintable(rewards[i].token).trustedMint(
                msg.sender,
                rewards[i].tokenId,
                rewards[i].tokenAmount * _amount
            );
            emit RewardGranted(rewards[i].token, rewards[i].tokenId, rewards[i].tokenAmount);
        }

        emit PackOpened(msg.sender, _token, _tokenId, _amount);
    }

    /// @inheritdoc IGenericBurnV2
    function getContentCategories(
        address _token,
        uint256 _tokenId
    ) external view returns (ContentCategory[] memory _categories) {
        _categories = s_tokenCategories[_token][_tokenId];
    }

    function getGuaranteedRewards(
        address _token,
        uint256 _tokenId
    ) external view returns (GuaranteedReward[] memory _rewards) {
        _rewards = s_guaranteedRewards[_token][_tokenId];
    }

    function getAvatarClaimStatus(address _user) external view returns (uint256) {
        return userAvatarSeed[_user];
    }

    function claimAvatar() external {
        require(userAvatarSeed[msg.sender] != 0, "PIX_BURN: USER HAS NO RANDOM NUMBER");
        uint256 randomSeed = userAvatarSeed[msg.sender];
        userAvatarSeed[msg.sender] = 0;

        RequestInputs memory req = userVRFRequestParams[msg.sender];
        delete userVRFRequestParams[msg.sender];

        uint256[] memory expandedValues = new uint256[](req.randWordsCount);
        for (uint256 i = 0; i < req.randWordsCount; i++) {
            expandedValues[i] = uint256(keccak256(abi.encode(randomSeed, i)));
        }

        uint256 contentAmountsTotalWeight = s_tokenCategories[pixAssets][req.tokenId][0].contentAmountsTotalWeight;
        uint256 contentsTotalWeight = s_tokenCategories[pixAssets][req.tokenId][0].contentsTotalWeight;

        uint256[] memory contentAmounts = s_tokenCategories[pixAssets][req.tokenId][0].contentAmounts;
        uint256[] memory contentAmountsWeights = s_tokenCategories[pixAssets][req.tokenId][0].contentAmountsWeights;
        uint256[] memory tokenWeights = s_tokenCategories[pixAssets][req.tokenId][0].tokenWeights;
        uint256[] memory tokenIds = s_tokenCategories[pixAssets][req.tokenId][0].tokenIds;

        for (uint256 j; j < req.openings; j++) {
            uint256 target = expandedValues[j] % contentAmountsTotalWeight;
            uint256 index = _binarySearch(contentAmountsWeights, target);
            uint256 draws = contentAmounts[index];
            for (uint256 k; k < draws; k++) {
                uint256 targetContent = expandedValues[k] % contentsTotalWeight;
                uint256 indexContent = _binarySearch(tokenWeights, targetContent);
                ITrustedMintable(avatarPack).trustedMint(msg.sender, tokenIds[indexContent], 1);
                emit AvatarClaimed(msg.sender, avatarPack, tokenIds[indexContent], 1);
            }
        }
    }

    /*------------------- INTERNAL FUNCTIONS -------------------*/

    /**
     * @notice Function for satisfying randomness requests from burnPack
     * @param requestId The particular request being serviced
     * @param randomWords Array of the random numbers requested
     */
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external onlyOracle {
        RequestInputs memory req = RequestInputs({
            user: s_requestUser[requestId],
            token: s_requestToken[requestId],
            tokenId: s_requestTokenId[requestId],
            openings: s_requestOpenings[requestId],
            randWordsCount: s_requestRandWords[requestId],
            excludedIds: s_requestExcludedIds[requestId]
        });

        if (s_requestToken[requestId] == pixAssets && s_requestTokenId[requestId] == AVATAR_PACK_ID) {
            userAvatarSeed[s_requestUser[requestId]] = randomWords[0];
            userVRFRequestParams[s_requestUser[requestId]] = req;
            delete s_requestUser[requestId];
            delete s_requestTokenId[requestId];
            delete s_requestOpenings[requestId];
            delete s_requestRandWords[requestId];
            delete s_requestExcludedIds[requestId];
            return;
        }

        ContentCategory[] memory categories = s_tokenCategories[req.token][req.tokenId];
        uint256 expandedValue;
        for (uint256 j; j < req.openings; j++) {
            for (uint256 i; i < categories.length; i++) {
                bool categoryExcluded;
                for (uint256 z; z < req.excludedIds.length; z++) {
                    if (categories[i].id == req.excludedIds[z]) {
                        categoryExcluded = true;
                        break;
                    }
                }
                if (categoryExcluded) continue;
                expandedValue = uint256(keccak256(abi.encode(randomWords[0], i, j)));
                uint256 target = expandedValue % categories[i].contentAmountsTotalWeight;
                uint256 index = _binarySearch(categories[i].contentAmountsWeights, target);
                uint256 draws = categories[i].contentAmounts[index];
                for (uint256 k; k < draws; k++) {
                    expandedValue = uint256(keccak256(abi.encode(randomWords[0], i, j, k)));
                    uint256 targetContent = expandedValue % categories[i].contentsTotalWeight;
                    uint256 indexContent = _binarySearch(categories[i].tokenWeights, targetContent);
                    _mintReward(req.user, categories[i], indexContent);
                }
            }
        }

        delete s_requestUser[requestId];
        delete s_requestTokenId[requestId];
        delete s_requestOpenings[requestId];
        delete s_requestRandWords[requestId];
        delete s_requestExcludedIds[requestId];
    }

    /**
     * @notice Burns a given amount of Gravity Grade tokens
     * @param _tokenId The id of the token to burn
     * @param _amount Amount of the token to burn
     */
    function _burnTokens(address _token, uint256 _tokenId, uint256 _amount) internal {
        IAssetManager(_token).trustedBurn(msg.sender, _tokenId, _amount);
    }

    /**
     * @notice Mints appropriate rewards for the user
     * @param _user The address of the user
     * @param _category The category from which the reward should come
     * @param _index Index of the particular contents to mint
     */
    function _mintReward(address _user, ContentCategory memory _category, uint256 _index) internal {
        ITrustedMintable(_category.tokens[_index]).trustedMint(
            _user,
            _category.tokenIds[_index],
            _category.tokenAmounts[_index]
        );
        emit RewardGranted(_category.tokens[_index], _category.tokenIds[_index], _category.tokenAmounts[_index]);
    }

    /**
     * @notice Converts an array of weights into a cumulative array
     * @param _arr The array to convert
     * @return _cumulativeArr The resultant cumulative array
     */
    function _arrayToCumulative(uint256[] memory _arr) private pure returns (uint256[] memory _cumulativeArr) {
        _cumulativeArr = new uint256[](_arr.length);
        _cumulativeArr[0] = _arr[0];
        for (uint256 i = 1; i < _arr.length; i++) {
            _cumulativeArr[i] = _cumulativeArr[i - 1] + _arr[i];
        }
    }

    /**
     * @notice Runs a binary search on an array
     * @param _arr The array to search
     * @param _target The target value
     * @return _location Index of the result
     */
    function _binarySearch(uint256[] memory _arr, uint256 _target) private pure returns (uint256 _location) {
        uint256 left;
        uint256 mid;
        uint256 right = _arr.length;

        while (left < right) {
            mid = Math.average(left, right);

            if (_target < _arr[mid]) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }

        if (left > 0 && _arr[left - 1] == _target) {
            return left - 1;
        } else {
            return left;
        }
    }
}
