// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IGravityGradeBurn_CargoDrop3.sol";
import "../mission_control/IAssetManager.sol";
import "./IGravityGrade.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";

/// @title Implementation of IGravityGradeBurn for the 5th sept drop
contract GravityGradeBurn_CargoDrop3 is IGravityGradeBurn_CargoDrop3, OwnableUpgradeable, PausableUpgradeable {

    /**
    * @notice Event emitted on every pack burn
    * @dev This contract does NOT emit PackBurned from IGravityGrade burned!
    * @param _player The address of the opener
    * @param _tokenId The token id of the pack that was burned
    * @param _numPacks How many packs that were burned
    */
    event GG_Drop3_PackBurned(address _player, uint _tokenId, uint _numPacks);
    /**
    * @notice Event hardcoded to be emitted specifically when tokenId = 2 is burned
    * @dev Used to help the front end fellas
    * @param _player The address of the opener
    * @param _tokenIds Array of token ids. Always = [3,4,5,6,7]
    * @param _amounts Amounts of each token id in token ids. Entries can be = 0
    */
    event GG_Drop3_MysteryBox(address _player, uint[] _tokenIds, uint[] _amounts);

    IAssetManager assetManager;
    IGravityGrade gravityGrade;

    uint globalNonce;

    uint constant PROBA_RES = 10000;
    uint[5] probabilities;

    uint[] allPacks;

    /**
    * @notice Sets the probabilities for token ids 3 through 7 when opening token id 2
    * @param _nonCumProbabilities 5-long array of non cumulative probabilities in basis points
    * @dev Will revert on _nonCumProbabilities not summing to unity (i.e 10**4)
    */
    function setPackDistribution(uint[5] calldata _nonCumProbabilities) external onlyOwner {
        probabilities = _nonCumProbabilities;
        __validate_probas(_nonCumProbabilities);
    }

    /**
    * @notice Makes sure that the _probas sum to unity, otherwise it reverts
    * @param _probas Array of probabilities, always 5 long
    */
    function __validate_probas(uint[5] calldata _probas) private {
        uint sum;
        for (uint i; i < _probas.length; i++) {
            sum += _probas[i];
        }
        require(sum == PROBA_RES, "Probabilities do not sum to unity");
    }

    /**
    * @notice Calls _unpause() of pausable
    */
    function start() external onlyOwner {
        _unpause();
    }

    /**
    * @notice Calls _pause() of pausable
    */
    function stop() external onlyOwner {
        _pause();
    }


    /**
    * @notice Initializer for this contract
    * @param _probas Probabilities for the mystery packs, see .setPackDistribution()
    */
    function initialize(uint[5] calldata _probas) public initializer {
        __Ownable_init();
        probabilities = _probas;
        __validate_probas(_probas);
        _pause();
        allPacks.push(uint(IGravityGrade.GravityGradeDrops.AnniversaryPackOutlier));
        allPacks.push(uint(IGravityGrade.GravityGradeDrops.AnniversaryPackCommon));
        allPacks.push(uint(IGravityGrade.GravityGradeDrops.AnniversaryPackUncommon));
        allPacks.push(uint(IGravityGrade.GravityGradeDrops.AnniversaryPackRare));
        allPacks.push(uint(IGravityGrade.GravityGradeDrops.AnniversaryPackLegendary));
    }

    /// @inheritdoc IGravityGradeBurn_CargoDrop3
    function setGravityGrade(address _erc1155) external override onlyOwner {
        gravityGrade = IGravityGrade(_erc1155);
    }
    /// @inheritdoc IGravityGradeBurn_CargoDrop3
    function setAssetManager(address _assetManager) external override onlyOwner {
        assetManager = IAssetManager(_assetManager);
    }
    /// @inheritdoc IGravityGradeBurn
    function burnPack(uint256 _tokenId, uint _amount) external override whenNotPaused {
        gravityGrade.burn(msg.sender, _tokenId, _amount);
        emit GG_Drop3_PackBurned(msg.sender, _tokenId, _amount);
        if (_tokenId == uint(IGravityGrade.GravityGradeDrops.CargoDrop3)) {
            //GUH
            uint[] memory crateNshare = new uint[](2);
            crateNshare[0] = uint(IAssetManager.AssetIds.LootCrate);
            crateNshare[1] = uint(IAssetManager.AssetIds.LuckyCatShare);

            uint[] memory mysteryPack = new uint[](1);
            mysteryPack[0] = uint(IGravityGrade.GravityGradeDrops.AnniversaryPackMystery);

            uint[] memory amounts_2 = new uint[](2);
            amounts_2[0] = _amount;
            amounts_2[1] = _amount;

            uint[] memory amounts_1 = new uint[](1);
            amounts_1[0] = _amount;

            assetManager.trustedBatchMint(msg.sender, crateNshare, amounts_2);

            address[] memory singleSender = new address[](1);
            singleSender[0] = msg.sender;

            gravityGrade.airdrop(singleSender, mysteryPack, amounts_1);
            return;
        }
        if (_tokenId == uint(IGravityGrade.GravityGradeDrops.AnniversaryPackMystery)) {
            uint[] memory amounts = new uint[](5);
            for (uint i; i < _amount; i++) {
                uint newDraw = _getPackRarity(msg.sender, ++globalNonce);
                amounts[newDraw] += 1;
            }

            // This feels dirty
            address[] memory fivePlayerAddresses = new address[](5);
            for (uint k; k < 5; k++) {
                fivePlayerAddresses[k] = msg.sender;
            }

            gravityGrade.airdrop(fivePlayerAddresses, allPacks, amounts);
            emit GG_Drop3_MysteryBox(msg.sender, allPacks, amounts);
            return;
        }
        if (_tokenId >= uint(IGravityGrade.GravityGradeDrops.AnniversaryPackOutlier)
            && _tokenId <= uint(IGravityGrade.GravityGradeDrops.AnniversaryPackLegendary)) {

            // This is unbelievably dumb, there has to be some better way?
            uint[] memory assets = new uint[](2);
            assets[0] = uint(IAssetManager.AssetIds.TicketRegular);
            assets[1] = uint(IAssetManager.AssetIds.TicketPremium);
            uint[] memory asset_amt = new uint[](2);
            asset_amt[0] = _amount * uint(50);
            asset_amt[1] = _amount * uint(5);

            assetManager.trustedBatchMint(msg.sender, assets, asset_amt);
            return;
        }
        if (_tokenId == uint(IGravityGrade.GravityGradeDrops.StarterPack)) {
            assetManager.trustedMint(msg.sender, uint(IAssetManager.AssetIds.TicketRegular), _amount * 5);
            return;
        }
        revert GravityGradeBurn__InvalidTokenId(_tokenId);
    }

    /**
    * @notice Draws a random anniversary pack
    * @param _sender The user address, used as a seed
    * @param _nonce Nonce which is also used as a seed
    */
    function _getPackRarity(address _sender, uint _nonce) private view returns (uint){
        uint sample = _uniform(_sender, block.timestamp, _nonce);
        // Linear search is fine for this, only 5 elements to check
        uint sum;
        for (uint i; i < 5; i++) {
            sum += probabilities[i];
            if (sum >= sample) {
                return i;
            }
        }
        revert("Somehow failed to draw, should never happen");
    }

    /**
    * @notice Draws a uniform sample
    * @param _sender Address used as a seed
    * @param _blocktime Integer used as a seed
    * @param _nonce Integer used as a seed
    * @return uniformSample A uniform sample between 0 \leq x  < PROBA_RES = 10**4
    */
    function _uniform(address _sender, uint _blocktime, uint _nonce) private pure returns (uint uniformSample){
        uniformSample = uint(keccak256(abi.encode(_sender, _blocktime, _nonce))) % PROBA_RES;
    }
}
