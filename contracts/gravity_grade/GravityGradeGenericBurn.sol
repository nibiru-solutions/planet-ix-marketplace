// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IGravityGrade.sol";
import "./IGravityGradeGenericBurn.sol";
import "../util/IConditionalProvider.sol";
import "../interfaces/ITrustedMintable.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/IERC165Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165CheckerUpgradeable.sol";

///@notice Interface used for interacting with the VRF oracle
interface IVRFConsumerBaseV2 {
    /**
     * @notice Requests a random number from the vrf oracle
     * @param draws The number of draws requested
     * @return requestID the id of the random number request
     */
    function getRandomNumber(uint32 draws) external returns (uint256 requestID);
}

/**
 * @title Gravity Grade Generic Burn
 * @author Jourdan
 * @notice This contract manages burning and custom rewards for any Gravity Grade token
 */
contract GravityGradeGenericBurn is IGravityGradeGenericBurn, OwnableUpgradeable {
    /*------------------- STATE VARIABLES -------------------*/

    /// @notice All category ids belonging to a tokenId, (tokenId => categoryIds)
    mapping(uint256 => uint256[]) private s_tokenCategoryIds;
    /// @notice Total categories belonging to a tokenId, (tokenId => total categories)
    mapping(uint256 => uint256) private s_tokenTotalCategories;
    /// @notice All categories belonging to a tokenId, (tokenId => ContentCategory[])
    mapping(uint256 => ContentCategory[]) private s_tokenCategories;
    /// @notice Shows whether a category is active, (tokenId => categoryId => bool)
    mapping(uint256 => mapping(uint256 => bool)) private s_tokenCategoryActive;
    /// @notice Shows the index where a category can be found, (tokenId => categoryId => index)
    mapping(uint256 => mapping(uint256 => uint256)) private s_tokenCategoryIndex;
    /// @notice Contract with eligibility requirements for category, (tokenId => categoryId => eligibility)
    mapping(uint256 => mapping(uint256 => IConditionalProvider)) public s_tokenEligibility;
    /// @notice Max number of draws any category can have for a particular token (tokenId => maxDraws)
    mapping(uint256 => uint32) public maxDrawsPerCategory;

    /// @notice Oracle being used for randomness
    IVRFConsumerBaseV2 public s_randNumOracle;

    /// @notice TokenId for a VRF request
    mapping(uint256 => uint256) s_requestTokenId;
    /// @notice User for a VRF request
    mapping(uint256 => address) private s_requestUser;
    /// @notice Ids of categories a user didn't qualify for on a VRF request
    mapping(uint256 => uint256[]) private s_requestExcludedIds;
    /// @notice Number of openings for a VRF request
    mapping(uint256 => uint256) private s_requestOpenings;
    /// @notice Number of random words needed
    mapping(uint256 => uint256) private s_requestRandWords;

    /// @notice TokenIds which are whitelisted for burning
    mapping(uint256 => bool) private s_whitelistedTokens;
    /// @notice Gravity Grade Contract
    address public gravityGrade;
    /// @notice Governance
    address public s_governance;

    mapping(uint256 => uint256) public tokenBurnLimit; // tokenId => max amount burnable

    mapping(uint256 => GuaranteedReward[]) public s_guaranteedRewards; // tokenId => guaranteed rewards

    IVRFConsumerBaseV2 public s_randNumOracleV2;
    /*------------------- MODIFIERS -------------------*/

    /// @notice Makes sure function is called only by governance
    modifier onlyGov() {
        if (msg.sender != owner() && msg.sender != s_governance) revert GB__NotGov(msg.sender);
        _;
    }

    /// @notice Makes sure function is called only by vrf oracle
    modifier onlyOracle() {
        if (msg.sender != address(s_randNumOracleV2)) revert GB__NotOracle();
        _;
    }

    /*------------------- INITIALIZER -------------------*/

    ///@notice Initializer
    function initialize() public initializer {
        __Ownable_init();
    }

    /*------------------- ADMIN - ONLY FUNCTIONS -------------------*/

    /// @inheritdoc IGenericBurn
    function whitelistToken(uint256 _tokenId, bool _isWhitelisted) external onlyGov {
        s_whitelistedTokens[_tokenId] = _isWhitelisted;
        emit TokenWhitelisted(_tokenId, _isWhitelisted);
    }

    /**
     * @notice Sets VRF consumer to be used
     * @param _vrfOracle The address of the oracle
     */
    function setVRFOracle(address _vrfOracle) external onlyOwner {
        if (_vrfOracle == address(0)) revert GB__ZeroAddress();
        s_randNumOracleV2 = IVRFConsumerBaseV2(_vrfOracle);
    }

    /// @inheritdoc IGravityGradeGenericBurn
    function setGravityGrade(address _gravityGrade) external onlyGov {
        gravityGrade = _gravityGrade;
        emit GravityGradeSet(_gravityGrade);
    }

    /**
     * @notice Sets governace
     * @param _governance The address of the oracle
     */
    function setGovernance(address _governance) external onlyGov {
        s_governance = _governance;
    }

    function setTokenBurnLimit(uint256 _tokenId, uint256 _limit) external onlyGov {
        tokenBurnLimit[_tokenId] = _limit;
    }

    /// @inheritdoc IGenericBurn
    function setContentEligibility(
        uint256 _tokenId,
        uint256 _categoryId,
        address _conditionalProvider
    ) external onlyGov {
        if (
            !ERC165CheckerUpgradeable.supportsInterface(_conditionalProvider, type(IERC165Upgradeable).interfaceId) ||
            !ERC165CheckerUpgradeable.supportsInterface(_conditionalProvider, type(IConditionalProvider).interfaceId)
        ) {
            revert GB__NotConditionalProvider(_conditionalProvider);
        }
        if (!s_tokenCategoryActive[_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);
        s_tokenEligibility[_tokenId][_categoryId] = IConditionalProvider(_conditionalProvider);
        emit CategoryEligibilitySet(_tokenId, _categoryId, _conditionalProvider);
    }

    /// @inheritdoc IGenericBurn
    function createContentCategory(uint256 _tokenId) external onlyGov returns (uint256 _categoryId) {
        unchecked {
            _categoryId = ++s_tokenTotalCategories[_tokenId];
        }

        s_tokenCategories[_tokenId].push(
            ContentCategory({
                id: _categoryId,
                contentAmountsTotalWeight: 0,
                contentsTotalWeight: 0,
                contentAmounts: new uint256[](0),
                contentAmountsWeights: new uint256[](0),
                tokenAmounts: new uint256[](0),
                tokenWeights: new uint256[](0),
                tokens: new address[](0),
                tokenIds: new uint256[](0)
            })
        );
        s_tokenCategoryIds[_tokenId].push(_categoryId);
        s_tokenCategoryIndex[_tokenId][_categoryId] = s_tokenCategories[_tokenId].length - 1;
        s_tokenCategoryActive[_tokenId][_categoryId] = true;

        emit CategoryCreated(_tokenId, _categoryId);
    }

    /// @inheritdoc IGenericBurn
    function deleteContentCategory(uint256 _tokenId, uint256 _categoryId) external onlyGov {
        if (!s_tokenCategoryActive[_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);

        uint256 index = s_tokenCategoryIndex[_tokenId][_categoryId];
        for (uint256 i = index; i < s_tokenCategories[_tokenId].length - 1; ) {
            s_tokenCategories[_tokenId][i] = s_tokenCategories[_tokenId][i + 1];
            s_tokenCategoryIds[_tokenId][i] = s_tokenCategoryIds[_tokenId][i + 1];
            s_tokenCategoryIndex[_tokenId][s_tokenCategories[_tokenId][i].id] = i;

            unchecked {
                ++i;
            }
        }
        s_tokenCategoryActive[_tokenId][_categoryId] = false;
        s_tokenCategoryIds[_tokenId].pop();
        s_tokenCategories[_tokenId].pop();

        emit CategoryDeleted(_tokenId, _categoryId);
    }

    /// @inheritdoc IGenericBurn
    function setContentAmounts(
        uint256 _tokenId,
        uint256 _categoryId,
        uint256[] calldata _amounts,
        uint256[] calldata _weights
    ) external onlyGov {
        if (!s_tokenCategoryActive[_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);
        if (_amounts.length != _weights.length) revert GB__ArraysNotSameLength();

        uint256 sum;
        for (uint256 i = 0; i < _weights.length; i++) {
            if (_weights[i] == 0) revert GB__ZeroWeight();
            if (_amounts[i] > maxDrawsPerCategory[_tokenId]) revert GB__MaxDrawsExceeded(_amounts[i]);
            sum += _weights[i];
        }

        uint256 index = s_tokenCategoryIndex[_tokenId][_categoryId];

        s_tokenCategories[_tokenId][index].contentAmounts = _amounts;
        s_tokenCategories[_tokenId][index].contentAmountsWeights = _arrayToCumulative(_weights);
        s_tokenCategories[_tokenId][index].contentAmountsTotalWeight = sum;

        emit ContentAmountsUpdated(_tokenId, _categoryId, _amounts, _weights);
    }

    /// @inheritdoc IGenericBurn
    function setContents(
        uint256 _tokenId,
        uint256 _categoryId,
        address[] calldata _tokens,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts,
        uint256[] calldata _weights
    ) external onlyGov {
        if (!s_tokenCategoryActive[_tokenId][_categoryId]) revert GB__InvalidCategoryId(_categoryId);
        if (
            _amounts.length != _weights.length ||
            _amounts.length != _tokens.length ||
            _amounts.length != _tokenIds.length
        ) revert GB__ArraysNotSameLength();

        uint256 sum;
        for (uint256 i = 0; i < _weights.length; i++) {
            if (_weights[i] == 0) revert GB__ZeroWeight();
            if (_amounts[i] == 0) revert GB_ZeroAmount();

            sum += _weights[i];
        }

        uint256 index = s_tokenCategoryIndex[_tokenId][_categoryId];

        s_tokenCategories[_tokenId][index].tokenAmounts = _amounts;
        s_tokenCategories[_tokenId][index].tokenWeights = _arrayToCumulative(_weights);
        s_tokenCategories[_tokenId][index].contentsTotalWeight = sum;
        s_tokenCategories[_tokenId][index].tokens = _tokens;
        s_tokenCategories[_tokenId][index].tokenIds = _tokenIds;

        emit ContentsUpdated(_tokenId, _categoryId, _tokens, _tokenIds, _amounts, _weights);
    }

    /**
     * @notice Function for setting the maximum # of draws any category can have
     * @param _maxDraws The max number of draws any category can have
     */
    function setMaxDraws(uint256 _tokenId, uint32 _maxDraws) external onlyGov {
        maxDrawsPerCategory[_tokenId] = _maxDraws;
    }

    function setGuaranteedRewards(uint256 _tokenId, GuaranteedReward[] memory _rewards) external onlyGov {
        for (uint256 i; i < _rewards.length; i++) {
            s_guaranteedRewards[_tokenId].push(_rewards[i]);
        }
        emit GuaranteedRewardSet(_tokenId, _rewards);
    }

    function deleteGuaranteedRewards(uint256 _tokenId, uint256 _position) external onlyGov {
        uint256 last = s_guaranteedRewards[_tokenId].length - 1;
        s_guaranteedRewards[_tokenId][_position] = s_guaranteedRewards[_tokenId][last];
        s_guaranteedRewards[_tokenId].pop();
    }

    /*------------------- END - USER FUNCTIONS -------------------*/

    /// @inheritdoc IGenericBurn
    function burnPack(uint256 _tokenId, uint32 _amount, bool _optInConditionals) external {
        require(_amount <= tokenBurnLimit[_tokenId], "GG BURN: BURN_LIMIT_EXCEEDED");
        if (!s_whitelistedTokens[_tokenId]) revert GB__TokenNotWhitelisted(_tokenId);

        _burnTokens(_tokenId, _amount);

        if (s_tokenCategories[_tokenId].length != 0) {
            uint256[] memory tokenCategoryIds = s_tokenCategoryIds[_tokenId];
            uint256[] memory excludedIds = new uint256[](tokenCategoryIds.length);
            uint256 numRequests;

            for (uint256 i; i < tokenCategoryIds.length; i++) {
                if (address(s_tokenEligibility[_tokenId][tokenCategoryIds[i]]) != address(0)) {
                    IConditionalProvider conditionalProvider = s_tokenEligibility[_tokenId][tokenCategoryIds[i]];
                    if (!_optInConditionals || !conditionalProvider.isEligible(msg.sender)) {
                        excludedIds[numRequests] = tokenCategoryIds[i];
                        unchecked {
                            ++numRequests;
                        }
                    }
                }
            }
            uint32 randWordsRequest = _amount > maxDrawsPerCategory[_tokenId] ? _amount : maxDrawsPerCategory[_tokenId];
            uint256 requestId = s_randNumOracleV2.getRandomNumber(1);
            s_requestUser[requestId] = msg.sender;
            s_requestTokenId[requestId] = _tokenId;
            s_requestOpenings[requestId] = _amount;
            s_requestRandWords[requestId] = randWordsRequest;

            for (uint256 i; i < excludedIds.length; i++) {
                if (excludedIds[i] != 0) {
                    s_requestExcludedIds[requestId].push(excludedIds[i]);
                }
            }
        }

        GuaranteedReward[] memory rewards = s_guaranteedRewards[_tokenId];
        for (uint256 i; i < rewards.length; i++) {
            ITrustedMintable(rewards[i].token).trustedMint(msg.sender, rewards[i].tokenId, rewards[i].tokenAmount);
            emit RewardGranted(rewards[i].token, rewards[i].tokenId, rewards[i].tokenAmount);
        }

        emit PackOpened(msg.sender, _tokenId, _amount);
    }

    /// @inheritdoc IGenericBurn
    function getContentCategories(uint256 _tokenId) external view returns (ContentCategory[] memory _categories) {
        _categories = s_tokenCategories[_tokenId];
    }

    function getGuaranteedRewards(uint256 _tokenId) external view returns (GuaranteedReward[] memory _rewards) {
        _rewards = s_guaranteedRewards[_tokenId];
    }

    /*------------------- INTERNAL FUNCTIONS -------------------*/

    /**
     * @notice Function for satisfying randomness requests from burnPack
     * @param requestId The particular request being serviced
     * @param randomWords Array of the random numbers requested
     */
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external onlyOracle {
        RequestInputs memory req = RequestInputs({
            user: s_requestUser[requestId],
            tokenId: s_requestTokenId[requestId],
            openings: s_requestOpenings[requestId],
            randWordsCount: s_requestRandWords[requestId],
            excludedIds: s_requestExcludedIds[requestId]
        });

        ContentCategory[] memory categories = s_tokenCategories[req.tokenId];
        uint256 expandedValue;
        for (uint256 j; j < req.openings; j++) {
            for (uint256 i; i < categories.length; i++) {
                bool categoryExcluded;
                for (uint256 z; z < req.excludedIds.length; z++) {
                    if (categories[i].id == req.excludedIds[z]) {
                        categoryExcluded = true;
                        break;
                    }
                }
                if (categoryExcluded) continue;
                expandedValue = uint256(keccak256(abi.encode(randomWords[0], i, j)));
                uint256 target = expandedValue % categories[i].contentAmountsTotalWeight;
                uint256 index = _binarySearch(categories[i].contentAmountsWeights, target);
                uint256 draws = categories[i].contentAmounts[index];
                for (uint256 k; k < draws; k++) {
                    expandedValue = uint256(keccak256(abi.encode(randomWords[0], i, j, k)));
                    uint256 targetContent = expandedValue % categories[i].contentsTotalWeight;
                    uint256 indexContent = _binarySearch(categories[i].tokenWeights, targetContent);
                    _mintReward(req.user, categories[i], indexContent);
                }
            }
        }

        delete s_requestUser[requestId];
        delete s_requestTokenId[requestId];
        delete s_requestOpenings[requestId];
        delete s_requestRandWords[requestId];
        delete s_requestExcludedIds[requestId];
    }

    /**
     * @notice Burns a given amount of Gravity Grade tokens
     * @param _tokenId The id of the token to burn
     * @param _amount Amount of the token to burn
     */
    function _burnTokens(uint256 _tokenId, uint256 _amount) internal {
        IGravityGrade(gravityGrade).burn(msg.sender, _tokenId, _amount);
    }

    /**
     * @notice Mints appropriate rewards for the user
     * @param _user The address of the user
     * @param _category The category from which the reward should come
     * @param _index Index of the particular contents to mint
     */
    function _mintReward(address _user, ContentCategory memory _category, uint256 _index) internal {
        ITrustedMintable(_category.tokens[_index]).trustedMint(
            _user,
            _category.tokenIds[_index],
            _category.tokenAmounts[_index]
        );
        emit RewardGranted(_category.tokens[_index], _category.tokenIds[_index], _category.tokenAmounts[_index]);
    }

    /**
     * @notice Converts an array of weights into a cumulative array
     * @param _arr The array to convert
     * @return _cumulativeArr The resultant cumulative array
     */
    function _arrayToCumulative(uint256[] memory _arr) private pure returns (uint256[] memory _cumulativeArr) {
        _cumulativeArr = new uint256[](_arr.length);
        _cumulativeArr[0] = _arr[0];
        for (uint256 i = 1; i < _arr.length; i++) {
            _cumulativeArr[i] = _cumulativeArr[i - 1] + _arr[i];
        }
    }

    /**
     * @notice Runs a binary search on an array
     * @param _arr The array to search
     * @param _target The target value
     * @return _location Index of the result
     */
    function _binarySearch(uint256[] memory _arr, uint256 _target) private pure returns (uint256 _location) {
        uint256 left;
        uint256 mid;
        uint256 right = _arr.length;

        while (left < right) {
            mid = Math.average(left, right);

            if (_target < _arr[mid]) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }

        if (left > 0 && _arr[left - 1] == _target) {
            return left - 1;
        } else {
            return left;
        }
    }
}
