// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IGravityGradeBurn.sol";

/// @title Extension of IGravityGrade for the contracts launched 5th Sep 2022
interface IGravityGradeBurn_CargoDrop3 is IGravityGradeBurn {
    /**
    * @notice Sets the gravity grade address
    * @param _gravityGrade The address to gravity grade
    */
    function setGravityGrade(address _gravityGrade) external;
    /**
    * @notice Sets the asset manager
    * @param _assetManager The address to the asset manager
    */
    function setAssetManager(address _assetManager) external;
}
