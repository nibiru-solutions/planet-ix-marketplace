// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.8;

import "../interfaces/ITrustedMintable.sol";
import "./IGravityGradeMintWrapper.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "../mission_control/IAssetManager.sol";

contract GravityGradeMintWrapper is OwnableUpgradeable, ITrustedMintable, IAssetManager, IGravityGradeMintWrapper {
    IGravityGrade gravityGrade;
    mapping(address => bool) private s_trustedAddresses;

    //Errors
    error GravityGradeMintWrapper__GravityGradeNotSet();
    error GravityGradeMintWrapper__NotTrusted(address _address);
    error GravityGradeMintWrapper__TokenIdsAndAmountsNotSameLength();

    //Events
    event GravityGradeMintWrapper__GravityGradeSet(address _address);

    function initialize() public initializer {
        __Ownable_init();
    }

    function setGravityGradeContract(address _gravityGradeContract) external onlyOwner {
        gravityGrade = IGravityGrade(_gravityGradeContract);
        emit GravityGradeMintWrapper__GravityGradeSet(_gravityGradeContract);
    }

    function trustedMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) external override(IAssetManager, ITrustedMintable) onlyTrusted ggIsSet {
        address[] memory to = new address[](1);
        uint[] memory amount = new uint[](1);
        uint[] memory tokenId = new uint[](1);
        to[0] = _to;
        amount[0] = _amount;
        tokenId[0] = _tokenId;
        IGravityGrade(gravityGrade).airdrop(to, tokenId, amount);
    }

    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override(IAssetManager, ITrustedMintable) onlyTrusted ggIsSet {
        if (_tokenIds.length != _amounts.length) revert GravityGradeMintWrapper__TokenIdsAndAmountsNotSameLength();
        address[] memory to = new address[](_tokenIds.length);
        for (uint i = 0; i < _tokenIds.length; i++) {
            to[i] = _to;
        }
        IGravityGrade(gravityGrade).airdrop(to, _tokenIds, _amounts);
    }

    /// @inheritdoc IAssetManager
    function trustedBurn(address _from, uint256 _tokenId, uint256 _amount) external override onlyTrusted {
        IGravityGrade(gravityGrade).burn(_from, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchBurn(
        address _from,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override onlyTrusted {}

    function setTrusted(address _trusted, bool _isTrusted) external onlyOwner {
        s_trustedAddresses[_trusted] = _isTrusted;
    }

    function isTrusted(address _trusted) external view returns (bool) {
        return s_trustedAddresses[_trusted];
    }

    //Modifiers
    modifier onlyTrusted() {
        if (!s_trustedAddresses[msg.sender]) revert GravityGradeMintWrapper__NotTrusted(msg.sender);
        _;
    }

    modifier ggIsSet() {
        if (address(gravityGrade) == address(0)) revert GravityGradeMintWrapper__GravityGradeNotSet();
        _;
    }
}
