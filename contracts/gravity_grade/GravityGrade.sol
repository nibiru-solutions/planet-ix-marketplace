// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.8;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/IERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165CheckerUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./IGravityGrade.sol";
import "../interfaces/ISwapManager.sol";
import "../interfaces/IOracleManager.sol";
import "./IGravityGrade_Paper.sol";

import "hardhat/console.sol";

/* error codes */
error GravityGrade__NotGovernance();
error GravityGrade__NotTrusted(address _address);
error GravityGrade__ZeroAddress();
error GravityGrade__PaperCurrencyTokenAddressNotSet();
error GravityGrade__ZeroSaleCap();
error GravityGrade__ZeroUserSaleCap();
error GravityGrade__TokenNotSet(uint256 _tokenId);
error GravityGrade__NonExistentSale(uint256 _tokenId);
error GravityGrade__SaleInactive();
error GravityGrade__ValueTooLarge(uint256 _given, uint256 _max);
error GravityGrade__CurrencyNotWhitelisted(address _address);
error GravityGrade__PurchaseExceedsTotalMax(uint256 _current, uint256 _max);
error GravityGrade__PurchaseExceedsPlayerMax(uint256 _current, uint256 _max);
error GravityGrade__InvalidSaleParameters();
error GravityGrade__WithdrawalFailed();
error GravityGrade__TransferFailed();
error GravityGrade__InsufficientEthValue(uint256 _value, uint256 _required);
error GravityGrade__SenderDoesNotOwnToken(address _token, uint256 _id);
error GravityGrade__TokenNotEligibleForRebate(address _token);
error GravityGrade__RefundFailed();
error GravityGrade__RebateTooLarge(uint256 _saleId, uint256 _price, uint256 _bulkDsc, uint256 _ownershipDsc);

/**
 * @title Gravity Grade
 * @author @Haltoshi
 * @notice This contract is for managing cargo and pack drops
 */
contract GravityGrade is IGravityGrade, IGravityGrade_Paper, OwnableUpgradeable, ERC1155Upgradeable {
    /* Type Declarations */
    struct Sale {
        uint256 saleId;
        uint256 tokenId;
        uint256 salePrice;
        uint256 totalSupply;
        uint256 userCap;
        address defaultCurrency;
        bool profitState;
    }

    struct Beneficiaries {
        uint256[] feeBps;
        address[] beneficiary;
    }

    /* State Variables */
    string public name;
    string public symbol;
    address private s_moderator;
    uint256 private s_saleId;
    Beneficiaries private s_beneficiaries;
    mapping(uint256 => uint256) s_sold;
    mapping(uint256 => string) private s_uris;
    mapping(uint256 => Sale) private s_sales;
    mapping(uint256 => bool) private s_saleStatus;
    mapping(address => bool) private s_trustedAddresses;
    mapping(uint256 => uint256[]) private s_bulkDiscountBreakpoints;
    mapping(uint256 => uint256[]) private s_bulkDiscountBasisPoints;
    mapping(uint256 => mapping(address => bool)) s_whitelistedCurrencies;
    mapping(uint256 => mapping(address => uint256)) s_perPlayerSold;
    mapping(uint256 => OwnershipRebate[]) private s_ownershipDiscounts;
    uint256 public constant MAXIMUM_BASIS_POINTS = 10_000;
    address public constant ADDRESS_ZERO = address(0);

    IOracleManager s_oracle;
    ISwapManager s_swapManager;

    address private s_paperCurrency;

    /* Modifiers */
    modifier onlyGov() {
        if (msg.sender != owner() && msg.sender != s_moderator) {
            revert GravityGrade__NotGovernance();
        }
        _;
    }

    modifier onlyTrusted() {
        if (!s_trustedAddresses[msg.sender]) revert GravityGrade__NotTrusted(msg.sender);
        _;
    }

    /* Functions */
    /**
     * @notice Initializer for the GravityGrade contract
     * @param _name Name for this token
     * @param _symbol Symbol for this token
     */
    function initialize(string memory _name, string memory _symbol) public initializer {
        __ERC1155_init("");
        __Ownable_init();
        name = _name;
        symbol = _symbol;
        s_moderator = msg.sender;
    }

    /**
     * @notice Assigns the moderator
     * @param _moderatorAddress The moderator-to-be
     */
    function setModerator(address _moderatorAddress) external onlyOwner {
        if (_moderatorAddress == ADDRESS_ZERO) revert GravityGrade__ZeroAddress();
        s_moderator = _moderatorAddress;
    }

    /// @inheritdoc IGravityGrade
    function createNewSale(
        uint256 _tokenId,
        uint256 _salePrice,
        uint256 _totalSupplyAmountToSell,
        uint256 _userCap,
        address _defaultCurrency,
        bool _profitState
    ) external onlyGov returns (uint256 saleId) {
        if (_totalSupplyAmountToSell == 0) revert GravityGrade__ZeroSaleCap();
        if (_userCap == 0) revert GravityGrade__ZeroUserSaleCap();
        if (bytes(s_uris[_tokenId]).length == 0) revert GravityGrade__TokenNotSet(_tokenId);

        uint256 newSaleId = s_saleId++;
        s_sales[newSaleId] = Sale(newSaleId, _tokenId, _salePrice, _totalSupplyAmountToSell, _userCap, _defaultCurrency, _profitState);
        s_whitelistedCurrencies[newSaleId][_defaultCurrency] = true;
        emit SaleInfoUpdated(newSaleId, _tokenId, _salePrice, _totalSupplyAmountToSell, _userCap, _defaultCurrency);
        return newSaleId;
    }

    /// @inheritdoc IGravityGrade
    function modifySale(
        uint256 _saleId,
        uint256 _salePrice,
        uint256 _totalSupplyAmountToSell,
        uint256 _userCap,
        address _defaultCurrency,
        bool _profitState
    ) external onlyGov {
        if (s_sales[_saleId].tokenId == 0) revert GravityGrade__NonExistentSale(_saleId);
        if (_totalSupplyAmountToSell == 0) revert GravityGrade__ZeroSaleCap();
        if (_userCap == 0) revert GravityGrade__ZeroUserSaleCap();
        if (bytes(s_uris[s_sales[_saleId].tokenId]).length == 0) revert GravityGrade__TokenNotSet(s_sales[_saleId].tokenId);

        s_sales[_saleId].salePrice = _salePrice;
        s_sales[_saleId].totalSupply = _totalSupplyAmountToSell;
        s_sales[_saleId].userCap = _userCap;
        s_sales[_saleId].defaultCurrency = _defaultCurrency;
        s_sales[_saleId].profitState = _profitState;
        emit SaleInfoUpdated(_saleId, s_sales[_saleId].tokenId, _salePrice, _totalSupplyAmountToSell, _userCap, _defaultCurrency);
    }

    /// @inheritdoc IGravityGrade
    function setTokenUri(uint256 _tokenId, string memory _uri) external onlyGov {
        s_uris[_tokenId] = _uri;
    }

    /** @notice retreive TokenURI
     *  @param _tokenId TokenId
     */
    function uri(uint256 _tokenId) public view override returns (string memory) {
        return s_uris[_tokenId];
    }

    /// @inheritdoc IGravityGrade
    function addBulkDiscount(
        uint256 _saleId,
        uint256 _breakpoint,
        uint256 _basisPoints
    ) external onlyGov {
        // todo: Delete entries when we set _basisPoints = 0
        if (s_sales[_saleId].tokenId == 0) revert GravityGrade__NonExistentSale(_saleId);
        if (s_sales[_saleId].userCap < _breakpoint) revert GravityGrade__ValueTooLarge(_breakpoint, s_sales[_saleId].userCap);
        uint256[] storage s_discount = s_bulkDiscountBasisPoints[_saleId];
        uint256[] storage s_break = s_bulkDiscountBreakpoints[_saleId];
        bool overwritten;
        for (uint256 i; i < s_discount.length; i++) {
            if (s_break[i] == _breakpoint) {
                s_discount[i] = _basisPoints;
                overwritten = true;
            }
        }
        if (!overwritten) {
            s_discount.push(_basisPoints);
            s_break.push(_breakpoint);
        }
        _validateRebates(_saleId);
    }

    /// @inheritdoc IGravityGrade
    function addOwnershipDiscount(uint256 _saleId, OwnershipRebate calldata _info) external onlyGov {
        if (_info.tokenAddress == ADDRESS_ZERO) revert GravityGrade__ZeroAddress();
        if (_info.basisPoints > MAXIMUM_BASIS_POINTS) revert GravityGrade__InvalidSaleParameters();

        if (uint256(_info.tokenType) == 0) {
            if (!ERC165CheckerUpgradeable.supportsInterface(_info.tokenAddress, type(IERC721Upgradeable).interfaceId)) revert GravityGrade__InvalidSaleParameters();
        }
        if (uint256(_info.tokenType) == 1) {
            if (!ERC165CheckerUpgradeable.supportsInterface(_info.tokenAddress, type(IERC1155Upgradeable).interfaceId)) revert GravityGrade__InvalidSaleParameters();
        }

        OwnershipRebate[] memory rebates = s_ownershipDiscounts[_saleId];
        bool foundRebate;
        for (uint256 i; i < rebates.length; ++i) {
            if (rebates[i].tokenAddress == _info.tokenAddress && rebates[i].tokenId == _info.tokenId) {
                foundRebate = true;
                s_ownershipDiscounts[_saleId][i].basisPoints = _info.basisPoints;
            }
        }
        if (!foundRebate) s_ownershipDiscounts[_saleId].push(_info);
        _validateRebates(_saleId);
    }

    /// @inheritdoc IGravityGrade
    function deleteSale(uint256 _saleId) external onlyGov {
        if (s_sales[_saleId].tokenId == 0) revert GravityGrade__NonExistentSale(_saleId);
        delete s_sales[_saleId];
        if (s_ownershipDiscounts[_saleId].length > 0) {
            delete s_ownershipDiscounts[_saleId];
        }
        emit SaleDeleted(_saleId);
    }

    /// @inheritdoc IGravityGrade
    function setAllowedPaymentCurrencies(uint256 _saleId, address[] calldata _currencyAddresses) external onlyGov {
        for (uint256 i; i < _currencyAddresses.length; i++) {
            s_whitelistedCurrencies[_saleId][_currencyAddresses[i]] = true;
        }
        emit PaymentCurrenciesSet(_saleId, _currencyAddresses);
    }

    /**
     * @notice remove payment currencies on a per saleId basis
     * @param _saleId the sale ID to set revoked payment currencies
     * @param _currencyAddresses the addresses of revoked payment currencies
     */
    function removePaymentCurrencies(uint256 _saleId, address[] calldata _currencyAddresses) external onlyGov {
        for (uint256 i; i < _currencyAddresses.length; ++i) {
            if (s_whitelistedCurrencies[_saleId][_currencyAddresses[i]]) {
                delete s_whitelistedCurrencies[_saleId][_currencyAddresses[i]];
            }
        }
        emit PaymentCurrenciesRevoked(_saleId, _currencyAddresses);
    }

    /// @inheritdoc IGravityGrade
    function setSwapManager(address _swapManager) external onlyGov {
        s_swapManager = ISwapManager(_swapManager);
    }

    /// @inheritdoc IGravityGrade
    function setOracleManager(address _oracleManager) external onlyGov {
        s_oracle = IOracleManager(_oracleManager);
    }

    /// @inheritdoc IGravityGrade
    function setTrusted(address _trusted, bool _isTrusted) external onlyGov {
        s_trustedAddresses[_trusted] = _isTrusted;
    }

    /** @notice retreive Trusted address status
     */
    function getTrusted(address _trusted) public view returns (bool) {
        return s_trustedAddresses[_trusted];
    }

    /// @inheritdoc IGravityGrade
    function withdraw(address _walletAddress, address _currency) external payable onlyGov {
        if (_currency == address(0)) {
            (bool callSuccess, ) = payable(_walletAddress).call{value: address(this).balance}("");
            if (!callSuccess) revert GravityGrade__WithdrawalFailed();
        } else {
            uint256 amount = IERC20(_currency).balanceOf(address(this));
            IERC20(_currency).approve(address(this), amount);
            if (!IERC20(_currency).transferFrom(address(this), _walletAddress, amount)) revert GravityGrade__WithdrawalFailed();
        }
    }

    /// @inheritdoc IGravityGrade
    function setFeeWalletsAndPercentages(address[] calldata _walletAddresses, uint256[] calldata _feeBps) external onlyOwner {
        uint256 sum;
        for (uint256 i; i < _feeBps.length; ++i) {
            sum += _feeBps[i];
        }
        if (sum > 10000) revert GravityGrade__ValueTooLarge(sum, 10000);
        s_beneficiaries = Beneficiaries(_feeBps, _walletAddresses);
        emit BeneficiariesUpdated(_walletAddresses, _feeBps);
    }

    /**
     * @notice Used to validate the rebates for a sale to make sure that they don't exceed 100%
     * @param _saleId The id of the sale
     * @dev This is a pessimistic validation as it makes sure that the price > 0 when all rebates are applied to a single unit
     */
    function _validateRebates(uint256 _saleId) internal {
        // Not very gas efficient but prevents any and all 100% rebate scenarios
        uint256[] memory bulks = s_bulkDiscountBasisPoints[_saleId];
        uint256 sum;
        for (uint256 i; i < bulks.length; i++) {
            sum += bulks[i];
        }
        OwnershipRebate[] memory ownershipRebates = s_ownershipDiscounts[_saleId];
        uint256 best;
        OwnershipRebate memory current;
        for (uint256 j; j < ownershipRebates.length; j++) {
            current = ownershipRebates[j];
            if (current.basisPoints > best) {
                best = current.basisPoints;
            }
        }
        if (sum >= MAXIMUM_BASIS_POINTS) revert GravityGrade__ValueTooLarge(sum, MAXIMUM_BASIS_POINTS);
        if (best >= MAXIMUM_BASIS_POINTS) revert GravityGrade__ValueTooLarge(best, MAXIMUM_BASIS_POINTS);
        /*
        This is a pessimistic check since
            (((info.salePrice * (MAXIMUM_BASIS_POINTS - sum)) / MAXIMUM_BASIS_POINTS)
             * (MAXIMUM_BASIS_POINTS - best)) / MAXIMUM_BASIS_POINTS == 0
        Does not necessarily imply that
            (((SOME_BULK_AMOUNT* info.salePrice * (MAXIMUM_BASIS_POINTS - sum)) / MAXIMUM_BASIS_POINTS)
             * (MAXIMUM_BASIS_POINTS - best)) / MAXIMUM_BASIS_POINTS == 0
        */
        Sale memory info = s_sales[_saleId];
        uint256 postRebatePrice = (((info.salePrice * (MAXIMUM_BASIS_POINTS - sum)) / MAXIMUM_BASIS_POINTS) * (MAXIMUM_BASIS_POINTS - best)) / MAXIMUM_BASIS_POINTS;
        if (postRebatePrice == 0) revert GravityGrade__RebateTooLarge(_saleId, info.salePrice, sum, best);
    }

    // @inheritdoc IGravityGrade_Paper
    function checkPrice(
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _numPurchases,
        uint256 _saleId
    ) external returns (uint256 price) {
        if (s_paperCurrency == ADDRESS_ZERO) revert GravityGrade__PaperCurrencyTokenAddressNotSet();
        address currency = s_paperCurrency;
        Sale memory info = s_sales[_saleId];
        uint256 salePrice = info.salePrice * _numPurchases;
        uint256 rebatePrice;
        if (_tokenAddress != ADDRESS_ZERO) {
            OwnershipRebate[] memory ownershipRebates = s_ownershipDiscounts[_saleId];
            uint256 rebateAmount;
            for (uint256 i; i < ownershipRebates.length; ++i) {
                if (ownershipRebates[i].tokenAddress == _tokenAddress && (ownershipRebates[i].tokenType == TokenType.ERC721 || _tokenId == ownershipRebates[i].tokenId)) {
                    rebateAmount = ownershipRebates[i].basisPoints;
                }
            }
            if (rebateAmount > 0) {
                rebatePrice = calculateDiscountedPrice(rebateAmount, salePrice);
            } else {
                revert GravityGrade__TokenNotEligibleForRebate(_tokenAddress);
            }
        }
        salePrice = rebatePrice > 0 ? rebatePrice : salePrice;
        if (currency != info.defaultCurrency) {
            salePrice = s_oracle.getAmountOut(info.defaultCurrency, currency, salePrice);
        }
        price = salePrice;
    }

    // @inheritdoc IGravityGrade_Paper
    function packClaimable(
        address _buyer,
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _numPurchases,
        uint256 _saleId
    ) external view override returns (bool eligible) {
        uint256 rebateAmount;
        uint256 tokenType;
        Sale memory info = s_sales[_saleId];
        if (_tokenAddress != address(0)) {
            OwnershipRebate[] memory rebates = s_ownershipDiscounts[_saleId];
            for (uint256 i; i < rebates.length; ++i) {
                if (rebates[i].tokenAddress == _tokenAddress && (rebates[i].tokenType == TokenType.ERC721 || (rebates[i].tokenType == TokenType.ERC1155 && rebates[i].tokenId == _tokenId))) {
                    rebateAmount = rebates[i].basisPoints;
                    tokenType = uint256(rebates[i].tokenType);
                    if (rebateAmount > 0) {
                        if (tokenType == 0) {
                            if (IERC721Upgradeable(_tokenAddress).ownerOf(_tokenId) != _buyer) {
                                return false;
                            }
                        }
                        if (tokenType == 1) {
                            if (IERC1155Upgradeable(_tokenAddress).balanceOf(_buyer, _tokenId) == 0) {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                }
            }
        }

        if (s_perPlayerSold[_saleId][_buyer] + _numPurchases > info.userCap) {
            return false;
        }
        if (s_sold[_saleId] + _numPurchases > info.totalSupply) {
            return false;
        }
        return true;
    }

    // @inheritdoc IGravityGrade_Paper
    function onPaper(
        address _buyer,
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _numPurchases,
        uint256 _saleId
    ) external override {
        if (s_paperCurrency == ADDRESS_ZERO) revert GravityGrade__PaperCurrencyTokenAddressNotSet();
        _buyPacks(_saleId, _numPurchases, _tokenId, _tokenAddress, s_paperCurrency, _buyer);
    }

    /**
     * @notice internal function to faciltate purchasing any active sale in any whitelisted currency
     * @param _saleId the sale ID of the pack to purchase
     * @param _numPurchases the number of packs to purchase
     * @param _tokenId the tokenId claimed to be owned (for rebates)
     * @param _tokenAddress the token address for the tokenId claimed to be owned (for rebates)
     * @param _currency address of currency to use, address(0) for matic
     * @param _recipient the buyers' EOA address (mint to address)
     */
    function _buyPacks(
        uint256 _saleId,
        uint256 _numPurchases,
        uint256 _tokenId,
        address _tokenAddress,
        address _currency,
        address _recipient
    ) internal {
        if (s_sales[_saleId].tokenId == 0) revert GravityGrade__NonExistentSale(_saleId);
        if (s_saleStatus[_saleId] == true) revert GravityGrade__SaleInactive();
        if (s_whitelistedCurrencies[_saleId][_currency] == false) revert GravityGrade__CurrencyNotWhitelisted(_currency);
        Sale memory info = s_sales[_saleId];

        calculateTotalPacksBought(info.totalSupply, info.userCap, _saleId, _numPurchases, _recipient);

        uint256 balance = info.salePrice * _numPurchases;
        /// This is a dangerous check to avoid wierd behaviour when buying for others (using Paper)
        if (msg.sender == _recipient) {
            balance = _apply_bulk_discount(_saleId, balance, _numPurchases);
        }
        if (_tokenAddress != ADDRESS_ZERO) {
            uint256 updatedBalance = applyOwnershipRebates(s_ownershipDiscounts[_saleId], balance, _tokenAddress, _tokenId);
            balance = updatedBalance > 0 ? updatedBalance : balance;
        }
        if (_currency == ADDRESS_ZERO) {
            ethPayment(msg.sender, info.defaultCurrency, _currency, balance);
        } else {
            erc20Payment(info, _saleId, balance, _tokenId, _tokenAddress, _currency, msg.sender);
        }
        _mint(_recipient, info.tokenId, _numPurchases, "");

        distributeBeneficiaryTokens(_numPurchases * info.salePrice, _currency, info.defaultCurrency);
    }

    /**
     * @notice internal function to validate received payment and refund the difference
     * @param _recipient the buyers' EOA address
     * @param _defaultCurrency default currency (contract address)
     * @param _currency address of currency to use, address(0) for matic
     * @param _balance Total cost of the purchase
     */
    function ethPayment(
        address _recipient,
        address _defaultCurrency,
        address _currency,
        uint256 _balance
    ) internal {
        uint256 ethPrice = s_oracle.getAmountOut(_defaultCurrency, _currency, _balance);
        if (ethPrice > msg.value) {
            revert GravityGrade__InsufficientEthValue(msg.value, ethPrice);
        } else {
            if (msg.value - ethPrice > 0) {
                (bool callSuccess, ) = payable(_recipient).call{value: msg.value - ethPrice}("");
                if (!callSuccess) revert GravityGrade__RefundFailed();
            }
        }
    }

    /**
     * @notice internal function to calculate and take erc20 payments
     * @param _info the Sale information
     * @param _saleId the sale ID of the purchase
     * @param _balance Total cost of the purchase
     * @param _tokenId the tokenId claimed to be owned (for rebates)
     * @param _tokenAddress the token address for the tokenId claimed to be owned (for rebates)
     * @param _currency address of currency to use
     * @param _recipient the buyers' EOA address
     */
    function erc20Payment(
        Sale memory _info,
        uint256 _saleId,
        uint256 _balance,
        uint256 _tokenId,
        address _tokenAddress,
        address _currency,
        address _recipient
    ) internal {
        uint256 balance = _balance;
        if (_currency != _info.defaultCurrency) {
            balance = s_oracle.getAmountOut(_info.defaultCurrency, _currency, balance);
        }
        if (!IERC20(_currency).transferFrom(_recipient, address(this), balance)) revert GravityGrade__TransferFailed();
        // Todo: swap MATIC for _info.defaultCurrency if they're not eq
        if (!_info.profitState && (_currency != _info.defaultCurrency)) {
            IERC20(_currency).approve(address(s_swapManager), balance);
            s_swapManager.swap(_currency, _info.defaultCurrency, balance, address(this));
        }
    }

    /// @inheritdoc IGravityGrade
    function buyPacks(
        uint256 _saleId,
        uint256 _numPurchases,
        uint256 _tokenId,
        address _tokenAddress,
        address _currency
    ) external payable {
        _buyPacks(_saleId, _numPurchases, _tokenId, _tokenAddress, _currency, msg.sender);
    }

    /** @notice calculates the total packs bought for a given buyer
     *  @param _totalSupply cap on total amount to be sold
     *  @param _userCap a per-user cap
     *  @param _saleId the sale ID of the pack to purchase
     *  @param _numPurchases the number of packs to purchase
     *  @param _buyer the buyers' EOA address
     */
    function calculateTotalPacksBought(
        uint256 _totalSupply,
        uint256 _userCap,
        uint256 _saleId,
        uint256 _numPurchases,
        address _buyer
    ) internal {
        uint256 playerBought = s_perPlayerSold[_saleId][_buyer] + _numPurchases;
        uint256 totalBought = s_sold[_saleId] + _numPurchases;
        if (playerBought > _userCap) {
            revert GravityGrade__PurchaseExceedsPlayerMax(s_perPlayerSold[_saleId][_buyer], _userCap);
        } else {
            s_perPlayerSold[_saleId][_buyer] = playerBought;
        }
        if (totalBought > _totalSupply) {
            revert GravityGrade__PurchaseExceedsTotalMax(s_sold[_saleId], _totalSupply);
        } else {
            s_sold[_saleId] = totalBought;
        }
    }

    /** @notice sends tokens to the beneficaries
     *  @param _salePrice price of the pack
     *  @param _currency address of the currency used to buy the pack
     *  @param _defaultCurrency default currency (contract address)
     */
    function distributeBeneficiaryTokens(
        uint256 _salePrice,
        address _currency,
        address _defaultCurrency
    ) internal {
        Beneficiaries memory beneficiaries = s_beneficiaries;
        uint256 beneficiariesSize = beneficiaries.feeBps.length;
        for (uint256 i; i < beneficiariesSize; ++i) {
            uint256 amount = (beneficiaries.feeBps[i] * _salePrice) / MAXIMUM_BASIS_POINTS;
            if (_currency != _defaultCurrency) {
                IERC20(_currency).approve(address(s_swapManager), amount);
                s_swapManager.swap(_currency, _defaultCurrency, amount, beneficiaries.beneficiary[i]);
            } else {
                IERC20(_currency).approve(address(this), amount);
                if (!IERC20(_currency).transferFrom(address(this), beneficiaries.beneficiary[i], amount)) revert GravityGrade__TransferFailed();
            }
        }
    }

    /** @notice calculates the balance after any applicable token ownership rebates
     *  @param _rebates applicable to the sale
     *  @param _balance the current pack balance after any applied bulk discounts
     *  @param _tokenAddress the token address for the tokenId claimed to be owned (for rebates)
     *  @param _tokenId the tokenId claimed to be owned (for rebates)
     */
    function applyOwnershipRebates(
        OwnershipRebate[] memory _rebates,
        uint256 _balance,
        address _tokenAddress,
        uint256 _tokenId
    ) internal view returns (uint256) {
        uint256 rebateAmount;
        uint256 tokenType;
        uint256 updatedbalance;
        for (uint256 i; i < _rebates.length; ++i) {
            if (_rebates[i].tokenAddress == _tokenAddress && (_rebates[i].tokenType == TokenType.ERC721 || (_rebates[i].tokenType == TokenType.ERC1155 && _rebates[i].tokenId == _tokenId))) {
                rebateAmount = _rebates[i].basisPoints;
                tokenType = uint256(_rebates[i].tokenType);
            }
        }
        if (rebateAmount > 0) {
            bool applyRebate;
            if (tokenType == 0) {
                if (IERC721Upgradeable(_tokenAddress).balanceOf(msg.sender) > 0) {
                    applyRebate = true;
                } else {
                    revert GravityGrade__SenderDoesNotOwnToken(_tokenAddress, 0);
                }
            }
            if (tokenType == 1) {
                if (IERC1155Upgradeable(_tokenAddress).balanceOf(msg.sender, _tokenId) > 0) {
                    applyRebate = true;
                } else {
                    revert GravityGrade__SenderDoesNotOwnToken(_tokenAddress, _tokenId);
                }
            }
            if (applyRebate) updatedbalance = calculateDiscountedPrice(rebateAmount, _balance);
        } else {
            revert GravityGrade__TokenNotEligibleForRebate(_tokenAddress);
        }
        return updatedbalance;
    }

    /** @notice calculates the discounted price
     *  @param _bps discount amount as bps (basis points) e.g. 500 == 5 pct
     *  @param _salePrice the price upon which a discount should be applied
     */
    function calculateDiscountedPrice(uint256 _bps, uint256 _salePrice) internal pure returns (uint256) {
        return ((MAXIMUM_BASIS_POINTS - _bps) * _salePrice) / MAXIMUM_BASIS_POINTS;
    }

    /**
     * @notice Applies a bulk discount to a price
     * @param _saleId The id of the sale
     * @param _price The net price (i.e not the unit price)
     * @param _bulk The amount of units being purchased
     */
    function _apply_bulk_discount(
        uint256 _saleId,
        uint256 _price,
        uint256 _bulk
    ) internal view returns (uint256 final_price) {
        uint256 mod = MAXIMUM_BASIS_POINTS;
        uint256[] memory breakpoints = s_bulkDiscountBreakpoints[_saleId];
        uint256[] memory discounts = s_bulkDiscountBasisPoints[_saleId];
        // Todo: If we sort the discounts we can save a slight amount of gas... Let's not bother for the launch
        for (uint256 i; i < breakpoints.length; i++) {
            if (breakpoints[i] <= _bulk) {
                mod -= discounts[i];
            }
        }
        final_price = (mod * _price) / MAXIMUM_BASIS_POINTS;
    }

    /// @inheritdoc IGravityGrade
    function airdrop(
        address[] calldata _recipients,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external onlyTrusted {
        for (uint256 i; i < _recipients.length; i++) {
            if (bytes(s_uris[_tokenIds[i]]).length == 0) revert GravityGrade__TokenNotSet(_tokenIds[i]);
            _mint(_recipients[i], _tokenIds[i], _amounts[i], "");
        }
    }

    /**
     * @notice retreive Moderator
     * @return address The address of the current moderator
     */
    function getModerator() public view returns (address) {
        return s_moderator;
    }

    /** @notice change Token Name
     *  @param _name new name
     */
    function setName(string memory _name) external onlyGov {
        name = _name;
    }

    /** @notice change Token Symbol
     *  @param _symbol new symbol
     */
    function setSymbol(string memory _symbol) external onlyGov {
        symbol = _symbol;
    }

    /** @notice retreive the latest SaleId
     * @return uint256 The latest sale id
     */
    function getSaleId() public view returns (uint256) {
        return s_saleId;
    }

    /** @notice retreives the beneficiaries
     * @return Beneficiaries The beneficiaries
     */
    function getBeneficiaries() public view returns (Beneficiaries memory) {
        return s_beneficiaries;
    }

    /** @notice retreives the sale
     *  @param _saleId saleId
     * @return Sale The information regarding the sale
     */
    function getSale(uint256 _saleId) public view returns (Sale memory) {
        return s_sales[_saleId];
    }

    /// @inheritdoc IGravityGrade
    function setSaleState(uint256 _saleId, bool _paused) external onlyGov {
        if (s_sales[_saleId].tokenId == 0) revert GravityGrade__NonExistentSale(_saleId);
        s_saleStatus[_saleId] = _paused;
        emit SaleState(_saleId, _paused);
    }

    /** @notice retreives the sale status
     *  @param _saleId saleId
     * @return Whether the sale is paused or not
     */
    function getSaleStatus(uint256 _saleId) public view returns (bool) {
        return s_saleStatus[_saleId];
    }

    /// @inheritdoc IGravityGrade_Paper
    function setPaperCurrency(address _paperCurrency) external onlyGov {
        if (_paperCurrency == ADDRESS_ZERO) revert GravityGrade__ZeroAddress();
        s_paperCurrency = _paperCurrency;
    }

    /** @notice retreives the paper currency token address
     * @return The address of the supported paper currency token
     */
    function getPaperCurrency() public view returns (address) {
        return s_paperCurrency;
    }

    /// @inheritdoc IGravityGrade
    function burn(
        address _from,
        uint256 _tokenId,
        uint256 _amount
    ) external override onlyTrusted {
        if (bytes(s_uris[_tokenId]).length == 0) revert GravityGrade__TokenNotSet(_tokenId);
        _burn(_from, _tokenId, _amount);
    }
}
