// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.2;

import "../interfaces/IERC1155StoreGenericDataProvider.sol";
import "./ERC1155StoreGeneric.sol";

contract ERC1155StoreGenericDataProvider is IERC1155StoreGenericDataProvider {

    ERC1155StoreGeneric storeContract;

    constructor(ERC1155StoreGeneric _storeContract) {
        storeContract = _storeContract;
    }

    function checkPrice(
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _numPurchases,
        uint256 _saleId
    ) external returns (uint256 _price) {
        if (storeContract.s_paperCurrency() == address(0)) revert SG__PaperCurrencyTokenAddressNotSet();
        address currency = storeContract.s_paperCurrency();
        
        // returned vaule from calling default map view function is tuple, not a struct
        // but we need only unitPrice and defaultCurrency from Sale struct
        ( , , , , uint256 unitPrice, , address defaultCurrency, , , ) = storeContract.s_sales(_saleId);

        uint256 salePrice = unitPrice * _numPurchases;
        uint256 discountPrice;
        if (_tokenAddress != address(0)) {
            IERC1155StoreGeneric.OwnershipDiscount[] memory ownershipDiscounts = getOwnershipDiscounts(_saleId);
            uint256 discount;
            for (uint256 i; i < ownershipDiscounts.length; ++i) {
                if (
                    ownershipDiscounts[i].tokenAddress == _tokenAddress &&
                    (ownershipDiscounts[i].tokenType == IERC1155StoreGeneric.TokenType.ERC721 ||
                        _tokenId == ownershipDiscounts[i].tokenId)
                ) {
                    discount = ownershipDiscounts[i].basisPoints;
                }
            }
            if (discount > 0) {
                discountPrice = storeContract._calculateDiscountedPrice(discount, salePrice);
            } else {
                revert SG__TokenNotEligibleForRebate(_tokenAddress);
            }
        }
        salePrice = discountPrice > 0 ? discountPrice : salePrice;
        if (currency != defaultCurrency) {
            salePrice = getOracle().getAmountOut(defaultCurrency, currency, salePrice);
        }
        _price = salePrice;
    }

    function checkERC1155PaymentPrice(
        uint256 _numPurchases,
        uint256 _saleId,
        uint256 _paymentTokenId
    ) external view returns(uint256 _price) {
        _price = _numPurchases * storeContract.s_ERC1155tokenPaymentPrices(_saleId, _paymentTokenId);
        if (_price == 0) revert SG__InvalidERC1155PaymentTokenId();
    }

    function getOracleManager() external view returns (address) {
        return address(storeContract.s_oracle());
    }

    function getSwapManager() external view returns (address) {
        return address(storeContract.s_swapManager());
    }

    function getSaleBonus() external view returns (address) {
        return address(storeContract.s_saleBonuses());
    }

    function getOwnershipDiscounts(uint256 _saleId) public view returns(IERC1155StoreGeneric.OwnershipDiscount[] memory) { 
        return storeContract.getOwnershipDiscounts(_saleId);
    }

    function getOracle() public view returns(IOracleManager) {
        return storeContract.s_oracle();
    }
}