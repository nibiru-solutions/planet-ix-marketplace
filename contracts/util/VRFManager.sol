// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "../rover/VRFCoordinatorV2Interface.sol";
//import "@chainlink/contracts/src/v0.8/VRFConsumerBaseV2.sol";

interface IRNGConsumer {
    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords) external;
}

contract VRFManager /*is VRFConsumerBaseV2 */ {
    struct Params {
        uint64 subscriptionId;
        bytes32 keyHash;
        uint32 callbackGasLimit;
        uint16 requestConfirmations;
        uint32 numWords;
    }
    VRFCoordinatorV2Interface COORDINATOR;

    mapping(uint256 => address) s_requestIds;
    mapping(address => bool) isConsumer;
    mapping(address => Params) consumerParams;

    address s_owner;

    modifier onlyOwner() {
        require(msg.sender == s_owner);
        _;
    }
    modifier onlyConsumer() {
        require(isConsumer[msg.sender]);
        _;
    }

    constructor(address vrfCoordinator) /* VRFConsumerBaseV2(vrfCoordinator) */ {
        COORDINATOR = VRFCoordinatorV2Interface(vrfCoordinator);
        s_owner = msg.sender;
    }

    function getRandomNumber(uint32 numWords) external onlyConsumer returns (uint256 requestId) {
        Params memory params = consumerParams[msg.sender];
        requestId = COORDINATOR.requestRandomWords(
            params.keyHash,
            params.subscriptionId,
            params.requestConfirmations,
            params.callbackGasLimit,
            numWords
        );
        s_requestIds[requestId] = msg.sender;
    }

    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords) internal /* override */ {
        address rngConsumer = s_requestIds[requestId];
        IRNGConsumer(rngConsumer).fulfillRandomWords(requestId, randomWords);
    }

    function setConsumer(
        address _consumer,
        uint64 _subscriptionId,
        bytes32 _keyHash,
        uint32 _callbackGasLimit,
        uint16 _requestConfirmations,
        uint32 _numWords
    ) external onlyOwner {
        isConsumer[_consumer] = true;
        consumerParams[_consumer] = Params({
            subscriptionId: _subscriptionId,
            keyHash: _keyHash,
            callbackGasLimit: _callbackGasLimit,
            requestConfirmations: _requestConfirmations,
            numWords: _numWords
        });
    }
}