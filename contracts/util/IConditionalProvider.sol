pragma solidity ^0.8.0;

/**
 * @title Interface used to check eligibility of an address for something
 */
interface IConditionalProvider {
    /**
     * @notice Returns whether the address is eligible
     * @param _address The address
     * @return _isEligible Whether the address is eligible
     */
    function isEligible(address _address) external view returns (bool _isEligible);
}
