// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;


/// @title Generic interface for burning.
interface Burnable {
    function burn(uint256 amount) external;
}