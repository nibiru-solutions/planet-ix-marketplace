pragma solidity ^0.8.0;

// @title Interface for a generic burn contract
interface IGenericBurnV2 {
    /**
    This interface is a little complex.

    When a player burns a token, they are guaranteed one draw from each content category for that token id.

    For a content category, first the amount of contents are drawn. Then, each content is drawn until the drawn amount
    has been reached.

    Here is an example of how it should be used

    The admin wants to players to burn GG token id 18. When they burn it, they have a 50% chance of getting a tokenID 19
    on GG and are guaranteed to get 2-5 additional goodies from the PIX Assets. Further, if you own a gold badge you also
    get 100 Astro Credits

    First, he calls

        let newID = contract.createContentCategory(18);

    Now, he wants there to be a 50% chance of nothing and a 50% chance of 1 token id 19 on GG, so he calls

        contract.setContentAmounts(18, newId, [1, 0], [10,10]);

    Since there is only one possibility for the contents, he calls

        contract.setContents(18, newId, [GG_address], [19], [1], [1]);

    Next, he configures the PIX asset goodies. Thus, he creates a content category

        let newID2 = contract.createContentCategory(18);

    Then, he sets the content amounts

        contract.setContentAmounts(18, newId2, [2, 3, 4, 5], [10,20, 20,10]);

    Finally, he sets the possible contents

        contract.setContents(18, newId2,
                        [asset_address,
                         asset_address,
                         asset_address],
                         [astro_credit_id
                          biomod_legendary_id
                          blueprint_id],
                         [100, 1, 1],
                         [100, 20, 10]);

    For the gold badge = astro credits, the admin has created a conditional provider along the lines of

        function isEligible(address _address) external view returns (bool _isEligible){
            _isEligible = IERC1555(pixAssets).balanceOf(_address, gold_badge_id) > 0;
        }

   He creates a new category with the contents as previously described, then he calls

       contract.setContentEligibility(18, id3, conditionalProvider.address);

    Finally, the admin whitelists the token for burning

        contract.whitelistToken(18, true);

    */

    error GB__ZeroWeight();
    error GB_ZeroAmount();
    error GB__TokenNotWhitelisted(uint256 _tokenId);
    error GB__InvalidCategoryId(uint256 _categoryId);
    error GB__NotTrustedMintable(address _tokenAddress);
    error GB__ArraysNotSameLength();
    error GB__NotConditionalProvider(address _address);
    error GB__NotGov(address _address);
    error GB__NotEligible(address _address);
    error GB__NotOracle();
    error GB__ZeroAddress();
    error GB__MaxDrawsExceeded(uint256 _amount);
    /**
     * @notice Event emitted upon opening packs
     * @param _opener The address of the opener
     * @param _tokenId The tokenid that was "opened"
     * @param _numPacks The number of tokens that were "opened"
     */
    event PackOpened(address _opener, address _token, uint256 _tokenId, uint256 _numPacks);
    /**
     * @notice Event emitted when a category is whitelisted
     * @param _tokenId The id of the token
     * @param _isWhitelisted Whether it's burnable
     */
    event TokenWhitelisted(uint256 _tokenId, bool _isWhitelisted);
    /**
     * @notice Event emitted when a category is created
     * @param _tokenId The token id a category has been created for
     * @param _categoryId The id of the new category
     */
    event CategoryCreated(uint256 _tokenId, uint256 _categoryId);
    /**
     * @notice Event emitted when a category is deleted
     * @param _tokenId The token id a category has been deleted for
     * @param _categoryId The id of the deleted category
     */
    event CategoryDeleted(uint256 _tokenId, uint256 _categoryId);
    /**
     * @notice Event emitted when a category has its eligibility updated
     * @param _tokenId The token id which the category belongs to
     * @param _categoryId The id of the category
     * @param _provider The address of the eligibility provider
     */
    event CategoryEligibilitySet(uint256 _tokenId, uint256 _categoryId, address _provider);
    /**
     * @notice Event emitted when a categories content amounts are updated
     * @param _tokenId The token id of the token
     * @param _categoryId The category Id of a token
     * @param _amounts Array containing the amounts
     * @param _weights Array containing the weights, corresponding by index.
     */
    event ContentAmountsUpdated(uint256 _tokenId, uint256 _categoryId, uint256[] _amounts, uint256[] _weights);

    /**
     * @notice Event emitted when the contents of a category are updated
     * @param _tokenId The token id of the token
     * @param _contentCategory The category Id of a token
     * @param _tokens Array of addresses to the content tokens.
     * @param _tokenIds Tokens ids of contents. Will be ignored if the token is an ERC721
     * @param _amounts Array containing the amounts of each tokens
     * @param _weights Array containing the weights, corresponding by index.
     */
    event ContentsUpdated(
        uint256 _tokenId,
        uint256 _contentCategory,
        address[] _tokens,
        uint256[] _tokenIds,
        uint256[] _amounts,
        uint256[] _weights
    );

    /**
     * @notice Event emitted when the user gains a reward from opening a pack.
     * @param _token Address of the reward token
     * @param _tokenId The token id of the token
     * @param _amount amount of the token being rewarded
     */
    event RewardGranted(address _token, uint256 _tokenId, uint256 _amount);

    event GuaranteedRewardSet(uint256 _tokenId, GuaranteedReward[] _rewards);

    struct ContentCategory {
        uint256 id;
        uint256 contentAmountsTotalWeight;
        uint256 contentsTotalWeight;
        uint256[] contentAmounts;
        uint256[] contentAmountsWeights;
        uint256[] tokenAmounts;
        uint256[] tokenWeights;
        address[] tokens;
        uint256[] tokenIds;
    }

    struct GuaranteedReward {
        address token;
        uint256 tokenId;
        uint256 tokenAmount;
    }

    struct RequestInputs {
        address user;
        address token;
        uint256 tokenId;
        uint256 openings;
        uint256 randWordsCount;
        uint256[] excludedIds;
    }

    struct SetContentInputs {
        address token;
        uint256 tokenId;
        uint256 categoryId;
        address[] tokens;
        uint256[] tokenIds;
        uint256[] amounts;
        uint256[] weights;
    }

    /**
     * @notice Burns the "pack" thus "opening" it
     * @param _tokenId The tokenId of the pack to burn
     * @param _amount The amount of tokens to burn
     * @param _optIn whether or not the user wants to check eligibility for categories with such requirements
     *
     * Throws GB__TokenNotWhitelisted on non whitelisted token
     *
     */
    function burnPack(address _token, uint256 _tokenId, uint32 _amount, bool _optIn) external;

    /**
     * @notice Used to set whether a token is burnable by this contract
     * @param _tokenId The id of the token
     * @param _isWhitelisted Whether it's burnable
     *
     * Throws GB__NotGov on non gov call
     *
     * Emits TokenWhitelisted
     */
    function whitelistToken(
        address[] calldata _token,
        uint256[] calldata _tokenId,
        bool[] calldata _isWhitelisted
    ) external;

    /**
     * @notice Used to create a content category
     * @param _tokenId The token id to create a category for
     * @return _categoryId The new ID of the content category
     *
     * Throws GB__NotGov on non gov call
     *
     * Emits CategoryCreated
     */
    function createContentCategory(address _token, uint256 _tokenId) external returns (uint256 _categoryId);

    /**
     * @notice Deletes a content category
     * @param _tokenId The token id
     * @param _contentCategory The content category ID
     *
     * Throws GB__NotGov on non gov call
     * Throws GB__InvalidCategoryId on invalid category ID
     *
     * Emits CategoryDeleted
     */
    function deleteContentCategory(address _token, uint256 _tokenId, uint256 _contentCategory) external;

    /**
     * @notice Used to set eligibility conditions for a content category
     * @param _tokenId The token id
     * @param _categoryId The category id
     * @param _conditionalProvider Address to contract implementing ConditionalProvider
     *
     * Throws GB__NotGov on non gov call
     * Throws GB__NotConditionalProvider on _conditionalProvider not implementing ConditionalProvider or erc165
     *
     * Emits CategoryEligibilitySet
     */
    function setContentEligibility(
        address _token,
        uint256 _tokenId,
        uint256 _categoryId,
        address _conditionalProvider
    ) external;

    /**
     * @notice Used to get the content categories for a token
     * @param _tokenId The token id
     * @return _categories Array of ContentCategory structs corresponding to the given id
     */
    function getContentCategories(
        address _token,
        uint256 _tokenId
    ) external view returns (ContentCategory[] calldata _categories);

    /**
     * @notice Used to edit the content amounts for a content category
     * @param _tokenId The token id of the token
     * @param _contentCategory The category Id of a token
     * @param _amounts Array containing the amounts
     * @param _weights Array containing the weights, corresponding by index.
     *
     * Throws GB__NotGov on non gov call.
     * Throws GB__ZeroWeight on any weight being zero
     * @dev Does not throw anything on zero amounts
     * Throws GB__InvalidCategoryId on invalid category ID
     * Throws GB__ArraysNotSameLength on arrays not being same length
     *
     * Emits ContentAmountsUpdated
     */
    function setContentAmounts(
        address _token,
        uint256 _tokenId,
        uint256 _contentCategory,
        uint256[] memory _amounts,
        uint256[] memory _weights
    ) external;

    /**
     * @notice Used to edit the contents for a content category
     * @dev _tokens needs to be implementing ITrustedMintable
     *
     * Throws GB__NotGov on non gov call.
     * Throws GB__ZeroWeight on any weight being zero
     * Throws GB__ZeroAmount on any amount being zero
     * Throws GB__InvalidCategoryId on invalid category ID
     * Throws GB__NotTrustedMintable on any address not implementing ITrustedMintable
     * Throws GB__ArraysNotSameLength on arrays not being same length
     *
     * Emits ContentsUpdated
     */
    function setContents(SetContentInputs calldata args) external;
}
