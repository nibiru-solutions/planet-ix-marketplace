pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165Upgradeable.sol";
import "./IConditionalProvider.sol";

/**
 * @title Abstract extension of IConditionalProvider which also implements ERC165
 */
abstract contract ConditionalProvider is ERC165Upgradeable, IConditionalProvider {
    ///@inheritdoc IERC165Upgradeable
    function supportsInterface(bytes4 interfaceId)
        public
        view
        virtual
        override(ERC165Upgradeable)
        returns (bool)
    {
        return
            interfaceId == type(IConditionalProvider).interfaceId ||
            super.supportsInterface(interfaceId);
    }
}
