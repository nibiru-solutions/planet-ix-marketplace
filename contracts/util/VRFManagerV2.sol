// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@chainlink/contracts/src/v0.8/vrf/dev/interfaces/IVRFCoordinatorV2Plus.sol";
import "@chainlink/contracts/src/v0.8/vrf/dev/VRFConsumerBaseV2Plus.sol";
import {VRFV2PlusClient} from "@chainlink/contracts/src/v0.8/vrf/dev/libraries/VRFV2PlusClient.sol";

interface IRNGConsumer {
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] calldata randomWords) external;
}

contract VRFManagerV2 is VRFConsumerBaseV2Plus {
    struct Params {
        uint256 subscriptionId;
        bytes32 keyHash;
        uint32 callbackGasLimit;
        uint16 requestConfirmations;
        uint32 numWords;
    }
    IVRFCoordinatorV2Plus COORDINATOR;

    mapping(uint256 => address) s_requestIds;
    mapping(address => bool) isConsumer;
    mapping(address => Params) consumerParams;

    address s_owner;

    modifier onlyConsumer() {
        require(isConsumer[msg.sender]);
        _;
    }

    constructor(address vrfCoordinator) VRFConsumerBaseV2Plus(vrfCoordinator) {
        COORDINATOR = IVRFCoordinatorV2Plus(vrfCoordinator);
        s_owner = msg.sender;
    }

    function getRandomNumber(uint32 numWords) external onlyConsumer returns (uint256 requestId) {
        Params memory params = consumerParams[msg.sender];
        requestId = COORDINATOR.requestRandomWords(
            VRFV2PlusClient.RandomWordsRequest({
                keyHash: params.keyHash,
                subId: params.subscriptionId,
                requestConfirmations: params.requestConfirmations,
                callbackGasLimit: params.callbackGasLimit,
                numWords: numWords,
                extraArgs: VRFV2PlusClient._argsToBytes(VRFV2PlusClient.ExtraArgsV1({nativePayment: false}))
            })
        );
        s_requestIds[requestId] = msg.sender;
    }

    function fulfillRandomWords(uint256 requestId, uint256[] calldata randomWords) internal override {
        address rngConsumer = s_requestIds[requestId];
        IRNGConsumer(rngConsumer).fulfillRandomWordsRaw(requestId, randomWords);
    }

    function setConsumer(
        address _consumer,
        uint256 _subscriptionId,
        bytes32 _keyHash,
        uint32 _callbackGasLimit,
        uint16 _requestConfirmations,
        uint32 _numWords
    ) external onlyOwner {
        isConsumer[_consumer] = true;
        consumerParams[_consumer] = Params({
            subscriptionId: _subscriptionId,
            keyHash: _keyHash,
            callbackGasLimit: _callbackGasLimit,
            requestConfirmations: _requestConfirmations,
            numWords: _numWords
        });
    }
}
