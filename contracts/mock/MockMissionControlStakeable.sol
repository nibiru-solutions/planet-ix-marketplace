pragma solidity ^0.8.0;

// import "../mission_control/IMissionControlStakeable.sol";

contract MockMissionControlStakeable {
    bool reverts;
    bool base;
    uint256 reward = 1;
    uint256 rewardId = 1;
    mapping(uint256 => bool) staked;

    mapping(uint256 => uint256) collected;

    mapping(address => mapping(uint256 => uint256)) tokenIds;
    mapping(address => mapping(uint256 => uint256)) seeds;

    mapping(uint256 => address) owner;

    constructor(bool _reverts, bool _base) {
        reverts = _reverts;
        base = _base;
    }

    int256 time_Left = 1000;

    function isStaked(uint256 tokenId) external view returns (bool) {
        return staked[tokenId];
    }

    function ownerOf(uint256 tokenId) external view returns (address) {
        return owner[tokenId];
    }

    function setOwner(uint256 tokenId, address _owner) public {
        owner[tokenId] = _owner;
    }

    function setReward(uint256 newReward, uint256 newRewardId) external {
        reward = newReward;
        rewardId = newRewardId;
    }

    function setReverts(bool _reverts) external {
        reverts = _reverts;
    }

    modifier checkRevert(string memory reason) {
        require(!reverts, reason);
        _;
    }

    function reset(address _userAddress, uint256 _nonce) external {}

    function checkTile(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external view checkRevert("CHECK TILE") returns (uint256 _amount, uint256 _retTokenId) {
        _amount = reward;
        _retTokenId = rewardId;
    }

    function onCollect(
        address _userAddress,
        uint256 _nonce,
        uint256 _pausedAt,
        int256 _x,
        int256 _y
    ) external checkRevert("ON COLLECT") returns (uint256, uint256) {
        collected[tokenIds[_userAddress][_nonce]] += reward;
        seeds[_userAddress][_nonce] = block.timestamp;
        return (reward, rewardId);
    }

    function isBase(uint256 _tokenId, int256 _x, int256 _y) external view returns (bool _isBase) {
        _isBase = base;
    }

    function isRaidable(uint256 _tokenId, int256 _x, int256 _y) external view returns (bool _isRaidable) {
        _isRaidable = true;
    }

    function collectedFromToken(uint256 tokenId) external view returns (uint256) {
        return collected[tokenId];
    }

    function onStaked(
        address _currentOwner,
        uint256 _tokenId,
        uint256 _nonce,
        address _underlyingToken,
        uint256 _underlyingTokenId,
        address _besideToken,
        uint256 _besideTokenId,
        uint256 _besideNonce,
        int256 _x,
        int256 _y
    ) external checkRevert("TRANFER TO MC") {
        require(owner[_tokenId] == _currentOwner, "Unexpected owner");
        tokenIds[_currentOwner][_nonce] = _tokenId;
        setOwner(_tokenId, address(this));
        __onActive(_currentOwner, _tokenId, _nonce);
        seeds[_currentOwner][_nonce] = block.timestamp;
    }

    function onUnstaked(
        address _newOwner,
        uint256 _nonce,
        uint256 /* _besideNonce */
    ) external checkRevert("TRANFER FROM MC") {
        uint256 _tokenId = tokenIds[_newOwner][_nonce];
        require(owner[_tokenId] == address(this), "Unexpected owner");
        setOwner(_tokenId, _newOwner);
        __onInactive(_newOwner, _tokenId, _nonce);
    }

    function __onActive(address _userAddress, uint256 _tokenId, uint256 _nonce) public checkRevert("ON STAKED") {
        staked[_tokenId] = true;
    }

    function __onInactive(address _userAddress, uint256 _tokenId, uint256 _nonce) public checkRevert("ON UNSTAKED") {
        staked[_tokenId] = false;
    }

    function checkTimestamp(address _userAddress, uint256 _nonce) external view returns (uint256 _timestamp) {
        _timestamp = seeds[_userAddress][_nonce];
    }

    function timeLeft(
        address _userAddress,
        uint256 _nonce,
        int256 _x,
        int256 _y
    ) external view returns (int256 _timeLeft) {
        _timeLeft = int256(block.timestamp - seeds[_userAddress][_nonce]); // I dunno....
    }

    function onResume(address _renter, uint256 _nonce) external {}
}
