pragma solidity ^0.8.0;

import "../interfaces/IOracleManager.sol";

contract OracleManagerMock is IOracleManager {

    mapping(address => mapping(address => uint)) numerators;
    mapping(address => mapping(address => uint)) denominators;

    function setRatio(
        address srcToken,
        address dstToken,
        uint numerator,
        uint denominator) external {
        // this may lead to some cheeky rounding errors but this is fine if its expected in a testing context
        numerators[srcToken][dstToken] = numerator;
        denominators[srcToken][dstToken] = denominator;
        numerators[dstToken][srcToken] = denominator;
        denominators[dstToken][srcToken] = numerator;
    }

    function _getAmountOut(
        address srcToken,
        address dstToken,
        uint256 amountIn
    ) internal view returns (uint256){
        if(srcToken == dstToken){
            return amountIn;
        }
        require(numerators[srcToken][dstToken] > 0, "Mock oracle manager: Pair not initialised");
        return (amountIn * numerators[srcToken][dstToken]) / denominators[srcToken][dstToken];
    }

    function getAmountOut(
        address srcToken,
        address dstToken,
        uint256 amountIn
    ) external override returns (uint256){
        return _getAmountOut(srcToken, dstToken, amountIn);
    }

    function getAmountOutView(
        address srcToken,
        address dstToken,
        uint256 amountIn
    ) external override view returns (uint256){
        return _getAmountOut(srcToken, dstToken, amountIn);
    }
}
