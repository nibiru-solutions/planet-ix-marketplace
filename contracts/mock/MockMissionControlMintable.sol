pragma solidity ^0.8.0;

import "../mission_control/IAssetManager.sol";
import "hardhat/console.sol";

contract MockMissionControlMintable is IAssetManager {


    mapping(address => mapping(uint256 => uint256)) balances;

    /// @inheritdoc IAssetManager
    function trustedMint(address _to, uint _tokenId, uint _amount) external override {
        _handle_mint(_to, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchMint(address _to, uint[] calldata _tokenIds, uint[] calldata _amounts) external override {
        require(_tokenIds.length == _amounts.length, "Mock asset manager: incorrect lengths for mint lists");
        for (uint i = 0; i < _tokenIds.length; i++) {
            _handle_mint(_to, _tokenIds[i], _amounts[i]);
        }

    }

    /// @inheritdoc IAssetManager
    function trustedBurn(address _from, uint _tokenId, uint _amount) external override {
        _handle_burn(_from, _tokenId, _amount);
    }

    /// @inheritdoc IAssetManager
    function trustedBatchBurn(address _from, uint[] calldata _tokenIds, uint[] calldata _amounts) external override {
        require(_tokenIds.length == _amounts.length, "Mock asset manager: incorrect lengths for burn lists");
        for (uint i = 0; i < _tokenIds.length; i++) {
            _handle_burn(_from, _tokenIds[i], _amounts[i]);
        }

    }

    function check_balance(address account, uint256 id) external view returns (uint) {
        return balances[account][id];
    }

    function _handle_mint(address account, uint id, uint amount) private {
        balances[account][id] += amount;
    }

    function _handle_burn(address account, uint id, uint amount) private {
        require(balances[account][id] >= amount, "mock asset manager: Not enough tokens to burn");
        balances[account][id] -= amount;

    }
}
