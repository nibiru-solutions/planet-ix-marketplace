// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../mission_control/IPIXRarityProvider.sol";
import "../interfaces/IPIX.sol";

/**
* @title A version of the
 */
contract ERC721MockPIX is ERC721, IPIXRarityProvider {
    constructor(string memory name, string memory symbol) ERC721(name, symbol) {}

    function getPIXRarity(uint _tokenId) external override returns (uint){
        return _tokenId % 5;
    }


    function getInfo(uint256 _tokenId) external view returns (IPIX.PIXInfo memory ret){
        ret.category = IPIX.PIXCategory(_tokenId % 5);
        ret.size = IPIX.PIXSize((_tokenId % 25) / 5);
    }

    function baseURI() public view returns (string memory) {
        return _baseURI();
    }

    function exists(uint256 tokenId) public view returns (bool) {
        return _exists(tokenId);
    }

    function mint(address to, uint256 tokenId) public {
        _mint(to, tokenId);
    }

    function safeMint(address to, uint256 tokenId) public {
        _safeMint(to, tokenId);
    }

    function safeMint(
        address to,
        uint256 tokenId,
        bytes memory _data
    ) public {
        _safeMint(to, tokenId, _data);
    }

    function burn(uint256 tokenId) public {
        _burn(tokenId);
    }
}