// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../interfaces/ITrustedMintable.sol";

/**
 * @title ERC721Mock
 This is a erc721 ganked from open zeppelin
 */
contract ERC721Mock is ERC721, ITrustedMintable {
    uint256 totalSupply;
    mapping(address => bool) trusted;

    modifier trustedOnly() {
        require(trusted[msg.sender], "Non trusted sender");
        _;
    }

    constructor(string memory name, string memory symbol) ERC721(name, symbol) {}

    function setTrusted(address _trusted, bool _isTrusted) external {
        trusted[_trusted] = _isTrusted;
    }

    function baseURI() public view returns (string memory) {
        return _baseURI();
    }

    function exists(uint256 tokenId) public view returns (bool) {
        return _exists(tokenId);
    }

    function mint(address to, uint256 tokenId) public {
        _mint(to, tokenId);
    }

    function safeMint(address to, uint256 tokenId) public {
        _safeMint(to, tokenId);
    }

    function safeMint(
        address to,
        uint256 tokenId,
        bytes memory _data
    ) public {
        _safeMint(to, tokenId, _data);
    }

    function burn(uint256 tokenId) public {
        _burn(tokenId);
    }

    function trustedMint(
        address _to,
        uint256, // _tokenId,
        uint256 _amount
    ) external trustedOnly {
        for (uint256 i; i < _amount; i++) {
            unchecked {
                ++totalSupply;
            }
            _safeMint(_to, totalSupply);
        }
    }

    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external trustedOnly {}
}
