// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../mission_control/IBaseLevel.sol";

contract BaseLevelMock is IBaseLevel {

    mapping(address => uint) public userLevel;

    function setSuperAppAddress(address _superAppAddress) external override {}

    function setSuperFluidPerLevel(uint _superFluidPerLevel) external override {}

    function setOrderCapacity(
        address _trader,
        uint _fromLevel,
        uint _toLevel,
        uint _additionalOrders
    ) external override {}

    function setSuperTokens(address _superToken, address _superTokenLite) external override {}

    function getOrderCapacity(address _trader, address _user) external view override returns (uint _extraOrders) {}

    function getBaseLevel(address _user) external view override returns (uint _level) {
      return userLevel[_user];
    }

    function setUserBaseLevel(address _user, uint _level) external {
      userLevel[_user] = _level;
    }
}