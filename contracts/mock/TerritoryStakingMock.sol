// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "../interfaces/IPIX.sol";
import "../interfaces/IPIXTerritoryStaking.sol";

contract PIXTerritoryStakingMock is IPIXTerritoryStaking {
    // You can define the internal states and structures used for the mock here

    function setModerator(address moderator, bool approved) external override {}

    function setRewardToken(address rewardTokenAddress) external override {}

    function initializePoolRewards(IPIX.PIXSize[] calldata pixSize, IPIX.PIXCategory[] calldata pixCategory, uint256[] calldata rewardPercentages, uint256[] calldata currentStakedTokens, uint256 _rewardPercentagesDenominator) external override {}

    function setRewardPercentagePaidPerTokensStaked(uint256[] calldata _rewardPercentagePaidPerTokensStaked) external override {}

    function setPushRewardNextEpochsPercentages(IPIX.PIXSize pixSize, uint256[] calldata _pushRewardNextEpochsPercentages) external override {}

    function setRewardDistributor(address distributor) external override {}

    function stake(uint256 tokenId) external override {}

    function unstake(uint256 tokenId) external override {}

    function addEpochRewards(uint256 epochTotalReward, uint256 epochDuration) external override {}

    function earnedReward(uint256 tokenId) external view override returns (uint256) {
        return 0;
    }

    function earnedRewardUntilTimestamp(uint256 tokenId, uint256 untilTimestamp) external view override returns (uint256) {
        return 0;
    }

    function earnedBatch(uint256[] memory tokenIds) external view override returns (uint256[] memory) {
        return new uint256[](0);
    }

    function earnedBatchUntilTimestamp(uint256[] memory tokenIds, uint256 untilTimestamp) external view override returns (uint256[] memory) {
        return new uint256[](0);
    }

    function earnedByAccount(address account) external view override returns (uint256) {
        return 0;
    }

    function earnedByAccountUntilTimestamp(address account, uint256 untilTimestamp) external view override returns (uint256) {
        return 0;
    }

    function claimBatch(uint256[] memory tokenIds) external override {}

    function claimAll() external override {}

    function isStaked(uint256 tokenId) external view override returns (bool) {
        return false;
    }

    function getStakedNFTsLength(address user) external view override returns (uint256) {
        return 0;
    }

    function getStakedNFTs(address user) external view override returns (uint256[] memory) {
        return new uint256[](0);
    }

    function getPushedRewards(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory, uint256 epochId) external view override returns (uint256) {
        return 0;
    }

    function getRewardPerToken(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external view override returns (uint256) {
        return 0;
    }

    function getCurrentRewardPerSecond(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external view override returns (uint256) {
        return 0;
    }

    function getTokensStaked(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external view override returns (uint256) {
        return 0;
    }
}
