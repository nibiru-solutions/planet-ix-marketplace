pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

/// @title Contract to test the event bot
contract EventTester is OwnableUpgradeable {

    enum Severity{
        ADMIM,
        NORMAL,
        IMPORTANT,
        CRITICAL
    }

    event AdminTestEvent(string _message);
    event NormalTestEvent(string _message);
    event ImportantTestEvent(string _message);
    event CriticalTestEvent(string _message);

    function initialize() public initializer {
        __Ownable_init();
    }

    function ping(Severity _severity, string calldata _message) external onlyOwner {
        if (_severity == Severity.ADMIM) {
            emit AdminTestEvent(_message);
        }
        if (_severity == Severity.NORMAL) {
            emit NormalTestEvent(_message);
        }
        if (_severity == Severity.IMPORTANT) {
            emit ImportantTestEvent(_message);
        }
        if (_severity == Severity.CRITICAL) {
            emit CriticalTestEvent(_message);
        }
    }

}
