//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {IXReceiver} from "@connext/nxtp-contracts/contracts/core/connext/interfaces/IXReceiver.sol";
import "hardhat/console.sol";

contract MockConnext {
    function xcall(
        uint32 _destination,
        address _to,
        address _asset,
        address _delegate,
        uint256 _amount,
        uint256 _slippage,
        bytes calldata _callData
    ) external payable returns (bytes32) {
        uint32 _origin = _destination == 1 ? 137 : 1;
        (bool success, bytes memory returnData) = _to.call(
            abi.encodeWithSelector(
                IXReceiver.xReceive.selector,
                keccak256("transferId"),
                _amount,
                _asset,
                msg.sender,
                _origin,
                _callData
            )
        );
        require(success, "CONNEXT: xcall failed");
        return keccak256(returnData);
    }
}
