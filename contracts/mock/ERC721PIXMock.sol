// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../interfaces/IPIX.sol";
import "../interfaces/IPIX.sol";

/**
 * @title ERC721Mock
 This is a erc721 ganked from open zeppelin
 */
contract ERC721PIXMock is ERC721, IPIX {
    constructor(string memory name, string memory symbol) ERC721(name, symbol) {}

    mapping(uint => PIXInfo) info;
    string baseURIVal;

    function setUri(string memory newUri) external{
        baseURIVal = newUri;
    }

    function baseURI() public view returns (string memory) {
        return _baseURI();
    }

    /**
 * @dev Base URI for computing {tokenURI}. If set, the resulting URI for each
     * token will be the concatenation of the `baseURI` and the `tokenId`. Empty
     * by default, can be overriden in child contracts.
     */
    function _baseURI() internal view override returns (string memory) {
        return baseURIVal;
    }

    function exists(uint256 tokenId) public view returns (bool) {
        return _exists(tokenId);
    }

    function mint(address to, uint256 tokenId) public {
        _mint(to, tokenId);
    }

    function safeMint(address to, uint256 tokenId) public {
        _safeMint(to, tokenId);
    }

    function safeMint(
        address to,
        uint256 tokenId,
        bytes memory _data
    ) public {
        _safeMint(to, tokenId, _data);
    }

    function burn(uint256 tokenId) public {
        _burn(tokenId);
    }

    function setPixInfo(uint _tokenId, PIXSize _size, PIXCategory _tier, uint _pixId) external {
        require(ownerOf(_tokenId) == msg.sender, "Dont meme other peoples pix bro");
        info[_tokenId].size = _size;
        info[_tokenId].category = _tier;
        info[_tokenId].pixId = _pixId;
    }


    function isTerritory(uint256 tokenId) external view returns (bool) {
        return info[tokenId].size != PIXSize.Pix;
    }

    function pixesInLand(uint256[] calldata tokenIds) external view returns (bool){
        return false;
    }

    function safeMint(address to, PIXInfo memory info) external{
        revert("Please use the other safemints and set info manually");
    }

    function lastTokenId() external view returns (uint256){
        revert("Not implemented in mock");
        return 0;
    }

    function getTier(uint256 tokenId) external view returns (uint256){
        revert("Not implemented in mock");
        return 0;
    }

    function getInfo(uint256 tokenId) external view returns (PIXInfo memory){
        return info[tokenId];
    }
}