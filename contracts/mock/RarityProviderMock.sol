pragma solidity ^0.8.0;

import "../mission_control/IPIXRarityProvider.sol";
import "../interfaces/IPIX.sol";
import "hardhat/console.sol";

contract RarityProviderMock is IPIXRarityProvider{

    mapping(uint => uint) rarities;
    mapping(uint => uint) sizes;

    function setRarity(uint _tokenId, uint _rarity) external {
        rarities[_tokenId] = _rarity;
    }

    function setSize(uint _tokenId, uint _size) external {
        sizes[_tokenId] = _size;
    }

    function getPIXRarity(uint _tokenId) external override returns (uint){
        return rarities[_tokenId];
    }
    function getInfo(uint256 _tokenId) external view returns (IPIX.PIXInfo memory ret){
        ret.category = IPIX.PIXCategory(rarities[_tokenId]);
        ret.size = IPIX.PIXSize(sizes[_tokenId]);
    }

}
