pragma solidity ^0.8.0;

import "../gravity_grade/IGravityGrade.sol";
import "../interfaces/ITrustedMintable.sol";
import "./ERC1155Mock.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";

contract GravityGradeMock is IGravityGrade, ITrustedMintable, ERC1155Upgradeable {
    function setTokenUri(uint256 _tokenId, string memory _uri) external override {}

    function createNewSale(
        uint256 _tokenId,
        uint256 _salePrice,
        uint256 _totalSupplyAmountToSell,
        uint256 _userCap,
        address _defaultCurrency,
        bool _profitState
    ) external override returns (uint256 saleId) {}

    function setSaleState(uint256 _saleId, bool _paused) external override {}

    function modifySale(
        uint256 _saleId,
        uint256 _salePrice,
        uint256 _totalSupplyAmountToSell,
        uint256 _userCap,
        address _defaultCurrency,
        bool _profitState
    ) external override {}

    function addBulkDiscount(
        uint256 _saleId,
        uint256 _breakpoint,
        uint256 _basisPoints
    ) external override {}

    function addOwnershipDiscount(uint256 _saleId, OwnershipRebate calldata _info)
        external
        override
    {}

    function deleteSale(uint256 _saleId) external override {}

    function setAllowedPaymentCurrencies(uint256 _saleId, address[] calldata _currencyAddresses)
        external
        override
    {}

    function setSwapManager(address _swapManager) external override {}

    function setOracleManager(address _oracleManager) external override {}

    function setModerator(address _moderatorAddress) external override {}

    function setTrusted(address _trusted, bool _isTrusted) external override {}

    function withdraw(address _walletAddress, address _currency) external payable override {}

    function setFeeWalletsAndPercentages(
        address[] calldata _walletAddresses,
        uint256[] calldata _feeBps
    ) external override {}

    function buyPacks(
        uint256 _saleId,
        uint256 _numPurchases,
        uint256 _tokenId,
        address _tokenAddress,
        address _currency
    ) external payable override {}

    function airdrop(
        address[] calldata _recipients,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override {
        for (uint256 i; i < _recipients.length; i++) {
            _mint(_recipients[i], _tokenIds[i], _amounts[i], "");
        }
    }

    function burn(
        address _from,
        uint256 tokenId,
        uint256 amount
    ) external override {
        _burn(_from, tokenId, amount);
    }

    function trustedMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) external {
        _mint(_to, _tokenId, _amount, "");
    }

    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external {}
}
