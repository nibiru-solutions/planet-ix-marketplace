// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "../interfaces/ITrustedMintable.sol";
import "../mission_control/IAssetManager.sol";

/**
 * @title ERC1155Mock
 * This mock just publicizes internal functions for testing purposes
 */
contract ERC1155MockAssetManager is ERC1155, IAssetManager, ITrustedMintable {
    constructor(string memory uri) ERC1155(uri) {}

    mapping(address => bool) trusted;
    mapping(uint256 => string) private s_uris;

    modifier trustedOnly() {
        require(trusted[msg.sender], "Non trusted sender");
        _;
    }

    function setTrusted(address _trusted, bool _isTrusted) external {
        trusted[_trusted] = _isTrusted;
    }

    function setURI(string memory newuri) public {
        _setURI(newuri);
    }

    function mint(
        address to,
        uint256 id,
        uint256 value
    ) public {
        _mint(to, id, value, "");
    }

    function mintBatch(
        address to,
        uint256[] memory ids,
        uint256[] memory values,
        bytes memory data
    ) public {
        _mintBatch(to, ids, values, data);
    }

    function burn(
        address owner,
        uint256 id,
        uint256 value
    ) public {
        _burn(owner, id, value);
    }

    function burnBatch(
        address owner,
        uint256[] memory ids,
        uint256[] memory values
    ) public {
        _burnBatch(owner, ids, values);
    }

    function trustedMint(
        address _to,
        uint256 _tokenId,
        uint256 _amount
    ) external override(ITrustedMintable, IAssetManager) trustedOnly {
        _mint(_to, _tokenId, _amount, "");
    }

    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override(ITrustedMintable, IAssetManager) trustedOnly {
        _mintBatch(_to, _tokenIds, _amounts, "");
    }

    function trustedBurn(
        address _from,
        uint256 _tokenId,
        uint256 _amount
    ) external override trustedOnly {
        _burn(_from, _tokenId, _amount);
    }

    function trustedBatchBurn(
        address _from,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external override trustedOnly {
        _burnBatch(_from, _tokenIds, _amounts);
    }

    function totalSupply(uint256 id) external pure returns (uint256) {
        return 9000;
    }
}
