pragma solidity ^0.8.0;

import "../util/ConditionalProvider.sol";

contract MockConditionalProvider2 is ConditionalProvider {
    mapping(address => bool) isTrue;
    function setTrue(address _person, bool _isTrue) external{
        isTrue[_person] = _isTrue;

    }
    function isEligible(address _address) external view returns (bool _isEligible) {
        _isEligible = isTrue[_address];
    }
}
