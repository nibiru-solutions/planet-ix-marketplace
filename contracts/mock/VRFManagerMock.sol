pragma solidity ^0.8.0;
import "hardhat/console.sol";

interface IRNGConsumer {
    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords) external;
}

contract VRFManagerMock {
    mapping(uint => address) requests;
    uint requestId;
    uint seed;
    mapping(uint => uint) num_words;

    event NewRequest(uint id);

    function getRandomNumber(uint32 _num_words) external returns (uint256 _requestId) {
        requests[++requestId] = msg.sender;
        num_words[requestId] = _num_words;
        emit NewRequest(requestId);
        _requestId = requestId;
    }


    function fulfill(uint _requestId) external {
        uint256[] memory ret = new uint256[](num_words[_requestId]);
        for(uint i; i < num_words[_requestId]; i++){
            ret[i] = _uniform();
        }
        IRNGConsumer(requests[_requestId]).fulfillRandomWords(_requestId, ret);
    }

    function _uniform() internal returns (uint uniform_sample){
        uniform_sample = uint(keccak256(abi.encode(++seed)));
    }

    function setSeed(uint _seed) external {
        seed = _seed;
    }


}
