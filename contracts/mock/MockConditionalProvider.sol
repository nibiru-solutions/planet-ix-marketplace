pragma solidity ^0.8.0;

import "../util/ConditionalProvider.sol";

contract MockConditionalProvider is ConditionalProvider {
    function isEligible(address _address) external view returns (bool _isEligible) {
        _isEligible = false;
    }
}
