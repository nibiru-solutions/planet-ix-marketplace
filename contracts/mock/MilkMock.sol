// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "../interfaces/ITrustedMintable.sol";

// mock class using ERC20
// I ganked this from openzeppelin
contract MilkMock is ERC20, ITrustedMintable {
    mapping(address => bool) trusted;

    modifier trustedOnly() {
        require(trusted[msg.sender], "Non trusted sender");
        _;
    }

    function setTrusted(address _trusted, bool _isTrusted) external {
        trusted[_trusted] = _isTrusted;
    }

    constructor(string memory name, string memory symbol) payable ERC20(name, symbol) {}

    function mint(address account, uint256 amount) public {
        _mint(account, amount);
    }

    function burn(address account, uint256 amount) public {
        _burn(account, amount);
    }

    function burn(uint256 amount) public {
        _burn(msg.sender, amount);
    }

    function transferInternal(
        address from,
        address to,
        uint256 value
    ) public {
        _transfer(from, to, value);
    }

    function approveInternal(
        address owner,
        address spender,
        uint256 value
    ) public {
        _approve(owner, spender, value);
    }

    function trustedMint(
        address _to,
        uint256, // _tokenId,
        uint256 _amount
    ) external trustedOnly {
        _mint(_to, _amount);
    }

    function trustedBatchMint(
        address _to,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external trustedOnly {}
}
