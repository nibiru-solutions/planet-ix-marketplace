// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../gravity_grade/ISaleBonuses.sol";

contract BonusConsumerMock {
    ISaleBonuses saleBonus;

    constructor(ISaleBonuses _saleBonus) {
        saleBonus = _saleBonus;
    }

    function claimBonusReward(
        uint256 _id,
        uint32 _amount,
        bool _optInConditionals,
        address _recipient
    ) external {
        saleBonus.claimBonusReward(_id, _amount, _optInConditionals, _recipient);
    }
}
