pragma solidity ^0.8.0;

import "../interfaces/ISwapManager.sol";
import "../interfaces/IOracleManager.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "hardhat/console.sol";


contract SwapManagerMock is ISwapManager {


    function payMe() payable external {
        // This is really dumb but I couldn't get hardhat to transfer the funds through a normal transaction
    }


    function swap(
        address srcToken,
        address dstToken,
        uint256 amount,
        address destination
    ) external payable override {
        if(srcToken == address(0)){
            require(msg.value == amount, "mock swap manager: value wrong");
        }else{
            bool success = IERC20(srcToken).transferFrom(msg.sender, address(this), amount);
            require(success, "mock swap manager: Something broke when trying to transfer erc20s");
        }

        if(dstToken == address(0)){
            bool success2 = payable(msg.sender).send(amount);
            require(success2, "mock swap manager: Couldnt send the cash for some reason");
        }else{
            bool success3 = IERC20(dstToken).transfer(destination, amount);
            require(success3, "mock swap manager: couldnt send the erc20 for some reason");
        }

    }
}
