// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../mission_control/IBaseLevel.sol";
interface IBaseLevelMock{
    function getOrderCapacity(address _trader, address _user) external view returns (uint _extraOrders);
}

contract MockBaseLevel is IBaseLevelMock{

    function getBaseLevel(address _user) public view returns (uint _level){
        return 6;
    }

    function getOrderCapacity(address _trader, address _user) override external view returns (uint _extraOrders){
        return 4;
    }

}