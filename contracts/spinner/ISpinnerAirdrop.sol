// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.8;


interface ISpinnerAirdrop {
    function registerVault(address _vault, address _ixtOutlet) external;

}