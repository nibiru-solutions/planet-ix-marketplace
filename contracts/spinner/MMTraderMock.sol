// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

interface IERC20Mock{
    function mint(address account, uint256 amount) external;
}


contract MMTraderMock is OwnableUpgradeable {

    address public receiver;
    address public emilCoin;
    address public usdt;

    function initialize(address _emilCoin, address _usdt) public initializer {
        __Ownable_init();
        emilCoin = _emilCoin;
        usdt = _usdt;
    }

    function sendConvertedToken(uint _amount) external {
        //IERC20(usdt).increaseAllowance(address(this), spender, IERC20(usdt).balanceOf(address(this)));
        IERC20(usdt).transferFrom(address(this), address(0), IERC20(usdt).balanceOf(address(this)));
        IERC20Mock(emilCoin).mint(address(this), _amount);
        IERC20(emilCoin).transferFrom(address(this), receiver, _amount);
    }

    function setReceiver(address _receiver) external {
        receiver = _receiver;
    }
}