// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.8;


interface ISpinner {
    function addSwapOrder(address _user, address _paymentToken, uint _price) external;
}