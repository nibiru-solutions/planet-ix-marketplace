// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@chainlink/contracts/src/v0.8/automation/interfaces/AutomationCompatibleInterface.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../interfaces/ISwapManager.sol";
import "./ISpinner.sol";

contract SpinnerInternalSwapTreasury is ISpinner, AutomationCompatibleInterface, OwnableUpgradeable {

    struct SwapOrder {
        address user;
        address paymentToken;
        uint price;
    }

    modifier onlyFluencr() {
        if (msg.sender != fluencr) revert("Not fluencr");
        _;
    }

    /** @notice Only admins can use this function.
     */
    modifier onlyAdmins {
        if (!admins[msg.sender]) revert ("Sender not admin");
        _;
    }

    function initialize(address _ixt) public initializer {
        __Ownable_init();
        ixt = _ixt;
        cutToUserBps = 10_000;
        admins[msg.sender] = true;
    }

    ISwapManager s_swapManager;
    address public ixt;
    address public fluencr;
    mapping(address => bool) public admins;
    uint constant public denominator = 10_000;
    uint public cutToUserBps;
    SwapOrder[] unProcessedSwapOrders;

    function addSwapOrder(address _user, address _paymentToken, uint _price) onlyFluencr external override {
        SwapOrder memory swapOrder;
        swapOrder.user = _user;
        swapOrder.paymentToken = _paymentToken;
        swapOrder.price = _price;
        unProcessedSwapOrders.push(swapOrder);
    }

    /** @notice Will be called on by every block to see if performUpkeep tx should be executed.
     *  @param upkeepNeeded return true if tx should be performed.
     */
    function checkUpkeep(
        bytes calldata /* checkData */
    )
    external
    view
    override
    returns (bool upkeepNeeded, bytes memory /* performData */)
    {
        upkeepNeeded = shouldPerformUpkeep();
    }
    /** @notice Check if batch should be processed.
    */ 
    function shouldPerformUpkeep() public view returns (bool){
        if (unProcessedSwapOrders.length > 0) {
            return true;
        }
        return false;
    }

    function performUpkeep(bytes calldata) external override {
        if (!shouldPerformUpkeep()) {
            revert("reveal criteria not met");
        }
        for (uint i; i < unProcessedSwapOrders.length && i<10; i++) {
            SwapOrder memory swapOrder = unProcessedSwapOrders[i];
            uint tokensToSwap = (swapOrder.price * cutToUserBps) / denominator;
            IERC20(swapOrder.paymentToken).approve(address(s_swapManager), tokensToSwap);
            s_swapManager.swap(swapOrder.paymentToken, ixt, tokensToSwap, swapOrder.user);
        }
        removeFirst10Elements();
    }

    function removeFirst10Elements() internal {
        uint256 length = unProcessedSwapOrders.length;

        if (length >= 10) {
            for (uint256 i = 0; i < length - 10; i++) {
                unProcessedSwapOrders[i] = unProcessedSwapOrders[i + 10];
            }
            for (uint256 i = 0; i < 10; i++) {
                unProcessedSwapOrders.pop();
            }
        }
        else {
            delete unProcessedSwapOrders;
        }
    }

    function setBpsSplits(uint _cutToUserBps) external onlyAdmins {
        if (_cutToUserBps > denominator) revert("Value must be between 0 and 10_000");
        cutToUserBps = _cutToUserBps;
    }

    function setAdmin(address _admin, bool _trusted) external onlyOwner {
        admins[_admin] = _trusted;
    }

    function setFluencr(address _fluencr) external onlyOwner {
        fluencr = _fluencr;
    }

    function setSwapManager(address _swapManager) external onlyOwner {
        s_swapManager = ISwapManager(_swapManager);
    }

    function getCutToUserBps() external view returns (uint){
        return cutToUserBps;
    }

    function getUnprocessedOrders() external view returns (SwapOrder[] memory){
        return unProcessedSwapOrders;
    }

    function withdraw(address _token, uint _amount) external onlyAdmins {
        IERC20(_token).transfer(msg.sender, _amount);
    }
}
