// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@chainlink/contracts/src/v0.8/automation/interfaces/AutomationCompatibleInterface.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./ISpinnerTreasury.sol";
import "./ISpinnerAirdrop.sol";
import "../interfaces/IOracleManager.sol";


contract SpinnerAirdrop is ISpinnerAirdrop, AutomationCompatibleInterface, OwnableUpgradeable {

    function initialize(address _ixt, address _usdt, address _spinnerTreasury) public initializer {
        __Ownable_init();
        ixt = _ixt;
        usdt = _usdt;
        spinnerTreasury = _spinnerTreasury;
    }

    IOracleManager s_oracle;
    address ixtOutlet;
    address vault;
    address ixt;
    address usdt;
    address spinnerTreasury;

    modifier onlySpinnerTreasury{
        if (msg.sender != spinnerTreasury) revert("Not spinner treasury");
        //add moderators...
        _;
    }

    function registerVault(address _vault, address _outlet) external override onlySpinnerTreasury {
        ixtOutlet = _outlet;
        vault = _vault;
    }

    /** @notice Will be called on by every block to see if performUpkeep tx should be executed.
     *  @param upkeepNeeded return true if tx should be performed.
     */
    function checkUpkeep(
        bytes calldata /* checkData */
    )
    external
    view
    override
    returns (bool upkeepNeeded, bytes memory /* performData */)
    {
        upkeepNeeded = shouldPerformUpkeep();
    }
    /** @notice Check if batch should be processed.
    */
    function shouldPerformUpkeep() public view returns (bool){
        //todo fix this ugly shit
        uint expectedIxt = s_oracle.getAmountOutView(usdt, ixt, 1);
        ISpinnerTreasury.SwapOrder memory swapOrder = ISpinnerTreasury(spinnerTreasury).getFirstSpinnerOrder();
        if (swapOrder.pending &&
            IERC20(ixt).balanceOf(ixtOutlet) > expectedIxt * (((swapOrder.totalAmount * ISpinnerTreasury(spinnerTreasury).getCutToUserBps()) / 10_000) / 2) &&
            IERC20(usdt).balanceOf(vault) < (((swapOrder.totalAmount * ISpinnerTreasury(spinnerTreasury).getCutToUserBps()) / 10_000)  / 10) * 1_000_000
        ) {
            return true;
        }

        return false;
    }

    function performUpkeep(bytes calldata) external override {

        if (!shouldPerformUpkeep()) {
            revert("reveal criteria not met");
        }

        uint outletBalance = IERC20(ixt).balanceOf(ixtOutlet);
        ISpinnerTreasury.SwapOrder memory swapOrder = ISpinnerTreasury(spinnerTreasury).getFirstSpinnerOrder();

        for (uint i; i < swapOrder.users.length; i++) {
            require(IERC20(ixt).transferFrom(ixtOutlet, swapOrder.users[i], (outletBalance * swapOrder.amounts[i]) / swapOrder.totalAmount), "tried to transfer more ixt than balance");
        }
        ISpinnerTreasury(spinnerTreasury).removePendingSwapOrder();
    }
}