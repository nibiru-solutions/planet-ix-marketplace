// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./ISpinner.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";


contract MockFluencrSale is OwnableUpgradeable{

    function initialize(address _spinnerTreasury) public initializer {
        __Ownable_init();
        spinnerTreasury = _spinnerTreasury;
    }
    address spinnerTreasury;
    function callSwapOrder(address _buyer, uint _price, address _token) external{
        IERC20(_token).transfer(spinnerTreasury, _price);
        ISpinner(spinnerTreasury).addSwapOrder(_buyer, _token, _price);
    }
}

