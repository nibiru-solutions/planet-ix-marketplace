// SPDX-License-Identifier: MIT
pragma solidity ^0.8.8;

import "@chainlink/contracts/src/v0.8/automation/interfaces/AutomationCompatibleInterface.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./ISpinnerTreasury.sol";
import "./ISpinnerAirdrop.sol";
import "../interfaces/IOracleManager.sol";

contract SpinnerTreasury is ISpinnerTreasury, AutomationCompatibleInterface, OwnableUpgradeable {

    modifier onlyFluencr() {
        if (msg.sender != fluencr) revert("Not fluencr");
        _;
    }
    modifier onlySpinnerAirdrop() {
        if (msg.sender != spinnerAirdrop) revert("Not fluencr");
        _;
    }

    /** @notice Only admins can use this function.
     */
    modifier onlyAdmins {
        if (!admins[msg.sender]) revert ("Sender not admin");
        _;
    }

    function initialize(address _ixt, address _usdt) public initializer {
        __Ownable_init();
        ixt = _ixt;
        usdt = _usdt;
        cutToUserBps = 8000;
        interval = 60;
        first = 1;
        last = 0;
        admins[msg.sender] = true;
    }

    address vault;
    uint interval;

    address public usdt;
    address public ixt;
    address spinnerAirdrop;
    address fluencr;
    uint last;
    uint first;
    mapping(address => bool) public admins;

    mapping(uint => SwapOrder) swapOrderQueue; // queueIndex to unprocessed swapOrder.
    uint constant denominator = 10_000;
    uint cutToUserBps;


    // queue functions
    function enqueue(SwapOrder memory data) internal {
        last += 1;
        swapOrderQueue[last] = data;
    }

    function dequeue() internal {
        require(last >= first);
        // non-empty queue
        first += 1;
    }

    function queueLength() public view returns (uint) {
        return 1 + last - first;
    }

    function addSwapOrder(address _user, uint _usdtAmount) onlyFluencr external override {
        //todo add a max batch size limit because of tx gaslimit in performUpkeep in SpinnerAirdrop.
        if (!swapOrderQueue[last].pending && last >= first) {
            swapOrderQueue[last].users.push(_user);
            swapOrderQueue[last].amounts.push(_usdtAmount);
            swapOrderQueue[last].totalAmount = swapOrderQueue[last].totalAmount + _usdtAmount;
        }
        else {
            SwapOrder memory swapOrder;
            swapOrder.timestamp = block.timestamp;
            swapOrder.totalAmount = _usdtAmount;
            enqueue(swapOrder);
            swapOrderQueue[last].amounts.push(_usdtAmount);
            swapOrderQueue[last].users.push(_user);
        }
    }

    /** @notice Will be called on by every block to see if performUpkeep tx should be executed.
     *  @param upkeepNeeded return true if tx should be performed.
     */
    function checkUpkeep(
        bytes calldata /* checkData */
    )
    external
    view
    override
    returns (bool upkeepNeeded, bytes memory /* performData */)
    {
        upkeepNeeded = shouldPerformUpkeep();
    }
    /** @notice Check if batch should be processed.
    */
    function shouldPerformUpkeep() public view returns (bool){
        if (!swapOrderQueue[first].pending && last >= first && block.timestamp - swapOrderQueue[first].timestamp > interval) {
            return true;
        }
        return false;
    }

    function performUpkeep(bytes calldata) external override {
        if (!shouldPerformUpkeep()) {
            revert("reveal criteria not met");
        }
        swapOrderQueue[first].pending = true;
        uint usdtToSwap = (swapOrderQueue[first].totalAmount * cutToUserBps) / denominator;
        require(IERC20(usdt).transfer(vault, usdtToSwap), "not enough usdt");
    }

    function removePendingSwapOrder() external override onlySpinnerAirdrop {
        dequeue();
    }

    function registerVault(address _vault, address _ixtOutlet) external override onlyOwner {
        vault = _vault;
        ISpinnerAirdrop(spinnerAirdrop).registerVault(_vault, _ixtOutlet);
        //may change this...
    }

    function setBpsSplits(uint _cutToUserBps) external override onlyAdmins {
        if (_cutToUserBps > denominator) revert("Value must be between 0 and 10_000");
        cutToUserBps = _cutToUserBps;
    }

    function setAdmin(address _admin, bool _trusted) external onlyOwner {
        admins[_admin] = _trusted;
    }

    function setFluencr(address _fluencr) external onlyOwner {
        fluencr = _fluencr;
    }

    function setSpinnerAirdrop(address _spinnerAirdrop) external onlyOwner {
        spinnerAirdrop = _spinnerAirdrop;
    }

    function getFirstSpinnerOrder() external view returns (SwapOrder memory){
        return swapOrderQueue[first];
    }

    function getCutToUserBps() override external view returns (uint){
        return cutToUserBps;
    }

}