// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.8;


interface ISpinnerTreasury {
    struct SwapOrder {
        uint timestamp;
        bool pending;
        address[] users;
        uint[] amounts;
        uint totalAmount;
    }

    function removePendingSwapOrder() external;

    function addSwapOrder(address _user, uint _usdtAmount) external;

    function registerVault(address _vault, address _ixtOutlet) external;

    function setBpsSplits(uint _cutToUserBps) external;

    function getFirstSpinnerOrder() external view returns (SwapOrder memory);

    function getCutToUserBps() external view returns (uint);
}