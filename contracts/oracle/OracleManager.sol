// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "../interfaces/IOracle.sol";
import "../interfaces/IOracleManager.sol";

/**
* @title Implementation of IOracleManager
*/
contract OracleManager is IOracleManager, OwnableUpgradeable {
    /**
    * @notice Event emitted when an oracle is registered
    * @param token0 Address of the first token the oracle is responsible for
    * @param token1 Address of the token the oracle is responsible for
    * @param oracle Address to the oracle
    */
    event OracleRegistered(address indexed token0, address indexed token1, address indexed oracle);
    /**
    * @notice Event emitted when an oracle is removed
    * @param token0 The first token the oracle is responsible for
    * @param token1 The second token the oracle is responsible for
    */
    event OracleRemoved(address indexed token0, address indexed token1);
    /**
    * @notice Event emitted when a stable is registered
    * @param token0 The first token the stable is responsible for
    * @param token1 The second token the stable is responsible for
    */
    event StableRegistered(address indexed token0, address indexed token1);
    /**
    * @notice Event emitted when a stable is removed
    * @param token0 The first token the stable is responsible for
    * @param token1 The second token the stable is responsible for
    */
    event StableRemoved(address indexed token0, address indexed token1);

    mapping(address => mapping(address => address)) public oracles;
    mapping(address => mapping(address => bool)) public stables;

    function initialize() external initializer {
        __Ownable_init();
    }

    /**
    * @notice Registers an oracle between the given addresses
    * @param token0 Address of the first token the oracle is responsible for
    * @param token1 Address of the token the oracle is responsible for
    * @param oracle Address to the oracle
    */
    function registerOracle(
        address token0,
        address token1,
        address oracle
    ) external onlyOwner {
        // address(0) => ETH
        require(token0 != token1, "invalid tokens");

        (address tokenA, address tokenB) = IOracle(oracle).tokens();
        if (tokenA == token0) {
            require(tokenB == token1, "token and oracle not match");
        } else if (tokenA == token1) {
            require(tokenB == token0, "token and oracle not match");
        } else {
            revert("token and oracle not match");
        }
        oracles[token0][token1] = oracle;
        oracles[token1][token0] = oracle;

        emit OracleRegistered(token0, token1, oracle);
    }

    /**
    * @notice Removes oracle between tokens
    * @param token0 Address of the first token the oracle is responsible for
    * @param token1 Address of the token the oracle is responsible for
    */
    function removeOracle(address token0, address token1) external onlyOwner {
        require(oracles[token0][token1] != address(0), "no oracle");

        delete oracles[token0][token1];
        delete oracles[token1][token0];

        emit OracleRemoved(token0, token1);
    }

    /// @dev What is this exactly?
    function registerStable(address token0, address token1) external onlyOwner {
        // address(0) => ETH
        require(token0 != token1, "invalid tokens");
        stables[token0][token1] = true;
        stables[token1][token0] = true;

        emit StableRegistered(token0, token1);
    }

    /// @dev What is this exactly?
    function removeStable(address token0, address token1) external onlyOwner {
        require(stables[token0][token1] == true, "no stable");

        delete stables[token0][token1];
        delete stables[token1][token0];

        emit StableRemoved(token0, token1);
    }

    /**
    * @inheritdoc IOracleManager
    */
    function getAmountOut(
        address srcToken,
        address dstToken,
        uint256 amountIn
    ) external override returns (uint256) {
        if (stables[srcToken][dstToken]) {
            return amountIn;
        }
        IOracle oracle = IOracle(oracles[srcToken][dstToken]);

        return oracle.getAmountOut(srcToken, amountIn);
    }

    /**
    * @inheritdoc IOracleManager
    */
    function getAmountOutView(
        address srcToken,
        address dstToken,
        uint256 amountIn
    ) external override view returns (uint256) {
        if (stables[srcToken][dstToken]) {
            return amountIn;
        }
        IOracle oracle = IOracle(oracles[srcToken][dstToken]);

        return oracle.getAmountOutView(srcToken, amountIn);
    }
}
