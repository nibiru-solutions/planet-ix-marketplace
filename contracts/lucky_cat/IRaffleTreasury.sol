// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

/// @title Interface for the Lucky cat raffle 3.0
/// @dev Quasi duplicate interface exist as IPIXCatRaffle.sol
interface IRaffleTreasury {
    function givePrize(
        address _winner,
        address _prize,
        uint256 _prizeId,
        uint256 _prizeAmount
    ) external;

    function giveBatchPrize(address[] memory winners, uint256 prizeId) external;
}
