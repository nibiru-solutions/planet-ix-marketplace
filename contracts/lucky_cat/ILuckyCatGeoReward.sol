// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {ISuperToken} from "@superfluid-finance/ethereum-contracts/contracts/interfaces/superfluid/ISuperfluid.sol";

interface ILuckyCatGeoReward {
    function safeMint(address to, int96 _flowRate) external;
    function trasferAGold(ISuperToken token, address to, uint256 _amount) external;
}
