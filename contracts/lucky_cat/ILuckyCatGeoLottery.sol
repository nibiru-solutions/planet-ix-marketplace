// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "../rover/VRFCoordinatorV2Interface.sol";

error LuckyCatGeoLottery__Unauthorized(address user);
error LuckyCatGeoLottery__NoTerritoryOwned(address user);
error LuckyCatGeoLottery__NoTicketOwned(address user);

/**
 * @title ILuckyCatGeoLottery
 * @dev Interface for the LuckyCatGeoLottery contract.
 */

interface ILuckyCatGeoLottery {
    struct ClaimData {
        uint256 _totalUserEntries;
        uint256 _userAmountStreamed;
        uint256 _userStreamUpdatedAt;
        uint256 _lotteryStartTime;
        uint256 _lotteryEndTime;
        uint256 _userLotteryFlowRate;
        uint256 _rewardToBeStreamed;
        uint256 _streamedThisWeek;
    }

    struct FlowRateVariables {
        bytes16 x;
        bytes16 rewardEther;
        bytes16 term1;
        bytes16 term2;
        bytes16 term3;
        bytes16 term4;
        bytes16 exponent;
        bytes16 term5;
        bytes16 diff;
        bytes16 numerator;
        bytes16 denominator;
        bytes16 quotient;
        bytes16 timeInDays;
        uint256 timeInDaysUInt;
        bytes16 timeInSeconds;
        bytes16 rewardFlowRate;
    }

    event UserEntered(address _user, uint256 _lotteryId, uint256 _entries);
    event UserStreamStarted(address _user, int96 _flowRate, uint256 _timestamp);
    event UserStreamEnded(address _user, int96 _flowRate, uint256 _timestamp);
    event RewardClaimed(uint256 _lotteryId, address _user, uint256 _rewardFlow);
    event PoolToppedUp(uint256 _pool, uint256 _amount, uint256 _lotteryId);
    event UserStreamUpdated(address _user, int96 _newFlowRate, int96 _oldFlowRate, uint256 _timestamp);

    /**
     * @notice Enter the lottery by purchasing a certain number of entries.
     * @param _entries The number of entries to purchase.
     */
    function enterLottery(uint256 _entries) external;

    /**
     * @notice Notify the contract when a user's streaming of funds has started.
     * @param _user The address of the user whose streaming has started.
     * @param _flowRate The flow rate at which the funds are being streamed.
     */    
    function onStreamStarted(address _user, int96 _flowRate) external;

    /**
     * @notice Notify the contract when a user's streaming of funds has been updated.
     * @param _user The address of the user whose streaming has been updated.
     * @param _newFlowRate The new flow rate at which the funds are being streamed.
     * @param _oldFlowRate The old flow rate at which the funds were being streamed.
     */
    function onStreamUpdated(address _user, int96 _newFlowRate, int96 _oldFlowRate) external;

    /**
     * @notice Notify the contract when a user's streaming of funds has ended.
     * @param _user The address of the user whose streaming has ended.
     * @param _flowRate The flow rate at which the funds were being streamed.
     */
    function onStreamEnded(address _user, int96 _flowRate) external;

    /**
     * @notice Draw winners for the lottery.
     * @return _requestId The request ID of the draw.
     */
    function drawWinners() external returns (uint256 _requestId);

    /**
     * @notice Claim rewards for participating in the lottery.
     * @param _lotteryId The ID of the lottery.
     * @param merkleProof The Merkle proofs for claiming rewards.
     */
    function claimReward(uint256 _lotteryId, bytes32[][] calldata merkleProof) external;

    /**
     * @notice Set the Merkle root for a specific pool in a lottery.
     * @param _lotteryId The ID of the lottery.
     * @param _merkleRoot The Merkle root hash.
     * @param _pool The pool index.
     */
    function setMerkleRoot(uint256 _lotteryId, bytes32[] memory _merkleRoot, uint256[] memory _pool) external;

    /**
     * @notice Set the key hash used for generating random numbers.
     * @param _keyHash The new key hash.
     */
    function setKeyHash(bytes32 _keyHash) external;

    /**
     * @notice Set the subscription ID for VRF randomness requests.
     * @param _subscriptionId The new subscription ID.
     */
    function setSubscriptionId(uint64 _subscriptionId) external;

    /**
     * @notice Set the gas limit for VRF callback.
     * @param _callbackGasLimit The new callback gas limit.
     */
    function setCallbackGasLimit(uint32 _callbackGasLimit) external;

    /**
     * @notice Set the number of confirmations required for VRF randomness requests.
     * @param _requestConfirmations The new number of request confirmations.
     */
    function setRequestConfirmations(uint16 _requestConfirmations) external;

    /**
     * @notice Set the address of the VRF coordinator contract.
     * @param _coordinator The new VRF coordinator address.
     */
    function setCOORDINATOR(VRFCoordinatorV2Interface _coordinator) external;

    /**
     * @notice Set the ticket price for participating in the lottery.
     * @param newTicketPrice The new ticket price.
     */
    function setTicketPrice(uint256 newTicketPrice) external;

    /**
     * @notice Set the maximum number of entries a user can purchase.
     * @param newMaxEntries The new maximum number of entries.
     */
    function setMaxEntries(uint256 newMaxEntries) external;

    /**
     * @notice Start the lottery (unpause).
     */
    function start() external;

    /**
     * @notice Stop the lottery (pause).
     */
    function stop() external;
}