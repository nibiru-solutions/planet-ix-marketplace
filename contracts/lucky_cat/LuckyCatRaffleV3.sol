//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../libraries/VRFConsumerBaseV2Upgradable.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";

import "../rover/VRFCoordinatorV2Interface.sol";
import "./ICatRaffle_3_0.sol";
import "./IRaffleTreasury.sol";

import "../mission_control/IAssetManager.sol";

interface IVRFManager {
    function getRandomNumber(uint32 numWords) external returns (uint256 requestId);
}

interface IRNGConsumer {
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] calldata randomWords) external;
}

contract LuckyCatRaffleV3 is
    ICatRaffle_3_0,
    VRFConsumerBaseV2Upgradable,
    OwnableUpgradeable,
    PausableUpgradeable,
    IRNGConsumer
{
    /* ========== STATE VARIABLES ========== */

    uint256 public raffleId;
    mapping(uint256 => Raffle) public raffles;
    mapping(uint256 => uint256) public raffleSeed;
    mapping(address => mapping(uint256 => uint256)) public userRaffleEntries;
    mapping(uint256 => bool) public raffleDrawn;
    mapping(uint256 => uint256) private drawRequest;

    mapping(uint256 => uint256) public multiplier;
    mapping(uint256 => uint256) public shifter;

    mapping(uint256 => mapping(uint256 => bool)) public claimed;

    IERC1155 public landmark;
    IERC1155 public ticket;

    IRaffleTreasury public erc721Treasury;
    IRaffleTreasury public erc1155Treasury;

    uint64 public subscriptionId;
    bytes32 public keyHash;
    uint32 public callbackGasLimit;
    uint16 public requestConfirmations;
    VRFCoordinatorV2Interface public COORDINATOR;

    mapping(address => bool) public moderators;
    mapping(uint256 => uint256[]) public cumulativeEntries;
    mapping(uint256 => mapping(uint256 => address)) public userAtIndex;

    IVRFManager public vrfManager;

    modifier onlyGov() {
        if (msg.sender != owner() && !moderators[msg.sender]) revert ICatRaffle_3_0__Unauthorized();
        _;
    }

    modifier onlyOracle() {
        require(msg.sender == address(vrfManager), "sender not VRF manager");
        _;
    }

    /* ========== INITIALIZER ========== */

    function initialize(
        address _landmark,
        address _ticket,
        address _vrfCoordinator,
        bytes32 _keyHash,
        uint64 _subscriptionId
    ) external initializer {
        require(_landmark != address(0), "invalid landmark");
        require(_ticket != address(0), "invalid ticket");

        landmark = IERC1155(_landmark);
        ticket = IERC1155(_ticket);

        COORDINATOR = VRFCoordinatorV2Interface(_vrfCoordinator);
        keyHash = _keyHash;
        subscriptionId = _subscriptionId;

        callbackGasLimit = 2000000;
        requestConfirmations = 3;

        moderators[msg.sender] = true;

        __Ownable_init();
        __VRFConsumerBaseV2_init(_vrfCoordinator);
    }

    /// @inheritdoc ICatRaffle_3_0
    function enterRaffle(uint256 _raffleId, uint256 _numTickets) external whenNotPaused {
        if (raffleDrawn[_raffleId]) revert ICatRaffle_3_0__IsDrawn(_raffleId);
        if (raffles[_raffleId].startTime == 0) revert ICatRaffle_3_0__InvalidRaffle(_raffleId);
        if (raffles[_raffleId].startTime + raffles[_raffleId].duration < block.timestamp)
            revert ICatRaffle_3_0__InvalidRaffle(_raffleId);
        if (raffles[_raffleId].paused) revert ICatRaffle_3_0__Paused(_raffleId);
        if (_numTickets == 0) revert ICatRaffle_3_0__ZeroEntries();
        if (raffles[_raffleId].entries + _numTickets > raffles[_raffleId].totalEntries)
            revert ICatRaffle_3_0__TotalEntriesExceeded();
        if (_numTickets < raffles[_raffleId].userMinEntries)
            revert ICatRaffle_3_0__NotEnoughEntries();
        if (
            userRaffleEntries[msg.sender][_raffleId] + _numTickets >
            raffles[_raffleId].userMaxEntries
        ) revert ICatRaffle_3_0__MaxEntriesPerUserExceeded();
        if (block.timestamp < raffles[_raffleId].startTime)
            revert ICatRaffle_3_0__RaffleNotStartedYet();

        Raffle storage _raffle = raffles[_raffleId];

        cumulativeEntries[_raffleId].push(raffles[_raffleId].entries + _numTickets);
        userAtIndex[_raffleId][cumulativeEntries[_raffleId].length - 1] = msg.sender;

        unchecked {
            userRaffleEntries[msg.sender][_raffleId] += _numTickets;
            raffles[_raffleId].entries += _numTickets;
        }

        IAssetManager(address(ticket)).trustedBurn(msg.sender, _raffle.ticketId, _numTickets);
        emit RaffleEntered(_raffleId, msg.sender);
    }

    /// @inheritdoc ICatRaffle_3_0
    function drawWinners(uint256 _raffleId) external onlyGov returns (uint256 _requestId) {
        if (raffles[_raffleId].startTime == 0) revert ICatRaffle_3_0__InvalidRaffle(_raffleId);
        if (block.timestamp < raffles[_raffleId].startTime + raffles[_raffleId].duration)
            revert ICatRaffle_3_0__NotDrawable(_raffleId);
        if (raffleDrawn[_raffleId]) revert ICatRaffle_3_0__IsDrawn(_raffleId);

        raffles[_raffleId].isDrawn = true;

        if (raffles[_raffleId].entries > 0) {
            require(keyHash != bytes32(0), "Must have valid key hash");

            _requestId = vrfManager.getRandomNumber(1);

            drawRequest[_requestId] = _raffleId;
            emit RaffleDrawn(_raffleId);
        }
    }

    /// @inheritdoc ICatRaffle_3_0
    function claimRewards(uint256 _raffleId, uint256[] memory _prizeIds) external whenNotPaused {
        if (!raffleDrawn[_raffleId]) revert ICatRaffle_3_0__RaffleNotDrawn();
        if (raffles[_raffleId].prizeType == PrizeType.ERC1155) {
            _handle1155Prizes(_raffleId, _prizeIds);
        } else {
            for (uint i; i < _prizeIds.length; i++) {
                if (!claimed[_raffleId][_prizeIds[i]]) {
                    address winningAddress = getWinningAddress(_raffleId, _prizeIds[i]);

                    _givePrize(winningAddress, _raffleId, 1);
                    claimed[_raffleId][_prizeIds[i]] = true;

                    emit RaffleClaimed(_raffleId, _prizeIds[i], winningAddress);
                }
            }
        }
    }

    function _givePrize(address _player, uint256 _raffleId, uint256 _amount) internal {
        Raffle memory raffle = raffles[_raffleId];

        for (uint256 i; i < raffle.prizeIds.length; i++) {
            IRaffleTreasury(raffle.raffleTreasury).givePrize(
                _player,
                raffle.prizeAddress,
                raffle.prizeIds[i],
                raffle.prizeAmounts[i] * _amount
            );
        }
    }

    /// @inheritdoc ICatRaffle_3_0
    function registerRaffle(RaffleInput calldata r) external onlyGov returns (uint256 _raffleId) {
        if (r.ticketId != 16 && r.ticketId != 17 && r.ticketId != 18)
            revert ICatRaffle_3_0__InvalidTicketId(r.ticketId);
        if (r.prizeIds.length != r.prizeAmounts.length)
            revert ICatRaffle_3_0__MismatchedPrizeLengths();
        if (r.userMinEntries > r.userMaxEntries)
            revert ICatRaffle_3_0__InvalidUserEntryLimits(r.userMinEntries, r.userMaxEntries);
        if (r.userMinEntries == 0)
            revert ICatRaffle_3_0__ZeroUserEntries(r.userMinEntries, r.userMaxEntries);
        if (r.userMaxEntries == 0)
            revert ICatRaffle_3_0__ZeroUserEntries(r.userMinEntries, r.userMaxEntries);
        if (r.userMaxEntries > r.totalEntries)
            revert ICatRaffle_3_0__UserMaxExceedsTotal(r.userMaxEntries, r.totalEntries);
        if (r.userMinEntries > r.totalEntries)
            revert ICatRaffle_3_0__UserMinExceedsTotal(r.userMinEntries, r.totalEntries);
        if (r.totalEntries == 0) revert ICatRaffle_3_0__ZeroTotalEntries();

        unchecked {
            _raffleId = ++raffleId;
        }

        raffles[_raffleId] = Raffle({
            ticketId: r.ticketId,
            prizeAddress: r.prizeAddress,
            prizeType: r.prizeType,
            prizeIds: r.prizeIds,
            prizeAmounts: r.prizeAmounts,
            totalPrizes: r.totalPrizes,
            totalEntries: r.totalEntries,
            userMinEntries: r.userMinEntries,
            userMaxEntries: r.userMaxEntries,
            startTime: r.startTime == 0 ? block.timestamp : r.startTime,
            duration: r.duration,
            raffleTreasury: r.raffleTreasury,
            paused: true,
            isDrawn: false,
            raffleId: _raffleId,
            entries: 0
        });

        for (uint256 i; i < r.prizeIds.length; i++) {
            if (r.prizeType == ICatRaffle_3_0.PrizeType.ERC721) {
                IERC721(r.prizeAddress).safeTransferFrom(
                    msg.sender,
                    address(erc721Treasury),
                    r.prizeIds[i]
                );
            }
            if (r.prizeType == ICatRaffle_3_0.PrizeType.ERC1155) {
                IERC1155(r.prizeAddress).safeTransferFrom(
                    msg.sender,
                    address(erc1155Treasury),
                    r.prizeIds[i],
                    r.prizeAmounts[i] * r.totalPrizes,
                    ""
                );
            }
        }

        emit RaffleRegistered(_raffleId);
    }

    /// @inheritdoc ICatRaffle_3_0
    function updateRaffle(uint256 _raffleId, RaffleInput memory r) external onlyGov {
        if (r.ticketId != 16 && r.ticketId != 17 && r.ticketId != 18)
            revert ICatRaffle_3_0__InvalidTicketId(r.ticketId);
        if (r.prizeIds.length != r.prizeAmounts.length)
            revert ICatRaffle_3_0__MismatchedPrizeLengths();
        if (r.userMinEntries > r.userMaxEntries)
            revert ICatRaffle_3_0__InvalidUserEntryLimits(r.userMinEntries, r.userMaxEntries);
        if (r.userMinEntries == 0)
            revert ICatRaffle_3_0__ZeroUserEntries(r.userMinEntries, r.userMaxEntries);
        if (r.userMaxEntries == 0)
            revert ICatRaffle_3_0__ZeroUserEntries(r.userMinEntries, r.userMaxEntries);
        if (r.userMaxEntries > r.totalEntries)
            revert ICatRaffle_3_0__UserMaxExceedsTotal(r.userMaxEntries, r.totalEntries);
        if (r.userMinEntries > r.totalEntries)
            revert ICatRaffle_3_0__UserMinExceedsTotal(r.userMinEntries, r.totalEntries);
        if (r.totalEntries == 0) revert ICatRaffle_3_0__ZeroTotalEntries();
        if (raffles[_raffleId].startTime == 0) revert ICatRaffle_3_0__InvalidRaffle(_raffleId);

        Raffle storage _raffle = raffles[_raffleId];

        _raffle.ticketId = r.ticketId;
        _raffle.prizeAddress = r.prizeAddress;
        _raffle.prizeType = r.prizeType;
        _raffle.prizeIds = r.prizeIds;
        _raffle.prizeAmounts = r.prizeAmounts;
        _raffle.totalPrizes = r.totalPrizes;
        _raffle.totalEntries = r.totalEntries;
        _raffle.userMinEntries = r.userMinEntries;
        _raffle.userMaxEntries = r.userMaxEntries;
        _raffle.startTime = r.startTime;
        _raffle.duration = r.duration;
        _raffle.raffleTreasury = r.raffleTreasury;

        emit RaffleUpdated(_raffleId);
    }

    /// @inheritdoc ICatRaffle_3_0
    function setPaused(uint256 _raffleId, bool _isPaused) external onlyGov {
        if (raffles[_raffleId].startTime == 0) revert ICatRaffle_3_0__InvalidRaffle(_raffleId);

        raffles[_raffleId].paused = _isPaused;
        emit RaffleSetPaused(_raffleId, _isPaused);
    }

    function setERC721Treasury(address _treasury) external onlyGov {
        erc721Treasury = IRaffleTreasury(_treasury);
    }

    function setERC1155Treasury(address _treasury) external onlyGov {
        erc1155Treasury = IRaffleTreasury(_treasury);
    }

    function setKeyHash(bytes32 _keyHash) external onlyGov {
        keyHash = _keyHash;
    }

    function setSubscriptionId(uint64 _subscriptionId) external onlyGov {
        subscriptionId = _subscriptionId;
    }

    function setCallbackGasLimit(uint32 _callbackGasLimit) external onlyGov {
        callbackGasLimit = _callbackGasLimit;
    }

    function setRequestConfirmations(uint16 _requestConfirmations) external onlyGov {
        requestConfirmations = _requestConfirmations;
    }

    function setCOORDINATOR(VRFCoordinatorV2Interface _coordinator) external onlyGov {
        COORDINATOR = _coordinator;
    }

    /// @inheritdoc ICatRaffle_3_0
    function deleteRaffle(uint256 _raffleId) external onlyGov {
        if (raffles[_raffleId].startTime == 0) revert ICatRaffle_3_0__InvalidRaffle(_raffleId);

        delete raffles[_raffleId];
        emit RaffleDeleted(_raffleId);
    }

    /// @inheritdoc ICatRaffle_3_0
    function getRaffle(uint256 _raffleId) external view returns (Raffle memory raffle) {
        return raffles[_raffleId];
    }

    /// @inheritdoc ICatRaffle_3_0
    function getActiveRaffles() external view returns (Raffle[] memory _activeRaffles) {
        uint size;
        uint activeRaffleCount;
        for (uint256 i = 1; i <= raffleId; i++) {
            if (
                block.timestamp > raffles[i].startTime &&
                block.timestamp < (raffles[i].startTime + raffles[i].duration)
            ) size++;
        }

        _activeRaffles = new Raffle[](size);
        for (uint256 i = 1; i <= raffleId; i++) {
            if (
                block.timestamp > raffles[i].startTime &&
                block.timestamp < (raffles[i].startTime + raffles[i].duration)
            ) {
                _activeRaffles[activeRaffleCount] = raffles[i];
                ++activeRaffleCount;
            }
        }
    }

    /// @inheritdoc ICatRaffle_3_0
    function getPastRaffles() external view returns (Raffle[] memory _pastRaffles) {
        uint size;
        uint pastRaffleCount;
        for (uint256 i = 1; i <= raffleId; i++) {
            if (block.timestamp > raffles[i].startTime + raffles[i].duration) size++;
        }

        _pastRaffles = new Raffle[](size);
        for (uint256 i = 1; i <= raffleId; i++) {
            if (block.timestamp > raffles[i].startTime + raffles[i].duration) {
                _pastRaffles[pastRaffleCount] = raffles[i];
                ++pastRaffleCount;
            }
        }
    }

    /// @inheritdoc ICatRaffle_3_0
    function getUpcomingRaffles() external view returns (Raffle[] memory _upcomingRaffles) {
        uint size;
        uint upcomingRaffleCount;
        for (uint256 i = 1; i <= raffleId; i++) {
            if (block.timestamp < raffles[i].startTime) size++;
        }

        _upcomingRaffles = new Raffle[](size);
        for (uint256 i = 1; i <= raffleId; i++) {
            if (block.timestamp < raffles[i].startTime) {
                _upcomingRaffles[upcomingRaffleCount] = raffles[i];
                ++upcomingRaffleCount;
            }
        }
    }

    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords) internal override {}
    
    function fulfillRandomWordsRaw(uint256 requestId, uint256[] memory randomWords) external onlyOracle {
        uint256 _raffleId = drawRequest[requestId];
        raffleSeed[_raffleId] = randomWords[0];
        raffleDrawn[_raffleId] = true;

        multiplier[_raffleId] =
            (raffles[_raffleId].entries / 2) +
            (uint256(keccak256(abi.encode(randomWords[0], 1))) % (raffles[_raffleId].entries / 2));

        while (gcd(multiplier[_raffleId], raffles[_raffleId].entries) != 1) multiplier[_raffleId]++;

        shifter[_raffleId] =
            uint256(keccak256(abi.encode(randomWords[0], 2))) %
            raffles[_raffleId].entries;
    }

    function getWinningAddress(uint256 _raffleId, uint256 _prizeId) public view returns (address) {
        if (!raffleDrawn[_raffleId]) revert ICatRaffle_3_0__RaffleNotDrawn();
        Raffle memory _raffle = raffles[_raffleId];

        uint256 N = raffles[_raffleId].entries;
        uint256 K = _raffle.totalPrizes;

        if (K > N) K = N;
        if (_prizeId >= K) return address(0);

        uint256 denseIndex = _prizeId * (N / K);

        uint256 winningTicket = (denseIndex * multiplier[_raffleId] + shifter[_raffleId]) %
            raffles[_raffleId].entries;

        int256 winnerIndex = binarySearch(cumulativeEntries[_raffleId], winningTicket + 1);

        if (winnerIndex == -1) revert ICatRaffle_3_0__TicketNotFound(winningTicket, N, K);
        return userAtIndex[_raffleId][uint256(winnerIndex)];
    }

    function _handle1155Prizes(uint256 _raffleId, uint256[] memory _prizeIds) internal {
        if (!raffleDrawn[_raffleId]) revert ICatRaffle_3_0__RaffleNotDrawn();
        Raffle memory _raffle = raffles[_raffleId];

        uint256 N = raffles[_raffleId].entries;
        uint256 K = _raffle.totalPrizes;

        if (K > N) K = N;

        address lastPlayer;
        uint256 tempPrizeAmount;

        uint256 denseIndex;
        uint256 winningTicket;
        int256 winnerIndex;
        for (uint256 i = 0; i < _prizeIds.length; i++) {
            if (claimed[_raffleId][_prizeIds[i]]) continue;
            if (_prizeIds[i] >= K) continue;
            denseIndex = _prizeIds[i] * (N / K);
            winningTicket =
                (denseIndex * multiplier[_raffleId] + shifter[_raffleId]) %
                raffles[_raffleId].entries;

            winnerIndex = binarySearch(cumulativeEntries[_raffleId], winningTicket + 1);
            if (winnerIndex == -1) revert ICatRaffle_3_0__TicketNotFound(winningTicket, N, K);

            address winningAddress = userAtIndex[_raffleId][uint256(winnerIndex)];
            if (i == 0 || lastPlayer == winningAddress) {
                lastPlayer = winningAddress;
                tempPrizeAmount++;
            } else {
                _givePrize(lastPlayer, _raffleId, tempPrizeAmount);
                lastPlayer = winningAddress;
                tempPrizeAmount = 1;
            }
            claimed[_raffleId][_prizeIds[i]] = true;
            emit RaffleClaimed(_raffleId, _prizeIds[i], lastPlayer);
        }
        _givePrize(lastPlayer, _raffleId, tempPrizeAmount);
    }

    function getAllWinningAddresses(uint256 _raffleId) public view returns (address[] memory) {
        address[] memory winningAddresses = new address[](raffles[_raffleId].totalPrizes);

        for (uint256 i; i < winningAddresses.length; i++) {
            winningAddresses[i] = getWinningAddress(_raffleId, i);
        }
        return winningAddresses;
    }

    function getCumulativeEntries(uint _raffleId) external view returns (uint[] memory) {
        return cumulativeEntries[_raffleId];
    }

    /// @inheritdoc ICatRaffle_3_0
    function canDraw(uint256 _raffleId) external view returns (bool _canDraw) {
        if (raffles[_raffleId].startTime == 0) return false;
        if (raffles[_raffleId].startTime + raffles[_raffleId].duration > block.timestamp)
            return false;
        if (raffleDrawn[_raffleId]) return false;

        _canDraw = true;
    }

    function setTicket(address _ticket) external onlyGov {
        require(_ticket != address(0), "LUCKY_CAT: ADDRESS_ZERO");
        ticket = IERC1155(_ticket);
    }

    function setModerator(address _mod, bool _isMod) external onlyOwner {
        moderators[_mod] = _isMod;
    }

    function setVrfManager(address _vrfManager) external onlyOwner {
        vrfManager = IVRFManager(_vrfManager);
    }

    function setUserAtIndex(
        uint256 _raffleId,
        uint256[] calldata _index,
        address[] calldata _players
    ) external onlyOwner {
        for (uint256 i; i < _players.length; i++) {
            userAtIndex[_raffleId][_index[i]] = _players[i];
        }
    }

    function gcd(uint256 a, uint256 b) public pure returns (uint256) {
        // If a is 0, the GCD is b.
        if (a == 0) {
            return b;
        }

        // While b is not 0, keep checking the remainder of a divided by b, and assign the value of b to a,
        // and the remainder of a divided by b to b.
        while (b != 0) {
            uint256 r = a % b;
            a = b;
            b = r;
        }

        // When b is 0, the GCD is a.
        return a;
    }

    function binarySearch(uint256[] memory arr, uint256 target) private pure returns (int256) {
        int256 left = 0;
        int256 right = int256(arr.length) - 1;
        int256 mid;

        while (left <= right) {
            mid = left + (right - left) / 2;

            if (arr[uint256(mid)] == target) {
                return mid;
            } else if (arr[uint256(mid)] < target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        if (left == 0 && arr[0] > target) {
            return 0;
        }

        return left;
    }

    function start() external onlyGov {
        _unpause();
    }

    function stop() external onlyGov {
        _pause();
    }
}
