pragma solidity ^0.8.0;

interface IGeoLotteryStream {
    function getFlowRate(address superToken, address player) external view returns (int96);
}
