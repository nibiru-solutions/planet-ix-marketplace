//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "./IRaffleTreasury.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract LuckyCatERC1155Treasury is IRaffleTreasury, ERC1155HolderUpgradeable, OwnableUpgradeable {
    address luckyCat;
    mapping(address => bool) moderator;

    modifier onlyLuckyCat() {
        require(msg.sender == luckyCat, "ERC1155_TREASURY_SENDER_NOT_LUCKYCAT");
        _;
    }

    modifier onlyMod() {
        require(moderator[msg.sender], "ERC_721_TREASURY_NOT_MODERATOR");
        _;
    }

    function initialize() external initializer {
        __Ownable_init();
    }

    function givePrize(
        address _winner,
        address _prize,
        uint256 _prizeId,
        uint256 _prizeAmount
    ) external onlyLuckyCat {
        IERC1155Upgradeable(_prize).safeTransferFrom(
            address(this),
            _winner,
            _prizeId,
            _prizeAmount,
            ""
        );
    }

    function giveBatchPrize(address[] memory winners, uint256 prizeId) external onlyLuckyCat {}

    function setLuckyCat(address _luckyCat) external onlyOwner {
        luckyCat = _luckyCat;
    }

    function setModerator(address _mod, bool _isModerator) external onlyOwner {
        moderator[_mod] = _isModerator;
    }

    function withdrawMultipleERC1155(
        address _token,
        uint256[] memory _ids,
        uint256[] memory _amounts,
        address _receiver
    ) external onlyMod {
        require(_ids.length == _amounts.length, "ERC1155_TREASURY_INVALID_ARRAY_LENGTHS");
        IERC1155Upgradeable(_token).safeBatchTransferFrom(
            address(this),
            _receiver,
            _ids,
            _amounts,
            ""
        );
    }
}
