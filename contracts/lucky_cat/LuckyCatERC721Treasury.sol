//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "./IRaffleTreasury.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/utils/ERC721HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/IERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract LuckyCatERC721Treasury is IRaffleTreasury, ERC721HolderUpgradeable, OwnableUpgradeable {
    address luckyCat;
    mapping(address => bool) moderator;

    modifier onlyLuckyCat() {
        require(msg.sender == luckyCat, "ERC721_TREASURY_SENDER_NOT_LUCKYCAT");
        _;
    }

    modifier onlyMod() {
        require(moderator[msg.sender], "ERC_721_TREASURY_NOT_MODERATOR");
        _;
    }

    function initialize() external initializer {
        __Ownable_init();
    }

    function givePrize(
        address _winner,
        address _prize,
        uint256 _prizeId,
        uint256 _prizeAmount
    ) external onlyLuckyCat {
        IERC721Upgradeable(_prize).safeTransferFrom(address(this), _winner, _prizeId);
    }

    function giveBatchPrize(address[] memory winners, uint256 prizeId) external onlyLuckyCat {}

    function setLuckyCat(address _luckyCat) external onlyOwner {
        luckyCat = _luckyCat;
    }

    function setModerator(address _mod, bool _isModerator) external onlyOwner {
        moderator[_mod] = _isModerator;
    }

    function withdrawMultipleERC721(
        address[] memory _tokens,
        uint256[] memory _tokenIds,
        address _receiver
    ) external onlyMod {
        require(_tokens.length == _tokenIds.length, "ERC721_TREASURY_INVALID_ARRAY_LENGTHS");
        for (uint256 i = 0; i < _tokens.length; i++) {
            IERC721Upgradeable(_tokens[i]).safeTransferFrom(address(this), _receiver, _tokenIds[i]);
        }
    }
}
