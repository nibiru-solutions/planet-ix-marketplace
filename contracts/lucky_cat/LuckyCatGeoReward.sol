// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import {SuperTokenV1Library} from "@superfluid-finance/ethereum-contracts/contracts/apps/SuperTokenV1Library.sol";
import {ISuperToken, ISuperfluid} from "@superfluid-finance/ethereum-contracts/contracts/interfaces/superfluid/ISuperfluid.sol";

contract LuckyCatGeoReward is Initializable, ERC721Upgradeable, OwnableUpgradeable {
    using SuperTokenV1Library for ISuperToken;

    ISuperToken public aGold;
    uint256 public tokenId;

    mapping(uint256 => int96) public tokenStream;
    address public geoLottery;
    string public tokenURI;
    mapping(address => bool) public moderators;

    event RewardInitiated(address _receiver, int96 _flowRate, uint256 _tokenId);

    modifier onlyGeoLottery() {
        require(msg.sender == geoLottery, "SENDER NOT GEO LOTTERY");
        _;
    }

    modifier onlyGov() {
        require (msg.sender == owner() || moderators[msg.sender], "LuckyCatGeoReward: Only Gov");
        _;
    }

    function initialize() public initializer {
        __ERC721_init("LuckyCatGeoReward", "GEO");
        __Ownable_init();
    }

    function safeMint(address to, int96 _flowRate) external onlyGeoLottery {
        _giveReward(to, _flowRate);
    }

    function trasferAGold(ISuperToken token, address _to, uint256 _amount) external onlyGeoLottery {
        require(address(token) == address(aGold), "LuckyCatGeoReward: Invalid token");

        uint256 balance = token.balanceOf(address(this));
        require(balance >= _amount, "LuckyCatGeoReward: Insufficient balance");

        bool success = token.transfer(_to, _amount);
        require(success, "LuckyCatGeoReward: Withdrawal failed");
    }

    function _beforeTokenTransfer(
        address from, // from
        address to,
        uint256 _tokenId // tokenId
    ) internal override {
        _changeReceiver(from, to, _tokenId);
    }

    function _changeReceiver(address oldReceiver, address newReceiver, uint256 _tokenId) internal {
        int96 flowRateOldReceiver = aGold.getFlowRate(address(this), oldReceiver);
        int96 flowRateNewReceiver = aGold.getFlowRate(address(this), newReceiver);
        int96 tokenFlowRate = tokenStream[_tokenId];

        if (flowRateOldReceiver > tokenFlowRate) aGold.updateFlow(oldReceiver, flowRateOldReceiver - tokenFlowRate);
        if (flowRateOldReceiver == tokenFlowRate) aGold.deleteFlow(address(this), oldReceiver);

        if (flowRateNewReceiver == 0 && newReceiver != address(0)) aGold.createFlow(newReceiver, tokenFlowRate);
        if (flowRateNewReceiver != 0 && newReceiver != address(0)) aGold.updateFlow(newReceiver, flowRateNewReceiver + tokenFlowRate);
    }

    function _giveReward(address to, int96 _flowRate) internal {
        tokenStream[tokenId] = _flowRate;
        _safeMint(to, tokenId);
        emit RewardInitiated(to, _flowRate, tokenId);
        ++tokenId;
    }

    function _baseURI() internal view virtual override returns (string memory) {
        return tokenURI;
    }

    function adminMint(address to, int96 _flowRate) external onlyOwner {
        _giveReward(to, _flowRate);
    }

    function setAGOLD(ISuperToken _aGold) external onlyOwner {
        aGold = _aGold;
    }

    function setGeoLottery(address _geoLottery) external onlyOwner {
        geoLottery = _geoLottery;
    }

    function setBaseURI(string memory _tokenURI) external onlyOwner {
        tokenURI = _tokenURI;
    }

    function setModerator(address _user, bool _isModerator) external onlyOwner {
        moderators[_user] = _isModerator;
    }

    function withdraw(ISuperToken token, uint256 amount) external onlyOwner {
        require(address(token) == address(aGold), "LuckyCatGeoReward: Invalid token");

        uint256 balance = token.balanceOf(address(this));
        require(balance >= amount, "LuckyCatGeoReward: Insufficient balance");

        bool success = token.transfer(owner(), amount);
        require(success, "LuckyCatGeoReward: Withdrawal failed");
    }

    function terminateStream(uint256 _tokenId) external onlyGov {
        address ownerOfToken = ownerOf(_tokenId);
        int96 tokenFlow = tokenStream[_tokenId];
        int96 userFlow = aGold.getFlowRate(address(this), ownerOfToken);

        if (userFlow > tokenFlow) aGold.updateFlow(ownerOfToken, userFlow - tokenFlow);
        if (userFlow == tokenFlow) aGold.deleteFlow(address(this), ownerOfToken);

        delete tokenStream[_tokenId];
    }
}
