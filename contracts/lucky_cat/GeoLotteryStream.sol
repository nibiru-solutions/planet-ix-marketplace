// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {ISuperfluid, ISuperToken, SuperAppDefinitions, ISuperAgreement} from "@superfluid-finance/ethereum-contracts/contracts/interfaces/superfluid/ISuperfluid.sol";
import {IConstantFlowAgreementV1} from "@superfluid-finance/ethereum-contracts/contracts/interfaces/agreements/IConstantFlowAgreementV1.sol";
import {SuperAppBase} from "@superfluid-finance/ethereum-contracts/contracts/apps/SuperAppBase.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./ILuckyCatGeoLottery.sol";

error NotCFAv1();
error NotSuperToken();
error NotHost();
error ARRAY_SIZES_DIFFERENT();

/// @title Lucky Cat Geo Lottery Stream receiver
/// @author Jourdan Dunkley
/// @notice Upgradable contract
contract GeoLotteryStream is SuperAppBase, OwnableUpgradeable {
    ISuperfluid public host;
    ISuperToken public aGold;
    IConstantFlowAgreementV1 public cfa;
    ILuckyCatGeoLottery public geoLottery;
    bytes32 constant cfaId = keccak256("org.superfluid-finance.agreements.ConstantFlowAgreement.v1");

    event AgreementCreated(address player, int96 newFlowRate);
    event AgreementUpdated(int96 oldFlowRate, int96 newFlowRate, address player);
    event AgreementTerminated(address player);
    event TerminationCallReverted(address indexed sender);

    modifier onlyHost() {
        if (msg.sender != address(host)) revert NotHost();
        _;
    }

    // @dev: function can only called if reacting to a CFA stream and super token are allowed
    modifier onlyExpected(ISuperToken superToken, address agreementClass) {
        if (!_isAcceptedToken(superToken)) revert NotSuperToken();
        if (!_isCFAv1(agreementClass)) revert NotCFAv1();
        _;
    }

    // @dev: bag struct for local variables to avoid stack too deep error
    function initialize(address _host, ISuperToken _aGold) external initializer {
        host = ISuperfluid(_host);
        cfa = IConstantFlowAgreementV1(address(ISuperfluid(_host).getAgreementClass(cfaId)));
        aGold = _aGold;

        host.registerAppWithKey(SuperAppDefinitions.APP_LEVEL_FINAL | SuperAppDefinitions.BEFORE_AGREEMENT_CREATED_NOOP, "k1");

        __Ownable_init();
    }

    // @dev: called by Superfluid as a callback after the stream is created
    function afterAgreementCreated(
        ISuperToken superToken,
        address agreementClass,
        bytes32 /*agreementId*/,
        bytes calldata agreementData,
        bytes calldata /*cbdata*/,
        bytes calldata ctx
    ) external override onlyHost onlyExpected(superToken, agreementClass) returns (bytes memory newCtx) {
        address player = _getPlayer(agreementData);
        int96 newFlowRate = _getFlowRate(superToken, player);
        newCtx = ctx;
        geoLottery.onStreamStarted(player, newFlowRate);
        emit AgreementCreated(player, newFlowRate);
    }

    // @dev: function called by Superfluid as a callback before the stream is updated
    function beforeAgreementUpdated(
        ISuperToken superToken,
        address /*agreementClass*/,
        bytes32 /*agreementId*/,
        bytes calldata agreementData,
        bytes calldata /*ctx*/
    ) external view virtual override returns (bytes memory cbdata) {
        address player = _getPlayer(agreementData);
        cbdata = abi.encode(_getFlowRate(superToken, player), player);
    }

    // @dev: function called by Superfluid as a callback after the stream is updated
    function afterAgreementUpdated(
        ISuperToken superToken,
        address agreementClass,
        bytes32 /*agreementId*/,
        bytes calldata /*agreementData*/,
        bytes calldata cbdata,
        bytes calldata ctx
    ) external override onlyHost returns (bytes memory newCtx) {
        if (!_isCFAv1(agreementClass)) revert NotCFAv1();

        (int96 oldFlowRate, address player) = abi.decode(cbdata, (int96, address));
        int96 newFlowRate = _getFlowRate(superToken, player);
        geoLottery.onStreamUpdated(player, newFlowRate, oldFlowRate);
        newCtx = ctx;

        emit AgreementUpdated(oldFlowRate, newFlowRate, player);
    }

    function beforeAgreementTerminated(
        ISuperToken superToken,
        address /*agreementClass*/,
        bytes32 /*agreementId*/,
        bytes calldata agreementData,
        bytes calldata /*ctx*/
    ) external view override onlyHost returns (bytes memory cbdata) {
        address player = _getPlayer(agreementData);
        cbdata = abi.encode(_getFlowRate(superToken, player), player);
    }

    function afterAgreementTerminated(
        ISuperToken superToken,
        address agreementClass,
        bytes32 /*agreementId*/,
        bytes calldata /*agreementData*/,
        bytes calldata cbdata,
        bytes calldata ctx
    ) external override onlyHost returns (bytes memory) {
        if (!_isAcceptedToken(superToken) || !_isCFAv1(agreementClass)) {
            return ctx;
        }

        (int96 oldFlowRate, address player) = abi.decode(cbdata, (int96, address));

        try geoLottery.onStreamEnded(player, oldFlowRate) {} catch {
            emit TerminationCallReverted(player);
        }

        emit AgreementTerminated(player);
        return ctx;
    }

    function deleteFlows(address[] calldata senders) external {
        uint256 length = senders.length;
        for (uint256 i; i < length; i++) {
            (bool success, ) = address(host).call(
                abi.encodeCall(
                    ISuperfluid(host).callAgreement,
                    (
                        ISuperAgreement(cfa),
                        abi.encodeCall(IConstantFlowAgreementV1(cfa).deleteFlow, (aGold, senders[i], address(this), new bytes(0))),
                        new bytes(0)
                    )
                )
            );
        }
    }

    // @dev: get flow rate that user is streaming to this contract
    function getFlowRate(address superToken, address player) public view returns (int96) {
        return _getFlowRate(ISuperToken(superToken), player);
    }

    // @dev: approve another address to move SuperToken on behalf of this contract
    function approve(ISuperToken superToken, address to, uint256 amount) public onlyOwner {
        superToken.approve(to, amount);
    }

    // @dev: get sender address from agreementData
    function _getPlayer(bytes calldata agreementData) internal pure returns (address player) {
        (player, ) = abi.decode(agreementData, (address, address));
    }

    // @dev: get flow rate that user is streaming to this contract
    function _getFlowRate(ISuperToken superToken, address sender) internal view returns (int96 flowRate) {
        (, flowRate, , ) = cfa.getFlow(superToken, sender, address(this));
    }

    // @dev: check if superToken is accepted by this contract
    function _isAcceptedToken(ISuperToken superToken) private view returns (bool) {
        return address(superToken) == address(aGold);
    }

    // @dev: check if agreementClass is CFAv1
    function _isCFAv1(address agreementClass) private view returns (bool) {
        return ISuperAgreement(agreementClass).agreementType() == cfaId;
    }

    function setGeoLottery(ILuckyCatGeoLottery _geoLottery) external onlyOwner {
        geoLottery = _geoLottery;
    }

    function withdraw(ISuperToken token, uint256 amount) external onlyOwner {
        require(address(token) == address(aGold), "GeoLotteryStream: Invalid token");

        uint256 balance = token.balanceOf(address(this));
        require(balance >= amount, "GeoLotteryStream: Insufficient balance");

        bool success = token.transfer(owner(), amount);
        require(success, "GeoLotteryStream: Withdrawal failed");
    }
}