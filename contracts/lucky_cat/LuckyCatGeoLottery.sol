//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "abdk-libraries-solidity/ABDKMathQuad.sol";
import "../libraries/VRFConsumerBaseV2Upgradable.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {SuperTokenV1Library} from "@superfluid-finance/ethereum-contracts/contracts/apps/SuperTokenV1Library.sol";
import {ISuperToken, ISuperfluid} from "@superfluid-finance/ethereum-contracts/contracts/interfaces/superfluid/ISuperfluid.sol";

import "../rover/VRFCoordinatorV2Interface.sol";
import "./IGeoLotteryStream.sol";
import "./ILuckyCatGeoReward.sol";
import "./ILuckyCatGeoLottery.sol";

contract LuckyCatGeoLottery is VRFConsumerBaseV2Upgradable, OwnableUpgradeable, PausableUpgradeable, ILuckyCatGeoLottery {
    using SuperTokenV1Library for ISuperToken;

    /* ========== STATE VARIABLES ========== */
    uint64 public subscriptionId;
    bytes32 public keyHash;
    uint32 public callbackGasLimit;
    uint16 public requestConfirmations;
    VRFCoordinatorV2Interface public COORDINATOR;

    uint256 public lotteryId;
    uint256 public maxEntries;
    uint256 public ticketPrice;
    mapping(uint256 => uint256) public entries;
    mapping(uint256 => uint256) public poolAmounts;
    mapping(uint256 => uint256) public lotteryStartedAt;
    mapping(uint256 => mapping(uint256 => bytes32)) public merkleRoot;
    mapping(uint256 => mapping(uint256 => uint256)) public rewardPerTicket;

    mapping(uint256 => bool) lotteryDrawn;
    mapping(uint256 => uint256) public drawRequest;
    mapping(uint256 => uint256) public winningCountry;
    mapping(uint256 => uint256) public winningTier;
    mapping(uint256 => uint256) public winningLevel;
    mapping(uint256 => uint256) public winningContinent;

    mapping(address => bool) public userFlowActive;
    mapping(address => mapping(uint256 => bool)) userFlowEndedAtId;
    mapping(address => mapping(uint256 => uint256)) public userEntries;
    mapping(address => mapping(uint256 => int96)) public userLotteryRate;
    mapping(address => mapping(uint256 => uint256)) public userAmountStreamed;
    mapping(address => mapping(uint256 => uint256)) public userStreamUpdatedAt;
    mapping(address => mapping(uint256 => mapping(uint256 => uint256))) public claimedRewards;

    ISuperToken public aGold;
    IGeoLotteryStream public geoStream;
    ILuckyCatGeoReward public luckyCatGeoReward;

    bool claimsPaused;
    mapping(address => bool) public moderators;

    // The first pool from which rewards are available starts at index 6.
    // This is because there are 8 pools in total, but only pools 6, 7, and 8 offer rewards.
    uint256 constant REWARD_STARTING_POOL_INDEX = 6;
    uint256 constant DIVISOR = 1e5;
    uint256 constant ONE_ETH_IN_WEI = 1e18; // 1 ether in wei
    uint256 public defaultThreshold;
    mapping(uint256 => uint256) public thresholdForId;

    modifier onlyGov() {
        if (msg.sender != owner() && !moderators[msg.sender]) revert LuckyCatGeoLottery__Unauthorized(msg.sender);
        _;
    }

    modifier onlyGeoStream() {
        require(msg.sender == address(geoStream), "Caller is not the GeoLottery contract");
        _;
    }

    modifier claimsNotPaused() {
        require(!claimsPaused, "Claims currently paused");
        _;
    }

    /* ========== INITIALIZER ========== */

    function initialize(address _vrfCoordinator, bytes32 _keyHash, uint64 _subscriptionId) external initializer {
        COORDINATOR = VRFCoordinatorV2Interface(_vrfCoordinator);
        __Ownable_init();
        __VRFConsumerBaseV2_init(_vrfCoordinator);

        lotteryStartedAt[lotteryId] = block.timestamp;

        COORDINATOR = VRFCoordinatorV2Interface(_vrfCoordinator);
        keyHash = _keyHash;
        subscriptionId = _subscriptionId;

        callbackGasLimit = 2500000;
        requestConfirmations = 3;
    }

    function enterLottery(uint256 _entries) external whenNotPaused {
        require(userEntries[msg.sender][lotteryId] + _entries <= maxEntries, "Max entries exceeded");
        require(address(luckyCatGeoReward) != address(0), "Geo reward not set");

        uint256 totalCost = ticketPrice * _entries;
        bool success = aGold.transferFrom(msg.sender, address(luckyCatGeoReward), totalCost);
        require(success, "AGOLD transfer failed");

        userEntries[msg.sender][lotteryId] += _entries;
        entries[lotteryId] += _entries;

        emit UserEntered(msg.sender, lotteryId, _entries);
    }

    function onStreamStarted(address _user, int96 _flowRate) external onlyGeoStream {
        userStreamUpdatedAt[_user][lotteryId] = block.timestamp;
        userLotteryRate[_user][lotteryId] = _flowRate;
        userFlowActive[_user] = true;
        userFlowEndedAtId[_user][lotteryId] = false;

        emit UserStreamStarted(_user, _flowRate, block.timestamp);
    }

    function onStreamUpdated(address _user, int96 _newFlowRate, int96 _oldFlowRate) external onlyGeoStream {
        uint256 lastUpdateTimeApplicable = userStreamUpdatedAt[_user][lotteryId] == 0
            ? lotteryStartedAt[lotteryId]
            : userStreamUpdatedAt[_user][lotteryId];
        uint256 timeElapsedSinceLastUpdate = block.timestamp - lastUpdateTimeApplicable;
        userAmountStreamed[_user][lotteryId] += timeElapsedSinceLastUpdate * uint256(int256(_oldFlowRate));
        userStreamUpdatedAt[_user][lotteryId] = block.timestamp;
        userLotteryRate[_user][lotteryId] = _newFlowRate;
        emit UserStreamUpdated(_user, _newFlowRate, _oldFlowRate, block.timestamp);
    }

    function onStreamEnded(address _user, int96 _flowRate) external onlyGeoStream {
        uint256 lastUpdateTimeApplicable = userStreamUpdatedAt[_user][lotteryId] == 0
            ? lotteryStartedAt[lotteryId]
            : userStreamUpdatedAt[_user][lotteryId];
        uint256 timeElapsedSinceLastUpdate = block.timestamp - lastUpdateTimeApplicable;
        userAmountStreamed[_user][lotteryId] += timeElapsedSinceLastUpdate * uint256(int256(_flowRate));
        userStreamUpdatedAt[_user][lotteryId] = block.timestamp;
        userLotteryRate[_user][lotteryId] = 0;
        userFlowActive[_user] = false;
        userFlowEndedAtId[_user][lotteryId] = true;
        emit UserStreamEnded(_user, _flowRate, block.timestamp);
    }

    function drawWinners() external onlyGov returns (uint256 _requestId) {
        require(keyHash != bytes32(0), "Must have valid key hash");
        _requestId = COORDINATOR.requestRandomWords(keyHash, subscriptionId, requestConfirmations, callbackGasLimit, uint32(4));
        drawRequest[_requestId] = lotteryId;
    }

    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords) internal override {
        uint256 _lotteryId = drawRequest[requestId];
        lotteryDrawn[_lotteryId] = true;
        winningCountry[_lotteryId] = randomWords[0] % 207;
        winningTier[_lotteryId] = randomWords[1] % 5;
        winningLevel[_lotteryId] = randomWords[2] % 4;
        winningContinent[_lotteryId] = randomWords[3] % 6;
        ++lotteryId;
        lotteryStartedAt[lotteryId] = block.timestamp;
    }

    function claimReward(uint256 _lotteryId, bytes32[][] calldata merkleProof) external claimsNotPaused {
        require(merkleProof.length != 0, "No Merkle Proof Provided");
        require(lotteryDrawn[_lotteryId], "Lottery Not Drawn Yet");

        ClaimData memory claimData;
        claimData._totalUserEntries = userEntries[msg.sender][_lotteryId];
        claimData._userAmountStreamed = userAmountStreamed[msg.sender][_lotteryId];
        claimData._lotteryStartTime = lotteryStartedAt[_lotteryId];
        claimData._lotteryEndTime = lotteryStartedAt[_lotteryId + 1];
        claimData._userLotteryFlowRate = uint256(int256(userLotteryRate[msg.sender][_lotteryId]));
        claimData._userStreamUpdatedAt = userStreamUpdatedAt[msg.sender][_lotteryId] < claimData._lotteryStartTime
            ? claimData._lotteryStartTime
            : userStreamUpdatedAt[msg.sender][_lotteryId];

        uint256 _lotteryIdCounter = _lotteryId;
        while (claimData._userLotteryFlowRate == 0) {
            claimData._userLotteryFlowRate = uint256(int256(userLotteryRate[msg.sender][_lotteryIdCounter]));
            if (_lotteryIdCounter == 0 || userFlowEndedAtId[msg.sender][_lotteryIdCounter] || claimData._userLotteryFlowRate != 0) break;
            --_lotteryIdCounter;
        }
        claimData._streamedThisWeek =
            claimData._userAmountStreamed +
            (claimData._lotteryEndTime - claimData._userStreamUpdatedAt) *
            claimData._userLotteryFlowRate;
        uint256 _scaledStreamedThisWeek = claimData._streamedThisWeek * 10000;
        uint256 _lotteryThreshold = getThresholdForId(_lotteryId);
        uint256 _roundedTotalUserEntries = roundUp(((_scaledStreamedThisWeek / ticketPrice) * ONE_ETH_IN_WEI) / 10000, _lotteryThreshold);
        claimData._totalUserEntries += _roundedTotalUserEntries / ONE_ETH_IN_WEI;

        for (uint256 i = 0; i < merkleProof.length; i++) {
            uint256 _pool = REWARD_STARTING_POOL_INDEX + i;
            bytes32 node = keccak256(abi.encode(msg.sender, _lotteryId, _pool));

            if (MerkleProof.verify(merkleProof[i], merkleRoot[_lotteryId][_pool], node) && claimedRewards[msg.sender][_lotteryId][_pool] == 0) {
                claimedRewards[msg.sender][_lotteryId][_pool] = claimData._totalUserEntries;
                claimData._rewardToBeStreamed += claimData._totalUserEntries * rewardPerTicket[_lotteryId][_pool];
            }
        }
        int96 rewardAmount = int96(int256(claimData._rewardToBeStreamed));

        if (rewardAmount < 100 * 1e18) {
            require(rewardAmount != 0, "Reward is already claimed");
            luckyCatGeoReward.trasferAGold(aGold, msg.sender, uint256(int256(rewardAmount)));
        } else {
            int96 rewardFlowRate = resolveFlowRate(rewardAmount);
            luckyCatGeoReward.safeMint(msg.sender, rewardFlowRate);
        }

        emit RewardClaimed(_lotteryId, msg.sender, claimData._rewardToBeStreamed);
    }

    function increasePool(uint256 _pool, uint256 _amount) external onlyGov {
        poolAmounts[_pool] += _amount;
        aGold.transferFrom(msg.sender, address(luckyCatGeoReward), _amount);
        emit PoolToppedUp(_pool, _amount, lotteryId);
    }

    function setMerkleRoot(uint256 _lotteryId, bytes32[] memory _merkleRoot, uint256[] memory _pool) external onlyGov {
        for (uint i = 0; i < _merkleRoot.length; i++) merkleRoot[_lotteryId][_pool[i]] = _merkleRoot[i];
    }

    function setPoolRewards(uint256 _lotteryId, uint256[] memory _rewardAmounts, uint256[] memory _pool) external onlyGov {
        for (uint8 i = 0; i < _rewardAmounts.length; i++) rewardPerTicket[_lotteryId][_pool[i]] = _rewardAmounts[i];
    }

    function setKeyHash(bytes32 _keyHash) external onlyGov {
        keyHash = _keyHash;
    }

    function setSubscriptionId(uint64 _subscriptionId) external onlyGov {
        subscriptionId = _subscriptionId;
    }

    function setCallbackGasLimit(uint32 _callbackGasLimit) external onlyGov {
        callbackGasLimit = _callbackGasLimit;
    }

    function setRequestConfirmations(uint16 _requestConfirmations) external onlyGov {
        requestConfirmations = _requestConfirmations;
    }

    function setCOORDINATOR(VRFCoordinatorV2Interface _coordinator) external onlyGov {
        COORDINATOR = _coordinator;
    }

    function setAGOLD(ISuperToken _aGold) external onlyGov {
        aGold = _aGold;
    }

    function setLuckyCatGeoReward(ILuckyCatGeoReward _luckyCatGeoReward) external onlyGov {
        luckyCatGeoReward = _luckyCatGeoReward;
    }

    function setTicketPrice(uint256 newTicketPrice) external onlyGov {
        ticketPrice = newTicketPrice;
    }

    function setMaxEntries(uint256 newMaxEntries) external onlyGov {
        maxEntries = newMaxEntries;
    }

    function setGeoStream(IGeoLotteryStream _geoStream) external onlyGov {
        geoStream = _geoStream;
    }

    function setModerator(address _user, bool _isModerator) external onlyOwner {
        moderators[_user] = _isModerator;
    }

    function setClaimsPaused(bool _claimsPaused) external onlyOwner {
        claimsPaused = _claimsPaused;
    }

    function start() external onlyGov {
        _unpause();
    }

    function stop() external onlyGov {
        _pause();
    }

    // days to stream = 7322.403 + (1.2 - 1/log10(x+0.1) - 7322.403) / (1 + (x / 6561358) ** 0.8898737)
    function resolveFlowRate(int96 _rewardAmount) public pure returns (int96 _rewardFlowRate) {
        FlowRateVariables memory flowRateVars;

        flowRateVars.x = toBytes16(uint256(int256(_rewardAmount)));
        flowRateVars.rewardEther = division(uint256(int256(_rewardAmount)), 1e18);
        flowRateVars.term1 = division(732240300, DIVISOR); // 7322.403
        flowRateVars.term2 = division(120000, DIVISOR); // 1.2
        flowRateVars.term3 = ABDKMathQuad.div(toBytes16(1), log10(ABDKMathQuad.add(flowRateVars.rewardEther, division(10000, DIVISOR)))); // 1 / log10(x+0.1)
        flowRateVars.term4 = ABDKMathQuad.fromUInt(1); // 1
        flowRateVars.exponent = division(8898737, 10000000);
        flowRateVars.term5 = pow(ABDKMathQuad.div(flowRateVars.rewardEther, toBytes16(6561358)), division(8898737, 10000000)); // (x / 6561358) ** 0.8898737
        flowRateVars.diff = ABDKMathQuad.sub(flowRateVars.term2, flowRateVars.term3);
        flowRateVars.numerator = ABDKMathQuad.sub(flowRateVars.diff, flowRateVars.term1);
        flowRateVars.denominator = ABDKMathQuad.add(flowRateVars.term4, flowRateVars.term5);
        flowRateVars.quotient = ABDKMathQuad.div(flowRateVars.numerator, flowRateVars.denominator);
        flowRateVars.timeInDays = ABDKMathQuad.add(flowRateVars.term1, flowRateVars.quotient);
        flowRateVars.timeInDaysUInt = toUInt(flowRateVars.timeInDays);
        flowRateVars.timeInSeconds = flowRateVars.timeInDaysUInt != 0
            ? ABDKMathQuad.mul(flowRateVars.timeInDays, toBytes16(86400))
            : toBytes16(86400);
        flowRateVars.rewardFlowRate = ABDKMathQuad.div(flowRateVars.x, flowRateVars.timeInSeconds);

        _rewardFlowRate = int96(int256(toUInt(flowRateVars.rewardFlowRate)));
    }

    function log10(bytes16 x) internal pure returns (bytes16) {
        bytes16 result = ABDKMathQuad.div(ABDKMathQuad.ln(x), ABDKMathQuad.ln(toBytes16(10)));
        return result;
    }

    function pow(bytes16 x, bytes16 y) internal pure returns (bytes16) {
        bytes16 result = ABDKMathQuad.exp(ABDKMathQuad.mul(ABDKMathQuad.ln(x), y));
        return result;
    }

    function add(bytes16 x, bytes16 y) internal pure returns (bytes16) {
        bytes16 result = ABDKMathQuad.add(x, y);
        return result;
    }

    function toBytes16(uint256 x) internal pure returns (bytes16) {
        return ABDKMathQuad.fromUInt(x);
    }

    function division(uint256 x, uint256 y) internal pure returns (bytes16) {
        return ABDKMathQuad.div(ABDKMathQuad.fromUInt(x), ABDKMathQuad.fromUInt(y));
    }

    function toUInt(bytes16 x) internal pure returns (uint256) {
        uint256 result = ABDKMathQuad.toUInt(x);
        return result;
    }

    /**
     * @dev Sets the default threshold for rounding which getThresholdForId should return to if nothing has been set for that specific Lottery ID.
     * @param _threshold The threshold for rounding. The threshold should be at most 1 ETH long. So 1 which is no rounding is 1e18. 0.9 is 9e17. If the remainder when divided by ONE_ETH_IN_WEI is greater than or equal to the threshold, it rounds up.
     *
     */
    function setDefaultThreshold(uint256 _threshold) external onlyGov {
        if (_threshold > ONE_ETH_IN_WEI) revert("Cannot set threshold to be bigger than one eth");
        defaultThreshold = _threshold;
    }

    /**
     * @dev Sets the threshold for rounding for a specific Lottery ID.
     * @param _id The lottery ID which should have a specific threshold.
     * @param _threshold The threshold for rounding. The threshold should be at most 1 ETH long. So 1 which is no rounding is 1e18. 0.9 is 9e17. If the remainder when divided by ONE_ETH_IN_WEI is greater than or equal to the threshold, it rounds up.
     *
     */
    function setThresholdForId(uint256 _id, uint256 _threshold) external onlyGov {
        if (_threshold > ONE_ETH_IN_WEI) revert("Cannot set threshold to be bigger than one eth");
        thresholdForId[_id] = _threshold;
    }

    function getThresholdForId(uint256 _id) internal view returns (uint256) {
        uint256 threshold = thresholdForId[_id];
        if (threshold == 0) {
            return defaultThreshold;
        }
        return threshold;
    }

    /**
     * @dev Rounds the given value (in wei) up to the nearest ETH based on the threshold.
     * @param _value The value in wei to be rounded.
     * @param _threshold The threshold for rounding. If the remainder when divided by ONE_ETH_IN_WEI is greater than or equal to the threshold, it rounds up.
     * @return The rounded value in wei.
     */
    function roundUp(uint256 _value, uint256 _threshold) public pure returns (uint256) {
        uint256 remainder = _value % ONE_ETH_IN_WEI;

        if (remainder == 0) {
            // If it's a full ETH, return as is
            return _value;
        } else if (remainder >= _threshold) {
            // If the remainder is greater than or equal to the threshold, round up
            return _value + (ONE_ETH_IN_WEI - remainder);
        } else {
            // If the remainder is less than the threshold, round down
            return _value - remainder;
        }
    }
}