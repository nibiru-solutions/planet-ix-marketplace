// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

/// @title Interface for the Lucky cat raffle 3.0
interface ICatRaffle_3_0 {
    error ICatRaffle_3_0__InvalidId(uint256 _raffleId);
    error ICatRaffle_3_0__InvalidInputs();
    error ICatRaffle_3_0__InvalidUserEntries();
    error ICatRaffle_3_0__InvalidStartTime(uint256 _invalidStart);
    error ICatRaffle_3_0__NotDrawable(uint256 _raffleId);
    error ICatRaffle_3_0__IsDrawn(uint256 _raffleId);
    error ICatRaffle_3_0__NoRewards(address _address);
    error ICatRaffle_3_0__ValueError(string _message);
    error ICatRaffle_3_0__NotPausable(uint256 _raffleId);
    error ICatRaffle_3_0__Unauthorized();
    error ICatRaffle_3_0__InvalidRaffle(uint256 _raffleId);
    error ICatRaffle_3_0__Paused(uint256 _raffleId);
    error ICatRaffle_3_0__ZeroEntries();
    error ICatRaffle_3_0__AlreadyClaimed();
    error ICatRaffle_3_0__RaffleNotDrawn();
    error ICatRaffle_3_0__TicketNotFound(
        uint256 _ticket,
        uint256 _totalEntries,
        uint256 _totalPrizes
    );
    error ICatRaffle_3_0__TotalEntriesExceeded();
    error ICatRaffle_3_0__NotEnoughEntries();
    error ICatRaffle_3_0__MaxEntriesPerUserExceeded();
    error ICatRaffle_3_0__RaffleNotStartedYet();

    error ICatRaffle_3_0__InvalidTicketId(uint256 ticketId);
    error ICatRaffle_3_0__MismatchedPrizeLengths();
    error ICatRaffle_3_0__InvalidUserEntryLimits(uint256 userMinEntries, uint256 userMaxEntries);
    error ICatRaffle_3_0__ZeroUserEntries(uint256 userMinEntries, uint256 userMaxEntries);
    error ICatRaffle_3_0__UserMaxExceedsTotal(uint256 userMaxEntries, uint256 totalEntries);
    error ICatRaffle_3_0__UserMinExceedsTotal(uint256 userMinEntries, uint256 totalEntries);
    error ICatRaffle_3_0__ZeroTotalEntries();

    /**
     * @notice Event emitted when a player has entered a raffle.
     * @param _raffleId The id of the raffle.
     * @param _player The address of the player who entered the raffle.
     */
    event RaffleEntered(uint256 _raffleId, address _player);
    /**
     * @notice Event emitted when winners are drawn for a raffle.
     * @param _raffleId The id of the raffle.
     */
    event RaffleDrawn(uint256 _raffleId);
    /**
     * @notice Event emitted when a player has claimed raffle rewards.
     * @param _player The address of the player who entered the raffle.
     */
    event RaffleClaimed(uint256 _raffleId, uint256 _winningIndex, address _player);
    /**
     * @notice Event emitted when a raffle has been registered.
     * @param _raffleId The id of the raffle.
     */
    event RaffleRegistered(uint256 _raffleId);
    /**
     * @notice Event emitted when a raffle has been updated.
     * @param _raffleId The id of the raffle.
     */
    event RaffleUpdated(uint256 _raffleId);
    /**
     * @notice Event emitted when a raffle has been paused.
     * @param _raffleId The id of the raffle.
     * @param _isPaused is true when raffle is paused and false when not.
     */
    event RaffleSetPaused(uint256 _raffleId, bool _isPaused);
    /**
     * @notice Event emitted when a raffle has been deleted.
     * @param _raffleId The id of the raffle.
     */
    event RaffleDeleted(uint256 _raffleId);

    enum PrizeType {
        ERC721,
        ERC1155
    }

    struct Raffle {
        uint256 ticketId;
        address prizeAddress;
        PrizeType prizeType;
        uint256[] prizeIds;
        uint256[] prizeAmounts;
        uint256 totalPrizes;
        uint256 totalEntries;
        uint256 userMinEntries;
        uint256 userMaxEntries;
        uint256 startTime;
        uint256 duration;
        address raffleTreasury;
        bool paused;
        bool isDrawn;
        uint raffleId;
        uint entries;
    }

    struct RaffleInput {
        uint256 ticketId;
        address prizeAddress;
        PrizeType prizeType;
        uint256[] prizeIds;
        uint256[] prizeAmounts;
        uint256 totalPrizes;
        uint256 totalEntries;
        uint256 userMinEntries;
        uint256 userMaxEntries;
        uint256 startTime;
        uint256 duration;
        address raffleTreasury;
    }

    /**
     * @notice Used for a user to enter a raffle.
     * @param _raffleId The ID of the raffle to enter.
     * @param _numTickets The number of tickets to enter the raffle with.
     *
     * @dev Requires that the raffle has not been drawn, is not paused, is valid and has started.
     * Also requires that the number of tickets is not zero, does not exceed the total entries,
     * and is not less than the minimum entries per user. Additionally, it requires that the total number
     * of tickets per user does not exceed the maximum entries per user.
     *
     * throws ICatRaffle_3_0__IsDrawn when the raffle has already been drawn.
     * throws ICatRaffle_3_0__InvalidRaffle when raffleId doesn't exist or when raffle is already finished.
     * throws ICatRaffle_3_0__Paused when the raffle is currently paused.
     * throws ICatRaffle_3_0__ZeroEntries when zero tickets are entered.
     * throws ICatRaffle_3_0__TotalEntriesExceeded when the total entries would be exceeded by the ticket purchase.
     * throws ICatRaffle_3_0__NotEnoughEntries when the number of tickets entered is less than the minimum entries per user.
     * throws ICatRaffle_3_0__MaxEntriesPerUserExceeded when the total number of tickets per user would exceed the maximum entries per user.
     * throws ICatRaffle_3_0__RaffleNotStartedYet when the raffle has not yet started.
     *
     * emit RaffleEntered upon successful raffle entry.
     */
    function enterRaffle(uint256 _raffleId, uint256 _numTickets) external;

    /**
     * @notice Draws winners for a given raffle, callable only by the contract's governor.
     * @param _raffleId The ID of the raffle to draw winners from.
     * @return _requestId The ID of the request to the random number generator.
     *
     * @dev Requires that the raffle exists, has finished and not already been drawn.
     * If there are entries in the raffle, it requests random words from the Chainlink coordinator,
     * and links the request ID to the raffle ID.
     *
     * throws ICatRaffle_3_0__InvalidRaffle when the raffleId doesn't exist.
     * throws ICatRaffle_3_0__NotDrawable when the raffle is not finished yet.
     * throws ICatRaffle_3_0__IsDrawn when all the winners have already been drawn.
     *
     * emit RaffleDrawn upon successful drawing of the raffle.
     */

    function drawWinners(uint256 _raffleId) external returns (uint256 _requestId);

    /**
     * @notice Allows a user to claim their rewards from finished raffles.
     * @param _raffleId The ID of the raffle from which to claim rewards.
     * @param _prizeIds An array of prize IDs to claim.
     *
     * @dev Iterates through each prize ID provided, checking if it has already been claimed
     * and if the raffle has been drawn. If these conditions are met, the prize is awarded to the winner.
     *
     * throws ICatRaffle_3_0__AlreadyClaimed if a prize has already been claimed.
     * throws ICatRaffle_3_0__RaffleNotDrawn if the raffle has not yet been drawn.
     *
     * emit RaffleClaimed for each successful claim of a prize, with the winning address as parameter.
     */
    function claimRewards(uint256 _raffleId, uint256[] memory _prizeIds) external;

    /**
     * @notice Admin register a new raffle with desired parameters.
     * @param _raffle RaffleInput struct with desired input parameters.
     *
     * Throws ICatRaffle_3_0__InvalidTicketId when ticketId isn't recognized.
     * Throws ICatRaffle_3_0__MismatchedPrizeLengths when the number of prize IDs doesn't match the number of prize amounts.
     * Throws ICatRaffle_3_0__InvalidUserEntryLimits when userMinEntries is higher than userMaxEntries.
     * Throws ICatRaffle_3_0__ZeroUserEntries when either userMinEntries or userMaxEntries is zero.
     * Throws ICatRaffle_3_0__UserMaxExceedsTotal when userMaxEntries are higher than totalEntries.
     * Throws ICatRaffle_3_0__UserMinExceedsTotal when userMinEntries is higher than totalEntries.
     * Throws ICatRaffle_3_0__ZeroTotalEntries when totalEntries is zero.
     *
     * It automatically assigns a new raffle ID, and creates a new raffle with the provided input parameters, which is stored in the 'raffles' mapping.
     * It also safely transfers the prize assets from the sender to the raffle treasury.
     *
     * Emits a RaffleRegistered event containing the new raffle ID.
     */
    function registerRaffle(RaffleInput memory _raffle) external returns (uint256 _raffleId);

    /**
     * @notice Updates an active raffle with new parameters.
     * @param _raffleId ID of the raffle to be updated.
     * @param _raffle RaffleInput struct with desired input parameters.
     *
     * Throws ICatRaffle_3_0__InvalidTicketId when ticketId isn't 16, 17, or 18.
     * Throws ICatRaffle_3_0__MismatchedPrizeLengths when the number of prize IDs doesn't match the number of prize amounts.
     * Throws ICatRaffle_3_0__InvalidUserEntryLimits when userMinEntries is higher than userMaxEntries.
     * Throws ICatRaffle_3_0__ZeroUserEntries when either userMinEntries or userMaxEntries is zero.
     * Throws ICatRaffle_3_0__UserMaxExceedsTotal when userMaxEntries are higher than totalEntries.
     * Throws ICatRaffle_3_0__UserMinExceedsTotal when userMinEntries is higher than totalEntries.
     * Throws ICatRaffle_3_0__ZeroTotalEntries when totalEntries is zero.
     * Throws ICatRaffle_3_0__InvalidRaffle when the start time of the raffle is zero.
     *
     * Updates the raffle with the provided parameters and emits a RaffleUpdated event containing the updated raffle ID.
     */
    function updateRaffle(uint256 _raffleId, RaffleInput memory _raffle) external;

    /**
     * @notice Used to set the paused status of a raffle.
     * @param _raffleId ID of the raffle whose pause status should be changed.
     * @param _isPaused Boolean flag used for pausing (true) or unpausing (false) a raffle.
     *
     * Throws ICatRaffle_3_0__InvalidRaffle when the raffle identified by _raffleId does not exist (start time of the raffle is zero).
     *
     * Sets the pause status of the raffle to the provided value and emits a RaffleSetPaused event containing the raffle ID and the new paused status.
     */
    function setPaused(uint256 _raffleId, bool _isPaused) external;

    /**
     * @notice Deletes the specified raffle. If tickets have already been entered, they must be manually reimbursed.
     * @param _raffleId The ID of the raffle that should be deleted.
     *
     * Throws ICatRaffle_3_0__InvalidRaffle when the raffle identified by _raffleId does not exist (start time of the raffle is zero).
     *
     * Deletes the raffle from the "raffles" mapping and emits a RaffleDeleted event with the ID of the deleted raffle.
     */
    function deleteRaffle(uint256 _raffleId) external;

    /**
     * @notice Retrieves details for the specified raffle.
     * @param _raffleId The ID of the raffle to retrieve.
     * @return raffle The Raffle struct associated with the given _raffleId. If no such raffle exists, returns a default Raffle struct.
     */
    function getRaffle(uint256 _raffleId) external view returns (Raffle memory raffle);

    /**
     * @notice Retrieves a list of all active raffles. A raffle is considered active if the current time is between its start time and end time.
     * @return raffles An array of Raffle structs, each representing an active raffle. If no active raffles exist, returns an empty array.
     */
    function getActiveRaffles() external view returns (Raffle[] memory raffles);

    /**
     * @notice Get list of all past raffles. Past raffle is defined as one where the current time is greater than the sum of its start time and duration.
     * @return _pastRaffles List of past raffles.
     */
    function getPastRaffles() external view returns (Raffle[] memory);

    /**
     * @notice Get a list of all upcoming raffles. An upcoming raffle is defined as one where the current time is less than its start time.
     * @return _upcomingRaffles List of upcoming raffles.
     */
    function getUpcomingRaffles() external view returns (Raffle[] memory);

    /**
     * @notice Checks if a draw can be performed on the raffle. A draw can be performed if the raffle has started, the current time is past the end of the raffle, and the raffle has not already been drawn.
     * @param _raffleId The id of the raffle to be checked.
     * @return _canDraw Returns true if a draw can be performed on the raffle, and false otherwise.
     */
    function canDraw(uint256 _raffleId) external view returns (bool _canDraw);
}
