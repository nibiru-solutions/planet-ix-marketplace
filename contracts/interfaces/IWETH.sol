// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

/// @title ERC-20 esque interface for wrapped ethers
/// @dev Do we need this? Isn't WETH an erc20?
interface IWETH {
    function deposit() external payable;

    function withdraw(uint256) external;

    function approve(address, uint256) external returns (bool);

    function transfer(address, uint256) external returns (bool);

    function transferFrom(
        address,
        address,
        uint256
    ) external returns (bool);

    function balanceOf(address) external view returns (uint256);
}
