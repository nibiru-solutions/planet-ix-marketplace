//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC1155/IERC1155Upgradeable.sol";

/// @title Interface defining PIX Landmark
interface IPIXLandmark is IERC1155Upgradeable {
    /**
     * @notice Event emitted when a landmark is minted
     * @param account The reciever's address
     * @param tokenId The token which was minted
     * @param amount The amount of token shares which were minted
     * @param category The category of minted token
     */
    event LandmarkMinted(
        address indexed account,
        uint256 indexed tokenId,
        uint256 amount,
        PIXCategory category
    );

    // Used to classify Landmark category
    enum PIXCategory {
        Legendary,
        Rare,
        Uncommon,
        Common,
        Outliers
    }

    /**
     * @notice Used to get Landmark maximum supply
     * @param _tokenId The token id to get maximum supply for
     * @return Landmark token maximum supply
     */
    function getMaxSupply(uint256 _tokenId) external view returns (uint256);

    /**
     * @notice Used to get Landmark category
     * @param _tokenId The token id to get category for
     * @return Landmark token category
     */
    function getLandCategory(uint256 _tokenId) external view returns (PIXCategory);

    /**
     * @notice Used to check whether address is a moderator
     * @param _walletAddress An address to be checked
     * @return Boolean value representing address being moderator or not
     */
    function isModerator(address _walletAddress) external view returns (bool);

    /**
     * @notice Used to set a moderator
     * @param _moderator The address of a moderator to be added or removed
     * @param _approved Boolean value for adding new or removing existing moderator
     */
    function setModerator(address _moderator, bool _approved) external;

    /**
     * @notice Used to set Categories for Landmarks
     * @param _tokenIds A list of token ids to set categories for
     * @param _categories A list of categories
     */
    function setCategories(uint256[] calldata _tokenIds, PIXCategory[] calldata _categories) external;

    /**
     * @notice Used to set maximum supplies for Landmarks
     * @param _tokenIds A list of token ids to set maximum supplies for
     * @param _supplies A list of maximum supplies
     */
    function setMaxSupplies(uint256[] calldata _tokenIds, uint256[] calldata _supplies) external;

    /**
     * @notice Used to set base uri for Landmark metadata
     * @param _baseURI A uri that will be used to generate Landmarks metadata uris
     */
    function setBaseURI(string memory _baseURI) external;

    /**
     * @notice Set a oracle manager to manage the means through which token prices are fetched
     * @param _oracleManager OracleManager address
     */
    function setOracleManager(address _oracleManager) external;

    /**
     * @notice Set a swap manager to manage the means through which tokens are exchanged
     * @param _swapManager SwapManager address
     */
    function setSwapManager(address _swapManager) external;

    /**
     * @notice Set a price for Infinite Backbone
     * @param _price price in usdt
     */
    function setPrice(uint256 _price) external;

    /**
     * @notice Set a usdt address that will be used for payment
     * @param _contractAddress usdt address
     */
    function setUsdtAddress(address _contractAddress) external;

    /**
     * @notice Set a ixt address that will be used for payment
     * @param _contractAddress ixt address
     */
    function setIxtAddress(address _contractAddress) external;

    /**
     * @notice Change profit state of all sales
     */
    function changeProfitState() external;

    /**
     * @notice Used to mint Landmarks for a single tokenId
     * @param _walletAddress An address to receive landmark shares
     * @param _tokenId The token id to be minted
     * @param _amount The amount of Landmark shares to be minted
     * @notice emit {LandmarkMinted} event
     */
    function safeMint(
        address _walletAddress,
        uint256 _tokenId,
        uint256 _amount
    ) external;

    /**
     * @notice Used to mint Landmarks for a batch of tokenIds
     * @param _walletAddresses A list of addresses to receive landmark shares
     * @param _tokenIds A list of token ids to be received
     * @param _amounts A list of amounts to be received
     * @notice emit {LandmarkMinted} event
     */
    function batchMint(
        address[] calldata _walletAddresses,
        uint256[] calldata _tokenIds,
        uint256[] calldata _amounts
    ) external;

    /**
     * @notice Called by a user to purchase Infinite Backbone landmark
     * @param _amount The number of landmark shares to be purchased
     * @param _currency The currency to pay with
     */
    function mint(uint256 _amount, address _currency) external payable;

    /**
     * @notice Withdraws tokens from this contract
     * @param tokens Array of token addresses
     */
    function withdraw(address[] calldata tokens) external;
}
