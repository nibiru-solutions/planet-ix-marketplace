//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

/// @title Interface defining Metashare Staking
interface IMetasharesStaking {
    /**
     * @notice Event emitted when a metashare is staked
     * @param account The staker's address
     * @param tokenId The token which was staked
     * @param amount The amount of tokens that were staked
     */
    event MetashareStaked(address indexed account, uint256 tokenId, uint256 amount);

    /**
     * @notice Event emitted when a metashare is unstaked
     * @param account The address of user that unstakes
     * @param tokenId The token which was unstaked
     * @param amount The amount of tokens that were unstaked
     */
    event MetashareUnstaked(address indexed account, uint256 tokenId, uint256 amount);

    /**
     * @notice Event emitted when a reward is claimed
     * @param account The user's address
     * @param reward The amount of tokens that were claimed
     */
    event RewardClaimed(address indexed account, uint256 reward);

    /**
     * @notice Event emitted when the reward is added to the contract
     * @param tokenId The token for which the reward was added
     * @param reward The amount of rewards that were added
     */
    event RewardAdded(uint256 tokenId, uint256 reward);

    /**
     * @notice Used to store info for Metashare pools
     * @param periodFinish The timestamp of epoch end
     * @param rewardRate The reward rate payout
     * @param lastUpdateTime Timestamp of last pool update
     * @param rewardPerTokenStored Total reward accumulated
     * @param tokensStaked The amount of tokens staked
     * @param userRewardPerTokenPaid The rewards paid out
     * @param rewards The rewards accumulated to be paid out
     */
    struct RewardPool {
        uint256 periodFinish;
        uint256 rewardRate;
        uint256 lastUpdateTime;
        uint256 rewardPerTokenStored;
        uint256 tokensStaked;
        mapping(address => uint256) userRewardPerTokenPaid;
        mapping(address => uint256) rewards;
    }

    /**
     * @notice Get reward per token staked for Metashare
     * @param _tokenId The Token to get reward
     * @param _untilTimestamp The timestamp by which the reward per token is requested
     */
    function rewardPerToken(uint256 _tokenId, uint256 _untilTimestamp) external view returns (uint256);

    /**
     * @notice Used to get the latest time for which the reward is applicable
     * @param _tokenId The token for which the reward was added
     * @param _untilTimestamp The timestamp by which the last time reward is applicable
     */
    function lastTimeRewardApplicable(uint256 _tokenId, uint256 _untilTimestamp) external view returns (uint256);

    /**
     * @notice Used to get the amount of token shares staked by address
     * @param _walletAddress The wallet address for which staked amounts are requested
     * @param _tokenId The token for which staked amounts are requested
     * @return Amount of shares of token staked by address
     */
    function getStakedAmounts(address _walletAddress, uint256 _tokenId) external view returns (uint256);

    /**
     * @notice Used to get all tokens address staked
     * @param _walletAddress The token for which the reward was added
     * @return A list of tokens staked by address
     */
    function getStakedIDs(address _walletAddress) external view returns (uint256[] memory);

    /**
     * @notice Used to get earned rewards for addess and token
     * @param _walletAddress The address to get earned rewards
     * @param _tokenId The token to be get earned rewards
     * @return Rewards accumulated by address for a given token
     */
    function earned(address _walletAddress, uint256 _tokenId) external view returns (uint256);

    /**
     * @notice Used to get earned rewards for addess and token until timestamp
     * @param _walletAddress The address to get earned rewards
     * @param _tokenId The token to be get earned rewards
     * @param _untilTimestamp The timestamp by which the earned reward is requested
     * @return reward Rewards accumulated by address for a given token until timestamp
     */
    function earnedUntilTimestamp(
        address _walletAddress,
        uint256 _tokenId,
        uint256 _untilTimestamp
    ) external view returns (uint256 reward);

    /**
     * @notice Used to get the amount of token shares staked per pool
     * @param _tokenId The token for which tokens staked are requested
     * @return Amount of shares of token staked per pool
     */
    function getTokensStakedPerPool(uint256 _tokenId) external view returns (uint256);

    /**
     * @notice Used to get earned rewards for a batch of tokens
     * @param _walletAddress The owner of tokenIDs
     * @param _tokenIds List of token ids to get rewards
     * @return Rewards accumulated by address for a given list of token ids
     */
    function earnedBatch(address _walletAddress, uint256[] calldata _tokenIds) external view returns (uint256[] memory);

    /**
     * @notice Used to get rewards earned by a given wallet address
     * @param _walletAddress Wallet address to get rewards
     * @return reward Rewards accumulated by address
     */
    function earnedByAccount(address _walletAddress) external view returns (uint256);

    /**
     * @notice Used to get rewards earned by a wallet address until timestamp
     * @param _walletAddress Wallet address to get rewards
     * @param _untilTimestamp The timestamp by which the account earned reward is requested
     * @return reward Rewards accumulated by address until timestamp
     */
    function earnedByAccountUntilTimestamp(address _walletAddress, uint256 _untilTimestamp) external view returns (uint256 reward);

    /**
     * @notice Stake Metashares to the pool
     * @param _tokenId Token id to be staked
     * @param _amount Token amount to be staked
     * @notice emit {MetasharesStaked} event
     */
    function stake(uint256 _tokenId, uint256 _amount) external;

    /**
     * @notice Unstake Metashares from the pool
     * @param _tokenId Token id to be unstaked
     * @param _amount Token amount to be unstaked
     * @notice emit {MetasharesUnstaked} event
     */
    function unstake(uint256 _tokenId, uint256 _amount) external;

    /**
     * @notice Claim rewards from all Metashares pools
     * @notice emit {RewardClaimed} event
     */
    function claim() external;

    /**
     * @notice Claim rewards for multiple Metashares pools
     * @notice emit {RewardClaimed} event
     */
    function claimBatch(uint256[] memory tokenIds) external;

    /// @notice sets the paused state of the contract
    function setPaused(bool _paused) external;

    /**
     * @notice Add rewards to Metashare pools
     * @param _tokenIds Token ids of pools to receive rewards
     * @param _poolRewards Reward token amounts to be distributed
     * @param _epochDuration Period in which reward token amounts will be distributed
     * @notice emit {RewardAdded} event
     */
    function initializePools(
        uint256[] calldata _tokenIds,
        uint256[] calldata _poolRewards,
        uint256 _epochDuration
    ) external;

    /**
     * @notice Set Reward distributor
     * @param _distributor Reward distributor to be address
     */
    function setRewardDistributor(address _distributor) external;
}
