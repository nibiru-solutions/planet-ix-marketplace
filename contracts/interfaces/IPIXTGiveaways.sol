//SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

interface IPIXTGiveaways {
    error IXT_Giveaway_InvalidId(uint256 _id);
    error IXT_Giveaway_InvalidSignature();
    error IXT_Giveaway_MaxAmountExceeded(uint256 _id);
    error IXT_Giveaway_AlreadyClaimed(address sender);

    /**
    * @notice Event emitted when a giveaway is claimed
    * @param _user The users address
    * @param _id The id of the giveaway
    */
    event GiveawayClaimed(address _user, uint256 _id);
    /**
    * @notice Event emitted when a giveaway is created
    * @param _id The id of the giveaway
    * @param _amount The amount of token id being given away
    * @param _totalAmount The total number of addresses that can claim this giveaway
    */
    event GiveawayCreated(uint256 _id, uint256 _amount, uint256 _totalAmount);
    /**
    * @notice Event emitted when a giveaway is deleted
    * @param _id The giveaways' id
    */
    event GiveawayDeleted(uint256 _id);
    /**
    * @notice Event emitted when the signer wallet is updated
    * @param _signer The address of the signer wallet
    */
    event SignerSet(address _signer);

    /**
     * @notice Struct for containing giveaway info
     * @param id Giveaway id
     * @param amount Amount given when claimed
     * @param claimed Total number claimed so far
     * @param cap Cap on total number of claims
     */
    struct GiveawayInfo {
        uint256 id;
        uint256 amount;
        uint256 claimed;
        uint256 cap;
    }

    /**
     * @notice Claims a giveaway.
     * @dev Use EIP712 to handle the signature stuff.
     * @param _id The giveaway id
     * @param v Signature
     * @param r Signature
     * @param s Signature
     *
     * Throws IXT_Giveaway_InvalidId on invalid id
     * Throws IXT_Giveaway_InvalidSignature on invalid signature
     *
     * Emits GiveawayClaimed on successful claim
     */
    function claim(
        uint256 _id,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external;

    /**
     * @notice Creates a giveaway
     * @param _amount The amount to give away when the giveaway is claiemd
     * @param _totalClaims The total amount of claims to give away
     *
     * Reverts on non owner call
     *
     * Emits GiveawayCreated on successful creation
     */
    function createGiveaway(
        uint256 _amount,
        uint256 _totalClaims
    ) external;

    /**
     * @notice Deletes a giveaway
     * @param _id The id of the giveaway to delete
     *
     * Reverts on non owner call
     * Throws IXT_Giveaway_InvalidId on invalid id
     * Emits GiveawayDeleted on deletion
     */
    function deleteGiveaway(uint256 _id) external;

    /**
     * @notice Used to retrieve info regarding a giveaway
     * @param _id The id of the giveaway
     * @return _info Info struct regarding the giveaway
     *
     * Throws IXT_Giveaway_InvalidId on invalid id
     */
    function getGiveawayInfo(uint256 _id) external view returns (GiveawayInfo memory _info);

    /**
     * @notice Used to check whether a user has already claimed a giveaway
     * @param _user The address of the user
     * @param _id The giveaway id
     *
     * Throws IXT_Giveaway_InvalidId on invalid id
     */
    function canClaim(address _user, uint256 _id) external view returns (bool _canClaim);

    /**
     * @notice Sets the signer for the claims
     * @param _signer The address of the signer
     *
     * Reverts on non owner call
     */
    function setSigner(address _signer) external;
}
