// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

interface IEnergyIXTStakingGG {
    /**
     * @notice Event emitted when energy/ixt is staked
     * @param account The staker's address
     */
    event EnergyStaked(address indexed account, uint256 rewardId);

    /**
     * @notice Event emitted when energy/ixt is unstaked
     * @param account The address of user that unstakes
     * @param amount The amount of tokens that were unstaked
     */
    event EnergyUnstaked(address indexed account, uint256 amount);

    /**
     * @notice Event emitted when a reward is claimed
     * @param account The user's address
     * @param reward The amount of tokens that were claimed
     */
    event RewardClaimed(address indexed account, uint256 reward);

    /**
     * @notice Event emitted when the reward is added to the contract
     */
    event RewardAdded(GGRewards _rewards);

    /**
     * @notice struct for gg rewards.
     * @param energyCost cost in units of ethers
     * @param ixtCost cost in units of ethers
     */
    struct GGRewards {
        uint256 rewardTokenId;
        uint256 rewardTokenAmount;
        uint256 energyCost;
        uint256 ixtCost;
        uint256 lockupTime;
        uint256 concurrentStakerCap;
        uint256 lifeTimePlayerCap;
        bool paused;
    }

    /**
     * @notice Stake Energy and IXT in the ratio 1:1
     * @notice emit {EnergyStaked} event
     * @dev Will internally match the IXT amount with Energy supplied from the user
     */
    function stake(uint256 _id) external;

    /**
     * @notice Claims all rewards due to a user from reward pool
     * @notice emit {RewardClaimed} event
     */
    function claim(uint256 _id) external;
}
