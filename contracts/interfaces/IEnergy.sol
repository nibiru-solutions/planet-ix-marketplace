//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface IEnergy {
    function totalSupply(uint256 id) external view returns (uint256);

    function trustedBurn(
        address _from,
        uint256 _tokenId,
        uint256 _amount
    ) external;
}
