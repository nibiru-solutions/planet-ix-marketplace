//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
* @title Interface defining a price-checking oracle
*/
interface IOracle {
    /**
    * @notice Used to check which tokens this oracle is responsible for
    * @return The addresses of the two tokens that this oracle is responsible for
    */
    function tokens() external view returns (address, address);

    /**
    * @notice Used to calculate exchange rates
    * @param token Address of the token to be exchanged // todo double check the token parameter, this interface is ambiguous
    * @param amount The amount of that token to be exchanged
    * @return The resulting amount of tokens
    */
    function getAmountOut(address token, uint256 amount) external returns (uint256);

    /**
    * @notice Used to see the current exchange rates
    * @param token Address of the token to be exchanged // todo double check the token parameter, this interface is ambiguous
    * @param amount The amount of that token to be exchanged
    * @return The resulting amount of tokens
    */
    function getAmountOutView(address token, uint256 amount) external view returns (uint256);
}
