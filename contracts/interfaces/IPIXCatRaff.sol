// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

/// @title Interface for the Lucky cat raffle
/// @dev Quasi duplicate interface exist as IPIXCatRaffle.sol
interface IPIXCatRaff {

    /**
    * @notice Used to check whether a raffle is drawable
    * @param _week The week number
    * @return Whether the raffle is drawable
    */
    function drawable(uint256 _week) external view returns (bool);

    /**
    * @notice Draws the weeks raffle
    * @param _week The week number
    */
    function draw(uint256 _week) external;

    /**
    * @notice Used to fetch the current week
    * @return The current week
    */
    function getCurrentWeek() external view returns (uint256);
}
