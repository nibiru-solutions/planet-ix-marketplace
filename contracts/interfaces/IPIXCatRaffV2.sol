// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

/// @title Interface for the Lucky cat raffle
/// @dev Quasi duplicate interface exist as IPIXCatRaffle.sol
interface IPIXCatRaffV2 {
    /* ========== ERRORS ========== */
    error PIXCatRaffV2__NotOracle();

    /* ========== EVENTS ========== */
    event DrawRequested(uint256 requestID, uint256 indexed week, uint256 tickets, uint256 prize);
    event ResultDrawn(uint256 indexed week);
    event PrizeClaimed(address indexed player, uint256 prize);
    event TicketsAdded(
        uint256 indexed week,
        address indexed player,
        uint256 tickets,
        uint256 timestamp
    );
    event TicketsWithdrawn(uint256 indexed week, address indexed player, uint256 tickets);
    event PrizeAdded(uint256 indexed week, uint256 amount, uint256 timestamp);

    /**
    * @notice Used to check whether a raffle is drawable
    * @param _week The week number
    * @return Whether the raffle is drawable
    */
    function drawable(uint256 _week) external view returns (bool);

    /**
    * @notice Draws the weeks raffle
    * @param _week The week number
    */
    function draw(uint256 _week) external;

    /**
    * @notice Used to fetch the current week
    * @return The current week
    */
    function getCurrentWeek() external view returns (uint256);
}
