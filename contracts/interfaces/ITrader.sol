//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
* @title Interface default functionality for traders.
*/
interface ITrader {

    /**
    * @notice Generic struct containing information about a timed build for a ITrader.
    * @param orderId, id for unique order.
    * @param orderAmount, amount of tokens requested in order.
    * @param createdAt, timestamp of creation for order.
    * @param speedUpDeductedAmount, total speed up time for order.
    * @param totalCompletionTime, default time for creation minus speedUpDeductedAmount.
    */
    struct Order {
        uint orderId;
        uint orderAmount; //can be multiple on wasteToCash, will be 1 on IfacilityStore, multiple for prospecting?
        uint createdAt; //start time for order. epoch Time
        uint speedUpDeductedAmount; //time that has been deducted.
        uint totalCompletionTime; // defaultOrdertime - speedUpDeductedAmount. In seconds.
    }

    /**
    * @notice Get all active orders for user.
    * @param _player, address for orders to be requested from.
    * @return All unclaimed orders.
    */
    function getOrders(address _player) external view returns (Order[] memory);

    /**
    * @notice Speed up one order.
    * @param _numSpeedUps, how many times you want to speed up an order.
    * @param _orderId, chosen order to speed up.
    */
    function speedUpOrder(uint _numSpeedUps, uint _orderId) external;

    /**
    * @notice Claim order single order.
    * @param _orderId, chosen order to claim
    */
    function claimOrder(uint _orderId) external;

    /**
    * @notice Claim all orders that are finished for user.
    */
    function claimBatchOrder() external;

    function setIXTSpeedUpParams(address _pixt, address _beneficiary, uint _pixtSpeedupSplitBps, uint _pixtSpeedupCost) external;

    function IXTSpeedUpOrder(uint _numSpeedUps, uint _orderId) external;

    function setBaseLevelAddress(address _baseLevelAddress) external;

}
