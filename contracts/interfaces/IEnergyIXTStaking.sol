// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

interface IEnergyIXTStaking {
    /**
     * @notice Event emitted when energy/ixt is staked
     * @param account The staker's address
     * @param amount The amount of tokens that were staked
     */
    event EnergyStaked(address indexed account, uint256 amount);

    /**
     * @notice Event emitted when energy/ixt is unstaked
     * @param account The address of user that unstakes
     * @param amount The amount of tokens that were unstaked
     */
    event EnergyUnstaked(address indexed account, uint256 amount);

    /**
     * @notice Event emitted when a reward is claimed
     * @param account The user's address
     * @param reward The amount of tokens that were claimed
     */
    event RewardClaimed(address indexed account, uint256 reward);

    /**
     * @notice Event emitted when the reward is added to the contract
     */
    event RewardAdded(uint256 _poolTotalReward, uint256 _epochDuration);

    /**
     * @notice Info on pool rewards
     * @param periodFinish The timestamp of epoch end
     * @param rewardRate The reward rate payout
     * @param lastUpdateTime Timestamp of last pool update
     * @param rewardPerTokenStored Total reward accumulated
     * @param tokensStaked The amount of tokens staked
     * @param userRewardPerTokenPaid The rewards paid out
     * @param rewards The rewards accumulated to be paid out
     * @param ameliaRewards Rewards a user has earned for committing to amelia fund
     * @param pushedRewards The rewards pushed to next epoch
     */
    struct RewardPool {
        uint256 periodFinish;
        uint256 rewardRate;
        uint256 lastUpdateTime;
        uint256 rewardPerTokenStored;
        uint256 tokensStaked;
        mapping(address => uint256) userRewardPerTokenPaid;
        mapping(address => uint256) rewards;
        mapping(address => uint256) ameliaRewards;
        mapping(uint256 => uint256) pushedRewards; //epochNumber => pushed amount from previous epochs
    }

    /**
     * @notice Add rewards to Energy Staking Pools
     * @param _poolRewardAmount Amount of a token to be distributed
     * @param _epochDuration Period in which rewards will be distributed
     * @notice emit {RewardAdded} event
     */
    function addCurrentEpochRewards(uint256 _poolRewardAmount, uint256 _epochDuration) external;

    /**
     * @notice Stake Energy and IXT in the ratio 1:1
     * @param _amount Amount of IXT to stake
     * @notice emit {EnergyStaked} event
     * @dev Will internally match the IXT amount with Energy supplied from the user
     */
    function stakeEnergy(uint256 _amount, bool _ameliaCommitment) external;

    /**
     * @notice Unstake Energy and IXT in the ratio 1:1
     * @param _amount Amount of IXT to unstake
     * @notice emit {EnergyUnstaked} event
     */
    function unstakeIXT(uint256 _amount) external;

    /**
     * @notice Claims all rewards due to a user from reward pool
     * @param _ameliaCommitment Option to donate portion of earnings to Amelia Foundation
     * @notice emit {RewardClaimed} event
     */
    function claim(bool _ameliaCommitment) external;

    /**
     * @notice Used to get the amount of token shares staked by address
     * @param _player The wallet address for which staked amounts are requested
     * @return userStakedAmount amount staked by the user
     */
    function getUserStakedAmount(address _player) external view returns (uint256);

    /**
     * @notice Used to get the amount of tokens in the pool
     * @return totalStakedAmount total amount staked
     */
    function getTotalStakedAmount() external view returns (uint256);

    /**
     * @notice Used to get rewards earned by a given wallet address
     * @param _player Wallet address to get rewards
     * @return reward Rewards accumulated by address
     */
    function earned(address _player, uint256 _epoch) external view returns (uint256);

    /**
     * @notice Set Reward distributor
     * @param _distributor Reward distributor to be address
     */
    function setRewardDistributor(address _distributor) external;
}
