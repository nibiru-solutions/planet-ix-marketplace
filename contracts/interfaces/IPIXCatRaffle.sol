// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;
/// @title Interface for the Lucky cat raffle
/// @dev Quasi duplicate interface exist as IPIXCatRaff.sol
interface IPIXCatRaffle {
    /**
    * @notice Used to check whether a raffle is drawable
    * @param _day The day number
    * @return Whether the raffle is drawable
    */
    function drawable(uint256 _day) external view returns (bool);

    /**
    * @notice Draws the weeks raffle
    * @param _day The day number
    */
    function draw(uint256 _day) external;

    /**
    * @notice Used to fetch the current day
    * @return The current day
    */
    function getCurrentDay() external view returns (uint256);

    /**
    * @notice resolves the winner for given day. 
    * @notice Can be called only when random seed is received from Chainlink 
    * @param _day day to resolve
    */
    function resolveDay(uint256 _day) external;
}
