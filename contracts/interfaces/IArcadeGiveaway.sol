// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "./IArcadeGiveawayTokenHandler.sol";

interface IArcadeGiveaway {

    /**
    * @notice Data about giveaways
    * @param rewardToken address of erc721 token which user should own to be able to get reward
    * @param startTokenId minimum token id which counts for rewards
    * @param endTokenId maximum token id which counts for rewards
    * @param needsProof flag if it is crosschain giveaway and proof is needed
    * @param claimings data about each reward inside this giveaway
    */
    struct Giveaway {
        address rewardToken;
        uint256 startTokenId;
        uint256 endTokenId;
        bool needsProof;
        Claiming[] claimings;
    }

    /**
    * @notice Data about claimings (rewards) inside of giveaway
    * @param giveawayHanlder address of IArcadeGiveawayTokenHandler contract which handles sending tokens to the users
    * @param tokenId id of the token which is rewarded (not important for erc20)
    * @param amount number of rewarded tokens of the same type
    * @param active flag if claiming is currently active
    */
    struct Claiming {
        IArcadeGiveawayTokenHandler giveawayHanlder;
        uint256 tokenId;
        uint256 amount;
        bool active;
    }
  
    /**
    * @notice Get all existing giveaways inside of this contract
    * @return retGiveaways list of all giveaways which exists
    */
    function getGiveaways() external view returns(Giveaway[] memory);

    /**
    * @notice Get all available claimings (rewards) for given giveaway id
    * @param giveawayId id of giveaway
    * @return claimings list of claimings inside of given giveaway
    */
    function getClaimings(uint256 giveawayId) external view returns(Claiming[] memory);

    /**
    * @notice Gets list of claimed rewards inside giveaway for given token id
    * @param giveawayId id of giveaway
    * @param withTokenId id of token for which to check
    * @return bool[] returns list of booleans, for each claiming inside of giveaway it is true if given token already claimed
    */
    function getAlreadyClaimedForTokenId(uint256 giveawayId, uint256 withTokenId) external view returns(bool[] memory);

    /**
    * @notice Claims rewards from giveaway. User must own token on this chain when calling this method.
    * @param giveawayId id of giveaway for which reward is claimed
    * @param withTokenIds token ids used to claim rewards
    * @param claimingIds indexes of claimings inside of giveaway for which user wants to claim rewards
    */
    function claim(
        uint256 giveawayId, 
        uint256[] calldata withTokenIds,
        uint256[] calldata claimingIds
    ) external; 

    /**
    * @notice Claims rewards from giveaway with given signature from backend. It means that base token is on other chain.
    * @param giveawayId id of giveaway for which reward is claimed
    * @param withTokenIds token ids used to claim rewards
    * @param claimingIds indexes of claimings inside of giveaway for which user wants to claim rewards
    * @param signatureProof signature obtained from backend
    * @param signatureTtl timestamp until signature is valid
    */
    function claimWithProof(
        uint256 giveawayId, 
        uint256[] calldata withTokenIds,
        uint256[] calldata claimingIds, 
        bytes calldata signatureProof,
        uint256 signatureTtl
    ) external;
}