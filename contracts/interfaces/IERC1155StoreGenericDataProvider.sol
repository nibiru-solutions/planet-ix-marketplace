// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface IERC1155StoreGenericDataProvider {
    /**
     * @notice calculates the exact price with ownership based discounts applied
     * @param _tokenAddress The token address for the tokenId claimed to be owned (for rebates)
     * @param _tokenId The token id for the token claimed to be owned (for rebates)
     * @param _numPurchases the number of packs to purchase
     * @param _saleId the sale ID of the pack to purchase
     * @return price The unit price
     */
    function checkPrice(
        address _tokenAddress,
        uint256 _tokenId,
        uint256 _numPurchases,
        uint256 _saleId
    ) external returns (uint256 price);

    /**
     * @notice returns the price for ERC1155 payments
     * @param _numPurchases the number of packs to purchase
     * @param _saleId the sale ID of the pack to purchase
     * @param _paymentTokenId token id used for payment
     * @return _price The unit price
     */
    function checkERC1155PaymentPrice(
        uint256 _numPurchases,
        uint256 _saleId,
        uint256 _paymentTokenId
    ) external view returns(uint256 _price);
}