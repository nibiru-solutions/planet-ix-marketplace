// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

import "./IPIX.sol";

interface IPIXTerritoryStaking {
    event PIXStaked(uint256 tokenId, address indexed account);
    event PIXUnstaked(uint256 tokenId, address indexed account);
    event RewardClaimed(uint256 reward, address indexed account);
    event RewardAdded(uint256 reward);
    event ModeratorUpdated(address indexed moderator, bool approved);
    event EpochAdded(uint256 indexed epochId, uint256 startTimestamp, uint256 endTimestamp, uint256 totalEpochReward);
    event RewardClaimedToken(address indexed user, uint256 indexed tokenId, uint256 reward);

    function setModerator(address moderator, bool approved) external;
    function setRewardToken(address rewardTokenAddress) external;

    /**
     * @dev called by admin to initialize new pool rewards mechanics. For each mini pool (size+categoty)
     *   we set percentage of total rewards which will go to the mini pool in that epoch.
     *   Also, we set number of currently staked tokens in the contract if we upgrade on existing one.
     * @param pixSize array of sizes
     * @param pixCategory array of categories
     * @param rewardPercentages corresponding percentage which mini pool should get
     * @param currentStakedTokens corresponding number of staked tokens
     * @param _rewardPercentagesDenominator percentage denominator
     */
    function initializePoolRewards(
        IPIX.PIXSize[] calldata pixSize, 
        IPIX.PIXCategory[] calldata pixCategory,
        uint256[] calldata rewardPercentages,
        uint256[] calldata currentStakedTokens,
        uint256 _rewardPercentagesDenominator
    ) external;

     /**
     * @dev set percentage of total reward which is paid in current epoch for any pool
     * based on number of staked tokens
     * @param _rewardPercentagePaidPerTokensStaked X-th member is percentage paid if there are X staked tokens
     */
    function setRewardPercentagePaidPerTokensStaked(
        uint256[] calldata _rewardPercentagePaidPerTokensStaked
    ) external;

    /**
     * @dev set for each size how many of pushed rewards are pushed to each of next epochs
     * @param pixSize size for which percentages are set
     * @param _pushRewardNextEpochsPercentages array of percentages, X-th member is percentage
     *   of how much is pushed to the currentEpoch+X epoch
     */
    function setPushRewardNextEpochsPercentages(
        IPIX.PIXSize pixSize, 
        uint256[] calldata _pushRewardNextEpochsPercentages
    ) external;

     /**
     * @dev set reward distributor by owner
     * reward distributor is the moderator who calls addEpochRewards function
     * whenever new epoch needs to be started
     * @param distributor new distributor address
     */
    function setRewardDistributor(address distributor) external;

    /**
     * @dev stake territory pix token
     * @param tokenId token id to stake
     */
    function stake(uint256 tokenId) external;

    /**
     * @dev unstake territory pix token. Any remaining reward is automatically collected.
     * @param tokenId token id to unstake.
     */
    function unstake(uint256 tokenId) external;

    /**
     * @dev called by reward distributor to add new epoch. Previous epoch must be finished to add new one.
     * @param epochTotalReward total reward for the new epoch, which will be splited to mini pools per size/category
     * @param epochDuration duration in seconds of this epoch
     */
    function addEpochRewards(uint256 epochTotalReward, uint256 epochDuration) external;

    /**
     * @dev views claimable reward for the one token, calculated until this moment
     * @param tokenId token id to view reward
     */
    function earnedReward(uint256 tokenId) external view returns(uint256);

    /**
     * @dev views claimable reward for the one token, calculated until specific timestamp
     * @param tokenId token id to view reward
     */
    function earnedRewardUntilTimestamp(uint256 tokenId, uint256 untilTimestamp) external view returns(uint256);

    /**
     * @dev views claimable reward for the list of tokens, calculated until this moment
     * @param tokenIds list of tokens to view rewards
     */
    function earnedBatch(uint256[] memory tokenIds) external view returns (uint256[] memory);

    /**
     * @dev views claimable reward for the list of tokens. calculated until specific timestamp
     * @param tokenIds list of tokens to view rewards
     */
    function earnedBatchUntilTimestamp(uint256[] memory tokenIds, uint256 untilTimestamp) external view returns (uint256[] memory);

    /**
     * @dev views total claimable reward for account, calculated until this moment
     * @param account address of account
     */
    function earnedByAccount(address account) external view returns (uint256);

    /**
     * @dev views total claimable reward for account, calculated until specific timestamp
     * @param account address of account
     */
    function earnedByAccountUntilTimestamp(address account, uint256 untilTimestamp) external view returns (uint256);

    /**
     * @dev claim reward for provided tokenIds and update reward related arguments.
     *      Function will revert if any provided tokenId is not currently staked by user.
     * @param tokenIds list of tokens to claim reward
     * @notice emit {RewardClaimed} event
     */
    function claimBatch(uint256[] memory tokenIds) external;

    /**
     * @dev claim reward for all user's tokens
     */
    function claimAll() external;

    /**
     * @dev returns true if token is staked
     * @param tokenId id of token to check
     */
    function isStaked(uint256 tokenId) external view returns (bool);

    /**
     * @dev returns the number of staked tokens for user
     * @param user address of the user
     */
    function getStakedNFTsLength(address user) external view returns(uint256);

    /**
     * @dev returns the array of all staked tokens for user
     * @param user address of the user
     */
    function getStakedNFTs(address user) external view returns(uint256[] memory);

    /**
     * @dev returns the amount of pushed rewards from previus epochs, for the reward pool with size and category
     * @param pixSize pix size
     * @param pixCategory pix category
     * @param epochId epoch id to check
     */
    function getPushedRewards(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory, uint256 epochId) external view returns(uint256);

    /**
     * @dev returns the total reward from the beginning for the reward pool with size and category.
     *      This parameter is used only for internal calculations.
     * @param pixSize pix size
     * @param pixCategory pix category
     */
    function getRewardPerToken(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external view returns(uint256);

    /**
     * @dev returns current rate of total reward per second for the reward pool with size and category
     * @param pixSize pix size
     * @param pixCategory pix category
     */
    function getCurrentRewardPerSecond(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external view returns(uint256);

    /**
     * @dev returns the total number of tokens currently staked in the reward pool with size and category
     * @param pixSize pix size
     * @param pixCategory pix category
     */
    function getTokensStaked(IPIX.PIXSize pixSize, IPIX.PIXCategory pixCategory) external view returns(uint256);
}
