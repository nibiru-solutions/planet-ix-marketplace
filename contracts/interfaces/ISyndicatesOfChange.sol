pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/IERC721MetadataUpgradeable.sol";

interface ISyndicatesOfChange is IERC721MetadataUpgradeable {
    /**
    * @notice Event emitted when a new charitable cause is registered
    * @param _causeId The id of the new cause
    * @param _beneficiary The recipient address of the cause
    */
    event CauseRegistered(uint256 _causeId, address _beneficiary);
    /**
    * @notice Event emitted when a cause is deleted
    * @param _causeId The id of the cause
    */
    event CauseDeleted(uint256 _causeId);
    /**
    * @notice Event emitted when money is donated towards a cause
    * @param _donator The address of the donator
    * @param _causeId The id of the cause
    * @param _paymentToken The token which was donated
    * @param _amount The amount of that token which was donated
    */
    event Donation(address _donator, uint256 _causeId, address _paymentToken, uint256 _amount);
    /**
    * @notice Event emitted when the whitelist status of a token is altered
    * @param _tokenAddress The address of the token
    * @param _isWhitelisted Whether that token is whitelisted
    */
    event TokenWhitelisted(address _tokenAddress, bool _isWhitelisted);
    /**
    * @notice Event emitted when a beneficiary withdraws donations from the contract
    * @param _beneficiary The address of the beneficiary
    * @param _paymentToken The token which has been withdrawn
    * @param _amount The amount which has been withdrawn
    */
    event Withdrawal(address _beneficiary, address _paymentToken, uint256 _amount);

    error SoC__TokenNotWhitelisted(address _tokenAddress);
    error SoC__DifferentCauses(uint256 _causeId, uint256 _tokenCauseId);
    error SoC__InvalidCauseId(uint256 _causeId);
    error SoC__NotERC20(address _tokenAddress);
    error SoC__NotOwnerOf(address _user, uint256 _tokenId);
    error SoC__IncorrectLength(uint256 _breakpointLength, uint256 _metadataLength);
    error SoC__CausePaused(uint256 _causeId);
    error SoC__InvalidBeneficiary(address _beneficiary);
    error SoC__ZeroValue();
    error SoC__DonationTooSmall(uint256 _amount);
    error SoC__NotBeneficiary(address _sender);
    error SoC__InputsNotSorted();

    struct CauseInfo {
        address _beneficiary;
        uint256 _totalDonatedAmount;
        bool _paused;
    }

    /**
     * @notice Donates some amount of currency towards a charitable cause. In return, a 721 is minted (if no existing token id is provided)
     * @param _causeId The id number for the cause
     * @param _paymentToken The address of the token being donated. Zero address if eth is being donated
     * @param _amount The amount of the currency being donated
     * @param _existingTokenId Id for existing ERC-721, allowing further donations to be added onto that token rather than minting a new one
     * @return _newTokenId Token id for new token if a new token is minted, otherwise _existingTokenId
     *
     * Throws SoC__TokenNotWhitelisted on non whitelisted token address
     * Throws SoC__InvalidCauseId on invalid cause id
     * Throws SoC__NotOwnerOf on sender not owning _existingTokenId (incl. the token not being minted)
     * Throws SoC__CausePaused if the cause is paused
     * Throws SoC__ZeroValue on attempting to donate zero value
     * Throws SoC__DifferentCauses if provided token id corresponds to a different cause
     *
     * Emits Donation upon successful donation
     */
    function donate(
        uint256 _causeId,
        address _paymentToken,
        uint256 _amount,
        uint256 _existingTokenId
    ) external payable returns (uint256 _newTokenId);

    /**
     * @notice Merges multiple tokens into one
     * @param _tokenIds Array containing all the token id's to merge
     *
     * Throws SoC__NotOwnerOf on sender not owning one of the provided token ids
     * Throws SoC__DifferentCausesWhenMerging on trying to merge tokens from different causes
     */
    function mergeTokens(uint256[] calldata _tokenIds) external;

    /**
     * @notice Registers a new cause that can be donated towards
     * @param _beneficiary The address where the donations are to be deposited
     * @param _breakpoints The breakpoint in USD cents where the token updates its appearance
     * @param _metadata The metadata for the base state and all breakpoints
     * @dev Note that _metadata.length = _breakpoints.length + 1 with the zeroth element corresponding to the base appearance
     * @return _causeId The id number for the new cause
     *
     * Throws SoC__IncorrectLength upon _metadata.length != _breakpoints.length + 1 and _breakpoints.length > 0
     *
     * Emits CauseRegistered on successful registration
     */
    function registerCause(
        address _beneficiary,
        uint256[] calldata _breakpoints,
        string[] calldata _metadata
    ) external returns (uint256 _causeId);

    /**
     * @notice Modifies parameters for a cause
     * @param _breakpoints The breakpoint in USD cents where the token updates its appearance
     * @param _metadata The metadata for the base state and all breakpoints
     * @dev Modifying cause should not change the appearance of existing tokens, but may alter them upon merging
     *
     * Throws SoC__IncorrectLength upon _metadata.length != _breakpoints.length + 1 and _breakpoints.length > 0
     *
     */
    function modifyCause(
        uint256 _causeId,
        uint256[] calldata _breakpoints,
        string[] calldata _metadata
    ) external;

    /**
     * @notice Deletes a cause
     * @param _causeId The id number of the cause
     *
     * Throws SoC__InvalidCauseId on invalid cause id
     *
     * Emits CauseDeleted on successful deletion
     */
    function deleteCause(uint256 _causeId) external;

    /**
     * @notice Used to pause and unpause a cause
     * @param _causeId The cause id
     * @param _isPaused Whether the cause should be paused or not
     *
     * Throws SoC__InvalidCauseId on invalid cause id
     */
    function setPaused(uint256 _causeId, bool _isPaused) external;

    /**
     * @notice Sets the whitelist status for a ERC-20 token
     * @param _tokenAddress The address of the ERC-20 token
     * @param _isWhitelisted Whether that token is whitelisted or not
     *
     * Throws SoC__NotERC20 on trying to whitelist address 0
     *
     * Emits TokenWhitelisted on successful whitelisting
     */
    function setTokenWhitelist(address _tokenAddress, bool _isWhitelisted) external;

    /**
     * @notice Returns the donated amount for the given token id
     * @param _tokenId The token id
     * @return _donatedAmountInUSDCents The donated amount in USD cents, rounded down
     *
     * Reverts with "ERC721: invalid token ID" on invalid token id
     */
    function getDonatedAmount(uint256 _tokenId)
        external
        view
        returns (uint256 _donatedAmountInUSDCents);

    /**
     * @notice Returns the cause of a token
     * @param _tokenId The id of the token
     * @return _causeId The causeId
     *
     * Reverts with "ERC721: invalid token ID" on invalid token id
     */
    function getCauseForToken(uint256 _tokenId) external view returns (uint256 _causeId);

    /**
     * @notice Returns the information of a cause
     * @param _causeId The causes id
     * @return _info Struct containing information
     *
     * Throws SoC__InvalidCauseId on invalid cause id
     */
    function getCauseInfo(uint256 _causeId) external view returns (CauseInfo memory _info);

    /**
     * @notice Returns the value of a particular token balance for a beneficiary
     * @param _paymentToken The token being queried for the beneficiary
     * @param _beneficiary The beneficiary whose balance is being queried
     *
     */
    function getPendingBalance(address _paymentToken, address _beneficiary)
        external
        view
        returns (uint256);
}
