from PIL import Image, ImageOps
import random
import json
from string import ascii_uppercase as auc
import os

### NECESSARY INFO
colors = [
    "IX GRADE",
    "BLACK",
    "WHITE",
    "ARCADE LIME",
    "NEW YELLOW",
    "ETERNA BLUE",
    "LUCKY PINK",
    "Y GREEN",
    "EMPIRE RED",
    "ASTRO BLUE",
    "GRAVITY ORANGE",
    "GLOBAL GREY",
]

# foundation
backgrounds = colors + [
    "INTERURBAN",
    "ORBIT",
    "AUTOMATA",
    "DOCKING",
    "HANGAR",
    "DROUGHT",
    "STEAM",
    "SPACETIME",
    "CITADEL",
    "CRIMSON",
    "CLOUDSCRAPER",
    "JAZZNOIR",
    "STARLADDER",
    "MEGACITY",
    "OUTLANDS",
    "AFTERMATH",
    "NOCTURNAL",
    "HIGHSCORE",
    "EXTRALIFE",
    "DUSK",
    "ASCENSION",
    "CENTRAL",
    "ADRIFT",
]

# A frame
layer1 = [
    "IX GRADE OUTLINE",
    "BLACK OUTLINE",
    "WHITE OUTLINE",
    "ARCADE LIME OUTLINE",
    "NEW YELLOW OUTLINE",
    "ETERNA BLUE OUTLINE",
    "LUCKY PINK OUTLINE",
    "Y GREEN OUTLINE",
    "EMPIRE RED OUTLINE",
    "ASTRO BLUE OUTLINE",
    "GRAVITY ORANGE OUTLINE",
    "GLOBAL GREY OUTLINE",
] + colors

# star line
layer2 = colors

# script
layer3 = [
    "TRANSPARENT",
] + colors

## FILE NAMES
background_files = {}
for i, x in enumerate(backgrounds):
    background_files[x] = str(i + 101)

# a frame
layer1_files = {}
for i, x in enumerate(auc[:-2]):
    layer1_files[layer1[i]] = "A" + x

# star line
layer_2_file_names = ["0" + str(i) for i in range(10)] + [str(i) for i in range(10, 12)]
layer2_files = {}
for i in range(len(layer_2_file_names)):
    layer2_files[layer2[i]] = layer_2_file_names[i]

# script
layer3_files = {}
for i, x in enumerate(auc[:-13]):
    layer3_files[layer3[i]] = x

## TRAIT GENERATION

TOTAL_BADGES = 69420

traits = []


def createCombo():
    trait = {}

    foundation = random.choices(backgrounds)[0]
    trait["Foundation"] = foundation

    aframe = ""
    while True:
        aframe = random.choices(layer1)[0]
        if not aframe.__contains__(foundation):
            trait["A Frame"] = aframe
            break

    starline = ""
    while True:
        starline = random.choices(layer2)[0]
        if not starline.__contains__(foundation):
            trait["Star Line"] = starline
            break

    script = ""
    while True:
        script = random.choices(layer3)[0]
        if not script.__contains__(foundation):
            trait["Script"] = script
            break

    if trait in traits:
        return createCombo()
    else:
        return trait


def createTraits():
    for i in range(TOTAL_BADGES):
        newtraitcombo = createCombo()

        traits.append(newtraitcombo)


## ARE ALL BADGES UNIQUE
def allUnique(x):
    seen = list()
    return not any(i in seen or seen.append(i) for i in x)


# ADD TOKEN IDS TO JSON


def addTokenId():
    i = 0
    for item in traits:
        item["tokenId"] = i
        i = i + 1


# GENERATE METADATA

metadatas = []


def traitsToMetadata():
    for item in traits:
        metadata = {}
        metadata["name"] = f'A.O.C Badge #{item["tokenId"]}'
        metadata["description"] = f'#{item["tokenId"]}'
        metadata["image"] = item["image"]
        metadata["attributes"] = [
            {"trait_type": "Foundation", "value": item["Foundation"]},
            {"trait_type": "A Frame", "value": item["A Frame"]},
            {"trait_type": "Star Line", "value": item["Star Line"]},
            {"trait_type": "Script", "value": item["Script"]},
        ]
        metadatas.append(metadata)


# JSON


def writeTraitsJson():
    with open("traits.json", "w") as outfile:
        json.dump(traits, outfile, indent=4)


def writeMetadataJson():
    path = "./metadata/"
    if not os.path.exists(path):
        os.makedirs(path)

    for item in metadatas:
        with open(f'./metadata/{item["name"]}.json', "w") as outfile:
            json.dump(item, outfile, indent=4)


# IMAGE GENERATION


def generateImages(traits_filename):
    if (traits_filename):
        with open(path, "r") as read_file:
            traits_json = json.load(read_file)
    else:
        traits_json = traits

    path = "./images/"
    if not os.path.exists(path):
        os.makedirs(path)

    for i, item in enumerate(traits_json):
        filename = (
            background_files[item["Foundation"]]
            + layer1_files[item["A Frame"]]
            + layer2_files[item["Star Line"]]
            + layer3_files[item["Script"]]
            + ".png"
        )
        im1 = Image.open(
            f'./layers/{background_files[item["Foundation"]]}.png'
        ).convert("RGBA")
        im2 = Image.open(f'./layers/{layer1_files[item["A Frame"]]}.png').convert(
            "RGBA"
        )
        im3 = Image.open(f'./layers/{layer2_files[item["Star Line"]]}.png').convert(
            "RGBA"
        )
        im4 = Image.open(f'./layers/{layer3_files[item["Script"]]}.png').convert("RGBA")

        # Create each composite
        com1 = Image.alpha_composite(im1, im2)
        com2 = Image.alpha_composite(com1, im3)
        com3 = Image.alpha_composite(com2, im4)

        # Convert to RGBA
        rgb_im = com3.convert("RGBA")

        rgb_im.save(path + filename)
        traits[i]["image"] = filename
        print(f'{str(item["tokenId"])} done, {filename}')


# UPDATE TRAITS JSON WITH FULL IMAGE URI

def add_image_link(uri):
    with open('traits.json', "r") as read_file:
        traits_json = json.load(read_file)

    for i, item in enumerate(traits_json):
        traits_json[i]["image"] = uri + item["image"]

    with open("traitsfinal.json", "w") as outfile:
        json.dump(traits_json, outfile, indent=4)


# MAIN


def main():
    # GENERATE TRAITS
    # createTraits()
    # if not allUnique(traits):
    #     print("Not all unique")
    #     return
    # addTokenId()

    # IMAGES
    generateImages('traits.json')

    # SAVE TRAITS JSON
    # writeTraitsJson()

    # ADD IMAGE LINK
    # add_image_link()

    # METADATA
    # traits = loadTraitsJson("traitsfinal.json")
    # traitsToMetadata()
    # writeMetadataJson()


if __name__ == "__main__":
    main()
