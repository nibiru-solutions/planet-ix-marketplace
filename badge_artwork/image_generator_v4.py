from PIL import Image, ImageOps
import random
import json
from string import ascii_uppercase as auc
import os


### NECESSARY INFO
colors = [
    "IX GRADE",
    "BLACK",
    "WHITE",
    "ARCADE LIME",
    "NEW YELLOW",
    "ETERNA BLUE",
    "LUCKY PINK",
    "Y GREEN",
    "EMPIRE RED",
    "ASTRO BLUE",
    "GRAVITY ORANGE",
    "GLOBAL GREY",
]

# foundation
backgrounds = colors
backgrounds2 = [
    "INTERURBAN",
    "ORBIT",
    "AUTOMATA",
    "DOCKING",
    "HANGAR",
    "DROUGHT",
    "STEAM",
    "SPACETIME",
    "CITADEL",
    "CRIMSON",
    "CLOUDSCRAPER",
    "JAZZNOIR",
    "STARLADDER",
    "MEGACITY",
    "OUTLANDS",
    "AFTERMATH",
    "NOCTURNAL",
    "COLONY",
    "HIGHSCORE",
    "EXTRALIFE",
    "DUSK",
    "ASCENSION",
    "CENTRAL",
    "ADRIFT",
]

# A frame
layer1 = [
    "IX GRADE OUTLINE",
    "BLACK OUTLINE",
    "WHITE OUTLINE",
    "ARCADE LIME OUTLINE",
    "NEW YELLOW OUTLINE",
    "ETERNA BLUE OUTLINE",
    "LUCKY PINK OUTLINE",
    "Y GREEN OUTLINE",
    "EMPIRE RED OUTLINE",
    "ASTRO BLUE OUTLINE",
    "GRAVITY ORANGE OUTLINE",
    "GLOBAL GREY OUTLINE",
] + colors

# star line
layer2 = colors

# script
layer3 = [
    "TRANSPARENT",
] + colors

## FILE NAMES
background_files = {}
for i, x in enumerate(backgrounds):
    background_files[x] = str(i + 101)

# a frame
layer1_files = {}
for i, x in enumerate(auc[:-2]):
    layer1_files[layer1[i]] = "A" + x

# star line
layer_2_file_names = ["0" + str(i) for i in range(10)] + [str(i) for i in range(10, 12)]
layer2_files = {}
for i in range(len(layer_2_file_names)):
    layer2_files[layer2[i]] = layer_2_file_names[i]

# script
layer3_files = {}
for i, x in enumerate(auc[:-13]):
    layer3_files[layer3[i]] = x

## TRAIT GENERATION

FIRST_ITERATION = 34848
TOTAL_BADGES = 69420

traits = []
sw = False

def createCombo():
    trait = {}

    foundation = random.choices(backgrounds)[0]
    trait["Foundation"] = foundation

    aframe = ""
    while True:
        aframe = random.choices(layer1)[0]
        if not aframe.__contains__(foundation):
            trait["A Frame"] = aframe
            break

    starline = ""
    while True:
        starline = random.choices(layer2)[0]
        if not starline.__contains__(foundation):
            trait["Star Line"] = starline
            break

    script = ""
    while True:
        script = random.choices(layer3)[0]
        if not script.__contains__(foundation):
            trait["Script"] = script
            break

    if trait in traits:
        return createCombo()
    else:
        return trait

def generateNewTraitGneric():
    for i in range(len(colors)):
        for j in range(len(layer1)):
            if (backgrounds[i] in layer1[j]):
                continue
            for z in range(len(layer2)):
                if (backgrounds[i] in layer2[z]):
                    continue
                for k in range(len(layer3)):
                    trait = {}
                    if (backgrounds[i] in layer3[k]):
                        continue
                    trait["Foundation"] = backgrounds[i]
                    trait["A Frame"] = layer1[j]
                    trait["Star Line"] = layer2[z]
                    trait["Script"] = layer3[k]
                    traits.append(trait)

def generateNewTraitGnericSecondPart():
    tempTraits = []
    for i in range(len(backgrounds2)):
        for j in range(len(layer1)):
            if (backgrounds2[i] in layer1[j]):
                continue
            for z in range(len(layer2)):
                if (backgrounds2[i] in layer2[z]):
                    continue
                for k in range(len(layer3)):
                    trait = {}
                    if (backgrounds2[i] in layer3[k]):
                        continue
                    trait["Foundation"] = backgrounds2[i]
                    trait["A Frame"] = layer1[j]
                    trait["Star Line"] = layer2[z]
                    trait["Script"] = layer3[k]
                    tempTraits.append(trait)
    for i in range(TOTAL_BADGES-FIRST_ITERATION):
        rand = random.randint(0, len(tempTraits)-1)
        traits.append(tempTraits[rand])
        tempTraits.pop(rand)

def createTraits():
    generateNewTraitGneric()
    generateNewTraitGnericSecondPart()
    global traits
    random.shuffle(traits)

## ARE ALL BADGES UNIQUE
def allUnique(x):
    seen = list()
    seen2 = {}
    for i in x:
        if seen2[i]:
            return False
        seen2[i] = True
        print(len(seen))
    return True

# ADD TOKEN IDS TO JSON
def addTokenId():
    i = 0
    global traits
    for item in traits:
        item["tokenId"] = i
        i = i + 1

# GENERATE METADATA
def traitsToMetadata(traits):
    metadatas = []
    for item in traits:
        metadata = {}
        metadata["name"] = f'AOC Badge #{item["tokenId"]}'
        metadata["description"] = f'''AOC Badges are an in-game cosmetic that can be added to your Agent Profile and in Mission Control. When several AOC Badges are owned, they can be upgraded to a higher tier for additional perks and benefits.\n\nThere are 24 unique background scenes, 12 colors and a total of 4 layers to the badges.\n\nThere will be several different burn mechanics added to this collection.'''
        metadata["image"] = item["image"]
        metadata["attributes"] = [
            {"trait_type": "Foundation", "value": item["Foundation"]},
            {"trait_type": "A Frame", "value": item["A Frame"]},
            {"trait_type": "Star Line", "value": item["Star Line"]},
            {"trait_type": "Script", "value": item["Script"]},
        ]
        metadatas.append(metadata)
    return metadatas

# JSON
def writeTraitsJson():
    with open("traits_v4.json", "w") as outfile:
        json.dump(traits, outfile, indent=4)

def writeMetadataJson(metadatas):
    path = "./metadata/"
    if not os.path.exists(path):
        os.makedirs(path)

    for item in metadatas:
        tokenId = item['name'].split('#')[1]
        with open(f'./metadata/{tokenId}', "w") as outfile:
            json.dump(item, outfile, indent=4)


# IMAGE GENERATION
def generateImages(traits_filename):
    path = "./images_v4/"
    if not os.path.exists(path):
        os.makedirs(path)
    if (traits_filename!=''):
        with open('traits_v4.json', "r") as read_file:
            traits_json = json.load(read_file)
    else:
        traits_json = traits

    for i, item in enumerate(traits):
        filename = (
            str(item["tokenId"])+".png"
        )
        im1 = Image.open(
            f'./layers/{background_files[item["Foundation"]]}.png'
        ).convert("RGBA")
        im2 = Image.open(f'./layers/{layer1_files[item["A Frame"]]}.png').convert(
            "RGBA"
        )
        im3 = Image.open(f'./layers/{layer2_files[item["Star Line"]]}.png').convert(
            "RGBA"
        )
        im4 = Image.open(f'./layers/{layer3_files[item["Script"]]}.png').convert("RGBA")

        # Create each composite
        com1 = Image.alpha_composite(im1, im2)
        com2 = Image.alpha_composite(com1, im3)
        com3 = Image.alpha_composite(com2, im4)

        # Convert to RGBA
        rgb_im = com3.convert("RGBA")

        rgb_im.save(path + filename)
        traits[i]["image"] = filename
        print(f'{str(item["tokenId"])} done, {filename}')


# UPDATE TRAITS JSON WITH FULL IMAGE URI
def add_image_link(uri):
    with open('traits_v4.json', "r") as read_file:
        traits_json = json.load(read_file)

    for i, item in enumerate(traits_json):
        traits_json[i]["image"] = uri[int(item["tokenId"]/2000)] + str(item["tokenId"])+".png"

    with open("traitsfinal.json", "w") as outfile:
        json.dump(traits_json, outfile, indent=4)


# MAIN
def main():
    # GENERATE TRAITS
    # print("Creating traits")
    # createTraits()
    # print("Traits created")

    # global traits
    # if not allUnique(traits):
    #    print("Not all unique")
    #    return
    #print("Unique check passed")
    #addTokenId()
    #print("Token ID added")

    # IMAGES
    #print("Generating images")
    #for i, x in enumerate(backgrounds+backgrounds2):
    #    background_files[x] = str(i + 101)
    # SAVE TRAITS JSON
    #writeTraitsJson()
    #generateImages('')



    # ADD IMAGE LINK
    uris = [
        'ipfs://QmPvZTuHLfwkifmZmMFsGWf7JZY1gHvtjyhRqHLtMhspa9/', # 1
        'ipfs://QmX2g7MZNjE52YDcXJVSg7PFPe9uzg8bxERSSfEBFijYTM/',
        'ipfs://QmfH1cwuPsgoupcGRRhz5KNfraT6gDDwNxBn3XdqoVYBU6/',
        'ipfs://QmcZaVMrNLQBFtZsXgtCDDJ3UcBNJxVrjWpY3vdt2hbDPM/',
        'ipfs://QmXtkVtyNNrr79ZgWb61MboCXcrNkJDVKcViNPJKRJXiyi/', # 5
        'ipfs://QmZhLSHgKmSxChrindGcbSRYg5ifvhuJnaHg8MhwHUaBDK/',
        'ipfs://QmNmTkctmMTsoMZfAo5pU712ZSuEsUX9QMX6wiKcT4A4BA/',
        'ipfs://QmXZpBp95863vsCpM7ecG6LF8QoynBeaCSDPTm7oS6dc3V/',
        'ipfs://QmfW95QAGuHDd88MDvu21cc3urpLszrgu53DEML7d3MaGC/',
        'ipfs://QmXgJHkHXpCmWXkuyt43FquJideGTUfUnSME3ppFT2fu8x/', # 10
        'ipfs://QmQpB5HBCxLWQeT6GjPznR73JQK4u33J1AAh5AJTiy5NXM/',
        'ipfs://Qmb5Vu9EGpT4322Uoo1KXBiGoiLRzcvzb8uz4S7owdypZF/',
        'ipfs://Qmf6Xx4ThrpFyrPUJHkGnRKkHwWpDtaaqR8kR6M7ZYvbgd/',
        'ipfs://QmaqrELGa8AwuDR1ZgDdFt8e7faNJUtg731sftQvQFVdLv/',
        'ipfs://QmfPb2dvk8UgZp8iq7G87UnLQ7T4gn6ABdgvJNbGPQwXPg/', # 15
        'ipfs://QmdTZdKxbXhug34XemE1KUj3RXwa7nfGrhfryXqGuSqgfR/',
        'ipfs://QmUNttYN8MXte6q75zee2FqEBY3DQtc7kLMpKndoiueJhJ/',
        'ipfs://QmT1jPabXe47eJEDVQRZ9JrKQJGxwCf7E1vnPn9X9aBnPN/',
        'ipfs://QmadEMEGnr6c7NvpYtkU1SNoMhEMfS3PqDH9PvNscXUo2H/',
        'ipfs://QmWc4wXiP6KC4AgGG2opoDLMFFPgXhRTFGud5VJLEi1yUx/', # 20
        'ipfs://QmQxmiXczN6m5WkY5oBALDfxXKCjEwv168pVkeTnJhBC89/',
        'ipfs://QmNjyjiDUeXCQuFoinkSAJEF78VQ9EBZGGsM1bAFknW3dW/',
        'ipfs://QmRC7NrXmTjC5ZkZ9oH2em2Zyx9uBtDLQp7HtxzXKdZuHj/',
        'ipfs://QmUEqmGFryjmjiUDMRHQVyJY589zpZQbQHp7SfPbTggvVb/',
        'ipfs://QmXTvKWkcw9Ppjcd1LKXhTCj9W8kNVPk6EAEnjE12odVso/', # 25
        'ipfs://QmPeq8D9Bcg72Z85bgaSfD9D25NM1Gqoet44AxZSvqVgnu/',
        'ipfs://QmZPDePWbcAPH1GmUqQf3oQRguymy5ari1Gq2PH3uUQ9hb/',
        'ipfs://QmYMA87yacMXYfZFDPuZwm7cJVdYrXqT3C5XqTGaXbvjnB/',
        'ipfs://QmP6CApXLWYQjEFZ6U5xkk8qvSseuXw3WFgYy6REDR8KSE/',
        'ipfs://QmQfuLVLEcLxS3fT5gn3YpaydUYw4a1PpcH5hzu42JekA2/', # 30
        'ipfs://QmaPDHnf2WpAoA2zwZqxngHqNorghs2VYQ4vjCfPbMmyWH/',
        'ipfs://QmQirqkZeHFuX3DWD2xESqDS3KfhYMktingjyi4iVovSRV/',
        'ipfs://QmasnXnpg6WmwUGGy4VGuQCyPFPdeX1JGbhPKDU9AqhUS2/',
        'ipfs://QmbxyBc5iAFyWE5zQmj5PtpDoejzkLieDtSLvvq5A6xtPo/',
        'ipfs://QmZvmawueaJBZLD6TTg9G2yKZxEsdbGXGbJbJ7VsYpLkah/'  # 35
    ]
    add_image_link(uris)

    # METADATA
    traits = []
    with open('traitsfinal.json', "r") as read_file:
        traits = json.load(read_file)
    metadatas = traitsToMetadata(traits)
    writeMetadataJson(metadatas)


if __name__ == "__main__":
    main()
