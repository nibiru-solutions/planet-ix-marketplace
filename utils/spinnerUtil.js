const Spinner = require('cli-spinner').Spinner;

const displaySpinner = async (promise, message) => {
  const spinner = new Spinner(`${message}`);
  spinner.setSpinnerString('⣾⣽⣻⢿⡿⣟⣯⣷');
  spinner.start();

  try {
    const result = await promise;
    spinner.stop();
    console.log('\nSuccess!\n');
  } catch (error) {
    spinner.stop();
    console.error('Error occurred:', error);
  }
};

module.exports = { displaySpinner };
