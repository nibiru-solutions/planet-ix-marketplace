import { AssetIds, Distribution } from '../test/utils';

export const PIERCER_DRONE_APOLLO = {
  token_id: 5,
  resource_ev: 18,
  resource_id: 7,
  resource_cap: 36,
  class: Distribution.LinearDeterministic,
};

export const PIERCER_DRONE_ARTEMIS = {
  token_id: 5,
  resource_ev: 22,
  resource_id: 7,
  resource_cap: 44,
  class: Distribution.LinearDeterministic,
};

export const PIERCER_DRONE_CHRONOS = {
  token_id: 5,
  resource_ev: 22,
  resource_id: 7,
  resource_cap: 44,
  class: Distribution.LinearDeterministic,
};

export const PIERCER_DRONE_HELIOS = {
  token_id: 5,
  resource_ev: 23,
  resource_id: 7,
  resource_cap: 47,
  class: Distribution.LinearDeterministic,
};

export const GENESIS_DRONE_APOLLO = {
  token_id: 4,
  resource_ev: 22,
  resource_id: 7,
  resource_cap: 44,
  class: Distribution.LinearDeterministic,
};

export const GENESIS_DRONE_ARTEMIS = {
  token_id: 4,
  resource_ev: 28,
  resource_id: 7,
  resource_cap: 55,
  class: Distribution.LinearDeterministic,
};

export const GENESIS_DRONE_CHRONOS = {
  token_id: 4,
  resource_ev: 27,
  resource_id: 7,
  resource_cap: 54,
  class: Distribution.LinearDeterministic,
};

export const GENESIS_DRONE_HELIOS = {
  token_id: 4,
  resource_ev: 29,
  resource_id: 7,
  resource_cap: 58,
  class: Distribution.LinearDeterministic,
};

export const NIGHT_DRONE_APOLLO = {
  token_id: 33,
  resource_ev: 30,
  resource_id: 7,
  resource_cap: 60,
  class: Distribution.LinearDeterministic,
};

export const NIGHT_DRONE_ARTEMIS = {
  token_id: 33,
  resource_ev: 36,
  resource_id: 7,
  resource_cap: 72,
  class: Distribution.LinearDeterministic,
};

export const NIGHT_DRONE_CHRONOS = {
  token_id: 33,
  resource_ev: 36,
  resource_id: 7,
  resource_cap: 72,
  class: Distribution.LinearDeterministic,
};

export const NIGHT_DRONE_HELIOS = {
  token_id: 33,
  resource_ev: 39,
  resource_id: 7,
  resource_cap: 78,
  class: Distribution.LinearDeterministic,
};

export const OUTLIER_FACILITY = {
  token_id: 19,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

export const COMMON_FACILITY = {
  token_id: 20,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

export const UNCOMMON_FACILITY = {
  token_id: 21,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

export const RARE_FACILITY = {
  token_id: 22,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

export const LEGENDARY_FACILITY = {
  token_id: 23,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

export const SOLAR_GRID2 = {
  token_id: 52,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
export const GEOWELL2 = {
  token_id: 53,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
export const TIDAL_TURBINES2 = {
  token_id: 54,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
export const RADIANT_GRADENS2 = {
  token_id: 55,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
export const FUSION_CORE2 = {
  token_id: 56,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
export const SOLAR_GRID3 = {
  token_id: 57,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
export const GEOWELL3 = {
  token_id: 58,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
export const TIDAL_TURBINES3 = {
  token_id: 59,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
export const RADIANT_GRADENS3 = {
  token_id: 60,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
export const FUSION_CORE3 = {
  token_id: 61,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};

export const apollo_tiles = [
  { x: -1, y: 0, z: 1 },
  { x: -1, y: 1, z: 0 },
  { x: 0, y: -1, z: 1 },
  { x: 0, y: 1, z: -1 },
  { x: 1, y: -1, z: 0 },
  { x: 1, y: 0, z: -1 },
];
export const artemis_tiles = [
  { x: 0, y: -2, z: 2 },
  { x: 1, y: -2, z: 1 },
  { x: 2, y: -2, z: 0 },
  { x: 2, y: -1, z: -1 },
  { x: 2, y: 0, z: -2 },
  { x: 1, y: 1, z: -2 },
  { x: 0, y: 2, z: -2 },
  { x: -1, y: 2, z: -1 },
  { x: -2, y: 2, z: 0 },
  { x: -2, y: 1, z: 1 },
  { x: -2, y: 0, z: 2 },
  { x: -1, y: -1, z: 2 },
];
export const chronos_tiles = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];
export const helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];
