const hre = require("hardhat");

let erc1155MockAssetManagerAddressTestnet = "0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc"
let metashareTransferAddressTestnet = "0x9A3c354aA5e9518629092b4d11367136A8B43e8A"

let AssetManagerAddressMainnet = "0xba6666B118f8303F990f3519DF07e160227cCE87"
// AstroCap Metashare
let tokenIdToMint = 75


async function main(){
    const metashareTransferFactory = await hre.ethers.getContractFactory('SafeMetashareTransfer');
    const metashareTransfer = await hre.upgrades.deployProxy(metashareTransferFactory,
      [erc1155MockAssetManagerAddressTestnet]);
    console.log('Contract deployed');

    await metashareTransfer.deployed();
    console.log("MetashareTransfer deployed: ", metashareTransfer.address);

    await metashareTransfer.setMaxSupply(50000)
    console.log("Setting Max Supply to 50000")

    await metashareTransfer.setTokenId(tokenIdToMint)
    console.log("Setting Token ID to", tokenIdToMint)
}

async function upgrade(){
  console.log("Starting upgrade:", metashareTransferAddressTestnet)
  const metashareTransferFactory = await hre.ethers.getContractFactory('SafeMetashareTransfer');
  await hre.upgrades.upgradeProxy(metashareTransferAddressTestnet,
    metashareTransferFactory);
  console.log('Contract upgraded:', metashareTransferAddressTestnet);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });