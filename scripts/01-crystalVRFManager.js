const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
  const vrfCoordinator = '0xec0Ed46f36576541C75739E915ADbCb3DE24bD77';

  const VRFManagerV2 = await ethers.getContractFactory('VRFManagerV2');
  const vrfManager = await VRFManagerV2.deploy(vrfCoordinator);

  console.log('vrf manager deployed at', vrfManager.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
