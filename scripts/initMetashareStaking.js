const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');

async function main() {
    const IXT = await hre.ethers.getContractFactory('PIXT');
    const ixt = await IXT.attach('0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE');

    const y =web3.utils.toWei("50000");
    const luckyCat =web3.utils.toWei("50000");
    const nl =web3.utils.toWei("100000");
    const gws =web3.utils.toWei("50000");
    const el =web3.utils.toWei("50000");
    const ne = web3.utils.toWei("250000");
    const gg = web3.utils.toWei("200000");
    const ac = web3.utils.toWei("50000");
    
    const value = web3.utils.toWei("800000");

    await ixt.increaseAllowance(
      '0xbeafbe2ecc41152b8cef4f155a260ca441f61829',
      web3.utils.toWei(value.toString(), 'ether'),
      { gasPrice: 250000000000, gasLimit: 9000000 },
    );
    const MetashareStaking = await hre.ethers.getContractFactory('MetashareStaking');
    const ms = await MetashareStaking.attach('0xbeafbe2ecc41152b8cef4f155a260ca441f61829');
    
    await ms.initializePools(
      [6,25,28,30,31,27,26,75],
      [y,luckyCat,nl,gws,el,ne,gg,ac],
      2592000,
      { gasPrice: 250000000000, gasLimit: 9000000 },
    );
    //console.log([y,luckyCat,nl,gws,el])
}

main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
