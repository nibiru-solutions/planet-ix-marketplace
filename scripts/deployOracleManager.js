const { ethers } = require('hardhat');
const { waitSeconds } = require('../helper/utils');

/**
 * deploys the oracle manager contract and registers the oracle for USDT and WMATIC
 */
async function main() {
  const { ixtToken, usdt, wmatic } = await hre.getNamedAccounts();

  // const oracleFactory = await ethers.getContractFactory('OracleManager');
  // oracleManager = await oracleFactory.deploy();
  // await oracleManager.deployed();
  // console.log(`oracle deployed to ${oracleManager.address}`);

  // const uniV2Factory = '0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32';
  // const uniOracleFactory = await ethers.getContractFactory('UniV2Oracle');
  // const oracle = await uniOracleFactory.deploy(uniV2Factory, usdt, wmatic, wmatic);
  // await oracle.deployed();
  // console.log(`oracle deployed to ${oracle.address}`);

  const oracle = await ethers.getContractAt(
    'UniV2Oracle',
    '0x22101f4e7b576fE95861c4F3dB19B81faBF62BbB',
  );

  console.log(await actualOracle.tokens())

  const oracleManager = await ethers.getContractAt(
    'OracleManager',
    '0x36EfC910B29EC45B5E258f8A2f8170deaBD5A3bD',
  );

  await oracleManager.registerOracle(usdt, '0x0000000000000000000000000000000000000000', oracle.address);

  console.log('verifying oracle manager');
  await waitSeconds(60);
  await hre.run('verify:verify', {
    address: oracleManager.address,
    constructorArguments: [],
  });
  console.log('oracle manager verified');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
