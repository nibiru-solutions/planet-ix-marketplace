const { ethers } = require('hardhat');
const { constants } = require('ethers');

const assets = '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc';
const gravityGradePacks = '0xF7b0892b05bd7Ae8d55c672f9c2406B651a98157';

const ggGenericBurn = '0xF80be59beeD04BA31DD94f75aECb480540AbB129';

async function main() {
  const ggMintFactory = await ethers.getContractFactory('GravityGradeMintWrapper');
  const ggMint = await ggMintFactory.deploy();
  await ggMint.deployed();

  await ggMint.setGravityGradeContract(gravityGradePacks);
  await ggMint.setTrusted(ggGenericBurn, true);

  console.log(`gg mint deployed to ${ggMint.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
