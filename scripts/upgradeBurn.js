const { ethers, upgrades } = require('hardhat');
const hre = require('hardhat');
const run = require('hardhat');

const NFTProxyAddress = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const gravityGradeAddress = '0x3376C61c450359d402F07909Bda979a4c0e6c32F';
const vrfManagerAddress = '';
const SUBSCRIPTION_ID = '';
const KEYHASH = '';
const CALLBACK_GAS_LIMIT = 2_500_000;
const REQUEST_CONFIRMATIONS = 3;
const NUM_WORDS = 1;

let genericBurn;
let vrfManager;

async function upgradeBurn() {
  const genericBurnFactory = await ethers.getContractFactory('GravityGradeGenericBurn');
  genericBurn = await upgrades.upgradeProxy(
    '0xF80be59beeD04BA31DD94f75aECb480540AbB129',
    genericBurnFactory,
  );

  console.log(`generic burn upgraded: ${genericBurn.address}`);
}

async function deployVRFManager() {
  const vrfFactory = await ethers.getContractFactory('VRFManager');
  vrfManager = await vrfFactory.deploy('0x7a1bac17ccc5b313516c5e16fb24f7659aa5ebed');
  await vrfManager.deployed();

  await genericBurn.setVRFOracle(vrfManager.address);

  await vrfManager.setConsumer(
    genericBurn.address,
    2541,
    '0x4b09e658ed251bcafeebbc69400383d49f344ace09b9576fe248bb02c003fe9f',
    2500000,
    3,
    1,
  );

  console.log(`vrf manager deployed to: ${vrfManager.address}`);
}

async function main() {
  await upgradeBurn();
  //   await deployGenericBurn();
  //   await deployVRFManager();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
