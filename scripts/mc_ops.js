const { ethers } = require('hardhat');
let helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];

let chronos_tile = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];

let artemis_tile = [
  { x: -2, y: 0, z: 2 },
  { x: -2, y: 1, z: 1 },
  { x: -2, y: 2, z: 0 },
  { x: -1, y: -1, z: 2 },
  { x: -1, y: 2, z: -1 },
  { x: 0, y: -2, z: 2 },
  { x: 0, y: 2, z: -2 },
  { x: 1, y: -2, z: 1 },
  { x: 1, y: 1, z: -2 },
  { x: 2, y: -2, z: 0 },
  { x: 2, y: -1, z: -1 },
  { x: 2, y: 0, z: -2 },
];

let apollo_tile = [
  { x: -1, y: 0, z: 1 },
  { x: -1, y: 1, z: 0 },
  { x: 0, y: -1, z: 1 },
  { x: 0, y: 1, z: -1 },
  { x: 1, y: -1, z: 0 },
  { x: 1, y: 0, z: -1 },
];
const roverAddress = '0x5B183670817aC4b59eAa758eE08D2cd9e889D971';

async function main() {
  const MCFactory = await ethers.getContractFactory('MissionControl');
  const mc = MCFactory.attach('0x0F4497805302E901fD9d4131eA70240474240a5d');

  const PIXFactory = await ethers.getContractFactory('ERC721PIXMock');
  const pix = PIXFactory.attach('0x215e26B3D4fD1E73837254B809e2B6A2c6967781');

  const RoverFactory = await ethers.getContractFactory('Rover');
  const rover = RoverFactory.attach(roverAddress);

  let ring = helios_tiles;
  let roverAdapter = '0x754895592FdDba6A5f0947cc08b393DA34C6CCAF';
  let pixAdapter = '0xa0C5A189733969d0F962Bc5284EeDb463BdA49C3';
  let assetsAdapter = '0xd226FB0E5a54a23e7c0333E001C0f0F19AB565F8';
  let user = '0x7d922bf0975424b3371074f54cC784AF738Dac0D';
  let j = 0;
  // for (let i = 0; i < ring.length; i++) {
  //   let tileInfo = await mc.checkStakedOnTile(user, ring[i].x, ring[i].y, ring[i].z);
  //   if (tileInfo['_top1'].nonce != 0 && tileInfo['_top2'].nonce != 0) continue;

  //   let tx = await rover.trustedMint(user, 1, 0);
  //   await tx.wait();

  //   j++;
  //   let tokenId = 81 + j;
  //   const orderPlacement = {
  //     order: { x: ring[i].x, y: ring[i].y, z: ring[i].z },
  //     tokenId: tokenId,
  //     tokenAddress: tokenAddress,
  //   };

  //   console.log(`Now placing ${tokenId} on ${ring[i].x},${ring[i].y},${ring[i].z}`);
  //   await mc.placeNFT(orderPlacement);
  // }

  // let tx = await mc.removeNFT({ x: -1, y: 0, z: 1 }, 1);
  // await tx.wait();

  // let tx2 = await mc.removeNFT({ x: -1, y: 0, z: 1 }, 2);
  // await tx2.wait();

  try {
    // let tx = await rover.trustedMint(user, 2, 1);
    // console.log(tx);
    let tx = await mc.placeNFT({ order: { x: -1, y: 0, z: 1 }, tokenId: 23, tokenAddress: assetsAdapter });
    // console.log(tx.hash);
  } catch (error) {
    console.log(error);
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
