const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');

async function main() {

  const LC3 = await hre.ethers.getContractFactory('LuckyCatRaffleV3');
  const lc = await LC3.attach('0x1f1045a8C9e957D698ED09786d21FC596a483105');

  const Airdrop = await hre.ethers.getContractFactory('Airdrop');
  const airdrop = await Airdrop.attach('0x2334bC478C60beE7674529580659CD6c115cBCbC');
  const IXT = await hre.ethers.getContractFactory('PIXT');
  const ixt = await IXT.attach('0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE');
  const csvFilePath = './scripts/lucky_cat_setup_userAtIndex.csv';

  const data = await csv().fromFile(csvFilePath);

  let index = 0;
  let value = 0;

  while (index < data.length) {
    // reset arrays
    recipients = [];
    indices = [];
    // fill up array with next 150 items
    for (let i = 0; i < 150; i++) {

      // break out of here if data is complete, batch is full
      if (index == data.length) {
        break;
      }

      value = parseInt(data[index]['raffleId']);
      recipients.push(data[index]['from_address']);
      indices.push(parseInt(data[index]['player_index']));
      index++;
    }

    /*const response = await airdrop.setUserAtIndex(
      value,
      indices,
      recipients,
      { gasPrice: 180000000000, gasLimit: 9000000 },
    );

    const receipt = await response.wait(1);
    console.log(receipt);*/
    console.log(value);
    console.log(indices);
    console.log(recipients);
    console.log('============================', index);
  }
}

main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
