const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('MissionControlStaking');
  const contract = await upgrades.deployProxy(ContractFactory, []);
  await contract.deployed();

  console.log('MC STAKING Deployed at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
