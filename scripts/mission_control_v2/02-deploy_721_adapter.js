const { ethers, upgrades } = require('hardhat');
const { AssetIds, Distribution, PIXCategory } = require('../../test/utils');

const PIX_CONTRACT = '0x215e26B3D4fD1E73837254B809e2B6A2c6967781';
const MC_V2 = '0x0F4497805302E901fD9d4131eA70240474240a5d';
const PIX_WASTE_EV = 6;
const PIX_WASTE_CAP = 3;

const PIX_WASTE_EV_BOOSTED = 12;
const PIX_WASTE_CAP_BOOSTED = 3;

const PIX_WASTE_EV_CHRONOS = 15;
const PIX_WASTE_EV_HELIOS = 18;

let helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];

let chronos_tile = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];

async function setup_c_ring() {
  let factory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
  let contract = await factory.attach('0x56B4d5e18441fdd09bF4Ad69C484866E92B8650e');

  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setResourceParameters(
      AssetIds.Waste,
      PIX_WASTE_EV_HELIOS,
      PIX_WASTE_CAP,
      true,
      true,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }

  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setResourceParameters(
      AssetIds.Waste,
      PIX_WASTE_EV_CHRONOS,
      PIX_WASTE_CAP,
      true,
      true,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}

async function deploy_pix_stakeable(mission_control, pix_address) {
  let factory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
  let contract = await upgrades.deployProxy(factory, []);
  await contract.deployed();
  console.log('Deployed ERC721MCStakeableAdapter at: ', contract.address);
  await contract.setMissionControl(mission_control);
  await contract.setERC721(pix_address);
  return contract;
}

async function setup_pix(stakeable) {
  let factory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setResourceParameters(
            AssetIds.Waste,
            PIX_WASTE_EV,
            PIX_WASTE_CAP,
            true,
            true,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setResourceParameters(
            AssetIds.Waste,
            PIX_WASTE_EV_BOOSTED,
            PIX_WASTE_CAP_BOOSTED,
            true,
            true,
            i,
            j,
          );
        }
      }
    }
  }
}

async function main() {
  let erc721Adapter = await deploy_pix_stakeable(MC_V2, PIX_CONTRACT);
  await setup_pix(erc721Adapter.address);
  await setup_c_ring();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
