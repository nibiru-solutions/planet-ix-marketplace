const { ethers, upgrades } = require('hardhat');
const { AssetIds, Distribution, PIXCategory } = require('../../test/utils');

const MC_V2 = '0x24E541A5C32830A4E8b89846fd4Bf86E294dd3cb';
const ROVER_CONTRACT = '0x836545dFc47F71ff33C458B98b283732A8ceE4F6';
const PIX_ASSET_ADAPTER = '0xDE953d986Df939E3ed802337Be363D91123ce699';
const PIX721_ADAPTER = '0x56B4d5e18441fdd09bF4Ad69C484866E92B8650e';

const VRF_COORDINATOR = '0xAE975071Be8F8eE67addBC1A82488F1C24858067';
const ASSETS = '0xba6666B118f8303F990f3519DF07e160227cCE87';

const KEYHASH = '0xd729dc84e21ae57ffb6be0053bf2b0668aa2aaf300a2a7b2ddf7dc0bb6e875a8';
const CALLBACK_GAS_LIMIT = 2500000;
const NUM_REQUESTS = 3;
const SUBSCRIPTION_ID = 358;

const GR_AP = {
  token_id: 1,
  resource_ev: 240,
  resource_id: AssetIds.Waste,
  resource_cap: 240,
};
const GR_ART = {
  token_id: 1,
  resource_ev: 288,
  resource_id: AssetIds.Waste,
  resource_cap: 288,
};
const GR_CHRON = {
  token_id: 1,
  resource_ev: 288,
  resource_id: AssetIds.Waste,
  resource_cap: 288,
};
const GR_HE = {
  token_id: 1,
  resource_ev: 312,
  resource_id: AssetIds.Waste,
  resource_cap: 312,
};
const PR_AP = {
  token_id: 2,
  resource_ev: 160,
  resource_id: AssetIds.Waste,
  resource_cap: 160,
};
const PR_ART = {
  token_id: 2,
  resource_ev: 192,
  resource_id: AssetIds.Waste,
  resource_cap: 192,
};
const PR_CHRON = {
  token_id: 2,
  resource_ev: 192,
  resource_id: AssetIds.Waste,
  resource_cap: 192,
};
const PR_HE = {
  token_id: 2,
  resource_ev: 208,
  resource_id: AssetIds.Waste,
  resource_cap: 208,
};
const NR_AP = {
  token_id: 0,
  resource_ev: 300,
  resource_id: AssetIds.Waste,
  resource_cap: 300,
};
const NR_ART = {
  token_id: 0,
  resource_ev: 360,
  resource_id: AssetIds.Waste,
  resource_cap: 360,
};
const NR_CHRON = {
  token_id: 0,
  resource_ev: 360,
  resource_id: AssetIds.Waste,
  resource_cap: 360,
};
const NR_HE = {
  token_id: 0,
  resource_ev: 390,
  resource_id: AssetIds.Waste,
  resource_cap: 390,
};

const apollo_tiles = [
  { x: -1, y: 0, z: 1 },
  { x: -1, y: 1, z: 0 },
  { x: 0, y: -1, z: 1 },
  { x: 0, y: 1, z: -1 },
  { x: 1, y: -1, z: 0 },
  { x: 1, y: 0, z: -1 },
];
const artemis_tiles = [
  { x: 0, y: -2, z: 2 },
  { x: 1, y: -2, z: 1 },
  { x: 2, y: -2, z: 0 },
  { x: 2, y: -1, z: -1 },
  { x: 2, y: 0, z: -2 },
  { x: 1, y: 1, z: -2 },
  { x: 0, y: 2, z: -2 },
  { x: -1, y: 2, z: -1 },
  { x: -2, y: 2, z: 0 },
  { x: -2, y: 1, z: 1 },
  { x: -2, y: 0, z: 2 },
  { x: -1, y: -1, z: 2 },
];
const chronos_tiles = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];
const helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];

async function deploy_rover_stakeable() {
  let factory = await ethers.getContractFactory('RoverMCStakeableAdapter');
  let contract = await upgrades.deployProxy(factory, [VRF_COORDINATOR, ASSETS, 0, KEYHASH, CALLBACK_GAS_LIMIT, NUM_REQUESTS, SUBSCRIPTION_ID]);
  await contract.deployed();
  console.log('Deployed RoverMCStakeableAdapter at: ', contract.address);
  await contract.setMissionControl(MC_V2);
  await contract.setPixAssetAdapter(PIX_ASSET_ADAPTER);
  await contract.setERC721(ROVER_CONTRACT);
  return contract;
}

async function setup_resource_params(ADAPTER) {
  let factory = await ethers.getContractFactory('RoverMCStakeableAdapter');
  let contract = await factory.attach(ADAPTER);

  await contract.setRings([
    { x: apollo_tiles[0].x, y: apollo_tiles[0].y, ring: 0 },
    { x: apollo_tiles[1].x, y: apollo_tiles[1].y, ring: 0 },
    { x: apollo_tiles[2].x, y: apollo_tiles[2].y, ring: 0 },
    { x: apollo_tiles[3].x, y: apollo_tiles[3].y, ring: 0 },
    { x: apollo_tiles[4].x, y: apollo_tiles[4].y, ring: 0 },
    { x: apollo_tiles[5].x, y: apollo_tiles[5].y, ring: 0 },
    { x: artemis_tiles[0].x, y: artemis_tiles[0].y, ring: 1 },
    { x: artemis_tiles[1].x, y: artemis_tiles[1].y, ring: 1 },
    { x: artemis_tiles[2].x, y: artemis_tiles[2].y, ring: 1 },
    { x: artemis_tiles[3].x, y: artemis_tiles[3].y, ring: 1 },
    { x: artemis_tiles[4].x, y: artemis_tiles[4].y, ring: 1 },
    { x: artemis_tiles[5].x, y: artemis_tiles[5].y, ring: 1 },
    { x: artemis_tiles[6].x, y: artemis_tiles[6].y, ring: 1 },
    { x: artemis_tiles[7].x, y: artemis_tiles[7].y, ring: 1 },
    { x: artemis_tiles[8].x, y: artemis_tiles[8].y, ring: 1 },
    { x: artemis_tiles[9].x, y: artemis_tiles[9].y, ring: 1 },
    { x: artemis_tiles[10].x, y: artemis_tiles[10].y, ring: 1 },
    { x: artemis_tiles[11].x, y: artemis_tiles[11].y, ring: 1 },
    { x: chronos_tiles[0].x, y: chronos_tiles[0].y, ring: 2 },
    { x: chronos_tiles[1].x, y: chronos_tiles[1].y, ring: 2 },
    { x: chronos_tiles[2].x, y: chronos_tiles[2].y, ring: 2 },
    { x: chronos_tiles[3].x, y: chronos_tiles[3].y, ring: 2 },
    { x: chronos_tiles[4].x, y: chronos_tiles[4].y, ring: 2 },
    { x: chronos_tiles[5].x, y: chronos_tiles[5].y, ring: 2 },
    { x: chronos_tiles[6].x, y: chronos_tiles[6].y, ring: 2 },
    { x: chronos_tiles[7].x, y: chronos_tiles[7].y, ring: 2 },
    { x: chronos_tiles[8].x, y: chronos_tiles[8].y, ring: 2 },
    { x: chronos_tiles[9].x, y: chronos_tiles[9].y, ring: 2 },
    { x: chronos_tiles[10].x, y: chronos_tiles[10].y, ring: 2 },
    { x: chronos_tiles[11].x, y: chronos_tiles[11].y, ring: 2 },
    { x: helios_tiles[0].x, y: helios_tiles[0].y, ring: 3 },
    { x: helios_tiles[1].x, y: helios_tiles[1].y, ring: 3 },
    { x: helios_tiles[2].x, y: helios_tiles[2].y, ring: 3 },
    { x: helios_tiles[3].x, y: helios_tiles[3].y, ring: 3 },
    { x: helios_tiles[4].x, y: helios_tiles[4].y, ring: 3 },
    { x: helios_tiles[5].x, y: helios_tiles[5].y, ring: 3 },
  ]);

  console.log('Rover rings set');

  await contract.setResourceParametersBulk([
    { tier: PR_AP.token_id, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, ring: 0 },
    { tier: GR_AP.token_id, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, ring: 0 },
    { tier: NR_AP.token_id, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, ring: 0 },
    { tier: PR_ART.token_id, resourceEV: PR_ART.resource_ev, resourceId: PR_ART.resource_id, resourceCap: PR_ART.resource_cap, isBase: false, isRaidable: true, ring: 1 },
    { tier: GR_ART.token_id, resourceEV: GR_ART.resource_ev, resourceId: GR_ART.resource_id, resourceCap: GR_ART.resource_cap, isBase: false, isRaidable: true, ring: 1 },
    { tier: NR_ART.token_id, resourceEV: NR_ART.resource_ev, resourceId: NR_ART.resource_id, resourceCap: NR_ART.resource_cap, isBase: false, isRaidable: true, ring: 1 },
    { tier: PR_CHRON.token_id, resourceEV: PR_CHRON.resource_ev, resourceId: PR_CHRON.resource_id, resourceCap: PR_CHRON.resource_cap, isBase: false, isRaidable: true, ring: 2 },
    { tier: GR_CHRON.token_id, resourceEV: GR_CHRON.resource_ev, resourceId: GR_CHRON.resource_id, resourceCap: GR_CHRON.resource_cap, isBase: false, isRaidable: true, ring: 2 },
    { tier: NR_CHRON.token_id, resourceEV: NR_CHRON.resource_ev, resourceId: NR_CHRON.resource_id, resourceCap: NR_CHRON.resource_cap, isBase: false, isRaidable: true, ring: 2 },
    { tier: PR_HE.token_id, resourceEV: PR_HE.resource_ev, resourceId: PR_HE.resource_id, resourceCap: PR_HE.resource_cap, isBase: false, isRaidable: true, ring: 3 },
    { tier: GR_HE.token_id, resourceEV: GR_HE.resource_ev, resourceId: GR_HE.resource_id, resourceCap: GR_HE.resource_cap, isBase: false, isRaidable: true, ring: 3 },
    { tier: NR_HE.token_id, resourceEV: NR_HE.resource_ev, resourceId: NR_HE.resource_id, resourceCap: NR_HE.resource_cap, isBase: false, isRaidable: true, ring: 3 },
  ]);

  console.log('Rover resource params set');
}

async function upgrade_contracts(ADAPTER) {
  const mcFactory = await ethers.getContractFactory('MissionControl');
  const mc = await upgrades.upgradeProxy(MC_V2, mcFactory);
  await mc.deployed();
  console.log('Mission Control Upgraded at', mc.address);

  await mc.setWhitelist(ADAPTER, true);

  const assetsAdapterFactory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  const assetsAdapter = await upgrades.upgradeProxy(PIX_ASSET_ADAPTER, assetsAdapterFactory);
  await assetsAdapter.deployed();
  console.log('PIX Assets Adapter Upgraded at', assetsAdapter.address);

  await assetsAdapter.setRover(ROVER_CONTRACT);
  await assetsAdapter.setRoverAdpater(ADAPTER);
  await assetsAdapter.setDroneTier([5, 4, 33], [2, 1, 0]);

  const pix721AdapterFactory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
  const pixAdapter = await upgrades.upgradeProxy(PIX721_ADAPTER, pix721AdapterFactory);
  await pixAdapter.deployed();
  console.log('PIX 721 Adapter Upgraded at', pixAdapter.address);
}

async function deploy_mc_tile_status() {
  const tileStatusFactory = await ethers.getContractFactory('MissionControlTileStatus');
  const tileStatus = await upgrades.deployProxy(tileStatusFactory, []);

  await tileStatus.deployed();
  await tileStatus.setMC(MC_V2);
  console.log('MC Tile Status Deployed To:', tileStatus.address);
}

async function main() {
  let roverAdapter = await deploy_rover_stakeable();
  await setup_resource_params(roverAdapter.address);
  await upgrade_contracts(roverAdapter.address);
  await deploy_mc_tile_status();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
