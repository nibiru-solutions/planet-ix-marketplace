const { ethers, upgrades } = require('hardhat');
const { AssetIds, Distribution, PIXCategory } = require('../../test/utils');

async function main() {
  let factory = await ethers.getContractFactory('MissionControlStaking');
  let contract = await factory.attach('0xc63352aCc1E22D74418f5c735adcf3aa23d0b078');

  await contract.whitelistTokens('0xba6666B118f8303F990f3519DF07e160227cCE87', [37, 38], true);
  await contract.setUnstakeProvider(
    '0xba6666B118f8303F990f3519DF07e160227cCE87',
    [37, 38],
    '0x03aDca77982A16376FA95776FC426598f4d037cC',
  );
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
