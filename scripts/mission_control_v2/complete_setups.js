const { ethers, upgrades } = require('hardhat');
const { AssetIds, Distribution, PIXCategory } = require('../../test/utils');

const PLACE_AGOLD = '0x3CAD7147c15C0864B8cF0EcCca43f98735e6e782';
const PLACE_AGOLD_LITE = '0x1B4a41740B5eF7FBBD4b30793a80635709aa5d8D';

async function setup_mc() {
  let factory = await ethers.getContractFactory('MissionControl');
  let contract = await factory.attach('0x0F4497805302E901fD9d4131eA70240474240a5d');

  await contract.setWhitelist('0xa0C5A189733969d0F962Bc5284EeDb463BdA49C3', true);
  await contract.setWhitelist('0xd226FB0E5a54a23e7c0333E001C0f0F19AB565F8', true);

  await contract.setMint('0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc');

  await contract.setRadius(3);

  await contract.setRaidSigner('0x7d922bf0975424b3371074f54cC784AF738Dac0D', 1);

  await contract.setStreamer('0xF35b29f24D90300EB5C762e4dbF64C9A209f0DCF');
  await contract.setAGold(PLACE_AGOLD);
  await contract.setAGoldLite(PLACE_AGOLD_LITE);

  await contract.setMissionControlStaking('0xc9866595453069eC204cc1E6d38bcC6dd8DE5dC7');

  await contract.setTileContracts('0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc');

  await contract.setCrosschainServices('0x44932f90d6Dc779D48Ec4964F1fD3c63cf333E57');

  await contract.setSignature(true);
  await contract.setTileContractLockProvider('0xD732Aa0c6eB8566434FD3e10A83D81a51da8F24B');
}

async function setup_crosschain_services() {
  let factory = await ethers.getContractFactory('MCCrosschainServices');
  let contract = await factory.attach('0x44932f90d6Dc779D48Ec4964F1fD3c63cf333E57');

  await contract.setOriginDomain(1735353714);
  await contract.setSource('0x8687d3e34ed36c9c36A798e90192D7c6A9ed7a7A');
  await contract.setConnext('0xFCa08024A6D4bCc87275b1E4A1E22B71fAD7f649');
  await contract.setMissionControl('0x0F4497805302E901fD9d4131eA70240474240a5d');
  await contract.setAGold(PLACE_AGOLD);
  await contract.setAGoldLite(PLACE_AGOLD_LITE);
}

async function setup_mission_control_staking_polygon() {
  let factory = await ethers.getContractFactory('MissionControlStaking');
  let contract = await factory.attach('0xc9866595453069eC204cc1E6d38bcC6dd8DE5dC7');

  await contract.whitelistTokens(
    '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    [35, 36, 37, 38],
    true,
  );
  await contract.setUnstakeProvider(
    '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    [35, 36, 37, 38],
    '0xD732Aa0c6eB8566434FD3e10A83D81a51da8F24B',
  );
}

async function setup_mission_control_staking_eth() {
  let factory = await ethers.getContractFactory('MissionControlStaking');
  let contract = await factory.attach('0x6C84fE21309D94Cc8B7ccFe8F53A47A5b3111587');

  await contract.whitelistTokens('0xba6666B118f8303F990f3519DF07e160227cCE87', [3], true);
  await contract.setUnstakeProvider(
    '0xba6666B118f8303F990f3519DF07e160227cCE87',
    [3],
    '0x765D46F721CDE3b5A2f48011eFC53C3A79567D74',
  );
}

async function setup_newlands_genesis_lock_provider_eth() {
  let factory = await ethers.getContractFactory('NewlandsGenesisLockProvider');
  let contract = await factory.attach('0x765D46F721CDE3b5A2f48011eFC53C3A79567D74');

  await contract.setConnext('0x8898B472C54c31894e3B9bb83cEA802a5d0e63C6');
  await contract.setDestinationDomain(1886350457);
  await contract.setTarget('0xe210c6E2A9114E9f276eeD8a428b35308090Ab1f');
  await contract.setMCStaking('0x6C84fE21309D94Cc8B7ccFe8F53A47A5b3111587');
}

async function setup_tile_contract_lock_provider() {
  let factory = await ethers.getContractFactory('TileContractLockProvider');
  let contract = await factory.attach('0xD732Aa0c6eB8566434FD3e10A83D81a51da8F24B');

  await contract.setMCStaking('0xc9866595453069eC204cc1E6d38bcC6dd8DE5dC7');
  await contract.setMissionControl('0x0F4497805302E901fD9d4131eA70240474240a5d');
}

async function main() {
  await setup_mc();
  await setup_crosschain_services();
  await setup_mission_control_staking_polygon();
  await setup_tile_contract_lock_provider();
  // await setup_mission_control_staking_eth();
  // await setup_newlands_genesis_lock_provider_eth();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
