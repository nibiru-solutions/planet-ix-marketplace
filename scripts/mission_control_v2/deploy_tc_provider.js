const { ethers, upgrades } = require('hardhat');

const INNER_RING_TILE_CONTRACT = 35;
const OUTER_RING_TILE_CONTRACT = 36;
const HELIOS_TILE_CONTRACT_ID = 37;
const CHRONOS_TILE_CONTRACT_ID = 38;

let helios_tiles = [
  [-3, 0, 3],
  [-3, 3, 0],
  [0, -3, 3],
  [0, 3, -3],
  [3, -3, 0],
  [3, 0, -3],
];

let chronos_tiles = [
  [-3, 1, 2],
  [-3, 2, 1],
  [-2, -1, 3],
  [-2, 3, -1],
  [-1, -2, 3],
  [-1, 3, -2],
  [1, -3, 2],
  [1, 2, -3],
  [2, -3, 1],
  [2, 1, -3],
  [3, -2, -1],
  [3, -1, -2],
];

async function main() {
  // const ContractFactory = await ethers.getContractFactory('TileContractLockProvider');
  // const contract = await upgrades.deployProxy(ContractFactory, []);
  // await contract.deployed();

  // await contract.addCoords(INNER_RING_TILE_CONTRACT, [
  //   [0, -1, 1],
  //   [1, -1, 0],
  //   [1, 0, -1],
  //   [0, 1, -1],
  //   [-1, 1, 0],
  //   [-1, 0, 1],
  // ]);

  // await contract.addCoords(OUTER_RING_TILE_CONTRACT, [
  //   [0, -2, 2],
  //   [1, -2, 1],
  //   [2, -2, 0],
  //   [2, -1, -1],
  //   [2, 0, -2],
  //   [1, 1, -2],
  //   [0, 2, -2],
  //   [-1, 2, -1],
  //   [-2, 2, 0],
  //   [-2, 1, 1],
  //   [-2, 0, 2],
  //   [-1, -1, 2],
  // ]);

  const ContractFactory = await ethers.getContractFactory('TileContractLockProvider');
  const contract = await ContractFactory.attach('0x03aDca77982A16376FA95776FC426598f4d037cC');

  await contract.addCoords(CHRONOS_TILE_CONTRACT_ID, chronos_tiles);
  await contract.addCoords(HELIOS_TILE_CONTRACT_ID, helios_tiles);

  console.log('Tile Contract Lock Provider Deployed at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
