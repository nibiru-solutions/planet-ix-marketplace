const { ethers, upgrades } = require('hardhat');

const MC_V2_RADIUS = 3;
const MC_V2_MINT = '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc';

const MC_RAID_TIMEOUT = 30;
const RAID_SIGNER = '0x7d922bf0975424b3371074f54cC784AF738Dac0D';

const SET_SIGNATURE = true;

const FLOW_RATE_PER_TILE = 385802469135;

const INNER_RING_TILE_CONTRACT_ID = 35;
const OUTER_RING_TILE_CONTRACT_ID = 36;
const HELIOS_TILE_CONTRACT_ID = 37;
const CHRONOS_TILE_CONTRACT_ID = 38;

let helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];

let chronos_tile = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];

async function setup_c_ring() {
  let factory = await ethers.getContractFactory('MissionControl');
  let contract = await factory.attach('0x24E541A5C32830A4E8b89846fd4Bf86E294dd3cb');

  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTileRequirements(
      helios_tiles[i].x,
      helios_tiles[i].y,
      FLOW_RATE_PER_TILE,
      HELIOS_TILE_CONTRACT_ID,
    );
  }

  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTileRequirements(
      chronos_tile[i].x,
      chronos_tile[i].y,
      FLOW_RATE_PER_TILE,
      CHRONOS_TILE_CONTRACT_ID,
    );
  }
}

async function main() {
  // const MissionControlV2Factory = await ethers.getContractFactory('MissionControl');
  // const missionControlV2 = await upgrades.deployProxy(MissionControlV2Factory, [
  //   MC_V2_RADIUS,
  //   MC_V2_MINT,
  // ]);
  // await missionControlV2.deployed();
  // console.log('MC STAKING Deployed at', missionControlV2.address);
  // await missionControlV2.setRaidParams(MC_RAID_TIMEOUT);
  // await missionControlV2.setRaidSigner(RAID_SIGNER, true);
  // await missionControlV2.setSignature(SET_SIGNATURE);
  // for (let i = -2; i <= 2; i++) {
  //   for (let j = -2; j <= 2; j++) {
  //     for (let k = -2; k <= 2; k++) {
  //       let sum = i + j + k;
  //       let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
  //       if (rad == 1 && sum == 0) {
  //         console.log(`Now setting up tile ${i},${j}`);
  //         await missionControlV2.setTileRequirements(i, j, 0, INNER_RING_TILE_CONTRACT_ID);
  //       }
  //       if (rad == 2 && sum == 0) {
  //         console.log(`Now setting up tile ${i},${j}`);
  //         await missionControlV2.setTileRequirements(
  //           i,
  //           j,
  //           FLOW_RATE_PER_TILE,
  //           OUTER_RING_TILE_CONTRACT_ID,
  //         );
  //       }
  //     }
  //   }
  // }
  await setup_c_ring();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
