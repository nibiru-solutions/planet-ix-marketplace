const { ethers, upgrades } = require('hardhat');
const { AssetIds, Distribution, PIXCategory } = require('../../test/utils');

const ASSETS_CONTRACT = '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc';
const PIX_CONTRACT = '0x215e26B3D4fD1E73837254B809e2B6A2c6967781';

const PIX_STAKEABLE = '0xa0C5A189733969d0F962Bc5284EeDb463BdA49C3';
const MC_V2 = '0x0F4497805302E901fD9d4131eA70240474240a5d';

const BASIC_FACILITY_RATE = 10000;
const BOOSTED_FACILITY_RATE = 11000;

const CHRONOS_FACILITY_RATE = 11000;
const HELIOS_FACILITY_RATE = 12000;

let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
let contract = await factory.attach('0xDE953d986Df939E3ed802337Be363D91123ce699');

let helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];

let chronos_tile = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];

let DRONE_GENESIS = {
  token_id: 4,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 22,
  resource_id: AssetIds.Waste,
  resource_cap: 44,
};
let DRONE_GENESIS_BOOSTED = {
  token_id: 4,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 27,
  resource_id: AssetIds.Waste,
  resource_cap: 54,
};
let DRONE_GENESIS_CHRONOS = {
  token_id: 4,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 27,
  resource_id: AssetIds.Waste,
  resource_cap: 54,
};
let DRONE_GENESIS_HELIOS = {
  token_id: 4,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 29,
  resource_id: AssetIds.Waste,
  resource_cap: 58,
};
let DRONE_PIERCER = {
  token_id: 5,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 18,
  resource_id: AssetIds.Waste,
  resource_cap: 36,
};
let DRONE_PIERCER_BOOSTED = {
  token_id: 5,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 22,
  resource_id: AssetIds.Waste,
  resource_cap: 44,
};
let DRONE_PIERCER_CHRONOS = {
  token_id: 5,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 22,
  resource_id: AssetIds.Waste,
  resource_cap: 44,
};
let DRONE_PIERCER_HELIOS = {
  token_id: 5,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 24,
  resource_id: AssetIds.Waste,
  resource_cap: 47,
};
let NIGHT_DRONE = {
  token_id: 33,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 30,
  resource_id: AssetIds.Waste,
  resource_cap: 60,
};
let NIGHT_DRONE_BOOSTED = {
  token_id: 33,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 36,
  resource_id: AssetIds.Waste,
  resource_cap: 72,
};
let NIGHT_DRONE_CHRONOS = {
  token_id: 33,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 36,
  resource_id: AssetIds.Waste,
  resource_cap: 72,
};
let NIGHT_DRONE_HELIOS = {
  token_id: 33,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 39,
  resource_id: AssetIds.Waste,
  resource_cap: 78,
};
let FACILITY_OUTLIER = {
  token_id: 19,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let FACILITY_COMMON = {
  token_id: 20,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let FACILITY_UNCOMMON = {
  token_id: 21,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let FACILITY_RARE = {
  token_id: 22,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let FACILITY_LEGENDARY = {
  token_id: 23,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

async function deploy_asset_stakeable(
  asset_address,
  pix_address,
  pix_stakeable_address,
  mission_control,
) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await upgrades.deployProxy(factory);
  await contract.deployed();
  console.log('Deployed PIXAssetStakeableAdapter at: ', contract.address);

  await contract.setERC1155(asset_address);
  await contract.setRarityProvider(pix_address, pix_stakeable_address);

  await contract.setFacilityStakeRequirements(AssetIds.FacilityOutlier, PIXCategory.Outliers, true);
  await contract.setFacilityStakeRequirements(AssetIds.FacilityCommon, PIXCategory.Common, true);
  await contract.setFacilityStakeRequirements(
    AssetIds.FacilityUncommon,
    PIXCategory.Uncommon,
    true,
  );
  await contract.setFacilityStakeRequirements(AssetIds.FacilityRare, PIXCategory.Rare, true);
  await contract.setFacilityStakeRequirements(
    AssetIds.FacilityLegendary,
    PIXCategory.Legendary,
    true,
  );

  let hour = 60 * 60;
  await contract.setFacilityStats(
    [8 * hour, 12 * hour, 16 * hour, 20 * hour, 24 * hour],
    [1, 3, 4, 5, 6],
  );

  await contract.setMissionControl(mission_control);

  return contract;
}

async function setup_genesis_drone(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            DRONE_GENESIS.token_id,
            DRONE_GENESIS.is_stakeable,
            DRONE_GENESIS.is_base,
            DRONE_GENESIS.is_raidable,
            DRONE_GENESIS.distribution,
            DRONE_GENESIS.resource_ev,
            DRONE_GENESIS.resource_id,
            DRONE_GENESIS.resource_cap,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            DRONE_GENESIS_BOOSTED.token_id,
            DRONE_GENESIS_BOOSTED.is_stakeable,
            DRONE_GENESIS_BOOSTED.is_base,
            DRONE_GENESIS_BOOSTED.is_raidable,
            DRONE_GENESIS_BOOSTED.distribution,
            DRONE_GENESIS_BOOSTED.resource_ev,
            DRONE_GENESIS_BOOSTED.resource_id,
            DRONE_GENESIS_BOOSTED.resource_cap,
            i,
            j,
          );
        }
      }
    }
  }
}

async function setup_piercer_drone(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            DRONE_PIERCER.token_id,
            DRONE_PIERCER.is_stakeable,
            DRONE_PIERCER.is_base,
            DRONE_PIERCER.is_raidable,
            DRONE_PIERCER.distribution,
            DRONE_PIERCER.resource_ev,
            DRONE_PIERCER.resource_id,
            DRONE_PIERCER.resource_cap,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            DRONE_PIERCER_BOOSTED.token_id,
            DRONE_PIERCER_BOOSTED.is_stakeable,
            DRONE_PIERCER_BOOSTED.is_base,
            DRONE_PIERCER_BOOSTED.is_raidable,
            DRONE_PIERCER_BOOSTED.distribution,
            DRONE_PIERCER_BOOSTED.resource_ev,
            DRONE_PIERCER_BOOSTED.resource_id,
            DRONE_PIERCER_BOOSTED.resource_cap,
            i,
            j,
          );
        }
      }
    }
  }
}

async function setup_night_drone(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            NIGHT_DRONE.token_id,
            NIGHT_DRONE.is_stakeable,
            NIGHT_DRONE.is_base,
            NIGHT_DRONE.is_raidable,
            NIGHT_DRONE.distribution,
            NIGHT_DRONE.resource_ev,
            NIGHT_DRONE.resource_id,
            NIGHT_DRONE.resource_cap,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            NIGHT_DRONE_BOOSTED.token_id,
            NIGHT_DRONE_BOOSTED.is_stakeable,
            NIGHT_DRONE_BOOSTED.is_base,
            NIGHT_DRONE_BOOSTED.is_raidable,
            NIGHT_DRONE_BOOSTED.distribution,
            NIGHT_DRONE_BOOSTED.resource_ev,
            NIGHT_DRONE_BOOSTED.resource_id,
            NIGHT_DRONE_BOOSTED.resource_cap,
            i,
            j,
          );
        }
      }
    }
  }
}

async function setup_facility_outlier(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_OUTLIER.token_id,
            FACILITY_OUTLIER.is_stakeable,
            FACILITY_OUTLIER.is_base,
            FACILITY_OUTLIER.is_raidable,
            FACILITY_OUTLIER.distribution,
            FACILITY_OUTLIER.resource_ev,
            FACILITY_OUTLIER.resource_id,
            FACILITY_OUTLIER.resource_cap,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_OUTLIER.token_id,
            FACILITY_OUTLIER.is_stakeable,
            FACILITY_OUTLIER.is_base,
            FACILITY_OUTLIER.is_raidable,
            FACILITY_OUTLIER.distribution,
            FACILITY_OUTLIER.resource_ev,
            FACILITY_OUTLIER.resource_id,
            FACILITY_OUTLIER.resource_cap,
            i,
            j,
          );
        }
      }
    }
  }
}

async function setup_facility_common(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_COMMON.token_id,
            FACILITY_COMMON.is_stakeable,
            FACILITY_COMMON.is_base,
            FACILITY_COMMON.is_raidable,
            FACILITY_COMMON.distribution,
            FACILITY_COMMON.resource_ev,
            FACILITY_COMMON.resource_id,
            FACILITY_COMMON.resource_cap,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_COMMON.token_id,
            FACILITY_COMMON.is_stakeable,
            FACILITY_COMMON.is_base,
            FACILITY_COMMON.is_raidable,
            FACILITY_COMMON.distribution,
            FACILITY_COMMON.resource_ev,
            FACILITY_COMMON.resource_id,
            FACILITY_COMMON.resource_cap,
            i,
            j,
          );
        }
      }
    }
  }
}

async function setup_facility_uncommon(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_UNCOMMON.token_id,
            FACILITY_UNCOMMON.is_stakeable,
            FACILITY_UNCOMMON.is_base,
            FACILITY_UNCOMMON.is_raidable,
            FACILITY_UNCOMMON.distribution,
            FACILITY_UNCOMMON.resource_ev,
            FACILITY_UNCOMMON.resource_id,
            FACILITY_UNCOMMON.resource_cap,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_UNCOMMON.token_id,
            FACILITY_UNCOMMON.is_stakeable,
            FACILITY_UNCOMMON.is_base,
            FACILITY_UNCOMMON.is_raidable,
            FACILITY_UNCOMMON.distribution,
            FACILITY_UNCOMMON.resource_ev,
            FACILITY_UNCOMMON.resource_id,
            FACILITY_UNCOMMON.resource_cap,
            i,
            j,
          );
        }
      }
    }
  }
}

async function setup_facility_rare(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_RARE.token_id,
            FACILITY_RARE.is_stakeable,
            FACILITY_RARE.is_base,
            FACILITY_RARE.is_raidable,
            FACILITY_RARE.distribution,
            FACILITY_RARE.resource_ev,
            FACILITY_RARE.resource_id,
            FACILITY_RARE.resource_cap,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_RARE.token_id,
            FACILITY_RARE.is_stakeable,
            FACILITY_RARE.is_base,
            FACILITY_RARE.is_raidable,
            FACILITY_RARE.distribution,
            FACILITY_RARE.resource_ev,
            FACILITY_RARE.resource_id,
            FACILITY_RARE.resource_cap,
            i,
            j,
          );
        }
      }
    }
  }
}

async function setup_facility_legendary(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_LEGENDARY.token_id,
            FACILITY_LEGENDARY.is_stakeable,
            FACILITY_LEGENDARY.is_base,
            FACILITY_LEGENDARY.is_raidable,
            FACILITY_LEGENDARY.distribution,
            FACILITY_LEGENDARY.resource_ev,
            FACILITY_LEGENDARY.resource_id,
            FACILITY_LEGENDARY.resource_cap,
            i,
            j,
          );
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setTokenInfo(
            FACILITY_LEGENDARY.token_id,
            FACILITY_LEGENDARY.is_stakeable,
            FACILITY_LEGENDARY.is_base,
            FACILITY_LEGENDARY.is_raidable,
            FACILITY_LEGENDARY.distribution,
            FACILITY_LEGENDARY.resource_ev,
            FACILITY_LEGENDARY.resource_id,
            FACILITY_LEGENDARY.resource_cap,
            i,
            j,
          );
        }
      }
    }
  }
}

async function setup_structure_bonuses(stakeable) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(stakeable);

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == 1 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setStructureBonuses(i, j, BASIC_FACILITY_RATE);
        }
        if (rad == 2 && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          await contract.setStructureBonuses(i, j, BOOSTED_FACILITY_RATE);
        }
      }
    }
  }
}

async function setup_genesis_drone_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTokenInfo(
      DRONE_GENESIS_CHRONOS.token_id,
      DRONE_GENESIS_CHRONOS.is_stakeable,
      DRONE_GENESIS_CHRONOS.is_base,
      DRONE_GENESIS_CHRONOS.is_raidable,
      DRONE_GENESIS_CHRONOS.distribution,
      DRONE_GENESIS_CHRONOS.resource_ev,
      DRONE_GENESIS_CHRONOS.resource_id,
      DRONE_GENESIS_CHRONOS.resource_cap,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}

async function setup_genesis_drone_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTokenInfo(
      DRONE_GENESIS_HELIOS.token_id,
      DRONE_GENESIS_HELIOS.is_stakeable,
      DRONE_GENESIS_HELIOS.is_base,
      DRONE_GENESIS_HELIOS.is_raidable,
      DRONE_GENESIS_HELIOS.distribution,
      DRONE_GENESIS_HELIOS.resource_ev,
      DRONE_GENESIS_HELIOS.resource_id,
      DRONE_GENESIS_HELIOS.resource_cap,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function setup_piercer_drone_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTokenInfo(
      DRONE_PIERCER_CHRONOS.token_id,
      DRONE_PIERCER_CHRONOS.is_stakeable,
      DRONE_PIERCER_CHRONOS.is_base,
      DRONE_PIERCER_CHRONOS.is_raidable,
      DRONE_PIERCER_CHRONOS.distribution,
      DRONE_PIERCER_CHRONOS.resource_ev,
      DRONE_PIERCER_CHRONOS.resource_id,
      DRONE_PIERCER_CHRONOS.resource_cap,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}

async function setup_piercer_drone_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTokenInfo(
      DRONE_PIERCER_HELIOS.token_id,
      DRONE_PIERCER_HELIOS.is_stakeable,
      DRONE_PIERCER_HELIOS.is_base,
      DRONE_PIERCER_HELIOS.is_raidable,
      DRONE_PIERCER_HELIOS.distribution,
      DRONE_PIERCER_HELIOS.resource_ev,
      DRONE_PIERCER_HELIOS.resource_id,
      DRONE_PIERCER_HELIOS.resource_cap,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function setup_night_drone_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTokenInfo(
      NIGHT_DRONE_CHRONOS.token_id,
      NIGHT_DRONE_CHRONOS.is_stakeable,
      NIGHT_DRONE_CHRONOS.is_base,
      NIGHT_DRONE_CHRONOS.is_raidable,
      NIGHT_DRONE_CHRONOS.distribution,
      NIGHT_DRONE_CHRONOS.resource_ev,
      NIGHT_DRONE_CHRONOS.resource_id,
      NIGHT_DRONE_CHRONOS.resource_cap,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}

async function setup_night_drone_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTokenInfo(
      NIGHT_DRONE_HELIOS.token_id,
      NIGHT_DRONE_HELIOS.is_stakeable,
      NIGHT_DRONE_HELIOS.is_base,
      NIGHT_DRONE_HELIOS.is_raidable,
      NIGHT_DRONE_HELIOS.distribution,
      NIGHT_DRONE_HELIOS.resource_ev,
      NIGHT_DRONE_HELIOS.resource_id,
      NIGHT_DRONE_HELIOS.resource_cap,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function setup_structure_bonuses_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setStructureBonuses(chronos_tile[i].x, chronos_tile[i].y, CHRONOS_FACILITY_RATE);
  }
}

async function setup_structure_bonuses_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setStructureBonuses(helios_tiles[i].x, helios_tiles[i].y, HELIOS_FACILITY_RATE);
  }
}

async function setup_facility_outlier_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTokenInfo(
      FACILITY_OUTLIER.token_id,
      FACILITY_OUTLIER.is_stakeable,
      FACILITY_OUTLIER.is_base,
      FACILITY_OUTLIER.is_raidable,
      FACILITY_OUTLIER.distribution,
      FACILITY_OUTLIER.resource_ev,
      FACILITY_OUTLIER.resource_id,
      FACILITY_OUTLIER.resource_cap,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}
async function setup_facility_outlier_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTokenInfo(
      FACILITY_OUTLIER.token_id,
      FACILITY_OUTLIER.is_stakeable,
      FACILITY_OUTLIER.is_base,
      FACILITY_OUTLIER.is_raidable,
      FACILITY_OUTLIER.distribution,
      FACILITY_OUTLIER.resource_ev,
      FACILITY_OUTLIER.resource_id,
      FACILITY_OUTLIER.resource_cap,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function setup_facility_common_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTokenInfo(
      FACILITY_COMMON.token_id,
      FACILITY_COMMON.is_stakeable,
      FACILITY_COMMON.is_base,
      FACILITY_COMMON.is_raidable,
      FACILITY_COMMON.distribution,
      FACILITY_COMMON.resource_ev,
      FACILITY_COMMON.resource_id,
      FACILITY_COMMON.resource_cap,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}
async function setup_facility_common_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTokenInfo(
      FACILITY_COMMON.token_id,
      FACILITY_COMMON.is_stakeable,
      FACILITY_COMMON.is_base,
      FACILITY_COMMON.is_raidable,
      FACILITY_COMMON.distribution,
      FACILITY_COMMON.resource_ev,
      FACILITY_COMMON.resource_id,
      FACILITY_COMMON.resource_cap,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function setup_facility_uncommon_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTokenInfo(
      FACILITY_UNCOMMON.token_id,
      FACILITY_UNCOMMON.is_stakeable,
      FACILITY_UNCOMMON.is_base,
      FACILITY_UNCOMMON.is_raidable,
      FACILITY_UNCOMMON.distribution,
      FACILITY_UNCOMMON.resource_ev,
      FACILITY_UNCOMMON.resource_id,
      FACILITY_UNCOMMON.resource_cap,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}
async function setup_facility_uncommon_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTokenInfo(
      FACILITY_UNCOMMON.token_id,
      FACILITY_UNCOMMON.is_stakeable,
      FACILITY_UNCOMMON.is_base,
      FACILITY_UNCOMMON.is_raidable,
      FACILITY_UNCOMMON.distribution,
      FACILITY_UNCOMMON.resource_ev,
      FACILITY_UNCOMMON.resource_id,
      FACILITY_UNCOMMON.resource_cap,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function setup_facility_rare_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTokenInfo(
      FACILITY_RARE.token_id,
      FACILITY_RARE.is_stakeable,
      FACILITY_RARE.is_base,
      FACILITY_RARE.is_raidable,
      FACILITY_RARE.distribution,
      FACILITY_RARE.resource_ev,
      FACILITY_RARE.resource_id,
      FACILITY_RARE.resource_cap,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}
async function setup_facility_rare_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTokenInfo(
      FACILITY_RARE.token_id,
      FACILITY_RARE.is_stakeable,
      FACILITY_RARE.is_base,
      FACILITY_RARE.is_raidable,
      FACILITY_RARE.distribution,
      FACILITY_RARE.resource_ev,
      FACILITY_RARE.resource_id,
      FACILITY_RARE.resource_cap,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function setup_facility_legendary_chronos() {
  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await contract.setTokenInfo(
      FACILITY_LEGENDARY.token_id,
      FACILITY_LEGENDARY.is_stakeable,
      FACILITY_LEGENDARY.is_base,
      FACILITY_LEGENDARY.is_raidable,
      FACILITY_LEGENDARY.distribution,
      FACILITY_LEGENDARY.resource_ev,
      FACILITY_LEGENDARY.resource_id,
      FACILITY_LEGENDARY.resource_cap,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }
}
async function setup_facility_legendary_helios() {
  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await contract.setTokenInfo(
      FACILITY_LEGENDARY.token_id,
      FACILITY_LEGENDARY.is_stakeable,
      FACILITY_LEGENDARY.is_base,
      FACILITY_LEGENDARY.is_raidable,
      FACILITY_LEGENDARY.distribution,
      FACILITY_LEGENDARY.resource_ev,
      FACILITY_LEGENDARY.resource_id,
      FACILITY_LEGENDARY.resource_cap,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function main() {
  // We should run these one by one
  // await setup_genesis_drone_chronos();
  // await setup_genesis_drone_helios();
  // await setup_piercer_drone_chronos();
  // await setup_piercer_drone_helios();
  // await setup_night_drone_chronos();
  // await setup_night_drone_helios();
  // await setup_structure_bonuses_chronos();
  // await setup_structure_bonuses_helios();
  // await setup_facility_outlier_chronos();
  // await setup_facility_outlier_helios();
  // await setup_facility_common_chronos();
  // await setup_facility_common_helios();
  // await setup_facility_uncommon_chronos();
  // await setup_facility_uncommon_helios();
  // await setup_facility_rare_chronos();
  // await setup_facility_rare_helios();
  // await setup_facility_legendary_chronos();
  // await setup_facility_legendary_helios();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
