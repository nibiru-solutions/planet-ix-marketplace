const { ethers, upgrades } = require('hardhat');
const { AssetIds, Distribution, PIXCategory } = require('../../test/utils');
const { displaySpinner } = require('../../utils/spinnerUtil');

const PIX_ASSET_ADAPTER = '0xDE953d986Df939E3ed802337Be363D91123ce699';

const PIERCER_DRONE_APOLLO = {
  token_id: 5,
  resource_ev: 18,
  resource_id: 7,
  resource_cap: 36,
  class: Distribution.LinearDeterministic,
};

const PIERCER_DRONE_ARTEMIS = {
  token_id: 5,
  resource_ev: 22,
  resource_id: 7,
  resource_cap: 44,
  class: Distribution.LinearDeterministic,
};

const PIERCER_DRONE_CHRONOS = {
  token_id: 5,
  resource_ev: 22,
  resource_id: 7,
  resource_cap: 44,
  class: Distribution.LinearDeterministic,
};

const PIERCER_DRONE_HELIOS = {
  token_id: 5,
  resource_ev: 23,
  resource_id: 7,
  resource_cap: 47,
  class: Distribution.LinearDeterministic,
};

const GENESIS_DRONE_APOLLO = {
  token_id: 4,
  resource_ev: 22,
  resource_id: 7,
  resource_cap: 44,
  class: Distribution.LinearDeterministic,
};

const GENESIS_DRONE_ARTEMIS = {
  token_id: 4,
  resource_ev: 28,
  resource_id: 7,
  resource_cap: 55,
  class: Distribution.LinearDeterministic,
};

const GENESIS_DRONE_CHRONOS = {
  token_id: 4,
  resource_ev: 27,
  resource_id: 7,
  resource_cap: 54,
  class: Distribution.LinearDeterministic,
};

const GENESIS_DRONE_HELIOS = {
  token_id: 4,
  resource_ev: 29,
  resource_id: 7,
  resource_cap: 58,
  class: Distribution.LinearDeterministic,
};

const NIGHT_DRONE_APOLLO = {
  token_id: 33,
  resource_ev: 30,
  resource_id: 7,
  resource_cap: 60,
  class: Distribution.LinearDeterministic,
};

const NIGHT_DRONE_ARTEMIS = {
  token_id: 33,
  resource_ev: 36,
  resource_id: 7,
  resource_cap: 72,
  class: Distribution.LinearDeterministic,
};

const NIGHT_DRONE_CHRONOS = {
  token_id: 33,
  resource_ev: 36,
  resource_id: 7,
  resource_cap: 72,
  class: Distribution.LinearDeterministic,
};

const NIGHT_DRONE_HELIOS = {
  token_id: 33,
  resource_ev: 39,
  resource_id: 7,
  resource_cap: 78,
  class: Distribution.LinearDeterministic,
};

const OUTLIER_FACILITY = {
  token_id: 19,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

const COMMON_FACILITY = {
  token_id: 20,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

const UNCOMMON_FACILITY = {
  token_id: 21,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

const RARE_FACILITY = {
  token_id: 22,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

const LEGENDARY_FACILITY = {
  token_id: 23,
  resource_ev: 1,
  resource_id: 24,
  resource_cap: 100,
  class: Distribution.Facility,
};

const SOLAR_GRID2 = {
  token_id: 52,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
const GEOWELL2 = {
  token_id: 53,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
const TIDAL_TURBINES2 = {
  token_id: 54,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
const RADIANT_GRADENS2 = {
  token_id: 55,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
const FUSION_CORE2 = {
  token_id: 56,
  resource_ev: 2,
  resource_id: AssetIds.Energy,
  resource_cap: 200,
  class: Distribution.Facility,
};
const SOLAR_GRID3 = {
  token_id: 57,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
const GEOWELL3 = {
  token_id: 58,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
const TIDAL_TURBINES3 = {
  token_id: 59,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
const RADIANT_GRADENS3 = {
  token_id: 60,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
const FUSION_CORE3 = {
  token_id: 61,
  resource_ev: 4,
  resource_id: AssetIds.Energy,
  resource_cap: 400,
  class: Distribution.Facility,
};
const SOLAR_GRID4 = {
  token_id: 62,
  resource_ev: 8,
  resource_id: AssetIds.Energy,
  resource_cap: 800,
  class: Distribution.Facility,
};
const GEOWELL4 = {
  token_id: 63,
  resource_ev: 8,
  resource_id: AssetIds.Energy,
  resource_cap: 800,
  class: Distribution.Facility,
};
const TIDAL_TURBINES4 = {
  token_id: 64,
  resource_ev: 8,
  resource_id: AssetIds.Energy,
  resource_cap: 800,
  class: Distribution.Facility,
};
const RADIANT_GRADENS4 = {
  token_id: 65,
  resource_ev: 8,
  resource_id: AssetIds.Energy,
  resource_cap: 800,
  class: Distribution.Facility,
};
const FUSION_CORE4 = {
  token_id: 66,
  resource_ev: 8,
  resource_id: AssetIds.Energy,
  resource_cap: 800,
  class: Distribution.Facility,
};
const SOLAR_GRID5 = {
  token_id: 67,
  resource_ev: 16,
  resource_id: AssetIds.Energy,
  resource_cap: 1600,
  class: Distribution.Facility,
};
const GEOWELL5 = {
  token_id: 68,
  resource_ev: 16,
  resource_id: AssetIds.Energy,
  resource_cap: 1600,
  class: Distribution.Facility,
};
const TIDAL_TURBINES5 = {
  token_id: 69,
  resource_ev: 16,
  resource_id: AssetIds.Energy,
  resource_cap: 1600,
  class: Distribution.Facility,
};
const RADIANT_GRADENS5 = {
  token_id: 70,
  resource_ev: 16,
  resource_id: AssetIds.Energy,
  resource_cap: 1600,
  class: Distribution.Facility,
};
const FUSION_CORE5 = {
  token_id: 71,
  resource_ev: 16,
  resource_id: AssetIds.Energy,
  resource_cap: 1600,
  class: Distribution.Facility,
};

const apollo_tiles = [
  { x: -1, y: 0, z: 1 },
  { x: -1, y: 1, z: 0 },
  { x: 0, y: -1, z: 1 },
  { x: 0, y: 1, z: -1 },
  { x: 1, y: -1, z: 0 },
  { x: 1, y: 0, z: -1 },
];
const artemis_tiles = [
  { x: 0, y: -2, z: 2 },
  { x: 1, y: -2, z: 1 },
  { x: 2, y: -2, z: 0 },
  { x: 2, y: -1, z: -1 },
  { x: 2, y: 0, z: -2 },
  { x: 1, y: 1, z: -2 },
  { x: 0, y: 2, z: -2 },
  { x: -1, y: 2, z: -1 },
  { x: -2, y: 2, z: 0 },
  { x: -2, y: 1, z: 1 },
  { x: -2, y: 0, z: 2 },
  { x: -1, y: -1, z: 2 },
];
const chronos_tiles = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];
const helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];

async function setup_new_facilities(ADAPTER) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(ADAPTER);

  const tx1 = await contract.setFacilityStakeRequirements(52, 4, true);
  await displaySpinner(tx1.wait(), `Setting up tokenid 52: ${tx1.hash}`);

  const tx2 = await contract.setFacilityStakeRequirements(53, 3, true);
  await displaySpinner(tx2.wait(), `Setting up tokenid 53: ${tx2.hash}`);

  const tx3 = await contract.setFacilityStakeRequirements(54, 2, true);
  await displaySpinner(tx3.wait(), `Setting up tokenid 54: ${tx3.hash}`);

  const tx4 = await contract.setFacilityStakeRequirements(55, 1, true);
  await displaySpinner(tx4.wait(), `Setting up tokenid 55: ${tx4.hash}`);

  const tx5 = await contract.setFacilityStakeRequirements(56, 0, true);
  await displaySpinner(tx5.wait(), `Setting up tokenid 56: ${tx5.hash}`);

  const tx6 = await contract.setFacilityStakeRequirements(57, 4, true);
  await displaySpinner(tx6.wait(), `Setting up tokenid 57: ${tx6.hash}`);

  const tx7 = await contract.setFacilityStakeRequirements(58, 3, true);
  await displaySpinner(tx7.wait(), `Setting up tokenid 58: ${tx7.hash}`);

  const tx8 = await contract.setFacilityStakeRequirements(59, 2, true);
  await displaySpinner(tx8.wait(), `Setting up tokenid 59: ${tx8.hash}`);

  const tx9 = await contract.setFacilityStakeRequirements(60, 1, true);
  await displaySpinner(tx9.wait(), `Setting up tokenid 60: ${tx9.hash}`);

  const tx10 = await contract.setFacilityStakeRequirements(61, 0, true);
  await displaySpinner(tx10.wait(), `Setting up tokenid 61: ${tx10.hash}`);
}

async function setup_resource_params(ADAPTER) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(ADAPTER);

  const ringsTx = await contract.setRings([
    { x: apollo_tiles[0].x, y: apollo_tiles[0].y, ring: 0 },
    { x: apollo_tiles[1].x, y: apollo_tiles[1].y, ring: 0 },
    { x: apollo_tiles[2].x, y: apollo_tiles[2].y, ring: 0 },
    { x: apollo_tiles[3].x, y: apollo_tiles[3].y, ring: 0 },
    { x: apollo_tiles[4].x, y: apollo_tiles[4].y, ring: 0 },
    { x: apollo_tiles[5].x, y: apollo_tiles[5].y, ring: 0 },
    { x: artemis_tiles[0].x, y: artemis_tiles[0].y, ring: 1 },
    { x: artemis_tiles[1].x, y: artemis_tiles[1].y, ring: 1 },
    { x: artemis_tiles[2].x, y: artemis_tiles[2].y, ring: 1 },
    { x: artemis_tiles[3].x, y: artemis_tiles[3].y, ring: 1 },
    { x: artemis_tiles[4].x, y: artemis_tiles[4].y, ring: 1 },
    { x: artemis_tiles[5].x, y: artemis_tiles[5].y, ring: 1 },
    { x: artemis_tiles[6].x, y: artemis_tiles[6].y, ring: 1 },
    { x: artemis_tiles[7].x, y: artemis_tiles[7].y, ring: 1 },
    { x: artemis_tiles[8].x, y: artemis_tiles[8].y, ring: 1 },
    { x: artemis_tiles[9].x, y: artemis_tiles[9].y, ring: 1 },
    { x: artemis_tiles[10].x, y: artemis_tiles[10].y, ring: 1 },
    { x: artemis_tiles[11].x, y: artemis_tiles[11].y, ring: 1 },
    { x: chronos_tiles[0].x, y: chronos_tiles[0].y, ring: 2 },
    { x: chronos_tiles[1].x, y: chronos_tiles[1].y, ring: 2 },
    { x: chronos_tiles[2].x, y: chronos_tiles[2].y, ring: 2 },
    { x: chronos_tiles[3].x, y: chronos_tiles[3].y, ring: 2 },
    { x: chronos_tiles[4].x, y: chronos_tiles[4].y, ring: 2 },
    { x: chronos_tiles[5].x, y: chronos_tiles[5].y, ring: 2 },
    { x: chronos_tiles[6].x, y: chronos_tiles[6].y, ring: 2 },
    { x: chronos_tiles[7].x, y: chronos_tiles[7].y, ring: 2 },
    { x: chronos_tiles[8].x, y: chronos_tiles[8].y, ring: 2 },
    { x: chronos_tiles[9].x, y: chronos_tiles[9].y, ring: 2 },
    { x: chronos_tiles[10].x, y: chronos_tiles[10].y, ring: 2 },
    { x: chronos_tiles[11].x, y: chronos_tiles[11].y, ring: 2 },
    { x: helios_tiles[0].x, y: helios_tiles[0].y, ring: 3 },
    { x: helios_tiles[1].x, y: helios_tiles[1].y, ring: 3 },
    { x: helios_tiles[2].x, y: helios_tiles[2].y, ring: 3 },
    { x: helios_tiles[3].x, y: helios_tiles[3].y, ring: 3 },
    { x: helios_tiles[4].x, y: helios_tiles[4].y, ring: 3 },
    { x: helios_tiles[5].x, y: helios_tiles[5].y, ring: 3 },
  ]);

  await displaySpinner(ringsTx.wait(), `Setting up rings... ${ringsTx.hash}`);

  const apolloTx = await contract.setTokenInfoBulk([
    { tokenId: SOLAR_GRID2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID2.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID2.resource_cap, ring: 0 },
    { tokenId: GEOWELL2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL2.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL2.resource_cap, ring: 0 },
    { tokenId: TIDAL_TURBINES2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES2.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES2.resource_cap, ring: 0 },
    { tokenId: RADIANT_GRADENS2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS2.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS2.resource_cap, ring: 0 },
    { tokenId: FUSION_CORE2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE2.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE2.resource_cap, ring: 0 },
    { tokenId: SOLAR_GRID3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID3.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID3.resource_cap, ring: 0 },
    { tokenId: GEOWELL3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL3.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL3.resource_cap, ring: 0 },
    { tokenId: TIDAL_TURBINES3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES3.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES3.resource_cap, ring: 0 },
    { tokenId: RADIANT_GRADENS3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS3.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS3.resource_cap, ring: 0 },
    { tokenId: FUSION_CORE3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE3.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE3.resource_cap, ring: 0 },
    { tokenId: OUTLIER_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: OUTLIER_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: OUTLIER_FACILITY.resource_cap, ring: 0 },
    { tokenId: COMMON_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: COMMON_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: COMMON_FACILITY.resource_cap, ring: 0 },
    { tokenId: UNCOMMON_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: UNCOMMON_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: UNCOMMON_FACILITY.resource_cap, ring: 0 },
    { tokenId: RARE_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RARE_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: RARE_FACILITY.resource_cap, ring: 0 },
    { tokenId: LEGENDARY_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: LEGENDARY_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: LEGENDARY_FACILITY.resource_cap, ring: 0 },
    { tokenId: PIERCER_DRONE_APOLLO.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: PIERCER_DRONE_APOLLO.resource_ev, resourceId: AssetIds.Waste, resourceCap: PIERCER_DRONE_APOLLO.resource_cap, ring: 0 },
    { tokenId: GENESIS_DRONE_APOLLO.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: GENESIS_DRONE_APOLLO.resource_ev, resourceId: AssetIds.Waste, resourceCap: GENESIS_DRONE_APOLLO.resource_cap, ring: 0 },
    { tokenId: NIGHT_DRONE_APOLLO.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: NIGHT_DRONE_APOLLO.resource_ev, resourceId: AssetIds.Waste, resourceCap: NIGHT_DRONE_APOLLO.resource_cap, ring: 0 },
  ]);

  await displaySpinner(apolloTx.wait(), `Setting up Apollo Resources... ${apolloTx.hash}`);

  const artemisTx = await contract.setTokenInfoBulk([
    { tokenId: SOLAR_GRID2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID2.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID2.resource_cap, ring: 1 },
    { tokenId: GEOWELL2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL2.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL2.resource_cap, ring: 1 },
    { tokenId: TIDAL_TURBINES2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES2.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES2.resource_cap, ring: 1 },
    { tokenId: RADIANT_GRADENS2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS2.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS2.resource_cap, ring: 1 },
    { tokenId: FUSION_CORE2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE2.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE2.resource_cap, ring: 1 },
    { tokenId: SOLAR_GRID3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID3.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID3.resource_cap, ring: 1 },
    { tokenId: GEOWELL3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL3.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL3.resource_cap, ring: 1 },
    { tokenId: TIDAL_TURBINES3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES3.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES3.resource_cap, ring: 1 },
    { tokenId: RADIANT_GRADENS3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS3.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS3.resource_cap, ring: 1 },
    { tokenId: FUSION_CORE3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE3.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE3.resource_cap, ring: 1 },
    { tokenId: OUTLIER_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: OUTLIER_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: OUTLIER_FACILITY.resource_cap, ring: 1 },
    { tokenId: COMMON_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: COMMON_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: COMMON_FACILITY.resource_cap, ring: 1 },
    { tokenId: UNCOMMON_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: UNCOMMON_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: UNCOMMON_FACILITY.resource_cap, ring: 1 },
    { tokenId: RARE_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RARE_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: RARE_FACILITY.resource_cap, ring: 1 },
    { tokenId: LEGENDARY_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: LEGENDARY_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: LEGENDARY_FACILITY.resource_cap, ring: 1 },
    { tokenId: PIERCER_DRONE_ARTEMIS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: PIERCER_DRONE_ARTEMIS.resource_ev, resourceId: AssetIds.Waste, resourceCap: PIERCER_DRONE_ARTEMIS.resource_cap, ring: 1 },
    { tokenId: GENESIS_DRONE_ARTEMIS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: GENESIS_DRONE_ARTEMIS.resource_ev, resourceId: AssetIds.Waste, resourceCap: GENESIS_DRONE_ARTEMIS.resource_cap, ring: 1 },
    { tokenId: NIGHT_DRONE_ARTEMIS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: NIGHT_DRONE_ARTEMIS.resource_ev, resourceId: AssetIds.Waste, resourceCap: NIGHT_DRONE_ARTEMIS.resource_cap, ring: 1 },
  ]);
  await displaySpinner(artemisTx.wait(), `Setting up Artemis Resources... ${artemisTx.hash}`);

  const chronosTx = await contract.setTokenInfoBulk([
    { tokenId: SOLAR_GRID2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID2.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID2.resource_cap, ring: 2 },
    { tokenId: GEOWELL2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL2.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL2.resource_cap, ring: 2 },
    { tokenId: TIDAL_TURBINES2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES2.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES2.resource_cap, ring: 2 },
    { tokenId: RADIANT_GRADENS2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS2.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS2.resource_cap, ring: 2 },
    { tokenId: FUSION_CORE2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE2.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE2.resource_cap, ring: 2 },
    { tokenId: SOLAR_GRID3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID3.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID3.resource_cap, ring: 2 },
    { tokenId: GEOWELL3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL3.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL3.resource_cap, ring: 2 },
    { tokenId: TIDAL_TURBINES3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES3.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES3.resource_cap, ring: 2 },
    { tokenId: RADIANT_GRADENS3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS3.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS3.resource_cap, ring: 2 },
    { tokenId: FUSION_CORE3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE3.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE3.resource_cap, ring: 2 },
    { tokenId: OUTLIER_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: OUTLIER_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: OUTLIER_FACILITY.resource_cap, ring: 2 },
    { tokenId: COMMON_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: COMMON_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: COMMON_FACILITY.resource_cap, ring: 2 },
    { tokenId: UNCOMMON_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: UNCOMMON_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: UNCOMMON_FACILITY.resource_cap, ring: 2 },
    { tokenId: RARE_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RARE_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: RARE_FACILITY.resource_cap, ring: 2 },
    { tokenId: LEGENDARY_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: LEGENDARY_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: LEGENDARY_FACILITY.resource_cap, ring: 2 },
    { tokenId: PIERCER_DRONE_CHRONOS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: PIERCER_DRONE_CHRONOS.resource_ev, resourceId: AssetIds.Waste, resourceCap: PIERCER_DRONE_CHRONOS.resource_cap, ring: 2 },
    { tokenId: GENESIS_DRONE_CHRONOS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: GENESIS_DRONE_CHRONOS.resource_ev, resourceId: AssetIds.Waste, resourceCap: GENESIS_DRONE_CHRONOS.resource_cap, ring: 2 },
    { tokenId: NIGHT_DRONE_CHRONOS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: NIGHT_DRONE_CHRONOS.resource_ev, resourceId: AssetIds.Waste, resourceCap: NIGHT_DRONE_CHRONOS.resource_cap, ring: 2 },
  ]);
  await displaySpinner(chronosTx.wait(), `Setting up Chronos Resources... ${chronosTx.hash}`);

  const heliosTx = await contract.setTokenInfoBulk([
    { tokenId: SOLAR_GRID2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID2.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID2.resource_cap, ring: 3 },
    { tokenId: GEOWELL2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL2.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL2.resource_cap, ring: 3 },
    { tokenId: TIDAL_TURBINES2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES2.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES2.resource_cap, ring: 3 },
    { tokenId: RADIANT_GRADENS2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS2.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS2.resource_cap, ring: 3 },
    { tokenId: FUSION_CORE2.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE2.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE2.resource_cap, ring: 3 },
    { tokenId: SOLAR_GRID3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID3.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID3.resource_cap, ring: 3 },
    { tokenId: GEOWELL3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL3.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL3.resource_cap, ring: 3 },
    { tokenId: TIDAL_TURBINES3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES3.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES3.resource_cap, ring: 3 },
    { tokenId: RADIANT_GRADENS3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS3.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS3.resource_cap, ring: 3 },
    { tokenId: FUSION_CORE3.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE3.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE3.resource_cap, ring: 3 },
    { tokenId: OUTLIER_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: OUTLIER_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: OUTLIER_FACILITY.resource_cap, ring: 3 },
    { tokenId: COMMON_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: COMMON_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: COMMON_FACILITY.resource_cap, ring: 3 },
    { tokenId: UNCOMMON_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: UNCOMMON_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: UNCOMMON_FACILITY.resource_cap, ring: 3 },
    { tokenId: RARE_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RARE_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: RARE_FACILITY.resource_cap, ring: 3 },
    { tokenId: LEGENDARY_FACILITY.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: LEGENDARY_FACILITY.resource_ev, resourceId: AssetIds.Energy, resourceCap: LEGENDARY_FACILITY.resource_cap, ring: 3 },
    { tokenId: PIERCER_DRONE_HELIOS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: PIERCER_DRONE_HELIOS.resource_ev, resourceId: AssetIds.Waste, resourceCap: PIERCER_DRONE_HELIOS.resource_cap, ring: 3 },
    { tokenId: GENESIS_DRONE_HELIOS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: GENESIS_DRONE_HELIOS.resource_ev, resourceId: AssetIds.Waste, resourceCap: GENESIS_DRONE_HELIOS.resource_cap, ring: 3 },
    { tokenId: NIGHT_DRONE_HELIOS.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.LinearDeterministic, resourceEv: NIGHT_DRONE_HELIOS.resource_ev, resourceId: AssetIds.Waste, resourceCap: NIGHT_DRONE_HELIOS.resource_cap, ring: 3 },
  ]);
  await displaySpinner(heliosTx.wait(), `Setting up Helios Resources... ${heliosTx.hash}`);

  console.log('Pix Asset resource params set!');
}

async function setupFacilityLeadTimesBonuses(ADAPTER) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(ADAPTER);

  let tx = await contract.setLeadTimeBonuses([0, 1, 2, 3], [10000, 9000, 9000, 8000]);
  await displaySpinner(tx.wait(), `Setting up Lead Time Bonuses... ${tx.hash}`);
}

async function setup_facilites_level4_level5(ADAPTER) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(ADAPTER);

  const tx1 = await contract.setFacilityStakeRequirements(62, 4, true);
  await displaySpinner(tx1.wait(), `Setting up tokenid 62: ${tx1.hash}`);

  const tx2 = await contract.setFacilityStakeRequirements(63, 3, true);
  await displaySpinner(tx2.wait(), `Setting up tokenid 63: ${tx2.hash}`);

  const tx3 = await contract.setFacilityStakeRequirements(64, 2, true);
  await displaySpinner(tx3.wait(), `Setting up tokenid 64: ${tx3.hash}`);

  const tx4 = await contract.setFacilityStakeRequirements(65, 1, true);
  await displaySpinner(tx4.wait(), `Setting up tokenid 65: ${tx4.hash}`);

  const tx5 = await contract.setFacilityStakeRequirements(66, 0, true);
  await displaySpinner(tx5.wait(), `Setting up tokenid 66: ${tx5.hash}`);

  const tx6 = await contract.setFacilityStakeRequirements(67, 4, true);
  await displaySpinner(tx6.wait(), `Setting up tokenid 67: ${tx6.hash}`);

  const tx7 = await contract.setFacilityStakeRequirements(68, 3, true);
  await displaySpinner(tx7.wait(), `Setting up tokenid 68: ${tx7.hash}`);

  const tx8 = await contract.setFacilityStakeRequirements(69, 2, true);
  await displaySpinner(tx8.wait(), `Setting up tokenid 69: ${tx8.hash}`);

  const tx9 = await contract.setFacilityStakeRequirements(70, 1, true);
  await displaySpinner(tx9.wait(), `Setting up tokenid 70: ${tx9.hash}`);

  const tx10 = await contract.setFacilityStakeRequirements(71, 0, true);
  await displaySpinner(tx10.wait(), `Setting up tokenid 71: ${tx10.hash}`);
}

async function setup_resource_params_level4_level5(ADAPTER) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await factory.attach(ADAPTER);

  const apolloTx = await contract.setTokenInfoBulk([
    { tokenId: SOLAR_GRID4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID4.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID4.resource_cap, ring: 0 },
    { tokenId: GEOWELL4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL4.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL4.resource_cap, ring: 0 },
    { tokenId: TIDAL_TURBINES4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES4.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES4.resource_cap, ring: 0 },
    { tokenId: RADIANT_GRADENS4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS4.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS4.resource_cap, ring: 0 },
    { tokenId: FUSION_CORE4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE4.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE4.resource_cap, ring: 0 },
    { tokenId: SOLAR_GRID5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID5.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID5.resource_cap, ring: 0 },
    { tokenId: GEOWELL5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL5.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL5.resource_cap, ring: 0 },
    { tokenId: TIDAL_TURBINES5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES5.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES5.resource_cap, ring: 0 },
    { tokenId: RADIANT_GRADENS5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS5.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS5.resource_cap, ring: 0 },
    { tokenId: FUSION_CORE5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE5.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE5.resource_cap, ring: 0 },
  ]);

  await displaySpinner(apolloTx.wait(), `Setting up Apollo Resources... ${apolloTx.hash}`);

  const artemisTx = await contract.setTokenInfoBulk([
    { tokenId: SOLAR_GRID4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID4.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID4.resource_cap, ring: 1 },
    { tokenId: GEOWELL4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL4.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL4.resource_cap, ring: 1 },
    { tokenId: TIDAL_TURBINES4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES4.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES4.resource_cap, ring: 1 },
    { tokenId: RADIANT_GRADENS4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS4.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS4.resource_cap, ring: 1 },
    { tokenId: FUSION_CORE4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE4.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE4.resource_cap, ring: 1 },
    { tokenId: SOLAR_GRID5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID5.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID5.resource_cap, ring: 1 },
    { tokenId: GEOWELL5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL5.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL5.resource_cap, ring: 1 },
    { tokenId: TIDAL_TURBINES5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES5.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES5.resource_cap, ring: 1 },
    { tokenId: RADIANT_GRADENS5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS5.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS5.resource_cap, ring: 1 },
    { tokenId: FUSION_CORE5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE5.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE5.resource_cap, ring: 1 },
  ]);
  await displaySpinner(artemisTx.wait(), `Setting up Artemis Resources... ${artemisTx.hash}`);

  const chronosTx = await contract.setTokenInfoBulk([
    { tokenId: SOLAR_GRID4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID4.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID4.resource_cap, ring: 4 },
    { tokenId: GEOWELL4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL4.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL4.resource_cap, ring: 4 },
    { tokenId: TIDAL_TURBINES4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES4.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES4.resource_cap, ring: 4 },
    { tokenId: RADIANT_GRADENS4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS4.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS4.resource_cap, ring: 4 },
    { tokenId: FUSION_CORE4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE4.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE4.resource_cap, ring: 4 },
    { tokenId: SOLAR_GRID5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID5.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID5.resource_cap, ring: 4 },
    { tokenId: GEOWELL5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL5.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL5.resource_cap, ring: 4 },
    { tokenId: TIDAL_TURBINES5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES5.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES5.resource_cap, ring: 4 },
    { tokenId: RADIANT_GRADENS5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS5.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS5.resource_cap, ring: 4 },
    { tokenId: FUSION_CORE5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE5.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE5.resource_cap, ring: 4 },
  ]);
  await displaySpinner(chronosTx.wait(), `Setting up Chronos Resources... ${chronosTx.hash}`);

  const heliosTx = await contract.setTokenInfoBulk([
    { tokenId: SOLAR_GRID4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID4.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID4.resource_cap, ring: 5 },
    { tokenId: GEOWELL4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL4.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL4.resource_cap, ring: 5 },
    { tokenId: TIDAL_TURBINES4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES4.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES4.resource_cap, ring: 5 },
    { tokenId: RADIANT_GRADENS4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS4.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS4.resource_cap, ring: 5 },
    { tokenId: FUSION_CORE4.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE4.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE4.resource_cap, ring: 5 },
    { tokenId: SOLAR_GRID5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: SOLAR_GRID5.resource_ev, resourceId: AssetIds.Energy, resourceCap: SOLAR_GRID5.resource_cap, ring: 5 },
    { tokenId: GEOWELL5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: GEOWELL5.resource_ev, resourceId: AssetIds.Energy, resourceCap: GEOWELL5.resource_cap, ring: 5 },
    { tokenId: TIDAL_TURBINES5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: TIDAL_TURBINES5.resource_ev, resourceId: AssetIds.Energy, resourceCap: TIDAL_TURBINES5.resource_cap, ring: 5 },
    { tokenId: RADIANT_GRADENS5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: RADIANT_GRADENS5.resource_ev, resourceId: AssetIds.Energy, resourceCap: RADIANT_GRADENS5.resource_cap, ring: 5 },
    { tokenId: FUSION_CORE5.token_id, isStakeable: true, isBase: false, isRaidable: true, class: Distribution.Facility, resourceEv: FUSION_CORE5.resource_ev, resourceId: AssetIds.Energy, resourceCap: FUSION_CORE5.resource_cap, ring: 5 },
  ]);
  await displaySpinner(heliosTx.wait(), `Setting up Helios Resources... ${heliosTx.hash}`);

  console.log('Pix Asset resource params set!');
}

async function main() {
  // await setup_resource_params(PIX_ASSET_ADAPTER);
  // await setup_new_facilities(PIX_ASSET_ADAPTER);
  // await setupFacilityLeadTimesBonuses(PIX_ASSET_ADAPTER);
  await setup_facilites_level4_level5(PIX_ASSET_ADAPTER);
  await setup_resource_params_level4_level5(PIX_ASSET_ADAPTER);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
