const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
  const subscriptionId = ethers.BigNumber.from("54216145499356993403168751869922995069159432937999774566231287190531013725864");

  const keyHash = '0x192234a5cda4cc07c0b66dfbcfbb785341cc790edc50032e842667dbb506cada'
  const callbackGasLimit = 2500000;
  const requestConfirmations = 3;
  const catRaff = '0x63679581F4c20531A0BA987f8dD843d0f67Adde8';
  const vrfManagerAddress ='0x3440Fb7224dD82496c8d8e407577b8DEC9379fB6';

  const VRFManagerV2 = await ethers.getContractFactory('VRFManagerV2');
  const vrfManager = await VRFManagerV2.attach(vrfManagerAddress);

  console.log('Upgrading  crystal');
  const CrystalPIXCatRaffFactory = await ethers.getContractFactory('CrystalPIXCatRaff');
  const crystalPIXCatRaff = await upgrades.upgradeProxy(
    catRaff,
    CrystalPIXCatRaffFactory,
  );
  console.log('crystal cat raff  upgraded', crystalPIXCatRaff.address);

  crystalPIXCatRaff.setVRFManager(vrfManagerAddress);

  console.log('vrfManager set at', vrfManager.address);
  
  console.log('Setting Consumer');

  await vrfManager.setConsumer(catRaff, subscriptionId, keyHash, callbackGasLimit, requestConfirmations, 1);

  console.log('Set');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
