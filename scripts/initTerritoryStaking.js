const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


async function main() {
  const PIXTerritoryStaking = await hre.ethers.getContractFactory('PIXTerritoryStaking');
  const terr = await PIXTerritoryStaking.attach('0x5cb9FcD65cfe3EEFDE88a7084caF2625d516BDD0');
  const IXT = await hre.ethers.getContractFactory('PIXT');
  const ixt = await IXT.attach('0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE');



  let epochTotalReward1 = web3.utils.toWei('91755');
  await ixt.approve(terr.address, epochTotalReward1,{ gasPrice: 222000000000, gasLimit: 9000000 });

  console.log(epochTotalReward1)
  const response = await terr.addEpochRewards(
    epochTotalReward1,
    2592000,
    { gasPrice: 222000000000, gasLimit: 9000000 },
  );

    const receipt = await response.wait(1);
    console.log(receipt);

  
}

main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
