const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
    const orderAutomationAddress = '0x31B86eaec4fc806d8E1e323D79DdbcFA21D03F27';
    const subscriptionId = ethers.BigNumber.from("54216145499356993403168751869922995069159432937999774566231287190531013725864");
    const keyHash = '0x192234a5cda4cc07c0b66dfbcfbb785341cc790edc50032e842667dbb506cada'
    const callbackGasLimit = 2500000;
    const requestConfirmations = 3;
  
    const OrderAutomationFactory = await ethers.getContractFactory('OrderAutomation');
  
    const vrfManagerAddress ='0x3440Fb7224dD82496c8d8e407577b8DEC9379fB6';
  
    const VRFManagerV2 = await ethers.getContractFactory('VRFManagerV2');
    const vrfManager = await VRFManagerV2.attach(vrfManagerAddress);
  
    const orderAutomation = await upgrades.upgradeProxy(
        orderAutomationAddress,
        OrderAutomationFactory,
    );
    console.log('FacilityStore Upgraded!');
  
    console.log('Setting VRF Oracle');
  
    await orderAutomation.setOracleV2(vrfManagerAddress);
  
    console.log('Set');
  
    console.log('Setting Consumer');
  
    await vrfManager.setConsumer(orderAutomationAddress, subscriptionId, keyHash, callbackGasLimit, requestConfirmations, 1);
  
    console.log('Set');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
