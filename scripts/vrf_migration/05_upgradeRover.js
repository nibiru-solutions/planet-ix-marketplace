const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
  const roverAddress = '0x836545dFc47F71ff33C458B98b283732A8ceE4F6';

  const RoverFactory = await ethers.getContractFactory('Rover');
  rover = await upgrades.upgradeProxy(
    roverAddress,
    RoverFactory,
  );
  console.log('Rover Upgraded!');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
