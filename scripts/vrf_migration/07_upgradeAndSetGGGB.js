const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
  const gggbAddress = '0x3C3dF115FA9336814854DA92572d4F5BA9865D2C';
  const subscriptionId = ethers.BigNumber.from("54216145499356993403168751869922995069159432937999774566231287190531013725864");
  const keyHash = '0x192234a5cda4cc07c0b66dfbcfbb785341cc790edc50032e842667dbb506cada'
  const callbackGasLimit = 2500000;
  const requestConfirmations = 3;

  const GGGBFactory = await ethers.getContractFactory('GravityGradeGenericBurn');
  const gggb = await GGGBFactory.attach(gggbAddress);

  const vrfManagerAddress ='0x3440Fb7224dD82496c8d8e407577b8DEC9379fB6';

  const VRFManagerV2 = await ethers.getContractFactory('VRFManagerV2');
  const vrfManager = await VRFManagerV2.attach(vrfManagerAddress);

  /*let gggb = await upgrades.upgradeProxy(
    gggbAddress,
    GGGBFactory,
  );*/
  console.log('GravityGradeGenericBurn Upgraded!');

  console.log('Setting VRF Oracle');

  //await gggb.setVRFOracle('0x3440Fb7224dD82496c8d8e407577b8DEC9379fB6');

  console.log('Set');

  console.log('Setting Consumer');

  await vrfManager.setConsumer(gggbAddress, subscriptionId, keyHash, callbackGasLimit, requestConfirmations, 1);

  console.log('Set');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
