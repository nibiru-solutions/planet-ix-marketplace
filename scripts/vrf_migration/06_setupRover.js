const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
  const subscriptionId = ethers.BigNumber.from("54216145499356993403168751869922995069159432937999774566231287190531013725864");

  const keyHash = '0x192234a5cda4cc07c0b66dfbcfbb785341cc790edc50032e842667dbb506cada'
  const callbackGasLimit = 2500000;
  const requestConfirmations = 3;
  const consumer = '0x836545dFc47F71ff33C458B98b283732A8ceE4F6';
  const vrfManagerAddress ='0x3440Fb7224dD82496c8d8e407577b8DEC9379fB6';

  const VRFManagerV2 = await ethers.getContractFactory('VRFManagerV2');
  const vrfManager = await VRFManagerV2.attach(vrfManagerAddress);

  console.log('vrfManager set at', vrfManager.address);
  
  console.log('Setting Consumer');

  await vrfManager.setConsumer(consumer, subscriptionId, keyHash, callbackGasLimit, requestConfirmations, 5);

  console.log('Set');


  const Rover = await ethers.getContractFactory('Rover');
  const rover = await Rover.attach(consumer);
  console.log('Setting Consumer');

  await rover.setVrfManager(vrfManager.address);

  console.log('Set');}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
