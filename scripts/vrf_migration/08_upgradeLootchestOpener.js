const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
  const lootChestAddress = '0x245755B57c16eC301e50D3B84fB12485f7E7860b';
  const subscriptionId = ethers.BigNumber.from("54216145499356993403168751869922995069159432937999774566231287190531013725864");
  const keyHash = '0x192234a5cda4cc07c0b66dfbcfbb785341cc790edc50032e842667dbb506cada'
  const callbackGasLimit = 2500000;
  const requestConfirmations = 3;

  const LootChestOpenerFactory = await ethers.getContractFactory('LootChestOpener');
  //const lootChest = await LootChestOpenerFactory.attach(lootChestAddress);

  const vrfManagerAddress ='0x3440Fb7224dD82496c8d8e407577b8DEC9379fB6';

  const VRFManagerV2 = await ethers.getContractFactory('VRFManagerV2');
  const vrfManager = await VRFManagerV2.attach(vrfManagerAddress);

  const lootChest = await upgrades.upgradeProxy(
    lootChestAddress,
    LootChestOpenerFactory,
  );
  console.log('LootChestOpener Upgraded!');

  console.log('Setting VRF Oracle');

  await lootChest.setVRF(vrfManagerAddress);

  console.log('Set');

  console.log('Setting Consumer');

  await vrfManager.setConsumer(lootChestAddress, subscriptionId, keyHash, callbackGasLimit, requestConfirmations, 1);

  console.log('Set');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
