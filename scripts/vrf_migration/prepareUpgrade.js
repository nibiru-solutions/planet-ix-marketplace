const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
  const lootChestAddress = '0x00899aA4335d8c0b3719Fc1f830729830776bCDa';
  const LootChestOpenerFactory = await ethers.getContractFactory('SpinnerInternalSwapTreasury');
  console.log("Preparing upgrade...");
  vnftxV4Address = await upgrades.forceImport(lootChestAddress, LootChestOpenerFactory);
  console.log('Set');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
