const { ethers, upgrades } = require('hardhat');
const { web3 } = require('hardhat');

async function main() {
  const catRaff = '0x38Ea41c0430f8E6547A7a28d419F64F6C3244730';

  const PIXCatRaffAutomationV2 = await ethers.getContractFactory('PIXCatRaffKeeperV2');
  const automation = await upgrades.deployProxy(PIXCatRaffAutomationV2, [catRaff]);

  console.log('PIXCatRaffKeeperV2 deployed at', automation.address);

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
