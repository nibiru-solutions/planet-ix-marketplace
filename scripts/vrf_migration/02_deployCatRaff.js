const { ethers, upgrades } = require('hardhat');
const { web3 } = require('hardhat');

async function main() {
  const pixt = '0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE';
  const vrfManager = '0x3440Fb7224dD82496c8d8e407577b8DEC9379fB6';
  const treasury = '0xe72638529aa2e3be133c266fb4628b7c4b289982';
  const ticketPrice = web3.utils.toWei("1", 'ether');
  const weeklyPrize = web3.utils.toWei("2500", 'ether');
  const PIXCatRaffV2 = await ethers.getContractFactory('PIXCatRaffV2');
  const pixCatRaffV2 = await upgrades.deployProxy(PIXCatRaffV2, [pixt, vrfManager, treasury, ticketPrice, weeklyPrize]);

  console.log('PIXCatRaffV2 deployed at', pixCatRaffV2.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
