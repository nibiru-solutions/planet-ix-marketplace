const { ethers, upgrades } = require('hardhat');
const { BigNumber } = require('ethers');

async function main() {
  const FacilityUpgradeAddress = '0xEAA88a8A4570D176D6e0E496fF0fcb3496Bf904B';
  const subscriptionId = ethers.BigNumber.from("54216145499356993403168751869922995069159432937999774566231287190531013725864");
  const keyHash = '0x192234a5cda4cc07c0b66dfbcfbb785341cc790edc50032e842667dbb506cada'
  const callbackGasLimit = 2500000;
  const requestConfirmations = 3;

  const FacilityUpgradeFactory = await ethers.getContractFactory('GenericBurnV2');
  const RoverMCAdapter = await FacilityUpgradeFactory.attach('0xEAA88a8A4570D176D6e0E496fF0fcb3496Bf904B');

  const vrfManagerAddress ='0x3440Fb7224dD82496c8d8e407577b8DEC9379fB6';

  const VRFManagerV2 = await ethers.getContractFactory('VRFManagerV2');
  const vrfManager = await VRFManagerV2.attach(vrfManagerAddress);

  /*const RoverMCAdapter = await upgrades.upgradeProxy(
    FacilityUpgradeAddress,
    FacilityUpgradeFactory,
  );*/
  console.log('FacilityUpgrade Upgraded!');

  console.log('Setting VRF Oracle');

  await RoverMCAdapter.setVRFOracleV2(vrfManagerAddress, { gasPrice: 300000000000, gasLimit: 15000000 });

  console.log('Set');

  console.log('Setting Consumer');

  await vrfManager.setConsumer(FacilityUpgradeAddress, subscriptionId, keyHash, callbackGasLimit, requestConfirmations, 1, { gasPrice: 300000000000, gasLimit: 15000000 });

  console.log('Set');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
