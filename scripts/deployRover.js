const { ethers } = require('hardhat');
const { waitSeconds } = require('../helper/utils');

// Init params - fill these in before running the script
// POLYGON MAINNET PARAMS
const uri = 'ipfs://QmTXCZAqyC3TAomvmdSFP8EC9Tn8GeNSnsB62YfXuFvPWf';
const missionControl = '0x0F4497805302E901fD9d4131eA70240474240a5d';
const oracle = '0x6aB819382A03D0DF86A2b95E2CD550Cd4148d34d';
const vrfCoordinator = '0x7a1BaC17Ccc5b313516C5E16fb24f7659aA5ebed';
const astroCredits = '0x53037f0b395d0518671Fe0E2a766F780Ed8e7cA6';
const astroCreditsId = 8;

const keyHash = '0x4b09e658ed251bcafeebbc69400383d49f344ace09b9576fe248bb02c003fe9f';
const subscriptionId = 2541;
const callbackGasLimit = 2500000;
const requestConfirmations = 3;

const ixtToken = '0x12168dD03349fa6B3dae6ac8c552b6C86810Bf55';
const usdt = '0x12168dD03349fa6B3dae6ac8c552b6C86810Bf55';

async function main() {
  const RoverFactory = await ethers.getContractFactory('Rover');
  const rover = await upgrades.deployProxy(RoverFactory, [
    uri,
    missionControl,
    oracle,
    vrfCoordinator,
    usdt,
    ixtToken,
    astroCredits,
    astroCreditsId,
  ]);
  await rover.deployed();
  await rover.setKeyHash(keyHash);
  await rover.setCallbackGasLimit(callbackGasLimit);
  await rover.setRequestConfirmations(requestConfirmations);
  await rover.setSubscriptionId(subscriptionId);
  console.log(`rover deployed to ${rover.address}`);

  console.log('adding rover as consumer');
  const vrfCoordinatorContract = await ethers.getContractAt(
    ['function addConsumer(uint64 subId, address consumer) external'],
    vrfCoordinator,
  );
  await vrfCoordinatorContract.addConsumer(subscriptionId, rover.address);

  console.log('verifying rover');
  await waitSeconds(20);
  await hre.run('verify:verify', {
    address: rover.address,
    constructorArguments: [],
  });
  console.log('rover verified');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
