const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


async function main() {
  const PIXEnergyStaking = await hre.ethers.getContractFactory('EnergyIXTStaking');
  const energy = await PIXEnergyStaking.attach('0x4f0eE43495f36e7665716B4d8d8701bC232E462b');
  const IXT = await hre.ethers.getContractFactory('PIXT');
  const ixt = await IXT.attach('0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE');



  let epochTotalReward1 = web3.utils.toWei('91755');
  await ixt.approve(energy.address, epochTotalReward1, { gasPrice: 222000000000, gasLimit: 9000000 });

  console.log(epochTotalReward1)
  const response = await energy.addCurrentEpochRewards(
    epochTotalReward1,
    2592000,
    { gasPrice: 222000000000, gasLimit: 9000000 },
  );

  const receipt = await response.wait(1);
  console.log(receipt);


}

main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
