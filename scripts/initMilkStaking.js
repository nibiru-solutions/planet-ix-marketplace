const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');
const delay = ms => new Promise(res => setTimeout(res, ms));

async function main() {

  const TokenStaking = await hre.ethers.getContractFactory('TokenStaking');
  const contractMonth1 = await TokenStaking.attach('0x0A5935af2f40C713eFfcC74236Ff7a154A5C062b');
  const contractMonth6 = await TokenStaking.attach('0x40dcef5b355877A98f9Be18f53565cc1B6C4fBE4');

  let month1 = web3.utils.toWei("25000");
  let month6 = web3.utils.toWei("75000");


  console.log("Staking..")
  await contractMonth1.notifyRewardAmount(month1);
  await contractMonth6.notifyRewardAmount(month6);
  console.log("Staked..");

}


main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
