const { ethers, upgrades } = require('hardhat');
const hre = require('hardhat');
const run = require('hardhat');

const NFTProxyAddress = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const pixAssetsAddress = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const vrfManagerAddress = '';
const SUBSCRIPTION_ID = '';
const KEYHASH = '';
const CALLBACK_GAS_LIMIT = 2_500_000;
const REQUEST_CONFIRMATIONS = 3;
const NUM_WORDS = 1;

let genericBurn;
let vrfManager;

async function deployGenericBurn() {
  const genericBurnFactory = await ethers.getContractFactory('PIXAssetsGenericBurn');
  genericBurn = await upgrades.deployProxy(genericBurnFactory, []);

  await genericBurn.deployed();

  await genericBurn.setPixAssets(pixAssetsAddress);
  await genericBurn.setVRFOracle('0x34F9a1963029EDB930d6841c236989Ce61002b39');

  console.log(`generic burn deployed to: ${genericBurn.address}`);
  return genericBurn.address;
}

async function setupVRFManager(genericBurnAddress) {
  const vrfFactory = await ethers.getContractFactory('VRFManager');
  vrfManager = await vrfFactory.attach('0x34F9a1963029EDB930d6841c236989Ce61002b39');
  await vrfManager.deployed();

  await vrfManager.setConsumer(
    genericBurn.address,
    358,
    '0xcc294a196eeeb44da2888d17c0625cc88d70d9760a69d58d853ba6581a9ab0cd',
    2500000,
    3,
    1,
  );

  console.log(`vrf manager deployed to: ${vrfManager.address}`);
}

async function main() {
  let genericBurnAddress = await deployGenericBurn();
  await setupVRFManager(genericBurnAddress);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
