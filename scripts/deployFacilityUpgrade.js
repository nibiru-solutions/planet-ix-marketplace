const { ethers } = require('hardhat');
const { constants } = require('ethers');
const { waitSeconds } = require('../helper/utils');

// testnet addresses
let assetManagerAddress = "0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc"; //should be 0xba6666B118f8303F990f3519DF07e160227cCE87 for mainnet
let baseLevelAddress = "0x1eB919aC2e0015a5E232A4CD0138931c67C562ec"; // should be 0x781a3ad5b3aD1Cb919468a4C1faA02320A2d9d3d for mainnet
let vrfManagerAddress = "0xBE7f0e2D5813ee339B5686B7BF95D4401bD3D325"; // should be 0x34F9a1963029EDB930d6841c236989Ce61002b39 for mainnet
let IXT = "0x12168dD03349fa6B3dae6ac8c552b6C86810Bf55"; //should be 0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE for mainnet

let facilityIds    = [19,20,21,22,23, 52,53,54,55,56, 57,58,59,60,61];
let facilityLevels = [1, 1, 1, 1, 1,  2, 2, 2, 2, 2,  3, 3, 3, 3, 3 ];

// lvl 3 is max currently
// should be updated for level 3 when we add more levels
let upgradeToIds   = [52,53,54,55,56, 57,58,59,60,61, 57,58,59,60,61]; 

const M3TAM0D = 0;
const ASTRO_CREDITS = 8;
const BLUEPRINT = 9;
const ENERGY = 24;

let upgradeCostLeve1 = {
  tokenIds: [ASTRO_CREDITS, ENERGY, M3TAM0D, BLUEPRINT],
  amounts: [1000, 100, 2, 2],
}

async function main() {
  const FacilityUpgradeFactory = await ethers.getContractFactory('FacilityUpgrade');
  const facilityUpgrade = await upgrades.deployProxy(
    FacilityUpgradeFactory, 
    [assetManagerAddress, baseLevelAddress, vrfManagerAddress, IXT]
  );
  await facilityUpgrade.deployed();
  console.log(`FacilityUpgrade deployed to ${facilityUpgrade.address}`);

  //todo: add facilityUpgrade as consumer in VRFManager

  let tx = await facilityUpgrade.setFacilityParams(facilityIds, facilityLevels, upgradeToIds);
  await tx.wait();
  console.log(`Facility params set: ${tx.hash}`);

  tx = await facilityUpgrade.setLevelUpgradeCost(1, upgradeCostLeve1);
  await tx.wait();
  console.log(`level 1 upgrade cost set: ${tx.hash}`);

  console.log("start verification")
  await waitSeconds(10);
  await hre.run("verify:verify", {
      address: facilityUpgrade.address
  });
  console.log("Done")
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
