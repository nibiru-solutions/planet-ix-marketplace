const { ethers, upgrades} = require('hardhat');
const { constants } = require('ethers');
const hre = require("hardhat");


const superAppAddress = "0xb9D70840CcA6e6F71d3C884060EE123e13b4C27D"
const superTokenAddress = "0xFAc83774854237b6E31c4B051b91015e403956d3"
const superTokenLiteAddress = "0x9308A7116106269eB11834dF494eFd00d244cF8e"
const flowRate = "385802469135"
const prospectingAddress = "0xa47c861DE3272690a51F57e0E43551e55F6C790d"
const wasteTraderAddress = "0xbb5f9ADe0023aE87BDCD2268bDD74a2711654BDB"
const facilityStoreAddress = "0x9C31aDa541dfa3E867460Be0572C1a57311Df6FF"



async function main() {
    let baseLevelFactory = await hre.ethers.getContractFactory('BaseLevel');
    let baseLevel = await hre.upgrades.deployProxy(baseLevelFactory);
    await baseLevel.deployed();
    console.log("contract BaseLevel deployed", baseLevel.address);

    await baseLevel.setSuperAppAddress(superAppAddress);
    await baseLevel.setSuperTokens(superTokenAddress, superTokenLiteAddress);
    await baseLevel.setSuperFluidPerLevel(hre.ethers.BigNumber.from(flowRate))

    await baseLevel.setOrderCapacity(prospectingAddress, 0, 11, 2);
    await baseLevel.setOrderCapacity(prospectingAddress, 12, 17, 4);
    await baseLevel.setOrderCapacity(prospectingAddress, 18, 23, 8);
    await baseLevel.setOrderCapacity(prospectingAddress, 24, 29, 12);

    await baseLevel.setOrderCapacity(wasteTraderAddress, 0, 11, 2);
    await baseLevel.setOrderCapacity(wasteTraderAddress, 12, 17, 4);
    await baseLevel.setOrderCapacity(wasteTraderAddress, 18, 23, 8);
    await baseLevel.setOrderCapacity(wasteTraderAddress, 24, 29, 12);

    await baseLevel.setOrderCapacity(facilityStoreAddress, 0, 23, 1);
    await baseLevel.setOrderCapacity(facilityStoreAddress, 24, 35, 2);
    await baseLevel.setOrderCapacity(facilityStoreAddress, 36, 41, 3);
    console.log("Set up done")

    console.log("start verification")
    await hre.run("verify:verify", {
        address: baseLevel.address
    });
    console.log("Done")
}

async function upgradeTraders(){
    const baseLevelAddress = "0x781a3ad5b3aD1Cb919468a4C1faA02320A2d9d3d"

    const wasteTraderFactory = await ethers.getContractFactory('WasteTrader');
    const wasteTrader = await upgrades.upgradeProxy(
        '0xbb5f9ADe0023aE87BDCD2268bDD74a2711654BDB',
        wasteTraderFactory,
    );

    console.log(`wasteTrader upgraded: ${wasteTrader.address}`);
    await wasteTrader.setBaseLevelAddress(baseLevelAddress);
    console.log("wasteTrader Done");

    const prospectingFactory = await ethers.getContractFactory('Prospecting');
    const prospecting = await upgrades.upgradeProxy(
        '0x86214d04286C0d853670bEd2A6B1eD9bF14e39Bc',
        prospectingFactory,
    );

    console.log(`prospecting upgraded: ${prospecting.address}`);
    await prospecting.setBaseLevelAddress(baseLevelAddress);
    console.log("prospecting Done")

    const facilityStoreFactory = await ethers.getContractFactory('FacilityStore');
    const facilityStore = await upgrades.upgradeProxy(
        '0x9C31aDa541dfa3E867460Be0572C1a57311Df6FF',
        facilityStoreFactory,
    );

    console.log(`facilityStore upgraded: ${facilityStore.address}`);
    await facilityStore.setBaseLevelAddress(baseLevelAddress);
    console.log("facilityStore Done")

}

async function upgradeBaseLevel(){
    const baseLevelAddress = "0x781a3ad5b3aD1Cb919468a4C1faA02320A2d9d3d"

    const baseLevelFactory = await ethers.getContractFactory('BaseLevel');
    const baseLevel = await upgrades.upgradeProxy(
        baseLevelAddress,
        baseLevelFactory,
    );

    console.log(`baseLevel upgraded: ${baseLevel.address}`);
    await baseLevel.setGenesisLevel(18);
    console.log("baseLevel set up Done");
}

upgradeBaseLevel()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });