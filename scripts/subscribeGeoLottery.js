const { ethers } = require('hardhat');

const ISuperfluid = require('@superfluid-finance/ethereum-contracts/build/contracts/ISuperfluid');
const IConstantFlowAgreementV1 = require('@superfluid-finance/ethereum-contracts/build/contracts/IConstantFlowAgreementV1');
const ISuperToken = require('@superfluid-finance/ethereum-contracts/build/contracts/ISuperToken');

const hostAddress = '0xEB796bdb90fFA0f28255275e16936D25d3418603';
const cfaAddress = '0x49e565Ed1bdc17F3d220f72DF0857C26FA83F873';

const geoStreamer = '0x6e488C8A7F7afcF4FfDbAd628d4ef9b312Bb11E1';
const superTokenAddress = '0x3CAD7147c15C0864B8cF0EcCca43f98735e6e782';

const SECONDS_PER_MINUTE = 60;
const SECONDS_PER_HOUR = 60 * SECONDS_PER_MINUTE;
const SECONDS_PER_DAY = 24 * SECONDS_PER_HOUR;

let cfaV1, host, superApp, wallet, superToken;

function toWei(value, unit = 'ether') {
  return ethers.utils.parseUnits(value.toString(), unit);
}

function getFlowRatePerTimeUnit(amountToStream, timeUnitInSeconds) {
  const oneEtherInWei = toWei(amountToStream);
  return oneEtherInWei.div(timeUnitInSeconds);
}

async function INIT() {
  if (wallet === undefined) throw 'set global wallet first...';
  host = new ethers.Contract(hostAddress, ISuperfluid.abi, wallet);
  cfaV1 = new ethers.Contract(cfaAddress, IConstantFlowAgreementV1.abi, wallet);
  superToken = new ethers.Contract(superTokenAddress, ISuperToken.abi, wallet);

  const ContractFactory = await ethers.getContractFactory('GeoLotteryStream');
  superApp = ContractFactory.attach(geoStreamer);
}
async function main() {
  // Configurations
  const url = `https://polygon-mumbai.g.alchemy.com/v2/${process.env.ALCHEMY_KEY}`;
  const provider = new ethers.providers.JsonRpcProvider(url);
  const privateKey = process.env.PRIVATE_KEY_TESTNET;
  wallet = new ethers.Wallet(privateKey, provider);

  // instance contracts
  await INIT();

  let action = 'createFlow';
  // let action = 'updateFlow';

  let flowRate = getFlowRatePerTimeUnit(10, SECONDS_PER_MINUTE);

  // call superApp
  let userData = '0x';
  const callData = cfaV1.interface.encodeFunctionData(action, [superTokenAddress, geoStreamer, flowRate, '0x']);
  try {
    const tx = await host.connect(wallet).callAgreement(cfaAddress, callData, userData);
    console.log(tx.hash);
    await tx.wait();
  } catch (e) {
    console.log(e);
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
