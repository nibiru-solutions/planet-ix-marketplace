const { ethers, upgrades } = require('hardhat');

async function main() {
    let swap = "0x5DDBebEFf1f010B74bF727393489648beFa73AC4";
    let oracle = "0xea432D49bD20f3Cb6DEe00A98b93Cd0f2C1087Bb";
    let derc20 = "0xfe4F5145f6e09952a5ba9e956ED0C25e3Fa4c7F1";

    const ContractFactory = await ethers.getContractFactory('GravityGrade');
    const contract = await upgrades.deployProxy(ContractFactory,["Gravity Grade", "PIX-GG"]);
    await contract.deployed();

    await contract.setPaperCurrency(derc20);
    await contract.setSwapManager(swap);
    await contract.setOracleManager(oracle);

    console.log('Deployed at', contract.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
