const { ethers } = require('hardhat');
const { waitSeconds } = require('../helper/utils');
const { AssetIds } = require('../test/utils');

// Init params - fill these in before running the script
// POLYGON MAINNET PARAMS
const vrfCoordinator = '0xAE975071Be8F8eE67addBC1A82488F1C24858067';
const m3tam0d = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const m3tam0dTokenId = 0;

const keyHash = '0xd729dc84e21ae57ffb6be0053bf2b0668aa2aaf300a2a7b2ddf7dc0bb6e875a8';
const subscriptionId = 358;
const callbackGasLimit = 2000000;
const requestConfirmations = 3;

// mission control
const missionControl = '0x78f6ef4FA55e41b816cbc0f97a2D8743ba8795d9';

// rover
// Fill this in after deploying the rover
const rover = '';

// Resource params - update these before running the script
let ROVER_PIERCER_A = {
  resource_ev: 160,
  resource_id: AssetIds.Waste,
  resource_cap: 160,
  is_base: false,
  is_raidable: false,
};
let ROVER_PIERCER_B = {
  resource_ev: 192,
  resource_id: AssetIds.Waste,
  resource_cap: 192,
  is_base: false,
  is_raidable: false,
};

let ROVER_GENESIS_A = {
  resource_ev: 240,
  resource_id: AssetIds.Waste,
  resource_cap: 240,
  is_base: false,
  is_raidable: false,
};
let ROVER_GENESIS_B = {
  resource_ev: 288,
  resource_id: AssetIds.Waste,
  resource_cap: 288,
  is_base: false,
  is_raidable: false,
};

let ROVER_NIGHT_A = {
  resource_ev: 300,
  resource_id: AssetIds.Waste,
  resource_cap: 300,
  is_base: false,
  is_raidable: false,
};
let ROVER_NIGHT_B = {
  resource_ev: 360,
  resource_id: AssetIds.Waste,
  resource_cap: 360,
  is_base: false,
  is_raidable: false,
};

// tile coordinates
let helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];

let chronos_tile = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];

// TODO: use all rover objects above for setting the rates on different tiles
async function setResourceParameters(address) {
  const stakeable = await ethers.getContractAt('RoverMCStakeableAdapter', address);

  for (let i = 0; i < chronos_tile.length; i++) {
    console.log(`CHRONOS SETUP: Now setting up tile ${chronos_tile[i].x},${chronos_tile[i].y}`);
    await stakeable.setResourceParameters(
      ROVER.resource_ev,
      ROVER.resource_id,
      ROVER.resource_cap,
      ROVER.is_base,
      ROVER.is_raidable,
      chronos_tile[i].x,
      chronos_tile[i].y,
    );
  }

  for (let i = 0; i < helios_tiles.length; i++) {
    console.log(`HELIOS SETUP: Now setting up tile ${helios_tiles[i].x},${helios_tiles[i].y}`);
    await stakeable.setResourceParameters(
      ROVER.resource_ev,
      ROVER.resource_id,
      ROVER.resource_cap,
      ROVER.is_base,
      ROVER.is_raidable,
      helios_tiles[i].x,
      helios_tiles[i].y,
    );
  }
}

async function main() {
  const RoverStakingFactory = await ethers.getContractFactory('RoverMCStakeableAdapter');
  const roverStaking = await upgrades.deployProxy(RoverStakingFactory, [
    vrfCoordinator,
    m3tam0d,
    m3tam0dTokenId,
    keyHash,
    callbackGasLimit,
    requestConfirmations,
    subscriptionId,
  ]);
  await roverStaking.deployed();
  console.log(`rover mission control staking adapter deployed to ${roverStaking.address}`);
  await roverStaking.setMissionControl(missionControl);
  console.log('mission control set');
  await roverStaking.setERC721(rover);
  console.log('rover set');
  await setResourceParameters(roverStaking.address);
  console.log('resource parameters set');

  console.log('adding rover mission control staking adapter as consumer');
  const vrfCoordinatorContract = await ethers.getContractAt(
    ['function addConsumer(uint64 subId, address consumer) external'],
    vrfCoordinator,
  );
  await vrfCoordinatorContract.addConsumer(subscriptionId, roverStaking.address);

  // add staking adapter as trusted
  console.log('setting trusted minter on asset manager');
  const AssetFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
  const pixAssets = AssetFactory.attach(m3tam0d);
  await pixAssets.setTrusted(roverStaking.address, true);

  console.log('verifying rover staking adapter');
  await waitSeconds(60);
  await hre.run('verify:verify', {
    address: roverStaking.address,
    constructorArguments: [],
  });
  console.log('rover staking adapter verified');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
