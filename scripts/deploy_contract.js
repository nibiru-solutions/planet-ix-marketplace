const { ethers, upgrades } = require('hardhat');

async function main() {
  const ixtfactory = await ethers.getContractFactory('ERC20Mock');
  const ixt = await ixtfactory.deploy('xIXT', 'xIXT');

  console.log('Contract Deployed To', ixt.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
