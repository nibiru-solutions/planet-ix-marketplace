const { ethers, upgrades } = require('hardhat');

const PIX_ASSETS = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const GG = '0x5A330dE8322aA11CF34e6E042e8eB617006397cf';

const GGBURNV2 = '0xEAA88a8A4570D176D6e0E496fF0fcb3496Bf904B';
const ROVER = 'ROVER_MAINNET';

async function main() {
  const GenericBurnV2Factory = await ethers.getContractFactory('GenericBurnV2');
  const genericBurnV2 = GenericBurnV2Factory.attach(GGBURNV2);
/*
  await genericBurnV2.whitelistToken([GG], [38], [true],{ gasPrice: 240000000000, gasLimit: 1000000 });
  console.log('All tokens whiteslisted');

  await genericBurnV2.setTokenBurnLimit([GG], [38], [10],{ gasPrice: 240000000000, gasLimit: 1000000 });
  console.log('Burn limits set');

  await genericBurnV2.setGuaranteedRewards(GG, 38, [{ token: PIX_ASSETS, tokenId: 77, tokenAmount: 360 }],{ gasPrice: 240000000000, gasLimit: 1000000 });
  console.log('ROVER KEY 72 SET');
*/
  await genericBurnV2.setGuaranteedRewards(GG, 38, [{ token: PIX_ASSETS, tokenId: 18, tokenAmount: 3 }],{ gasPrice: 240000000000, gasLimit: 1000000 });
  console.log('ROVER KEY 72 SET');

  await genericBurnV2.setGuaranteedRewards(GG, 38, [{ token: PIX_ASSETS, tokenId: 79, tokenAmount: 1 }],{ gasPrice: 240000000000, gasLimit: 1000000 });
  console.log('ROVER KEY 72 SET');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
