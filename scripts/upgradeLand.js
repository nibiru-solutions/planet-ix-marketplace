const { ethers, upgrades } = require('hardhat');

async function main() {
  const PIXLanmarkFactory = await ethers.getContractFactory('PIX');

  //const tokensByRarity = [269,189,1,131,186,164];
  //const indices = [0,1,2,3,4,5];
  const contract = await upgrades.upgradeProxy('0xB2435253C71FcA27bE41206EB2793E44e1Df6b6D', PIXLanmarkFactory,{ gasPrice: 240000000000, gasLimit: 9000000 });
  await contract.deployed();

  console.log('Deployed at', contract.address);
/*
  console.log('Setting arrays');
  contract.setTokensByRarity(tokensByRarity);
  contract.setTokenRarityIndices(tokensByRarity, indices);
  console.log('Arrays set');
*/
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
