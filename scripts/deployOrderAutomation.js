const hre = require("hardhat");

let batchOrderInterval = 60;
let batchMaxSize = 20;
let prospectingTestNetAddress = "0xC0b7EbC988e683be95B2543c9Bf50FE0547AEe96"
let vrfManagerTestNetAddress = ""
let prospectingV2Address = "0xa47c861DE3272690a51F57e0E43551e55F6C790d"
let vrfManagerAddress = "0x34F9a1963029EDB930d6841c236989Ce61002b39"

async function main(){

    //upgrade prospecting
    const contract = await hre.ethers.getContractFactory('ProspectingV2');
    console.log('Upgrading contract: ',prospectingTestNetAddress);
    const prospecting = await hre.upgrades.upgradeProxy(prospectingV2Address, contract);
    console.log('Contract upgraded');

    // deploy orderAutomation
    let orderAutomationFactory = await hre.ethers.getContractFactory("OrderAutomation");
    let orderAutomation = await hre.upgrades.deployProxy(orderAutomationFactory, [batchOrderInterval, batchMaxSize, prospectingTestNetAddress]);
    await orderAutomation.deployed();
    console.log("OrderAutomation deployed: ", orderAutomation.address);

    // set up
    await orderAutomation.setOracle(vrfManagerAddress);
    await prospecting.setOrderAutomation(orderAutomation.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });