const { ethers } = require('hardhat');
const { constants } = require('ethers');

const landmark = '0x24CfF55d808fD10A1E730B86037760E57eCAF549';
const tickets = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const vrfCoordinatorV2 = '0xAE975071Be8F8eE67addBC1A82488F1C24858067';

const KEYHASH = '0xcc294a196eeeb44da2888d17c0625cc88d70d9760a69d58d853ba6581a9ab0cd';
const SUBSCRIPTION_ID = 358;

async function main() {
  const luckyCatRaffleFactory = await ethers.getContractFactory('LuckyCatRaffleV3');
  luckyCatRaffle = await upgrades.deployProxy(luckyCatRaffleFactory, [
    landmark,
    tickets,
    vrfCoordinatorV2,
    KEYHASH,
    SUBSCRIPTION_ID,
  ]);

  const erc721TreasuryFactory = await ethers.getContractFactory('LuckyCatERC721Treasury');
  erc721Treasury = await upgrades.deployProxy(erc721TreasuryFactory, []);

  const erc1155TreasuryFactory = await ethers.getContractFactory('LuckyCatERC1155Treasury');
  erc1155Treasury = await upgrades.deployProxy(erc1155TreasuryFactory, []);

  await luckyCatRaffle.setERC721Treasury(erc721Treasury.address);
  await luckyCatRaffle.setERC1155Treasury(erc1155Treasury.address);

  await erc1155Treasury.setLuckyCat(luckyCatRaffle.address);
  await erc721Treasury.setLuckyCat(luckyCatRaffle.address);

  console.log('Lucky cat deployed: ', luckyCatRaffle.address);
  console.log('1155 treasury: ', erc1155Treasury.address);
  console.log('721 treasury: ', erc721Treasury.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
