const { ethers, upgrades } = require('hardhat');

const { MerkleTree } = require('merkletreejs');
const keccak256 = require('keccak256');

const csv = require('csvtojson');


async function main() {

    for (let i = 207; i < 211; i++) {
        const csvFilePath = `scripts/out${i}.csv`;
        console.log(csvFilePath);
        const data = await csv().fromFile(csvFilePath);

        const leafNodes = data.map((pix) =>
            keccak256(
                ethers.utils.defaultAbiCoder.encode(
                    ['address', 'uint256', 'uint8', 'uint8'],
                    [pix.address, pix.pix_id, pix.category, pix.size],
                ),
            ),
        );

        const merkleTree = new MerkleTree(leafNodes, keccak256, { sortPairs: true });
        const newRoot = "0x" + merkleTree.getRoot().toString('hex');
        console.log(newRoot);
        console.log("New root: ", merkleTree.getRoot().toJSON());

        const ContractFactory = await ethers.getContractFactory('PIXMerkleMinter');
        const pixMM = await ContractFactory.attach('0xDcE37f71D193026a4E7b61fF14B34075858b6F84');
        await pixMM.setMerkleRoot(newRoot, true, { gasPrice: 150000000000, gasLimit: 9000000 });
        console.log("New root set.");
    }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });