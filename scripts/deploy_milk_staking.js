const { ethers, upgrades } = require('hardhat');

const distributor = '0xd94E880075AFa8395Fd61Bf47595c878A84e8aDd';
const xIXT = '0x8b04bf3358B88e3630aa64C1c76FF3B6C699C6a7';
const milk = '0xf538296e7dd856af7044deec949489e2f25705bc';
const rewardsDuration = '2592000';
const lockPeriod1 = '2592000';
const lockPeriod2 = '15552000';

async function main() {
  const MilkStakingFactory = await ethers.getContractFactory('MilkStaking');
  //const milkStaking = await upgrades.deployProxy(MilkStakingFactory, [distributor, xIXT, milk, rewardsDuration, lockPeriod2]);
  const milkStaking1 = await MilkStakingFactory.attach('0x0A5935af2f40C713eFfcC74236Ff7a154A5C062b');
  const milkStaking6 = await MilkStakingFactory.attach('0x40dcef5b355877A98f9Be18f53565cc1B6C4fBE4');

  console.log('Contract Deployed To', milkStaking.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
