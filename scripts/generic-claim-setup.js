const { ethers, upgrades } = require('hardhat');

const PIX_ASSETS = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const VOUCHERS = '0xBa60120b7C5c4159FF259D5f75Ae76a84B13256e';
const GG_TOKENS = '0x3376C61c450359d402F07909Bda979a4c0e6c32F';
const AGOLDLITE = '0x9308A7116106269eB11834dF494eFd00d244cF8e';

async function main() {
  const GGWrapperFactory = await ethers.getContractFactory('GravityGradeMintWrapper');
  const ggWrapper = await upgrades.deployProxy(GGWrapperFactory, []);
  await ggWrapper.deployed();
  console.log(`Wrapper deployed to ${ggWrapper.address}`);

  await ggWrapper.setGravityGradeContract(GG_TOKENS);
  console.log(`GG Token Contract Set on Wrapper ${GG_TOKENS}`);

  const GGFactory = await ethers.getContractFactory('GravityGrade');
  const gravityGrade = GGFactory.attach(GG_TOKENS);
  await gravityGrade.setTrusted(ggWrapper.address, true);
  console.log('Set gg wrapper as trusted on GG Contract');

  const GenericBurnV2Factory = await ethers.getContractFactory('GenericBurnV2');
  const genericBurnV2 = await upgrades.deployProxy(GenericBurnV2Factory, []);
  await genericBurnV2.deployed();
  console.log(`generic burn v2 deployed to ${genericBurnV2.address}`);

  const PIXAssetsFactory = await ethers.getContractFactory('NFT');
  const pixAssets = PIXAssetsFactory.attach(PIX_ASSETS);
  await pixAssets.setTrustedContract(genericBurnV2.address);
  await ggWrapper.setTrusted(genericBurnV2.address, true);
  console.log('generic burn v2 set trusted on gg wrapper and pix assets');

  await genericBurnV2.whitelistToken(
    [ggWrapper.address, ggWrapper.address, ggWrapper.address, ggWrapper.address, ggWrapper.address, ggWrapper.address, VOUCHERS, VOUCHERS, VOUCHERS, VOUCHERS],
    [18, 19, 20, 21, 22, 23, 1, 2, 3, 4],
    [true, true, true, true, true, true, true, true, true, true],
  );
  console.log('All tokens whiteslisted');

  await genericBurnV2.setTokenBurnLimit(
    [ggWrapper.address, ggWrapper.address, ggWrapper.address, ggWrapper.address, ggWrapper.address, ggWrapper.address, VOUCHERS, VOUCHERS, VOUCHERS, VOUCHERS],
    [18, 19, 20, 21, 22, 23, 1, 2, 3, 4],
    [100, 100, 100, 100, 100, 100, 100, 100, 100, 100],
  );
  console.log('Burn limits set');

  await genericBurnV2.setGuaranteedRewards(ggWrapper.address, 18, [
    { token: PIX_ASSETS, tokenId: 37, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 72, tokenAmount: 1 },
  ]);
  console.log('GG Token 18 set');

  await genericBurnV2.setGuaranteedRewards(ggWrapper.address, 19, [
    { token: PIX_ASSETS, tokenId: 32, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 37, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 38, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 72, tokenAmount: 3 },
    { token: PIX_ASSETS, tokenId: 31, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 5 },
  ]);
  console.log('GG Token 19 set');

  await genericBurnV2.setGuaranteedRewards(ggWrapper.address, 20, [
    { token: PIX_ASSETS, tokenId: 51, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 32, tokenAmount: 2 },
    { token: PIX_ASSETS, tokenId: 37, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 38, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 31, tokenAmount: 5 },
    { token: PIX_ASSETS, tokenId: 73, tokenAmount: 1 },
    { token: AGOLDLITE, tokenId: 0, tokenAmount: ethers.utils.parseEther('12') },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 10 },
    { token: PIX_ASSETS, tokenId: 0, tokenAmount: 1 },
    { token: ggWrapper.address, tokenId: 16, tokenAmount: 1 },
  ]);
  console.log('GG Token 20 set');

  await genericBurnV2.setGuaranteedRewards(ggWrapper.address, 21, [
    { token: PIX_ASSETS, tokenId: 32, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 3 },
    { token: PIX_ASSETS, tokenId: 72, tokenAmount: 1 },
  ]);
  console.log('GG Token 21 set');

  await genericBurnV2.setGuaranteedRewards(ggWrapper.address, 22, [
    { token: PIX_ASSETS, tokenId: 32, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 0, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 73, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 28, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 31, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 5 },
  ]);
  console.log('GG Token 22 set');

  await genericBurnV2.setGuaranteedRewards(ggWrapper.address, 23, [
    { token: PIX_ASSETS, tokenId: 51, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 32, tokenAmount: 3 },
    { token: PIX_ASSETS, tokenId: 0, tokenAmount: 2 },
    { token: PIX_ASSETS, tokenId: 28, tokenAmount: 5 },
    { token: PIX_ASSETS, tokenId: 31, tokenAmount: 5 },
    { token: PIX_ASSETS, tokenId: 74, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 10 },
    { token: ggWrapper.address, tokenId: 16, tokenAmount: 1 },
  ]);
  console.log('GG Token 23 set');

  await genericBurnV2.setGuaranteedRewards(VOUCHERS, 1, [
    { token: PIX_ASSETS, tokenId: 37, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 72, tokenAmount: 1 },
  ]);
  console.log('Voucher 1 set');

  await genericBurnV2.setGuaranteedRewards(VOUCHERS, 2, [
    { token: PIX_ASSETS, tokenId: 32, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 37, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 38, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 72, tokenAmount: 3 },
    { token: PIX_ASSETS, tokenId: 31, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 5 },
  ]);
  console.log('Voucher 2 set');

  await genericBurnV2.setGuaranteedRewards(VOUCHERS, 3, [
    { token: PIX_ASSETS, tokenId: 51, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 32, tokenAmount: 2 },
    { token: PIX_ASSETS, tokenId: 37, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 38, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 31, tokenAmount: 5 },
    { token: PIX_ASSETS, tokenId: 73, tokenAmount: 1 },
    { token: AGOLDLITE, tokenId: 0, tokenAmount: ethers.utils.parseEther('12') },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 10 },
    { token: ggWrapper.address, tokenId: 16, tokenAmount: 1 },
  ]);
  console.log('Voucher 3 set');

  await genericBurnV2.setGuaranteedRewards(VOUCHERS, 4, [
    { token: PIX_ASSETS, tokenId: 51, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 32, tokenAmount: 5 },
    { token: PIX_ASSETS, tokenId: 37, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 38, tokenAmount: 1 },
    { token: PIX_ASSETS, tokenId: 31, tokenAmount: 10 },
    { token: PIX_ASSETS, tokenId: 28, tokenAmount: 10 },
    { token: PIX_ASSETS, tokenId: 74, tokenAmount: 1 },
    { token: AGOLDLITE, tokenId: 0, tokenAmount: ethers.utils.parseEther('36') },
    { token: PIX_ASSETS, tokenId: 15, tokenAmount: 25 },
    { token: ggWrapper.address, tokenId: 16, tokenAmount: 2 },
  ]);
  console.log('Voucher 4 set');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
