const { ethers } = require('hardhat');
const ProgressBar = require('progress');

const luckyCatAddress = '0x8555b3258989b3C8F28017939C10C63159E18C70';
const landmark = '0xE56502D7598EA159ea6664E236cE830081b4fFAa';
const erc1155Treasury = '0xc2BFC947290fb41c906fC5c6748d5e01EE4D34A8';
const erc721Treasury = '0xD7682e41954032AFC91B0DfF584FD17C7285eFa4';
const assetsAddress = '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc';

async function main() {
  const [deployer] = await ethers.getSigners();

  const luckyCatRaffleFactory = await ethers.getContractFactory('LuckyCatRaffleV3');
  const luckyCatRaffle = luckyCatRaffleFactory.attach(luckyCatAddress);

  const assetsFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
  const assets = assetsFactory.attach(assetsAddress);

  const raffleParams = {
    ticketId: 16,
    prizeAddress: landmark,
    prizeType: 1,
    prizeIds: [1000],
    prizeAmounts: [2],
    totalPrizes: 3,
    totalEntries: 10,
    userMinEntries: 1,
    userMaxEntries: 1,
    startTime: 0,
    duration: 240,
    raffleTreasury: erc1155Treasury,
  };

  let tx = await luckyCatRaffle.registerRaffle(raffleParams);
  let currentRaff = (await tx.wait()).events[1].args[0];

  let tx2 = await luckyCatRaffle.setPaused(currentRaff, false);
  console.log((await tx2.wait()).transactionHash);
  console.log(`Created And Unpaused Raffle ${currentRaff}`);

  let gasPrice = await ethers.provider.getGasPrice();
  gasPrice = gasPrice.mul(3);

  // Generate 5 new accounts, send them Ether, assets and have them enter the raffle
  for (let i = 0; i < 3; i++) {
    const wallet = ethers.Wallet.createRandom();
    console.log(`Account ${i} Address: ${wallet.address}`);

    // Send Ether to the new wallet
    const amountToSend = ethers.utils.parseEther('0.01'); // Convert to wei
    let tx = await deployer.sendTransaction({
      to: wallet.address,
      value: amountToSend,
      gasPrice: gasPrice,
    });
    await tx.wait();

    // Transfer assets to the new wallet
    const assetAmountToSend = 10; // Number of assets to send`
    tx = await assets.safeTransferFrom(
      deployer.address,
      wallet.address,
      16,
      assetAmountToSend,
      '0x',
      {
        gasPrice: gasPrice,
      },
    );
    await tx.wait();

    // Connect the wallet to the contract and enter the raffle
    const connectedWallet = wallet.connect(deployer.provider);
    let tx3 = await luckyCatRaffle.connect(connectedWallet).enterRaffle(currentRaff, 1, {
      gasPrice: gasPrice,
    });
    console.log(`Account ${i} entered raffle: ${(await tx3.wait()).transactionHash}`);
  }

  // Create new progress bar instance
  const durationInSeconds = raffleParams.duration;
  const bar = new ProgressBar(':bar :percent :etas', { total: durationInSeconds });

  // Update progress bar every second
  const interval = setInterval(() => {
    bar.tick();
    if (bar.complete) {
      clearInterval(interval);
    }
  }, 1000);

  console.log(`Drawing Winners In ${durationInSeconds} seconds...`);
  return new Promise((resolve) => {
    setTimeout(async () => {
      clearInterval(interval); // stop the progress bar
      const drawTx = await luckyCatRaffle.drawWinners(currentRaff);
      console.log(`\nWinners drawn txId: ${(await drawTx.wait()).transactionHash}`);
      resolve(); // Resolve the promise when setTimeout callback is done
    }, durationInSeconds * 1000 + 60); // setTimeout uses milliseconds
  });
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
