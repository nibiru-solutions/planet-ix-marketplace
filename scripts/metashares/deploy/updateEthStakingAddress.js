const { ethers } = require('hardhat');

// Update the Eth Staking address in the Polygon Staking contract
async function main() {
  // fill out these params before running
  const polygonStakingAddress = '0x2806FE8803BC6F99F06F09c9191bC54363eAcE9c';
  const ethStaking = '0xD21F24da5CebB96c9F48A77772EC218c25bE8B3B';

  const PolygonStakingFactory = await ethers.getContractFactory('MetashareStaking');
  const polygonStaking = PolygonStakingFactory.attach(polygonStakingAddress);

  console.log('Updating Eth Staking address in Polygon Staking contract...');
  const tx = await polygonStaking.setEthAddress(ethStaking);
  await tx.wait();
  console.log('Done!');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
