const { ethers } = require('hardhat');

// Initializer parameters - fill in before running deploy
const stakeable_token_id = '1';
const connext = '0xFCa08024A6D4bCc87275b1E4A1E22B71fAD7f649';
const polygonStaking = '0x2806FE8803BC6F99F06F09c9191bC54363eAcE9c';
const metashares = '0xAeeD6406078Ac29C19C6a62d30cbb1c30D1Af582';

async function main() {
  // const CorporationsFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
  // const metashares = await CorporationsFactory.deploy('uri');
  // await metashares.deployed();
  // console.log(`metashares deployed to ${metashares.address}`);

  const MetashareStakingEthFactory = await ethers.getContractFactory('MetashareStakingEth');
  const staking = await upgrades.deployProxy(MetashareStakingEthFactory, [
    stakeable_token_id,
    metashares,
    connext,
    polygonStaking,
  ]);
  await staking.deployed();

  console.log(`staking deployed to ${staking.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
