const { ethers } = require('hardhat');

// Initializer parameters - fill in before running deploy
const metashares = '0x53037f0b395d0518671Fe0E2a766F780Ed8e7cA6';
const connext = '0x2334937846Ab2A3FCE747b32587e1A1A2f6EEC5a';

async function main() {
  const { ixtToken } = await hre.getNamedAccounts();

  const MetashareStakingFactory = await ethers.getContractFactory('MetashareStaking');
  const staking = await upgrades.deployProxy(MetashareStakingFactory, [
    ixtToken,
    metashares,
    connext,
  ]);
  await staking.deployed();

  console.log(`staking deployed to ${staking.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
