const { ethers, upgrades } = require('hardhat');

const stakingAddress = '0x2806FE8803BC6F99F06F09c9191bC54363eAcE9c';

async function main() {
  const MetasharesStaking = await ethers.getContractFactory('MetashareStaking');
  const staking = await upgrades.upgradeProxy(stakingAddress, MetasharesStaking);
  await staking.deployed();
  console.log(`Staking upgraded: ${staking.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
