const { ethers, upgrades } = require('hardhat');

const stakingAddress = '0xD21F24da5CebB96c9F48A77772EC218c25bE8B3B';

async function main() {
  const MetashareStakingEth = await ethers.getContractFactory('MetashareStakingEth');
  const staking = await upgrades.upgradeProxy(stakingAddress, MetashareStakingEth);
  await staking.deployed();
  console.log(`Staking upgraded: ${staking.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
