const { ethers } = require('hardhat');
const { constants } = require('ethers');

const assets = '0x53037f0b395d0518671Fe0E2a766F780Ed8e7cA6';
const gravityGradePacks = '0xF7b0892b05bd7Ae8d55c672f9c2406B651a98157';
const ixt = '0xDFDb9Bc9bED1C061D9B1FD93A4Aef280607A6885';

const pushRewardNextEpochPercents = [
  2500, 2000, 1500, 1000, 500, 500, 500, 500, 400, 200, 200, 200,
];

const ameliaWallet = '0x7d922bf0975424b3371074f54cC784AF738Dac0D';
const rewardWallet = '0x7d922bf0975424b3371074f54cC784AF738Dac0D';

let rewardAmount = ethers.utils.parseEther('1000');
let month = 2_592_000;

async function main() {
  const energyStakingGGFactory = await ethers.getContractFactory('EnergyIXTStakingGG');
  energyStakingGG = await energyStakingGGFactory.attach(
    '0x6f050e1623a003f470eB29eDecb2877a0b9d79f8',
  );

  console.log(`Energy Staking GG Pool deployed to: ${energyStakingGG.address}`);

  let rewardAmount = ethers.utils.parseEther('10000');
  let month = 60;

  const rewardTokenId = 5;
  const rewardTokenAmount = 10;
  const energyCost = 100;
  const ixtCost = 100;
  const lockupTime = month;
  const concurrentStakerCap = 3;
  const lifeTimePlayerCap = 5;
  const paused = false;

  const gravityGradeRewards = {
    rewardTokenId: rewardTokenId,
    rewardTokenAmount: rewardTokenAmount,
    energyCost: energyCost,
    ixtCost: ixtCost,
    lockupTime: lockupTime,
    concurrentStakerCap: concurrentStakerCap,
    lifeTimePlayerCap: lifeTimePlayerCap,
    paused: paused,
  };

  const tx = await energyStakingGG.addReward(gravityGradeRewards);
  await tx.wait();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
