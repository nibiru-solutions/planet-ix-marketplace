const { ethers } = require('hardhat');
const { constants } = require('ethers');

let categoryId;
let tokenId = 12;

async function main() {
  const ggBurnFactory = await ethers.getContractFactory('GravityGradeGenericBurn');
  const ggBurn = await ggBurnFactory.attach('0x3C3dF115FA9336814854DA92572d4F5BA9865D2C');
  console.log("here");
  await ggBurn.setMaxDraws(12, 1);
  await ggBurn.whitelistToken(12, true);
  await ggBurn.setTokenBurnLimit(12, 5);
  console.log("here");
  categoryId = await ggBurn.callStatic.createContentCategory(12);

  await ggBurn.createContentCategory(12);
  await ggBurn.setContentAmounts(12, categoryId, [1], [10]);
  await ggBurn.setContents(
    12,
    categoryId,
    ['0xba6666B118f8303F990f3519DF07e160227cCE87'],
    [16],
    [5],
    [10],
  );

  categoryId = await ggBurn.callStatic.createContentCategory(12);
  await ggBurn.createContentCategory(12);
  await ggBurn.setContentAmounts(12, categoryId, [1], [10]);
  await ggBurn.setContents(
    12,
    categoryId,
    ['0xba6666B118f8303F990f3519DF07e160227cCE87'],
    [17],
    [1],
    [10],
  );

  await ggBurn.whitelistToken(tokenId, true);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
