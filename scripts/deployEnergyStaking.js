const { ethers } = require('hardhat');
const { constants } = require('ethers');

const assets = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const ixt = '0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE';

const pushRewardNextEpochPercents = [
  2500, 2000, 1500, 1000, 500, 500, 500, 500, 400, 200, 200, 200,
];

const ameliaWallet = '0x64E6a9C8eCd0aBcac0C12bDA69E30ea2fe6A7d37';
const rewardDistributorWallet = '0xd94E880075AFa8395Fd61Bf47595c878A84e8aDd';

let month = 2_592_000;

async function main() {
  const energyStakingFactory = await ethers.getContractFactory('EnergyIXTStaking');
  const energyStaking = await upgrades.deployProxy(energyStakingFactory, [ixt, assets]);
  await energyStaking.deployed();

  // await energyStaking.setAmeliaFoundationWallet(ameliaWallet);
  // await energyStaking.setPushRewardNextEpochsPercentages(pushRewardNextEpochPercents);
  // await energyStaking.setRewardDistributor(rewardDistributorWallet);
  // await energyStaking.setRewardPercentageDenominator(10000);
  // await energyStaking.setAmeliaFoundationShare(1000, 10000);

  console.log(`energy staking deployed to ${energyStaking.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
