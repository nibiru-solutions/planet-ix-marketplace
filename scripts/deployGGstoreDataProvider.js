const { ethers, upgrades } = require('hardhat');
const { constants } = require('ethers');
const { upgrade } = require('@connext/nxtp-contracts/dist/src/typechain-types/contracts/shared');

// testnet address
let erc1155genericStoreAddress = '0xbd54DcA0D7AFd2D076D1425b25049d4B6e2f48Ce';

async function main() {
  const storeDataProviderFactory = await ethers.getContractFactory('ERC1155StoreGenericDataProvider');

  const storeDataProvider = await storeDataProviderFactory.deploy(erc1155genericStoreAddress);
  console.log('storeDataProvider deployed to:', storeDataProvider.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
