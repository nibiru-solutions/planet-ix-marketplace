const { ethers, upgrades } = require('hardhat');

async function main() {
  console.log('Deploying arcade giveaway...');
  const arcadeGiveawayFactory = await ethers.getContractFactory('ArcadeGiveaway');
  const arcadeGiveaway = await upgrades.deployProxy(arcadeGiveawayFactory, []);
  console.log('Arcade Giveaway Deployed at', arcadeGiveaway.address);

  await hre.run(`verify:verify`, {
    address: arcadeGiveaway.address,
    constructorArguments: [],
  });

  console.log('Arcade Giveaway Deployed at', arcadeGiveaway.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
