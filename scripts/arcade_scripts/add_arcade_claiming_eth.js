const { ethers, upgrades } = require('hardhat');

async function main() {
  const arcadeGiveawayAddress = '';

  const arcadeGiveawayFactory = await ethers.getContractFactory('ArcadeGiveaway');
  const arcadeGiveaway = await arcadeGiveawayFactory.attach(arcadeGiveawayAddress);

  const PinballClaimings = [
    {
      giveawayHanlder: '', //Genesis Lucky Cat Y Handler
      tokenId: 1,
      amount: 1,
      active: true,
    },
    {
      giveawayHanlder: '', //Genesis Y Handler
      tokenId: 2,
      amount: 1,
      active: true,
    },
    {
      giveawayHanlder: '', //Genesis Newlands
      tokenId: 3,
      amount: 1,
      active: true,
    },
  ];

  //TODO will have to ask Lukas to make sheet copyable, can't get addresses for requiredTokens
  await arcadeGiveaway.createGiveaway(requiredToken, 0, 500, false, PinballClaimings);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
