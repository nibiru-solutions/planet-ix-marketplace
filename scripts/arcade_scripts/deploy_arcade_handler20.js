const { ethers, upgrades } = require('hardhat');

async function main() {
  const rewardAddress = '';
  const arcadeAddress = '';

  console.log('Deploying erc20 handler...');
  const erc20HandlerFactory = await ethers.getContractFactory('ArcadeGiveawayHandler_ERC20');
  const erc20Handler = await upgrades.deployProxy(erc20HandlerFactory, [
    rewardAddress,
    arcadeAddress,
  ]);
  console.log('ERC20 Handler Deployed at', erc20Handler.address);

  await hre.run(`verify:verify`, {
    address: erc20Handler.address,
    constructorArguments: [rewardAddress, arcadeAddress],
  });

  console.log('erc20Handler Deployed at', erc20Handler.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
