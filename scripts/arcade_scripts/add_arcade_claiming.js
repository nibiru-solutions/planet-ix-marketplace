const { ethers, upgrades } = require('hardhat');

async function main() {
  const arcadeGiveawayAddress = '';

  const arcadeGiveawayFactory = await ethers.getContractFactory('ArcadeGiveaway');
  const arcadeGiveaway = await arcadeGiveawayFactory.attach(arcadeGiveawayAddress);

  const PinballClaimings = [
    {
      giveawayHanlder: '', //$IXT Handler
      tokenId: 0,
      amount: 10,
      active: true,
    },
    {
      giveawayHanlder: '', //Cargo Drop Handler
      tokenId: 1,
      amount: 5,
      active: true,
    },
    {
      giveawayHanlder: '', //Golden Ticket Handler
      tokenId: 18,
      amount: 1,
      active: true,
    },
    {
      giveawayHanlder: '', //Pinball Landmark Handler
      tokenId: 526,
      amount: 1,
      active: true,
    },
    {
      giveawayHanlder: '', //Blueprint Handler
      tokenId: 9,
      amount: 1,
      active: true,
    },
    {
      giveawayHanlder: '', //Legendary Biomod Handler
      tokenId: 14,
      amount: 2,
      active: true,
    },
    {
      giveawayHanlder: '', //Rare Biomod Handler
      tokenId: 13,
      amount: 5,
      active: true,
    },
    {
      giveawayHanlder: '', //Uncommon Biomod Handler
      tokenId: 12,
      amount: 10,
      active: true,
    },
    {
      giveawayHanlder: '', //Common Biomod Handler
      tokenId: 11,
      amount: 15,
      active: true,
    },
    {
      giveawayHanlder: '', //Outlier Biomod Handler
      tokenId: 10,
      amount: 20,
      active: true,
    },
    {
      giveawayHanlder: '', //AstroCredits Handler
      tokenId: 8,
      amount: 1000,
      active: true,
    },
  ];

  const MetaDragonzClaimings = [
    {
      giveawayHanlder: '', //IXT HANDLER
      tokenId: 0,
      amount: 39.6,
      active: true,
    },
    {
      giveawayHanlder: '', //GG MEDIUM PACK HANDLER
      tokenId: 12,
      amount: 1,
      active: true,
    },
    {
      giveawayHanlder: '', //REGULAR TICKET HANDLER
      tokenId: 16,
      amount: 50,
      active: true,
    },
    {
      giveawayHanlder: '', //PREMIUM TICKET HANDLER
      tokenId: 17,
      amount: 25,
      active: true,
    },
  ];

  const ArcadeClassicsClaimings = [
    {
      giveawayHanlder: '', //IXT HANDLER
      tokenId: 0,
      amount: 21.56,
      active: true,
    },
    {
      giveawayHanlder: '', //GG MEDIUM PACK HANDLER
      tokenId: 12,
      amount: 1,
      active: true,
    },
    {
      giveawayHanlder: '', //REGULAR TICKET HANDLER
      tokenId: 16,
      amount: 20,
      active: true,
    },
    {
      giveawayHanlder: '', //PREMIUM TICKET HANDLER
      tokenId: 17,
      amount: 5,
      active: true,
    },
  ];

  const ToadRunnerzClaimings = [
    {
      giveawayHanlder: '', //IXT HANDLER
      tokenId: 0,
      amount: 16.75,
      active: true,
    },
    {
      giveawayHanlder: '', //GG MEDIUM PACK HANDLER
      tokenId: 12,
      amount: 1,
      active: true,
    },
    {
      giveawayHanlder: '', //REGULAR TICKET HANDLER
      tokenId: 16,
      amount: 20,
      active: true,
    },
    {
      giveawayHanlder: '', //PREMIUM TICKET HANDLER
      tokenId: 17,
      amount: 5,
      active: true,
    },
  ];

  await arcadeGiveaway.createGiveaway(
    '0xa0c38108bbb0f5f2fb46a2019d7314cce38f3f22',
    0,
    500,
    true,
    PinballClaimings,
  );

  //TODO set total supply for MetaDragonz
  await arcadeGiveaway.createGiveaway(
    '0x2e543d056da8f1e4bc7b01c0320e64fc5d656e97',
    0,
    0,
    true,
    MetaDragonzClaimings,
  );

  await arcadeGiveaway.createGiveaway(
    '0xa0c38108bbb0f5f2fb46a2019d7314cce38f3f22',
    501,
    6065,
    true,
    ArcadeClassicsClaimings,
  );

  //TODO set total supply for ToadRunnerz
  await arcadeGiveaway.createGiveaway(
    '0x1e038a99aac19162633dcc4d215e5a27e6eb0355',
    0,
    0,
    true,
    ToadRunnerzClaimings,
  );
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
