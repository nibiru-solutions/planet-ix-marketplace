const { ethers, upgrades } = require('hardhat');

async function main() {
  const rewardAddress = '';
  const arcadeAddress = '';

  console.log('Deploying erc1155 handler...');
  const erc1155HandlerFactory = await ethers.getContractFactory('ArcadeGiveawayHandler_ERC1155');
  const erc1155Handler = await upgrades.deployProxy(erc1155HandlerFactory, [
    rewardAddress,
    arcadeAddress,
  ]);
  await erc1155Handler.deployed();
  console.log('ERC1155 Handler Deployed at', erc1155Handler.address);

  await hre.run(`verify:verify`, {
    address: erc1155Handler.address,
    constructorArguments: [rewardAddress, arcadeAddress],
  });

  console.log('erc1155Handler Deployed at', erc1155Handler.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
