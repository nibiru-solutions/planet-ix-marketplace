const { ethers } = require('hardhat');
const { constants } = require('ethers');
const { waitSeconds } = require('../helper/utils');

// testnet addresses
let assetManagerAddress = "0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc";
let IXT = "0x12168dD03349fa6B3dae6ac8c552b6C86810Bf55";

const M3TAM0D = 0;
const ASTRO_CREDITS = 8;
const ENERGY = 24;
const BIOMOD_OUTLIER = 10;
const BIOMOD_COMMON = 11;
const BIOMOD_UNCOMMON = 12;
const BIOMOD_RARE = 13;
const BIOMOD_LEGENDARY = 14;

let costTokenIds = [ASTRO_CREDITS, BIOMOD_OUTLIER, BIOMOD_COMMON, BIOMOD_UNCOMMON, BIOMOD_RARE, BIOMOD_LEGENDARY, ENERGY];
let costAmounts = [3000, 75, 57, 38, 19, 8, 100];

async function main() {
  const MetamodFusionFactory = await ethers.getContractFactory("MetamodFusion");
  const metamodFusion = await upgrades.deployProxy(
    MetamodFusionFactory, 
    [assetManagerAddress, IXT]
  );
  await metamodFusion.deployed();
  console.log(`MetamodFusion deployed to ${metamodFusion.address}`);

  let tx = await metamodFusion.setFusionCost(costTokenIds, costAmounts);
  await tx.wait();
  console.log(`Fusion cost set: ${tx.hash}`);

  console.log("start verification")
  await waitSeconds(10);
  await hre.run("verify:verify", {
      address: metamodFusion.address
  });
  console.log("Done")
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });