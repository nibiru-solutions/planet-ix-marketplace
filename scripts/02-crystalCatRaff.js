const { ethers, upgrades } = require('hardhat');
const { web3 } = require('hardhat');

async function main() {
  const pixt = '0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE';
  const vrfManager = '0xC9F43cCeDDB64645724bABCB50B30AaE0c5768f3';
  const treasury = '0xa4605a10d830c232291e9396e4afd5f2ca6e3836';
  const pixAssets = '0xba6666b118f8303f990f3519df07e160227cce87';
  const weeklyPrize = web3.utils.toWei("5000", 'ether');
  const CrystalPIXCatRaff = await ethers.getContractFactory('CrystalPIXCatRaff');
  const pixCatRaffV2 = await upgrades.deployProxy(CrystalPIXCatRaff, [pixt, pixAssets, vrfManager, treasury, weeklyPrize]);

  console.log('CrystalPIXCatRaff deployed at', pixCatRaffV2.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
