const { ethers, upgrades } = require('hardhat');

const crosschainService = '0xe210c6E2A9114E9f276eeD8a428b35308090Ab1f';
const ygLock = 'INSERT_AFTER_DEPLOYING_YGLOCK';
const ngLock = '0x3c8E15f0668ed6C3790cBFD4eaBD98059236ef50';

async function main() {
  const RoverMCStakeableAdapterFactory = await ethers.getContractFactory('RoverMCStakeableAdapter');
  const roverAdapter = await upgrades.upgradeProxy('0x43f924C8D203AFE03D5c452eBb603b7E64BA4227', RoverMCStakeableAdapterFactory);
  await roverAdapter.deployed();

  console.log('Rover Adapter upgraded at', roverAdapter.address);

  const RoverFactory = await ethers.getContractFactory('Rover');
  const rover = await upgrades.upgradeProxy('0x836545dFc47F71ff33C458B98b283732A8ceE4F6', RoverFactory);
  await rover.deployed();

  console.log('Rover upgraded at', rover.address);

  const RoverHelperFactory = await ethers.getContractFactory('RoverHelper');
  const roverHelper = await upgrades.deployProxy(RoverHelperFactory, []);
  await roverHelper.deployed();

  console.log('Rover Helper deployed at', rover.address);

  await rover.setCrosschain(crosschainService);
  await rover.setRoverAdapter(roverAdapter.address);
  await rover.setRoverHelper(roverHelper.address);

  const CrosschainFactory = await ethers.getContractFactory('MCCrosschainServices');
  const crosschain = await upgrades.upgradeProxy(crosschainService, CrosschainFactory);
  await crosschain.deployed();
  console.log('Crosschain Services upgraded at', rover.address);

  await crosschain.setValidSource(ygLock, true);
  await crosschain.setValidSource(ngLock, true);

  await crosschain.setYLock(ygLock);
  await crosschain.setNLock(ngLock);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
