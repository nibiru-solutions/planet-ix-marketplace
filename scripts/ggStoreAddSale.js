const { ethers, upgrades } = require('hardhat');

// testnet address
let erc1155genericStoreAddress = '0xbd54DcA0D7AFd2D076D1425b25049d4B6e2f48Ce';

async function main() {
  const ContractFactory = await ethers.getContractFactory('ERC1155StoreGeneric');
  const contract = ContractFactory.attach(erc1155genericStoreAddress);

  let tx1 = await contract.createSale(
    "0x5A330dE8322aA11CF34e6E042e8eB617006397cf",
    21,
    1,
    1000,
    800,
    1000,
    "0x5A330dE8322aA11CF34e6E042e8eB617006397cf",
    false,
    false
  );
  await tx1.wait();
  console.log("tx1", tx1);

  let tx2 = await contract.setERC1155PaymentPrices(
    23,
    "0xba6666B118f8303F990f3519DF07e160227cCE87",
    [24],
    [800]
  );
  await tx2.wait();
  console.log("tx2", tx2);

  let tx3 = await contract.setSaleState(23, false);
  await tx3.wait();
  console.log("tx3", tx3);

  let tx1 = await contract.createSale(
    "0x5A330dE8322aA11CF34e6E042e8eB617006397cf",
    22,
    1,
    100,
    4800,
    100,
    "0x5A330dE8322aA11CF34e6E042e8eB617006397cf",
    false,
    false
  );
  await tx1.wait();
  console.log("tx1", tx1);

  let tx2 = await contract.setERC1155PaymentPrices(
    24,
    "0xba6666B118f8303F990f3519DF07e160227cCE87",
    [24],
    [4800]
  );
  await tx2.wait();
  console.log("tx2", tx2);

  let tx3 = await contract.setSaleState(23, false);
  await tx3.wait();
  console.log("tx3", tx3);

  let tx1 = await contract.createSale(
    "0x5A330dE8322aA11CF34e6E042e8eB617006397cf",
    23,
    1,
    10,
    8800,
    10,
    "0x5A330dE8322aA11CF34e6E042e8eB617006397cf",
    false,
    false
  );
  await tx1.wait();
  console.log("tx1", tx1);

  let tx2 = await contract.setERC1155PaymentPrices(
    25,
    "0xba6666B118f8303F990f3519DF07e160227cCE87",
    [24],
    [8800]
  );
  await tx2.wait();
  console.log("tx2", tx2);

  let tx3 = await contract.setSaleState(23, false);
  await tx3.wait();
  console.log("tx3", tx3);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
