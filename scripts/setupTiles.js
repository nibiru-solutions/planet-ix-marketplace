const { ethers, upgrades } = require('hardhat');
const { AssetIds, Distribution, PIXCategory } = require('../test/utils');

async function main() {
  let mcFactory = await ethers.getContractFactory('MissionControl');
  const missionControl = await mcFactory.attach('0x29295020DaA9D2afBaA6A9bF80E08332Fe8e7a69');

  let adapterFactory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
  const pixadapter = await adapterFactory.attach('0x496092fe78E8C9189282AEd26F1eE1294FDCe337');

  const radius = 2;
  const price = 0;
  const tileContractId = 35;

  let PIX_WASTE_EV = 12;
  let PIX_WASTE_CAP = 3;

  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
        if (rad == radius && sum == 0) {
          console.log(`Now setting up tile ${i},${j}`);
          // await missionControl.setTileRequirements(i, j, price, tileContractId);
          await pixadapter.setResourceParameters(
            AssetIds.Waste,
            PIX_WASTE_EV,
            PIX_WASTE_CAP,
            true,
            i,
            j,
          );
        }
      }
    }
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
