const { ethers, upgrades } = require('hardhat');
const { hasRestParameter } = require('typescript');
const { AssetIds, Distribution, PIXCategory } = require('../test/utils');
require('../helper-hardhat-config');

/// Start MC params

let MC_RADIUS = 1;
let MC_RAID_TIMEOUT = 30;

let PIX_WASTE_EV = 12;
let PIX_WASTE_CAP = 3;

let ASSET_TOKEN_SETTINGS_BASE = {
  token_id: 0,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Poisson,
  resource_ev: 0,
  resource_id: AssetIds.Waste,
  resource_cap: 1,
};

let DRONE_GENESIS = {
  token_id: 4,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 14,
  resource_id: AssetIds.Waste,
  resource_cap: 28,
};
let DRONE_PIERCER = {
  token_id: 5,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.LinearDeterministic,
  resource_ev: 13,
  resource_id: AssetIds.Waste,
  resource_cap: 26,
};
let FACILITY_OUTLIER = {
  token_id: 19,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let FACILITY_COMMON = {
  token_id: 20,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let FACILITY_UNCOMMON = {
  token_id: 21,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let FACILITY_RARE = {
  token_id: 22,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let FACILITY_LEGENDARY = {
  token_id: 23,
  is_stakeable: true,
  is_base: false,
  is_raidable: false,
  distribution: Distribution.Facility,
  resource_ev: 1,
  resource_id: AssetIds.Energy,
  resource_cap: 100,
};

let STAKEABLE_ASSETS = [
  DRONE_GENESIS,
  DRONE_PIERCER,
  FACILITY_OUTLIER,
  FACILITY_COMMON,
  FACILITY_UNCOMMON,
  FACILITY_RARE,
  FACILITY_LEGENDARY,
];

let WASTE_TRADER_NUMERATOR = 1;
let WASTE_TRADER_DENOMINATOR = 1;
let WASTE_TRADER_TIME = 60 * 60 * 6;
let WASTE_TRADER_SPEEDUP_TIME = 60 * 60;
let WASTE_TRADER_MIN_ORDER = 25;
let WASTE_TRADER_TAX = 500;
let WASTE_TRADER_MAX = 100;
let WASTE_TRADER_SPEEDUP_COST = 5;

let PROSPECTOR_FEE = 25;
let PROSPECTOR_TIME = 60 * 60 * 6;
let PROSPECTOR_MAX_ORDERS = 4;
let PROSPECTOR_SPEEDUP_TIME = 60 * 60;
let PROSPECTOR_SPEEDUP_COST = 5;
let PROSPECTOR_TAX = 500;
// Hardcoded weights lmao  check the function

let FACILITY_STORE_TIME = 60 * 60 * 48;
let FACILITY_STORE_SPEEDUP_TOKEN_ID = AssetIds.AstroCredit;
let FACILITY_STORE_SPEEDUP_COST = 10;
let FACILITY_STORE_SPEEDUP_TIME = 60 * 60;
let FACILITY_STORE_MAX_ORDERS = 1;
let FACILITY_STORE_FIXED_PRICE_TOKEN_IDS = [AssetIds.AstroCredit, AssetIds.Blueprint];
let FACILITY_STORE_FIXED_PRICE_AMOUNTS = [1000, 1];
let FACILITY_STORE_BIOMOD_TOKEN_IDS = [
  AssetIds.BioModOutlier,
  AssetIds.BioModCommon,
  AssetIds.BioModUncommon,
  AssetIds.BioModRare,
  AssetIds.BioModLegendary,
];
let FACILITY_STORE_BIOMOD_AMOUNTS = [20, 15, 10, 5, 2];
let FACILITY_STORE_FACILITY_TOKEN_IDS = [
  AssetIds.FacilityOutlier,
  AssetIds.FacilityCommon,
  AssetIds.FacilityUncommon,
  AssetIds.FacilityRare,
  AssetIds.FacilityLegendary,
];
let FACILITY_STORE_PROB_WEIGHTS = [200, 150, 100, 50, 10];

/// End MC params

async function deploy_pix_stakeable(mission_control) {
  let factory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
  let contract = await upgrades.deployProxy(factory, [
    AssetIds.Waste,
    PIX_WASTE_EV,
    PIX_WASTE_CAP,
    true,
  ]);
  await contract.deployed();
  // await contract.setMissionControl(mission_control);
  // await mission_control.setWhitelist(contract.address, true);
  console.log('Deployed ERC721MCStakeableAdapter at: ', contract.address);
  return contract;
}

async function deploy_asset_stakeable(
  asset_address,
  pix_address,
  pix_stakeable_address,
  mission_control,
) {
  let factory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  let contract = await upgrades.deployProxy(factory);
  await contract.deployed();
  await contract.setERC1155(asset_address);
  await contract.setRarityProvider(pix_address, pix_stakeable_address);
  await contract.setFacilityStakeRequirements(AssetIds.FacilityOutlier, PIXCategory.Outliers, true);
  await contract.setFacilityStakeRequirements(AssetIds.FacilityCommon, PIXCategory.Common, true);
  await contract.setFacilityStakeRequirements(
    AssetIds.FacilityUncommon,
    PIXCategory.Uncommon,
    true,
  );
  await contract.setFacilityStakeRequirements(AssetIds.FacilityRare, PIXCategory.Rare, true);
  await contract.setFacilityStakeRequirements(
    AssetIds.FacilityLegendary,
    PIXCategory.Legendary,
    true,
  );
  for (const token of STAKEABLE_ASSETS) {
    await contract.setTokenInfo(
      token.token_id,
      token.is_stakeable,
      token.is_base,
      token.is_raidable,
      token.distribution,
      token.resource_ev,
      token.resource_id,
      token.resource_cap,
    );
  }
  let hour = 60 * 60;
  await contract.setFacilityStats(
    [8 * hour, 12 * hour, 16 * hour, 20 * hour, 24 * hour],
    [1, 3, 4, 5, 6],
  );
  await contract.setMissionControl(mission_control.address);
  await mission_control.setWhitelist(contract.address, true);
  console.log('Deployed PIXAssetStakeableAdapter at: ', contract.address);
  return contract;
}

async function deploy_mission_control(asset_address, signer, radius) {
  let factory = await ethers.getContractFactory('MissionControl');
  let contract = await upgrades.deployProxy(factory, [radius, asset_address]);
  await contract.setSignature(true);
  await contract.setRaidSigner(signer, true); // Confusing name but this sets both types of signer
  await contract.setRaidParams(MC_RAID_TIMEOUT);
  console.log('Deployed MissionControl at: ', contract.address);

  // await hre.run('verify:verify', {
  //   address: contract.address,
  //   constructorArguments: [radius, asset_address],
  // });

  return contract;
}

async function deploy_vrf_manager(vrfCoordinatorAddress) {
  let vrfManagerFactory = await ethers.getContractFactory('VRFManager');
  let vrfManager = await vrfManagerFactory.deploy(vrfCoordinatorAddress);

  return vrfManager;
}

async function deploy_waste_trader(pix_assets_address, gws_wallet_address) {
  let factory = await ethers.getContractFactory('WasteTrader');
  let contract = await upgrades.deployProxy(factory, [
    pix_assets_address,
    gws_wallet_address,
    WASTE_TRADER_NUMERATOR,
    WASTE_TRADER_DENOMINATOR,
    WASTE_TRADER_TIME,
    WASTE_TRADER_SPEEDUP_TIME,
    WASTE_TRADER_MIN_ORDER,
    WASTE_TRADER_TAX,
    WASTE_TRADER_MAX,
    WASTE_TRADER_SPEEDUP_COST,
  ]);
  await contract.deployed();
  console.log('Deployed Waste Trader at: ', contract.address);
  return contract;
}

async function deploy_prospector(fee_wallet, asset_address, vrf_manager, vrf_params) {
  let factory = await ethers.getContractFactory('Prospecting');
  let contract = await upgrades.deployProxy(factory, [
    fee_wallet,
    PROSPECTOR_FEE,
    PROSPECTOR_TIME,
    PROSPECTOR_MAX_ORDERS,
    PROSPECTOR_SPEEDUP_TIME,
    PROSPECTOR_SPEEDUP_COST,
    PROSPECTOR_TAX,
  ]);
  await contract.deployed();
  await contract.setIAssetManager(asset_address);
  await contract.setOracle(vrf_manager.address);
  await contract.setBiomodWeights(
    [
      0,
      AssetIds.BioModOutlier,
      AssetIds.BioModCommon,
      AssetIds.BioModUncommon,
      AssetIds.BioModRare,
      AssetIds.BioModLegendary,
      AssetIds.Blueprint,
    ],
    [300, 200, 150, 100, 50, 20, 10],
  );
  await vrf_manager.setConsumer(
    contract.address,
    vrf_params.sub_id,
    vrf_params.key_hash,
    vrf_params.callback_gas_limit,
    vrf_params.req_conf,
    vrf_params.num_words,
  );
  console.log('Deployed Prospector at: ', contract.address);
  return contract;
}

async function deploy_facility_store(asset_address, fee_wallet, vrf_manager, vrf_params) {
  let settings = {
    _assetManager: asset_address,
    _feeWallet: fee_wallet,
    _facilityTime: FACILITY_STORE_TIME,
    _speedupPriceTokenId: FACILITY_STORE_SPEEDUP_TOKEN_ID,
    _speedupPrice: FACILITY_STORE_SPEEDUP_COST,
    _speedupTime: FACILITY_STORE_SPEEDUP_TIME,
    _maxOrders: FACILITY_STORE_MAX_ORDERS,
    _fixedPriceTokenIds: FACILITY_STORE_FIXED_PRICE_TOKEN_IDS,
    _fixedPriceTokenAmounts: FACILITY_STORE_FIXED_PRICE_AMOUNTS,
    _biomodTokenIds: FACILITY_STORE_BIOMOD_TOKEN_IDS,
    _biomodPriceAmounts: FACILITY_STORE_BIOMOD_AMOUNTS,
    _facilityTokenIds: FACILITY_STORE_FACILITY_TOKEN_IDS,
    _facilityProbabilityWeights: FACILITY_STORE_PROB_WEIGHTS,
    _vrfCoordinator: vrf_manager.address,
  };
  let factory = await ethers.getContractFactory('FacilityStore');
  let contract = await upgrades.deployProxy(factory, [settings]);
  await contract.deployed();
  await vrf_manager.setConsumer(
    contract.address,
    vrf_params.sub_id,
    vrf_params.key_hash,
    vrf_params.callback_gas_limit,
    vrf_params.req_conf,
    vrf_params.num_words,
  );
  console.log('Deployed Facility Store at: ', contract.address);
  return contract;
}

async function main() {
  let deploy_traders = false; // Should be false on 8th nov
  let set_trusted = false; // Should be false on 8th nov
  let pix_address = '0xB2435253C71FcA27bE41206EB2793E44e1Df6b6D'; // Pix contract address
  let pix_assets_address = '0xba6666B118f8303F990f3519DF07e160227cCE87'; // NFT.sol address
  let albans_public_key = '0x24183be0440409EbcD1c70260D98ab1268E36AC4'; // The key alban uses for sigs on the backend

  let vrf_coordinator_address = '0xAE975071Be8F8eE67addBC1A82488F1C24858067';

  let pix_asset_stakeable_address = '0xb17Fa3ECB312A3757F95b98f2D44A29392E9Bb7b';
  let erc721_mc_stakeable_adapter_address = '0xBB93395df76d2f8E4ace48E5dEFaB43da18878AA';
  let mission_control_address = '0x78f6ef4FA55e41b816cbc0f97a2D8743ba8795d9';

  // These are not needed for the 8th Nov deploy
  let gws_wallet_address = "0x4d233b9066F3A5Cdb3DBd2e887c29144c0152315";
  let eternalabs_wallet_address = "0x3360C99dc43F724Ba0180C2b78E397f0B9C6678A";
  let newlands_wallet_address = "0xECd0ae7aF68c1230A18238bAeD1fC49AE4f057A4";
  let vrf_manager_address = '';
  let vrf_params = {
    sub_id: 358,
    key_hash: '0xd729dc84e21ae57ffb6be0053bf2b0668aa2aaf300a2a7b2ddf7dc0bb6e875a8',
    callback_gas_limit: 300000,
    req_conf: 3,
    num_words: 1, // Should always be just 1
  };

  let waste_trader;
  let prospector;
  let facility_store;

  // await deploy_mission_control(
  // '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
  // '0x7d922bf0975424b3371074f54cC784AF738Dac0D',
  // 2,
  // );

  await deploy_asset_stakeable(
    '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    '0x1eB138c81590683343C3f1c855858c684f15920a',
  );

  // await deploy_pix_stakeable(
  //   '0x1eB138c81590683343C3f1c855858c684f15920a',
  //   '0x215e26B3D4fD1E73837254B809e2B6A2c6967781',
  // );

  if (deploy_traders) {
    let vrf_manager = await deploy_vrf_manager(vrf_coordinator_address);
    waste_trader = await deploy_waste_trader(pix_assets_address, gws_wallet_address);
    prospector = await deploy_prospector(
      eternalabs_wallet_address,
      pix_assets_address,
      vrf_manager,
      vrf_params,
    );
    facility_store = await deploy_facility_store(
      pix_assets_address,
      newlands_wallet_address,
      vrf_manager,
      vrf_params,
    );
  }

  if (set_trusted) {
    let pix_assets_fac = await ethers.getContractFactory('NFT');
    let pix_assets = await pix_assets_fac.attach(pix_assets_address);

    await pix_assets.setTrustedContract(mission_control_address);
    await pix_assets.setTrustedContract(waste_trader.address);
    await pix_assets.setTrustedContract(prospector.address);
    await pix_assets.setTrustedContract(facility_store.address);
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
