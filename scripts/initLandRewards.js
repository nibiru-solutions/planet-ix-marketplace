const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');

async function main() {
  const LandStaking = await hre.ethers.getContractFactory('PIXLandStaking');
  const landStaking = await LandStaking.attach('0xB030dA155b6F3e67921157Ec7793EDF3e407a55B');
  const IXT = await hre.ethers.getContractFactory('PIXT');
  const ixt = await IXT.attach('0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE');
  const csvFilePath = './scripts/july-rewards.csv';

  const data = await csv().fromFile(csvFilePath);

  let index = 0;
  let value = 0;

  while (index < data.length) {
    // reset arrays
    recipients = [];
    nfts = [];
    amounts = [];
    // fill up array with next 150 items
    value = 0;
    for (let i = 0; i < 100; i++) {
      // break out of here if data is complete, batch is full
      if (index == data.length) {
        break;
      }
      nfts.push(data[index]['tokenID']);
      let epochTotalReward1 = web3.utils.toWei(data[index]['amount']);
      amounts.push(epochTotalReward1);
      value += Number(data[index]['amount']);
      index++;
    }


    console.log("nfts", nfts)
    console.log("amounts", amounts)
    console.log("value", value)


    await ixt.increaseAllowance(
      '0xB030dA155b6F3e67921157Ec7793EDF3e407a55B',
      web3.utils.toWei(value.toString(), 'ether'),
    );

    console.log("increased allowance")

    const response = await landStaking.initializePools(
      nfts,
      amounts,
      2592000,
      { gasPrice: 250000000000, gasLimit: 9000000 },
    );

    const receipt = await response.wait(1);
    console.log(receipt);
    console.log('Set', '============================', index);

  }
}

main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
