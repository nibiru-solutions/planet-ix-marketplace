const { ethers } = require('hardhat');

const MissionControlStreamABI = require('./MissionControlStream.json');
const ISuperfluid = require('@superfluid-finance/ethereum-contracts/build/contracts/ISuperfluid');
const IConstantFlowAgreementV1 = require('@superfluid-finance/ethereum-contracts/build/contracts/IConstantFlowAgreementV1');
const ISuperToken = require('@superfluid-finance/ethereum-contracts/build/contracts/ISuperToken');

// price per second for each tile

let helios_tiles = [
  { x: -3, y: 0, z: 3 },
  { x: -3, y: 3, z: 0 },
  { x: 0, y: -3, z: 3 },
  { x: 0, y: 3, z: -3 },
  { x: 3, y: -3, z: 0 },
  { x: 3, y: 0, z: -3 },
];

let chronos_tile = [
  { x: -3, y: 1, z: 2 },
  { x: -3, y: 2, z: 1 },
  { x: -2, y: -1, z: 3 },
  { x: -2, y: 3, z: -1 },
  { x: -1, y: -2, z: 3 },
  { x: -1, y: 3, z: -2 },
  { x: 1, y: -3, z: 2 },
  { x: 1, y: 2, z: -3 },
  { x: 2, y: -3, z: 1 },
  { x: 2, y: 1, z: -3 },
  { x: 3, y: -2, z: -1 },
  { x: 3, y: -1, z: -2 },
];

let artemis_tile = [
  { x: -2, y: 0, z: 2 },
  { x: -2, y: 1, z: 1 },
  { x: -2, y: 2, z: 0 },
  { x: -1, y: -1, z: 2 },
  { x: -1, y: 2, z: -1 },
  { x: 0, y: -2, z: 2 },
  { x: 0, y: 2, z: -2 },
  { x: 1, y: -2, z: 1 },
  { x: 1, y: 1, z: -2 },
  { x: 2, y: -2, z: 0 },
  { x: 2, y: -1, z: -1 },
  { x: 2, y: 0, z: -2 },
];

let apollo_tile = [
  { x: -1, y: 0, z: 1 },
  { x: -1, y: 1, z: 0 },
  { x: 0, y: -1, z: 1 },
  { x: 0, y: 1, z: -1 },
  { x: 1, y: -1, z: 0 },
  { x: 1, y: 0, z: -1 },
];

const encodePlaceOrder = (x, y, z) => {
  return ethers.utils.defaultAbiCoder.encode(['tuple(int256, int256, int256)[]'], [[[x, y, z]]]);
};

const encodeAdditionalTile = (newX, newY, newZ) => {
  return ethers.utils.defaultAbiCoder.encode(
    ['tuple(int256, int256, int256)[]', 'tuple(int256, int256, int256)[]'],
    [[[newX, newY, newZ]], []],
  );
};

const encodeRemoveTile = (removeX, removeY, removeZ) => {
  return ethers.utils.defaultAbiCoder.encode(
    ['tuple(int256, int256, int256)[]', 'tuple(int256, int256, int256)[]'],
    [[], [[removeX, removeY, removeZ]]],
  );
};

const hostAddress = '0xEB796bdb90fFA0f28255275e16936D25d3418603';
const cfaAddress = '0x49e565Ed1bdc17F3d220f72DF0857C26FA83F873';
const missionAddress = '0xA71dcF8B49890F1D2C4809bed7a2Be44bA64A406';
const superTokenAddress = '0x3CAD7147c15C0864B8cF0EcCca43f98735e6e782';

// let userData = encodePlaceOrder(tile.x, tile.y, tile.z);
// let userData = encodeAdditionalTile(tile.x, tile.y, tile.z);
// let userData = encodeRemoveTile(tile.x, tile.y, tile.z);

let cfaV1, host, superApp, wallet, superToken;

function INIT() {
  if (wallet === undefined) throw 'set global wallet first...';
  host = new ethers.Contract(hostAddress, ISuperfluid.abi, wallet);
  cfaV1 = new ethers.Contract(cfaAddress, IConstantFlowAgreementV1.abi, wallet);
  superApp = new ethers.Contract(missionAddress, MissionControlStreamABI, wallet);
  superToken = new ethers.Contract(superTokenAddress, ISuperToken.abi, wallet);
}

async function main() {
  // Configurations
  const url = `https://polygon-mumbai.g.alchemy.com/v2/${process.env.ALCHEMY_KEY}`;
  const provider = new ethers.providers.JsonRpcProvider(url);
  const privateKey = process.env.PRIVATE_KEY_TESTNET;
  wallet = new ethers.Wallet(privateKey, provider);

  // instance contracts
  INIT();

  const MCFactory = await ethers.getContractFactory('MissionControl');
  const mc = MCFactory.attach('0x0F4497805302E901fD9d4131eA70240474240a5d');

  //encode userData to send with stream. userData = PlaceOrder[]

  let j = 0;
  let ring = helios_tiles;
  for (let i = 0; i < ring.length; i++) {
    console.log('RENTING....', ring[i].x, ring[i].y, ring[i].z);
    let tileInfo = await mc.tileRentalInfo(
      '0x7d922bf0975424b3371074f54cC784AF738Dac0D',
      ring[i].x,
      ring[i].y,
    );
    if (tileInfo.isRented) continue;

    let action = 'updateFlow';

    j++;
    let multiplier = 19 + j;
    let flowRate = 385802469135 * multiplier;
    let tile = { x: ring[i].x, y: ring[i].y, z: ring[i].z };
    let userData = encodeAdditionalTile(tile.x, tile.y, tile.z);

    // call superApp
    const callData = cfaV1.interface.encodeFunctionData(action, [
      superTokenAddress,
      missionAddress,
      flowRate,
      '0x',
    ]);
    const tx = await host.connect(wallet).callAgreement(cfaAddress, callData, userData);
    console.log(tx.hash);
    await tx.wait();
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
