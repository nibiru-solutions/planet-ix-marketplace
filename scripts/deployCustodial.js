const { ethers, upgrades } = require('hardhat');

async function main() {

    let erc20Factory = await hre.ethers.getContractFactory('ERC20Mock');
    //let emilCoin = await erc20Factory.deploy('EmilCoin', 'EC');
    let emilCoin = await erc20Factory.attach('0x3F1D7D306AA18EF94B9b9e76A7F118723128A57B');

    //await emilCoin.deployed()
    console.log("contract EmilCoin deployed", emilCoin.address)

    const ContractFactory = await ethers.getContractFactory('PIXOffchainCustodial');
    //const pixOffchainCustodial = await upgrades.deployProxy(ContractFactory,[emilCoin.address]);
    //await pixOffchainCustodial.deployed();
    const pixOffchainCustodial = await ContractFactory.attach('0xce4225B83003f93140E4CF06172C9629d03BC7D5');

    console.log('PIXOffchainCustodial deployed at', pixOffchainCustodial.address);
    let fiveHundredTokens = ethers.utils.parseEther("500");
    let tenKTokens = ethers.utils.parseEther("10000");
    let fiftyKTokens = ethers.utils.parseEther("50000");

    //await emilCoin.mint('0x4f742BA964e7630fEB332197BFF0b433e2029585', fiftyKTokens);

    //await emilCoin.approve(pixOffchainCustodial.address, tenKTokens);
    //await pixOffchainCustodial.deposit(fiveHundredTokens);

    //await pixOffchainCustodial.setSigner(signer.address);
    //await pixOffchainCustodial.setWithdrawalLimit(tenKTokens);
    //await pixOffchainCustodial.setSignatureValidityPeriod(300);
    await pixOffchainCustodial.setSigner('0xD7Ad58Ab64F2Bf86017Df74081C0645de07F0D89');
    await pixOffchainCustodial.setAdminSigners('0x404E4b5AA02051e8D2caA9fBc8B70D27Ab2e94A2');
}


main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
