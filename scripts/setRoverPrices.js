const { ethers } = require('hardhat');

// Init params - fill these in before running the script
let roverAddress = '0x836545dFc47F71ff33C458B98b283732A8ceE4F6';

let ixtNamePrice = ethers.utils.parseEther('25');
let acNamePrice = ethers.utils.parseEther('1000');

let ixtRepairWornPrice = ethers.utils.parseEther('1');
let ixtRepairDamagedPrice = ethers.utils.parseEther('2.5');
let acRepairWornPrice = ethers.utils.parseEther('30');
let acRepairDamagedPrice = ethers.utils.parseEther('80');

let purchaseUSDTprice = 10_000_000; // 10 usdt, it has 6 decimals

const PayableToken = {
  IXT: 0,
  AstroCredits: 1,
  USDT: 2,
  Matic: 3,
};

const Status = {
  Pristine: 0,
  Worn: 1,
  Damaged: 2,
  Wrecked: 3,
};

async function main() {
  const RoverFactory = await ethers.getContractFactory('Rover');
  const rover = RoverFactory.attach(roverAddress);

  console.log(purchaseUSDTprice);

  await rover.setNamePrice(PayableToken.IXT, ixtNamePrice);
  await rover.setNamePrice(PayableToken.AstroCredits, acNamePrice);

  await rover.setRepairPrice(PayableToken.IXT, Status.Worn, ixtRepairWornPrice);
  await rover.setRepairPrice(PayableToken.IXT, Status.Damaged, ixtRepairDamagedPrice);
  await rover.setRepairPrice(PayableToken.AstroCredits, Status.Worn, acRepairWornPrice);
  await rover.setRepairPrice(PayableToken.AstroCredits, Status.Damaged, acRepairDamagedPrice);

  await rover.setPurchaseUSDTprice(purchaseUSDTprice);
  await rover.setOracleManager('0xA1e722dA2F27201B7394A73a2D42ff5d43F9B14D');

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
