const { ethers, upgrades } = require('hardhat');
const { web3 } = require('hardhat');

async function main() {
  const catRaff = '0x63679581F4c20531A0BA987f8dD843d0f67Adde8';

  const PIXCatRaffAutomationV2 = await ethers.getContractFactory('PIXCatRaffKeeperV2');
  const automation = await upgrades.deployProxy(PIXCatRaffAutomationV2, [catRaff]);

  console.log('PIXCatRaffKeeperV2 deployed at', automation.address);

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
