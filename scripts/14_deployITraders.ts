import hre from "hardhat";
import {BigNumber, ethers} from "ethers";

const erc1155MockAssteManager = require("../artifacts/contracts/mock/ERC1155MockAssetManager.sol/ERC1155MockAssetManager.json");
const erc1155MockAssteManagerAbi = erc1155MockAssteManager.abi;

const testnetAssetManagerAddress = "0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc"
const testnetFeeWalletAddress = "0xbC3244a304698e7a15817737CF2E603C2AF8eEed" // Emils wallet
const testnetVRFCoordinatorAddress = "0xBE7f0e2D5813ee339B5686B7BF95D4401bD3D325"

async function deployFacilityStore(){
    const params = {
        _assetManager: testnetAssetManagerAddress,
        _feeWallet: testnetFeeWalletAddress,
        _facilityTime: BigNumber.from(1000),
        _speedupPriceTokenId: BigNumber.from(10),
        _speedupPrice: BigNumber.from(5),
        _speedupTime: BigNumber.from(600),
        _maxOrders: BigNumber.from(5),
        _fixedPriceTokenIds: [BigNumber.from(1), BigNumber.from(2)],
        _fixedPriceTokenAmounts: [BigNumber.from(1000), BigNumber.from(100)],
        _biomodTokenIds: [BigNumber.from(3), BigNumber.from(4)],
        _biomodPriceAmounts: [BigNumber.from(1000), BigNumber.from(100)],
        _facilityTokenIds: [BigNumber.from(5), BigNumber.from(6)],
        _facilityProbabilityWeights: [BigNumber.from(1), BigNumber.from(10)],
        _vrfCoordinator: testnetVRFCoordinatorAddress
    }

    let facilityStoreFactory = await hre.ethers.getContractFactory("FacilityStore");
    let facilityStore = await hre.upgrades.deployProxy(facilityStoreFactory, [params])

    await facilityStore.deployed();
    console.log("Contract deployed. facilityStore" , facilityStore.address);

    //AssetManager set up.
    const alchemy = new hre.ethers.providers.AlchemyProvider('maticmum', process.env.ALCHEMY_API_KEY);
    const userWallet = new hre.ethers.Wallet(process.env.PRIVATE_KEY, alchemy);
    const mockAsstetManagerContract = new hre.ethers.Contract(testnetAssetManagerAddress, erc1155MockAssteManagerAbi, userWallet);
    await mockAsstetManagerContract.connect(userWallet).setApprovalForAll(facilityStore.address, true); // have not done this..
    await mockAsstetManagerContract.connect(userWallet).setTrusted(testnetFeeWalletAddress, true);
    await mockAsstetManagerContract.connect(userWallet).setTrusted(facilityStore.address, true);

    //Ask VRFCoordinator Owner to call setNumWords(facilityStore.address, numWords)

    //then this needs to be done:
    /*await assetManager.connect(alice).setTrusted(params._feeWallet, true);
    await assetManager.connect(alice).setTrusted(facilityStore.address, true);
    await vrfCoordinatorMock.setNumWords(facilityStore.address, 6);*/
    console.log("facility store setup done")
}

async function deployProspecting(){
    let PROSPECTOR_FEE = 25
    let PROSPECTOR_TIME = 21600
    let MAX_ORDERS = 4
    let SPEEDUP_TIME = 3600
    let SPEEDUP_COST = 5
    let PROSPECTOR_TAX = 500

    const WASTE_ID = 7;
    const ASTRO_CREDITS = 8;
    const LEGENDARY_BIOMOD = 14;
    const RARE_BIOMOD = 13;
    const UNCOMMON_BIOMOD = 12;
    const COMMON_BIOMOD = 11;
    const OUTLIER_BIOMOD = 10;
    const BLUEPRINT = 9;
    const NO_MINT = 0;

    let prospectingFactory = await hre.ethers.getContractFactory("Prospecting");
    let prospecting = await hre.upgrades.deployProxy(prospectingFactory, [
        testnetFeeWalletAddress,
        PROSPECTOR_FEE,
        PROSPECTOR_TIME,
        MAX_ORDERS,
        SPEEDUP_TIME,
        SPEEDUP_COST,
        PROSPECTOR_TAX,
    ]);

    await prospecting.deployed();
    console.log("Contract deployed. prospecting" , prospecting.address);

    //prospecting set up
    await prospecting.setOracle(testnetVRFCoordinatorAddress);
    // ask vrfmanager owner to call setNumWords(prospecting.address, 1)

    //Assetmanager set up
    //await nft.setTrustedContract(prospecting.address);
    //await nft.setTrustedContract(deployer.address);
    /*const alchemy = new hre.ethers.providers.AlchemyProvider('maticmum', process.env.ALCHEMY_API_KEY);
    const userWallet = new hre.ethers.Wallet(process.env.PRIVATE_KEY, alchemy);
    const mockAsstetManagerContract = new hre.ethers.Contract(testnetAssetManagerAddress, erc1155MockAssteManagerAbi, userWallet);
    //await mockAsstetManagerContract.connect(userWallet).setTrusted(testnetFeeWalletAddress, true);
    await mockAsstetManagerContract.connect(userWallet).setTrusted(prospecting.address, true);*/


    await prospecting.setIAssetManager(testnetAssetManagerAddress);
    await prospecting.setBiomodWeights(
        [
            LEGENDARY_BIOMOD,
            RARE_BIOMOD,
            UNCOMMON_BIOMOD,
            COMMON_BIOMOD,
            OUTLIER_BIOMOD,
            BLUEPRINT,
            NO_MINT,
        ],
        [2, 5, 10, 15, 20, 1, 30],
    );
    console.log("prospecting setup done")
}

async function deployWasteTrader(){
    const numerator = 1
    const denominator = 1
    const gwsTime = 21600
    const speedupTime = 3600
    const gwsMinimumOrder = 25
    const gwsTax = 500
    const gwsMax = 100
    const gwsSpeedupCost = 5

    const wtArgs = [
        testnetAssetManagerAddress,
        testnetFeeWalletAddress,
        numerator,
        denominator,
        gwsTime,
        speedupTime,
        gwsMinimumOrder,
        gwsTax,
        gwsMax,
        gwsSpeedupCost,
    ];

    const wasteTraderContract = await hre.ethers.getContractFactory('WasteTrader');
    const wasteTrader = await hre.upgrades.deployProxy(wasteTraderContract, wtArgs);
    await wasteTrader.deployed();
    console.log("wasteTrader deployed: ", wasteTrader.address)

   /* const alchemy = new hre.ethers.providers.AlchemyProvider('maticmum', process.env.ALCHEMY_API_KEY);
    const userWallet = new hre.ethers.Wallet(process.env.PRIVATE_KEY, alchemy);
    const mockAsstetManagerContract = new hre.ethers.Contract(testnetAssetManagerAddress, erc1155MockAssteManagerAbi, userWallet);

    await mockAsstetManagerContract.setTrustedContract(wasteTrader.address);*/
    console.log("wastetrader setup done")
}

async function deployEmilCoin(){
    let erc20Factory = await hre.ethers.getContractFactory('ERC20Mock');
    let emilCoin = await erc20Factory.deploy('EmilCoin', 'EC', "0xbC3244a304698e7a15817737CF2E603C2AF8eEed", 500);
    await emilCoin.deployed()
    console.log("contract EmilCoin deployed", emilCoin.address)
}

async function upgradeContract(){
    const upgradeContracrAddress = "0x6202595744051Ed42fD9A7a98b2729A961d2d4C8"
    const contract = await hre.ethers.getContractFactory('Prospecting');
    console.log('Upgrading contract: ',upgradeContracrAddress);
    await hre.upgrades.upgradeProxy(upgradeContracrAddress, contract);
    console.log('Contract upgraded');
}

async function verifyContract(){
    await hre.run("verify:verify", {
        address: "0x12168dD03349fa6B3dae6ac8c552b6C86810Bf55",
        constructorArguments: [
            "EmilCoin",
            "EC",
            "0xbC3244a304698e7a15817737CF2E603C2AF8eEed",
            500,
        ],
    });
}



async function main(){
    //await deployFacilityStore()
    //await deployProspecting()
    //await deployWasteTrader()
    //await verifyContract()
    await upgradeContract()
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });