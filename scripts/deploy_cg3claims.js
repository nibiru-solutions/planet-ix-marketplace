const { ethers, upgrades } = require('hardhat');

// constructor params - fill these in before deploying
const cargoDrop3NFT = '0x3376c61c450359d402f07909bda979a4c0e6c32f';
const cg3TokenId = 38;

async function main() {
  const ClaimsFactory = await ethers.getContractFactory('ClaimVerifier');
  const claims = await upgrades.deployProxy(ClaimsFactory, [cargoDrop3NFT, cg3TokenId], { gasPrice: 140000000000, gasLimit: 1000000 });
  await claims.deployed();

  console.log('Deployed at', claims.address);

  await claims.setNotary('0x890264F2553855154D0EB505CC1D1166F5FdBaA1',{ gasPrice: 140000000000, gasLimit: 1000000 });
  /*
  const nft = await ethers.getContractAt(
    ['function setTrusted(address _trusted, bool _isTrusted) external'],
    cargoDrop3NFT,
  );
  await nft.setTrusted(claims.address, true);

  console.log('Set trusted');
  */
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
