const { ethers, upgrades } = require('hardhat');

async function main() {
  const PIXVauchersFactory = await ethers.getContractFactory('PIXVouchers');
  const contract = await upgrades.deployProxy(PIXVauchersFactory, ["PIX Vouchers", "PIX-V"]);
  await contract.deployed();
  //const contract = PIXVauchersFactory.attach('0xBa60120b7C5c4159FF259D5f75Ae76a84B13256e');
  console.log('Deployed at', contract.address);

  await contract.setURI(1, "ipfs://QmSjeM31SLL6Upmf62FLfVnimdDSX3PnC5PVu9Lrwbwyjk");
  await contract.setURI(2, "ipfs://QmcidYRgccLPNXUEbDyJdmxkbuyUSkxhfDkTmxbg3T9AhE");
  await contract.setURI(3, "ipfs://QmQTic4rk2K19DqArYR1jD6TfH9YegSbQ3URSixP6PBA5A");
  await contract.setURI(4, "ipfs://QmYXtpcAUfQEkAXp36kmDVjcrAvNSMfpbXfAZ3zN5dUk34");
  console.log('URIs set');
  //await contract.trustedBatchMint("0x30f467143d2D36913eFA962612C2a704e1b8f3Fb", [1,2,3,4], [1,1,1,1])


  const FlunecrSaleFactory = await ethers.getContractFactory('FluencrSale');
  const fluncrSale = await upgrades.deployProxy(FlunecrSaleFactory, ["0x64dCD1dA866B3aB3EaB419b99d785811D40a9026"]);
  await fluncrSale.deployed();
  //const fluncrSale = FlunecrSaleFactory.attach('0x1c6FFbB778427724307A585352a09d118906Ccc2');
  await contract.setTrustedContract(fluncrSale.address);
  await fluncrSale.toggleStatus();
  console.log('Deployed at', fluncrSale.address);

  await fluncrSale.createProduct("ipfs://QmSjeM31SLL6Upmf62FLfVnimdDSX3PnC5PVu9Lrwbwyjk", 19, contract.address, 1, 1);
  await fluncrSale.createProduct("ipfs://QmcidYRgccLPNXUEbDyJdmxkbuyUSkxhfDkTmxbg3T9AhE", 79, contract.address, 2, 1);
  await fluncrSale.createProduct("ipfs://QmQTic4rk2K19DqArYR1jD6TfH9YegSbQ3URSixP6PBA5A", 199, contract.address, 3, 1);
  await fluncrSale.createProduct("ipfs://QmYXtpcAUfQEkAXp36kmDVjcrAvNSMfpbXfAZ3zN5dUk34", 399, contract.address, 4, 1);
  console.log('Products created');

  await fluncrSale.setAlternativeLanguageUris(1,["zh"],["ipfs://Qmb8JnLWgtB6HCcfKqVTggs2VXZ7C26wBaRocSpHJuCCxw"]);
  await fluncrSale.setAlternativeLanguageUris(2,["zh"],["ipfs://Qmeh8XfLaxfncuHtGHBuMgGhCk7rbrGgtWAGrDgrVFU8rg"]);
  await fluncrSale.setAlternativeLanguageUris(3,["zh"],["ipfs://QmYS29AuBz4RTq2ZcEDsj5jG5M8B1pEkTdMBAXnNzrgX5f"]);
  await fluncrSale.setAlternativeLanguageUris(4,["zh"],["ipfs://QmUMtbG4iiRjGVLo8hDdsYXWRWnWqnnkJpMmopMbwWsZDq"]);
  console.log('Chenese Language set');

  await fluncrSale.setSaleLimits(1, 10000);
  await fluncrSale.setSaleLimits(2, 10000);
  await fluncrSale.setSaleLimits(3, 10000);
  await fluncrSale.setSaleLimits(4, 10000);
  console.log('Sale limits set');
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
