const { ethers, upgrades } = require('hardhat');

const COORDINATOR = '0xAE975071Be8F8eE67addBC1A82488F1C24858067';
const KEYHASH = '0xd729dc84e21ae57ffb6be0053bf2b0668aa2aaf300a2a7b2ddf7dc0bb6e875a8';
const SUBSCRIPTION_ID = '358';

const SUBPERFLUID_HOST = '0x3E14dC1b13c488a8d5D310918780c983bD5982E7';
const AGOLD = '0xFAc83774854237b6E31c4B051b91015e403956d3';

const TICKET_PRICE = ethers.utils.parseEther('10');
const GEO_LOTTERY_MAX_ENTRIES = 20;

const GEO_REWARD_BASE_URI = 'https://api.planetix.app/api/v1/geo/lottery/token/';

async function main() {
  const GeoLotteryFactory = await ethers.getContractFactory('LuckyCatGeoLottery');
  const geoLottery = await upgrades.deployProxy(GeoLotteryFactory, [COORDINATOR, KEYHASH, SUBSCRIPTION_ID]);
  console.log('Geo Lottery Deployed To: ', geoLottery.address);

  const GeoLotteryStreamFactory = await ethers.getContractFactory('GeoLotteryStream');
  const geoLotteryStream = await upgrades.deployProxy(GeoLotteryStreamFactory, [SUBPERFLUID_HOST, AGOLD]);
  console.log('Geo Lottery SuperApp Deployed To: ', geoLotteryStream.address);

  const GeoRewardFactory = await ethers.getContractFactory('LuckyCatGeoReward');
  const geoReward = await upgrades.deployProxy(GeoRewardFactory, []);
  console.log('Geo Lottery Reward Deployed To: ', geoReward.address);

  // GEO LOTTERY SETUP
  await geoLottery.setAGOLD(AGOLD);
  await geoLottery.setLuckyCatGeoReward(geoReward.address);
  await geoLottery.setTicketPrice(TICKET_PRICE);
  await geoLottery.setMaxEntries(GEO_LOTTERY_MAX_ENTRIES);
  await geoLottery.setGeoStream(geoLotteryStream.address);

  // GEO STREAM SETUP
  await geoLotteryStream.setGeoLottery(geoLottery.address);

  // GEO REWARD SETUP
  await geoReward.setAGOLD(AGOLD);
  await geoReward.setGeoLottery(geoLottery.address);
  await geoReward.setBaseURI(GEO_REWARD_BASE_URI);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });