const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('LuckyCatGeoReward');
  const contract = ContractFactory.attach('0x4a21Be7F3b7c845400ea2eE27A15b7C843d7f3e6');

  const toAddress = '0x30f467143d2D36913eFA962612C2a704e1b8f3Fb';
  const mintAmount = 11574074074074;

  try {
    // Estimate gas for the transaction
    const gasEstimate = await contract.estimateGas.adminMint(toAddress, mintAmount);
    console.log(`Gas estimate: ${gasEstimate.toString()}`);

    // Simulate the transaction
    const simulationResult = await contract.callStatic.adminMint(toAddress, mintAmount);
    console.log(`Simulation result: ${simulationResult}`);

    // If the above lines don't throw an error, then it's likely safe to send the transaction
    const tx = await contract.adminMint(toAddress, mintAmount);
    const receipt = await tx.wait();

    console.log(`Transaction confirmed in block: ${receipt.blockNumber}`);
    console.log(`Transaction hash: ${receipt.transactionHash}`);
  } catch (error) {
    console.error(`An error occurred: ${error}`);
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
