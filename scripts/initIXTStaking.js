const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');
const delay = ms => new Promise(res => setTimeout(res, ms));

async function main() {

  const PIXT = await hre.ethers.getContractFactory('PIXT');
  const pixt = await PIXT.attach('0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE');

  const TokenStaking = await hre.ethers.getContractFactory('TokenStaking');
  const contractMonth1 = await TokenStaking.attach('0x7D0495D8a918Fb9aA02feA8D23c970d5933bD793');
  const contractMonth3 = await TokenStaking.attach('0x08971219534e1F8B6e4Afb2bf4A5Cf3929A141b1');
  const contractMonth6 = await TokenStaking.attach('0xcB985163ca943FE6382a4165a762b38809Ea7Ff8');
  const contractMonth12 = await TokenStaking.attach('0x87277D6676cD567DFB5536E30146Cd54815f0C53');
  const contractMonthLp1 = await TokenStaking.attach('0xc4c3956732A5d8EdA6C0163C174e413cB1CaD1dc');
  const contractMonthLp2 = await TokenStaking.attach('0x0B8738735c72BE95Fe5f5636B1807CAB8C9DddD8');


  let month1 = web3.utils.toWei("44400");
  let month3 = web3.utils.toWei("66600");
  let month6 = web3.utils.toWei("111100");
  let month12 = web3.utils.toWei("177600");
  let oneMonthLp1 = web3.utils.toWei("45000");
  let oneMonthLp2 = web3.utils.toWei("45000");


  console.log("Setting approval..")
  /*await pixt.approve(contractMonth1.address, month1,{ gasPrice: 180000000000, gasLimit: 9000000 });
  await pixt.approve(contractMonth3.address, month3,{ gasPrice: 180000000000, gasLimit: 9000000 });
  await pixt.approve(contractMonth6.address, month6,{ gasPrice: 180000000000, gasLimit: 9000000 });
  await pixt.approve(contractMonth12.address, month12,{ gasPrice: 180000000000, gasLimit: 9000000 });*/
  await pixt.approve(contractMonthLp1.address, oneMonthLp1,{ gasPrice: 180000000000, gasLimit: 9000000 });
  await pixt.approve(contractMonthLp2.address, oneMonthLp2,{ gasPrice: 180000000000, gasLimit: 9000000 });
  console.log("Approval set.");


  console.log("Staking..")
  await contractMonth1.notifyRewardAmount(month1,{ gasPrice: 180000000000, gasLimit: 9000000 },);
  await contractMonth3.notifyRewardAmount(month3,{ gasPrice: 180000000000, gasLimit: 9000000 },);
  await contractMonth6.notifyRewardAmount(month6,{ gasPrice: 180000000000, gasLimit: 9000000 },);
  await contractMonth12.notifyRewardAmount(month12,{ gasPrice: 180000000000, gasLimit: 9000000 },);
  await contractMonthLp1.notifyRewardAmount(oneMonthLp1,{ gasPrice: 180000000000, gasLimit: 9000000 },);
  await contractMonthLp2.notifyRewardAmount(oneMonthLp2,{ gasPrice: 180000000000, gasLimit: 9000000 },);


  console.log("Staked..");

}


main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
