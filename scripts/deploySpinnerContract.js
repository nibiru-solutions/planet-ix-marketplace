const { ethers, upgrades } = require('hardhat');
const hre = require("hardhat");

// Init params - fill these in before running the script
// POLYGON MAINNET PARAMS

const fluencrSaleAddress = "0x1c6FFbB778427724307A585352a09d118906Ccc2"
const swapManagerAddress = "0x14804EA255F018542d66cf5311873F4Ed124D620"
const spinnerAddress = "0xc5c0fB8aA03c4F33b98BeABd90ed1E99b2470442"
const flunecrTreasuryWallet = "0xfA858E30Dcb15e07612F64d48dB2559B8efA6378"

const ixtAddress = '0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE';
const usdtAddress = '0x12168dD03349fa6B3dae6ac8c552b6C86810Bf55';
const usdcAddress = "0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174"
const daiAddress = "0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063"
const router = "0x1b02dA8Cb0d097eB8D57A175b88c7D8b47997506"

//TESTNETS ADDRESSES
const testnetEmilCoin = "0x12168dD03349fa6B3dae6ac8c552b6C86810Bf55"


async function main() {

    console.log("start spinner deploy script")
    let spinnerFactory = await ethers.getContractFactory('SpinnerInternalSwapTreasury');
    let spinner = await upgrades.deployProxy(spinnerFactory, [ixtAddress]);
    console.log("spinner deployed to", spinner.address);

    let mockFluencrSaleFactory = await ethers.getContractFactory('MockFluencrSale');
    let mockFluencrSale = await upgrades.deployProxy(mockFluencrSaleFactory, [spinner.address]);//[spinner.address]);
    console.log("mock fluencr sale deployed", mockFluencrSale.address)


    await spinner.setSwapManager(swapManagerAddress);
    await spinner.setFluencr(mockFluencrSale.address); // we will have the mock temporarily for testing.
    await spinner.setAdmin(flunecrTreasuryWallet, true);

    console.log('verifying mockFluencrSale');
    await hre.run('verify:verify', {
        address: mockFluencrSale.address,
        constructorArguments: [spinner.address],
    });
    console.log('mockFluencrSale verified');
}

async function changeAdmin() {

    console.log("start spinner deploy script")
    let spinnerFactory = await ethers.getContractFactory('SpinnerInternalSwapTreasury');
    const spinner = await spinnerFactory.attach('0xc5c0fB8aA03c4F33b98BeABd90ed1E99b2470442');
    //await spinner.setAdmin("0x27bd44a6ee5a03E5c04551463A712e1389e59F94", true)
    //console.log("admin changed")
    console.log(await spinner.admins("0x27bd44a6ee5a03E5c04551463A712e1389e59F94"))
}

async function withdrawFunds() {
    const spinnerFactory = await ethers.getContractFactory('SpinnerInternalSwapTreasury');
    const spinner = spinnerFactory.attach('0xc5c0fB8aA03c4F33b98BeABd90ed1E99b2470442');

    const usdt = "0xc2132D05D31c914a87C6611C10748AEb04B58e8F"

    console.log("start withdrawal")
    await spinner.withdraw(usdt, 1100000000, { gasPrice: 250000000000, gasLimit: 10000000 })
    console.log("complete")
}


async function upgradeAndSetupFluencrSale() {
    console.log('Upgrading contract: ', fluencrSaleAddress);
    const fluncrSaleFactory = await hre.ethers.getContractFactory('FluencrSale');
    const fluencrSale = await hre.upgrades.upgradeProxy(fluencrSaleAddress, fluncrSaleFactory);
    console.log('fluencrSale upgraded');

    await fluencrSale.setSpinnerTreasury(spinnerAddress)


    //todo 5 = INVEST
    //todo 6 = GAMING

    /*
    //todo createProduct first for investor pack, then for gaming pack
    await fluencrSale.createProduct()
    await fluencrSale.createProduct()*/
    await fluencrSale.setIdToSwap(5, true)
    await fluencrSale.setIdToSwap(6, true)
    await fluencrSale.setIdToPriceCutBps(5, 8000)
    await fluencrSale.setIdToPriceCutBps(6, 2000)
    // fluencrSale.setSaleLimits(5, unlimited)
    // fluencrSale.setSaleLimits(6, unlimited)
    //todo tell fluencr boys to change payment address on to spinner.
}


withdrawFunds()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
