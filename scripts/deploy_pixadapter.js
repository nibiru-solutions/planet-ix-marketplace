const { ethers, upgrades } = require('hardhat');
const { AssetIds, PIXCategory, PIXSize } = require('../test/utils');

async function main() {
  let rarityFactory = await ethers.getContractFactory('RarityProviderMock');
  rarityProvider = await rarityFactory.deploy();
  console.log('Rarity Provider deployed to:', rarityProvider.address);

  let stakeableFactory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  stakeable = await upgrades.deployProxy(stakeableFactory);
  console.log('StakeableAdapter deployed to:', stakeable.address);

  // Rarity providers provides a mock address for PIX in these tests
  await stakeable.setRarityProvider(rarityProvider.address, rarityProvider.address);
  await stakeable.setMissionControl('0x421f672626c253462521AD7616Df4622bbC51523');
  await stakeable.setERC1155('0xbF2c9163B6f1db374847a7b52bb459dfbD83AaB9');

  // PIX categories are used in lieu of actual tokens ids in these tests
  let qualityKeys = Object.keys(PIXCategory).filter((v) => !isNaN(Number(v)));
  qualityKeys.forEach(async (value) => {
    await rarityProvider.setRarity(value, value);
  });
  //   await stakeable.setFacilityStakeRequirements(
  //     AssetIds.FacilityOutlier,
  //     PIXCategory.Outliers,
  //     true,
  //   );xxxx
  //   await stakeable.setFacilityStakeRequirements(
  //     AssetIds.FacilityLegendary,
  //     PIXCategory.Legendary,
  //     true,
  //   );
  //   let hour = 60 * 60;
  //   await stakeable.setFacilityStats(
  //     [8 * hour, 12 * hour, 16 * hour, 20 * hour, 24 * hour],
  //     [1, 3, 4, 5, 6],
  //   );

  //   await stakeable.setTokenInfo(
  //     AssetIds.GenesisDrone,
  //     true,
  //     false,
  //     false,
  //     Class.Linear,
  //     droneEv,
  //     AssetIds.Waste,
  //     droneCap,
  //   );
  //   await stakeable.setTokenInfo(
  //     AssetIds.FacilityOutlier,
  //     true,
  //     false,
  //     false,
  //     Class.Facility,
  //     facilityEv,
  //     AssetIds.Energy,
  //     facilityCap,
  //   );
  //   await stakeable.setTokenInfo(
  //     AssetIds.FacilityLegendary,
  //     true,
  //     false,
  //     false,
  //     Class.Facility,
  //     facilityEv,
  //     AssetIds.Energy,
  //     facilityCap,
  //   );

  let count = 1;
  for (let i = -2; i <= 2; i++) {
    for (let j = -2; j <= 2; j++) {
      for (let k = -2; k <= 2; k++) {
        let sum = i + j + k;
        let rad = Math.abs(i) + Math.abs(j) + Math.abs(k) / 2;
        if (rad <= 1 && rad > 0 && sum == 0) {
          console.log(`Now setting token Info for: ${i}, ${j}, ${k}`);
          await stakeable.setTokenInfo(
            AssetIds.GenesisDrone,
            true,
            true,
            true,
            1, //CLASS.LINEAR
            14,
            AssetIds.Energy,
            28,
            i,
            j,
          );
        }
      }
    }
  }
  // This is purely for testing
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
