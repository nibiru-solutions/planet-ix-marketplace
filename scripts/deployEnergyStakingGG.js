const { ethers } = require('hardhat');
const { constants } = require('ethers');

const assets = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const gravityGradePacks = '0x3376C61c450359d402F07909Bda979a4c0e6c32F';
const ixt = '0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE';

const pushRewardNextEpochPercents = [
  2500, 2000, 1500, 1000, 500, 500, 500, 500, 400, 200, 200, 200,
];

const ameliaWallet = '0x20f5a599760907cF39e4Aee699DBDF564dE98c6D';

async function main() {
  const energyStakingGGFactory = await ethers.getContractFactory('EnergyIXTStakingGG');
  energyStakingGG = await upgrades.deployProxy(energyStakingGGFactory, [
    gravityGradePacks,
    ixt,
    assets,
  ]);

  console.log(`Energy Staking GG Pool deployed to: ${energyStakingGG.address}`);
  let month = 2_592_000;

  const rewardTokenId = 15;
  const rewardTokenAmount = 1;
  const energyCost = 20;
  const ixtCost = 20;
  const lockupTime = month;
  const concurrentStakerCap = 9999999;
  const lifeTimePlayerCap = 9999999;
  const paused = false;

  const gravityGradeRewards = {
    rewardTokenId: rewardTokenId,
    rewardTokenAmount: rewardTokenAmount,
    energyCost: energyCost,
    ixtCost: ixtCost,
    lockupTime: lockupTime,
    concurrentStakerCap: concurrentStakerCap,
    lifeTimePlayerCap: lifeTimePlayerCap,
    paused: paused,
  };

  const tx = await energyStakingGG.addReward(gravityGradeRewards);

  const rewardTokenIdMedium = 16;
  const energyCostMedium = 20;
  const ixtCostMedium = 20;


  const gravityGradeRewardsMedium = {
    rewardTokenId: rewardTokenIdMedium,
    rewardTokenAmount: rewardTokenAmount,
    energyCost: energyCostMedium,
    ixtCost: ixtCostMedium,
    lockupTime: lockupTime,
    concurrentStakerCap: concurrentStakerCap,
    lifeTimePlayerCap: lifeTimePlayerCap,
    paused: paused,
  };

  const tx = await energyStakingGG.addReward(gravityGradeRewardsMedium);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
