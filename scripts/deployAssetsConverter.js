const { ethers, upgrades} = require('hardhat');
const { constants } = require('ethers');
const hre = require("hardhat");

const assetManager = "0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc"
const WASTE_ID = 7;
const ASTRO_CREDITS = 8;
const LEGENDARY_BIOMOD = 14;
const RARE_BIOMOD = 13;
const UNCOMMON_BIOMOD = 12;
const COMMON_BIOMOD = 11;
const OUTLIER_BIOMOD = 10;
const fromTokenIds = [WASTE_ID, WASTE_ID, WASTE_ID, WASTE_ID, WASTE_ID]
const toTokenIds = [OUTLIER_BIOMOD, COMMON_BIOMOD, UNCOMMON_BIOMOD, RARE_BIOMOD, LEGENDARY_BIOMOD]
const prices = [150, 200, 300, 500, 1337]


async function main() {
    //let assetManagerFactory = await ethers.getContractFactory('NFT');

    let assetsConverterFactory = await ethers.getContractFactory('AssetsConverter');
    let assetsConverter = await upgrades.deployProxy(assetsConverterFactory, []);
    await assetsConverter.setAssetManager(assetManager)
    await assetsConverter.deployed();
    console.log("contract AssetsConverter deployed", assetsConverter.address);


    for (let i = 0; i < fromTokenIds.length; i++) {
        await assetsConverter.createAssetConversionRecipe([fromTokenIds[i]], [toTokenIds[i]], [prices[i]], 0, 0)
    }

    const assetManagerContract = await ethers.getContractAt(
        ['function setTrusted(address, bool) external'],
        assetManager,
    );
    await assetManagerContract.setTrusted(assetsConverter.address, true);

    console.log("Set up done")

    await hre.run("verify:verify", {
        address: assetsConverter.address
    });
    console.log("Done")
}

async function upgradeAssetsConverter() {
    const assetsConverterFactory = await ethers.getContractFactory('AssetsConverter');
    let assetsConverter = await upgrades.upgradeProxy(
        '0x9424dE952994112C4cAa83eA587A7C239896A654',
        assetsConverterFactory,
    );

    console.log(`assconvert upgraded: ${assetsConverter.address}`);
    await hre.run("verify:verify", {
        address: "0x9424dE952994112C4cAa83eA587A7C239896A654"
    });
    console.log("Done")
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });