const ethers = require('ethers');
const { MerkleTree } = require('merkletreejs');
const keccak256 = require('keccak256');

const generateMerkleProofForuser = (userAddress, lotteryId, pool) => {
  // Generate a list of random Ethereum addresses for testing purposes
  const addresses = Array.from({ length: 20 }, () => ethers.Wallet.createRandom().address);

  // Add user's address to the list so she can claim
  addresses.push(userAddress);

  // ABI-encode each address along with the lottery ID and pool ID, then hash the result to get the Merkle leaves
  const leaves = addresses.map((address) => {
    const encodedData = ethers.utils.defaultAbiCoder.encode(['address', 'uint256', 'uint256'], [address, lotteryId, pool]);
    return keccak256(Buffer.from(encodedData.slice(2), 'hex')).toString('hex'); // Removing the '0x' prefix before hashing
  });

  // Construct the Merkle Tree using the generated leaves
  const tree = new MerkleTree(leaves, keccak256, { sortPairs: true });

  // Extract the Merkle Root from the tree
  const root = tree.getHexRoot();

  // Generate a Merkle proof for user
  const userEncodedData = ethers.utils.defaultAbiCoder.encode(['address', 'uint256', 'uint256'], [userAddress, lotteryId, pool]);
  const userLeaf = keccak256(Buffer.from(userEncodedData.slice(2), 'hex')).toString('hex');
  const userMerkleProof = tree.getHexProof(userLeaf);

  console.log(`Merkle Root: ${root}`);
  console.log(`Proof: ${userMerkleProof}`);
};

// Example usage (dummy values for demonstration)
const userAddress = '0x30f467143d2D36913eFA962612C2a704e1b8f3Fb';
const lotteryId = 11;
const pool = 6;

generateMerkleProofForuser(userAddress, lotteryId, pool);
