const { ethers, upgrades } = require('hardhat');

const connext = '0x8898B472C54c31894e3B9bb83cEA802a5d0e63C6';
const target = '0xe210c6E2A9114E9f276eeD8a428b35308090Ab1f';
const destination_domain = 1886350457;
const mc_staking = '0x6C84fE21309D94Cc8B7ccFe8F53A47A5b3111587';

const eth_assets = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const ygensisId = 2;
const missionContrlStaking = '0x6C84fE21309D94Cc8B7ccFe8F53A47A5b3111587';

async function main() {
  const YGLockFactory = await ethers.getContractFactory('YGenesisLockProvider');
  const ygLock = await upgrades.deployProxy(YGLockFactory, []);
  await ygLock.deployed();

  await ygLock.setConnext(connext);
  await ygLock.setDestinationDomain(destination_domain);
  await ygLock.setTarget(target);
  await ygLock.setMCStaking(mc_staking);

  console.log('YGenesisLockProvider Deployed at', ygLock.address);

  const MissionControlStakingFactory = await ethers.getContractFactory('MissionControlStaking');
  const mcStaking = await MissionControlStakingFactory.attach(missionContrlStaking);

  await mcStaking.whitelistTokens(eth_assets, [ygensisId], true);
  await mcStaking.setUnstakeProvider(eth_assets, [ygensisId], ygLock.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
