const { ethers, upgrades } = require('hardhat');
const { constants } = require('ethers');

async function main() {
  const nftFactory = await ethers.getContractFactory('ERC1155MockAssetManager');

  const nft = await nftFactory.deploy('TOY URI');
  console.log(`energy staking deployed to ${nft.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
