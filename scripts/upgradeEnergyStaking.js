const { ethers } = require('hardhat');
const { constants } = require('ethers');

const assets = '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc';
const gravityGradePacks = '0xF7b0892b05bd7Ae8d55c672f9c2406B651a98157';
const mockToken = '0x86E6aeD8Fef97C57c183CE7f04C1649A0eE4db20';

const pushRewardNextEpochPercents = [
  2500, 2000, 1500, 1000, 500, 500, 500, 500, 400, 200, 200, 200,
];

const ameliaWallet = '0x7d922bf0975424b3371074f54cC784AF738Dac0DX';
const rewardWallet = '0x7d922bf0975424b3371074f54cC784AF738Dac0DX';

let rewardAmount = ethers.utils.parseEther('1000');
let month = 2_592_000;

async function main() {
  const energyStakingFactory = await ethers.getContractFactory('EnergyIXTStaking');
  const energyStaking = await upgrades.upgradeProxy(
    '0x07906AD2c52Ce04b6b184558B919f20E274987eE',
    energyStakingFactory,
  );

  console.log(`energy staking upgraded: ${energyStaking.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
