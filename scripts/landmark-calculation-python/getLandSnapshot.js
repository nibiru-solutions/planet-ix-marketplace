const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');

async function main() {
  
  const PIXLandmark = await hre.ethers.getContractFactory('PIXLandStaking');
  const pixLandmark = await PIXLandmark.attach('0xB030dA155b6F3e67921157Ec7793EDF3e407a55B');


  for (let i = 0; i < 999; i++) {
    // break out of here if data is complete, batch is full
    console.log(i.toString()+','+ (await pixLandmark.getTokensStakedPerPool(i)).toString());
  }
}


main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
