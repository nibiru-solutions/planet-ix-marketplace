## Landmark staking
cd planet-ix-marketplace
npx hardhat run scripts/landmark-calculation-python/getLandSnapshot.js --network polygon >> scripts/landmark-calculation-python/2024/snapshotMarch.csv
Update rows 5 and 13 in getRewardsPerPool.py and run
python3 scripts/landmark-calculation-python/2024/getRewardsPerPool.py  >> scripts/march-rewards.csv
Update row 11 initLandRewards.js
npx hardhat run scripts/initLandRewards.js


## Metashare staking
Update rows 10-17
npx hardhat run scripts/initMetashareStaking.js --network polygon 


## Territory staking
Update row 20
npx hardhat run scripts/initTerritoryStaking.js --network polygon 

## IXT Staking
npx hardhat run scripts/initIXTStaking.js --network polygon 

## MILK Staking
Make sure you approved token spending(some high amount) in advance to save on gas
npx hardhat run scripts/initMilkStaking.js --network mainnet 
