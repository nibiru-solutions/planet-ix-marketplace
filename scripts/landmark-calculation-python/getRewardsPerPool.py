import json
import csv

d={}
with open("./scripts/landmark-calculation-python/2024/snapshotMarch.csv") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
        a1 = int(row[0].replace('"',''))
        a2 = int(row[1].replace('"',''))
        d[a1]=a2

# Opening JSON file
f = open('./scripts/landmark-calculation-python/2024/landmark_treasury-15_1_2024-15_2_2024-full.json')

# returns JSON object as
# a dictionary
data = json.load(f)

# Iterating through the json
# list
s=0
for i in data['landmarkTreasuries']:
    #print(i['token_id'],i['treasury'])
    s += i['final_treasury']

su2=0
#print(s)

for i in data['landmarkTreasuries']:
    a3=False
    if int(i['token_id']) >527:
        continue
    try:
        final_treasury = round(float(i['final_treasury']),9)
        if d[int(i['token_id'])]>0 and final_treasury != 0:
            a3=True
            su2 += final_treasury
            print(i['token_id']+',"'+str(final_treasury)+'"')
    except:
        continue
    s += i['final_treasury']
#print(su2)
# Closing file
f.close()
