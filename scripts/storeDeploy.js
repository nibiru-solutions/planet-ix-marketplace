const { ethers, upgrades } = require('hardhat');

async function main() {
  const storeFactory = await ethers.getContractFactory('ERC1155StoreGeneric');
  const store = await upgrades.deployProxy(storeFactory, []);

  console.log(`Store deployed to: ${store.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
