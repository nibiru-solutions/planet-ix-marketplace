const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');

async function main() {
  const Nft = await hre.ethers.getContractFactory('NFT');
  const nft = await Nft.attach('0xba6666B118f8303F990f3519DF07e160227cCE87');
  const csvFilePath = 'scripts/crypto-avatar-packs.csv';

  const data = await csv().fromFile(csvFilePath);

  let index = 0;
  let value = 0;

  while (index < data.length) {
    // reset arrays
    recipients = [];
    nfts = [];
    amounts = [];
    // fill up array with next 150 items
    value = 0;
    for (let i = 0; i < 100; i++) {
      // break out of here if data is complete, batch is full
      if (index == data.length) {
        break;
      }
      recipients.push(data[index]['wallet_address']);
      nfts.push(data[index]['tokenID']);
      amounts.push(data[index]['amount']);
      value += parseInt(data[index]['amount']);
      index++;
    }

    console.log("recipients", recipients)
    console.log("nfts", nfts)
    console.log("amounts", amounts)


    const response = await nft.airdrop(
      recipients,
      nfts,
      amounts,
      { gasPrice: 140000000000, gasLimit: 1000000 },
    );

    const receipt = await response.wait(1);
    console.log(receipt);
    console.log('Airdropped' ,'============================', index);
  }
}

main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
