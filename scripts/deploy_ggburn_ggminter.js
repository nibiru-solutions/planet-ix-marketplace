const { ethers, upgrades, run } = require('hardhat');
const hre = require('hardhat');

//mainnet
const assetManager = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const gravityGradeAddress = '0x3376C61c450359d402F07909Bda979a4c0e6c32F';
const vrfManagerAddress = '0x34F9a1963029EDB930d6841c236989Ce61002b39';
const PIXLandmarkAddress = '0x24CfF55d808fD10A1E730B86037760E57eCAF549';

//testnet
// const assetManager = '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc';
// const gravityGradeAddress = '0xF7b0892b05bd7Ae8d55c672f9c2406B651a98157';
// const vrfManagerAddress = '0xd58a467Da7cd1f1Fe9cf37F1e23B554115352840';
// const PIXLandmarkAddress = '0x62cAB957F0bec4d912b9AFADd8C50f86574E3D48';

let genericBurn;
let vrfManager;
let landmark;

const categories = [
  {
    token_id: 9,
    max_draws: 1,
    draws_amounts: 1,
    draws_weights: 10,
    content_addr: PIXLandmarkAddress,
    content_tokenId: 2186,
    content_amounts: 1,
    content_weights: 10,
  },
  {
    token_id: 9,
    max_draws: 1,
    draws_amounts: 1,
    draws_weights: 10,
    content_addr: assetManager,
    content_tokenId: 15,
    content_amounts: 1,
    content_weights: 10,
  },
  {
    token_id: 9,
    max_draws: 1,
    draws_amounts: 1,
    draws_weights: 10,
    content_addr: assetManager,
    content_tokenId: 7,
    content_amounts: 100,
    content_weights: 10,
  },
  {
    token_id: 10,
    max_draws: 1,
    draws_amounts: 1,
    draws_weights: 10,
    content_addr: assetManager,
    content_tokenId: 7,
    content_amounts: 50,
    content_weights: 10,
  },
  {
    token_id: 13,
    max_draws: 1,
    draws_amounts: 1,
    draws_weights: 10,
    content_addr: assetManager,
    content_tokenId: 16,
    content_amounts: 5,
    content_weights: 10,
  },
  {
    token_id: 14,
    max_draws: 1,
    draws_amounts: 1,
    draws_weights: 10,
    content_addr: assetManager,
    content_tokenId: 16,
    content_amounts: 20,
    content_weights: 10,
  },
  {
    token_id: 14,
    max_draws: 1,
    draws_amounts: 1,
    draws_weights: 10,
    content_addr: assetManager,
    content_tokenId: 17,
    content_amounts: 5,
    content_weights: 10,
  },
  {
    token_id: 14,
    max_draws: 1,
    draws_amounts: 1,
    draws_weights: 10,
    content_addr: assetManager,
    content_tokenId: 7,
    content_amounts: 25,
    content_weights: 10,
  },
];

async function deployGenericBurn() {
  console.log('Deploying Generic Burn...............');
  const genericBurnFactory = await ethers.getContractFactory('GravityGradeGenericBurn');
  genericBurn = await upgrades.deployProxy(genericBurnFactory, []);
  await genericBurn.deployed();
  console.log(`Generic Burn Deployed To: ${genericBurn.address}`);

  console.log('Setting Gravity Grade For Generic Burn...............');
  await genericBurn.setGravityGrade(gravityGradeAddress);
  console.log(`Gravity Grade Set: ${gravityGradeAddress}`);

  console.log('Setting VRF Manager...............');
  await genericBurn.setVRFOracle(vrfManagerAddress);
  console.log(`VRF Manager Set: ${vrfManagerAddress}`);
}

async function deployMintWrapper() {
  console.log('Deploying GG Mint Adapter...............');
  const ggMintFactory = await ethers.getContractFactory('GravityGradeMintWrapper');
  const ggMint = await ggMintFactory.deploy();
  await ggMint.deployed();
  console.log(`GG Mint Adapter Deployed To: ${ggMint.address}`);

  console.log('Setting Gravity Grade For Mint Adapter...............');
  await ggMint.setGravityGradeContract(gravityGradeAddress);
  console.log(`Gravity Grade Set: ${gravityGradeAddress}`);

  console.log('Setting Generic Burn to trusted for Mint Adapter.............');
  await ggMint.setTrusted(genericBurn.address, true);
  console.log('Generic Burn Now Trusted');
}

async function upgradeLandmark() {
  console.log('Upgrading Landmark...............');

  const landmarkFactory = await ethers.getContractFactory('PIXLandmark');
  landmark = await upgrades.upgradeProxy(PIXLandmarkAddress, landmarkFactory);
  console.log(`Landmark Upgraded: ${landmark.address}`);

  console.log('Setting Generic Burn to trusted for Landmark...............');
  await landmark.setTrusted(genericBurn.address, true);
  console.log('Generic Burn Now Trusted');
}

async function setupCategories() {
  console.log('Setting up categories...............');

  let categoryId;
  for (let i = 0; i < categories.length; i++) {
    await genericBurn.setMaxDraws(categories[i].token_id, categories[i].max_draws);

    let tx = await genericBurn.createContentCategory(categories[i].token_id);
    let receipt = await tx.wait();

    let filteredEvents = receipt.events.filter((e) => e.event === 'CategoryCreated');

    if (filteredEvents.length > 0) {
      categoryId = filteredEvents[0].args['_categoryId'];

      await genericBurn.setContentAmounts(
        categories[i].token_id,
        categoryId,
        [categories[i].draws_amounts],
        [categories[i].draws_weights],
      );

      await genericBurn.setContents(
        categories[i].token_id,
        categoryId,
        [categories[i].content_addr],
        [categories[i].content_tokenId],
        [categories[i].content_amounts],
        [categories[i].content_weights],
      );

      await genericBurn.whitelistToken(categories[i].token_id, true);

      console.log(`Category ${categoryId} set up for token Id ${categories[i].token_id}`);
    }
  }
}

async function main() {
  await deployGenericBurn();
  await deployMintWrapper();
  await upgradeLandmark();
  await setupCategories();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
