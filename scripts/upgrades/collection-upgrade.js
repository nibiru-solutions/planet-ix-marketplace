const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('NFT');
  const contract = await upgrades.upgradeProxy(
    '0xF616D9Ebc4C1f44EAA1CadB16019f989eE858476',
    ContractFactory,
  );
  await contract.deployed();

  console.log('Deployed at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
