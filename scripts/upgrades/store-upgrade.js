const { ethers, upgrades } = require('hardhat');

// testnet address
let erc1155genericStoreAddress = '0xbd54DcA0D7AFd2D076D1425b25049d4B6e2f48Ce';

async function main() {
  const ContractFactory = await ethers.getContractFactory('ERC1155StoreGeneric');

  // const txResponse = await upgrades.prepareUpgrade(
  //   erc1155genericStoreAddress,
  //   ContractFactory,
  // );
  // console.log(txResponse);

  // await upgrades.forceImport(erc1155genericStoreAddress, ContractFactory);
  
  const contract = await upgrades.upgradeProxy(
    erc1155genericStoreAddress,
    ContractFactory,
  );
  await contract.deployed();

  console.log('ERC1155StoreGeneric upgraded at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
