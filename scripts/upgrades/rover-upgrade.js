const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('Rover');
  const contract = await upgrades.upgradeProxy(
    '0x1f9df63E56b62eEBF676f75A0Acb10009C9D8288',
    ContractFactory,
  );
  await contract.deployed();

  console.log('Deployed at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
