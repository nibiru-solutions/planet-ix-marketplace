const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('EnergyIXTStaking');

  //   const txResponse = await upgrades.prepareUpgrade(
  //     '0x38be64051025BE6916647e431A38d9dcc7168c30',
  //     ContractFactory,
  //   );
  //   console.log(txResponse);

  //await upgrades.forceImport('0x38be64051025BE6916647e431A38d9dcc7168c30', ContractFactory);
  const contract = await upgrades.upgradeProxy(
    '0x13f1d0e58728c9710794143d9236b5C40f22423d',
    ContractFactory,
  );
  await contract.deployed();

  console.log('EnergyIXTStaking', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
