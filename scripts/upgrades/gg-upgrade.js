const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('GravityGrade');
  //await upgrades.forceImport('0x5E5B9c8bdD4510747745F9Ff546Dfa5101b38357', ContractFactory);
  const contract = await upgrades.upgradeProxy(
    '0x990fcaA5aF06924627f721176f44898BD28CBD07',
    ContractFactory,
  );
  await contract.deployed();

  console.log('GravityGrade upgraded at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
