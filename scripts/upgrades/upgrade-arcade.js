const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('ArcadeGiveaway');
  //await upgrades.forceImport('0x5E5B9c8bdD4510747745F9Ff546Dfa5101b38357', ContractFactory);
  const contract = await upgrades.upgradeProxy(
    '0x9fC851B1C193A2eD5f200C85Ff5082c80dd8d3d2',
    ContractFactory,
  );
  await contract.deployed();

  console.log('ArcadeGA upgraded at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
