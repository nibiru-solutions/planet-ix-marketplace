const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
  const contract = await upgrades.upgradeProxy(
    '0x496092fe78E8C9189282AEd26F1eE1294FDCe337',
    ContractFactory,
  );
  await contract.deployed();

  console.log('Deployed at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
