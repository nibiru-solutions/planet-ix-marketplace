const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('MilkStaking');

  const contract = await upgrades.upgradeProxy('0x70c65974104D99526d172a18ed17Ea3F1abf7d74', ContractFactory);
  await contract.deployed();

  console.log('Upgraded at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
