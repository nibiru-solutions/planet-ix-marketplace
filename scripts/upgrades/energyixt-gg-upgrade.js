const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('EnergyIXTStakingGG');

  //   const txResponse = await upgrades.prepareUpgrade(
  //     '0x38be64051025BE6916647e431A38d9dcc7168c30',
  //     ContractFactory,
  //   );
  //   console.log(txResponse);

  //await upgrades.forceImport('0x38be64051025BE6916647e431A38d9dcc7168c30', ContractFactory);
  const contract = await upgrades.upgradeProxy(
    '0x6f050e1623a003f470eB29eDecb2877a0b9d79f8',
    ContractFactory,
  );
  await contract.deployed();

  console.log('EnergyIXTStakingGG', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
