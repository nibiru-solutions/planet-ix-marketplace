const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('MissionControl');
  const contract = await upgrades.upgradeProxy(
    '0x29295020DaA9D2afBaA6A9bF80E08332Fe8e7a69',
    ContractFactory,
  );
  await contract.deployed();

  console.log('Deployed at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
