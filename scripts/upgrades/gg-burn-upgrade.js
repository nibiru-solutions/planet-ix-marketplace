const { ethers, upgrades } = require('hardhat');

async function main() {
  const ContractFactory = await ethers.getContractFactory('GravityGradeBurn_CargoDrop3');

  //   const txResponse = await upgrades.prepareUpgrade(
  //     '0x38be64051025BE6916647e431A38d9dcc7168c30',
  //     ContractFactory,
  //   );
  //   console.log(txResponse);

  //await upgrades.forceImport('0x38be64051025BE6916647e431A38d9dcc7168c30', ContractFactory);
  const contract = await upgrades.upgradeProxy(
    '0x38be64051025BE6916647e431A38d9dcc7168c30',
    ContractFactory,
  );
  await contract.deployed();

  console.log('GravityGradeBurn_CargoDrop3 upgraded at', contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
