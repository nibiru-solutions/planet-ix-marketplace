const hre = require('hardhat');

const csv = require('csvtojson');
const { web3 } = require('hardhat');

async function main() {
  const Airdrop = await hre.ethers.getContractFactory('Airdrop');
  const airdrop = await Airdrop.attach('0x8C7D0585685E6357ee999D02b383C0CaB73Cf247');
  const MILK = await hre.ethers.getContractFactory('Illumicati');
  const milk = await MILK.attach('0xf538296e7dd856af7044deec949489e2f25705bc');
  const csvFilePath = './scripts/airdrop/MILKWEEK1.csv';

  const data = await csv().fromFile(csvFilePath);

  let index = 0;
  let value = 0;

  while (index < data.length) {
    // reset arrays
    recipients = [];
    amounts = [];
    // fill up array with next 150 items
    value = 0;
    for (let i = 0; i < 100; i++) {
      // break out of here if data is complete, batch is full
      if (index == data.length) {
        break;
      }
      recipients.push(data[index]['wallet_address']);
      amounts.push(web3.utils.toWei(data[index]['milk'], 'ether'));
      value += parseInt(data[index]['milk']);
      index++;
    }

    console.log("recipients", recipients)
    console.log("amounts", amounts)

    console.log("recipients.length", recipients.length)
    console.log("amounts.length", amounts.length)



    console.log("value.toString()", value.toString())

    await milk.approve(
      '0x8C7D0585685E6357ee999D02b383C0CaB73Cf247',
      web3.utils.toWei(value.toString(), 'ether'),
    );

    const response = await airdrop.airdrop(
      '0xf538296e7dd856af7044deec949489e2f25705bc',
      recipients,
      amounts
    );

    const receipt = await response.wait(1);
    console.log(receipt);
    console.log('============================', index);
  }
}

main()
  .then(() => console.log('continue'))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
