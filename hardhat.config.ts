import '@nomiclabs/hardhat-etherscan';
import '@nomiclabs/hardhat-web3';
//import '@nomiclabs/hardhat-waffle';
import 'hardhat-deploy';
import '@typechain/hardhat';
import 'hardhat-gas-reporter';
import 'solidity-coverage';
import 'dotenv/config';
import 'hardhat-dependency-compiler';
import 'hardhat-contract-sizer';
import '@openzeppelin/hardhat-upgrades';
import 'hardhat-contract-sizer';
// import '@nomicfoundation/hardhat-chai-matchers';
// import '@nomiclabs/hardhat-waffle';

export default {
  networks: {
    hardhat: {
      allowUnlimitedContractSize: true,
      timeout: 1000000,
      initialBaseFeePerGas: 0,
      chainId: 31337,
      blockConfirmations: 1,
    },
    polygon: {
      url: `https://polygon-mainnet.g.alchemy.com/v2/${process.env.ALCHEMY_KEY}`,
      chainId: 137,
      gasMultiplier: 3,
      accounts: [process.env.PRIVATE_KEY],
    },
    polygonAmoy: {
      url: `https://rpc-amoy.polygon.technology/`,
      chainId: 80002,
      accounts: [process.env.PRIVATE_KEY_TESTNET],
    },
    testnet: {
      url: `https://polygon-mumbai.g.alchemy.com/v2/${process.env.ALCHEMY_KEY}`,
      chainId: 80001,
      accounts: [process.env.PRIVATE_KEY_TESTNET],
    },
    testneteth: {
      url: `https://eth-goerli.g.alchemy.com/v2/${process.env.ALCHEMY_KEY}`,
      chainId: 5,
      accounts: [process.env.PRIVATE_KEY_TESTNET],
    },
    mainnet: {
      url: `https://eth-mainnet.g.alchemy.com/v2/${process.env.ALCHEMY_KEY}`,
      chainId: 1,
      // gasMultiplier: 1.5,
      accounts: [process.env.PRIVATE_KEY],
    },
  },
  namedAccounts: {
    deployer: {
      default: 0,
    },
    player: {
      default: 1,
    },
    gwsAccount: {
      default: 2,
    },
    proxyAdmin: {
      31337: 3,
      137: '0xdcEcfaC75FB9E46EA2759F8932d5c5D00f8c96Fd',
      80001: '0x736316f40b36472d447fc86b7ff78484e4261ad1',
    },
    ixtToken: {
      31337: 0,
      137: '0xE06Bd4F5aAc8D0aA337D13eC88dB6defC6eAEefE',
      80001: '0xae5039fc6D8360008419E169d54F1C81c665c55D',
    },
    ixtMaticLpToken: {
      31337: 0,
      137: '0x014ac2a53aa6fba4dcd93fde6d3c787b79a1a6e6',
      80001: '0x4E35eB336A9D1ACC2B2626874B5e2bfA22571a23',
    },
    ixtUsdtLpToken: {
      31337: 0,
      137: '0x304e57c752e854e9a233ae82fcc42f7568b81180',
      80001: '0xd391B55DbA3572033fCA80644071dBC5E0573b2B',
    },
    pixNFT: {
      137: '0xB2435253C71FcA27bE41206EB2793E44e1Df6b6D',
      80001: '0x4BDcFa73220358b2072D58BD30ac565Ed1111B0c',
    },
    pixLandmark: {
      137: '0x9AfB93F1E6D9b13546C4050BA39f0B48a4FB13A7',
      80001: '0x86D5B7f00eF93244eC53A2Ae982F8E5AF47B4Fd7',
    },
    PIXAssets: {
      80001: '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    },
    MaxProxyAdmin: {
      80001: '0x008e33E53678F07C18A6cFF32fa912c67AA6ccaF',
    },
    wmatic: {
      80001: '0x9c3C9283D3e44854697Cd22D3Faa240Cfb032889',
      137: '0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270',
    },
    usdt: {
      80001: '0x5d3F061386d69CF4DC54467CEac58bC277e437b1',
      137: '0xc2132D05D31c914a87C6611C10748AEb04B58e8F',
    },
  },
  dependencyCompiler: {
    paths: ['@openzeppelin/contracts/proxy/transparent/TransparentUpgradeableProxy.sol', '@openzeppelin/contracts/proxy/transparent/ProxyAdmin.sol'],
  },
  typechain: {
    outDir: 'src/types',
    target: 'ethers-v5',
  },
  gasReporter: {
    currency: 'EUR',
    token: 'MATIC',
    gasPriceApi: 'https://api.polygonscan.com/api?module=proxy&action=eth_gasPrice',
    cointmarketcap: 'ba5e8b55-8b40-414a-8d16-608b491bc607', // This is a free key i grabbed
    enabled: false,
  },
  etherscan: {
    apiKey: {
      polygonMumbai: process.env.POLYGONSCAN_TOKEN,
      polygon: process.env.POLYGONSCAN_TOKEN,
      goerli: process.env.ETHERSCAN_TOKEN,
      mainnet: process.env.ETHERSCAN_TOKEN,
      polygonAmoy: process.env.OKLINK_API_KEY,
    },
    customChains: [
      {
        network: "polygonAmoy",
        chainId: 80002,
        urls: {
          apiURL: "https://www.oklink.com/api/explorer/v1/contract/verify/async/api/polygonAmoy",
          browserURL: "https://www.oklink.com/polygonAmoy"
        },
      }
    ],
    gasPriceApi: 'https://api.polygonscan.com/api?module=proxy&action=eth_gasPrice',
    cointmarketcap: 'ba5e8b55-8b40-414a-8d16-608b491bc607', // This is a free key i grabbed
    enabled: true,
  },
  solidity: {
    compilers: [
      {
        version: '0.8.2',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.8.4',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.8.8',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.8.9',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.8.17',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.8.19',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  mocha: {
    timeout: 2000000,
  },
  docgen: {
    path: './docs',
    clear: true,
    runOnCompile: false,
  },
};
