import {expect} from 'chai';
import {ethers, upgrades} from 'hardhat';
import {Signer, Contract, BigNumber} from 'ethers';
import {constants} from '@openzeppelin/test-helpers';
import {PIXCategory, PIXSize, increaseTime} from './utils';
import {SignerWithAddress} from '@nomiclabs/hardhat-ethers/signers';

describe('SyndicatesOfChange', function () {
    let owner: SignerWithAddress;
    let alice: SignerWithAddress;
    let bob: SignerWithAddress;
    let charity: SignerWithAddress;
    let charity2: SignerWithAddress;
    let SoC: Contract;
    let centCoin: Contract; // One cent coin unit is worth one cent
    let notWhitelistedCoin: Contract;
    let oracle: Contract;
    let usdCoin: Contract;
    let goldCoin: Contract;

    beforeEach(async function () {
        [owner, alice, bob, charity, charity2] = await ethers.getSigners();
        let ercFactory = await ethers.getContractFactory('ERC20Mock');
        centCoin = await ercFactory.deploy('CentCoin', 'cc', owner.address, 0);
        usdCoin = await ercFactory.deploy('USDCoin', 'usd', owner.address, 0);
        notWhitelistedCoin = await ercFactory.deploy('CentCoin', 'cc', owner.address, 0);
        goldCoin = await ercFactory.deploy('GoldCoin', 'GC', owner.address, 0);

        let oracleFactory = await ethers.getContractFactory('OracleManagerMock');
        oracle = await oracleFactory.deploy();

        await oracle.setRatio(usdCoin.address, centCoin.address, 1, 1);
        await oracle.setRatio(usdCoin.address, ethers.constants.AddressZero, 1, 1);

        // YOUR SoC IMPLEMENTATION HERE
        let socFactory = await ethers.getContractFactory('SyndicatesOfChange');
        SoC = await socFactory.deploy();
        await SoC.initialize();
        await SoC.setOracleManager(oracle.address);
        await SoC.setUsdToken(usdCoin.address);
        await SoC.setMinimumDonation(1);
    });

    describe('Register cause', function () {
        it('Should emit event upon cause registration', async function () {
            await expect(SoC.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']))
                .to.emit(SoC, 'CauseRegistered')
                .withArgs(1, charity.address);
        });

        it('Should handle default metadata registration', async function () {
            await expect(SoC.registerCause(charity.address, [], ['Toy 1'])).to.not.reverted;
        });

        it("Should increment id's", async function () {
            let firstId = await SoC.callStatic.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            await SoC.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            let secondId = await SoC.callStatic.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            expect(secondId - firstId > 0).to.true;
        });

        it('Should return correct cause info after registration', async function () {
            let firstId = await SoC.callStatic.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            await SoC.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            let causeInfo = await SoC.getCauseInfo(firstId);
            expect(causeInfo['_beneficiary']).to.equal(charity.address);
            expect(causeInfo['_totalDonatedAmount']).to.equal(0);
            expect(causeInfo['_paused']).to.equal(true);
        });
    });

    describe('Register cause reverts', function () {
        it('Should revert on non owner registration', async function () {
            await expect(
                SoC.connect(alice).registerCause(charity.address, [100], ['Toy 0', 'Toy 1']),
            ).to.revertedWith('Ownable: caller is not the owner');
        });

        it('Should revert on zero address beneficiary', async function () {
            await expect(SoC.registerCause(ethers.constants.AddressZero, [], [])).to.revertedWith(
                `SoC__InvalidBeneficiary("${constants.ZERO_ADDRESS}")`,
            );
        });

        it('Should revert on no URI given', async function () {
            await expect(SoC.registerCause(charity.address, [], [])).to.revertedWith(
                `SoC__IncorrectLength(0, 0)`,
            );
        });

        it('Should revert on invalid lengths for arrays', async function () {
            await expect(SoC.registerCause(charity.address, [1], [])).to.revertedWith(
                'SoC__IncorrectLength(1, 0)',
            );
            await expect(SoC.registerCause(charity.address, [1], [1, 2, 3])).to.revertedWith(
                'SoC__IncorrectLength(1, 3)',
            );
            await expect(SoC.registerCause(charity.address, [1, 2, 3, 4, 5], [1, 2, 3])).to.revertedWith(
                'SoC__IncorrectLength(5, 3)',
            );
        });
        it('Should revert on unsorted inputs', async function () {
            await expect(
                SoC.registerCause(charity.address, [200, 100, 300], ['Toy 0', 'Toy 1', 'Toy 2', 'Toy 3']),
            ).to.revertedWith('SoC__InputsNotSorted()');
        });
    });

    describe('Delete cause', function () {
        let id;
        beforeEach(async function () {
            id = await SoC.callStatic.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            await SoC.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
        });
        it('Should emit event upon deletion', async function () {
            await expect(SoC.deleteCause(id)).to.emit(SoC, 'CauseDeleted').withArgs(id);
        });
    });

    describe('Delete cause reverts', function () {
        let id;
        beforeEach(async function () {
            id = await SoC.callStatic.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            await SoC.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
        });
        it('Should revert on non owner call', async function () {
            await expect(SoC.connect(alice).deleteCause(id)).to.revertedWith(
                'Ownable: caller is not the owner',
            );
        });
        it('Should revert on invalid id', async function () {
            await expect(SoC.deleteCause(999)).to.revertedWith('SoC__InvalidCauseId(999)');
        });
    });

    describe('Pause cause', function () {
        let id;
        beforeEach(async function () {
            id = await SoC.callStatic.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            await SoC.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
        });
        it('Should allow owner to pause and unpause', async function () {
            await expect(SoC.setPaused(id, false)).to.not.reverted;
            expect((await SoC.getCauseInfo(id))['_paused']).to.equal(false);
            await expect(SoC.setPaused(id, true)).to.not.reverted;
            expect((await SoC.getCauseInfo(id))['_paused']).to.equal(true);
        });
    });
    describe('Pause cause reverts', function () {
        let id;
        beforeEach(async function () {
            id = await SoC.callStatic.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
            await SoC.registerCause(charity.address, [100], ['Toy 0', 'Toy 1']);
        });
        it('Should revert on non owner call', async function () {
            await expect(SoC.connect(alice).setPaused(id, true)).to.revertedWith(
                'Ownable: caller is not the owner',
            );
            await expect(SoC.connect(alice).setPaused(id, false)).to.revertedWith(
                'Ownable: caller is not the owner',
            );
        });
        it('Should revert on invalid id', async function () {
            await expect(SoC.setPaused(999, true)).to.revertedWith('SoC__InvalidCauseId(999)');
        });
    });

    describe('Whitelist token', function () {
        it('Should emit event upon whitelisting', async function () {
            await expect(SoC.setTokenWhitelist(centCoin.address, true))
                .to.emit(SoC, 'TokenWhitelisted')
                .withArgs(centCoin.address, true);
            await expect(SoC.setTokenWhitelist(centCoin.address, false))
                .to.emit(SoC, 'TokenWhitelisted')
                .withArgs(centCoin.address, false);
        });
    });

    describe('Whitelist token reverts', function () {
        it('Should revert on non owner call', async function () {
            await expect(SoC.connect(alice).setTokenWhitelist(centCoin.address, true)).to.revertedWith(
                'Ownable: caller is not the owner',
            );
        });
        it('Should revert on zero address token', async function () {
            await expect(SoC.setTokenWhitelist(ethers.constants.AddressZero, true)).to.revertedWith(
                `SoC__NotERC20("${ethers.constants.AddressZero}")`,
            );
        });
    });

    describe('Donate', function () {
        let id0;
        let id1;
        beforeEach(async function () {
            id0 = await SoC.callStatic.registerCause(
                charity.address,
                [100, 1000],
                ['Toy 0', 'Toy 1', 'Toy 2'],
            );
            await SoC.registerCause(charity.address, [100, 1000], ['Toy 0', 'Toy 1', 'Toy 2']);
            id1 = await SoC.callStatic.registerCause(
                charity2.address,
                [200, 2000],
                ['Soy 0', 'Soy 1', 'Soy 2'],
            );
            await SoC.registerCause(charity2.address, [200, 2000], ['Soy 0', 'Soy 1', 'Soy 2']);
            await centCoin.mint(alice.address, 10_000000);
            await centCoin.connect(alice).approve(SoC.address, 10_00000);
            await SoC.setPaused(id0, false);
            await SoC.setPaused(id1, false);
            await SoC.setTokenWhitelist(centCoin.address, true);
        });
        it('Should emit Donation event on successful donation', async function () {
            await expect(SoC.connect(alice).donate(id0, centCoin.address, 100, 0))
                .to.emit(SoC, 'Donation')
                .withArgs(alice.address, id0, centCoin.address, 100);
            await expect(SoC.connect(alice).donate(id1, centCoin.address, 100, 0))
                .to.emit(SoC, 'Donation')
                .withArgs(alice.address, id1, centCoin.address, 100);
        });
        it('Should emit Transfer event on successful donation without given token id', async function () {
            await expect(SoC.connect(alice).donate(id0, centCoin.address, 100, 0)).to.emit(
                SoC,
                'Transfer',
            );
            await expect(SoC.connect(alice).donate(id1, centCoin.address, 100, 0)).to.emit(
                SoC,
                'Transfer',
            );
        });
        it('Should mint token to user on successful donation without given token id', async function () {
            let donatedAmount = 100;
            let newTokenId = await SoC.connect(alice).callStatic.donate(
                id0,
                centCoin.address,
                donatedAmount,
                0,
            );
            await SoC.connect(alice).donate(id0, centCoin.address, donatedAmount, 0);
            expect(await SoC.ownerOf(newTokenId)).to.equal(alice.address);
            expect(await SoC.getDonatedAmount(newTokenId)).to.equal(donatedAmount);
            expect(await SoC.getCauseForToken(newTokenId)).to.equal(id0);
            expect((await SoC.getCauseInfo(id0))['_totalDonatedAmount']).to.equal(donatedAmount);
        });
        it('Should mutate existing token on successful donation with valid token id', async function () {
            let donatedAmount = 100;
            let donatedAmount2 = 300;
            let newTokenId = await SoC.connect(alice).callStatic.donate(
                id0,
                centCoin.address,
                donatedAmount,
                0,
            );
            let newTokenString = newTokenId.toNumber();
            await SoC.connect(alice).donate(id0, centCoin.address, donatedAmount, 0);
            expect(await SoC.ownerOf(newTokenString)).to.equal(alice.address);
            expect(await SoC.getDonatedAmount(newTokenString)).to.equal(donatedAmount);
            expect(await SoC.getCauseForToken(newTokenString)).to.equal(id0);
            let sameId = await SoC.connect(alice).callStatic.donate(
                id0,
                centCoin.address,
                donatedAmount2,
                newTokenString,
            );
            let sameIdString = sameId.toNumber();
            await SoC.connect(alice).donate(id0, centCoin.address, donatedAmount2, newTokenString);
            expect(sameIdString).to.equal(newTokenString);
            expect(await SoC.ownerOf(newTokenString)).to.equal(alice.address);
            expect(await SoC.getDonatedAmount(newTokenString)).to.equal(donatedAmount + donatedAmount2);
            expect(await SoC.getCauseForToken(newTokenString)).to.equal(id0);
        });
        it('Should mutate cause info upon donations', async function () {
            let donatedAmount = 100;
            let donatedAmount2 = 300;
            let newTokenId = await SoC.connect(alice).callStatic.donate(
                id0,
                centCoin.address,
                donatedAmount,
                0,
            );
            await SoC.connect(alice).donate(id0, centCoin.address, donatedAmount, 0);
            expect((await SoC.getCauseInfo(id0))['_totalDonatedAmount']).to.equal(donatedAmount);
            await SoC.connect(alice).donate(id0, centCoin.address, donatedAmount2, newTokenId.toNumber());
            expect((await SoC.getCauseInfo(id0))['_totalDonatedAmount']).to.equal(
                donatedAmount + donatedAmount2,
            );
        });
    });

    describe('Donate reverts', function () {
        let id0;
        let id1;
        beforeEach(async function () {
            id0 = await SoC.callStatic.registerCause(
                charity.address,
                [100, 1000],
                ['Toy 0', 'Toy 1', 'Toy 2'],
            );
            await SoC.registerCause(charity.address, [100, 1000], ['Toy 0', 'Toy 1', 'Toy 2']);
            id1 = await SoC.callStatic.registerCause(
                charity2.address,
                [200, 2000],
                ['Soy 0', 'Soy 1', 'Soy 2'],
            );
            await SoC.registerCause(charity2.address, [200, 2000], ['Soy 0', 'Soy 1', 'Soy 2']);
            await centCoin.mint(alice.address, 10000000);
            await centCoin.connect(alice).approve(SoC.address, 1000000);
            await SoC.setPaused(id0, false);
            await SoC.setPaused(id1, false);
            await SoC.setTokenWhitelist(centCoin.address, true);
        });
        it('Should revert on donation less than minimum', async function () {
            await expect(SoC.connect(alice).donate(id0, centCoin.address, 0, 0)).to.revertedWith(
                'SoC__DonationTooSmall',
            );
        });
        it('Should revert on invalid cause id', async function () {
            await expect(SoC.connect(alice).donate(9999, centCoin.address, 10, 0)).to.revertedWith(
                'SoC__InvalidCauseId(9999)',
            );
        });
        it('Should revert on non whitelisted token', async function () {
            await notWhitelistedCoin.mint(alice.address, 10_000000);
            await notWhitelistedCoin.connect(alice).approve(SoC.address, 10_00000);
            await expect(
                SoC.connect(alice).donate(id0, notWhitelistedCoin.address, 10, 0),
            ).to.revertedWith(`SoC__TokenNotWhitelisted("${notWhitelistedCoin.address}")`);
        });
        it('Should revert on not owned token id given as existing token', async function () {
            let donatedAmount = 100;
            await centCoin.mint(bob.address, 10_000000);
            await centCoin.connect(bob).approve(SoC.address, 10_00000);
            let newTokenId = await SoC.connect(alice).callStatic.donate(
                id0,
                centCoin.address,
                donatedAmount,
                0,
            );
            await SoC.connect(alice).donate(id0, centCoin.address, donatedAmount, 0);
            await expect(
                SoC.connect(bob).donate(id0, centCoin.address, donatedAmount, newTokenId),
            ).to.revertedWith(`SoC__NotOwnerOf("${bob.address}", ${newTokenId})`);
        });
        it('Should revert on given token id not being minted', async function () {
            let donatedAmount = 100;
            await centCoin.mint(bob.address, 10_000000);
            await expect(
                SoC.connect(bob).donate(id0, centCoin.address, donatedAmount, 1),
            ).to.revertedWith(`SoC__NotOwnerOf("${bob.address}", ${1})`);
        });
        it('Should revert on given token id not corresponding to the same cause', async function () {
            let donatedAmount = 100;
            let newTokenId = await SoC.connect(alice).callStatic.donate(
                id0,
                centCoin.address,
                donatedAmount,
                0,
            );
            await SoC.connect(alice).donate(id0, centCoin.address, donatedAmount, 0);
            await expect(
                SoC.connect(alice).donate(id1, centCoin.address, donatedAmount, newTokenId),
            ).to.revertedWith(`SoC__DifferentCauses(${id1}, ${id0})`);
        });
        it('Should revert on paused cause', async function () {
            await SoC.setPaused(id0, true);
            await expect(SoC.connect(alice).donate(id0, centCoin.address, 100, 0)).to.revertedWith(
                `SoC__CausePaused(${id0})`,
            );
        });
    });

    describe('Get Donated Amount reverts', function () {
        it('Reverts on invalid token id', async function () {
            await expect(SoC.getDonatedAmount(999)).to.revertedWith('ERC721: token not minted');
        });
    });

    describe('Get Cause for Token reverts', function () {
        it('Reverts on invalid token id', async function () {
            await expect(SoC.getCauseForToken(999)).to.revertedWith('ERC721: token not minted');
        });
    });

    describe('Get Cause Info reverts', function () {
        it('Reverts on invalid cause id', async function () {
            await expect(SoC.getCauseInfo(999)).to.revertedWith('SoC__InvalidCauseId');
        });
    });

    describe('Modify Cause', function () {
        let id0;
        beforeEach(async function () {
            id0 = await SoC.callStatic.registerCause(
                charity.address,
                [100, 1000],
                ['Toy 0', 'Toy 1', 'Toy 2'],
            );
            await SoC.registerCause(charity.address, [100, 1000], ['Toy 0', 'Toy 1', 'Toy 2']);
            await centCoin.mint(alice.address, 10_000000);
            await centCoin.connect(alice).approve(SoC.address, 10_00000);
            await SoC.setPaused(id0, false);
            await SoC.setTokenWhitelist(centCoin.address, true);
        });
        it('Modifies as expected', async function () {
            let firstId = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 100, 0);

            await SoC.connect(alice).donate(id0, centCoin.address, 100, 0);
            await expect(await SoC.tokenURI(firstId)).to.equal('Toy 1');
            await SoC.modifyCause(id0, [200, 2000], ['Soy 0', 'Soy 1', 'Soy 2']);

            let secondId = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 100, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 100, 0);
            await expect(await SoC.tokenURI(secondId)).to.equal('Soy 0');
        });
        it('Modifies when only a single uri given', async function () {
            await expect(SoC.modifyCause(id0, [], ['Boy 1'])).to.not.reverted;
        });
        it('Does not alter existing tokens when modifying', async function () {
            let firstId = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 100, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 100, 0);
            SoC.modifyCause(id0, [200, 2000, 3000, 4000], ['Soy 0', 'Soy 1', 'Soy 2', 'Soy 3', 'Soy 4']);
            let secondId = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 100, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 100, 0);
            let thirdId = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 200, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 200, 0);
            let fourthId = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 2000, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 2000, 0);
            let fifthId = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 3000, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 3000, 0);
            let sixthId = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 4000, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 4000, 0);
            await expect(await SoC.tokenURI(firstId)).to.equal('Toy 1');
            await expect(await SoC.tokenURI(secondId)).to.equal('Soy 0');
            await expect(await SoC.tokenURI(thirdId)).to.equal('Soy 1');
            await expect(await SoC.tokenURI(fourthId)).to.equal('Soy 2');
            await expect(await SoC.tokenURI(fifthId)).to.equal('Soy 3');
            await expect(await SoC.tokenURI(sixthId)).to.equal('Soy 4');
        });
    });

    describe('Modify Cause reverts', function () {
        let id0;
        beforeEach(async function () {
            id0 = await SoC.callStatic.registerCause(
                charity.address,
                [100, 1000],
                ['Toy 0', 'Toy 1', 'Toy 2'],
            );
            await SoC.registerCause(charity.address, [100, 1000], ['Toy 0', 'Toy 1', 'Toy 2']);
            await centCoin.mint(alice.address, 10_000000);
            await centCoin.connect(alice).approve(SoC.address, 10_00000);
            await SoC.setPaused(id0, false);
        });
        it('Reverts on non owner call', async function () {
            await expect(SoC.connect(alice).modifyCause(id0, [], [])).to.revertedWith(
                'Ownable: caller is not the owner',
            );
        });
        it('Reverts on invalid cause id', async function () {
            await expect(SoC.modifyCause(999, [], [])).to.revertedWith('SoC__InvalidCauseId(999)');
        });
        it('Reverts on no URI given', async function () {
            await expect(SoC.modifyCause(id0, [], [])).to.revertedWith('SoC__IncorrectLength(0, 0)');
        });
        it('Reverts on unsorted breakpoints given', async function () {
            await expect(SoC.modifyCause(id0, [200, 100], ['Soy 0', 'Soy 1', 'Soy 2'])).to.revertedWith(
                'SoC__InputsNotSorted()',
            );
        });
        it('Reverts on invalid list lengths', async function () {
            await expect(SoC.modifyCause(id0, [1], [])).to.revertedWith('SoC__IncorrectLength(1, 0)');
            await expect(SoC.modifyCause(id0, [1], [1, 2, 3])).to.revertedWith(
                'SoC__IncorrectLength(1, 3)',
            );
            await expect(SoC.modifyCause(id0, [1, 2, 3, 4, 5], [1, 2, 3])).to.revertedWith(
                'SoC__IncorrectLength(5, 3)',
            );
        });
    });

    describe('Merge Tokens', function () {
        let id0;
        let id1;
        let tokenid0;
        let tokenid1;
        let tokenid2;

        beforeEach(async function () {
            id0 = await SoC.callStatic.registerCause(
                charity.address,
                [100, 1000],
                ['Toy 0', 'Toy 1', 'Toy 2'],
            );
            await SoC.registerCause(charity.address, [100, 1000], ['Toy 0', 'Toy 1', 'Toy 2']);
            id1 = await SoC.callStatic.registerCause(
                charity2.address,
                [200, 2000],
                ['Soy 0', 'Soy 1', 'Soy 2'],
            );
            await SoC.registerCause(charity2.address, [200, 2000], ['Soy 0', 'Soy 1', 'Soy 2']);
            await centCoin.mint(alice.address, 10_000000);
            await centCoin.connect(alice).approve(SoC.address, 10_00000);
            await centCoin.mint(bob.address, 10_000000);
            await centCoin.connect(bob).approve(SoC.address, 10_00000);
            await SoC.setPaused(id0, false);
            await SoC.setPaused(id1, false);
            await SoC.setTokenWhitelist(centCoin.address, true);
            tokenid0 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 1, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 1, 0);
            tokenid1 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 3, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 3, 0);
            tokenid2 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 7, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 7, 0);
        });
        it('Merges tokens as expected', async function () {
            let donation0 = await SoC.getDonatedAmount(tokenid0);
            let donation1 = await SoC.getDonatedAmount(tokenid1);
            await SoC.connect(alice).mergeTokens([tokenid0, tokenid1]);
            let newTokenId = await SoC.s_totalSupply();

            expect(await SoC.ownerOf(newTokenId)).to.equal(alice.address);
            expect(await SoC.getDonatedAmount(newTokenId)).to.equal(
                donation0.toNumber() + donation1.toNumber(),
            );
            expect(await SoC.getCauseForToken(newTokenId)).to.equal(id0);
        });
        it('Assigns new metadata if merging after a cause has been modified', async function () {
            await SoC.modifyCause(id0, [200, 2000], ['Boy 0', 'Boy 1', 'Boy 2']);

            await SoC.connect(alice).mergeTokens([tokenid0, tokenid1, tokenid2]);
            let newTokenId = await SoC.s_totalSupply();

            await expect(await SoC.tokenURI(newTokenId)).to.equal('Boy 0');
        });
    });

    describe('Merge Tokens reverts', function () {
        let id0;
        let id1;
        let tokenid0;
        let tokenid1;
        let tokenid2;
        let tokenid3;
        let bobTokenId;

        beforeEach(async function () {
            id0 = await SoC.callStatic.registerCause(
                charity.address,
                [100, 1000],
                ['Toy 0', 'Toy 1', 'Toy 2'],
            );
            await SoC.registerCause(charity.address, [100, 1000], ['Toy 0', 'Toy 1', 'Toy 2']);
            id1 = await SoC.callStatic.registerCause(
                charity2.address,
                [200, 2000],
                ['Soy 0', 'Soy 1', 'Soy 2'],
            );
            await SoC.registerCause(charity2.address, [200, 2000], ['Soy 0', 'Soy 1', 'Soy 2']);
            await centCoin.mint(alice.address, 10_000000);
            await centCoin.connect(alice).approve(SoC.address, 10_00000);
            await centCoin.mint(bob.address, 10_000000);
            await centCoin.connect(bob).approve(SoC.address, 10_00000);
            await SoC.setPaused(id0, false);
            await SoC.setPaused(id1, false);
            await SoC.setTokenWhitelist(centCoin.address, true);
            tokenid0 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 1, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 1, 0);
            tokenid1 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 3, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 3, 0);
            tokenid2 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 7, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 7, 0);
            tokenid3 = await SoC.connect(alice).callStatic.donate(id1, centCoin.address, 7, 0);
            await SoC.connect(alice).donate(id1, centCoin.address, 7, 0);
            bobTokenId = await SoC.connect(bob).callStatic.donate(id0, centCoin.address, 11, 0);
            await SoC.connect(bob).donate(id0, centCoin.address, 11, 0);
        });
        it('Reverts on caller not being owner of token(s)', async function () {
            await expect(SoC.connect(alice).mergeTokens([tokenid0, bobTokenId])).to.revertedWith(
                `SoC__NotOwnerOf("${alice.address}", ${bobTokenId})`,
            );
        });
        it('Reverts on given tokens corresponding to different causes', async function () {
            let soc0;
            let soc3;
            soc0 = await SoC.getCauseForToken(tokenid0.toNumber());
            soc3 = await SoC.getCauseForToken(tokenid3.toNumber());
            await expect(SoC.connect(alice).mergeTokens([tokenid0, tokenid3])).to.revertedWith(
                `SoC__DifferentCauses(${id0}, ${id1})`,
            );
        });
        it('Reverts on not minted token id', async function () {
            await expect(SoC.connect(alice).mergeTokens([tokenid0, 99999])).to.revertedWith(
                `SoC__NotOwnerOf("${alice.address}", 99999)`,
            );
        });
    });

    describe('Withdraw donations', function () {
        let id0;
        let id1;
        let tokenid0;
        let tokenid1;
        let tokenid2;
        let tokenid3;
        let tokenid4;
        let tokenid5;
        let addressZero = ethers.constants.AddressZero;
        beforeEach(async function () {
            id0 = await SoC.callStatic.registerCause(
                charity.address,
                [100, 1000],
                ['Toy 0', 'Toy 1', 'Toy 2'],
            );
            await SoC.registerCause(charity.address, [100, 1000], ['Toy 0', 'Toy 1', 'Toy 2']);
            id1 = await SoC.callStatic.registerCause(
                charity2.address,
                [200, 2000],
                ['Soy 0', 'Soy 1', 'Soy 2'],
            );
            await SoC.registerCause(charity2.address, [200, 2000], ['Soy 0', 'Soy 1', 'Soy 2']);
            await centCoin.mint(alice.address, 10_000000);
            await centCoin.connect(alice).approve(SoC.address, 10_00000);
            await centCoin.mint(bob.address, 10_000000);
            await centCoin.connect(bob).approve(SoC.address, 10_00000);
            await SoC.setPaused(id0, false);
            await SoC.setPaused(id1, false);
            await SoC.setTokenWhitelist(centCoin.address, true);
            tokenid0 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 100, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 100, 0);
            tokenid1 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 300, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 300, 0);
            tokenid2 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 700, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 700, 0);
            tokenid3 = await SoC.connect(alice).callStatic.donate(id1, centCoin.address, 700, 0);
            await SoC.connect(alice).donate(id1, centCoin.address, 700, 0);

            tokenid4 = await SoC.connect(alice).callStatic.donate(id0, addressZero, 0, 0, {
                value: ethers.utils.parseEther('1.0'),
            });
            await SoC.connect(alice).donate(id0, addressZero, 0, 0, {
                value: ethers.utils.parseEther('1.0'),
            });

            tokenid5 = await SoC.connect(alice).callStatic.donate(id0, addressZero, 0, 0, {
                value: ethers.utils.parseEther('1.0'),
            });
            await SoC.connect(alice).donate(id0, addressZero, 0, 0, {
                value: ethers.utils.parseEther('1.0'),
            });
        });
        it('Withdraws eth as expected', async function () {
            let bal;
            let preBal = await charity.getBalance();
            bal = await SoC.getPendingBalance(addressZero, charity.address);
            await expect(SoC.connect(charity).withdraw(addressZero))
                .to.emit(SoC, 'Withdrawal')
                .withArgs(charity.address, constants.ZERO_ADDRESS, bal);
            expect(await SoC.getPendingBalance(addressZero, charity.address)).to.equal(0);
            let postBal = await charity.getBalance();
        });
        it('Withdraws erc-20 as expected', async function () {
            let bal;
            let preBal = await centCoin.balanceOf(charity.address);

            bal = await SoC.getPendingBalance(centCoin.address, charity.address);
            await expect(SoC.connect(charity).withdraw(centCoin.address))
                .to.emit(SoC, 'Withdrawal')
                .withArgs(charity.address, centCoin.address, bal);
            expect(await SoC.getPendingBalance(centCoin.address, charity.address)).to.equal(0);
            expect(await centCoin.balanceOf(charity.address)).to.equal(1100);
            let postBal = await centCoin.balanceOf(charity.address);
        });
        it('Withdraws both eth and erc-20 as expected', async function () {
            let ethBal;
            let erc20Bal;
            ethBal = await SoC.getPendingBalance(addressZero, charity.address);
            erc20Bal = await SoC.getPendingBalance(centCoin.address, charity.address);

            await expect(SoC.connect(charity).withdraw(addressZero))
                .to.emit(SoC, 'Withdrawal')
                .withArgs(charity.address, constants.ZERO_ADDRESS, ethBal);
            expect(await SoC.getPendingBalance(addressZero, charity.address)).to.equal(0);

            await expect(SoC.connect(charity).withdraw(centCoin.address))
                .to.emit(SoC, 'Withdrawal')
                .withArgs(charity.address, centCoin.address, erc20Bal);
            expect(await SoC.getPendingBalance(centCoin.address, charity.address)).to.equal(0);
            expect(await centCoin.balanceOf(charity.address)).to.equal(1100);
        });
        it('Withdraws eth before and after new donations', async function () {
            let ethBal;
            ethBal = await SoC.getPendingBalance(addressZero, charity.address);

            await SoC.connect(charity).withdraw(addressZero);

            expect(await SoC.getPendingBalance(addressZero, charity.address)).to.equal(0);
            await expect(SoC.connect(charity).withdraw(addressZero)).to.revertedWith(
                'USER HAS NO BALANCE',
            );

            await SoC.connect(bob).donate(id0, addressZero, 0, 0, {
                value: ethers.utils.parseEther('3.0'),
            });

            await expect(SoC.connect(charity).withdraw(addressZero)).to.not.reverted;
            expect(await SoC.getPendingBalance(addressZero, charity.address)).to.equal(0);
        });
        it('Withdraws erc-20 before and after new donations', async function () {
            let erc20Bal;
            erc20Bal = await SoC.getPendingBalance(centCoin.address, charity.address);

            await SoC.connect(charity).withdraw(centCoin.address);

            expect(await SoC.getPendingBalance(centCoin.address, charity.address)).to.equal(0);
            await expect(SoC.connect(charity).withdraw(centCoin.address)).to.revertedWith(
                'USER HAS NO BALANCE',
            );

            await SoC.connect(bob).donate(id0, centCoin.address, 100, 0);

            await expect(SoC.connect(charity).withdraw(centCoin.address)).to.not.reverted;
            expect(await SoC.getPendingBalance(centCoin.address, charity.address)).to.equal(0);
            expect(await centCoin.balanceOf(charity.address)).to.equal(1200);
        });
    });
    describe('Withdraw donations reverts', function () {
        let id0;
        let id1;
        let tokenid0;
        let tokenid1;
        let tokenid2;
        let tokenid4;
        let tokenid5;
        let addressZero = ethers.constants.AddressZero;
        beforeEach(async function () {
            id0 = await SoC.callStatic.registerCause(
                charity.address,
                [100, 1000],
                ['Toy 0', 'Toy 1', 'Toy 2'],
            );
            await SoC.registerCause(charity.address, [100, 1000], ['Toy 0', 'Toy 1', 'Toy 2']);
            id1 = await SoC.callStatic.registerCause(
                charity2.address,
                [200, 2000],
                ['Soy 0', 'Soy 1', 'Soy 2'],
            );
            await SoC.registerCause(charity2.address, [200, 2000], ['Soy 0', 'Soy 1', 'Soy 2']);
            await centCoin.mint(alice.address, 10_000000);
            await centCoin.connect(alice).approve(SoC.address, 10_00000);
            await centCoin.mint(bob.address, 10_000000);
            await centCoin.connect(bob).approve(SoC.address, 10_00000);
            await SoC.setPaused(id0, false);
            await SoC.setPaused(id1, false);
            await SoC.setTokenWhitelist(centCoin.address, true);
            tokenid0 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 100, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 100, 0);
            tokenid1 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 300, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 300, 0);
            tokenid2 = await SoC.connect(alice).callStatic.donate(id0, centCoin.address, 700, 0);
            await SoC.connect(alice).donate(id0, centCoin.address, 700, 0);

            tokenid4 = await SoC.connect(alice).callStatic.donate(id0, addressZero, 200, 0, {
                value: ethers.utils.parseEther('1.0'),
            });
            await SoC.connect(alice).donate(id0, addressZero, 200, 0, {
                value: ethers.utils.parseEther('1.0'),
            });

            tokenid5 = await SoC.connect(alice).callStatic.donate(id0, addressZero, 400, 0, {
                value: ethers.utils.parseEther('1.0'),
            });
            await SoC.connect(alice).donate(id0, addressZero, 400, 0, {
                value: ethers.utils.parseEther('1.0'),
            });
        });
        it('Should revert on non-beneficiary trying to withdraw', async function () {
            await expect(SoC.connect(bob).withdraw(addressZero)).to.revertedWith(
                `SoC__NotBeneficiary("${bob.address}")`,
            );
        });
        it('Should Revert on address with zero balance withdrawing', async function () {
            await expect(SoC.connect(charity2).withdraw(addressZero)).to.reverted;
            await expect(SoC.connect(charity2).withdraw(centCoin.address)).to.reverted;
        });
        it('Should Revert on trying to withdraw non whitelisted token', async function () {
            await expect(SoC.connect(charity).withdraw(goldCoin.address)).to.reverted;
        });
        it('Should Revert when beneficiary tries to withdraw eth twice in a row with no new donations in between', async function () {
            await SoC.connect(charity).withdraw(addressZero);
            await expect(SoC.connect(charity).withdraw(addressZero)).to.reverted;
        });
        it('Should Revert when beneficiary tries to withdraw erc-20 twice in a row with no new donations in between', async function () {
            await SoC.connect(charity).withdraw(centCoin.address);
            await expect(SoC.connect(charity).withdraw(centCoin.address)).to.reverted;
        });
        it('Should Revert when beneficiary tries to withdraw both types twice in a row with no new donations in between', async function () {
            await SoC.connect(charity).withdraw(addressZero);
            await SoC.connect(charity).withdraw(centCoin.address);
            await expect(SoC.connect(charity).withdraw(addressZero)).to.reverted;
            await expect(SoC.connect(charity).withdraw(centCoin.address)).to.reverted;
        });
    });
});
