import {network, ethers, upgrades} from 'hardhat';
import {SignerWithAddress} from '@nomiclabs/hardhat-ethers/signers';
import {expect} from 'chai';
import {BigNumber, Contract} from 'ethers';
import {describe} from "mocha";

describe('AssetConverter', function () {
    this.timeout(90000);
    let assetManager: Contract;
    let assetsConverter: Contract;
    let deployer: SignerWithAddress;
    let player: SignerWithAddress;
    let alice: SignerWithAddress;
    const WASTE_ID = 7;
    const ASTRO_CREDITS = 8;
    const LEGENDARY_BIOMOD = 14;
    const RARE_BIOMOD = 13;
    const UNCOMMON_BIOMOD = 12;
    const COMMON_BIOMOD = 11;
    const OUTLIER_BIOMOD = 10;
    const INITIAL_WASTE_BALANCE = 3000;
    const fromTokenIds = [WASTE_ID, WASTE_ID, WASTE_ID, WASTE_ID, WASTE_ID]
    const toTokenIds = [OUTLIER_BIOMOD, COMMON_BIOMOD, UNCOMMON_BIOMOD, RARE_BIOMOD, LEGENDARY_BIOMOD]
    const prices = [150, 200, 300, 500, 1337]


    beforeEach(async () => {
        [deployer, player, alice] = await ethers.getSigners();


        let assetManagerFactory = await ethers.getContractFactory('NFT');
        assetManager = await upgrades.deployProxy(assetManagerFactory, ['PlanetIX', 'PIX']);

        await assetManager.deployed()
        let assetsConverterFactory = await ethers.getContractFactory('AssetsConverter');
        assetsConverter = await upgrades.deployProxy(assetsConverterFactory, []);
        await assetsConverter.setAssetManager(assetManager.address)

        await assetManager.setTrustedContract(deployer.getAddress());
        await assetManager.setTrustedContract(assetsConverter.address);

        await assetManager.toggleStatus();
        await assetManager.setURI(WASTE_ID, 'TokenURI');
        await assetManager.setURI(ASTRO_CREDITS, 'TokenURI');
        await assetManager.setURI(LEGENDARY_BIOMOD, 'TokenURI');
        await assetManager.setURI(RARE_BIOMOD, 'TokenURI');
        await assetManager.setURI(UNCOMMON_BIOMOD, 'TokenURI');
        await assetManager.setURI(COMMON_BIOMOD, 'TokenURI');
        await assetManager.setURI(OUTLIER_BIOMOD, 'TokenURI');
        await assetManager.connect(deployer).trustedMint(player.address, WASTE_ID, INITIAL_WASTE_BALANCE);
        //set up AssetsConverter


        for (let i = 0; i < fromTokenIds.length; i++) {
            await assetsConverter.createAssetConversionRecipe([fromTokenIds[i]], [toTokenIds[i]], [prices[i]], 0, 0)
        }
    });

    describe('create asset conversion recipe as expected', () => {
        it('create asset conversion', async () => {
            await expect(assetsConverter.createAssetConversionRecipe([1], [2], [3], 0, 0))
                .to.emit(assetsConverter, 'AssetsConverter__NewRecipe')
                .withArgs([1], [2], [3], 6, 0, 0);

            await expect(assetsConverter.createAssetConversionRecipe([1, 2], [3, 4], [4, 5], 0, 0))
                .to.emit(assetsConverter, 'AssetsConverter__NewRecipe')
                .withArgs([1, 2], [3, 4], [4, 5], 7, 0, 0);

            await expect(assetsConverter.createAssetConversionRecipe([1], [3, 4, 5], [5], 0, 0))
                .to.emit(assetsConverter, 'AssetsConverter__NewRecipe')
                .withArgs([1], [3, 4, 5], [5], 8, 0, 0);


            await expect(assetsConverter.createAssetConversionRecipe([1, 2, 3], [5], [5, 6, 7], 2, 3))
                .to.emit(assetsConverter, 'AssetsConverter__NewRecipe')
                .withArgs([1, 2, 3], [5], [5, 6, 7], 9, 2, 3);
        });
        it('revert creating of asset conversion', async () => {
            await expect(assetsConverter.createAssetConversionRecipe([1], [2], [3, 4], 0, 0))
                .to.revertedWith('AssetsConverter__InvalidRecipes()')

            await expect(assetsConverter.createAssetConversionRecipe([1, 2, 3], [3, 4], [4, 5], 0, 0))
                .to.revertedWith('AssetsConverter__InvalidRecipes()')
        });
    });
    describe('convert assets as expected', () => {
        it('asset conversion', async () => {
            await expect(assetsConverter.connect(player).convertAsset(1, 1))
                .to.emit(assetsConverter, 'AssetsConverter__ConversionComplete')
                .withArgs(player.address, 1, 1);

        });
        it('asset conversion2', async () => {
            let amount = 2
            let id = 2
            await expect(assetsConverter.connect(player).convertAsset(id, amount))
                .to.emit(assetsConverter, 'AssetsConverter__ConversionComplete')
                .withArgs(player.address, id, amount);
            expect(await assetManager.balanceOf(player.address, WASTE_ID)).to.equal(INITIAL_WASTE_BALANCE - prices[1] * amount)
            expect(await assetManager.balanceOf(player.address, toTokenIds[1])).to.equal(amount)
        });
        it('asset conversion, multiple assets', async () => {
            await assetsConverter.createAssetConversionRecipe([ASTRO_CREDITS,WASTE_ID], [LEGENDARY_BIOMOD,RARE_BIOMOD], [1, 2], 0, 0)
            let amount = 1
            let id = 6
            let mintAmount = 5
            await assetManager.connect(deployer).trustedMint(alice.address, WASTE_ID, mintAmount);
            await assetManager.connect(deployer).trustedMint(alice.address, ASTRO_CREDITS, mintAmount);

            await assetsConverter.connect(alice).convertAsset(id, amount)

            expect(await assetManager.balanceOf(alice.address, ASTRO_CREDITS)).to.equal(mintAmount - 1 * amount)
            expect(await assetManager.balanceOf(alice.address, WASTE_ID)).to.equal(mintAmount - 2 * amount)
            expect(await assetManager.balanceOf(alice.address, LEGENDARY_BIOMOD)).to.equal(amount)
            expect(await assetManager.balanceOf(alice.address, RARE_BIOMOD)).to.equal(amount)
        });

        it('revert asset conversion AssetsConverter__InvalidAmount', async () => {
            let id = 2
            await expect(assetsConverter.connect(player).convertAsset(id, 0)).to.revertedWith('AssetsConverter__InvalidAmount()')
        });
        it('revert asset conversion AssetsConverter__InvalidRecipeId', async () => {
            await expect(assetsConverter.connect(player).convertAsset(6, 1)).to.revertedWith('AssetsConverter__InvalidRecipeId()')
        });
        it('revert asset conversion AssetsConverter__MaximumConversionsExceeded', async () => {
            await assetManager.connect(deployer).trustedMint(alice.address, WASTE_ID, INITIAL_WASTE_BALANCE);
            let [id, amount] = [6,1]

            await assetsConverter.createAssetConversionRecipe([fromTokenIds[1]], [toTokenIds[1]], [prices[1]], 2, 0)
            await expect(assetsConverter.connect(player).convertAsset(id, amount))
                .to.emit(assetsConverter, 'AssetsConverter__ConversionComplete')
                .withArgs(player.address, id, amount);
            await expect(assetsConverter.connect(player).convertAsset(id, amount))
                .to.emit(assetsConverter, 'AssetsConverter__ConversionComplete')
                .withArgs(player.address, id, amount);
            await expect(assetsConverter.connect(player).convertAsset(id, amount)).to.revertedWith('AssetsConverter__MaximumConversionsExceeded()')
            await expect(assetsConverter.connect(alice).convertAsset(id, amount)).to.emit(assetsConverter, 'AssetsConverter__ConversionComplete')
                .withArgs(alice.address, id, amount);
        });
        it('revert asset conversion AssetsConverter__TotalConversionSupplyExceeded', async () => {
            let [id, amount] = [6,3]
            await assetsConverter.createAssetConversionRecipe([fromTokenIds[1]], [toTokenIds[1]], [prices[1]], 3, 2)

            await expect(assetsConverter.connect(player).convertAsset(id, amount)).to.revertedWith('AssetsConverter__TotalConversionSupplyExceeded()')
            await expect(assetsConverter.connect(alice).convertAsset(id, amount)).to.revertedWith('AssetsConverter__TotalConversionSupplyExceeded()')
        });
        it('revert asset conversion no assets', async () => {
            let [id, amount] = [1,1]
            await expect(assetsConverter.connect(alice).convertAsset(id, amount)).to.revertedWith('ERC1155: burn amount exceeds balance')
        });
        it('revert not admin', async () => {
            let [id, amount] = [1,1]
            await expect(assetsConverter.connect(player).deleteAssetConversionRecipe(id)).to.revertedWith(`AssetsConverter__NotAdmin("${player.address}")`)
        });
        it('revert after deleteAssetConversionRecipe', async () => {
            let [id, amount] = [1,1]
            await expect(assetsConverter.connect(player).convertAsset(id, amount)).to.emit(assetsConverter, 'AssetsConverter__ConversionComplete')
                .withArgs(player.address, id, amount);

            await assetsConverter.deleteAssetConversionRecipe(id)
            await expect(assetsConverter.connect(player).convertAsset(id, amount)).to.revertedWith('AssetsConverter__InvalidRecipeId()')
        });
        it('view get recipe', async () => {
            let rec = await assetsConverter.recipes[1]
            let rec1 = await assetsConverter.getRecipeFormula(1)
        });
    });
});