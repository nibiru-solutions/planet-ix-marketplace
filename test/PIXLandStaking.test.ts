import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Signer, Contract, BigNumber, constants, utils } from 'ethers';
import { PIXCategory, increaseTime, advanceTime } from './utils';
import { formatEther, parseEther } from 'ethers/lib/utils';


describe('PIXLandStaking', function () {
  let owner: Signer;
  let alice: Signer;
  let bob: Signer;
  let pixToken: Contract;
  let pixLandmark: Contract;
  let landStaking: Contract;

  beforeEach(async function () {
    [owner, alice, bob] = await ethers.getSigners();

    const PIXTFactory = await ethers.getContractFactory('PIXT');
    pixToken = await PIXTFactory.deploy();

    const PIXLandmarkFactory = await ethers.getContractFactory('PIXLandmark');
    pixLandmark = await upgrades.deployProxy(PIXLandmarkFactory);

    const PIXLandStakingFactory = await ethers.getContractFactory('PIXLandStaking');
    landStaking = await upgrades.deployProxy(PIXLandStakingFactory, [
      pixToken.address,
      pixLandmark.address,
    ]);

    await pixLandmark.setMaxSupplies([1], [100]);
    await pixLandmark.safeMint(await alice.getAddress(), 1, 20);
    await pixLandmark.safeMint(await bob.getAddress(), 1, 20);
    await pixToken.approve(landStaking.address, utils.parseEther('1000000'));
    await pixLandmark.connect(alice).setApprovalForAll(landStaking.address, true);
    await pixLandmark.connect(bob).setApprovalForAll(landStaking.address, true);
    await landStaking.setRewardDistributor(await owner.getAddress());    
  });

  describe('stake', () => {
    it('should stake an NFT', async function () {
      await landStaking.connect(alice).stake(1, 5);
      expect(await pixLandmark.balanceOf(await alice.getAddress(), 1)).to.equal(15);
      expect(await landStaking.getStakedAmounts(await alice.getAddress(), 1)).to.equal(5);
    });
  });
  
  describe('initializePools', () => {
    it('revert if msg.sender is not reward distributor', async function () {
      await expect(landStaking.connect(alice).initializePools([1], [1000], 86400)).to.revertedWith('Staking: NON_DISTRIBUTOR');
    });

    it('revert if arguments are invalid', async function () {
      await expect(landStaking.initializePools([1, 2], [1000], 86400)).to.revertedWith('Staking: INVALID_ARGUMENTS');
    });

    it('should deposit the correct staking rewards', async function () {
      await landStaking.initializePools([1], [1000], 86400);
      expect(await pixToken.balanceOf(await landStaking.address)).to.equal(1000);
    });
  });

  describe('claim', () => {
    beforeEach(async function () {
      let monthSeconds = 60 * 60 * 24 * 30;
      let epochTotalReward = parseEther('1000');
      await landStaking.initializePools([1], [epochTotalReward], monthSeconds);
      // Stake an NFT from Alice
      await landStaking.connect(alice).stake(1, 5);
    });

    it('should return the correct staking amount', async function () {
      expect((await landStaking.getStakedAmounts(await alice.getAddress(), 1))).to.equal(5);
    });

    it('should claim correct rewards amount', async function () {
      await advanceTime(60 * 60 * 24);
      await landStaking.connect(alice).claim();
      let expectedDailyReward = parseEther("33.3333333333332928");
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedDailyReward))).to.be.most(1);
    });

    it('should claim all rewards amount after epoch end', async function () {
      await advanceTime(60 * 60 * 24 * 30);
      await landStaking.connect(alice).claim();
      let expectedMonthlyReward = parseEther("999.999614197529648195");
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedMonthlyReward))).to.be.most(1);
    });

    it('should claim correct amount of rewards after second epoch epoch end', async function () {
      let monthSeconds = 60 * 60 * 24 * 30;
      let epochTotalReward = parseEther('1000');
      await advanceTime(monthSeconds);
      await landStaking.initializePools([1],[epochTotalReward], monthSeconds);
      await landStaking.connect(bob).stake(1, 5);
      await advanceTime(monthSeconds);
      await landStaking.connect(alice).claim();
      await landStaking.connect(bob).claim();
      let expectedAlicesReward = parseEther("1499.999807098763608090");
      let expectedBobsReward = parseEther("499.999807098764824095");
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedAlicesReward))).to.be.most(1);
      expect(Math.abs((await pixToken.balanceOf(await bob.getAddress())).sub(expectedBobsReward))).to.be.most(1);
    });
  });

  describe('unstake', () => {
    beforeEach(async function () {
      // Stake an NFT from Alice
      await landStaking.connect(alice).stake(1, 5);
    });

    it('revert if msg.sender is unstaking invalid token', async function () {
      await expect(landStaking.unstake(0, 10)).to.revertedWith('LandStaking: INVALID_TOKEN_ID');
    });

    it('revert if msg.sender is not staker', async function () {
      await expect(landStaking.unstake(1, 10)).to.revertedWith('LandStaking: NOT_ENOUGH');
    });

    it('revert if msg.sender is trying to unstake more than he staked', async function () {
      await expect(landStaking.connect(alice).unstake(1, 10)).to.revertedWith('LandStaking: NOT_ENOUGH');
    });

    it('should return the correct staking amount', async function () {
      await landStaking.connect(alice).unstake(1, 3);
      expect((await landStaking.getStakedAmounts(await alice.getAddress(), 1))).to.equal(2);
    });

    it('should stake again', async function () {
      await landStaking.connect(alice).unstake(1, 5);
      await landStaking.connect(alice).stake(1, 10);
      expect(await pixLandmark.balanceOf(await alice.getAddress(), 1)).to.equal(10);
    });
  });
});