import { assert, expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { Signer, Contract, BigNumber, providers, utils } from 'ethers';
import { constants } from '@openzeppelin/test-helpers';
import { parseEther } from 'ethers/lib/utils';

import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

import { ERC1155StoreGeneric } from '../src/types/ERC1155StoreGeneric';
import { ERC1155MockAssetManager } from '../src/types/ERC1155MockAssetManager';
import { ERC20Mock } from '../src/types/ERC20Mock';
import { ERC721Mock } from '../src/types/ERC721Mock';
import { SaleBonuses } from '../src/types/SaleBonuses';
import { VRFManager } from '../src/types/VRFManager';
import { ERC1155StoreGenericDataProvider } from '../src/types/ERC1155StoreGenericDataProvider'

describe.only('ERC1155 GENERIC STORE', function () {
  let owner: SignerWithAddress;
  let alice_moderator: SignerWithAddress;
  let bob: SignerWithAddress;
  let elise_beneficiary: SignerWithAddress;
  let cindy_beneficiary: SignerWithAddress;
  let allen_beneficiary: SignerWithAddress;
  let lisa_donor: SignerWithAddress;

  let jourdanCoin: ERC20Mock;
  let maxCoin: ERC20Mock;

  let oracle: Contract;
  let swapManager: Contract;

  let genericStore: ERC1155StoreGeneric;
  let toyNFT1: ERC1155MockAssetManager;
  let toyNFT2: ERC1155MockAssetManager;
  let toyNFT3: ERC1155MockAssetManager;

  let swap_mint_amt = 100;

  beforeEach(async function () {
    [owner, alice_moderator, bob, elise_beneficiary, cindy_beneficiary, allen_beneficiary, lisa_donor] = await ethers.getSigners();

    let erc20Factory = await ethers.getContractFactory('ERC20Mock');
    jourdanCoin = await erc20Factory.deploy('JDCoin', 'JDC');
    maxCoin = await erc20Factory.deploy('MaxCoin', 'MXC');

    let toyERC1155Factory = await ethers.getContractFactory('ERC1155MockAssetManager');
    toyNFT1 = await toyERC1155Factory.deploy('Toy URI 1');
    toyNFT2 = await toyERC1155Factory.deploy('Toy URI 2');
    toyNFT3 = await toyERC1155Factory.deploy('Toy URI 3');

    let oracleFactory = await ethers.getContractFactory('OracleManagerMock');
    oracle = await oracleFactory.deploy();
    let swapFactory = await ethers.getContractFactory('SwapManagerMock');
    swapManager = await swapFactory.deploy();
    await swapManager.payMe({ value: parseEther('50.0') });
    await jourdanCoin.mint(swapManager.address, swap_mint_amt);
    await maxCoin.mint(swapManager.address, swap_mint_amt);
    await oracle.setRatio(constants.ZERO_ADDRESS, jourdanCoin.address, 1, 10);
    await oracle.setRatio(constants.ZERO_ADDRESS, maxCoin.address, 1, 5);
    await oracle.setRatio(jourdanCoin.address, maxCoin.address, 10, 5);

    const genericStoreFactory = await ethers.getContractFactory('ERC1155StoreGeneric');
    genericStore = (await upgrades.deployProxy(genericStoreFactory, [])) as ERC1155StoreGeneric;

    await genericStore.setModerator(await alice_moderator.getAddress());
    await genericStore.connect(alice_moderator).setSwapManager(swapManager.address);
    await genericStore.connect(alice_moderator).setOracleManager(oracle.address);

    await toyNFT1.setTrusted(genericStore.address, true);
    await toyNFT2.setTrusted(genericStore.address, true);
    await toyNFT3.setTrusted(genericStore.address, true);
  
    // await toyNFT1.setTokenUri(1, 'TOY 1');
    // await toyNFT2.setTokenUri(1, 'TOY 2');
    // await toyNFT3.setTokenUri(1, 'TOY 3');
  });

  describe.only('Create sale', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let adminSuppliedSale;

    let zeroAddress = ethers.constants.AddressZero;

    let numPurchases = 1;
    it("Should increment saleId's by one", async function () {
      let saleId1 = await genericStore.callStatic.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      await genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);

      let saleId2 = await genericStore.callStatic.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      expect(saleId2).to.equal(saleId1.toNumber() + 1);
    });
    it('Should emit event', async function () {
      let tx = await genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      expect(tx).to.emit(genericStore, 'SaleCreated');
    });
    it('Creates admin supplied sale without issue', async () => {
      await toyNFT2.mint(lisa_donor.address, tokenId, totalUnitSupply * unitSize);
      await toyNFT2.connect(lisa_donor).setApprovalForAll(genericStore.address, true);
      await genericStore.setDonor(lisa_donor.address);

      console.log(`donor balance before : ${await toyNFT2.balanceOf(lisa_donor.address, tokenId)}`);
      console.log(`contract balance before: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);

      adminSuppliedSale = await genericStore.callStatic.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);
      await genericStore.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);

      const donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      const contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);
      console.log(`donor balance after : ${donor_balance_after}`);
      console.log(`contract balance after: ${contract_balance_after}`);
      expect(donor_balance_after).to.equal(0);
      expect(contract_balance_after).to.equal(totalUnitSupply * unitSize);
    });
  });

  describe('Create sale reverts', function () {
    it('Should revert on non moderator trying to create sale', async function () {
      await expect(genericStore.connect(bob).createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false)).to.revertedWith(
        `SG__NotGov("${bob.address}")`,
      );
    });
    it('Should revert on zero sale cap', async function () {
      await expect(genericStore.createSale(toyNFT1.address, 1, 1, 0, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false)).to.revertedWith(`SG__ZeroSaleCap()`);
    });
    it('Should revert on zero user sale cap', async function () {
      await expect(genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 0, jourdanCoin.address, true, false)).to.revertedWith(`SG__ZeroUserSaleCap()`);
    });
    it('Should revert on zero unit size', async () => {
      await expect(genericStore.createSale(toyNFT1.address, 1, 0, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false)).to.revertedWith(`SG__ZeroUnitSize()`);
    });
    // it('Should revert on non initialised token id', async function () {
    //   let noURIToken: ERC1155MockAssetManager;
    //   const noURITokenFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    //   noURIToken = await noURITokenFactory.deploy('TOY URI');

    //   await expect(genericStore.createSale(noURIToken.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false)).to.revertedWith(`SG__TokenNotSet(1)`);
    // });
  });

  describe('Delete sale', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let adminSuppliedSale;

    let zeroAddress = ethers.constants.AddressZero;

    let numPurchases = 1;
    let saleId;
    beforeEach(async () => {
      saleId = await genericStore.callStatic.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      await genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
    });
    it('Should emit event on deletion', async function () {
      let tx = await genericStore.deleteSale(saleId.toString());
    });
    it("Should return donor's tokens when sale deleted", async () => {
      await toyNFT2.mint(lisa_donor.address, tokenId, totalUnitSupply * unitSize);
      await toyNFT2.connect(lisa_donor).setApprovalForAll(genericStore.address, true);
      await genericStore.setDonor(lisa_donor.address);

      console.log(`donor balance before : ${await toyNFT2.balanceOf(lisa_donor.address, tokenId)}`);
      console.log(`contract balance before: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);

      adminSuppliedSale = await genericStore.callStatic.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);
      await genericStore.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);

      let donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      let contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);
      console.log(`donor balance after : ${donor_balance_after}`);
      console.log(`contract balance after: ${contract_balance_after}`);
      expect(donor_balance_after).to.equal(0);
      expect(contract_balance_after).to.equal(totalUnitSupply * unitSize);

      await genericStore.deleteSale(adminSuppliedSale);

      donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);
      expect(donor_balance_after).to.equal(10);
      expect(contract_balance_after).to.equal(0);
    });
  });

  describe('Delete sale reverts', function () {
    let saleId;
    beforeEach(async () => {
      saleId = await genericStore.callStatic.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      await genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
    });
    it('Should revert on non moderator call', async function () {
      await expect(genericStore.connect(bob).deleteSale(saleId)).to.revertedWith(`SG__NotGov("${bob.address}")`);
    });
    it('Should revert on invalid sale id', async function () {
      await expect(genericStore.deleteSale(99)).to.revertedWith(`SG__NonExistentSale(99)`);
    });
    it('Should revert on deleted sale id', async function () {
      let tx = await genericStore.deleteSale(saleId.toString());
      await expect(genericStore.deleteSale(saleId.toString())).to.revertedWith(`SG__NonExistentSale(${saleId})`);
    });
  });

  describe('Modify sale', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let adminSuppliedSale;

    let zeroAddress = ethers.constants.AddressZero;

    let numPurchases = 1;
    let saleId;
    beforeEach(async () => {
      saleId = await genericStore.callStatic.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      await genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
    });
    it('Should emit event when modifying sale', async function () {
      let tx = await genericStore.modifySale(saleId, 1, 20, ethers.utils.parseEther('20'), 10, jourdanCoin.address, false);
      expect(tx).to.emit(genericStore, 'SaleModified');
    });
    it('Should handle modifying an admin supplied sale correctly (SUPPLY INCREASE)', async () => {
      await toyNFT2.mint(lisa_donor.address, tokenId, 1000);
      await toyNFT2.connect(lisa_donor).setApprovalForAll(genericStore.address, true);
      await genericStore.setDonor(lisa_donor.address);
      console.log(`-------------------------------------------------------------------------------------`);
      console.log(`donor balance before sale create: ${await toyNFT2.balanceOf(lisa_donor.address, tokenId)}`);
      console.log(`contract balance before sale create: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);
      console.log(`-------------------------------------------------------------------------------------`);

      adminSuppliedSale = await genericStore.callStatic.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);
      await genericStore.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);

      let donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      let contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);

      console.log(`donor balance after sale create: ${donor_balance_after}`);
      console.log(`contract balance after sale create: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - unitSize * totalUnitSupply);
      expect(contract_balance_after).to.equal(totalUnitSupply * unitSize);

      let newSize = 5;
      let newSupply = 30;
      await genericStore.modifySale(adminSuppliedSale, newSize, newSupply, price, userCap, jourdanCoin.address, profitState);

      let newBalance = newSize * newSupply;
      donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);

      console.log(`donor balance after modify: ${donor_balance_after}`);
      console.log(`contract balance after modify: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - newBalance);
      expect(contract_balance_after).to.equal(newBalance);
    });
    it('Should handle modifying an admin supplied sale correctly (SUPPLY DECREASE)', async () => {
      await toyNFT2.mint(lisa_donor.address, tokenId, 1000);
      await toyNFT2.connect(lisa_donor).setApprovalForAll(genericStore.address, true);
      await genericStore.setDonor(lisa_donor.address);
      console.log(`-------------------------------------------------------------------------------------`);
      console.log(`donor balance before sale create: ${await toyNFT2.balanceOf(lisa_donor.address, tokenId)}`);
      console.log(`contract balance before sale create: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);
      console.log(`-------------------------------------------------------------------------------------`);

      adminSuppliedSale = await genericStore.callStatic.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);
      await genericStore.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);

      let donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      let contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);
      console.log(`donor balance after sale create: ${donor_balance_after}`);
      console.log(`contract balance after sale create: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - unitSize * totalUnitSupply);
      expect(contract_balance_after).to.equal(totalUnitSupply * unitSize);

      let newSize = 1;
      let newSupply = 5;
      await genericStore.modifySale(adminSuppliedSale, newSize, newSupply, price, userCap, jourdanCoin.address, profitState);

      let newBalance = newSize * newSupply;
      donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);

      console.log(`donor balance after modify: ${donor_balance_after}`);
      console.log(`contract balance after modify: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - newBalance);
      expect(contract_balance_after).to.equal(newBalance);
    });
    it('Should handle modifying sale after purchases have been made', async () => {
      await toyNFT2.mint(lisa_donor.address, tokenId, 1000);
      await toyNFT2.connect(lisa_donor).setApprovalForAll(genericStore.address, true);
      await genericStore.setDonor(lisa_donor.address);
      console.log(`-------------------------------------------------------------------------------------`);
      console.log(`donor balance before sale create: ${await toyNFT2.balanceOf(lisa_donor.address, tokenId)}`);
      console.log(`contract balance before sale create: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);
      console.log(`-------------------------------------------------------------------------------------`);

      adminSuppliedSale = await genericStore.callStatic.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);
      await genericStore.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);

      let donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      let contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);
      console.log(`donor balance after sale create: ${donor_balance_after}`);
      console.log(`contract balance after sale create: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - unitSize * totalUnitSupply);
      expect(contract_balance_after).to.equal(totalUnitSupply * unitSize);

      let purchasesMade = 3;
      await genericStore.setSaleState(adminSuppliedSale, false);
      await jourdanCoin.mint(bob.address, 10000);
      await jourdanCoin.connect(bob).approve(genericStore.address, 10000);
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, purchasesMade, adminSuppliedSale, jourdanCoin.address, false, false);

      let newSize = 1;
      let newSupply = 60;
      await genericStore.modifySale(adminSuppliedSale, newSize, newSupply, price, userCap, jourdanCoin.address, profitState);

      let newBalance = newSize * newSupply;
      donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);

      console.log(`donor balance after modify: ${donor_balance_after}`);
      console.log(`contract balance after modify: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - newBalance - purchasesMade);
      expect(contract_balance_after).to.equal(newBalance);
    });
  });

  describe('Modify sale reverts', function () {
    let saleId;
    beforeEach(async () => {
      saleId = await genericStore.callStatic.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      await genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
    });
    it('Should revert on non moderator call', async function () {
      await expect(genericStore.connect(bob).modifySale(saleId, 1, 20, ethers.utils.parseEther('20'), 10, jourdanCoin.address, false)).to.revertedWith(
        `SG__NotGov("${bob.address}")`,
      );
    });
    it('Should revert on invalid sale id', async function () {
      await expect(genericStore.modifySale(99, 1, 20, ethers.utils.parseEther('20'), 10, jourdanCoin.address, false)).to.revertedWith(`SG__NonExistentSale(99)`);
    });

    it('Should revert on deleted sale', async function () {
      await genericStore.deleteSale(saleId.toString());
      await expect(genericStore.modifySale(saleId, 1, 20, ethers.utils.parseEther('20'), 10, jourdanCoin.address, false)).to.revertedWith(`SG__NonExistentSale(${saleId})`);
    });

    it('Should revert on zero cap', async function () {
      await expect(genericStore.modifySale(saleId, 1, 0, ethers.utils.parseEther('20'), 10, jourdanCoin.address, false)).to.revertedWith(`SG__ZeroSaleCap()`);
    });
    it('Should revert on zero user cap', async function () {
      await expect(genericStore.modifySale(saleId, 1, 20, ethers.utils.parseEther('20'), 0, jourdanCoin.address, false)).to.revertedWith(`SG__ZeroUserSaleCap()`);
    });
  });

  describe('Set Sale State ', function () {
    let saleId;
    beforeEach(async () => {
      saleId = await genericStore.callStatic.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      await genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
    });
    it('Should emit event when updating sale state', async function () {
      let tx = await genericStore.setSaleState(saleId, false);
      expect(tx).to.emit(genericStore, 'SaleStateSet');
    });
  });

  describe('Set Sale State reverts', function () {
    let saleId;
    beforeEach(async () => {
      saleId = await genericStore.callStatic.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
      await genericStore.createSale(toyNFT1.address, 1, 1, 10, ethers.utils.parseEther('10'), 2, jourdanCoin.address, true, false);
    });
    it('Should revert on non moderator call', async function () {
      await expect(genericStore.connect(bob).setSaleState(saleId, false)).to.revertedWith(`SG__NotGov("${bob.address}")`);
    });
    it('Should revert on invalid sale id', async function () {
      await expect(genericStore.setSaleState(99, false)).to.revertedWith(`SG__NonExistentSale(${99})`);
    });
    it('Should revert on deleted sale', async function () {
      await genericStore.deleteSale(saleId);
      await expect(genericStore.setSaleState(saleId, false)).to.revertedWith(`SG__NonExistentSale(${saleId})`);
    });
  });

  describe('Buy token', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let adminSuppliedSale;

    let zeroAddress = ethers.constants.AddressZero;

    let numPurchases = 1;
    let saleBonuses: SaleBonuses;
    let assets: ERC1155MockAssetManager;
    let toyNFT: ERC1155MockAssetManager;
    let erc721Mock: ERC721Mock;

    let vrfCoordinatorV2Mock: Contract;
    let vrfManager: VRFManager;
    const SUBSCRIPTION_ID = 1;
    const KEYHASH = ethers.utils.defaultAbiCoder.encode(['uint256'], [1]);
    const CALLBACK_GAS_LIMIT = 2_500_000;
    const REQUEST_CONFIRMATIONS = 3;
    const NUM_WORDS = 1;

    let saleId = 1;
    let tokens;
    let categoryId1;
    let categoryId2;
    let categoryId3;
    let predetermined_draws;
    let maxDraws = 10;
    let conditionalBonusFactory;
    let conditionalBonusProvider;

    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      maxCoinSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.setSaleState(maxCoinSale, false);

      maticSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(maticSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, 10000);
      await maxCoin.mint(bob.address, 10000);

      await jourdanCoin.connect(bob).approve(genericStore.address, 10000);
      await maxCoin.connect(bob).approve(genericStore.address, 10000);

      let VRFCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
      vrfCoordinatorV2Mock = await VRFCoordinatorV2MockFactory.deploy(0, 6);
      await vrfCoordinatorV2Mock.createSubscription();
      await vrfCoordinatorV2Mock.fundSubscription(SUBSCRIPTION_ID, ethers.utils.parseEther('1000000'));

      let VRFManagerFactory = await ethers.getContractFactory('VRFManager');
      vrfManager = await VRFManagerFactory.deploy(vrfCoordinatorV2Mock.address);

      let assetFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
      assets = await assetFactory.deploy('toy uri');

      let erc721Factory = await ethers.getContractFactory('ERC721Mock');
      erc721Mock = await erc721Factory.deploy('TOYTOKEN', 'TOY');

      let saleBonusesFactory = await ethers.getContractFactory('SaleBonuses');
      saleBonuses = (await upgrades.deployProxy(saleBonusesFactory, [])) as SaleBonuses;

      await saleBonuses.setVRFOracle(vrfManager.address);

      await assets.setTrusted(saleBonuses.address, true);
      await erc721Mock.setTrusted(saleBonuses.address, true);

      await vrfManager.setConsumer(saleBonuses.address, SUBSCRIPTION_ID, KEYHASH, CALLBACK_GAS_LIMIT, REQUEST_CONFIRMATIONS, NUM_WORDS);

      tokens = [assets.address, assets.address, assets.address];

      predetermined_draws = 10;
      await saleBonuses.setMaxDraws(genericStore.address, saleId, maxDraws);

      categoryId1 = await saleBonuses.callStatic.createContentCategory(genericStore.address, saleId);
      await saleBonuses.createContentCategory(genericStore.address, saleId);
      await saleBonuses.setContentAmounts(genericStore.address, saleId, categoryId1, [predetermined_draws, 2], [10, 10]);
      await saleBonuses.setContents(genericStore.address, saleId, categoryId1, { _tokens: [assets.address], _ids: [19], _amounts: [1], _weights: [1] });

      categoryId2 = await saleBonuses.callStatic.createContentCategory(genericStore.address, saleId);
      await saleBonuses.createContentCategory(genericStore.address, saleId);
      await saleBonuses.setContentAmounts(genericStore.address, saleId, categoryId2, [predetermined_draws, 4, 5], [50, 20, 10]);
      await saleBonuses.setContents(genericStore.address, saleId, categoryId2, {
        _tokens: [assets.address, assets.address, assets.address], // token addresses
        _ids: [1, 2, 3], // token Ids
        _amounts: [2, 3, 5], // token amounts
        _weights: [40, 20, 50], // token weights
      });

      categoryId3 = await saleBonuses.callStatic.createContentCategory(genericStore.address, saleId);
      await saleBonuses.createContentCategory(genericStore.address, saleId);
      await saleBonuses.setContentAmounts(genericStore.address, saleId, categoryId3, [5, predetermined_draws, 7], [225, 200, 200]);
      await saleBonuses.setContents(genericStore.address, saleId, categoryId3, { _tokens: [erc721Mock.address], _ids: [0], _amounts: [1], _weights: [1] });

      conditionalBonusFactory = await ethers.getContractFactory('MockConditionalProvider2');
      conditionalBonusProvider = await conditionalBonusFactory.deploy();
      await conditionalBonusProvider.setTrue(bob.address, true);

      await saleBonuses.setBonusEligibility(genericStore.address, saleId, conditionalBonusProvider.address);
      await saleBonuses.setTrusted(genericStore.address, true);
      await genericStore.setSaleBonuses(saleBonuses.address);
    });
    it('Allows Bob to buy his token', async function () {
      let tx = await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      let bobBalance = await toyNFT1.balanceOf(bob.address, numPurchases);
      console.log(bobBalance);
    });

    it('Allows Bob (via paper) to buy his token', async function () {
      let tx = await genericStore.connect(bob).onPaper(bob.address, ethers.constants.AddressZero, 0, numPurchases, jourdanCoinSale);
      let bobBalance = await toyNFT1.balanceOf(bob.address, 1);
      expect(bobBalance).to.equal(1);
    });

    it('Allows Bob to buy multiple tokens', async function () {
      let tx = await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, 3, jourdanCoinSale, jourdanCoin.address, false, false);
      let bobBalance = await toyNFT1.balanceOf(bob.address, 1);
      expect(bobBalance).to.equal(3);
    });

    it('Allows Bob (via paper) to buy multiple tokens', async function () {
      let tx = await genericStore.connect(bob).onPaper(bob.address, ethers.constants.AddressZero, 0, 3, jourdanCoinSale);
      let bobBalance = await toyNFT1.balanceOf(bob.address, 1);
      expect(bobBalance).to.equal(3);
    });

    it('Allows Bob to make multiple purchases without unexpected behaviour', async function () {
      let initBalance = await jourdanCoin.balanceOf(bob.address);
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);

      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(3);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.toNumber() - 3 * price);
    });

    it('Allows Bob (via paper) to make multiple purchases without unexpected behaviour', async function () {
      let initBalance = await jourdanCoin.balanceOf(bob.address);

      await genericStore.connect(bob).onPaper(bob.address, ethers.constants.AddressZero, 0, numPurchases, jourdanCoinSale);
      await genericStore.connect(bob).onPaper(bob.address, ethers.constants.AddressZero, 0, numPurchases, jourdanCoinSale);
      await genericStore.connect(bob).onPaper(bob.address, ethers.constants.AddressZero, 0, numPurchases, jourdanCoinSale);

      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(3);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.toNumber() - 3 * price);
    });

    it('Allows Bob to purchase with Matic', async function () {
      let initBalance = BigNumber.from(await bob.getBalance());
      let price = BigNumber.from(10);
      console.log(`Initial Balance: ${initBalance}`);
      let tx = await genericStore
        .connect(bob)
        .buyTokens(bob.address, zeroAddress, 0, 1, maticSale, ethers.constants.AddressZero, false, false, { value: ethers.utils.parseEther('1')} );
      const receipt = await tx.wait();
      const gasCost = BigNumber.from(receipt.effectiveGasPrice.mul(receipt.gasUsed));
      console.log(`Final Balance Calculated: ${initBalance.sub(gasCost.add(price))}`);

      let finalBalance = await bob.getBalance();
      console.log(`Balance after: ${finalBalance}`);
      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(1);
      expect(finalBalance).to.equal(initBalance.sub(gasCost.add(price)));
    });

    it('Allows Bob to purchase with Matic and gives him the correct change', async function () {
      let initBalance = BigNumber.from(await bob.getBalance());
      let price = BigNumber.from(10);
      console.log(`Initial Balance: ${initBalance}`);
      let tx = await genericStore
        .connect(bob)
        .buyTokens(bob.address, zeroAddress, 0, 1, maticSale, ethers.constants.AddressZero, false, false, { value: ethers.utils.parseEther('9999')} );
      const receipt = await tx.wait();
      const gasCost = BigNumber.from(receipt.effectiveGasPrice.mul(receipt.gasUsed));
      console.log(`Final Balance Calculated: ${initBalance.sub(gasCost.add(price))}`);

      let finalBalance = await bob.getBalance();
      console.log(`Balance after: ${finalBalance}`);
      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(1);
      expect(finalBalance).to.equal(initBalance.sub(gasCost.add(price)));
    });

    it('Allows Bob to purchase with jourdan coin in a max coin default currency sale', async function () {
      await genericStore.whitelistCurrencies(maxCoinSale, [jourdanCoin.address]);
      let tx = await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maxCoinSale, jourdanCoin.address, false, false);
      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(1);
    });

    it('Allows Bob to purchase with jourdan coin in a matic default currency sale', async function () {
      await genericStore.whitelistCurrencies(maticSale, [jourdanCoin.address]);
      let tx = await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maticSale, jourdanCoin.address, false, false);
      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(1);
    });

    it('Allows Bob to purchase with matic in a jourdan coin default currency sale', async function () {
      await genericStore.whitelistCurrencies(jourdanCoinSale, [zeroAddress]);
      let tx = await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, zeroAddress, false, false, { value: ethers.utils.parseEther('20') });
      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(1);
    });

    it("Retains the jourdan coin if it's supposed to", async function () {
      await genericStore.whitelistCurrencies(maxCoinSale, [jourdanCoin.address]);
      let tx = await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maxCoinSale, jourdanCoin.address, false, false);
      expect(await jourdanCoin.balanceOf(genericStore.address)).to.equal(price / 2);
    });

    it("Exchanges the jourdan coin for MaxCoins if it's supposed to", async function () {
      await genericStore.modifySale(maxCoinSale, numPurchases, totalUnitSupply, price, userCap, maxCoin.address, false);
      await genericStore.whitelistCurrencies(maxCoinSale, [jourdanCoin.address]);
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maxCoinSale, jourdanCoin.address, false, false);
      expect(await maxCoin.balanceOf(genericStore.address)).to.equal(price);
    });

    it('Buys token from admin-supplied sale', async () => {
      await toyNFT2.mint(lisa_donor.address, tokenId, 1000);
      await toyNFT2.connect(lisa_donor).setApprovalForAll(genericStore.address, true);
      await genericStore.setDonor(lisa_donor.address);
      console.log(`-------------------------------------------------------------------------------------`);
      console.log(`donor balance before sale create: ${await toyNFT2.balanceOf(lisa_donor.address, tokenId)}`);
      console.log(`contract balance before sale create: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);
      console.log(`-------------------------------------------------------------------------------------`);

      adminSuppliedSale = await genericStore.callStatic.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);
      await genericStore.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);

      let donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      let contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);
      console.log(`donor balance after sale create: ${donor_balance_after}`);
      console.log(`contract balance after sale create: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - unitSize * totalUnitSupply);
      expect(contract_balance_after).to.equal(totalUnitSupply * unitSize);

      let newSize = 3;
      let newSupply = 100;
      await genericStore.modifySale(adminSuppliedSale, newSize, newSupply, price, userCap, jourdanCoin.address, profitState);

      let newBalance = newSize * newSupply;
      donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);

      console.log(`donor balance after modify: ${donor_balance_after}`);
      console.log(`contract balance after modify: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - newBalance);
      expect(contract_balance_after).to.equal(newBalance);

      await genericStore.setSaleState(adminSuppliedSale, false);
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, 3, adminSuppliedSale, jourdanCoin.address, false, false);

      console.log(`Bob's balance: ${await toyNFT2.balanceOf(bob.address, tokenId)}`);
      console.log(`contract balance after purchase: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);

      expect(await toyNFT2.balanceOf(bob.address, tokenId)).to.equal(3 * newSize);
    });
    it.skip('Applies relevant bonuses when user opts in', async () => {
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, 3, jourdanCoinSale, jourdanCoin.address, true, false);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });

      console.log(`Bob balance from purchase: ${await toyNFT1.balanceOf(bob.address, 1)}`);
      await printBalances(bob, assets, erc721Mock);
    });
    it.skip('Applies relevant bonuses when user opts in for bonuses and conditional categories are present which user also opts into', async () => {
      let conditionalFactory = await ethers.getContractFactory('MockConditionalProvider');
      let conditionalProvider = await conditionalFactory.deploy();

      await saleBonuses.setContentEligibility(genericStore.address, saleId, categoryId1, conditionalProvider.address);
      await saleBonuses.setContentEligibility(genericStore.address, saleId, categoryId2, conditionalProvider.address);

      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, 3, jourdanCoinSale, jourdanCoin.address, true, true);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });

      console.log(`Bob balance from purchase: ${await toyNFT1.balanceOf(bob.address, 1)}`);
      await printBalances(bob, assets, erc721Mock);
    });
    it('Applies relevant bonuses when user opts in for bonuses and conditional categories are present which user does not opt into', async () => {
      let conditionalFactory = await ethers.getContractFactory('MockConditionalProvider');
      let conditionalProvider = await conditionalFactory.deploy();

      await saleBonuses.setContentEligibility(genericStore.address, saleId, categoryId1, conditionalProvider.address);
      await saleBonuses.setContentEligibility(genericStore.address, saleId, categoryId2, conditionalProvider.address);

      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, 3, jourdanCoinSale, jourdanCoin.address, true, false);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });

      console.log(`Bob balance from purchase: ${await toyNFT1.balanceOf(bob.address, 1)}`);
      await printBalances(bob, assets, erc721Mock);
    });
    it('Buys token from admin-supplied sale(ON PAPER)', async () => {
      await toyNFT2.mint(lisa_donor.address, tokenId, 1000);
      await toyNFT2.connect(lisa_donor).setApprovalForAll(genericStore.address, true);
      await genericStore.setDonor(lisa_donor.address);
      console.log(`-------------------------------------------------------------------------------------`);
      console.log(`donor balance before sale create: ${await toyNFT2.balanceOf(lisa_donor.address, tokenId)}`);
      console.log(`contract balance before sale create: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);
      console.log(`-------------------------------------------------------------------------------------`);

      adminSuppliedSale = await genericStore.callStatic.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);
      await genericStore.createSale(toyNFT2.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, true);

      let donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      let contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);
      console.log(`donor balance after sale create: ${donor_balance_after}`);
      console.log(`contract balance after sale create: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - unitSize * totalUnitSupply);
      expect(contract_balance_after).to.equal(totalUnitSupply * unitSize);

      let newSize = 3;
      let newSupply = 100;
      await genericStore.modifySale(adminSuppliedSale, newSize, newSupply, price, userCap, jourdanCoin.address, profitState);

      let newBalance = newSize * newSupply;
      donor_balance_after = await toyNFT2.balanceOf(lisa_donor.address, tokenId);
      contract_balance_after = await toyNFT2.balanceOf(genericStore.address, tokenId);

      console.log(`donor balance after modify: ${donor_balance_after}`);
      console.log(`contract balance after modify: ${contract_balance_after}`);
      console.log(`-------------------------------------------------------------------------------------`);

      expect(donor_balance_after).to.equal(1000 - newBalance);
      expect(contract_balance_after).to.equal(newBalance);

      await genericStore.setSaleState(adminSuppliedSale, false);
      await genericStore.onPaper(bob.address, zeroAddress, 0, 3, adminSuppliedSale);

      console.log(`Bob's balance: ${await toyNFT2.balanceOf(bob.address, tokenId)}`);
      console.log(`contract balance after purchase: ${await toyNFT2.balanceOf(genericStore.address, tokenId)}`);

      expect(await toyNFT2.balanceOf(bob.address, tokenId)).to.equal(3 * newSize);
    });
    it('Applies relevant bonuses when user opts in (ON PAPER)', async () => {
      await genericStore.onPaper(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });

      console.log(`Bob balance from purchase: ${await toyNFT1.balanceOf(bob.address, 1)}`);
      await printBalances(bob, assets, erc721Mock);
    });
    it('Applies relevant bonuses when user opts in for bonuses and conditional categories are present which user also opts into (ON PAPER)', async () => {
      let conditionalFactory = await ethers.getContractFactory('MockConditionalProvider');
      let conditionalProvider = await conditionalFactory.deploy();

      await saleBonuses.setContentEligibility(genericStore.address, saleId, categoryId1, conditionalProvider.address);
      await saleBonuses.setContentEligibility(genericStore.address, saleId, categoryId2, conditionalProvider.address);

      await genericStore.onPaper(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });

      console.log(`Bob balance from purchase: ${await toyNFT1.balanceOf(bob.address, 1)}`);
      await printBalances(bob, assets, erc721Mock);
    });
    it('Applies relevant bonuses when user opts in for bonuses and conditional categories are present which user does not opt into (ON PAPER)', async () => {
      let conditionalFactory = await ethers.getContractFactory('MockConditionalProvider');
      let conditionalProvider = await conditionalFactory.deploy();

      await saleBonuses.setContentEligibility(genericStore.address, saleId, categoryId1, conditionalProvider.address);
      await saleBonuses.setContentEligibility(genericStore.address, saleId, categoryId2, conditionalProvider.address);

      await genericStore.onPaper(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });

      console.log(`Bob balance from purchase: ${await toyNFT1.balanceOf(bob.address, 1)}`);
      await printBalances(bob, assets, erc721Mock);
    });
  });

  describe('Buy token reverts', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;
    let nonWhitelistToken;
    let zeroAddress = ethers.constants.AddressZero;
    let numPurchases = 1;

    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, 10000);
      await maxCoin.mint(bob.address, 10000);
      await jourdanCoin.connect(bob).approve(genericStore.address, 10000);
      await maxCoin.connect(bob).approve(genericStore.address, 10000);
    });
    it('Should revert on sale inactive', async function () {
      await genericStore.setSaleState(jourdanCoinSale, true);
      await expect(genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false)).to.revertedWith(`SG__SaleInactive()`);
    });
    it('Should revert on invalid sale id', async function () {
      await expect(genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, 99, jourdanCoin.address, false, false)).to.revertedWith(`SG__NonExistentSale(${99})`);
    });

    it('Should revert on deleted sale', async function () {
      await genericStore.deleteSale(jourdanCoinSale);
       await expect(genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false)).to.revertedWith(
        `SG__NonExistentSale(${jourdanCoinSale})`,
      );
    });

    it('Should revert on exceeding per player cap', async function () {
      await expect(genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, 10, jourdanCoinSale, jourdanCoin.address, false, false)).to.revertedWith(`SG__PurchaseExceedsPlayerMax`);
    });

    it('Should revert on exceeding total cap', async function () {
      jourdanCoin.mint(alice_moderator.address, 10000);
      jourdanCoin.mint(allen_beneficiary.address, 10000);
      jourdanCoin.connect(alice_moderator).approve(genericStore.address, 10000);
      jourdanCoin.connect(allen_beneficiary).approve(genericStore.address, 10000);
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, 5, jourdanCoinSale, jourdanCoin.address, false, false);
      await genericStore.connect(alice_moderator).buyTokens(alice_moderator.address, zeroAddress, 0, 5, jourdanCoinSale, jourdanCoin.address, false, false);
      await expect(genericStore.connect(allen_beneficiary).buyTokens(allen_beneficiary.address, zeroAddress, 0, 5, jourdanCoinSale, jourdanCoin.address, false, false)).to.revertedWith(
        `SG__PurchaseExceedsTotalMax()`,
      );
    });

    it('Should revert on non whitelisted currency', async function () {
      const nonWhitelistTokenFactory = await ethers.getContractFactory('ERC20Mock');
      nonWhitelistToken = await nonWhitelistTokenFactory.deploy('Nonwhitelisttoken', 'NWT');
      nonWhitelistToken.mint(bob.address, ethers.utils.parseEther('10000'));
      nonWhitelistToken.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));
      await expect(genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, nonWhitelistToken.address, false, false)).to.revertedWith(
        `SG__CurrencyNotWhitelisted("${nonWhitelistToken.address}")`,
      );
    });
    it('Should revert on insufficient value sent when paying with MATIC', async function () {
      await genericStore.whitelistCurrencies(jourdanCoinSale, [zeroAddress]);
      await expect(
        genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, zeroAddress, false, false, { value: ethers.utils.parseEther('0') })
      ).to.revertedWith(`SG__InsufficientEthValue(0, ${price * 10})`);
    });
  });

  describe('Fee wallets and percentages', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let zeroAddress = ethers.constants.AddressZero;

    let numPurchases = 1;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, 10000);
      await maxCoin.mint(bob.address, 10000);

      await jourdanCoin.connect(bob).approve(genericStore.address, 10000);
      await maxCoin.connect(bob).approve(genericStore.address, 10000);
    });
    it('Should allow setting the wallets and percentages', async function () {
      await expect(genericStore.setFeeWalletsAndPercentages([cindy_beneficiary.address, elise_beneficiary.address, allen_beneficiary.address], [200, 300, 400])).to.not.reverted;
    });
    it('Should give beneficiaries expected tokens', async function () {
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      expect(await jourdanCoin.balanceOf(cindy_beneficiary.address)).to.equal(0);
      expect(await jourdanCoin.balanceOf(elise_beneficiary.address)).to.equal(0);
      expect(await jourdanCoin.balanceOf(allen_beneficiary.address)).to.equal(0);
    });
    it('Should overwrite previous beneficiaries on repeated calls', async function () {
      let tx = await genericStore.setFeeWalletsAndPercentages([cindy_beneficiary.address, elise_beneficiary.address, allen_beneficiary.address], [200, 300, 400]);
      await expect(genericStore.setFeeWalletsAndPercentages([owner.address, bob.address], [400, 500]));
    });
  });

  describe('Fee wallets and percentages reverts', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, 10000);
      await maxCoin.mint(bob.address, 10000);

      await jourdanCoin.connect(bob).approve(genericStore.address, 10000);
      await maxCoin.connect(bob).approve(genericStore.address, 10000);
    });
    it('Should revert on non owner call', async function () {
      await expect(
        genericStore.connect(bob).setFeeWalletsAndPercentages([elise_beneficiary.address, cindy_beneficiary.address, allen_beneficiary.address], [400, 300, 200]),
      ).to.revertedWith(`SG__NotGov("${bob.address}")`);
    });

    it('Should revert on sum exceeding 100%', async function () {
      await expect(genericStore.setFeeWalletsAndPercentages([elise_beneficiary.address, cindy_beneficiary.address, allen_beneficiary.address], [5000, 6000, 6000])).to.revertedWith(
        `SG__ValueTooLarge(${5000 + 6000 + 6000})`,
      );
    });

    it('Should revert on singular entry exceeding 100%', async function () {
      await expect(genericStore.setFeeWalletsAndPercentages([elise_beneficiary.address, cindy_beneficiary.address, allen_beneficiary.address], [10001, 60, 60])).to.revertedWith(
        `SG__ValueTooLarge(${10001 + 60 + 60})`,
      );
    });
  });

  describe.skip('Bulk Discounts', async function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let zeroAddress = ethers.constants.AddressZero;

    let numPurchases = 1;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      maxCoinSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.setSaleState(maxCoinSale, false);

      maticSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(maticSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, 10000);
      await maxCoin.mint(bob.address, 10000);

      await jourdanCoin.connect(bob).approve(genericStore.address, 10000);
      await maxCoin.connect(bob).approve(genericStore.address, 10000);
    });
    it('Should allow the moderator to add a bulk rebate', async function () {
      let tx = await genericStore.addBulkDiscount(jourdanCoinSale, 3, 200);
      expect(tx).to.emit(genericStore, 'BulkDiscountAdded');
    });

    it('The bulk rebate should give the expected discount', async function () {
      let tx = await genericStore.addBulkDiscount(jourdanCoinSale, 3, 200);
      let initBalance = await jourdanCoin.balanceOf(bob.address);
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, 3, jourdanCoinSale, jourdanCoin.address, false, false);

      let effectivePrice = Math.floor((3 * (price * (10000 - 200))) / 10000);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(effectivePrice));
    });

    it('bulk rebate should give the expected discount when paying with matic', async function () {
      await genericStore.addBulkDiscount(maticSale, 3, 200);

      let initBalance = BigNumber.from(await bob.getBalance());
      console.log(`Initial Balance: ${initBalance}`);
      let tx = await genericStore
        .connect(bob)
        .buyTokens(bob.address, zeroAddress, 0, 3, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('1') });
      const receipt = await tx.wait();
      const gasCost = BigNumber.from(receipt.effectiveGasPrice.mul(receipt.gasUsed));
      console.log(`Final Balance Calculated: ${initBalance.sub(gasCost.add(price))}`);

      let finalBalance = await bob.getBalance();
      console.log(`Balance after: ${finalBalance}`);
      let effectivePrice = Math.floor((3 * (price * (10000 - 200))) / 10000);

      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(3);
      expect(finalBalance).to.equal(initBalance.sub(gasCost.add(effectivePrice)));
    });

    it('Adding subsequent Discounts at same breakpoint should result in overwriting', async function () {
      await genericStore.addBulkDiscount(maticSale, 3, 200);
      await genericStore.addBulkDiscount(maticSale, 3, 500);

      let initBalance = BigNumber.from(await bob.getBalance());
      console.log(`Initial Balance: ${initBalance}`);
      let tx = await genericStore
        .connect(bob)
        .buyTokens(bob.address, zeroAddress, 0, 3, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('1') });
      const receipt = await tx.wait();
      const gasCost = BigNumber.from(receipt.effectiveGasPrice.mul(receipt.gasUsed));
      console.log(`Final Balance Calculated: ${initBalance.sub(gasCost.add(price))}`);

      let finalBalance = await bob.getBalance();
      console.log(`Balance after: ${finalBalance}`);
      let effectivePrice = Math.floor((3 * (price * (10000 - 500))) / 10000);

      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(3);
      expect(finalBalance).to.equal(initBalance.sub(gasCost.add(effectivePrice)));
    });
    it('should count Discounts cumulatively', async function () {
      await genericStore.addBulkDiscount(maticSale, 2, 200);
      await genericStore.addBulkDiscount(maticSale, 3, 500);
      await genericStore.addBulkDiscount(maticSale, 4, 700);

      let initBalance = BigNumber.from(await bob.getBalance());
      console.log(`Initial Balance: ${initBalance}`);
      let tx = await genericStore
        .connect(bob)
        .buyTokens(bob.address, zeroAddress, 0, 4, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('1') });
      const receipt = await tx.wait();
      const gasCost = BigNumber.from(receipt.effectiveGasPrice.mul(receipt.gasUsed));
      console.log(`Final Balance Calculated: ${initBalance.sub(gasCost.add(price))}`);

      let finalBalance = await bob.getBalance();
      console.log(`Balance after: ${finalBalance}`);
      let effectivePrice = Math.floor((4 * (price * (10000 - 200 - 500 - 700))) / 10000);

      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(4);
      expect(finalBalance).to.equal(initBalance.sub(gasCost.add(effectivePrice)));
    });
  });

  describe.skip('Bulk Discounts reverts', async function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let zeroAddress = ethers.constants.AddressZero;

    let numPurchases = 1;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      maxCoinSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.setSaleState(maxCoinSale, false);

      maticSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(maticSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, 10000);
      await maxCoin.mint(bob.address, 10000);

      await jourdanCoin.connect(bob).approve(genericStore.address, 10000);
      await maxCoin.connect(bob).approve(genericStore.address, 10000);
    });
    it('Should not allow non moderator to add a bulk rebate', async function () {
      await genericStore.setModerator(alice_moderator.address);
      await expect(genericStore.connect(alice_moderator).addBulkDiscount(jourdanCoinSale, 2, 200)).to.revertedWith('Ownable: caller is not the owner');
    });

    it('Should not allow adding a sale for an invalid saleId', async function () {
      await expect(genericStore.addBulkDiscount(99, 4, 200)).to.revertedWith(`SG__NonExistentSale(99)`);
    });
    it('Should not allow adding a sale for a deleted sale', async function () {
      await genericStore.deleteSale(jourdanCoinSale);
      await expect(genericStore.addBulkDiscount(jourdanCoinSale, 4, 200)).to.revertedWith(`SG__NonExistentSale(${jourdanCoinSale})`);
    });
    it('Should not allow discount breakpoint to exceed per player cap', async function () {
      await expect(genericStore.addBulkDiscount(jourdanCoinSale, 60, 200)).to.revertedWith(`SG__ValueTooLarge(60)`);
    });
    it('Should not allow discount amount to be exceed 100% ', async function () {
      await expect(genericStore.addBulkDiscount(jourdanCoinSale, 4, 10000)).to.revertedWith(`SG__DiscountTooLarge(10000, 10000)`);
    });
    it('Should not allow cumulative discount amount to exceed 100% ', async function () {
      genericStore.addBulkDiscount(jourdanCoinSale, 2, 200);
      genericStore.addBulkDiscount(jourdanCoinSale, 3, 200);
      genericStore.addBulkDiscount(jourdanCoinSale, 4, 200);
      await expect(genericStore.addBulkDiscount(jourdanCoinSale, 5, 10001)).to.revertedWith(`SG__DiscountTooLarge(${200 + 200 + 200 + 10001}, 10000)`);
    });

    it('Should not revert when overwriting same sale multiple times', async function () {
      genericStore.addBulkDiscount(jourdanCoinSale, 2, 200);
      genericStore.addBulkDiscount(jourdanCoinSale, 3, 200);
      genericStore.addBulkDiscount(jourdanCoinSale, 4, 200);
    });
  });

  describe.skip('Ownership Discounts', async function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let zeroAddress = ethers.constants.AddressZero;
    let nonWhitelistToken: ERC20Mock;
    let discount1155NFT: ERC1155MockAssetManager;
    let discount721NFT: ERC721Mock;

    const discount1 = 1000;
    const discount2 = 2000;

    const bob_mint = 10000;

    let numPurchases = 1;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      maxCoinSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.setSaleState(maxCoinSale, false);

      maticSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(maticSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, ethers.utils.parseEther('10000'));
      await maxCoin.mint(bob.address, ethers.utils.parseEther('10000'));

      await jourdanCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));
      await maxCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));

      const discountTokenFactory1155 = await ethers.getContractFactory('ERC1155MockAssetManager');
      discount1155NFT = await discountTokenFactory1155.deploy('DISCOUNT TOKEN');

      const discountTokenFactory721 = await ethers.getContractFactory('ERC721Mock');
      discount721NFT = await discountTokenFactory721.deploy('MOCK721', 'M721');
    });
    it('Should allow the moderator to add ownership rebate', async function () {
      let tx = await genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 1, tokenAddress: discount1155NFT.address, tokenId: 1, basisPoints: 1000 });
      expect(tx).to.emit(genericStore, 'OwnershipDiscountAdded');
    });

    it('Should apply rebate when owning the correct erc 721 token', async function () {
      let initBalance = await jourdanCoin.balanceOf(bob.address);
      await genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 0, tokenAddress: discount721NFT.address, tokenId: 0, basisPoints: 1000 });
      await discount721NFT.mint(bob.address, 1);
      await genericStore.connect(bob).buyTokens(bob.address, discount721NFT.address, 0, 3, jourdanCoinSale, jourdanCoin.address, false, false);
      let effectivePrice = Math.floor((price * (10000 - 1000)) / 10000);

      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(effectivePrice));
    });

    it('Should give the expected ownership discount when paying with matic', async function () {
      await genericStore.addOwnershipDiscount(maticSale, { tokenType: 0, tokenAddress: discount721NFT.address, tokenId: 0, basisPoints: 1000 });
      await discount721NFT.mint(bob.address, 1);

      let initBalance = await bob.getBalance();

      let tx = await genericStore
        .connect(bob)
        .buyTokens(bob.address, discount721NFT.address, 0, 1, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('1') });
      const receipt = await tx.wait();

      const gasCost = BigNumber.from(receipt.effectiveGasPrice.mul(receipt.gasUsed));
      let effectivePrice = Math.floor((price * (10000 - 1000)) / 10000);

      let finalBalance = await bob.getBalance();

      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.equal(1);
      expect(finalBalance).to.equal(initBalance.sub(gasCost.add(effectivePrice)));
    });

    it('Should override previous rebate if rebate is added to same token address (ERC721)', async function () {
      let initBalance = await jourdanCoin.balanceOf(bob.address);
      await genericStore.addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 0, // ERC 721
        tokenAddress: discount721NFT.address,
        tokenId: 0,
        basisPoints: 1000,
      });

      await genericStore.addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 0, // ERC 721
        tokenAddress: discount721NFT.address,
        tokenId: 0,
        basisPoints: 2000,
      });
      await discount721NFT.mint(bob.address, 1);

      await genericStore.connect(bob).buyTokens(bob.address, discount721NFT.address, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      let effectivePrice = Math.floor((price * (10000 - 2000)) / 10000);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(effectivePrice));
    });

    it('Should apply rebate when owning the correct erc 1155 token', async function () {
      const initBalance = await jourdanCoin.balanceOf(bob.address);
      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 1,
        basisPoints: 1000,
      });

      await discount1155NFT.mint(bob.address, 1, 1);
      await genericStore.connect(bob).buyTokens(bob.address, discount1155NFT.address, 1, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      let effectivePrice = Math.floor((price * (10000 - 1000)) / 10000);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(effectivePrice));
    });

    it('Should apply rebate when owning the correct erc 1155 token and there are multiple Discounts on the same contract', async function () {
      const initBalance = await jourdanCoin.balanceOf(bob.address);
      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 1,
        basisPoints: 1000,
      });
      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 2,
        basisPoints: 3000,
      });
      await discount1155NFT.mint(bob.address, 1, 1);

      await genericStore.connect(bob).buyTokens(bob.address, discount721NFT.address, 1, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      let effectivePrice = Math.floor((price * (10000 - 1000)) / 10000);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(effectivePrice));
    });

    it('Should apply rebate when owning the correct erc 1155 token and there are multiple Discounts on the same contract and there are erc721 Discounts', async function () {
      const initBalance = await jourdanCoin.balanceOf(bob.address);
      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 1,
        basisPoints: BigNumber.from(discount1),
      });
      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 2,
        basisPoints: BigNumber.from(discount2),
      });
      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 0, // ERC 721
        tokenAddress: discount721NFT.address,
        tokenId: 0,
        basisPoints: BigNumber.from(discount2),
      });
      await discount1155NFT.mint(bob.address, 1, 1);

      await genericStore.connect(bob).buyTokens(bob.address, discount1155NFT.address, 1, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      let effectivePrice = Math.floor((price * (10000 - 1000)) / 10000);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(effectivePrice));
    });

    it('Should apply rebate when owning the correct erc 721 token and there are multiple erc 1155 Discounts', async function () {
      const initBalance = await jourdanCoin.balanceOf(bob.address);

      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 1,
        basisPoints: 2000,
      });
      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 2,
        basisPoints: 3000,
      });
      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 0, // ERC 721
        tokenAddress: discount721NFT.address,
        tokenId: 0,
        basisPoints: 1000,
      });

      await discount721NFT.mint(bob.address, 1);

      await genericStore.connect(bob).buyTokens(bob.address, discount721NFT.address, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      let effectivePrice = Math.floor((price * (10000 - 1000)) / 10000);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(effectivePrice));
    });

    it('Should override previous rebate if rebate is added to same token address + token id (ERC1155)', async function () {
      const initBalance = await jourdanCoin.balanceOf(bob.address);

      await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 1,
        basisPoints: 2000,
      });
      let tx = await genericStore.connect(alice_moderator).addOwnershipDiscount(jourdanCoinSale, {
        tokenType: 1, // ERC 1155
        tokenAddress: discount1155NFT.address,
        tokenId: 1,
        basisPoints: 1000,
      });

      await discount1155NFT.mint(bob.address, 1, 1);

      await genericStore.connect(bob).buyTokens(bob.address, discount1155NFT.address, 1, 3, jourdanCoinSale, jourdanCoin.address, false, false);
      let effectivePrice = Math.floor((price * (10000 - 1000)) / 10000);
      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(effectivePrice));
    });
  });

  describe.skip('Ownership Discounts reverts', async function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let zeroAddress = ethers.constants.AddressZero;
    let nonWhitelistToken: ERC20Mock;
    let discount1155NFT: ERC1155MockAssetManager;
    let discount721NFT: ERC721Mock;

    const discount1 = 1000;
    const discount2 = 2000;

    const bob_mint = 10000;

    let numPurchases = 1;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      maxCoinSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.setSaleState(maxCoinSale, false);

      maticSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(maticSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, ethers.utils.parseEther('10000'));
      await maxCoin.mint(bob.address, ethers.utils.parseEther('10000'));

      await jourdanCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));
      await maxCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));

      const discountTokenFactory1155 = await ethers.getContractFactory('ERC1155MockAssetManager');
      discount1155NFT = await discountTokenFactory1155.deploy('DISCOUNT TOKEN');

      const discountTokenFactory721 = await ethers.getContractFactory('ERC721Mock');
      discount721NFT = await discountTokenFactory721.deploy('MOCK721', 'M721');
    });
    it('Should not allow non moderator to add ownership rebate', async function () {
      await expect(
        genericStore.connect(bob).addOwnershipDiscount(jourdanCoinSale, { tokenType: 1, tokenAddress: discount1155NFT.address, tokenId: 1, basisPoints: discount1 }),
      ).to.revertedWith(`SG__NotGov("${bob.address}")`);
    });
    it('Should not allow Discounts exceeding 100% (erc721)', async function () {
      await expect(genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 0, tokenAddress: discount721NFT.address, tokenId: 0, basisPoints: 10001 })).to.revertedWith(
        `SG__InvalidSaleParameters()`,
      );
    });
    it('Should not allow Discounts exceeding 100% (erc1155)', async function () {
      await expect(genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 1, tokenAddress: discount1155NFT.address, tokenId: 1, basisPoints: 10001 })).to.revertedWith(
        `SG__InvalidSaleParameters()`,
      );
    });
    it('Should not allow ERC721s to be passed off as erc 1155', async function () {
      await expect(genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 1, tokenAddress: discount721NFT.address, tokenId: 1, basisPoints: discount1 })).to.revertedWith(
        `SG__InvalidSaleParameters()`,
      );
    });
    it('Should not allow ERC1155s to be passed off as erc 721', async function () {
      await expect(genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 0, tokenAddress: discount1155NFT.address, tokenId: 1, basisPoints: discount1 })).to.revertedWith(
        `SG__InvalidSaleParameters()`,
      );
    });

    it('Should revert if the user buys and does not actually own the token', async function () {
      await genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 1, tokenAddress: discount1155NFT.address, tokenId: 1, basisPoints: discount1 });
      await expect(genericStore.connect(bob).buyTokens(bob.address, discount1155NFT.address, 1, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false)).to.revertedWith(
        `SG__SenderDoesNotOwnToken("${discount1155NFT.address}", 1)`,
      );
    });

    it('Should revert if the user buys and the token is not eligible for a rebate', async function () {
      await expect(genericStore.connect(bob).buyTokens(bob.address, discount721NFT.address, 55, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false)).to.revertedWith(
        `SG__TokenNotEligibleForRebate("${discount721NFT.address}")`,
      );
    });
  });

  describe.skip('Compound rebate tests', async function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let zeroAddress = ethers.constants.AddressZero;
    let nonWhitelistToken: ERC20Mock;
    let discount1155NFT: ERC1155MockAssetManager;
    let discount721NFT: ERC721Mock;

    const discount1 = 1000;
    const discount2 = 2000;

    const bob_mint = 10000;

    let numPurchases = 1;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      maxCoinSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.setSaleState(maxCoinSale, false);

      maticSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(maticSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, ethers.utils.parseEther('10000'));
      await maxCoin.mint(bob.address, ethers.utils.parseEther('10000'));

      await jourdanCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));
      await maxCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));

      const discountTokenFactory1155 = await ethers.getContractFactory('ERC1155MockAssetManager');
      discount1155NFT = await discountTokenFactory1155.deploy('DISCOUNT TOKEN');

      const discountTokenFactory721 = await ethers.getContractFactory('ERC721Mock');
      discount721NFT = await discountTokenFactory721.deploy('MOCK721', 'M721');
    });
    it('Should apply both bulk and ownership rebate', async function () {
      let initBalance = await jourdanCoin.balanceOf(bob.address);
      await genericStore.addBulkDiscount(jourdanCoinSale, 2, 1000);
      await genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 1, tokenAddress: discount1155NFT.address, tokenId: 1, basisPoints: 1000 });

      await discount1155NFT.mint(bob.address, 1, 1);
      await genericStore.connect(bob).buyTokens(bob.address, discount1155NFT.address, 1, 2, jourdanCoinSale, jourdanCoin.address, false, false);

      let bulkDiscountedPrice = Math.floor((2 * price * (10000 - 1000)) / 10000);
      let ownerDiscountedPrice = Math.floor((bulkDiscountedPrice * (10000 - 1000)) / 10000);

      expect(await jourdanCoin.balanceOf(bob.address)).to.equal(initBalance.sub(ownerDiscountedPrice));
    });

    it('Should revert if total bulk discounts and rebate amount effectively gives away the token for free', async function () {
      await genericStore.addBulkDiscount(jourdanCoinSale, 2, 5000);
      await expect(genericStore.addOwnershipDiscount(jourdanCoinSale, { tokenType: 1, tokenAddress: discount1155NFT.address, tokenId: 1, basisPoints: 8500 })).to.revertedWith(
        `SG__CombinedDiscountTooLarge(${jourdanCoinSale}, ${price}, 5000, 8500)`,
      );
    });
  });

  describe('withdraw', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = ethers.utils.parseEther('100');
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let zeroAddress = ethers.constants.AddressZero;
    let discount1155NFT: ERC1155MockAssetManager;
    let discount721NFT: ERC721Mock;

    let numPurchases = 1;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      maxCoinSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.setSaleState(maxCoinSale, false);

      maticSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(maticSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, ethers.utils.parseEther('10000'));
      await maxCoin.mint(bob.address, ethers.utils.parseEther('10000'));

      await jourdanCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));
      await maxCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));

      const discountTokenFactory1155 = await ethers.getContractFactory('ERC1155MockAssetManager');
      discount1155NFT = await discountTokenFactory1155.deploy('DISCOUNT TOKEN');

      const discountTokenFactory721 = await ethers.getContractFactory('ERC721Mock');
      discount721NFT = await discountTokenFactory721.deploy('MOCK721', 'M721');
    });
    it('withdraws jourdanCoin', async function () {
      console.log(await jourdanCoin.balanceOf(bob.address));
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      console.log(await jourdanCoin.balanceOf(bob.address));

      await genericStore.withdraw(alice_moderator.address, jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(alice_moderator.address)).to.equal(price);
    });
    it('withdraws matic', async function () {
      const initBalance = await alice_moderator.getBalance();
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('100') });

      await genericStore.withdraw(alice_moderator.address, zeroAddress);
      expect(await alice_moderator.getBalance()).to.equal(initBalance.add(price));
    });
    it('reverts on non gov call', async function () {
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false, { value: ethers.utils.parseEther('10') });
      await expect(genericStore.connect(bob).withdraw(alice_moderator.address, zeroAddress)).to.revertedWith('SG__NotGov("0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC")');
    });
    it.skip('Reverts on admin withdrawing jourdanCoin more than once', async () => {
      console.log(await jourdanCoin.balanceOf(bob.address));
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      console.log(await jourdanCoin.balanceOf(bob.address));

      await genericStore.withdraw(alice_moderator.address, jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(alice_moderator.address)).to.equal(price);

      await expect(genericStore.withdraw(alice_moderator.address, jourdanCoin.address)).to.revertedWith(`SG__ZeroValue()`);
    });
    it.skip('Reverts on admin withdrawing matic more than once', async () => {
      const initBalance = await alice_moderator.getBalance();
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('100') });

      await genericStore.withdraw(alice_moderator.address, zeroAddress);
      expect(await alice_moderator.getBalance()).to.equal(initBalance.add(price));

      await expect(genericStore.withdraw(alice_moderator.address, zeroAddress)).to.revertedWith(`SG__ZeroValue()`);
    });
  });
  describe('beneficiary withdraw', function () {
    let jourdanCoinSale;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = ethers.utils.parseEther('100');
    let userCap = 5;
    let profitState = true;
    let adminSupplied = false;

    let maxCoinSale;
    let maticSale;

    let zeroAddress = ethers.constants.AddressZero;
    let discount1155NFT: ERC1155MockAssetManager;
    let discount721NFT: ERC721Mock;

    let shares1 = 1000;
    let shares2 = 1000;
    let shares3 = 1000;
    let totalBeneficiaresShare = shares1 + shares2 + shares3;

    let numPurchases = 1;
    beforeEach(async () => {
      jourdanCoinSale = await genericStore.callStatic.createSale(
        toyNFT1.address,
        tokenId,
        unitSize,
        totalUnitSupply,
        price,
        userCap,
        jourdanCoin.address,
        profitState,
        adminSupplied,
      );
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, jourdanCoin.address, profitState, adminSupplied);
      await genericStore.setSaleState(jourdanCoinSale, false);

      maxCoinSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, maxCoin.address, profitState, false);
      await genericStore.setSaleState(maxCoinSale, false);

      maticSale = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(maticSale, false);

      await genericStore.setPaperCurrency(jourdanCoin.address);
      await jourdanCoin.mint(bob.address, ethers.utils.parseEther('10000'));
      await maxCoin.mint(bob.address, ethers.utils.parseEther('10000'));

      await genericStore.setFeeWalletsAndPercentages([allen_beneficiary.address, cindy_beneficiary.address, elise_beneficiary.address], [shares1, shares2, shares3]);

      await jourdanCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));
      await maxCoin.connect(bob).approve(genericStore.address, ethers.utils.parseEther('10000'));

      const discountTokenFactory1155 = await ethers.getContractFactory('ERC1155MockAssetManager');
      discount1155NFT = await discountTokenFactory1155.deploy('DISCOUNT TOKEN');

      const discountTokenFactory721 = await ethers.getContractFactory('ERC721Mock');
      discount721NFT = await discountTokenFactory721.deploy('MOCK721', 'M721');
    });
    it('withdraws jourdanCoin', async function () {
      console.log(await jourdanCoin.balanceOf(bob.address));
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      console.log(await jourdanCoin.balanceOf(bob.address));

      await genericStore.withdraw(alice_moderator.address, jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(alice_moderator.address)).to.equal(price);
    });
    it('beneficiariy withdraws jourdanCoin', async () => {
      console.log(await jourdanCoin.balanceOf(bob.address));
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      console.log(await jourdanCoin.balanceOf(bob.address));

      await genericStore.connect(cindy_beneficiary).beneficiaryWithdraw(jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(cindy_beneficiary.address)).to.equal(price.mul(shares1).div(10000));
    });
    it('withdraws matic', async function () {
      const initBalance = await alice_moderator.getBalance();
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('100') });

      await genericStore.withdraw(alice_moderator.address, zeroAddress);
      expect(await alice_moderator.getBalance()).to.equal(initBalance.add(price.mul(10000 - totalBeneficiaresShare).div(10000)));
    });
    it('beneficary withdraws matic', async function () {
      const initBalance = await cindy_beneficiary.getBalance();
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('100') }); 

      let tx = await genericStore.connect(cindy_beneficiary).beneficiaryWithdraw(zeroAddress);
      let receipt = await tx.wait();
      const gasCost = BigNumber.from(receipt.effectiveGasPrice.mul(receipt.gasUsed));

      expect(await cindy_beneficiary.getBalance()).to.equal(initBalance.add(price.mul(shares1).div(10000)).sub(gasCost));
    });
    it('reverts on non beneficiary call', async function () {
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false, { value: ethers.utils.parseEther('100') });
      await expect(genericStore.connect(bob).beneficiaryWithdraw(zeroAddress)).to.revertedWith(`SG__NotBeneficiary("${bob.address}")`);
    });
    it('reverts on beneficiary withdrawing matic more than once', async () => {
      const initBalance = await cindy_beneficiary.getBalance();
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('100') });

      let tx = await genericStore.connect(cindy_beneficiary).beneficiaryWithdraw(zeroAddress);
      let receipt = await tx.wait();
      const gasCost = BigNumber.from(receipt.effectiveGasPrice.mul(receipt.gasUsed));

      expect(await cindy_beneficiary.getBalance()).to.equal(initBalance.add(price.mul(shares1).div(10000)).sub(gasCost));

      await expect(genericStore.connect(cindy_beneficiary).beneficiaryWithdraw(zeroAddress)).to.revertedWith(`SG__ZeroValue()`);
    });
    it('reverts on beneficiary withdrawing jourdanCoin more than once', async () => {
      console.log(await jourdanCoin.balanceOf(bob.address));
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      console.log(await jourdanCoin.balanceOf(bob.address));

      await genericStore.connect(cindy_beneficiary).beneficiaryWithdraw(jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(cindy_beneficiary.address)).to.equal(price.mul(shares1).div(10000));

      await expect(genericStore.connect(cindy_beneficiary).beneficiaryWithdraw(jourdanCoin.address)).to.revertedWith(`SG__ZeroValue()`);
    });
    it('All beneficiaries and moderator withdraw jourdanCoin', async () => {
      console.log(await jourdanCoin.balanceOf(bob.address));
      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, jourdanCoinSale, jourdanCoin.address, false, false);
      console.log(await jourdanCoin.balanceOf(bob.address));

      await genericStore.withdraw(alice_moderator.address, jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(alice_moderator.address)).to.equal(price);

      await genericStore.connect(cindy_beneficiary).beneficiaryWithdraw(jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(cindy_beneficiary.address)).to.equal(price.mul(shares1).div(10000));

      await genericStore.connect(allen_beneficiary).beneficiaryWithdraw(jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(allen_beneficiary.address)).to.equal(price.mul(shares1).div(10000));

      await genericStore.connect(elise_beneficiary).beneficiaryWithdraw(jourdanCoin.address);
      expect(await jourdanCoin.balanceOf(elise_beneficiary.address)).to.equal(price.mul(shares1).div(10000));

      expect(await jourdanCoin.balanceOf(genericStore.address)).to.equal(0);
    });
    it('All beneficiaries and moderator withdraw matic', async () => {
      const initBalance = await alice_moderator.getBalance();
      const initBalance_cindy = await cindy_beneficiary.getBalance();
      const initBalance_allen = await allen_beneficiary.getBalance();
      const initBalance_elise = await elise_beneficiary.getBalance();

      await genericStore.connect(bob).buyTokens(bob.address, zeroAddress, 0, numPurchases, maticSale, zeroAddress, false, false, { value: ethers.utils.parseEther('100') });

      await genericStore.withdraw(alice_moderator.address, zeroAddress);
      expect(await alice_moderator.getBalance()).to.equal(initBalance.add(price.mul(10000 - totalBeneficiaresShare).div(10000)));

      let tx_cindy = await genericStore.connect(cindy_beneficiary).beneficiaryWithdraw(zeroAddress);
      let receipt_cindy = await tx_cindy.wait();
      const gasCost_cindy = BigNumber.from(receipt_cindy.effectiveGasPrice.mul(receipt_cindy.gasUsed));
      expect(await cindy_beneficiary.getBalance()).to.equal(initBalance_cindy.add(price.mul(shares1).div(10000)).sub(gasCost_cindy));

      let tx_allen = await genericStore.connect(allen_beneficiary).beneficiaryWithdraw(zeroAddress);
      let receipt_allen = await tx_allen.wait();
      const gasCost_allen = BigNumber.from(receipt_allen.effectiveGasPrice.mul(receipt_allen.gasUsed));
      expect(await allen_beneficiary.getBalance()).to.equal(initBalance_allen.add(price.mul(shares1).div(10000)).sub(gasCost_allen));

      let tx_elise = await genericStore.connect(elise_beneficiary).beneficiaryWithdraw(zeroAddress);
      let receipt_elise = await tx_elise.wait();
      const gasCost_elise = BigNumber.from(receipt_elise.effectiveGasPrice.mul(receipt_elise.gasUsed));
      expect(await elise_beneficiary.getBalance()).to.equal(initBalance_elise.add(price.mul(shares1).div(10000)).sub(gasCost_elise));
    });
  });
  describe('Getters work properly', () => {
    it('Get oracle', async () => {
      let _oracle = await genericStore.getOracleManager();
      expect(_oracle).to.equal(oracle.address);
    });
    it('Get swap manager', async () => {
      let _swapManager = await genericStore.getSwapManager();
      expect(_swapManager).to.equal(swapManager.address);
    });
    it('Get sale bonus contract', async () => {
      let saleBonusesFactory = await ethers.getContractFactory('SaleBonuses');
      let saleBonuses = (await upgrades.deployProxy(saleBonusesFactory, [])) as SaleBonuses;
      await genericStore.setSaleBonuses(saleBonuses.address);

      let _saleBonus = await genericStore.getSaleBonus();

      expect(_saleBonus).to.equal(saleBonuses.address);
    });
    it('Get beneficiaries', async () => {
      await genericStore.setFeeWalletsAndPercentages([cindy_beneficiary.address, elise_beneficiary.address, allen_beneficiary.address], [200, 300, 400]);
      let beneficaries = await genericStore.getBeneficiaries();

      let beneficaries1 = beneficaries['beneficiary'][0];
      let beneficaries2 = beneficaries['beneficiary'][1];
      let beneficaries3 = beneficaries['beneficiary'][2];

      let share1 = beneficaries['feeBps'][0];
      let share2 = beneficaries['feeBps'][1];
      let share3 = beneficaries['feeBps'][2];

      expect(beneficaries1).to.equal(cindy_beneficiary.address);
      expect(beneficaries2).to.equal(elise_beneficiary.address);
      expect(beneficaries3).to.equal(allen_beneficiary.address);

      expect(share1).to.equal(200);
      expect(share2).to.equal(300);
      expect(share3).to.equal(400);
    });
  });

  describe('ERC1155 payments', () => {
    let saleWith1155payments;
    let tokenId = 1;
    let unitSize = 1;
    let totalUnitSupply = 10;
    let price = 10;
    let userCap = 5;
    let profitState = false;
    let adminSupplied = false;

    let paymentTokenIds = [55, 56];
    let paymentTokenPrices = [100, 200];

    let genericStoreDataProvider;

    beforeEach(async () => {
      saleWith1155payments = await genericStore.callStatic.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.createSale(toyNFT1.address, tokenId, unitSize, totalUnitSupply, price, userCap, ethers.constants.AddressZero, profitState, false);
      await genericStore.setSaleState(saleWith1155payments, false);

      await genericStore.setERC1155PaymentPrices(saleWith1155payments, toyNFT1.address, paymentTokenIds, paymentTokenPrices);

      await toyNFT1.setTrusted(owner.address, true);
      await toyNFT1.trustedBatchMint(bob.address, paymentTokenIds, [1000, 1000]);

      await toyNFT1.connect(bob).setApprovalForAll(genericStore.address, true);

      const genericStoreDataProviderFactory = await ethers.getContractFactory('ERC1155StoreGenericDataProvider');
      genericStoreDataProvider = await genericStoreDataProviderFactory.deploy(genericStore.address);
    });

    it('Bob can buy tokens with erc1155 payment', async () => {
      await genericStore.connect(bob).buyTokensWithERC1155(
        bob.address,
        ethers.constants.AddressZero,
        0,
        1,
        saleWith1155payments,
        toyNFT1.address,
        false,
        false,
        paymentTokenIds[0]
      );
      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.be.equal(1);

      await genericStore.connect(bob).buyTokensWithERC1155(
        bob.address,
        ethers.constants.AddressZero,
        0,
        1,
        saleWith1155payments,
        toyNFT1.address,
        false,
        false,
        paymentTokenIds[1]
      );
      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.be.equal(2);

    });

    it('Should be able to buy multiple tokens', async () => {
      let numPurchases = 3;

      await genericStore.connect(bob).buyTokensWithERC1155(
        bob.address,
        ethers.constants.AddressZero,
        0,
        numPurchases,
        saleWith1155payments,
        toyNFT1.address,
        false,
        false,
        paymentTokenIds[0]
      );

      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.be.equal(numPurchases);
    });

    it('Should not be able to pay with non approved erc1155 tokenId', async () => {
      await toyNFT1.trustedMint(bob.address, 999, 1000);

      await expect(
        genericStore.connect(bob).buyTokensWithERC1155(
          bob.address,
          ethers.constants.AddressZero,
          0,
          1,
          saleWith1155payments,
          toyNFT1.address,
          false,
          false,
          999
        )
      ).to.be.revertedWith('SG__InvalidERC1155PaymentTokenId');
    });

    it('Should be able to withdraw paid ERC1155 tokens', async () => {
      let numPurchases = 3;

      await genericStore.connect(bob).buyTokensWithERC1155(
        bob.address,
        ethers.constants.AddressZero,
        0,
        numPurchases,
        saleWith1155payments,
        toyNFT1.address,
        false,
        false,
        paymentTokenIds[0]
      );

      await genericStore.withdrawERC1155token(owner.address, toyNFT1.address, paymentTokenIds[0]);

      expect(await toyNFT1.balanceOf(owner.address, paymentTokenIds[0])).to.be.equal(numPurchases*paymentTokenPrices[0]);
    });

    it('Should be able to modify ERC1155 prices', async () => {

      await genericStore.setERC1155PaymentPrices(saleWith1155payments, toyNFT1.address, [54,55], [100, 0]);
      await toyNFT1.trustedMint(bob.address, 54, 1000);

      await genericStore.connect(bob).buyTokensWithERC1155(
        bob.address,
        ethers.constants.AddressZero,
        0,
        1,
        saleWith1155payments,
        toyNFT1.address,
        false,
        false,
        54
      );
      expect(await toyNFT1.balanceOf(bob.address, tokenId)).to.be.equal(1);

      await expect(
        genericStore.connect(bob).buyTokensWithERC1155(
          bob.address,
          ethers.constants.AddressZero,
          0,
          1,
          saleWith1155payments,
          toyNFT1.address,
          false,
          false,
          55
        )
      ).to.be.revertedWith('SG__InvalidERC1155PaymentTokenId');
    });

    it('Non gov should not be able to modify prices', async () => {
      await expect(
        genericStore.connect(bob).setERC1155PaymentPrices(saleWith1155payments, toyNFT1.address, [54,55], [100, 0])
      ).to.be.revertedWith(`SG__NotGov("${bob.address}")`);
    });

    it('Check price should return correct value', async () => {
      expect(await genericStoreDataProvider.callStatic.checkERC1155PaymentPrice(1, saleWith1155payments, paymentTokenIds[0]))
        .to.be.equal(paymentTokenPrices[0]);
      expect(await genericStoreDataProvider.callStatic.checkERC1155PaymentPrice(1, saleWith1155payments, paymentTokenIds[1]))
        .to.be.equal(paymentTokenPrices[1]);
      expect(await genericStoreDataProvider.callStatic.checkERC1155PaymentPrice(3, saleWith1155payments, paymentTokenIds[0]))
        .to.be.equal(3 * paymentTokenPrices[0]);
    });
  });
});

async function printBalances(user: Signer, assets: Contract, erc721Mock: Contract) {
  const aliceBal1 = await assets.balanceOf(await user.getAddress(), 1);
  const aliceBal2 = await assets.balanceOf(await user.getAddress(), 2);
  const aliceBal3 = await assets.balanceOf(await user.getAddress(), 3);
  const aliceBal19 = await assets.balanceOf(await user.getAddress(), 19);
  const aliceBal4 = await erc721Mock.balanceOf(await user.getAddress());
  console.log(`Alice's Assets 1 balance: ${aliceBal1}`);
  console.log(`Alice's Assets 2 balance: ${aliceBal2}`);
  console.log(`Alice's Assets 3 balance: ${aliceBal3}`);
  console.log(`Alice's Assets 19 balance: ${aliceBal19}`);
  console.log(`Alice's ERC721 balance: ${aliceBal4}`);
}
