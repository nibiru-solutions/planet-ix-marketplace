import {expect} from 'chai';
import hre, {ethers, upgrades} from 'hardhat';
import {Signer, Contract, BigNumber, providers, utils} from 'ethers';
import {PIXCategory, PIXSize} from './utils';
import {constants} from '@openzeppelin/test-helpers';
import {parseEther} from "ethers/lib/utils";

describe('Gravity Grade Burn Cargo Drop #3', function () {
    enum GravityGradeDrops {
        UNUSED,
        CargoDrop3,
        AnniversaryPackMystery,
        AnniversaryPackOutlier,
        AnniversaryPackCommon,
        AnniversaryPackUncommon,
        AnniversaryPackRare,
        AnniversaryPackLegendary,
        StarterPack
    }

    let gravityGrade: Contract;
    let burn: Contract;
    let assets: Contract;

    let owner: Signer;
    let alice: Signer;
    let probabilities = [450, 4505, 4505, 450, 90];

    beforeEach(async function () {
        [owner, alice] = await ethers.getSigners();
        let ggFactory = await ethers.getContractFactory("GravityGradeMock");
        gravityGrade = await ggFactory.deploy();
        let assetFactory = await ethers.getContractFactory("ERC1155MockAssetManager");
        assets = await assetFactory.deploy("toy uri");

        let burnFactory = await ethers.getContractFactory("GravityGradeBurn_CargoDrop3");
        burn = await upgrades.deployProxy(burnFactory, [probabilities]);

        await burn.setGravityGrade(gravityGrade.address);
        await burn.setAssetManager(assets.address);
        await burn.start();

        await assets.setTrusted(burn.address, true);
    });

    describe("Burn pack", function () {

        it("Should burn Cargo Drop 3", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.CargoDrop3);
        });

        it("Should burn Mystery APack", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackMystery);
        });

        it("Should burn Outlier APack", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackOutlier);
        });

        it("Should burn Common APack", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackCommon);
        });

        it("Should burn Uncommon APack", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackUncommon);
        });

        it("Should burn Rare APack", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackRare);
        });

        it("Should burn Legendary APack", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackLegendary);
        });

        it("Gives correct rewards for Cargo Drop 3", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.CargoDrop3);
            expect(await gravityGrade.balanceOf(await alice.getAddress(), GravityGradeDrops.AnniversaryPackMystery)).to.equal(1);
            expect(await assets.balanceOf(await alice.getAddress(), 25)).to.equal(1);
            expect(await assets.balanceOf(await alice.getAddress(), 15)).to.equal(1);
        });

        it("Gives correct rewards for Mystery APack", async function () {
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackMystery);
            expect(await totalBalanceOfAPacks(alice, gravityGrade)).to.equal(1);
        });

        it("Gives correct rewards for Non Mystery APack", async function () {
            let count = 0;
            for (let i = 3; i <= 7; i++) {
                count++;
                await mintNBurnGG(alice, burn, gravityGrade, i);
                expect(await assets.balanceOf(await alice.getAddress(), 16)).to.equal(count * 50);
                expect(await assets.balanceOf(await alice.getAddress(), 17)).to.equal(count * 5);
            }
        });

        it("Gives correct rewards when multi-burning Cargo Drops", async function () {
            let num = 3;
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.CargoDrop3, num);
            expect(await gravityGrade.balanceOf(await alice.getAddress(), GravityGradeDrops.AnniversaryPackMystery)).to.equal(num);
            expect(await assets.balanceOf(await alice.getAddress(), 25)).to.equal(num);
            expect(await assets.balanceOf(await alice.getAddress(), 15)).to.equal(num);
        });

        it("Gives correct rewards when multi-burning Mystery APacks", async function () {
            let num = 10;
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackMystery, num);
            expect(await totalBalanceOfAPacks(alice, gravityGrade)).to.equal(num);
        });

        it("Gives correct rewards when multi-burning Outlier APacks", async function () {
            let num = 10;
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackOutlier, num);
            expect(await assets.balanceOf(await alice.getAddress(), 16)).to.equal(num * 50);
            expect(await assets.balanceOf(await alice.getAddress(), 17)).to.equal(num * 5);
        });

        it("Gives correct rewards when multi-burning AnniversaryPackCommon ", async function () {
            let num = 10;
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackCommon, num);
            expect(await assets.balanceOf(await alice.getAddress(), 16)).to.equal(num * 50);
            expect(await assets.balanceOf(await alice.getAddress(), 17)).to.equal(num * 5);
        });

        it("Gives correct rewards when multi-burning AnniversaryPackUncommon ", async function () {
            let num = 10;
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackUncommon, num);
            expect(await assets.balanceOf(await alice.getAddress(), 16)).to.equal(num * 50);
            expect(await assets.balanceOf(await alice.getAddress(), 17)).to.equal(num * 5);
        });

        it("Gives correct rewards when multi-burning AnniversaryPackRare ", async function () {
            let num = 10;
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackRare, num);
            expect(await assets.balanceOf(await alice.getAddress(), 16)).to.equal(num * 50);
            expect(await assets.balanceOf(await alice.getAddress(), 17)).to.equal(num * 5);
        });

        it("Gives correct rewards when multi-burning AnniversaryPackLegendary ", async function () {
            let num = 10;
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackLegendary, num);
            expect(await assets.balanceOf(await alice.getAddress(), 16)).to.equal(num * 50);
            expect(await assets.balanceOf(await alice.getAddress(), 17)).to.equal(num * 5);
        });

        it("Gives correct rewards when multi-burning StarterPack ", async function () {
            let num = 10;
            await mintNBurnGG(alice, burn, gravityGrade, GravityGradeDrops.StarterPack, num);
            expect(await assets.balanceOf(await alice.getAddress(), 16)).to.equal(num * 5);
        });


        it("Emits correct event when multi-burning Outlier APacks", async function () {
            let num = 1;
            await mintNBurnAPackEventCheck(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackOutlier, num);
        });



        it("Emits correct event when multi-burning Mystery box ", async function () {
            let num = 2;
            let tokenId = GravityGradeDrops.AnniversaryPackMystery;
            await quickDrop(alice, gravityGrade, tokenId, num);
            expect(await gravityGrade.balanceOf(await alice.getAddress(), tokenId)).to.equal(num);
            await expect(burn.connect(alice).burnPack(tokenId, num)).to.emit(burn, "GG_Drop3_MysteryBox");
            expect(await gravityGrade.balanceOf(await alice.getAddress(), tokenId)).to.equal(0);
        });

        it("Emits correct event when multi-burning AnniversaryPackCommon ", async function () {
            let num = 2;
            await mintNBurnAPackEventCheck(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackCommon, num);
        });

        it("Emits correct event when multi-burning AnniversaryPackUncommon ", async function () {
            let num = 3;
            await mintNBurnAPackEventCheck(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackUncommon, num);
        });

        it("Emits correct event when multi-burning AnniversaryPackRare ", async function () {
            let num = 4;
            await mintNBurnAPackEventCheck(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackRare, num);
        });

        it("Emits correct event when multi-burning AnniversaryPackLegendary ", async function () {
            let num = 5;
            await mintNBurnAPackEventCheck(alice, burn, gravityGrade, GravityGradeDrops.AnniversaryPackLegendary, num);
        });

        it("Emits correct event when multi-burning StarterPack ", async function () {
            let num = 5;
            await mintNBurnAPackEventCheck(alice, burn, gravityGrade, GravityGradeDrops.StarterPack, num);
        });

        it("Handles probabilities as expected (p_i = 1 test)", async function () {
            for (let i = 0; i < 5; i++) {
                let probas = [0, 0, 0, 0, 0];
                probas[i] = 10000;
                await burn.setPackDistribution(probas);
                await quickDrop(alice, gravityGrade, 2);
                await burn.connect(alice).burnPack(2, 1);
                expect(await gravityGrade.balanceOf(await alice.getAddress(), i + 3)).to.equal(1);
            }
        });

        it("Handles probabilities as expected (monte carlo test - FLAKY!)", async function () {
            let probas = [2000, 2000, 2000, 2000, 2000];
            await burn.setPackDistribution(probas);
            let N = 1000;
            let thresh = N * 0.05;
            this.timeout(50 * N)
            for (let i = 0; i < N; i++) {
                await quickDrop(alice, gravityGrade, 2);
                await burn.connect(alice).burnPack(2, 1);
            }
            for (let j = 0; j < 5; j++) {
                let balance = await gravityGrade.balanceOf(await alice.getAddress(), j + 3);
                expect(Math.abs(balance - N * probas[j] / 10000) < thresh).to.equal(true);
            }

        });

    });

    describe("Burn pack reverts", function () {

        it("Should revert when the contract is paused", async function () {
            await burn.stop()
            await quickDrop(alice, gravityGrade, 1, 1);
            expect(await gravityGrade.balanceOf(await alice.getAddress(), 1)).to.equal(1);
            await expect(burn.connect(alice).burnPack(1, 1)).to.revertedWith("Pausable: paused");
        });

        it("Should revert on invalid token ID", async function () {
            await quickDrop(alice, gravityGrade, 99, 1);
            expect(await gravityGrade.balanceOf(await alice.getAddress(), 99)).to.equal(1);
            await expect(burn.connect(alice).burnPack(99, 1)).to.revertedWith("GravityGradeBurn__InvalidTokenId(99)");
        });

    })

    describe("Misc.", function () {

        it("Should revert on calling stop as non owner", async function () {
            await expect(burn.connect(alice).stop()).to.revertedWith("Ownable: caller is not the owner");
        });

        it("Should revert on calling start as non owner", async function () {
            await burn.stop()
            await expect(burn.connect(alice).start()).to.revertedWith("Ownable: caller is not the owner");
        });

        it("Should revert on calling set pack distribution as non owner", async function () {
            await expect(burn.connect(alice).setPackDistribution([10000, 0, 0, 0, 0])).to.revertedWith("Ownable: caller is not the owner");
        });

        it("Should revert on calling set pack distribution with invalid paramters", async function () {
            await expect(burn.setPackDistribution([10001, 0, 0, 0, 0])).to.revertedWith("Probabilities do not sum to unity");
            await expect(burn.setPackDistribution([100, 1000, 9000, 0, 0])).to.revertedWith("Probabilities do not sum to unity");
            await expect(burn.setPackDistribution([10000, 1, 0, 0, 0])).to.revertedWith("Probabilities do not sum to unity");
            await expect(burn.setPackDistribution([11, 1, 59, 2, 44])).to.revertedWith("Probabilities do not sum to unity");
        });


    })


});

async function quickDrop(player: Signer, gg: Contract, tokenId, num = 1) {
    await gg.airdrop([await player.getAddress()], [tokenId], [num]);
}

async function totalBalanceOfAPacks(player: Signer, gg: Contract) {
    let sum = BigNumber.from(0);
    for (let i = 3; i <= 7; i++) {
        sum = sum.add(BigNumber.from(await gg.balanceOf(await player.getAddress(), i)));
    }
    return sum;
}

async function mintNBurnGG(alice: Signer, burn: Contract, gravityGrade: Contract, tokenId, num = 1) {
    await quickDrop(alice, gravityGrade, tokenId, num);
    expect(await gravityGrade.balanceOf(await alice.getAddress(), tokenId)).to.equal(num);
    await expect(burn.connect(alice).burnPack(tokenId, num)).to.not.reverted;
    expect(await gravityGrade.balanceOf(await alice.getAddress(), tokenId)).to.equal(0);
}

async function mintNBurnAPackEventCheck(alice: Signer, burn: Contract, gravityGrade: Contract, tokenId, num = 1) {
    await quickDrop(alice, gravityGrade, tokenId, num);
    expect(await gravityGrade.balanceOf(await alice.getAddress(), tokenId)).to.equal(num);
    await expect(burn.connect(alice).burnPack(tokenId, num)).to.emit(burn, "GG_Drop3_PackBurned").withArgs(await alice.getAddress(), tokenId, num);
    expect(await gravityGrade.balanceOf(await alice.getAddress(), tokenId)).to.equal(0);
}
