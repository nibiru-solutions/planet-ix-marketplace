import { expect } from 'chai';
import { ethers } from 'hardhat';
import { Contract, utils } from 'ethers';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

async function createSig(signer, amount, sender, nonce) {
  const message = utils.defaultAbiCoder.encode(['uint256', 'address', 'uint256'], [amount, sender, nonce]);
  const hash = utils.keccak256(message);
  const signature = await signer.signMessage(utils.arrayify(hash));
  return { signature, hash };
}

describe('Cargo Drop 3 Claims', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;
  let cg3: Contract;
  let claims: Contract;
  let cg3TokenId = 1;

  beforeEach(async function () {
    [owner, alice, bob] = await ethers.getSigners();

    let CG3Factory = await ethers.getContractFactory('ERC1155MockAssetManager');
    cg3 = await CG3Factory.deploy('uri');

    let ClaimsFactory = await ethers.getContractFactory('ClaimVerifier');
    claims = await ClaimsFactory.deploy(cg3.address, cg3TokenId);

    await cg3.setTrusted(claims.address, true);
  });

  describe('#initialize', () => {
    it('check initial values', async function () {
      expect(await claims.owner()).to.equal(owner.address);
      expect(await claims.notary()).to.equal(owner.address);
    });
  });

  describe('#claim', () => {
    it('invalid notary', async function () {
      const { signature } = await createSig(alice, 1, alice.address, 1);
      await expect(claims.connect(alice).mint(1, 1, signature)).to.be.revertedWith('InvalidNotarization()');
    });
    it('reused nonce', async function () {
      let { signature: sig1 } = await createSig(owner, 1, alice.address, 1);
      await claims.connect(alice).mint(1, 1, sig1);

      let { signature: sig2 } = await createSig(owner, 1, alice.address, 1);
      await expect(claims.connect(alice).mint(1, 1, sig2)).to.be.revertedWith('NonceReused()');
    });
    it('only sender can claim', async function () {
      let { signature } = await createSig(owner, 1, alice.address, 1);
      await expect(claims.connect(bob).mint(1, 1, signature)).to.be.revertedWith('InvalidNotarization()');
    });
    it('valid claim', async function () {
      let { signature: sig1 } = await createSig(owner, 1, alice.address, 1);
      await claims.connect(alice).mint(1, 1, sig1);

      expect(await cg3.balanceOf(alice.address, cg3TokenId)).to.equal(1);
    });
  });
});
