import {network, ethers, upgrades} from 'hardhat';
import {SignerWithAddress} from '@nomiclabs/hardhat-ethers/signers';
import {expect} from 'chai';
import {BigNumber, Contract} from 'ethers';
import {exp} from "@prb/math";
import {max} from "hardhat/internal/util/bigint";
import {describe} from "mocha";

describe('Spinner', function () {
    this.timeout(120000);
    let emilCoin: Contract;
    let usdt: Contract;
    let spinnerTreasury: Contract;
    let spinnerAirdrop: Contract;
    let MMTraderMock: Contract;
    let mockFluencrSale: Contract;

    let deployer: SignerWithAddress;
    let player: SignerWithAddress;
    let feeWallet: SignerWithAddress;
    let alice: SignerWithAddress;
    let USDT_BALANCE = 10_000_000;

    beforeEach(async () => {
        [deployer, player, feeWallet, alice] = await ethers.getSigners();
        let erc20Factory = await ethers.getContractFactory('ERC20Mock');
        emilCoin = await erc20Factory.deploy('EmilCoin', 'EC');

        let erc20Factory2 = await ethers.getContractFactory('ERC20Mock');
        usdt = await erc20Factory2.deploy('usdt', 'usdt');
        await usdt.mint(player.address, USDT_BALANCE);

        let spinnerTreasuryFactory = await ethers.getContractFactory('SpinnerTreasury');
        spinnerTreasury = await upgrades.deployProxy(spinnerTreasuryFactory, [emilCoin.address, usdt.address]);

        let spinnerAirdropFactory = await ethers.getContractFactory('SpinnerAirdrop');
        spinnerAirdrop = await upgrades.deployProxy(spinnerAirdropFactory, [emilCoin.address, usdt.address, spinnerTreasury.address]);

        let mockFluencrSaleFactory = await ethers.getContractFactory('MockFluencrSale');
        mockFluencrSale = await upgrades.deployProxy(mockFluencrSaleFactory, [usdt.address, spinnerTreasury.address]);

        let MMTraderMockFactory = await ethers.getContractFactory('MMTraderMock');
        MMTraderMock = await upgrades.deployProxy(MMTraderMockFactory, [emilCoin.address, usdt.address]);
        await MMTraderMock.setReceiver(MMTraderMock.address);

        await spinnerTreasury.setSpinnerAirdrop(spinnerAirdrop.address);
        await spinnerTreasury.setFluencr(mockFluencrSale.address);
        await spinnerTreasury.registerVault(MMTraderMock.address, spinnerAirdrop.address);
        await MMTraderMock.setReceiver(MMTraderMock.address);

    });
    async function automationTreasury(){
        let checkUpkeep = await spinnerTreasury.checkUpkeep(1);
        //console.log("rett", checkUpkeep.upkeepNeeded)
        if(checkUpkeep.upkeepNeeded){
            await spinnerTreasury.performUpkeep(1);
            return true;
        }
    }
    async function automationAirdrop(){
        let checkUpkeep = await spinnerAirdrop.checkUpkeep(1);
        //console.log("rett", checkUpkeep.upkeepNeeded)
        if(checkUpkeep.upkeepNeeded){
            await spinnerAirdrop.performUpkeep(1);
            return true;
        }
    }

    describe('Place swapOrder', () => {
        it('expect correct queue len after fluencr sell', async () => {
            await usdt.connect(deployer).mint(mockFluencrSale.address, USDT_BALANCE)
            console.log(await usdt.balanceOf(mockFluencrSale.address))
            await mockFluencrSale.callSwapOrder(alice.address, USDT_BALANCE)
            expect(await spinnerTreasury.queueLength()).to.equal(1)
        });
        it('expect correct queue len after fluencr sell2', async () => {
            await usdt.connect(deployer).mint(mockFluencrSale.address, USDT_BALANCE*2)
            await mockFluencrSale.callSwapOrder(alice.address, USDT_BALANCE)
            await automationTreasury()
            await mockFluencrSale.callSwapOrder(alice.address, USDT_BALANCE)
            expect(await spinnerTreasury.queueLength()).to.equal(1)
        });
        it('expect correct queue len after fluencr sell3', async () => {
            await usdt.connect(deployer).mint(mockFluencrSale.address, USDT_BALANCE*2)
            await mockFluencrSale.callSwapOrder(alice.address, USDT_BALANCE)
            console.log("fluencr", await usdt.balanceOf(mockFluencrSale.address))
            console.log("spin", await usdt.balanceOf(spinnerTreasury.address))
            await network.provider.send('evm_increaseTime', [61]);
            await network.provider.send('evm_mine');
            await automationTreasury()
            console.log("fluencr", await usdt.balanceOf(mockFluencrSale.address))
            console.log("spin", await usdt.balanceOf(spinnerTreasury.address))
            await mockFluencrSale.callSwapOrder(alice.address, USDT_BALANCE)
            expect(await spinnerTreasury.queueLength()).to.equal(2)
        });
        it('expect only fluencr to be able to call on addSwapOrder', async () => {
        });

    });

    describe('spinner treasury automation', () => {
        beforeEach(async () => {
        });
        it('automation return false when less time than interval has passed', async () => {
        });
        it('automation return false when there is nothing in queue', async () => {
        });
        it('automation return false when first swaporder in queue is pending', async () => {
        });
        it('expect correct amount of usdt in vault after perfromUpkeep', async () => {
        });
        it('expect correct amount of usdt in spinnerTreasury after perfromUpkeep', async () => {
        });
        it('expect correct amount of usdt in spinnerTreasury after perfromUpkeep with multiple swaporders in queue', async () => {
            //only the usdt for the first swaporder should be sent to vault. so there should be usdt left in spinnertreasury.
        });
        it('expect correct amount of usdt in spinnerTreasury after perfromUpkeep', async () => {
        });
        it('expect queue to work correctly after multiple fulfilled swaporders', async () => {
        });
    });

    describe('spinner airdrop automation', () => {
        beforeEach(async () => {
        });
        it('checkUpkeep returns false when swaporder queue is empty', async () => {
        });
        it('checkUpkeep returns false when usdt hasnt been swapped in vault', async () => {
        });
        it('checkUpkeep returns false when malicious user sends small amount of ixt to ixtOutlet', async () => {
        });
        it('checkUpkeep returns false when first swaporder in queue is not pending', async () => {
        });
        it('expect performUpkeep to give one player correct amount of ixt', async () => {
        });
        it('expect performUpkeep to give multiple players correct amount of ixt', async () => {
        });
        it('expect performUpkeep to give multiple players correct amount of ixt with different prices', async () => {
        });
        it('expect performUpkeep to be reverted when called on multiple times', async () => {
        });
        it('expect ixt balance of ixtOutlet to be 0 after performUpkeep', async () => {
        });
        it('expect correct swaporder queue len after performUpkeep', async () => {
        });
        it('expect only spinner airdrop to be able to call on removePendingSwapOrder', async () => {
        });

    });

});