import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Signer, Contract, BigNumber, constants, Wallet } from 'ethers';
import { PIXCategory, PIXSize, increaseTime } from './utils';
import { zeroAddress } from 'ethereumjs-util';
import { formatEther, parseEther } from 'ethers/lib/utils';

describe('PIXTerritoryStaking', function () {
  let owner: Signer;
  let moderator: Signer;
  let alice: Signer;
  let bob: Signer;
  let pixToken: Contract;
  let usdc: Contract;
  let pixNFT: Contract;
  let pixStaking: Contract;

  let sizes = [
    PIXSize.Area,
    PIXSize.Area,
    PIXSize.Area,
    PIXSize.Area,
    PIXSize.Area,
    PIXSize.Sector,
    PIXSize.Sector,
    PIXSize.Sector,
    PIXSize.Sector,
    PIXSize.Sector,
    PIXSize.Zone,
    PIXSize.Zone,
    PIXSize.Zone,
    PIXSize.Zone,
    PIXSize.Zone,
    PIXSize.Domain,
    PIXSize.Domain,
    PIXSize.Domain,
    PIXSize.Domain,
    PIXSize.Domain,
  ];

  let categories = [
    PIXCategory.Outliers,
    PIXCategory.Common,
    PIXCategory.Uncommon,
    PIXCategory.Rare,
    PIXCategory.Legendary,
    PIXCategory.Outliers,
    PIXCategory.Common,
    PIXCategory.Uncommon,
    PIXCategory.Rare,
    PIXCategory.Legendary,
    PIXCategory.Outliers,
    PIXCategory.Common,
    PIXCategory.Uncommon,
    PIXCategory.Rare,
    PIXCategory.Legendary,
    PIXCategory.Outliers,
    PIXCategory.Common,
    PIXCategory.Uncommon,
    PIXCategory.Rare,
    PIXCategory.Legendary,
  ];

  let rewardPercentages = [
    50, 80, 100, 120, 150, 100, 200, 200, 300, 500, 200, 300, 400, 500, 900, 500, 700, 1000, 1200,
    2500,
  ];

  let tokensStaked = [1, 2, 3, 1, 2, 1, 2, 3, 1, 2, 1, 2, 3, 1, 2, 1, 2, 3, 1, 2];

  // test values
  let rewardPercentagePaidPerTokensStaked = [0, 1000, 1500, 3000, 5000, 7500, 10000];

  let rewardPercentagesDenominator = 10000;

  let AreaPushRewards = [0, 10000];
  let SectorPushRewards = [0, 5000, 3000, 2000];
  let ZonePushRewards = [0, 3000, 2500, 2000, 1500, 500, 500];
  let DomainPushRewards = [0, 2500, 2000, 1500, 1000, 500, 500, 500, 500, 400, 200, 200, 200];

  async function _setupNewContracts() {
    const PIXTFactory = await ethers.getContractFactory('PIXT');
    pixToken = await PIXTFactory.deploy();

    const MockTokenFactory = await ethers.getContractFactory('MockToken');
    usdc = await MockTokenFactory.deploy('Mock USDC', 'USDC', 6);

    const PIXFactory = await ethers.getContractFactory('PIX');
    pixNFT = await upgrades.deployProxy(PIXFactory, [pixToken.address, usdc.address]);

    const PIXStakingFactory = await ethers.getContractFactory('PIXTerritoryStaking');
    pixStaking = await upgrades.deployProxy(PIXStakingFactory, [pixToken.address, pixNFT.address]);

    await pixNFT.setTrader(pixStaking.address, true);
    await pixToken.approve(pixStaking.address, ethers.utils.parseEther('1000000000'));
    await pixToken.transfer(pixStaking.address, ethers.utils.parseEther('1000000'));
    await pixToken.transfer(await alice.getAddress(), BigNumber.from(10000));
    await pixToken.transfer(await bob.getAddress(), BigNumber.from(10000));

    await pixStaking.setRewardDistributor(await owner.getAddress());
    await pixStaking.setModerator(await owner.getAddress(), true);

    await pixStaking.initializePoolRewards(
      sizes,
      categories,
      rewardPercentages,
      tokensStaked,
      rewardPercentagesDenominator,
    );
    await pixStaking.setRewardPercentagePaidPerTokensStaked(rewardPercentagePaidPerTokensStaked);

    await pixStaking.setPushRewardNextEpochsPercentages(PIXSize.Area, AreaPushRewards);
    await pixStaking.setPushRewardNextEpochsPercentages(PIXSize.Sector, SectorPushRewards);
    await pixStaking.setPushRewardNextEpochsPercentages(PIXSize.Zone, ZonePushRewards);
    await pixStaking.setPushRewardNextEpochsPercentages(PIXSize.Domain, DomainPushRewards);

    const allSizes = Object.values(PIXSize).filter((v) => !isNaN(Number(v)));
    const allCategories = Object.values(PIXCategory).filter((v) => !isNaN(Number(v)));

    let pixInfos = [];

    allSizes.forEach((size) => {
      allCategories.forEach((category) => {
        if (size != PIXSize.Pix) {
          pixInfos.push([0, category, size]);
        }
      });
    });

    await pixNFT.batchMint(await alice.getAddress(), pixInfos);
    await pixNFT.batchMint(await bob.getAddress(), pixInfos);

    await pixNFT.connect(alice).setApprovalForAll(pixStaking.address, true);
    await pixNFT.connect(bob).setApprovalForAll(pixStaking.address, true);
  }

  before(async function () {
    [owner, moderator, alice, bob] = await ethers.getSigners();

    await _setupNewContracts();
  });

  describe('setRewardDistributor', () => {
    it('it should set reward distributor correctly', async () => {
      await pixStaking.setRewardDistributor(await alice.getAddress());
      expect(await pixStaking.rewardDistributor()).to.equal(await alice.getAddress());
    });

    it('it should revert if the caller is not a owner', async () => {
      await expect(
        pixStaking.connect(alice).setRewardDistributor(await bob.getAddress()),
      ).to.revertedWith('Ownable: caller is not the owner');
    });

    it('it should revert if the distributor address is zero address', async () => {
      await expect(pixStaking.setRewardDistributor(constants.AddressZero)).to.revertedWith(
        'Staking: INVALID_DISTRIBUTOR',
      );
    });
  });

  describe('stake and unstake', () => {
    it('should revert if not owner', async function () {
      await expect(pixStaking.connect(alice).stake(30)).to.be.revertedWith(
        'ERC721: transfer of token that is not own',
      );
    });

    it('should stake', async function () {
      await pixStaking.connect(alice).stake(1);
      expect(await pixStaking.stakers(1)).to.be.equal(await alice.getAddress());
      expect(await pixNFT.ownerOf(1)).to.be.equal(pixStaking.address);
      expect(await pixStaking.getTokensStaked(PIXSize.Area, PIXCategory.Outliers)).to.be.equal(1);
    });

    it('should unstake', async function () {
      await pixStaking.connect(alice).unstake(1);
      expect(await pixStaking.stakers(1)).to.be.equal(zeroAddress());
      expect(await pixNFT.ownerOf(1)).to.be.equal(await alice.getAddress());
    });

    it('should update stakedNFT on each unstake', async function () {
      await pixStaking.connect(alice).stake(1);
      await pixStaking.connect(alice).stake(2);
      await pixStaking.connect(alice).stake(3);
      expect(await pixStaking.getStakedNFTsLength(await alice.getAddress())).to.be.equal(3);

      await pixStaking.connect(alice).unstake(2);

      expect(await pixStaking.getStakedNFTsLength(await alice.getAddress())).to.be.equal(2);
      expect(await pixStaking.stakedNFTs(await alice.getAddress(), 0)).to.be.equal(1);
      expect(await pixStaking.stakedNFTs(await alice.getAddress(), 1)).to.be.equal(3);
    });

    it('should revert if not territory', async function () {
      await pixNFT.safeMint(await alice.getAddress(), [1, PIXCategory.Common, PIXSize.Pix]);
      let tokenId = await pixNFT.lastTokenId();
      await expect(pixStaking.connect(alice).stake(tokenId)).to.be.revertedWith(
        'Staking: TERRITORY_ONLY',
      );
    });
  });

  describe('epochs', () => {
    before(async function () {
      await pixStaking.setRewardDistributor(await owner.getAddress());
    });

    it('should add new epoch', async function () {
      let daySeconds = 60 * 60 * 24;
      let epochTotalReward = parseEther('100000');
      await pixStaking.connect(owner).addEpochRewards(epochTotalReward, daySeconds);
      expect(await pixStaking.currentEpoch()).to.be.equal(1);
    });

    it('should push rewards to the next epochs after first epoch', async function () {
      // When rewards R are added to a newly initiated epoch, R is splited into equaly sized
      // dividends depending on PIX size of given pool over next months:
      // Domain 12 months
      // Zone    6 months
      // Sector  3 months
      // Area    1 month
      // Additionaly, dividends for a given month will be pushed to the next 1-12 months(The Plow)
      // if there is less than 147 tokens staked in a given pool.
      // The same monthly distribution applies.
      //
      // For example Outlier Sector's reward will be split into 3 dividends of equal size 333,33.
      // Since there is only one staker, 10% is going to be paid out in Month 1, so the
      // rewards for the first month are computed like this : 0,1 * 333,33 = 33,33 IXT
      // The rest is pushed to the following months:
      //   Month 2: 0,9 * 333,33 * 0,5 = 150 IXT
      //   Month 3: 0,9 * 333,33 * 0,3 = 90 IXT
      //   Month 4: 0,9 * 333,33 * 0,2 = 60 IXT
      // Finally, total pushed rewards after the first epoch is initiated are the sum of dividends
      // and Plow
      //   Month 2: 333,33 + 150 = 483,33 IXT
      //   Month 3: 333,33 + 90 = 423,33 IXT
      //   Month 4: 60 IXT

      let epochTotalReward = parseEther('100000');
      let pixSize = PIXSize.Sector;
      let pixCategory = PIXCategory.Outliers;
      let poolIndex = 5;

      let poolMonthlyDividendReward = epochTotalReward
        .mul(rewardPercentages[poolIndex])
        .div(rewardPercentagesDenominator)
        .div(3);
      let percentageToPush =
        rewardPercentagesDenominator - rewardPercentagePaidPerTokensStaked[tokensStaked[poolIndex]];
      let totalRewardToPush = poolMonthlyDividendReward
        .mul(percentageToPush)
        .div(rewardPercentagesDenominator);

      let currentEpoch = await pixStaking.currentEpoch();

      for (let i = 1; i < SectorPushRewards.length; i++) {
        let shouldPush = totalRewardToPush
          .mul(SectorPushRewards[i])
          .div(rewardPercentagesDenominator);
        if (i != 3) {
          shouldPush = poolMonthlyDividendReward.add(shouldPush);
        }
        let pushedInEpoch = await pixStaking.getPushedRewards(
          pixSize,
          pixCategory,
          currentEpoch.add(i),
        );
        expect(pushedInEpoch.sub(shouldPush)).to.be.most(1);
        expect(shouldPush.sub(pushedInEpoch)).to.be.most(1);
      }
    });

    it('should revert if previous epoch not finished', async function () {
      let hourSeconds = BigNumber.from(60 * 60);
      increaseTime(hourSeconds);
      let epochTotalReward = parseEther('100000');
      await expect(
        pixStaking.connect(owner).addEpochRewards(epochTotalReward, hourSeconds),
      ).to.be.revertedWith('Staking: EPOCH_NOT_FINISHED');
    });

    it('should add second epoch after endTime pass', async function () {
      let oneDayOneSecond = BigNumber.from(60 * 60 * 24 + 1);
      increaseTime(oneDayOneSecond);
      let epochTotalReward = parseEther('100000');
      await pixStaking.connect(owner).addEpochRewards(epochTotalReward, oneDayOneSecond);
      expect(await pixStaking.currentEpoch()).to.be.equal(2);
    });
  });

  describe('claim rewards', () => {
    let newUser: Signer;
    let newUserFirstTokenId: BigNumber;
    let newUserTokens: number;
    let epochDuration: number;
    let epochTotalReward: BigNumber;

    before(async function () {
      await _setupNewContracts();

      newUser = (await ethers.getSigners())[5];

      newUserTokens = 3;

      let pixInfos = [];
      for (let i = 0; i < newUserTokens; i++) {
        pixInfos.push([0, sizes[i], categories[i]]);
      }

      await pixNFT.batchMint(await newUser.getAddress(), pixInfos);
      await pixNFT.connect(newUser).setApprovalForAll(pixStaking.address, true);

      let lastTokenId = await pixNFT.lastTokenId();

      newUserFirstTokenId = lastTokenId.sub(newUserTokens).add(1);

      for (let i = 0; i < newUserTokens; i++) {
        await pixStaking.connect(newUser).stake(newUserFirstTokenId.add(i));
      }

      await pixStaking.connect(alice).stake(10);

      epochDuration = 60 * 60 * 24;
      epochTotalReward = parseEther('100000');
      await pixStaking.connect(owner).addEpochRewards(epochTotalReward, epochDuration);
    });

    it('should claim correct amount', async function () {
      let hourSeconds = 60 * 60;
      increaseTime(BigNumber.from(hourSeconds));

      let tokenIds = [newUserFirstTokenId];

      let startBalance = await pixToken.balanceOf(await newUser.getAddress());

      let earned = await pixStaking.connect(newUser).earnedReward(newUserFirstTokenId);

      let tx = await pixStaking.connect(newUser).claimBatch(tokenIds);
      await tx.wait();

      let endBalance = await pixToken.balanceOf(await newUser.getAddress());

      let claimedTokens = endBalance.sub(startBalance);
      let smallValue = ethers.utils.parseEther('1');

      // not equal because few seconds pass between calculations, and we earn more in the meantime
      expect(claimedTokens.sub(earned)).to.be.lte(BigNumber.from(smallValue));
    });

    it('should claim batch', async function () {
      let hourSeconds = 60 * 60;
      increaseTime(BigNumber.from(hourSeconds));

      let tokenIds = [newUserFirstTokenId, newUserFirstTokenId.add(1)];

      let startBalance = await pixToken.balanceOf(await newUser.getAddress());

      let earnedBatch = await pixStaking.connect(newUser).earnedBatch(tokenIds);

      await pixStaking.connect(newUser).claimBatch(tokenIds);

      let endBalance = await pixToken.balanceOf(await newUser.getAddress());

      let claimedTokens = endBalance.sub(startBalance);
      let smallValue = ethers.utils.parseEther('1');

      let totalEarned = BigNumber.from(0);
      for (let earned of earnedBatch) {
        totalEarned = totalEarned.add(earned);
      }

      // not equal because few seconds pass between calculations, and we earn more in the meantime
      expect(claimedTokens.sub(totalEarned)).to.be.lte(BigNumber.from(smallValue));
    });

    it('should claim all', async function () {
      let hourSeconds = 60 * 60;
      increaseTime(BigNumber.from(hourSeconds));

      let earnedAcc = await pixStaking.connect(newUser).earnedByAccount(await newUser.getAddress());

      let startBalance = await pixToken.balanceOf(await newUser.getAddress());

      await pixStaking.connect(newUser).claimAll();

      let endBalance = await pixToken.balanceOf(await newUser.getAddress());

      let claimedTokens = endBalance.sub(startBalance);
      let smallValue = ethers.utils.parseEther('1');

      // not equal because few seconds pass between calculations, and we earn more in the meantime
      expect(claimedTokens.sub(earnedAcc)).to.be.lte(smallValue);
    });

    it('should claim on unstake', async function () {
      let hourSeconds = 60 * 60;
      increaseTime(BigNumber.from(hourSeconds));

      let startBalance = await pixToken.balanceOf(await newUser.getAddress());

      let earned = await pixStaking.connect(newUser).earnedReward(newUserFirstTokenId);

      await pixStaking.connect(newUser).unstake(newUserFirstTokenId);

      let endBalance = await pixToken.balanceOf(await newUser.getAddress());

      let claimedTokens = endBalance.sub(startBalance);
      let smallValue = ethers.utils.parseEther('1');

      // not equal because few seconds pass between calculations, and we earn more in the meantime
      expect(claimedTokens.sub(earned)).to.be.lte(smallValue);
    });

    it('should revert if not your token', async function () {
      await expect(pixStaking.connect(newUser).claimBatch([10])).to.be.revertedWith(
        'Staking: NOT_STAKER',
      );
    });
  });
});
