import {expect} from 'chai';
import {ethers, upgrades} from 'hardhat';
const hre = require("hardhat");
import {Signer, Contract, BigNumber, providers, utils} from 'ethers';
import {PIXCategory, PIXSize} from './utils';
import {time} from '@openzeppelin/test-helpers';
import {address} from "hardhat/internal/core/config/config-validation";
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/signers";

describe('Poisson generator tests', function () {
    let owner: SignerWithAddress;
    let alice: Signer;
    let bob: Signer;
    let generator: Contract;

    beforeEach(async function () {
        this.timeout(4000); // due to token creation loop
        [owner, alice, bob] = await ethers.getSigners();

        let generatorFactory = await ethers.getContractFactory("PoissonResourceGenerator");
        generator = await generatorFactory.deploy();
    });

    describe("Sanity checks", function () {
        it("should be deterministic", async function () {
            let first = await generator.getT(1, 1);
            let second = await generator.getT(1, 1);
            expect(first).to.equal(second);
        });
    });

    describe(".csv dump", function () {
        let N = 20000;
        this.timeout((N/100)*4000);

        it("Dump to .csv", async function () {
            console.log(", T")
            for (let i = 1; i <= N; i++) {
                console.log(i,",", (await generator.getT(2, i, owner.address)).toNumber())
            }
        });
    });

    describe("Check tile", function () {
        beforeEach(async function () {
            await generator.init_token(1);
        });
        it("this shudd werk", async function () {
            let first;
            await increaseTime(11111);
            first = await generator.checkTile(1);
            console.log(first);
            if(first[0] > 1){
                console.log("More than one!")
                await generator.onCollect(1, 1);
                await generator.checkTile(1);
            }
        });
    });

});

async function increaseTime(amount) {
    await hre.network.provider.send("evm_increaseTime", [amount])
    await hre.network.provider.send("evm_mine")
    console.log("EVM time " + amount + " seconds increased!")
}