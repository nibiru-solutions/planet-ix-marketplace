import {expect} from 'chai';
import {ethers, upgrades} from 'hardhat';
import {Contract, BigNumber, utils} from 'ethers';
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/signers";
import "hardhat-gas-reporter";
import {describe} from "mocha";
import { getCurrentTime, increaseTime } from './utils';

describe('Facility Upgrade tests', function () {
    let owner: SignerWithAddress;
    let alice: SignerWithAddress;
    let bob: SignerWithAddress;
    let assetManagerMock: Contract;
    let vrfManagerMock: Contract;
    let baseLevelMock: Contract;
    let ixt:Contract;
    let facilityUpgrade: Contract;

    const M3TAM0D = 0;
    const ASTRO_CREDITS = 8;
    const BLUEPRINT = 9;
    const ENERGY = 24;

    let facilityIds    = [19,20,21,22,23, 52,53,54,55,56, 57,58,59,60,61];
    let facilityLevels = [1, 1, 1, 1, 1,  2, 2, 2, 2, 2,  3, 3, 3, 3, 3 ];
    let upgradeToIds   = [52,53,54,55,56, 57,58,59,60,61, 57,58,59,60,61]; // lvl 3 is max currently

    let upgradeCostLeve1 = {
      tokenIds: [ASTRO_CREDITS, ENERGY, M3TAM0D, BLUEPRINT],
      amounts: [1000, 100, 2, 2],
    }

    beforeEach(async function () {
        [owner, alice, bob] = await ethers.getSigners();

        let assetManagerFactory = await ethers.getContractFactory("ERC1155MockAssetManager");
        assetManagerMock = await assetManagerFactory.deploy("mockUri");
        await assetManagerMock.connect(alice).setTrusted(alice.address, true);

        let vrfManagerFactory = await ethers.getContractFactory("VRFManagerMock");
        vrfManagerMock = await vrfManagerFactory.deploy();

        let baseLevelFactory = await ethers.getContractFactory('BaseLevelMock');
        baseLevelMock = await upgrades.deployProxy(baseLevelFactory);

        const IXTFactory = await ethers.getContractFactory('ERC20Mock');
        ixt = await IXTFactory.deploy('IXT', 'IXT');

        let facilityUpgradeFactory = await ethers.getContractFactory("FacilityUpgrade");
        facilityUpgrade = await upgrades.deployProxy(facilityUpgradeFactory, [
          assetManagerMock.address,
          baseLevelMock.address,
          vrfManagerMock.address,
          ixt.address
        ])

        await facilityUpgrade.setFacilityParams(facilityIds, facilityLevels, upgradeToIds);
        await facilityUpgrade.setLevelUpgradeCost(1, upgradeCostLeve1);

        let maxEnergy = 1500;
        let maxChanceParam = 2845;
        let step = 10;
        let expcParam = 14;
        await facilityUpgrade.calcExponentDoubleUpgradeChance(maxEnergy, maxChanceParam, step, expcParam);

        await assetManagerMock.setTrusted(facilityUpgrade.address, true);
        
        await assetManagerMock.mintBatch(
          alice.address, 
          [ASTRO_CREDITS, ENERGY, M3TAM0D, BLUEPRINT], 
          [1000000000, 1000000000, 1000000000, 1000000000],
          []
        );
        await assetManagerMock.mintBatch(
          bob.address, 
          [ASTRO_CREDITS, ENERGY, M3TAM0D, BLUEPRINT], 
          [1000000000, 1000000000, 1000000000, 1000000000],
          []
        );

        await ixt.mint(alice.address, utils.parseEther("10000"));
        await ixt.mint(bob.address, utils.parseEther("10000"));
        await ixt.connect(alice).approve(facilityUpgrade.address, utils.parseEther("10000"));
        await ixt.connect(bob).approve(facilityUpgrade.address, utils.parseEther("10000"));

        await assetManagerMock.connect(alice).setApprovalForAll(facilityUpgrade.address, true);
        await assetManagerMock.connect(bob).setApprovalForAll(facilityUpgrade.address, true);

        await baseLevelMock.setUserBaseLevel(alice.address, 30);
        await baseLevelMock.setUserBaseLevel(bob.address, 30);
    });

    describe('Place order', () => {

      let facilityIndex = 0;
      let facilityId = facilityIds[facilityIndex];

      beforeEach(async function () {
        await assetManagerMock.mint(alice.address, facilityId, 1);
      });

      it('should place upgrade order and burn cost tokens', async function () {
        let amountsBefore = [];
        for(let i=0; i<upgradeCostLeve1.tokenIds.length; i++) {
          amountsBefore[i] = await assetManagerMock.balanceOf(alice.address, upgradeCostLeve1.tokenIds[i]);
        }

        await facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0);
        
        let amountsAfter = [];
        for(let i=0; i<upgradeCostLeve1.tokenIds.length; i++) {
          amountsAfter[i] = await assetManagerMock.balanceOf(alice.address, upgradeCostLeve1.tokenIds[i]);
          expect(amountsAfter[i]).to.be.equal(amountsBefore[i] - upgradeCostLeve1.amounts[i]);
        }

        expect((await facilityUpgrade.upgradingOrders(1)).player).to.be.equal(alice.address);
        expect(await assetManagerMock.balanceOf(alice.address, facilityId)).to.be.equal(0);
      });

      it('revets if base level is below min', async function () {
        await baseLevelMock.setUserBaseLevel(alice.address, await facilityUpgrade.minBaseLevel() - 1);
        await expect(facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0)).to.be.revertedWith("FacilityUpgrade__BaseLevelUnderMinimum()");
      });

      it('reverts if you already have upgrading order', async function () {
        await facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0);

        await assetManagerMock.mint(alice.address, facilityId, 1);
        await expect(facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0)).to.be.revertedWith("FacilityUpgrade__AlreadyUpgrading(1)");
      });

      it('should have double chance on additional energy added', async function () {
        let additionalEnergy = 100;
        let doubleChanceShouldBe = 371;

        let amountBefore = await assetManagerMock.balanceOf(alice.address, ENERGY);
        let totalEnergyCost = BigNumber.from(upgradeCostLeve1.amounts[1] + additionalEnergy);

        await facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, additionalEnergy);

        expect(await assetManagerMock.balanceOf(alice.address, ENERGY)).to.be.equal(amountBefore.sub(totalEnergyCost));
        expect((await facilityUpgrade.upgradingOrders(1)).doubleUpgradeChance).to.be.equal(doubleChanceShouldBe);
      });

      it('reverts if wrong amount of additional energy passed', async function() {
        let additionalEnergy = 101;
        await expect(facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, additionalEnergy))
          .to.be.revertedWith("FacilityUpgrade__WrongAdditionalEnergy()");
      });
    });

    describe('Claim order', () => {

      let facilityIndex = 0;
      let facilityId = facilityIds[facilityIndex];
      let upgradingTime : BigNumber;

      beforeEach(async function () {
        upgradingTime = await facilityUpgrade.upgradingTime();

        await assetManagerMock.mint(alice.address, facilityId, 1);
        await facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0);
      });

      it('Should be able to claim order', async function () {
        await increaseTime(upgradingTime.add(100));
        await expect(facilityUpgrade.connect(alice).claimOrder(1)).to.emit(facilityUpgrade, "ClaimOrder")
      });

      it('Should be able to call claim batch order function', async function () {
        await increaseTime(upgradingTime.add(100));
        await expect(facilityUpgrade.connect(alice).claimBatchOrder()).to.emit(facilityUpgrade, "ClaimOrder")
      });

      it('Should not be able to call with invalid order id', async function () {
        await assetManagerMock.mint(bob.address, facilityId, 1);
        await facilityUpgrade.connect(bob).placeFacilityUpgradeOrder(facilityId, 0);
        await increaseTime(upgradingTime.add(100));
        await expect(facilityUpgrade.connect(alice).claimOrder(2)).to.be.revertedWith("FacilityUpgrade__InvalidOrderId(2)");
        await expect(facilityUpgrade.connect(bob).claimOrder(1)).to.be.revertedWith("FacilityUpgrade__InvalidOrderId(1)");
      });

      it('Should not be able to claim if order is not finished', async function () {
        await expect(facilityUpgrade.connect(alice).claimOrder(1)).to.be.revertedWith("FacilityUpgrade__OrderNotFinished()");
      });

      it('Should not be able to claim order twice', async function () {
        await increaseTime(upgradingTime.add(100));
        await facilityUpgrade.connect(alice).claimOrder(1);
        await expect(facilityUpgrade.connect(alice).claimOrder(1)).to.be.revertedWith("FacilityUpgrade__InvalidOrderId(1)");
      });

      it('Should be able to speedup order with IXT', async function () {
        await facilityUpgrade.connect(alice).IXTSpeedUpOrder(24, 1);
        let order = await facilityUpgrade.upgradingOrders(1);
        expect(order.baseOrder.speedUpDeductedAmount).to.be.above(BigNumber.from(0));
        expect(order.baseOrder.totalCompletionTime).to.be.equal(upgradingTime.sub(order.baseOrder.speedUpDeductedAmount));

        await expect(facilityUpgrade.connect(alice).claimOrder(1)).to.be.revertedWith("FacilityUpgrade__OrderNotFinished()");
        await facilityUpgrade.connect(alice).IXTSpeedUpOrder(24*6 - 1, 1);
        await expect(facilityUpgrade.connect(alice).claimOrder(1)).to.be.revertedWith("FacilityUpgrade__OrderNotFinished()");
        await facilityUpgrade.connect(alice).IXTSpeedUpOrder(1, 1);

        await expect(facilityUpgrade.connect(alice).claimOrder(1)).to.emit(facilityUpgrade, "ClaimOrder")
      });

      it('Should receive new facility token after vrf is received', async function () {
        await increaseTime(upgradingTime.add(100));
        await facilityUpgrade.connect(alice).claimOrder(1);

        let upgradedFacilityId = upgradeToIds[facilityIndex];
        let balanceBefore = await assetManagerMock.balanceOf(alice.address, upgradedFacilityId);

        await vrfManagerMock.fulfill(1);

        expect(await assetManagerMock.balanceOf(alice.address, upgradedFacilityId)).to.be.equal(balanceBefore.add(1));
      });
    });

    describe('Cooldwon', () => {
      let facilityIndex = 0;
      let facilityId = facilityIds[facilityIndex];
      let cooldownTime : BigNumber;

      beforeEach(async function () {
        cooldownTime = await facilityUpgrade.cooldownTimePerLevel();

        await assetManagerMock.mint(alice.address, facilityId, 2);
        await facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0);
        await increaseTime((await facilityUpgrade.upgradingTime()).add(100));
        await facilityUpgrade.connect(alice).claimOrder(1);
        await vrfManagerMock.fulfill(1);
      });

      it('Should have cooldown', async function() {
        expect(await facilityUpgrade.cooldownEndTimestamp(alice.address))
          .to.be.equal((await getCurrentTime()).add(cooldownTime));
      });

      it('Should not be able to place order while in cooldown', async function() {
        await expect(facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0)).to.be.revertedWith("FacilityUpgrade__CooldownNotFinished()");
      });

      it('Should be able to place order after cooldown', async function () {
        await increaseTime(cooldownTime.add(100));
        await expect(facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0)).to.emit(facilityUpgrade, "PlaceFacilityUpgradeOrder");
      });

      it('Should be able to speedup cooldown', async function () {
        let cooldownEndBefore = await facilityUpgrade.cooldownEndTimestamp(alice.address);
        let speedUpTime = await facilityUpgrade.cooldownSpeedupTime();

        await facilityUpgrade.connect(alice).speedUpCooldown(24);

        expect(await facilityUpgrade.cooldownEndTimestamp(alice.address)).to.be.equal(cooldownEndBefore.sub(speedUpTime.mul(24)));

        await expect(facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0)).to.be.revertedWith("FacilityUpgrade__CooldownNotFinished()");

        await facilityUpgrade.connect(alice).speedUpCooldown(6*24);

        await expect(facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, 0)).to.emit(facilityUpgrade, "PlaceFacilityUpgradeOrder");
      });
    });

    describe.skip("Double upgrade chance", () => {
      let facilityIndex = 0;
      let facilityId = facilityIds[facilityIndex];
      let doubleUpgradeId = facilityIds[facilityIndex + 10];
      let upgradingTime : BigNumber;
      let cooldownTime : BigNumber;

      let additionalEnergyOptions = [0, 100, 300, 500, 1000, 1500];

      beforeEach(async function () {
        await assetManagerMock.mint(alice.address, facilityId, 100000);
        upgradingTime = await facilityUpgrade.upgradingTime();
        cooldownTime = await facilityUpgrade.cooldownTimePerLevel();
      });

      it('monte carlo', async function () {
        let numOfTests = 100;
        let orderId = 1;

        for(let additionalEnergy of additionalEnergyOptions) {
          let doubleUpgradesBefore = await assetManagerMock.balanceOf(alice.address, doubleUpgradeId);

          for(let i=0; i<numOfTests; i++) {
            await facilityUpgrade.connect(alice).placeFacilityUpgradeOrder(facilityId, additionalEnergy);
            await increaseTime(upgradingTime.add(100));
            await facilityUpgrade.connect(alice).claimOrder(orderId);
            await vrfManagerMock.fulfill(orderId);
            await increaseTime(cooldownTime.add(100));

            orderId++;
          }

          let doubleUpgradesAfter = await assetManagerMock.balanceOf(alice.address, doubleUpgradeId);
          let doubleUgprades = doubleUpgradesAfter.sub(doubleUpgradesBefore);
          console.log(`With additional energy ${additionalEnergy} got ${doubleUgprades.toString()} of ${numOfTests} double upgrades`)
        }
      });

      it('calcExponentDoubleUpgradeChance', async function () {

        let maxEnergy = 1500;
        let maxChanceParam = 2845;
        let step = 10;
        let expcParam = 14;

        await facilityUpgrade.calcExponentDoubleUpgradeChance(maxEnergy, maxChanceParam, step, expcParam);

        for(let additionalEnergy=step; additionalEnergy<=maxEnergy; additionalEnergy+=step) {
          console.log(additionalEnergy, (await facilityUpgrade.doubleUpgradeChanceForEnergy(additionalEnergy)).toString());
        }
      });
    });
});