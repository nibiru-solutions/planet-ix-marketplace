import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Contract, BigNumber, utils, constants, Wallet, BigNumberish, Signer } from 'ethers';
import { DENOMINATOR, generateRandomAddress, getCurrentTime, getMerkleTree, PIXCategory, PIXSize } from './utils';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { ArcadeGiveaway, ArcadeGiveawayHandlerERC1155, ArcadeGiveawayHandlerERC1155__factory, ArcadeGiveawayHandlerERC20, ArcadeGiveawayHandlerERC20__factory, ArcadeGiveaway__factory, ERC1155, ERC1155Mock, ERC1155Mock__factory, ERC20Mock, ERC20Mock__factory, ERC721Mock, ERC721Mock__factory, IArcadeGiveawayTokenHandler } from '../src/types';

type Claiming = {
    giveawayHanlder: string;
    tokenId: BigNumberish;
    amount: BigNumberish;
    active: boolean;
};

describe('ArcadeGiveaway', function () {
    let owner: SignerWithAddress;
    let alice: SignerWithAddress;
    let bob: SignerWithAddress;

    let rewardToken: ERC721Mock;
    let erc20token: ERC20Mock;
    let erc1155token: ERC1155Mock;

    let giveaway: ArcadeGiveaway;
    let erc20handler: ArcadeGiveawayHandlerERC20;
    let erc1155handler: ArcadeGiveawayHandlerERC1155;

    beforeEach(async function () {
        [owner, alice, bob] = await ethers.getSigners();

        rewardToken = await new ERC721Mock__factory(owner).deploy("ERC721", "ERC721") as ERC721Mock;

        erc20token = await new ERC20Mock__factory(owner).deploy("ERC20", "ERC20", owner.address, 0) as ERC20Mock;
        erc1155token = await new ERC1155Mock__factory(owner).deploy("ERC1155") as ERC1155Mock;

        giveaway = await upgrades.deployProxy(new ArcadeGiveaway__factory(owner), []) as ArcadeGiveaway;

        erc20handler = await upgrades.deployProxy(new ArcadeGiveawayHandlerERC20__factory(owner), [erc20token.address, giveaway.address]) as ArcadeGiveawayHandlerERC20;
        erc1155handler = await upgrades.deployProxy(new ArcadeGiveawayHandlerERC1155__factory(owner), [erc1155token.address, giveaway.address]) as ArcadeGiveawayHandlerERC1155;
    });
  
    describe('#giveaway same chain rewards', () => {

      let erc20claimAmount = ethers.utils.parseEther("10");
      let erc1155ids = [1, 2];
      let erc1155amounts = [3, 7];

      let erc1155holderAmounts = [10000, 10000];

      let startTokenId = 10;
      let endTokenId = 35;

      let aliceRewardTokenId = startTokenId + 1;

      let claimings: Claiming[];

      beforeEach(async () => {

        await erc20token.mint(erc20handler.address, ethers.utils.parseEther("10000000"));
        await erc1155token.mintBatch(erc1155handler.address, erc1155ids, erc1155holderAmounts, []);
        
        claimings = [
          {
            giveawayHanlder: erc20handler.address,
            tokenId: 0,
            amount: erc20claimAmount,
            active: true
          },
          {
            giveawayHanlder: erc1155handler.address,
            tokenId: erc1155ids[0],
            amount: erc1155amounts[0],
            active: true
          },
          {
            giveawayHanlder: erc1155handler.address,
            tokenId: erc1155ids[1],
            amount: erc1155amounts[1],
            active: true
          }
        ];

        await giveaway.createGiveaway(
          rewardToken.address,
          startTokenId,
          endTokenId,
          false,
          claimings
        );

        await rewardToken.mint(alice.address, aliceRewardTokenId);

      });
  
      it('should have created giveaway', async () => {
        expect(await giveaway.giveawayCount()).to.be.equal(1);
        expect((await giveaway.getClaimings(1)).length).to.be.equal(3);
      });

      it('should be able to do multiple claimings from giveaway', async () => {

        await giveaway.connect(alice).claim(1, [aliceRewardTokenId], [0,1,2]);

        expect(await erc20token.balanceOf(alice.address)).to.be.equal(erc20claimAmount);
        expect(await erc1155token.balanceOf(alice.address, erc1155ids[0])).to.be.equal(erc1155amounts[0]);
        expect(await erc1155token.balanceOf(alice.address, erc1155ids[1])).to.be.equal(erc1155amounts[1]);
      });

      it('should be able to claim with multiple tokens', async () => {

        let additionalTokenId = aliceRewardTokenId+1;
        await rewardToken.mint(alice.address, additionalTokenId);
      
        await giveaway.connect(alice).claim(1, [aliceRewardTokenId, additionalTokenId], [0,1,2]);

        expect(await erc20token.balanceOf(alice.address)).to.be.equal(erc20claimAmount.mul(2));
        expect(await erc1155token.balanceOf(alice.address, erc1155ids[0])).to.be.equal(erc1155amounts[0] * 2);
        expect(await erc1155token.balanceOf(alice.address, erc1155ids[1])).to.be.equal(erc1155amounts[1] * 2);

      });

      it('should not be able to claim twice from claiming', async () => {
        await giveaway.connect(alice).claim(1, [aliceRewardTokenId], [1]);
        await expect(giveaway.connect(alice).claim(1, [aliceRewardTokenId], [1])).to.be.revertedWith("Giveaway: ALREADY_CLAIMED");
      });

      it('should not be able to claim with wrong token id', async () => {
        let tokenIdOut = endTokenId+1;
        await rewardToken.mint(alice.address, tokenIdOut);

        await expect(giveaway.connect(alice).claim(1, [tokenIdOut], [1])).to.be.revertedWith("Giveaway: WRONG_TOKEN_ID");
      });

      it('should not be able to claim without owning token', async () => {
        await expect(giveaway.connect(bob).claim(1, [aliceRewardTokenId], [1])).to.be.revertedWith("Giveaway: NOT_OWNER");
      });

      it('should be able to disable claiming', async () => {

        await giveaway.setClaimingsActive(1, [1], [false]);

        await expect(giveaway.connect(alice).claim(1, [aliceRewardTokenId], [1])).to.be.revertedWith("Giveaway: CLAIMING_NOT_ACTIVE");
      });

      it('should revert on wrong giveaway id', async () => {
        await expect(giveaway.connect(alice).claim(3, [aliceRewardTokenId], [1])).to.be.revertedWith("Giveaway: WRONG_GIVEAWAY_ID");
        await expect(giveaway.connect(alice).claim(0, [aliceRewardTokenId], [1])).to.be.revertedWith("Giveaway: WRONG_GIVEAWAY_ID");
      });

      it('should revert on wrong claiming id', async () => {
        await expect(giveaway.connect(alice).claim(1, [aliceRewardTokenId], [3])).to.be.revertedWith("Giveaway: WRONG_CLAIMING_ID");
      });

      it('should be able to add new claiming in existing giveaway', async () => {
        await giveaway.addClaimingsToGiveaway(1, [
          {
            giveawayHanlder: erc20handler.address,
            tokenId: 0,
            amount: erc20claimAmount,
            active: true
          }
        ]);

        expect((await giveaway.getClaimings(1)).length).to.be.equal(4);

        await giveaway.connect(alice).claim(1, [aliceRewardTokenId], [3]);
        expect(await erc20token.balanceOf(alice.address)).to.be.equal(erc20claimAmount);
      });
    });
  
    describe('#giveaway crosschain rewards with proofs', () => {
      let erc20claimAmount = ethers.utils.parseEther("10");
      let erc1155ids = [1, 2];
      let erc1155amounts = [3, 7];

      let erc1155holderAmounts = [10000, 10000];

      let startTokenId = 10;
      let endTokenId = 35;

      let aliceRewardTokenId = startTokenId + 1;

      let claimings: Claiming[];

      beforeEach(async () => {

        await erc20token.mint(erc20handler.address, ethers.utils.parseEther("10000000"));
        await erc1155token.mintBatch(erc1155handler.address, erc1155ids, erc1155holderAmounts, []);
        
        claimings = [
          {
            giveawayHanlder: erc20handler.address,
            tokenId: 0,
            amount: erc20claimAmount,
            active: true
          },
          {
            giveawayHanlder: erc1155handler.address,
            tokenId: erc1155ids[0],
            amount: erc1155amounts[0],
            active: true
          },
          {
            giveawayHanlder: erc1155handler.address,
            tokenId: erc1155ids[1],
            amount: erc1155amounts[1],
            active: true
          }
        ];

        await giveaway.createGiveaway(
          rewardToken.address,
          startTokenId,
          endTokenId,
          true,
          claimings
        );

        await rewardToken.mint(alice.address, aliceRewardTokenId);

        giveaway.setMintingSigner(owner.address);

      });
  
      it('should have created giveaway', async () => {
        expect(await giveaway.giveawayCount()).to.be.equal(1);
        expect((await giveaway.getClaimings(1)).length).to.be.equal(3);
      });

      it('should not be able to call regular claim function', async () => {
        await expect(giveaway.connect(alice).claim(1, [aliceRewardTokenId], [1])).to.be.revertedWith("Giveaway: NEEDS_PROOF");
      });

      it('should be able to claim with proof', async () => {

        let ttl = (await getCurrentTime()).add(100);
        let proof = await getProof(owner, alice.address, [aliceRewardTokenId], [0,1,2], ttl);

        await giveaway.connect(alice).claimWithProof(1, [aliceRewardTokenId], [0,1,2], proof, ttl);

        expect(await erc20token.balanceOf(alice.address)).to.be.equal(erc20claimAmount);
        expect(await erc1155token.balanceOf(alice.address, erc1155ids[0])).to.be.equal(erc1155amounts[0]);
        expect(await erc1155token.balanceOf(alice.address, erc1155ids[1])).to.be.equal(erc1155amounts[1]);
      });

      it('should not be able to claim with ttl passed', async () => {
        let ttl = (await getCurrentTime()).sub(100);
        let proof = await getProof(owner, alice.address, [aliceRewardTokenId], [0,1,2], ttl);

        await expect(giveaway.connect(alice).claimWithProof(1, [aliceRewardTokenId], [0,1,2], proof, ttl)).to.be.revertedWith("Giveaway: TTL_PASSED");
      });

      it('should not be able to claim with invalid proof', async () => {
        let ttl = (await getCurrentTime()).add(100);
        let proof = await getProof(owner, alice.address, [aliceRewardTokenId], [0,1,2], ttl);

        // invalid token id
        await expect(giveaway.connect(alice).claimWithProof(1, [aliceRewardTokenId + 1], [0,1,2], proof, ttl)).to.be.revertedWith("Giveaway: INVALID_SIGNATURE");
        
        // invlalid claimingIds
        await expect(giveaway.connect(alice).claimWithProof(1, [aliceRewardTokenId], [0,1], proof, ttl)).to.be.revertedWith("Giveaway: INVALID_SIGNATURE");
        
        // invalid msg.sender
        await expect(giveaway.connect(bob).claimWithProof(1, [aliceRewardTokenId], [0,1,2], proof, ttl)).to.be.revertedWith("Giveaway: INVALID_SIGNATURE");

        // invalid ttl
        await expect(giveaway.connect(alice).claimWithProof(1, [aliceRewardTokenId], [0,1,2], proof, ttl.add(1))).to.be.revertedWith("Giveaway: INVALID_SIGNATURE");

        // control if everything is ok
        await expect(giveaway.connect(alice).claimWithProof(1, [aliceRewardTokenId], [0,1,2], proof, ttl)).to.not.be.reverted;
      });

    });

    describe('#test giveaway handlers', () => {
      let erc1155ids = [1, 2];
      let erc1155holderAmounts = [10000, 10000];

      beforeEach(async () => {

        await erc20token.mint(erc20handler.address, ethers.utils.parseEther("10000000"));
        await erc1155token.mintBatch(erc1155handler.address, erc1155ids, erc1155holderAmounts, []);

      });

      it('should be able to withdraw tokens from erc20 handler', async () => {
        let balance = await erc20token.balanceOf(erc20handler.address);

        await erc20handler.connect(owner).withdrawTokens(balance);

        expect(await erc20token.balanceOf(owner.address)).to.be.equal(balance);
      });

      it('should be able to withdraw tokens from erc1155 handler', async () => {
        for(let i=0; i<erc1155ids.length; i++) {
          let balance = await erc1155token.balanceOf(owner.address, erc1155ids[i]);
          await erc1155handler.connect(owner).withdrawTokens(erc1155ids[i], balance);

          expect(await erc1155token.balanceOf(owner.address, erc1155ids[i])).to.be.equal(balance);
        }
      });

    });

    const getProof = async(
      signer: Signer,
      account: string,
      withTokenIds: BigNumberish[],
      claimingIds: BigNumberish[],
      signatureTtl: BigNumberish
    ) => {
      const message = utils.defaultAbiCoder.encode(
        [
          'address',
          'uint256[]',
          'uint256[]',
          'uint256',
        ],
        [account, withTokenIds, claimingIds, signatureTtl],
      );
      const hash = utils.keccak256(message);
      return signer.signMessage(utils.arrayify(hash));
    }
});

