import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Contract, BigNumber } from 'ethers';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { ERC20Mock } from '../src/types/ERC20Mock';
import { ERC1155MockAssetManager } from '../src/types/ERC1155MockAssetManager';
import { GravityGradeMock } from '../src/types/GravityGradeMock';
import { EnergyIXTStakingGG } from '../src/types/EnergyIXTStakingGG';
import { CompilationJobCreationErrorReason } from 'hardhat/types';
import { TokenStaking } from '../src/types';

describe('ENERGY/IXT STAKING - GG POOL', function () {
  let owner: SignerWithAddress;
  let moderator: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;
  let allen_donor: SignerWithAddress;
  let amelia_foundation: SignerWithAddress;
  let energyStakingGG: EnergyIXTStakingGG;
  let ixtMock: ERC20Mock;
  let assetManagerMock: ERC1155MockAssetManager;
  let gravityGrade: GravityGradeMock;
  let staker1: SignerWithAddress;
  let staker2: SignerWithAddress;
  let staker3: SignerWithAddress;
  let staker4: SignerWithAddress;

  beforeEach(async function () {
    [
      owner,
      moderator,
      alice,
      bob,
      allen_donor,
      amelia_foundation,
      staker1,
      staker2,
      staker3,
      staker4,
    ] = await ethers.getSigners();

    const ixtFactory = await ethers.getContractFactory('ERC20Mock');
    ixtMock = await ixtFactory.deploy('Planet IX Token', 'IXT', owner.address, 50);

    const assetManagerFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    assetManagerMock = await assetManagerFactory.deploy('TOY URI');

    const gravityGradeFactory = await ethers.getContractFactory('GravityGradeMock');
    gravityGrade = await gravityGradeFactory.deploy();

    const energyStakingGGFactory = await ethers.getContractFactory('EnergyIXTStakingGG');
    energyStakingGG = (await upgrades.deployProxy(energyStakingGGFactory, [
      gravityGrade.address,
      ixtMock.address,
      assetManagerMock.address,
    ])) as EnergyIXTStakingGG;

    await assetManagerMock.setTrusted(energyStakingGG.address, true);
  });

  describe('Add Rewards', () => {
    let rewardAmount = ethers.utils.parseEther('10000');
    let month = 2_592_000;

    const rewardTokenId = 1;
    const rewardTokenAmount = 10;
    const energyCost = 100;
    const ixtCost = 100;
    const lockupTime = month;
    const concurrentStakerCap = 3;
    const lifeTimePlayerCap = 5;
    const paused = false;

    const gravityGradeRewards = {
      rewardTokenId: rewardTokenId,
      rewardTokenAmount: rewardTokenAmount,
      energyCost: energyCost,
      ixtCost: ixtCost,
      lockupTime: lockupTime,
      concurrentStakerCap: concurrentStakerCap,
      lifeTimePlayerCap: lifeTimePlayerCap,
      paused: paused,
    };

    describe('Add GG Rewards', () => {
      it('should add rewards', async () => {
        const id = await energyStakingGG.callStatic.addReward(gravityGradeRewards);
        const tx = await energyStakingGG.addReward(gravityGradeRewards);
        expect(tx).to.emit(energyStakingGG, 'RewardAdded');

        const rewardJustAdded = await energyStakingGG.ggRewards(id);
        console.log(rewardJustAdded);
      });
      it('should add several rewards', async () => {
        const id1 = await energyStakingGG.callStatic.addReward(gravityGradeRewards);
        const tx1 = await energyStakingGG.addReward(gravityGradeRewards);
        expect(tx1).to.emit(energyStakingGG, 'RewardAdded');

        const id2 = await energyStakingGG.callStatic.addReward(gravityGradeRewards);
        const tx2 = await energyStakingGG.addReward(gravityGradeRewards);
        expect(tx2).to.emit(energyStakingGG, 'RewardAdded');

        const id3 = await energyStakingGG.callStatic.addReward(gravityGradeRewards);
        const tx3 = await energyStakingGG.addReward(gravityGradeRewards);
        expect(tx3).to.emit(energyStakingGG, 'RewardAdded');
      });
    });
    describe('Add GG Rewards Reverts', () => {
      it('Should revert if reward token amount 0', async () => {
        await expect(
          energyStakingGG.addReward({
            rewardTokenId: rewardTokenId,
            rewardTokenAmount: 0,
            energyCost: energyCost,
            ixtCost: ixtCost,
            lockupTime: lockupTime,
            concurrentStakerCap: concurrentStakerCap,
            lifeTimePlayerCap: lifeTimePlayerCap,
            paused: paused,
          }),
        ).to.revertedWith('STAKING: INVALID REWARD AMOUNT');
      });
      it('Should revert if lockup time 0', async () => {
        await expect(
          energyStakingGG.addReward({
            rewardTokenId: rewardTokenId,
            rewardTokenAmount: rewardTokenAmount,
            energyCost: energyCost,
            ixtCost: ixtCost,
            lockupTime: 0,
            concurrentStakerCap: concurrentStakerCap,
            lifeTimePlayerCap: lifeTimePlayerCap,
            paused: paused,
          }),
        ).to.revertedWith('STAKING: LOCKUP TIME ZERO');
      });
      it('Should revert if staker cap 0', async () => {
        await expect(
          energyStakingGG.addReward({
            rewardTokenId: rewardTokenId,
            rewardTokenAmount: rewardTokenAmount,
            energyCost: energyCost,
            ixtCost: ixtCost,
            lockupTime: lockupTime,
            concurrentStakerCap: 0,
            lifeTimePlayerCap: lifeTimePlayerCap,
            paused: paused,
          }),
        ).to.revertedWith('STAKING: STAKER CAP ZERO');
      });
      it('Should revert if staker cap 0', async () => {
        await expect(
          energyStakingGG.addReward({
            rewardTokenId: rewardTokenId,
            rewardTokenAmount: rewardTokenAmount,
            energyCost: energyCost,
            ixtCost: ixtCost,
            lockupTime: lockupTime,
            concurrentStakerCap: concurrentStakerCap,
            lifeTimePlayerCap: 0,
            paused: paused,
          }),
        ).to.revertedWith('STAKING: LIFETIME PLAYER CAP ZERO');
      });
    });
  });

  describe('Staking', () => {
    let month = 2_592_000;

    const rewardTokenId = 1;
    const rewardTokenAmount = 10;
    const energyCost = 100;
    const ixtCost = 100;
    const lockupTime = month;
    const concurrentStakerCap = 3;
    const lifeTimePlayerCap = 5;
    const paused = false;

    const gravityGradeRewards = {
      rewardTokenId: rewardTokenId,
      rewardTokenAmount: rewardTokenAmount,
      energyCost: energyCost,
      ixtCost: ixtCost,
      lockupTime: lockupTime,
      concurrentStakerCap: concurrentStakerCap,
      lifeTimePlayerCap: lifeTimePlayerCap,
      paused: paused,
    };
    const gravityGradeRewards2 = {
      rewardTokenId: 2,
      rewardTokenAmount: rewardTokenAmount,
      energyCost: energyCost,
      ixtCost: ixtCost,
      lockupTime: lockupTime,
      concurrentStakerCap: concurrentStakerCap,
      lifeTimePlayerCap: lifeTimePlayerCap,
      paused: paused,
    };
    const gravityGradeRewards3 = {
      rewardTokenId: 3,
      rewardTokenAmount: rewardTokenAmount,
      energyCost: energyCost,
      ixtCost: ixtCost,
      lockupTime: lockupTime,
      concurrentStakerCap: concurrentStakerCap,
      lifeTimePlayerCap: lifeTimePlayerCap,
      paused: paused,
    };

    const sevenDays = 7 * 24 * 60 * 60;
    const day = 24 * 60 * 60;

    let rewardId;

    let bob_ixt_bal = ethers.utils.parseEther('10000');
    let bob_energy_bal = 10000;

    beforeEach(async () => {
      rewardId = await energyStakingGG.callStatic.addReward(gravityGradeRewards);
      await energyStakingGG.addReward(gravityGradeRewards);
    });
    describe('Staking', () => {
      beforeEach(async () => {
        await ixtMock.mint(bob.address, bob_ixt_bal);
        await assetManagerMock.mint(bob.address, 24, bob_energy_bal);
        await ixtMock.connect(bob).approve(energyStakingGG.address, bob_ixt_bal);
      });
      it('Stakes as expected', async function () {
        const tx = energyStakingGG.connect(bob).stake(rewardId);
        expect(tx).to.emit(energyStakingGG, 'EnergyStaked').withArgs(bob.address, rewardId);
      });
      it('Allows multiple stakers', async () => {
        let randomStaker;
        for (let i; i < concurrentStakerCap; i++) {
          randomStaker = ethers.Wallet.createRandom();
          randomStaker = randomStaker.connect(ethers.provider);

          await ixtMock.mint(randomStaker.address, bob_ixt_bal);
          await assetManagerMock.mint(randomStaker.address, 24, bob_energy_bal);

          let tx = await energyStakingGG.connect(randomStaker).stake(rewardId);
          expect(tx)
            .to.emit(energyStakingGG, 'EnergyStaked ')
            .withArgs(randomStaker.address, rewardId);
        }
      });
      it('Allows user to stake for multiple rewards', async () => {
        let rewardId2 = await energyStakingGG.callStatic.addReward(gravityGradeRewards2);
        await energyStakingGG.addReward(gravityGradeRewards2);
        let rewardId3 = await energyStakingGG.callStatic.addReward(gravityGradeRewards3);
        await energyStakingGG.addReward(gravityGradeRewards3);

        await energyStakingGG.connect(bob).stake(rewardId);
        await energyStakingGG.connect(bob).stake(rewardId2);
        await energyStakingGG.connect(bob).stake(rewardId3);

        let userStaked = await energyStakingGG.getUserStaked(bob.address);
        console.log(userStaked);
        expect(userStaked[0]).to.equal(1);
        expect(userStaked[1]).to.equal(2);
        expect(userStaked[2]).to.equal(3);
      });
      it('allow user to stake again after lockperiod passed and they have claimed', async () => {
        await energyStakingGG.connect(bob).stake(rewardId);

        await ethers.provider.send('evm_increaseTime', [month * 1.5]);
        await ethers.provider.send('evm_mine', []);

        await energyStakingGG.connect(bob).claim(rewardId);

        await expect(energyStakingGG.connect(bob).stake(rewardId)).to.not.reverted;
      });
    });

    describe('Staking Reverts', () => {
      it('should revert if energy balance insufficient', async () => {
        await ixtMock.mint(bob.address, bob_ixt_bal);

        await expect(energyStakingGG.connect(bob).stake(rewardId)).to.revertedWith(
          'ERC1155: burn amount exceeds balance',
        );
      });

      it('should revert if ixt balance insufficient', async () => {
        await assetManagerMock.mint(bob.address, 24, bob_energy_bal);

        await expect(energyStakingGG.connect(bob).stake(rewardId)).to.revertedWith(
          'ERC20: transfer amount exceeds balance',
        );
      });
      it('should revert if reward paused', async () => {
        await energyStakingGG.setRewardState(rewardId, true);

        await expect(energyStakingGG.connect(bob).stake(rewardId)).to.reverted.revertedWith(
          'STAKING: REWARD PAUSED',
        );
      });
      it('should revert if user already has tokens locked and lock period has not finished', async () => {
        await ixtMock.mint(bob.address, bob_ixt_bal);
        await assetManagerMock.mint(bob.address, 24, bob_energy_bal);
        await ixtMock.connect(bob).approve(energyStakingGG.address, bob_ixt_bal);

        await energyStakingGG.connect(bob).stake(rewardId);
        await expect(energyStakingGG.connect(bob).stake(rewardId)).to.revertedWith(
          'STAKING: USER CURRENTLY STAKING',
        );
      });
      it('should revert if too many concurrent stakers', async () => {
        let randomStaker;
        for (let i = 0; i < concurrentStakerCap; i++) {
          randomStaker = ethers.Wallet.createRandom();
          randomStaker = randomStaker.connect(ethers.provider);

          await owner.sendTransaction({
            to: randomStaker.address,
            value: ethers.utils.parseEther('10'),
          });

          await ixtMock.mint(randomStaker.address, bob_ixt_bal);
          await assetManagerMock.mint(randomStaker.address, 24, bob_energy_bal);
          await ixtMock.connect(randomStaker).approve(energyStakingGG.address, bob_ixt_bal);

          await energyStakingGG.connect(randomStaker).stake(rewardId);
        }

        await ixtMock.mint(bob.address, bob_ixt_bal);
        await assetManagerMock.mint(bob.address, 24, bob_energy_bal);
        await ixtMock.connect(bob).approve(energyStakingGG.address, bob_ixt_bal);

        await expect(energyStakingGG.connect(bob).stake(rewardId)).to.revertedWith(
          'STAKING: CAP REACHED',
        );
      });
      it('should revert if surpassing lifetime player cap', async () => {
        let randomStaker;
        for (let i = 0; i < lifeTimePlayerCap; i++) {
          randomStaker = ethers.Wallet.createRandom();
          randomStaker = randomStaker.connect(ethers.provider);

          await owner.sendTransaction({
            to: randomStaker.address,
            value: ethers.utils.parseEther('10'),
          });

          await ixtMock.mint(randomStaker.address, bob_ixt_bal);
          await assetManagerMock.mint(randomStaker.address, 24, bob_energy_bal);
          await ixtMock.connect(randomStaker).approve(energyStakingGG.address, bob_ixt_bal);

          await energyStakingGG.connect(randomStaker).stake(rewardId);

          await ethers.provider.send('evm_increaseTime', [month * 1.5]);
          await ethers.provider.send('evm_mine', []);

          await energyStakingGG.connect(randomStaker).claim(rewardId);
        }

        await ixtMock.mint(bob.address, bob_ixt_bal);
        await assetManagerMock.mint(bob.address, 24, bob_energy_bal);
        await ixtMock.connect(bob).approve(energyStakingGG.address, bob_ixt_bal);

        await expect(energyStakingGG.connect(bob).stake(rewardId)).to.revertedWith(
          'STAKING: LIFETIME CAP REACHED',
        );
      });
      it("should revert if user tries to stake for same reward again after lockperiod is finished but they haven't claimed", async () => {
        await ixtMock.mint(bob.address, bob_ixt_bal);
        await assetManagerMock.mint(bob.address, 24, bob_energy_bal);
        await ixtMock.connect(bob).approve(energyStakingGG.address, bob_ixt_bal);

        await energyStakingGG.connect(bob).stake(rewardId);

        await ethers.provider.send('evm_increaseTime', [month * 1.5]);
        await ethers.provider.send('evm_mine', []);

        await expect(energyStakingGG.connect(bob).stake(rewardId)).to.revertedWith(
          'STAKING: USER CURRENTLY STAKING',
        );
      });
    });
  });

  describe('Claim Rewards', () => {
    let month = 2_592_000;

    const rewardTokenId = 1;
    const rewardTokenAmount = 10;
    const energyCost = 100;
    const ixtCost = 100;
    const lockupTime = month;
    const concurrentStakerCap = 3;
    const lifeTimePlayerCap = 5;
    const paused = false;

    const gravityGradeRewards = {
      rewardTokenId: rewardTokenId,
      rewardTokenAmount: rewardTokenAmount,
      energyCost: energyCost,
      ixtCost: ixtCost,
      lockupTime: lockupTime,
      concurrentStakerCap: concurrentStakerCap,
      lifeTimePlayerCap: lifeTimePlayerCap,
      paused: paused,
    };

    const gravityGradeRewards2 = {
      rewardTokenId: 2,
      rewardTokenAmount: rewardTokenAmount,
      energyCost: energyCost,
      ixtCost: ixtCost,
      lockupTime: lockupTime,
      concurrentStakerCap: concurrentStakerCap,
      lifeTimePlayerCap: lifeTimePlayerCap,
      paused: paused,
    };

    const gravityGradeRewards3 = {
      rewardTokenId: 3,
      rewardTokenAmount: rewardTokenAmount,
      energyCost: energyCost,
      ixtCost: ixtCost,
      lockupTime: lockupTime,
      concurrentStakerCap: concurrentStakerCap,
      lifeTimePlayerCap: lifeTimePlayerCap,
      paused: paused,
    };

    const sevenDays = 7 * 24 * 60 * 60;
    const day = 24 * 60 * 60;

    let rewardId;

    let bob_ixt_bal = ethers.utils.parseEther('10000');
    let bob_energy_bal = 10000;

    describe('Claim Rewards', () => {
      beforeEach(async () => {
        rewardId = await energyStakingGG.callStatic.addReward(gravityGradeRewards);
        await energyStakingGG.addReward(gravityGradeRewards);

        await ixtMock.mint(bob.address, bob_ixt_bal);
        await assetManagerMock.mint(bob.address, 24, bob_energy_bal);
        await ixtMock.connect(bob).approve(energyStakingGG.address, bob_ixt_bal);
        const tx = await energyStakingGG.connect(bob).stake(rewardId);
      });
      it('should claim correct rewards', async function () {
        expect(await ixtMock.balanceOf(bob.address)).to.equal(
          bob_ixt_bal.sub(ethers.utils.parseEther(ixtCost.toString())),
        );

        await ethers.provider.send('evm_increaseTime', [month]);
        await ethers.provider.send('evm_mine', []);

        await energyStakingGG.connect(bob).claim(rewardId);

        await expect(await gravityGrade.balanceOf(bob.address, rewardTokenId)).to.equal(
          rewardTokenAmount,
        );

        expect(await ixtMock.balanceOf(bob.address)).to.equal(bob_ixt_bal);
      });
      it('lets user claim from several different rewards one at a time', async () => {
        let rewardId2 = await energyStakingGG.callStatic.addReward(gravityGradeRewards2);
        await energyStakingGG.addReward(gravityGradeRewards2);
        let rewardId3 = await energyStakingGG.callStatic.addReward(gravityGradeRewards3);
        await energyStakingGG.addReward(gravityGradeRewards3);

        await energyStakingGG.connect(bob).stake(rewardId2);
        await energyStakingGG.connect(bob).stake(rewardId3);

        let userStaked = await energyStakingGG.getUserStaked(bob.address);
        console.log(userStaked);
        expect(userStaked[0]).to.equal(1);
        expect(userStaked[1]).to.equal(2);
        expect(userStaked[2]).to.equal(3);

        await ethers.provider.send('evm_increaseTime', [month]);
        await ethers.provider.send('evm_mine', []);

        await energyStakingGG.connect(bob).claim(rewardId2);
        userStaked = await energyStakingGG.getUserStaked(bob.address);
        console.log(userStaked);

        await energyStakingGG.connect(bob).claim(rewardId3);
        userStaked = await energyStakingGG.getUserStaked(bob.address);
        console.log(userStaked);

        await energyStakingGG.connect(bob).claim(rewardId);
        userStaked = await energyStakingGG.getUserStaked(bob.address);
        console.log(userStaked);

        expect(await gravityGrade.balanceOf(bob.address, rewardId)).to.equal(10);
        expect(await gravityGrade.balanceOf(bob.address, rewardId2)).to.equal(10);
        expect(await gravityGrade.balanceOf(bob.address, rewardId3)).to.equal(10);
      });
      it('lets user claim from several different rewards in one tx', async () => {
        let rewardId2 = await energyStakingGG.callStatic.addReward(gravityGradeRewards2);
        await energyStakingGG.addReward(gravityGradeRewards2);
        let rewardId3 = await energyStakingGG.callStatic.addReward(gravityGradeRewards3);
        await energyStakingGG.addReward(gravityGradeRewards3);

        await energyStakingGG.connect(bob).stake(rewardId2);
        await energyStakingGG.connect(bob).stake(rewardId3);

        expect(await ixtMock.balanceOf(bob.address)).to.equal(
          bob_ixt_bal.sub(ethers.utils.parseEther((3 * ixtCost).toString())),
        );

        let userStaked = await energyStakingGG.getUserStaked(bob.address);
        console.log(userStaked);
        expect(userStaked[0]).to.equal(1);
        expect(userStaked[1]).to.equal(2);
        expect(userStaked[2]).to.equal(3);

        await ethers.provider.send('evm_increaseTime', [month]);
        await ethers.provider.send('evm_mine', []);

        await energyStakingGG.connect(bob).claimAll();

        userStaked = await energyStakingGG.getUserStaked(bob.address);
        console.log(userStaked);

        expect(await gravityGrade.balanceOf(bob.address, rewardId)).to.equal(10);
        expect(await gravityGrade.balanceOf(bob.address, rewardId2)).to.equal(10);
        expect(await gravityGrade.balanceOf(bob.address, rewardId3)).to.equal(10);

        expect(await ixtMock.balanceOf(bob.address)).to.equal(bob_ixt_bal);
      });
    });

    describe('Claim Rewards Reverts', () => {
      beforeEach(async () => {
        rewardId = await energyStakingGG.callStatic.addReward(gravityGradeRewards);
        await energyStakingGG.addReward(gravityGradeRewards);

        await ixtMock.mint(bob.address, bob_ixt_bal);
        await assetManagerMock.mint(bob.address, 24, bob_energy_bal);
        await ixtMock.connect(bob).approve(energyStakingGG.address, bob_ixt_bal);
        const tx = await energyStakingGG.connect(bob).stake(rewardId);
      });
      it('should revert if lockup time hasnt passed yet', async () => {
        await ethers.provider.send('evm_increaseTime', [month / 2]);
        await ethers.provider.send('evm_mine', []);

        await expect(energyStakingGG.connect(bob).claim(rewardId)).to.revertedWith(
          'STAKING: LOCKUP PERIOD NOT OVER',
        );
      });
      it('should revert if user claims twice in a row', async () => {
        await ethers.provider.send('evm_increaseTime', [month]);
        await ethers.provider.send('evm_mine', []);

        await energyStakingGG.connect(bob).claim(rewardId);
        await expect(energyStakingGG.connect(bob).claim(rewardId)).to.revertedWith(
          'STAKING: PLAYER HAS NOT STAKED YET',
        );
      });
      it('should revert if user isnt currently staking', async () => {
        await expect(energyStakingGG.connect(alice).claim(rewardId)).to.revertedWith(
          'STAKING: PLAYER HAS NOT STAKED YET',
        );
      });
    });
  });
});
