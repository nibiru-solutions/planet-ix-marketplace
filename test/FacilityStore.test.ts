import {expect} from 'chai';
import {ethers, network, upgrades} from 'hardhat';
import {Signer, Contract, BigNumber, providers, utils} from 'ethers';
import {advanceTime} from './utils';
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/signers";
import "hardhat-gas-reporter";
import {delay} from "@nomiclabs/hardhat-etherscan/dist/src/etherscan/EtherscanService";
import {describe} from "mocha";

type InitializeParams = {
    _assetManager: string;
    _feeWallet: string;
    _facilityTime: BigNumber;
    _speedupPriceTokenId: BigNumber;
    _speedupPrice: BigNumber;
    _speedupTime: BigNumber;
    _maxOrders: BigNumber;
    _fixedPriceTokenIds: BigNumber[];
    _fixedPriceTokenAmounts: BigNumber[];
    _biomodTokenIds: BigNumber[];
    _biomodPriceAmounts: BigNumber[];
    _facilityTokenIds: BigNumber[];
    _facilityProbabilityWeights: BigNumber[];
    _vrfCoordinator: string;
}

describe('Facility store tests', function () {
    let owner: SignerWithAddress;
    let alice: SignerWithAddress;
    let assetManager: Contract;
    let facilityStore: Contract;
    let vrfCoordinatorMock: Contract;
    let vrfManager: Contract
    let emilCoin: Contract;
    let params: InitializeParams;
    const WASTE_ID = 7;
    const ASTRO_CREDITS = 8;
    const BLUEPRINT = 9;
    const LEGENDARY_BIOMOD = 14;
    const RARE_BIOMOD = 13;
    const UNCOMMON_BIOMOD = 12;
    const COMMON_BIOMOD = 11;
    const OUTLIER_BIOMOD = 10;
    let facility1_id = 21;
    let facility2_id = 22
    let aliceTokenAmount
    let pixtSpeedupSplitBps = 500;
    let pixtSpeedupCost = 500;
    const EMIL_COINT_BALANCE = pixtSpeedupCost * 6;

    beforeEach(async function () {
        this.timeout(70000); // due to token creation loop
        [owner, alice] = await ethers.getSigners();

        let assetManagerFactory = await ethers.getContractFactory("ERC1155MockAssetManager");
        assetManager = await assetManagerFactory.deploy("mockUri");
        await assetManager.connect(alice).setTrusted(alice.address, true);

        let vrfCoordinatorMockFactory = await ethers.getContractFactory("VRFManagerMock");
        vrfCoordinatorMock = await vrfCoordinatorMockFactory.deploy();

        params = {
            _assetManager: assetManager.address,
            _feeWallet: owner.address,
            _facilityTime: BigNumber.from(0),
            _speedupPriceTokenId: BigNumber.from(10),
            _speedupPrice: BigNumber.from(10),
            _speedupTime: BigNumber.from(0),
            _maxOrders: BigNumber.from(5),
            _fixedPriceTokenIds: [BigNumber.from(1), BigNumber.from(2)],
            _fixedPriceTokenAmounts: [BigNumber.from(1000), BigNumber.from(100)],
            _biomodTokenIds: [BigNumber.from(3), BigNumber.from(4)],
            _biomodPriceAmounts: [BigNumber.from(123), BigNumber.from(345)],
            _facilityTokenIds: [BigNumber.from(facility1_id), BigNumber.from(facility2_id)],
            _facilityProbabilityWeights: [BigNumber.from(10), BigNumber.from(100)],
            _vrfCoordinator: vrfCoordinatorMock.address
        }

        aliceTokenAmount = Array(5).fill(BigNumber.from(5000));
        let tokenIds = [...params._fixedPriceTokenIds, ...params._biomodTokenIds, params._speedupPriceTokenId];

        let facilityStoreFactory = await ethers.getContractFactory("FacilityStore");
        facilityStore = await upgrades.deployProxy(facilityStoreFactory, [params])

        let erc20Factory = await ethers.getContractFactory('ERC20Mock');
        emilCoin = await erc20Factory.deploy('EmilCoin', 'MXC', await owner.getAddress(), 0);
        await emilCoin.mint(await alice.getAddress(), EMIL_COINT_BALANCE);

        let baseLevelFactory = await ethers.getContractFactory('BaseLevel');
        let baseLevel = await upgrades.deployProxy(baseLevelFactory);
        await facilityStore.setBaseLevelAddress(baseLevel.address);

        let mockStreamFactory = await ethers.getContractFactory("MockSFStream");
        let mockSFStream = await mockStreamFactory.deploy();
        await mockSFStream.setStream(await alice.getAddress(), 0)

        await baseLevel.setSuperFlowAddress(mockSFStream.address)
        await baseLevel.setSuperFluidPerLevel(385802469135)
        await baseLevel.setOrderCapacity(facilityStore.address, 0, 6, 4)

        await assetManager.connect(alice).setTrusted(params._feeWallet, true);
        await assetManager.connect(alice).setTrusted(facilityStore.address, true);
        await assetManager.connect(alice).trustedBatchMint(alice.address, tokenIds, aliceTokenAmount)

        await assetManager.connect(alice).setApprovalForAll(assetManager.address, true);
        await assetManager.connect(alice).setApprovalForAll(facilityStore.address, true);

    });

    describe("Place Facility Order", function () {
        it("Should emit correct event upon placing order", async function () {
            expect(await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])).to.emit(facilityStore, "RequestClaimFacility");
        })

        it("Should draw correct payment", async function () {
            await facilityStore.connect(alice).placeFacilityOrder([0, 0]);
            for (let i = 0; i < params._fixedPriceTokenIds.length; i++) {
                expect(await assetManager.balanceOf(alice.address, params._fixedPriceTokenIds[i])).to.equal(
                    (BigNumber.from(aliceTokenAmount[0]).sub(params._fixedPriceTokenAmounts[i])))
            }
            for (let i = 0; i < params._biomodTokenIds.length; i++) {
                expect(await assetManager.balanceOf(alice.address, params._biomodTokenIds[i])).to.equal(
                    (BigNumber.from(aliceTokenAmount[0]).sub(params._biomodPriceAmounts[i])))
            }
        })

        it("Should return correct order struct", async function () {
            let tx = await facilityStore.connect(alice).placeFacilityOrder([0, 0]);
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);
            for (const it of (await facilityStore.getOrders(alice.address))) {
                if (it["orderId"] == orderId) {
                    expect(it["orderAmount"]).to.equal(1);
                    let time = (await ethers.provider.getBlock(tx.blockHash)).timestamp
                    expect(it["createdAt"]).to.equal(time)
                }
            }
        })

        it("Should return correct order structs", async function () {
            let tx1 = await facilityStore.connect(alice).placeFacilityOrder([0, 0]);
            let tx2 = await facilityStore.connect(alice).placeFacilityOrder([0, 0]);
            let orders = await facilityStore.getOrders(alice.address);
            expect(orders.length).to.equal(2);
            expect(orders[0]["createdAt"]).to.not.equal(orders[1]["createdAt"])
        })

        it("Should draw correct payment when committing extra biomods", async function () {
            let extra = [1, 2]
            await facilityStore.connect(alice).placeFacilityOrder(extra);
            for (let i = 0; i < params._fixedPriceTokenIds.length; i++) {
                expect(await assetManager.balanceOf(alice.address, params._fixedPriceTokenIds[i])).to.equal(
                    (BigNumber.from(aliceTokenAmount[0]).sub(params._fixedPriceTokenAmounts[i])))
            }
            for (let i = 0; i < params._biomodTokenIds.length; i++) {
                expect(await assetManager.balanceOf(alice.address, params._biomodTokenIds[i])).to.equal(
                    (BigNumber.from(aliceTokenAmount[0]).sub(params._biomodPriceAmounts[i].add(BigNumber.from(extra[i])))))
            }
        })

        it("Should deposit correct fee amount to fee wallet", async function () {
            let fee = 1000;
            await facilityStore.setFeePercentage(fee);
            let expectedFee = (params._fixedPriceTokenAmounts[0].mul(BigNumber.from(fee))).div(10000);
            await facilityStore.connect(alice).placeFacilityOrder([0, 0]);
            expect(await assetManager.balanceOf(owner.address, params._fixedPriceTokenIds[0])).to.equal(expectedFee)
        })
    })

    describe("Claim order", function () {

        it("should emit event", async function () {
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);

            //await advanceTime(150);
            await network.provider.send('evm_increaseTime', [150]);
            expect(await facilityStore.connect(alice).claimOrder(orderId)).to.emit(facilityStore, "RequestClaimFacility");
        }).timeout(10000);

        it("Should be able to claim facility", async function () {
            let initial = (await assetManager.balanceOf(alice.address, facility1_id))
            initial = initial.add(await assetManager.balanceOf(alice.address, facility2_id))
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);
            //await advanceTime(150);
            await network.provider.send('evm_increaseTime', [150]);
            await facilityStore.connect(alice).claimOrder(orderId);
            vrfCoordinatorMock.connect(alice).fulfill(1)
            let posterior = (await assetManager.balanceOf(alice.address, facility1_id))
            posterior = posterior.add(await assetManager.balanceOf(alice.address, facility2_id))
            expect(posterior.sub(initial)).to.equal(1)
        })

        it("Should remove order from orders list", async function () {
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            let beforeLength = (await facilityStore.connect(alice).getOrders(alice.address)).length;
            expect(beforeLength).to.equal(1)
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);
            //await advanceTime(150);
            await network.provider.send('evm_increaseTime', [150]);
            await facilityStore.connect(alice).claimOrder(orderId);
            vrfCoordinatorMock.connect(alice).fulfill(1)
            let afterLength = (await facilityStore.connect(alice).getOrders(alice.address)).length;
            expect(afterLength).to.equal(0)

        })

        it("should only get 1 facility when trying to claim same order multiple times.", async function () {
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);

            await network.provider.send('evm_increaseTime', [150]);
            // Deterministic to get token id facility1_id bc fixed random seed
            expect(await assetManager.balanceOf(alice.address, facility1_id)).equal(0)
            facilityStore.connect(alice).claimOrder(orderId)
            facilityStore.connect(alice).claimOrder(orderId)
            vrfCoordinatorMock.connect(alice).fulfill(1)
            vrfCoordinatorMock.connect(alice).fulfill(1)
            vrfCoordinatorMock.connect(alice).fulfill(2)
            await delay(4000)
            expect(await assetManager.balanceOf(alice.address, facility2_id)).equal(1)
        }).timeout(10000);

        it("emits event when batch claiming", async function () {
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            let tx2 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);
            await network.provider.send('evm_increaseTime', [150]);
            await expect(facilityStore.connect(alice).claimBatchOrder()).to.emit(facilityStore, "RequestClaimFacility");
        }).timeout(10000);

        it("Should give all facilties when batch claiming", async function () {
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let initial = (await assetManager.balanceOf(alice.address, facility1_id))
            initial = initial.add(await assetManager.balanceOf(alice.address, facility2_id))
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            let tx2 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            let tx3 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            await network.provider.send('evm_increaseTime', [150]);
            facilityStore.connect(alice).claimBatchOrder()
            vrfCoordinatorMock.connect(alice).fulfill(1)
            vrfCoordinatorMock.connect(alice).fulfill(2)
            vrfCoordinatorMock.connect(alice).fulfill(3)
            let posterior = (await assetManager.balanceOf(alice.address, facility1_id))
            posterior = posterior.add(await assetManager.balanceOf(alice.address, facility2_id))
            expect(posterior.sub(initial)).to.equal(3)

        })

        it("Should clear orders list when batch claiming", async function () {
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            let tx2 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            let tx3 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            let beforeLength = (await facilityStore.connect(alice).getOrders(alice.address)).length;
            expect(beforeLength).to.equal(3)
            await network.provider.send('evm_increaseTime', [150]);
            facilityStore.connect(alice).claimBatchOrder()
            vrfCoordinatorMock.connect(alice).fulfill(1)
            vrfCoordinatorMock.connect(alice).fulfill(2)
            vrfCoordinatorMock.connect(alice).fulfill(3)
            let afterLength = (await facilityStore.connect(alice).getOrders(alice.address)).length;
            expect(afterLength).to.equal(0)

        })

    })

    describe("Claim order reverts", function () {
        it("should revert when trying to claim same order multiple times", async function () {
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            let tx2 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);

            await network.provider.send('evm_increaseTime', [150]);
            facilityStore.connect(alice).claimOrder(orderId)
            await expect(facilityStore.connect(alice).claimOrder(orderId)).to.be.revertedWith(
                'InvalidOrderId',
            );
        }).timeout(10000);

        it("should revert with no orders", async function () {
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(100));
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);

            //await advanceTime(150);
            await network.provider.send('evm_increaseTime', [150]);
            facilityStore.connect(alice).claimOrder(orderId)
            await expect(facilityStore.connect(alice).claimOrder(orderId)).to.be.revertedWith(
                'NoOrders',
            );
        }).timeout(10000);

        it("Should revert on invalid order id", async function () {
            await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            await expect(facilityStore.connect(alice).claimOrder(999)).to.revertedWith("InvalidOrderId(999)")
        })

        it("Should revert on no orders when batch claiming", async function () {
            await expect(facilityStore.connect(alice).claimBatchOrder()).to.revertedWith("NoOrders(\"0x70997970C51812dc3A010C7d01b50e0d17dc79C8\")")
        })
    })

    describe("Speedup", function () {
        let speedupPriceTokenId
        let speedupPrice
        let speedupTime
        let facilityTime = BigNumber.from(100)
        beforeEach(async function () {

            await facilityStore.connect(owner).setFacilityTime(facilityTime);
            speedupPriceTokenId = BigNumber.from("10");
            speedupPrice = BigNumber.from("10");
            speedupTime = BigNumber.from("10");
            await facilityStore.connect(owner).setSpeedupParameters(speedupPriceTokenId, speedupPrice, speedupTime);
        })
        it("should be able to speed up and claim facility", async function () {
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])

            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);

            let balanceBefore = await assetManager.balanceOf(alice.address, params._speedupPriceTokenId);
            let numSpeedUps = BigNumber.from("15");

            expect(await facilityStore.connect(alice).speedUpOrder(numSpeedUps, 0)).to.emit(facilityStore, "SpeedUpConstruction").withArgs(alice.address, orderId, numSpeedUps);
            expect(await facilityStore.connect(alice).claimOrder(orderId)).to.emit(facilityStore, "RequestClaimFacility");

            let balanceAfter = await assetManager.balanceOf(alice.address, params._speedupPriceTokenId);

            expect(balanceBefore.sub(balanceAfter)).equal(BigNumber.from(speedupPrice.mul(numSpeedUps)));
        }).timeout(10000);

        it("should be able to speed up and claim facility 2", async function () {
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])

            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let orderId = _getOrderIdFromReceipt(receipt, 3);

            let balanceBefore = await assetManager.balanceOf(alice.address, params._speedupPriceTokenId);
            let numSpeedUps = BigNumber.from("1");
            await network.provider.send('evm_increaseTime', [99]);

            expect(await facilityStore.connect(alice).speedUpOrder(numSpeedUps, 0)).to.emit(facilityStore, "SpeedUpConstruction").withArgs(alice.address, orderId, numSpeedUps);
            expect(await facilityStore.connect(alice).claimOrder(orderId)).to.emit(facilityStore, "RequestClaimFacility");

            let balanceAfter = await assetManager.balanceOf(alice.address, params._speedupPriceTokenId);

            expect(balanceBefore.sub(balanceAfter)).equal(BigNumber.from(speedupPrice.mul(numSpeedUps)));
        }).timeout(10000);

    })

    describe('Speed up order with erc20', () => {
        let orderAmount = 50;
        let speedUpTime = 100
        let numSpeedups = 1;
        let orderId = 0;
        let defaultOrderTime;
        let playerAddress;
        beforeEach(async () => {
            await facilityStore.setIXTSpeedUpParams(
                emilCoin.address,
                emilCoin.address, //isn't used yet.
                pixtSpeedupSplitBps,
                pixtSpeedupCost
            )
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(speedUpTime));
            await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
            await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
        });
        it('Emits event', async () => {
            await emilCoin.connect(alice).approve(facilityStore.address, numSpeedups * pixtSpeedupCost);
            await expect(facilityStore.connect(alice).IXTSpeedUpOrder(numSpeedups, orderId))
                .to.emit(facilityStore, "SpeedUpConstruction").withArgs(alice.address, orderId, numSpeedups);

        });
        it('Draws coins as expected', async () => {
            let priorBalance = await emilCoin.balanceOf(alice.address)
            await emilCoin.connect(alice).approve(facilityStore.address, numSpeedups * pixtSpeedupCost);
            await expect(facilityStore.connect(alice).IXTSpeedUpOrder(numSpeedups, orderId)).to.not.reverted
            expect(priorBalance - (await emilCoin.balanceOf(alice.address))).to.equal(numSpeedups * pixtSpeedupCost)
        });
        it('Deposits expected amount into contract', async () => {
            let priorBalance = await emilCoin.balanceOf(facilityStore.address)
            await emilCoin.connect(alice).approve(facilityStore.address, numSpeedups * pixtSpeedupCost);
            await expect(facilityStore.connect(alice).IXTSpeedUpOrder(numSpeedups, 0)).to.not.reverted
            expect((await emilCoin.balanceOf(facilityStore.address)) - priorBalance).to.equal(BigNumber.from(numSpeedups * pixtSpeedupCost * (10000 - pixtSpeedupSplitBps)).div(BigNumber.from(10000)))
        });
    });
    describe('Revert Speed up order with erc20', () => {
        let orderAmount = 50;
        let speedUpTime = 100
        let numSpeedups = 1;
        let orderId = 0;
        let defaultOrderTime;
        let playerAddress;
        beforeEach(async () => {
            defaultOrderTime = await facilityStore._facilityTime
            await facilityStore.connect(owner).setFacilityTime(BigNumber.from(speedUpTime));
            await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)])
        });
        it('Revert when IXTParams is not set', async () => {
            await expect(facilityStore.connect(alice).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith("IXT address not set")
        });
        it('Revert when not enough erc20 in player wallet', async () => {
            await facilityStore.setIXTSpeedUpParams(
                emilCoin.address,
                emilCoin.address,
                pixtSpeedupSplitBps,
                pixtSpeedupCost
            )
            await emilCoin.connect(alice).transfer(owner.address, EMIL_COINT_BALANCE)
            //await expect(facilityStore.connect(alice).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith("Transfer of funds failed") // should be this...
            await expect(facilityStore.connect(alice).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith("ERC20: transfer amount exceeds balance")
        });
    });

    describe("Compound/misc", async function () {
        it("should be able to place, claim and recieve facility order", async function () {
            let tx = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)]);
            let tx2 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)]);
            let tx3 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)]);
            let tx4 = await facilityStore.connect(alice).placeFacilityOrder([BigNumber.from(5), BigNumber.from(3)]);
            const receipt = await ethers.provider.getTransactionReceipt(tx.hash);
            let reqId = 1//_getRequestIdFromReceipt(receipt, 3);
            await facilityStore.connect(alice).claimOrder(0)
            await facilityStore.connect(alice).claimOrder(1)
            await facilityStore.connect(alice).claimOrder(2)
            await facilityStore.connect(alice).claimOrder(3)
            console.log("balance before: ", await assetManager.balanceOf(alice.address, 6))
            expect(await vrfCoordinatorMock.connect(alice).fulfill(reqId)).to.emit(facilityStore, "ReceiveFacility");
            expect(await vrfCoordinatorMock.connect(alice).fulfill(reqId + 1)).to.emit(facilityStore, "ReceiveFacility");
            expect(await vrfCoordinatorMock.connect(alice).fulfill(reqId + 2)).to.emit(facilityStore, "ReceiveFacility");
            expect(await vrfCoordinatorMock.connect(alice).fulfill(reqId + 3)).to.emit(facilityStore, "ReceiveFacility");
            console.log("balance after: ", await assetManager.balanceOf(alice.address, 6))
        });


        it("should be able to get Facility Fixed Tokens Price", async function () {
            let prices = await facilityStore.connect(owner).getFacilityFixedTokensPrice();
            expect(prices[1][0]).to.equal(1000);
        });

        it("Should return correct Bio Mod price", async function () {
            let prices = await facilityStore.connect(owner).getFacilityBiomodTokensPrice();
            // this is a lazy ass test
            expect(prices[1][0]).to.equal(123)
        })

        it("Should revert on non gov setting asset manager", async function () {
            await expect(facilityStore.connect(alice).setAssetManager(alice.address)).to.revertedWith("OnlyGovCanCall")
        })

        it("Should revert on non gov setting facility time", async function () {
            await expect(facilityStore.connect(alice).setFacilityTime(1)).to.revertedWith("OnlyGovCanCall")

        })

        it("Should revert on non gov setting max orders", async function () {
            await expect(facilityStore.connect(alice).setMaxOrders(1)).to.revertedWith("OnlyGovCanCall")
        })
        it("Should revert on non gov setting speed params", async function () {
            await expect(facilityStore.connect(alice).setSpeedupParameters(1, 1, 1)).to.revertedWith("OnlyGovCanCall")
        })

        it("Should revert on non gov setting fee percentage", async function () {
            await expect(facilityStore.connect(alice).setFeePercentage(100)).to.revertedWith("OnlyGovCanCall")

        })
        it("Should revert on non gov setting prices", async function () {
            await expect(facilityStore.connect(alice).setFixedPriceTokenIds([1], [2])).to.revertedWith("OnlyGovCanCall")
            await expect(facilityStore.connect(alice).setBiomodTokenPrices([1], [2])).to.revertedWith("OnlyGovCanCall")
        })

        it("Should revert on non gov setting facility token ids n weights", async function () {
            await expect(facilityStore.connect(alice).setFacilityTokenIdsAndWeights([1], [2])).to.revertedWith("OnlyGovCanCall")
        })

        it("Should revert on non gov setting vrf coordinator", async function () {
            await expect(facilityStore.connect(alice).setVrfCoordinator(alice.address)).to.revertedWith("OnlyGovCanCall")
        })

        it("Should revert on non gov setting moderator", async function () {
            await expect(facilityStore.connect(alice).setModerator(alice.address)).to.revertedWith("OnlyGovCanCall")
        })
    })

    describe('Monte carlo print tests', function () {
        /// todo gotta fix assertions here but i am getting too tired
        beforeEach(async function () {
            await facilityStore.setFacilityTime(1)
            await facilityStore.setFixedPriceTokenIds([1], [10])
            await facilityStore.setBiomodTokenPrices([2, 3], [10, 10])
            await facilityStore.setFacilityTokenIdsAndWeights([facility1_id, facility2_id], [10, 10])
        })
        it("Should give expected number of facilities", async function () {
            let N = 1000
            await assetManager.trustedBatchMint(alice.address, [1, 2, 3], [N * 10, N * 20, N * 20])
            for (let i = 0; i < N; i++) {
                let tx = await facilityStore.connect(alice).placeFacilityOrder([0, 0])
                let receipt = await ethers.provider.getTransactionReceipt(tx.hash);
                let orderId = _getOrderIdFromReceipt(receipt, 3);
                await network.provider.send('evm_increaseTime', [2]);
                await facilityStore.connect(alice).claimOrder(orderId);
                let reqId = i + 1
                await vrfCoordinatorMock.connect(alice).fulfill(reqId);
            }
            let type1 = await assetManager.balanceOf(alice.address, facility1_id);
            let type2 = await assetManager.balanceOf(alice.address, facility2_id);
            console.log(type1, type2)
        }).timeout(60000)

        it("Should give expected number of facilities when adding more biomods", async function () {
            let N = 1000
            await assetManager.trustedBatchMint(alice.address, [1, 2, 3], [N * 10, N * 20, N * 20])
            for (let i = 0; i < N; i++) {
                let tx = await facilityStore.connect(alice).placeFacilityOrder([1, 0])
                let receipt = await ethers.provider.getTransactionReceipt(tx.hash);
                let orderId = _getOrderIdFromReceipt(receipt, 3);
                await network.provider.send('evm_increaseTime', [2]);
                await facilityStore.connect(alice).claimOrder(orderId);
                let reqId = i + 1
                await vrfCoordinatorMock.connect(alice).fulfill(reqId);
            }
            let type1 = await assetManager.balanceOf(alice.address, facility1_id);
            let type2 = await assetManager.balanceOf(alice.address, facility2_id);
            console.log(type1, type2)
        }).timeout(60000)

        it("Should give expected number of facilities when adding more biomods 2", async function () {
            let N = 1000

            await facilityStore.setFixedPriceTokenIds([1], [10])
            await facilityStore.setBiomodTokenPrices([2, 3, 4], [10, 10, 10])
            await facilityStore.setFacilityTokenIdsAndWeights([facility1_id, facility2_id, facility2_id + 1], [10, 10, 10])
            await assetManager.trustedBatchMint(alice.address, [1, 2, 3, 4], [N * 10, N * 20, N * 20, N * 20])
            for (let i = 0; i < N; i++) {
                let tx = await facilityStore.connect(alice).placeFacilityOrder([1, 0, 0])
                let receipt = await ethers.provider.getTransactionReceipt(tx.hash);
                let orderId = _getOrderIdFromReceipt(receipt, 3);
                await network.provider.send('evm_increaseTime', [2]);
                await facilityStore.connect(alice).claimOrder(orderId);
                let reqId = i + 1
                await vrfCoordinatorMock.connect(alice).fulfill(reqId);
            }
            let type1 = await assetManager.balanceOf(alice.address, facility1_id);
            let type2 = await assetManager.balanceOf(alice.address, facility2_id);
            let type3 = await assetManager.balanceOf(alice.address, facility2_id + 1);
            console.log(type1, type2, type3)
        }).timeout(60000)

    });


});

function _getOrderIdFromReceipt(receipt: providers.TransactionReceipt, eventId: number): string {
    let eventInterface = "event FacilityOrderPlaced(address indexed user, uint256 indexed orderId)";
    let eventName = "FacilityOrderPlaced";
    let event = _decodeEventFromReceipt(receipt, eventInterface, eventName, eventId);
    return event.orderId.toString();
}

function _getRequestIdFromReceipt(receipt: providers.TransactionReceipt, eventId: number): string {
    let eventInterface = "event NewRequest(uint id)";
    let eventName = "NewRequest";
    let event = _decodeEventFromReceipt(receipt, eventInterface, eventName, eventId);
    return event.id;
}

function _decodeEventFromReceipt(
    receipt: providers.TransactionReceipt,
    eventInterface: string,
    eventName: string,
    eventId: number
): utils.Result {
    const evInterface = new ethers.utils.Interface([eventInterface]);
    const data = receipt.logs[eventId].data;
    const topics = receipt.logs[eventId].topics;
    const event = evInterface.decodeEventLog(eventName, data, topics);
    return event;
}