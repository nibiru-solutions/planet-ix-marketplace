import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Signer, Contract, utils, BigNumber } from 'ethers';
import { advanceTime } from './utils';
import { parseEther } from 'ethers/lib/utils';

describe('MetashareStakingEth', function () {
  let owner: Signer;
  let alice: Signer;
  let bob: Signer;
  let pixToken: Contract;
  let corporations: Contract;
  let shareStakingPoly: Contract;
  let shareStakingEth: Contract;
  let connext: Contract;

  beforeEach(async function () {
    [owner, alice, bob] = await ethers.getSigners();

    const ConnextFactory = await ethers.getContractFactory('MockConnext');
    connext = await ConnextFactory.deploy();

    const PIXTFactory = await ethers.getContractFactory('PIXT');
    pixToken = await PIXTFactory.deploy();

    const CorporationsFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    corporations = await CorporationsFactory.deploy('uri');

    const PIXMetashareStakingPolygon = await ethers.getContractFactory('MetashareStaking');
    const PIXMetashareStakingFactory = await ethers.getContractFactory('MetashareStakingEth');

    shareStakingPoly = await upgrades.deployProxy(PIXMetashareStakingPolygon, [pixToken.address, corporations.address, connext.address]);

    shareStakingEth = await upgrades.deployProxy(PIXMetashareStakingFactory, [1, corporations.address, connext.address, shareStakingPoly.address]);

    await pixToken.transfer(shareStakingPoly.address, utils.parseEther('1000000'));
    await shareStakingPoly.setEthAddress(shareStakingEth.address);
    await corporations.connect(alice).mint(await alice.getAddress(), 1, 20);
    await corporations.connect(bob).mint(await bob.getAddress(), 1, 20);
    await pixToken.approve(shareStakingPoly.address, utils.parseEther('1000000'));
    await corporations.connect(alice).setApprovalForAll(shareStakingEth.address, true);
    await corporations.connect(bob).setApprovalForAll(shareStakingEth.address, true);
    await shareStakingPoly.setRewardDistributor(await owner.getAddress());
    await shareStakingEth.setRewardDistributor(await owner.getAddress());
  });

  describe('stake', () => {
    it('should stake an NFT', async function () {
      await shareStakingEth.connect(alice).stake(5);
      expect(await corporations.balanceOf(await alice.getAddress(), 1)).to.equal(15);
      expect(await shareStakingEth.getStakedAmounts(await alice.getAddress())).to.equal(5);
    });
  });

  describe('initializePools', () => {
    it('revert if msg.sender is not reward distributor', async function () {
      await expect(shareStakingEth.connect(alice).initializePools(1000, 86400)).to.revertedWith('Staking: NON_DISTRIBUTOR');
    });

    it('should deposit the correct staking rewards', async function () {
      await shareStakingEth.initializePools(1000, 86400);
      // expect(await pixToken.balanceOf(await shareStaking.address)).to.equal(1000);
    });
  });

  describe('claim', () => {
    beforeEach(async function () {
      let monthSeconds = 60 * 60 * 24 * 30;
      let epochTotalReward = parseEther('1000');
      await shareStakingEth.initializePools(epochTotalReward, monthSeconds);
      // Stake an NFT from Alice
      await shareStakingEth.connect(alice).stake(5);
    });

    it('rewardPerTokenFromNow is correct', async function () {
      const oneDayFromNow = Math.floor(Date.now() / 1000) + 86400;
      const rpt = await shareStakingEth.rewardPerTokenFromNow(oneDayFromNow);
      expect(rpt.toString().startsWith('6666')).to.equal(true);
      expect(rpt.toString().length).to.equal(19);
    });

    it('rewardPerTokenFromNow is correct with 2 staked users', async function () {
      await shareStakingEth.connect(bob).stake(5);
      const oneDayFromNow = Math.floor(Date.now() / 1000) + 86400;
      const rpt = await shareStakingEth.rewardPerTokenFromNow(oneDayFromNow);
      expect(rpt.toString().startsWith('333')).to.equal(true);
      expect(rpt.toString().length).to.equal(19);
    });

    it('should return the correct staking amount', async function () {
      expect(await shareStakingEth.getStakedAmounts(await alice.getAddress())).to.equal(5);
    });

    it('should claim correct rewards amount', async function () {
      await advanceTime(60 * 60 * 24);
      await shareStakingEth.connect(alice).claimOnPolygon(0);
      let expectedDailyReward = parseEther('33.3333333333332928');
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedDailyReward))).to.be.most(1);
    });

    it('should claim all rewards amount after epoch end', async function () {
      await advanceTime(60 * 60 * 24 * 30);
      await shareStakingEth.connect(alice).claimOnPolygon(0);
      let expectedMonthlyReward = parseEther('999.999614197529648195');
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedMonthlyReward))).to.be.most(1);
    });

    it('should claim correct amount of rewards after second epoch epoch end', async function () {
      let monthSeconds = 60 * 60 * 24 * 30;
      let epochTotalReward = parseEther('1000');
      await advanceTime(monthSeconds);
      await shareStakingEth.initializePools(epochTotalReward, monthSeconds);
      await shareStakingEth.connect(bob).stake(5);
      await advanceTime(monthSeconds);
      await shareStakingEth.connect(alice).claimOnPolygon(0);
      await shareStakingEth.connect(bob).claimOnPolygon(0);
      let expectedAlicesReward = parseEther('1499.999807098763608090');
      let expectedBobsReward = parseEther('499.999807098764824095');
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedAlicesReward))).to.be.most(1);
      expect(Math.abs((await pixToken.balanceOf(await bob.getAddress())).sub(expectedBobsReward))).to.be.most(1);
    });

    it('should be able to claim from polygon', async function () {
      await owner.sendTransaction({
        to: shareStakingEth.address,
        value: parseEther('10'),
      });
      await advanceTime(60 * 60 * 24);
      await shareStakingPoly.connect(alice).claimOnEthereum(10, 10, { value: 10 });
      let expectedDailyReward = parseEther('33.3333333333332928');
      const after = await pixToken.balanceOf(await alice.getAddress());

      expect(Math.abs(after.sub(expectedDailyReward))).to.be.most(BigNumber.from('800000000000000'));
    });

    it('should be able to claim from ethereum', async function () {
      await advanceTime(60 * 60 * 24);
      await shareStakingEth.connect(alice).claimOnPolygon(10, { value: 10 });
      let expectedDailyReward = parseEther('33.3333333333332928');
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedDailyReward))).to.be.most(1);
    });
  });

  describe('unstake', () => {
    beforeEach(async function () {
      // Stake an NFT from Alice
      await shareStakingEth.connect(alice).stake(5);
    });

    it('revert if msg.sender is not staker', async function () {
      await expect(shareStakingEth.unstake(10, 0)).to.revertedWith('MetashareStaking: NOT_ENOUGH');
    });

    it('revert if msg.sender is trying to unstake more than he staked', async function () {
      await expect(shareStakingEth.connect(alice).unstake(10, 0)).to.revertedWith('MetashareStaking: NOT_ENOUGH');
    });

    it('should return the correct staking amount', async function () {
      await shareStakingEth.connect(alice).unstake(3, 0);
      expect(await shareStakingEth.getStakedAmounts(await alice.getAddress())).to.equal(2);
    });

    it('should stake again', async function () {
      await shareStakingEth.connect(alice).unstake(5, 0);
      await shareStakingEth.connect(alice).stake(10);
      expect(await corporations.balanceOf(await alice.getAddress(), 1)).to.equal(10);
    });
  });

  describe('unstake cross chain', () => {
    beforeEach(async function () {
      // Stake an NFT from Alice
      let monthSeconds = 60 * 60 * 24 * 30;
      let epochTotalReward = parseEther('1000');
      await shareStakingEth.initializePools(epochTotalReward, monthSeconds);
      await shareStakingEth.connect(alice).stake(5);
    });

    it('unstake triggers claim on polygon', async function () {
      await advanceTime(60 * 60 * 24);
      await shareStakingEth.connect(alice).unstake(5, 5, { value: 5 });
      const after = await pixToken.balanceOf(await alice.getAddress());
      expect(after.toString()).to.equal('33333333333333292800');
    });
  });
});
