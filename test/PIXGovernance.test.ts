import { expect, use } from "chai";
import { Contract, ContractFactory, utils, Wallet } from "ethers";
import {
  deployContract,
  deployMockContract,
  MockProvider,
  solidity,
} from "ethereum-waffle";
import { ethers, upgrades } from "hardhat";
import { PIXGovernanceERC20__factory, PIXT__factory } from "../src/types";

use(solidity);

const MONTH = 2592000;
const WEEK = 604800;
const ZERO = 0;
const ONE = 1;
const DELEGATE_AMOUNT = 20;
const NON_EMPTY_STRING = "NON EMPTY STRING";
const EMPTY_STRING = "";
const HUNDRED = 100;
const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";
const COMPLETED = 2;

describe("PIXGovernance", async () => {

  let pixToken: Contract;
  let pixGovernance : Contract;

  beforeEach(async function () {
    const [deployer] = await ethers.getSigners();

    pixToken = await deployMockContract(deployer, PIXT__factory.abi);

    pixGovernance = await upgrades.deployProxy(
      new PIXGovernanceERC20__factory(deployer),
      [pixToken.address, WEEK, MONTH]
    );
  });

  context("Stake and Vote functionalities", function () {
    it("User succesfully stakes tokens", async function () {
      const [user] = await ethers.getSigners();
      const amount = utils.parseEther(HUNDRED.toString());

      await pixToken.mock.approve.returns(true);
      await pixToken.mock.transferFrom.returns(true);

      await (
        await pixToken
          .connect(user)
          .approve(pixGovernance.address, amount)
      ).wait();

      await (await pixGovernance.stake(amount)).wait();

      const votingPower = await pixGovernance.votingPower(user.address);

      expect(votingPower).to.be.equal(
        amount,
        "Tokens are not staked successfully"
      );
    });

    it("Staking fails due to token transfer fail", async function () {
      const [user] = await ethers.getSigners();
      const amount = utils.parseEther(HUNDRED.toString());

      await pixToken.mock.approve.returns(true);
      await pixToken.mock.transferFrom.revertsWithReason(
        "Revert message"
      );

      await (
        await pixToken
          .connect(user)
          .approve(pixGovernance.address, amount)
      ).wait();

      await expect(pixGovernance.stake(amount)).to.be.revertedWith(
        "Revert message"
      );
    });

    it("User votes successfully", async function () {
      const [user] = await ethers.getSigners();
      const amount = utils.parseEther(HUNDRED.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(amount);

      const pid = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();
      await (await pixGovernance.stake(amount)).wait();

      const votingPowerStakedBefore =
        await pixGovernance.votingPowerStaked(user.address);
      const votingPowerBefore = await pixGovernance.votingPower(
        user.address
      );

      await (await pixGovernance.vote(pid, true)).wait();

      const votingPowerStakedAfter = await pixGovernance.votingPowerStaked(
        user.address
      );
      const votingPowerAfter = await pixGovernance.votingPower(
        user.address
      );

      expect(votingPowerAfter).to.be.equal(
        ZERO,
        "Voting power is not updated correctly."
      );

      expect(votingPowerStakedAfter).to.be.equal(
        votingPowerStakedBefore.add(votingPowerBefore),
        "Staked voting power is not updated correctly."
      );
    });

    it("User tries to vote and fail due to invalid pid and proposal ending", async function () {
      const amount = utils.parseEther(HUNDRED.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(amount);

      const pid = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();
      await (await pixGovernance.stake(amount)).wait();

      await expect(
        pixGovernance.vote(pid.add(ONE), true)
      ).to.be.revertedWith("GOV: INVALID_PID");

      const period = await pixGovernance.period();
      await ethers.provider.send("evm_increaseTime", [period.toNumber()]);

      await expect(pixGovernance.vote(pid, true)).to.be.revertedWith(
        "GOV: PROPOSAL_ENDED"
      );
    });
  });

  context("Delegation tests", function () {
    it("User successfully delegates power to another user", async function () {
      const [firstUser, secondUser] = await ethers.getSigners();

      const stakingAmount = utils.parseEther(HUNDRED.toString());
      const delegateAmount = utils.parseEther(DELEGATE_AMOUNT.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(stakingAmount);

      await (await pixGovernance.stake(stakingAmount)).wait();

      const votingPowerBefore = await pixGovernance.votingPower(
        firstUser.address
      );
      const delegatedPowerBefore = await pixGovernance.delegatedPower(
        secondUser.address
      );
      const delegatedAmountBefore = await pixGovernance.delegatedAmount(
        firstUser.address,
        secondUser.address
      );

      await (
        await pixGovernance.delegatePower(
          secondUser.address,
          delegateAmount
        )
      ).wait();

      const votingPowerAfter = await pixGovernance.votingPower(
        firstUser.address
      );
      const delegatedPowerAfter = await pixGovernance.delegatedPower(
        secondUser.address
      );
      const delegatedAmountAfter = await pixGovernance.delegatedAmount(
        firstUser.address,
        secondUser.address
      );

      expect(votingPowerAfter).to.be.equal(
        votingPowerBefore.sub(delegateAmount),
        "Voting power is not updated correctly."
      );
      expect(delegatedPowerAfter).to.be.equal(
        delegatedPowerBefore.add(delegateAmount),
        "Delegated power is not updated correctly."
      );
      expect(delegatedAmountAfter).to.be.equal(
        delegatedAmountBefore.add(delegateAmount),
        "Delegated amount is not updated correctly."
      );
    });

    it("User tries to delegates power and fails due to invalid user and invalid amount", async function () {
      const [firstUser, secondUser] = await ethers.getSigners();

      const amount = utils.parseEther(HUNDRED.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(amount);

      const pid = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();
      await (await pixGovernance.stake(amount)).wait();

      await expect(
        pixGovernance.delegatePower(ZERO_ADDRESS, amount)
      ).to.revertedWith("GOV: INVALID_USER");

      await expect(
        pixGovernance.delegatePower(secondUser.address, amount.add(ONE))
      ).to.revertedWith("GOV: INVALID_AMOUNT");
    });

    it("User successfully removes delegate. Delegate didn't use votes", async function () {
      const [firstUser, secondUser] = await ethers.getSigners();

      const stakingAmount = utils.parseEther(HUNDRED.toString());
      const delegateAmount = utils.parseEther(DELEGATE_AMOUNT.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(stakingAmount);

      await (await pixGovernance.stake(stakingAmount)).wait();
      await (
        await pixGovernance.delegatePower(
          secondUser.address,
          delegateAmount
        )
      ).wait();

      const votingPowerBefore = await pixGovernance.votingPower(
        firstUser.address
      );
      const delegatedPowerBefore = await pixGovernance.delegatedPower(
        secondUser.address
      );
      const delegatedAmountBefore = await pixGovernance.delegatedAmount(
        firstUser.address,
        secondUser.address
      );

      await (
        await pixGovernance.removeDelegate(secondUser.address)
      ).wait();

      const votingPowerAfter = await pixGovernance.votingPower(
        firstUser.address
      );
      const delegatedPowerAfter = await pixGovernance.delegatedPower(
        secondUser.address
      );
      const delegatedAmountAfter = await pixGovernance.delegatedAmount(
        firstUser.address,
        secondUser.address
      );

      expect(votingPowerAfter).to.be.equal(
        votingPowerBefore.add(delegatedAmountBefore),
        "Voting power is not updated correctly."
      );
      expect(delegatedPowerAfter).to.be.equal(
        delegatedPowerBefore.sub(delegatedAmountBefore),
        "Delegated power is not updated correctly."
      );
      expect(delegatedAmountAfter).to.be.equal(
        ZERO,
        "Delegated amount is not updated correctly."
      );
    });

    it("User successfully removes delegate. Delegate used votes", async function () {
      const [firstUser, secondUser] = await ethers.getSigners();

      const stakingAmount = utils.parseEther(HUNDRED.toString());
      const delegateAmount = utils.parseEther(DELEGATE_AMOUNT.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(stakingAmount);

      await (await pixGovernance.stake(stakingAmount)).wait();
      await (
        await pixGovernance.delegatePower(
          secondUser.address,
          delegateAmount
        )
      ).wait();

      const pid = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();

      await (
        await pixGovernance.connect(secondUser).vote(pid, true)
      ).wait();

      const votingPowerBefore = await pixGovernance.votingPower(
        firstUser.address
      );
      const delegatePowerStakedBefore =
        await pixGovernance.delegatedPowerStaked(secondUser.address);
      const delegatedAmountBefore = await pixGovernance.delegatedAmount(
        firstUser.address,
        secondUser.address
      );

      const lockPeriod = await pixGovernance.lockPeriod();
      await ethers.provider.send("evm_increaseTime", [lockPeriod.toNumber()]);

      await (
        await pixGovernance.removeDelegate(secondUser.address)
      ).wait();

      const votingPowerAfter = await pixGovernance.votingPower(
        firstUser.address
      );
      const delegatePowerStakedAfter =
        await pixGovernance.delegatedPowerStaked(secondUser.address);
      const delegatedAmountAfter = await pixGovernance.delegatedAmount(
        firstUser.address,
        secondUser.address
      );

      expect(votingPowerAfter).to.be.equal(
        votingPowerBefore.add(delegatedAmountBefore),
        "Voting power is not updated correctly."
      );
      expect(delegatePowerStakedAfter).to.be.equal(
        delegatePowerStakedBefore.sub(delegatedAmountBefore),
        "Delegated power is not updated correctly."
      );
      expect(delegatedAmountAfter).to.be.equal(
        ZERO,
        "Delegated amount is not updated correctly."
      );
    });

    it("User tries to remove delegate and fails due to locking period not expired", async function () {
      const [firstUser, secondUser] = await ethers.getSigners();

      const stakingAmount = utils.parseEther(HUNDRED.toString());
      const delegateAmount = utils.parseEther(DELEGATE_AMOUNT.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(stakingAmount);

      await (await pixGovernance.stake(stakingAmount)).wait();
      await (
        await pixGovernance.delegatePower(
          secondUser.address,
          delegateAmount
        )
      ).wait();

      const pid = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();

      await (
        await pixGovernance.connect(secondUser).vote(pid, true)
      ).wait();

      await expect(
        pixGovernance.removeDelegate(secondUser.address)
      ).to.be.revertedWith("GOV: LOCKED");
    });

    it("User withdraws voting power after delegate votes for him", async function () {
      const [firstUser, secondUser] = await ethers.getSigners();

      const stakingAmount = utils.parseEther(HUNDRED.toString());
      const delegateAmount = utils.parseEther(DELEGATE_AMOUNT.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(stakingAmount);

      await (await pixGovernance.stake(stakingAmount)).wait();
      await (
        await pixGovernance.delegatePower(
          secondUser.address,
          delegateAmount
        )
      ).wait();

      const pid = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();

      await (
        await pixGovernance.connect(secondUser).vote(pid, true)
      ).wait();

      const votingPowerBefore = await pixGovernance.votingPower(
        firstUser.address
      );
      const votingPowerStakedBefore =
        await pixGovernance.votingPowerStaked(firstUser.address);
      const delegatedPowerBefore = await pixGovernance.delegatedPower(
        firstUser.address
      );
      const delegatedPowerStakedBefore =
        await pixGovernance.delegatedPowerStaked(firstUser.address);

      const lockPeriod = await pixGovernance.lockPeriod();
      await ethers.provider.send("evm_increaseTime", [lockPeriod.toNumber()]);

      await (await pixGovernance.withdrawVotingPower()).wait();

      const votingPowerAfter = await pixGovernance.votingPower(
        firstUser.address
      );
      const votingPowerStakedAfter = await pixGovernance.votingPowerStaked(
        firstUser.address
      );
      const delegatedPowerAfter = await pixGovernance.delegatedPower(
        firstUser.address
      );
      const delegatedPowerStakedAfter =
        await pixGovernance.delegatedPowerStaked(firstUser.address);

      expect(votingPowerAfter).to.be.equal(
        votingPowerBefore.add(votingPowerStakedBefore),
        "Voting power is not updated correctly."
      );
      expect(votingPowerStakedAfter).to.be.equal(
        ZERO,
        "Delegated power is not updated correctly."
      );
      expect(delegatedPowerAfter).to.be.equal(
        delegatedPowerBefore.add(delegatedPowerStakedBefore),
        "Delegated amount is not updated correctly."
      );
      expect(delegatedPowerStakedAfter).to.be.equal(
        ZERO,
        "Delegated power is not updated correctly."
      );
    });
  });

  context("Pause and Proposal Tests", function () {
    it("User tries to unpause not paused contract", async function () {
      await expect(pixGovernance.unpause()).to.be.revertedWith(
        "Pausable: not paused"
      );
    });

    it("User fails to create proposal when contract is paused", async function () {
      await (await pixGovernance.pause()).wait();
      await expect(
        pixGovernance.createProposal(NON_EMPTY_STRING, NON_EMPTY_STRING)
      ).to.be.revertedWith("Pausable: paused");
    });

    it("User unpauses paused contract and successfully creates proposal", async function () {
      await (await pixGovernance.pause()).wait();
      await expect(
        pixGovernance.createProposal(NON_EMPTY_STRING, NON_EMPTY_STRING)
      ).to.be.revertedWith("Pausable: paused");
      await (await pixGovernance.unpause()).wait();

      await pixToken.mock.balanceOf.returns(
        utils.parseEther(HUNDRED.toString())
      );

      const pidBefore = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();
      const pidAfter = await pixGovernance.pid();

      expect(pidAfter).to.be.equal(
        pidBefore.add(ONE),
        "Proposal is not created"
      );
    });

    it("User tries to create proposal and fails due to insufficient balance, invalid title and invalid description", async function () {
      await pixToken.mock.balanceOf.returns(
        utils.parseEther(ZERO.toString())
      );

      await expect(
        pixGovernance.createProposal(NON_EMPTY_STRING, NON_EMPTY_STRING)
      ).to.be.revertedWith("GOV: INSUFFICIENT_BALANCE");

      await pixToken.mock.balanceOf.returns(
        utils.parseEther(HUNDRED.toString())
      );

      await expect(
        pixGovernance.createProposal(EMPTY_STRING, NON_EMPTY_STRING)
      ).to.be.revertedWith("GOV: INVALID_TITLE");

      await expect(
        pixGovernance.createProposal(NON_EMPTY_STRING, EMPTY_STRING)
      ).to.be.revertedWith("GOV: INVALID_DESCRIPTION");
    });

    it("Complete proposal fails due to invalid pid, period not ended and proposal being active", async function () {
      await pixToken.mock.balanceOf.returns(
        utils.parseEther(HUNDRED.toString())
      );

      const pid = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();

      await expect(
        pixGovernance.completeProposal(pid.add(ONE))
      ).to.be.revertedWith("GOV: INVALID_PID");

      await expect(pixGovernance.completeProposal(pid)).to.be.revertedWith(
        "GOV: PERIOD_NOT_ENDED"
      );

      const period = await pixGovernance.period();
      await ethers.provider.send("evm_increaseTime", [period.toNumber()]);

      await expect(pixGovernance.completeProposal(pid)).to.be.revertedWith(
        "GOV: NOT_ACTIVEproposal"
      );
    });

    it("Owner completes proposal successfully", async function () {
      const amount = utils.parseEther(HUNDRED.toString());

      await pixToken.mock.transferFrom.returns(true);
      await pixToken.mock.balanceOf.returns(amount);

      const pid = await pixGovernance.pid();
      await (
        await pixGovernance.createProposal(
          NON_EMPTY_STRING,
          NON_EMPTY_STRING
        )
      ).wait();
      await (await pixGovernance.stake(amount)).wait();
      await (await pixGovernance.vote(pid, true)).wait();

      const period = await pixGovernance.period();
      await ethers.provider.send("evm_increaseTime", [period.toNumber()]);
      await (await pixGovernance.completeProposal(pid)).wait();

      const proposal = await pixGovernance.proposals(pid);
      expect(proposal.status).to.be.equal(
        COMPLETED,
        "Proposal is not completed successfully."
      );
    });
  });
});
