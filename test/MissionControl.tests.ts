import { expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { Signer, Contract, BigNumber, providers, utils } from 'ethers';
import { PIXCategory, PIXSize, AssetIds } from './utils';
import { constants } from '@openzeppelin/test-helpers';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import {
  ERC1155,
  ERC1155MockAssetManager,
  ERC721MCStakeableAdapter,
  ERC721MockPIX,
  MCCrosschainServices,
  MissionControl,
  MissionControlStaking,
  MissionControlTileStatus,
  Mock1155FT,
  MockMissionControlMintable,
  NewlandsGenesisLockProvider,
  PIXAssetStakeableAdapter,
  Rover,
  RoverMCStakeableAdapter,
  RoverMock,
  TileContractLockProvider,
} from '../src/types';

interface order {
  x: number;
  y: number;
  z: number;
}
interface placeOrder {
  order: order;
  tokenId: number;
  tokenAddress: string;
}

describe('Mission Control', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let streamer: SignerWithAddress;
  let bob: SignerWithAddress;
  let missionControl: MissionControl;
  let mintable: MockMissionControlMintable;
  let stakeable_base: Contract;
  let stakeable_top: Contract;
  let stakeable_base_big_waste: Contract;
  let notWhitelisted: Contract;
  let missionControlStaking: MissionControlStaking;
  let mockTileContracts: Mock1155FT;
  let mcCrosschainServices: MCCrosschainServices;
  let missionControlTileStatus: MissionControlTileStatus;

  const aGold = '0x3CAD7147c15C0864B8cF0EcCca43f98735e6e782';
  const aGoldLite = '0x39161eb4Ce381d92a472Dfc88dA033700C14D49D';

  const tile_price = 385802469135;

  beforeEach(async function () {
    // this.timeout(4000); // due to token creation loop
    [owner, alice, bob, streamer] = await ethers.getSigners();

    let mintableFactory = await ethers.getContractFactory('MockMissionControlMintable');
    mintable = await mintableFactory.deploy();
    let mcFactory = await ethers.getContractFactory('MissionControl');
    missionControl = (await upgrades.deployProxy(mcFactory, [2, mintable.address])) as MissionControl;

    let stakeableFactory = await ethers.getContractFactory('MockMissionControlStakeable');
    stakeable_base = await stakeableFactory.deploy(false, true);
    stakeable_top = await stakeableFactory.deploy(false, false);
    notWhitelisted = await stakeableFactory.deploy(false, true);

    let crosschainFactory = await ethers.getContractFactory('MCCrosschainServices');
    mcCrosschainServices = (await upgrades.deployProxy(crosschainFactory, [])) as MCCrosschainServices;

    let missionControlTileStatusFactory = await ethers.getContractFactory('MissionControlTileStatus');
    missionControlTileStatus = (await upgrades.deployProxy(missionControlTileStatusFactory, [])) as MissionControlTileStatus;
    missionControlTileStatus.setMC(missionControl.address);

    stakeable_base_big_waste = await stakeableFactory.deploy(false, true);
    stakeable_base_big_waste.setReward(10, 7);

    await missionControl.setWhitelist(stakeable_base.address, true);
    await missionControl.setWhitelist(stakeable_top.address, true);
    await missionControl.setWhitelist(stakeable_base_big_waste.address, true);
    await stakeable_top.setReward(2, 1);
    await missionControl.setStreamer(streamer.address);

    for (let i = 1; i <= 6; i++) {
      // Doing this loop here makes the tests very slow to run, but it's fine for now
      await stakeable_base.setOwner(i, await alice.getAddress());
      await stakeable_top.setOwner(i, await alice.getAddress());
      await notWhitelisted.setOwner(i, await alice.getAddress());
      await stakeable_base_big_waste.setOwner(i, await alice.getAddress());
    }
    await stakeable_base.setOwner(7, await bob.getAddress());

    await missionControl.setAGold(aGold);
    await missionControl.setAGoldLite(aGoldLite);

    let stakingFactory = await ethers.getContractFactory('MissionControlStaking');
    missionControlStaking = (await upgrades.deployProxy(stakingFactory, [])) as MissionControlStaking;

    let tileContractFactory = await ethers.getContractFactory('Mock1155FT');
    mockTileContracts = await tileContractFactory.deploy('TOY URI');

    mockTileContracts.mint(alice.address, 35, 10000);
    mockTileContracts.mint(alice.address, 36, 10000);

    await missionControl.setMissionControlStaking(missionControlStaking.address);
    await missionControl.setTileContracts(mockTileContracts.address);
    await missionControl.setCrosschainServices(mcCrosschainServices.address);
    await mockTileContracts.connect(alice).setApprovalForAll(missionControlStaking.address, true);

    await missionControl.setTileRequirements(0, -2, tile_price, 36);
    await missionControl.setTileRequirements(1, -2, tile_price, 36);
    await missionControl.setTileRequirements(2, -2, tile_price, 36);
    await missionControl.setTileRequirements(2, -1, tile_price, 36);
    await missionControl.setTileRequirements(2, 0, tile_price, 36);
    await missionControl.setTileRequirements(1, 1, tile_price, 36);

    await missionControl.setTileRequirements(0, -1, 0, 35);
    await missionControl.setTileRequirements(1, -1, 0, 35);
    await missionControl.setTileRequirements(1, 0, 0, 35);
    await missionControl.setTileRequirements(0, 1, 0, 35);
    await missionControl.setTileRequirements(-1, 1, 0, 35);
    await missionControl.setTileRequirements(-1, 0, 0, 35);

    await missionControlStaking.whitelistTokens(mockTileContracts.address, [35], true);
    await missionControlStaking.whitelistTokens(mockTileContracts.address, [36], true);
    await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 35, 1, 0);
    await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);
  });

  describe('Place NFT', function () {
    it('Stakes base', async function () {
      const orderPlacement: placeOrder = { order: { x: -1, y: 0, z: 1 }, tokenId: 1, tokenAddress: stakeable_base.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);
      expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
      expect(await stakeable_base.isStaked(1)).to.equal(true);

      const stakedOnTile = await missionControl.checkStakedOnTile(alice.address, -1, 0, 1);
      expect(stakedOnTile['_base']['stakeable']).to.equal(stakeable_base.address);
      expect(stakedOnTile['_base']['tokenId']).to.equal(1);
      expect(stakedOnTile['_base']['nonce']).to.equal(1);

      const tileRentalInfo = await missionControl.tileRentalInfo(alice.address, -1, 0);
      expect(tileRentalInfo['isRented']).to.equal(false);
      expect(tileRentalInfo['pausedAt']).to.equal(0);
      expect(tileRentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);

      const allTileInfo = await missionControlTileStatus.getAllTileInfo(alice.address);
      const filtered = allTileInfo.filter(
        (item) => item['base']['stakeable'] === stakeable_base.address,
        // item['base']['nonce'] == BigNumber.from(1),
      );
      // console.log(filtered[0]['base']['tokenId']);
      // console.log(BigNumber.from(1));
      expect(filtered).to.not.empty;
    });
    it('Stakes top', async function () {
      const orderPlacement: placeOrder = { order: { x: -1, y: 0, z: 1 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacement1: placeOrder = { order: { x: -1, y: 0, z: 1 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await mockTileContracts.connect(alice).setApprovalForAll(missionControlStaking.address, true);

      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement1);
      expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
      expect(await stakeable_top.ownerOf(1)).to.equal(await stakeable_top.address);
      expect(await stakeable_base.isStaked(1)).to.equal(true);
      expect(await stakeable_top.isStaked(1)).to.equal(true);

      const stakedOnTile = await missionControl.checkStakedOnTile(alice.address, -1, 0, 1);
      expect(stakedOnTile['_base']['stakeable']).to.equal(stakeable_base.address);
      expect(stakedOnTile['_base']['tokenId']).to.equal(1);
      expect(stakedOnTile['_base']['nonce']).to.equal(1);

      const tileRentalInfo = await missionControl.tileRentalInfo(alice.address, -1, 0);
      expect(tileRentalInfo['isRented']).to.equal(false);
      expect(tileRentalInfo['pausedAt']).to.equal(0);
      expect(tileRentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);

      const allTileInfo = await missionControlTileStatus.getAllTileInfo(alice.address);
      const filtered = allTileInfo.filter((item) => item['base']['stakeable'] === stakeable_base.address);
      expect(filtered).to.not.empty;
    });
    it('Should allow two different players to stake on the same tile in their respective miniverse', async function () {
      mockTileContracts.mint(bob.address, 35, 10000);
      mockTileContracts.mint(bob.address, 36, 10000);

      await mockTileContracts.connect(bob).setApprovalForAll(missionControlStaking.address, true);

      await missionControlStaking.connect(bob).stakeSemiFungible(mockTileContracts.address, 35, 1, 0);
      await missionControlStaking.connect(bob).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);

      const orderPlacement: placeOrder = { order: { x: -1, y: 0, z: 1 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacement1: placeOrder = { order: { x: -1, y: 0, z: 1 }, tokenId: 7, tokenAddress: stakeable_base.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(bob).placeNFT(orderPlacement1);
      expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
      expect(await stakeable_base.ownerOf(7)).to.equal(await stakeable_base.address);
    });
    it('Allows staking of multiple NFTs on different tiles', async function () {
      let count = 1;
      for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
          for (let k = -1; k < 2; k++) {
            if (i + j + k == 0 && (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2 == 1) {
              let orderPlacement: placeOrder = { order: { x: i, y: j, z: k }, tokenId: count, tokenAddress: stakeable_base.address };

              await missionControl.connect(alice).placeNFT(orderPlacement);
              expect(await stakeable_base.ownerOf(1)).to.equal(stakeable_base.address);
              expect(await stakeable_base.isStaked(1)).to.equal(true);
              count += 1;
            }
          }
        }
      }
      count = 1;
      for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
          for (let k = -1; k < 2; k++) {
            if (i + j + k == 0 && (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2 == 1) {
              let orderPlacement: placeOrder = { order: { x: i, y: j, z: k }, tokenId: count, tokenAddress: stakeable_top.address };

              await missionControl.connect(alice).placeNFT(orderPlacement);
              expect(await stakeable_top.ownerOf(1)).to.equal(stakeable_top.address);
              expect(await stakeable_top.isStaked(1)).to.equal(true);
              count += 1;
            }
          }
        }
      }
    });
  });

  describe('Contract Tiles', () => {
    const RENT_PER_TILE = 385802469135;
    let orders: order[];

    describe('Create Rentals', () => {
      beforeEach(async () => {
        orders = [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
        ];
      });
      it('Creates a new rental as expected', async () => {
        const flowRate = orders.length * RENT_PER_TILE;

        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0]], RENT_PER_TILE);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        const rentalInfo = await missionControl.tileRentalInfo(alice.address, orders[0].x, orders[0].y);

        expect(userRentals[0]['x']).to.equal(0);
        expect(userRentals[0]['y']).to.equal(-2);
        expect(userRentals[0]['z']).to.equal(2);

        expect(rentalInfo['isRented']).to.equal(true);
        expect(rentalInfo['pausedAt']).to.equal(0);
        expect(rentalInfo['rentalToken']).to.equal(aGold);
      });
      it('Should not allow user to rent the same tile twice', async () => {
        const flowRate = RENT_PER_TILE;

        await expect(missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[0]], flowRate)).to.revertedWith('MC__TileRented()');

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        const rentalInfo = await missionControl.tileRentalInfo(alice.address, orders[0].x, orders[0].y);
        expect(userRentals).to.empty;

        expect(rentalInfo['isRented']).to.equal(false);
        expect(rentalInfo['pausedAt']).to.equal(0);
        expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);
      });
      it('Should rent several different tiles in one tx', async () => {
        const flowRate = RENT_PER_TILE * 6;

        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, flowRate);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        for (let i; i < userRentals.length; i++) {
          expect(userRentals[i]['x']).to.equal(orders[i].x);
          expect(userRentals[i]['y']).to.equal(orders[i].y);
          expect(userRentals[i]['z']).to.equal(orders[i].z);

          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[i]['x'], userRentals[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }
      });
    });
    describe('Update Rentals', () => {
      let orders: order[];
      beforeEach(async () => {
        orders = [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
        ];

        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], RENT_PER_TILE * 3);
      });
      it('updates a user stream by adding more rentals', async () => {
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [orders[3]], [], RENT_PER_TILE * 3, RENT_PER_TILE * 4);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        for (let i = 0; i < userRentals.length; i++) {
          expect(userRentals[i]['x']).to.equal(orders[i].x);
          expect(userRentals[i]['y']).to.equal(orders[i].y);
          expect(userRentals[i]['z']).to.equal(orders[i].z);

          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[i]['x'], userRentals[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }
        expect(userRentals[3]['x']).to.equal(orders[3].x);
        expect(userRentals[3]['y']).to.equal(orders[3].y);
        expect(userRentals[3]['z']).to.equal(orders[3].z);

        let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[3]['x'], userRentals[3]['y']);
        expect(rentalInfo['isRented']).to.equal(true);
        expect(rentalInfo['pausedAt']).to.equal(0);
        expect(rentalInfo['rentalToken']).to.equal(aGold);
      });
      it('updates a user stream by removing rentals', async () => {
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], RENT_PER_TILE * 3, RENT_PER_TILE * 2);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals.length).to.equal(2);
        for (let i = 0; i < userRentals.length; i++) {
          if (userRentals[i]['x'].toNumber() === orders[0].x && userRentals[i]['y'].toNumber() === orders[0].y && userRentals[i]['z'].toNumber() === orders[0].z) {
            throw new Error('order still in user rental array');
          }
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[i]['x'], userRentals[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);

          console.log(userRentals[i]['x'], userRentals[i]['y'], userRentals[i]['z']);
        }

        let rentalInfo = await missionControl.tileRentalInfo(alice.address, orders[0].x, orders[0].y);
        expect(rentalInfo['isRented']).to.equal(false);
        expect(rentalInfo['pausedAt']).to.not.equal(0);
        expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);
      });
    });
    describe("Pause Rentals/User's stream stops", () => {
      let orders: order[];
      beforeEach(async () => {
        orders = [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
        ];

        const flowRate = orders.length * RENT_PER_TILE;
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, RENT_PER_TILE * 6);
      });
      it('pauses all tiles after stream ends', async () => {
        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals).to.be.empty;

        for (let i = 0; i < orders.length; i++) {
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, orders[i].x, orders[i].y);
          expect(rentalInfo['isRented']).to.equal(false);
          expect(rentalInfo['pausedAt']).to.not.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);
        }
      });
      it('resumes tiles after being paused', async () => {
        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], RENT_PER_TILE * 3);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals.length).to.equal(3);

        for (let i = 0; i < userRentals.length; i++) {
          expect(userRentals[i]['x']).to.equal(orders[i].x);
          expect(userRentals[i]['y']).to.equal(orders[i].y);
          expect(userRentals[i]['z']).to.equal(orders[i].z);

          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[i]['x'], userRentals[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }
      });
    });
    describe('Contract Tiles using both supertokens', () => {
      let aGoldOrders: order[];
      let aGoldLiteOrders: order[];

      beforeEach(async () => {
        aGoldOrders = [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
        ];

        aGoldLiteOrders = [
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
        ];
      });
      it('Rents 3 tiles using aGold, and another 3 tiles using aGoldLite', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, aGoldOrders, 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).createRentTiles(aGoldLite, alice.address, aGoldLiteOrders, 3 * RENT_PER_TILE);

        const userRentalsGold = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentalsGold.length).to.equal(3);
        for (let i = 0; i < userRentalsGold.length; i++) {
          expect(userRentalsGold[i]['x']).to.equal(aGoldOrders[i].x);
          expect(userRentalsGold[i]['y']).to.equal(aGoldOrders[i].y);
          expect(userRentalsGold[i]['z']).to.equal(aGoldOrders[i].z);

          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentalsGold[i]['x'], userRentalsGold[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }

        const userRentalsLite = await missionControl.getUserRentals(alice.address, aGoldLite);
        expect(userRentalsLite.length).to.equal(3);

        for (let i = 0; i < userRentalsLite.length; i++) {
          expect(userRentalsLite[i]['x']).to.equal(aGoldLiteOrders[i].x);
          expect(userRentalsLite[i]['y']).to.equal(aGoldLiteOrders[i].y);
          expect(userRentalsLite[i]['z']).to.equal(aGoldLiteOrders[i].z);

          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentalsLite[i]['x'], userRentalsLite[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGoldLite);
        }
      });
      it('When a particular stream runs out, only the tiles rented using that streams token are paused', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, aGoldOrders, 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).createRentTiles(aGoldLite, alice.address, aGoldLiteOrders, 3 * RENT_PER_TILE);

        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);

        const userRentalsGold = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentalsGold).to.empty;
        for (let i = 0; i < aGoldOrders.length; i++) {
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, aGoldOrders[i].x, aGoldOrders[i].y);
          expect(rentalInfo['isRented']).to.equal(false);
          expect(rentalInfo['pausedAt']).to.not.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);
        }

        const userRentalsLite = await missionControl.getUserRentals(alice.address, aGoldLite);
        expect(userRentalsLite.length).to.equal(3);

        for (let i = 0; i < userRentalsLite.length; i++) {
          expect(userRentalsLite[i]['x']).to.equal(aGoldLiteOrders[i].x);
          expect(userRentalsLite[i]['y']).to.equal(aGoldLiteOrders[i].y);
          expect(userRentalsLite[i]['z']).to.equal(aGoldLiteOrders[i].z);

          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentalsLite[i]['x'], userRentalsLite[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGoldLite);
        }
      });
    });
    describe('Testing different flows for renting, pausing and de-renting', () => {
      let aGoldOrders: order[];
      let aGoldLiteOrders: order[];
      let orders: order[];

      beforeEach(async () => {
        aGoldOrders = [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
        ];

        aGoldLiteOrders = [
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
        ];
        orders = [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
        ];
      });
      it('User rents a few tiles and their stream ends', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, aGoldOrders, 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals).to.be.empty;

        for (let i = 0; i < aGoldOrders.length; i++) {
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, aGoldOrders[i].x, aGoldOrders[i].y);
          expect(rentalInfo['isRented']).to.equal(false);
          expect(rentalInfo['pausedAt']).to.not.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);
        }
      });
      it('User rents a few tiles, their stream ends, then they re rent the tiles using a different currency', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, aGoldOrders, 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);

        await missionControl.connect(streamer).createRentTiles(aGoldLite, alice.address, aGoldOrders, 3 * RENT_PER_TILE);

        const userRentalsGoldLite = await missionControl.getUserRentals(alice.address, aGoldLite);
        const userRentalsGold = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentalsGold).to.empty;
        expect(userRentalsGoldLite.length).to.equal(3);
        for (let i = 0; i < userRentalsGoldLite.length; i++) {
          expect(userRentalsGoldLite[i]['x']).to.equal(aGoldOrders[i].x);
          expect(userRentalsGoldLite[i]['y']).to.equal(aGoldOrders[i].y);
          expect(userRentalsGoldLite[i]['z']).to.equal(aGoldOrders[i].z);

          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentalsGoldLite[i]['x'], userRentalsGoldLite[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGoldLite);
        }
      });
      it('User rents a few tiles, their stream ends, then they re rent the tiles using the same currency', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);

        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);

        const userRentalsGold = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentalsGold.length).to.equal(3);
        for (let i = 0; i < userRentalsGold.length; i++) {
          expect(userRentalsGold[i]['x']).to.equal(aGoldOrders[i].x);
          expect(userRentalsGold[i]['y']).to.equal(aGoldOrders[i].y);
          expect(userRentalsGold[i]['z']).to.equal(aGoldOrders[i].z);

          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentalsGold[i]['x'], userRentalsGold[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }
      });
      it('User rents a few tiles and then removes one', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 3 * RENT_PER_TILE, 2 * RENT_PER_TILE);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals.length).to.equal(2);
        for (let i = 0; i < userRentals.length; i++) {
          if (userRentals[i]['x'].toNumber() === orders[0].x && userRentals[i]['y'].toNumber() === orders[0].y && userRentals[i]['z'].toNumber() === orders[0].z) {
            throw new Error('order still in user rental array');
          }
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[i]['x'], userRentals[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }

        let rentalInfo = await missionControl.tileRentalInfo(alice.address, orders[0].x, orders[0].y);
        expect(rentalInfo['isRented']).to.equal(false);
        expect(rentalInfo['pausedAt']).to.not.equal(0);
        expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);
      });
      it('User rents a few tiles, removes one tile, then re rents that tile using the same token', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 3 * RENT_PER_TILE, 2 * RENT_PER_TILE);

        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [orders[0]], [], 2 * RENT_PER_TILE, 3 * RENT_PER_TILE);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        for (let i = 0; i < userRentals.length; i++) {
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[i]['x'], userRentals[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }
      });
      it('User rents a few tiles, removes one tile, then re rents that tile using a different token', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 3 * RENT_PER_TILE, 2 * RENT_PER_TILE);

        await missionControl.connect(streamer).updateRentTiles(aGoldLite, alice.address, [orders[0]], [], 0, RENT_PER_TILE);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals.length).to.equal(2);

        for (let i = 0; i < userRentals.length; i++) {
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[i]['x'], userRentals[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }

        const userRentalsLite = await missionControl.getUserRentals(alice.address, aGoldLite);
        expect(userRentalsLite.length).to.equal(1);
        for (let i = 0; i < userRentalsLite.length; i++) {
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentalsLite[i]['x'], userRentalsLite[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGoldLite);
        }
      });
      it('User rents a few tiles, removes one tile, then adds a different tile.', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 3 * RENT_PER_TILE, 2 * RENT_PER_TILE);

        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [orders[3]], [], 2 * RENT_PER_TILE, 3 * RENT_PER_TILE);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals.length).to.equal(3);

        for (let i = 0; i < userRentals.length; i++) {
          if (userRentals[i]['x'].toNumber() === orders[0].x && userRentals[i]['y'].toNumber() === orders[0].y && userRentals[i]['z'].toNumber() === orders[0].z) {
            throw new Error('order still in user rental array');
          }
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, userRentals[i]['x'], userRentals[i]['y']);
          expect(rentalInfo['isRented']).to.equal(true);
          expect(rentalInfo['pausedAt']).to.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(aGold);
        }

        let rentalInfo = await missionControl.tileRentalInfo(alice.address, orders[0].x, orders[0].y);
        expect(rentalInfo['isRented']).to.equal(false);
        expect(rentalInfo['pausedAt']).to.not.equal(0);
        expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);

        let rentalInfoAdded = await missionControl.tileRentalInfo(alice.address, orders[3].x, orders[3].y);
        expect(rentalInfoAdded['isRented']).to.equal(true);
        expect(rentalInfoAdded['pausedAt']).to.equal(0);
        expect(rentalInfoAdded['rentalToken']).to.equal(aGold);
      });
      it('User rents a few tiles, removes one tile, then stream ends.', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 3 * RENT_PER_TILE, 2 * RENT_PER_TILE);

        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals).to.be.empty;

        for (let i = 0; i < 3; i++) {
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, orders[i].x, orders[i].y);
          expect(rentalInfo['isRented']).to.equal(false);
          expect(rentalInfo['pausedAt']).to.not.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);
        }
      });
      it('User rents 6 tiles and removes them one by one', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[5]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[1]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[2]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[4]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[3]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);

        const userRentals = await missionControl.getUserRentals(alice.address, aGold);
        expect(userRentals).to.be.empty;

        for (let i = 0; i < orders.length; i++) {
          let rentalInfo = await missionControl.tileRentalInfo(alice.address, orders[i].x, orders[i].y);
          expect(rentalInfo['isRented']).to.equal(false);
          expect(rentalInfo['pausedAt']).to.not.equal(0);
          expect(rentalInfo['rentalToken']).to.equal(ethers.constants.AddressZero);
        }
      });
    });
    describe('Using rented tiles', () => {
      let aGoldOrders: order[];
      let aGoldLiteOrders: order[];
      let orders: order[];

      beforeEach(async () => {
        aGoldOrders = [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
        ];

        aGoldLiteOrders = [
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
        ];
        orders = [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
        ];
      });
      it('User rents 6 tiles and tries staking items on them', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

        const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: stakeable_base.address };
        await missionControl.connect(alice).placeNFT(orderPlacement);

        expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
        expect(await stakeable_base.isStaked(1)).to.equal(true);

        const stakedOnTile = await missionControl.checkStakedOnTile(alice.address, orderPlacement.order.x, orderPlacement.order.y, orderPlacement.order.z);
        expect(stakedOnTile['_base']['stakeable']).to.equal(stakeable_base.address);
        expect(stakedOnTile['_base']['tokenId']).to.equal(1);
        expect(stakedOnTile['_base']['nonce']).to.equal(1);

        const tileRentalInfo = await missionControl.tileRentalInfo(alice.address, orderPlacement.order.x, orderPlacement.order.y);
        expect(tileRentalInfo['isRented']).to.equal(true);
        expect(tileRentalInfo['pausedAt']).to.equal(0);
        expect(tileRentalInfo['rentalToken']).to.equal(aGold);

        const allTileInfo = await missionControlTileStatus.getAllTileInfo(alice.address);
        const filtered = allTileInfo.filter((item) => item['base']['stakeable'] === stakeable_base.address);
        expect(filtered).to.not.empty;
      });
      it('If tile is not rented, user should not be able to stake', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        const orderPlacement: placeOrder = { order: { x: 1, y: 1, z: -2 }, tokenId: 1, tokenAddress: stakeable_base.address };
        await expect(missionControl.connect(alice).placeNFT(orderPlacement)).to.revertedWith('MC__TileNotRented()');
      });
      it('If tile is rented and their stream ends, user should not be able to stake', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);

        const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: stakeable_base.address };

        await expect(missionControl.connect(alice).placeNFT(orderPlacement)).to.revertedWith('MC__TileNotRented()');
      });
      it('If tile is rented and they remove a tile, user should not be able to stake on that tile', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 3 * RENT_PER_TILE, 2 * RENT_PER_TILE);

        const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: stakeable_base.address };

        await expect(missionControl.connect(alice).placeNFT(orderPlacement)).to.revertedWith('MC__TileNotRented()');
      });
      it('If their stream runs out and they re-rent a tile, they should be able to stake on that tile again.', async () => {
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0], orders[1], orders[2]], 3 * RENT_PER_TILE);
        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);
        await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orders[0]], RENT_PER_TILE);

        const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: stakeable_base.address };

        await missionControl.connect(alice).placeNFT(orderPlacement);

        expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
        expect(await stakeable_base.isStaked(1)).to.equal(true);

        const stakedOnTile = await missionControl.checkStakedOnTile(alice.address, orderPlacement.order.x, orderPlacement.order.y, orderPlacement.order.z);
        expect(stakedOnTile['_base']['stakeable']).to.equal(stakeable_base.address);
        expect(stakedOnTile['_base']['tokenId']).to.equal(1);
        expect(stakedOnTile['_base']['nonce']).to.equal(1);

        const tileRentalInfo = await missionControl.tileRentalInfo(alice.address, orderPlacement.order.x, orderPlacement.order.y);
        expect(tileRentalInfo['isRented']).to.equal(true);
        expect(tileRentalInfo['pausedAt']).to.equal(0);
        expect(tileRentalInfo['rentalToken']).to.equal(aGold);

        const allTileInfo = await missionControlTileStatus.getAllTileInfo(alice.address);
        const filtered = allTileInfo.filter((item) => item['base']['stakeable'] === stakeable_base.address);
        expect(filtered).to.not.empty;
      });
    });
  });

  describe('Place NFT reverts', function () {
    it('Revert on too large a radius', async function () {
      const orderPlacement: placeOrder = { order: { x: 3, y: -3, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };

      await expect(missionControl.connect(alice).placeNFT(orderPlacement)).to.revertedWith('MC__TileNotRented()');
    });
    it('Revert on zero radius', async function () {
      const orderPlacement: placeOrder = { order: { x: 0, y: 0, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };

      await expect(missionControl.connect(alice).placeNFT(orderPlacement)).to.revertedWith('MC__NoTileContractStaked()');
    });
    it('Revert on invalid box coordinates', async function () {
      const orderPlacement0: placeOrder = { order: { x: 1, y: 1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacement1: placeOrder = { order: { x: 1, y: -2, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacement2: placeOrder = { order: { x: 1, y: 1, z: -1 }, tokenId: 1, tokenAddress: stakeable_base.address };

      await expect(missionControl.connect(alice).placeNFT(orderPlacement0)).to.revertedWith('MC__InvalidCoordinates()');
      await expect(missionControl.connect(alice).placeNFT(orderPlacement1)).to.revertedWith('MC__InvalidCoordinates()');
      await expect(missionControl.connect(alice).placeNFT(orderPlacement2)).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on non whitelisted token', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: notWhitelisted.address };

      await expect(missionControl.connect(alice).placeNFT(orderPlacement)).to.revertedWith('MC__NotWhitelisted()');
    });
    it('Revert on double base stake', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);
      await expect(missionControl.connect(alice).placeNFT(orderPlacement)).to.revertedWith('MC__BaseStaked()');
    });
    it('Revert on non base stake without base', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await expect(missionControl.connect(alice).placeNFT(orderPlacement)).to.revertedWith('MC__InvalidPlacement()');
    });
  });

  describe('Check last updated', function () {
    it('Should give me the correct timestamp', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };

      let receipt = await missionControl.connect(alice).placeNFT(orderPlacement);
      let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      expect(await missionControl.checkLastUpdated(alice.address, 1, -1, 0, 0)).to.equal(timestamp);
    });
  });

  describe('Check last reverts', function () {
    it('Revert on too large a radius', async function () {
      await expect(missionControl.checkLastUpdated(alice.address, 3, -3, 0, 0)).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on zero radius', async function () {
      await expect(missionControl.checkLastUpdated(alice.address, 0, 0, 0, 0)).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on invalid box coordinates', async function () {
      await expect(missionControl.checkLastUpdated(alice.address, 1, 1, 0, 0)).to.revertedWith('MC__InvalidCoordinates()');
      await expect(missionControl.checkLastUpdated(alice.address, 1, -2, 0, 0)).to.revertedWith('MC__InvalidCoordinates()');
      await expect(missionControl.checkLastUpdated(alice.address, 1, 1, -1, 0)).to.revertedWith('MC__InvalidCoordinates()');
    });
  });

  describe('Place NFTs', function () {
    /**
     * Struct:
     *         int x;
     *         int y;
     *         int z;
     *         uint tokenId;
     *         address tokenAddress;
     */
    it('Stakes single base', async function () {
      await missionControl.connect(alice).placeNFTs([{ order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address }]);
      expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
      expect(await stakeable_base.isStaked(1)).to.equal(true);
    });
    it('Stakes multiple bases', async function () {
      await missionControl.connect(alice).placeNFTs([
        {
          order: { x: 1, y: -1, z: 0 },
          tokenId: 1,
          tokenAddress: stakeable_base.address,
        },
        { order: { x: 1, y: 0, z: -1 }, tokenId: 2, tokenAddress: stakeable_base.address },
      ]);
      expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
      expect(await stakeable_base.isStaked(1)).to.equal(true);
      expect(await stakeable_base.ownerOf(2)).to.equal(await stakeable_base.address);
      expect(await stakeable_base.isStaked(2)).to.equal(true);
    });
    it('Stakes single base then top', async function () {
      await missionControl.connect(alice).placeNFTs([
        {
          order: { x: 1, y: -1, z: 0 },
          tokenId: 1,
          tokenAddress: stakeable_base.address,
        },
        { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address },
      ]);
      expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
      expect(await stakeable_top.ownerOf(1)).to.equal(await stakeable_top.address);
      expect(await stakeable_top.isStaked(1)).to.equal(true);
    });

    it('Stakes multiple base then top', async function () {
      await missionControl.connect(alice).placeNFTs([
        { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address },
        { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address },
        {
          order: { x: 1, y: 0, z: -1 },
          tokenId: 2,
          tokenAddress: stakeable_base.address,
        },
        { order: { x: 1, y: 0, z: -1 }, tokenId: 2, tokenAddress: stakeable_top.address },
      ]);
      expect(await stakeable_base.ownerOf(1)).to.equal(await stakeable_base.address);
      expect(await stakeable_top.ownerOf(1)).to.equal(await stakeable_top.address);
      expect(await stakeable_top.isStaked(1)).to.equal(true);

      expect(await stakeable_base.ownerOf(2)).to.equal(await stakeable_base.address);
      expect(await stakeable_top.ownerOf(2)).to.equal(await stakeable_top.address);
      expect(await stakeable_top.isStaked(2)).to.equal(true);
    });
  });

  describe('Place NFTs reverts', function () {
    it('Revert on too large a radius', async function () {
      await expect(
        missionControl.placeNFTs([
          {
            order: { x: 3, y: -3, z: 0 },
            tokenId: 1,
            tokenAddress: stakeable_base.address,
          },
        ]),
      ).to.revertedWith('MC__TileNotRented()');
    });
    it('Revert on zero radius', async function () {
      await expect(
        missionControl.placeNFTs([
          {
            order: { x: 0, y: 0, z: 0 },

            tokenId: 1,
            tokenAddress: stakeable_base.address,
          },
        ]),
      ).to.revertedWith('MC__NoTileContractStaked()');
    });
    it('Revert on invalid box coordinates', async function () {
      await expect(
        missionControl.placeNFTs([
          {
            order: { x: 1, y: 1, z: 0 },

            tokenId: 1,
            tokenAddress: stakeable_base.address,
          },
        ]),
      ).to.revertedWith('MC__NoTileContractStaked()');
      await expect(
        missionControl.placeNFTs([
          {
            order: { x: 1, y: -2, z: 0 },

            tokenId: 1,
            tokenAddress: stakeable_base.address,
          },
        ]),
      ).to.revertedWith('MC__NoTileContractStaked()');
      await expect(
        missionControl.placeNFTs([
          {
            order: { x: 1, y: -1, z: 1 },

            tokenId: 1,
            tokenAddress: stakeable_base.address,
          },
        ]),
      ).to.revertedWith('MC__NoTileContractStaked()');
    });
    it('Revert on non whitelisted token', async function () {
      await expect(
        missionControl.connect(alice).placeNFTs([
          {
            order: { x: 1, y: -1, z: 0 },

            tokenId: 1,
            tokenAddress: notWhitelisted.address,
          },
        ]),
      ).to.revertedWith('MC__NotWhitelisted()');
    });
    it('Revert on double base stake', async function () {
      await expect(
        missionControl.connect(alice).placeNFTs([
          {
            order: { x: 1, y: -1, z: 0 },

            tokenId: 1,
            tokenAddress: stakeable_base.address,
          },
          {
            order: { x: 1, y: -1, z: 0 },

            tokenId: 1,
            tokenAddress: stakeable_base.address,
          },
        ]),
      ).to.revertedWith('MC__BaseStaked()');
    });
    it('Revert on non base stake without base', async function () {
      await expect(
        missionControl.connect(alice).placeNFTs([
          {
            order: { x: 1, y: -1, z: 0 },

            tokenId: 1,
            tokenAddress: stakeable_top.address,
          },
        ]),
      ).to.revertedWith('MC__InvalidPlacement()');
    });
  });

  describe('Remove NFT', function () {
    it('Unstakes base', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      const removal = { x: 1, y: -1, z: 0 };
      await missionControl.connect(alice).removeNFT(removal, 0);
      expect(await stakeable_base.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_base.isStaked(1)).to.equal(false);
    });
    it('Can unstake base which is out of bounds', async function () {
      await missionControl.setRadius(2);
      const orderPlacement = { order: { x: 2, y: -2, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const RENT_PER_TILE = 385802469135;

      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, [orderPlacement.order], RENT_PER_TILE);
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.setRadius(1);
      const removal = { x: 2, y: -2, z: 0 };

      await expect(missionControl.connect(alice).removeNFT(removal, 0)).to.not.reverted;
      expect(await stakeable_base.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_base.isStaked(1)).to.equal(false);
    });
    it('Unstakes top', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);
      const removal = { x: 1, y: -1, z: 0 };
      await missionControl.connect(alice).removeNFT(removal, 1);
      expect(await stakeable_base.ownerOf(1)).to.equal(stakeable_base.address);
      expect(await stakeable_base.isStaked(1)).to.equal(true);
      expect(await stakeable_top.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_top.isStaked(1)).to.equal(false);
    });
    it('should allow Alice to unstake without unstaking Bob', async function () {
      mockTileContracts.mint(bob.address, 35, 10000);
      mockTileContracts.mint(bob.address, 36, 10000);
      await mockTileContracts.connect(bob).setApprovalForAll(missionControlStaking.address, true);

      await missionControlStaking.connect(bob).stakeSemiFungible(mockTileContracts.address, 35, 1, 0);
      await missionControlStaking.connect(bob).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);
      const orderPlacementAlice: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementBob: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 7, tokenAddress: stakeable_base.address };

      await missionControl.connect(alice).placeNFT(orderPlacementAlice);
      await missionControl.connect(bob).placeNFT(orderPlacementBob);
      const removal = { x: 1, y: -1, z: 0 };

      await missionControl.connect(alice).removeNFT(removal, 0);
      expect(await stakeable_base.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_base.isStaked(1)).to.equal(false);
      expect(await stakeable_base.ownerOf(7)).to.equal(stakeable_base.address);
      expect(await stakeable_base.isStaked(7)).to.equal(true);
    });
    it('Subsequent removals works as expected (top then base)', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);
      const removal = { x: 1, y: -1, z: 0 };

      await missionControl.connect(alice).removeNFT(removal, 1);
      await missionControl.connect(alice).removeNFT(removal, 0);
      expect(await stakeable_base.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_base.isStaked(1)).to.equal(false);
      expect(await stakeable_top.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_top.isStaked(1)).to.equal(false);
    });
    it('Has no unexpected behaviour when staking and unstaking and then staking some more', async function () {
      const orderPlacement0: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacement1: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };
      const orderPlacement2: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 2, tokenAddress: stakeable_top.address };

      await missionControl.connect(alice).placeNFT(orderPlacement0);
      await missionControl.connect(alice).placeNFT(orderPlacement1);
      const removal0: order = { x: 1, y: -1, z: 0 };

      await missionControl.connect(alice).removeNFT(removal0, 1);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      expect(await stakeable_base.ownerOf(1)).to.equal(stakeable_base.address);
      expect(await stakeable_top.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_top.isStaked(1)).to.equal(false);
      expect(await stakeable_top.ownerOf(2)).to.equal(stakeable_top.address);
      expect(await stakeable_top.isStaked(2)).to.equal(true);

      const orderPlacement3: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 3, tokenAddress: stakeable_base.address };
      const orderPlacement4: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 3, tokenAddress: stakeable_top.address };
      const removal = { x: 1, y: -1, z: 0 };

      await missionControl.connect(alice).removeNFT(removal, 1);
      await missionControl.connect(alice).removeNFT(removal, 0);
      await missionControl.connect(alice).placeNFT(orderPlacement3);
      await missionControl.connect(alice).placeNFT(orderPlacement4);

      expect(await stakeable_base.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_top.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_top.isStaked(1)).to.equal(false);
      expect(await stakeable_top.ownerOf(2)).to.equal(await alice.getAddress());
      expect(await stakeable_top.isStaked(2)).to.equal(false);
      expect(await stakeable_top.ownerOf(3)).to.equal(stakeable_top.address);
      expect(await stakeable_top.isStaked(3)).to.equal(true);
      expect(await stakeable_base.ownerOf(3)).to.equal(stakeable_base.address);
    });
  });

  describe('Remove NFT reverts', function () {
    it('Revert on zero radius', async function () {
      const removal = { x: 0, y: 0, z: 0 };

      await expect(missionControl.removeNFT(removal, 0)).to.revertedWith('MC__TileEmpty()');
    });
    it('Revert on invalid box coordinates', async function () {
      const removal0 = { x: 1, y: 1, z: 0 };
      const removal1 = { x: 1, y: -2, z: 0 };
      const removal2 = { x: 1, y: 1, z: -1 };

      await expect(missionControl.removeNFT(removal0, 0)).to.revertedWith('MC__TileEmpty()');
      await expect(missionControl.removeNFT(removal1, 0)).to.revertedWith('MC__TileEmpty()');
      await expect(missionControl.removeNFT(removal2, 0)).to.revertedWith('MC__TileEmpty()');
    });
    it('Revert on no staked token', async function () {
      const removal = { x: 1, y: -1, z: 0 };

      await expect(missionControl.removeNFT(removal, 0)).to.revertedWith('MC__TileEmpty()');
    });
  });

  describe('Remove NFTs', function () {
    beforeEach(async function () {});
    it('Unstakes base', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);
      const removal = { x: 1, y: -1, z: 0 };

      await missionControl.connect(alice).removeNFTs([removal], [0]);
      expect(await stakeable_base.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_base.isStaked(1)).to.equal(false);
    });

    it('Unstakes top', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);
      const removal = { x: 1, y: -1, z: 0 };

      await missionControl.connect(alice).removeNFTs([removal], [1]);
      expect(await stakeable_top.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_top.isStaked(1)).to.equal(false);
    });
    it('Unstakes top and base', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };
      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);

      const removal = { x: 1, y: -1, z: 0 };

      await missionControl.connect(alice).removeNFTs([removal, removal], [1, 0]);
      expect(await stakeable_base.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_base.isStaked(1)).to.equal(false);
      expect(await stakeable_top.ownerOf(1)).to.equal(await alice.getAddress());
      expect(await stakeable_top.isStaked(1)).to.equal(false);
    });
  });

  describe('Remove NFTs reverts', function () {
    it('Revert on zero radius', async function () {
      const removal = { x: 0, y: 0, z: 0 };

      await expect(missionControl.removeNFTs([removal], [0])).to.revertedWith('MC__TileEmpty()');
    });
    it('Revert on invalid box coordinates', async function () {
      const removal0 = { x: 1, y: 1, z: 0 };

      await expect(missionControl.removeNFTs([removal0], [0])).to.revertedWith('MC__TileEmpty()');

      const removal1 = { x: 1, y: -2, z: 0 };
      await expect(missionControl.removeNFTs([removal1], [0])).to.revertedWith('MC__TileEmpty()');

      const removal2 = { x: 1, y: -1, z: 1 };
      await expect(missionControl.removeNFTs([removal2], [0])).to.revertedWith('MC__TileEmpty()');
    });
    it('Revert on no staked token', async function () {
      const removal = { x: 1, y: -1, z: 0 };

      await expect(missionControl.removeNFTs([removal], [0])).to.revertedWith('MC__TileEmpty()');
    });
  });

  describe('Check staked on tile', function () {
    let base;
    let top;
    it('Check when base staked', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      [base, top] = await missionControl.checkStakedOnTile(await alice.getAddress(), 1, -1, 0);
      expect(base['stakeable']).to.equal(stakeable_base.address);
      expect(base['tokenId']).to.equal(1);
      expect(base['nonce']).to.equal(1);

      expect(top['stakeable']).to.equal(constants.ZERO_ADDRESS);
      expect(top['tokenId']).to.equal(0);
      expect(top['nonce']).to.equal(0);
    });
    it('Check when top is staked', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);
      [base, top] = await missionControl.checkStakedOnTile(await alice.getAddress(), 1, -1, 0);
      expect(base['stakeable']).to.equal(stakeable_base.address);
      expect(base['tokenId']).to.equal(1);
      expect(base['nonce']).to.equal(1);

      expect(top['stakeable']).to.equal(stakeable_top.address);
      expect(top['tokenId']).to.equal(1);
      expect(top['nonce']).to.equal(2);
    });

    it('Check when top is staked then removed', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);
      [base, top] = await missionControl.checkStakedOnTile(await alice.getAddress(), 1, -1, 0);
      expect(base['stakeable']).to.equal(stakeable_base.address);
      expect(base['tokenId']).to.equal(1);
      expect(base['nonce']).to.equal(1);

      expect(top['stakeable']).to.equal(stakeable_top.address);
      expect(top['tokenId']).to.equal(1);
      expect(top['nonce']).to.equal(2);
      const removal = { x: 1, y: -1, z: 0 };

      await missionControl.connect(alice).removeNFT(removal, 1);

      [base, top] = await missionControl.checkStakedOnTile(await alice.getAddress(), 1, -1, 0);
      expect(base['stakeable']).to.equal(stakeable_base.address);
      expect(base['tokenId']).to.equal(1);
      expect(base['nonce']).to.equal(1);

      expect(top['stakeable']).to.equal(constants.ZERO_ADDRESS);
      expect(top['tokenId']).to.equal(0);
      expect(top['nonce']).to.equal(0);
    });
    it('should give expected results when doing a whole bunch of different checks', async function () {
      let count = 1;
      let nonce_count = 1;
      for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
          for (let k = -1; k < 2; k++) {
            if (i + j + k == 0 && (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2 == 1) {
              const orderPlacementBottom: placeOrder = { order: { x: i, y: j, z: k }, tokenId: count, tokenAddress: stakeable_base.address };

              await expect(missionControl.connect(alice).placeNFT(orderPlacementBottom)).to.not.reverted;
              [base, top] = await missionControl.checkStakedOnTile(await alice.getAddress(), i, j, k);
              expect(base['stakeable']).to.equal(stakeable_base.address);
              expect(base['tokenId']).to.equal(count);
              expect(base['nonce']).to.equal(nonce_count);

              expect(top['stakeable']).to.equal(constants.ZERO_ADDRESS);
              expect(top['tokenId']).to.equal(0);
              expect(top['nonce']).to.equal(0);
              count += 1;
              nonce_count += 1;
            }
          }
        }
      }
      count = 1;
      for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
          for (let k = -1; k < 2; k++) {
            if (i + j + k == 0 && (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2 == 1) {
              const orderPlacementTop: placeOrder = { order: { x: i, y: j, z: k }, tokenId: count, tokenAddress: stakeable_top.address };

              await expect(missionControl.connect(alice).placeNFT(orderPlacementTop)).to.not.reverted;
              [base, top] = await missionControl.checkStakedOnTile(await alice.getAddress(), i, j, k);
              expect(base['stakeable']).to.equal(stakeable_base.address);
              expect(base['tokenId']).to.equal(count);
              expect(base['nonce']).to.equal(count); // This is lazy but correct :)

              expect(top['stakeable']).to.equal(stakeable_top.address);
              expect(top['tokenId']).to.equal(count);
              expect(top['nonce']).to.equal(nonce_count);
              count += 1;
              nonce_count += 1;
            }
          }
        }
      }
    });
  });

  describe('Check staked on tile reverts', function () {
    it('Revert on too large a radius', async function () {
      await expect(missionControl.checkStakedOnTile(await alice.getAddress(), 3, -3, 0)).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on zero radius', async function () {
      await expect(missionControl.checkStakedOnTile(await alice.getAddress(), 0, 0, 0)).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on invalid box coordinates', async function () {
      await expect(missionControl.checkStakedOnTile(await alice.getAddress(), 1, 1, 0)).to.revertedWith('MC__InvalidCoordinates()');
      await expect(missionControl.checkStakedOnTile(await alice.getAddress(), 1, -2, 0)).to.revertedWith('MC__InvalidCoordinates()');
      await expect(missionControl.checkStakedOnTile(await alice.getAddress(), 1, 1, -1)).to.revertedWith('MC__InvalidCoordinates()');
    });
  });

  describe('Check tile', function () {
    it('Returns expected reward when only base is staked', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);

      const checkInputs = { user: alice.address, x: 1, y: -1, z: 0 };
      let reward = await missionControl.connect(alice).checkTile(checkInputs);
      expect(reward[0].toNumber() == 1 && reward[1].toNumber() == 1).to.equal(true);
    });
    it('Returns expected reward when top is staked', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);

      const checkInputs = { user: alice.address, x: 1, y: -1, z: 0 };

      let reward = await missionControl.connect(alice).checkTile(checkInputs);
      expect(reward['bottomAmount'].toNumber() == 1 && reward['topAmount1'].toNumber() == 2).to.equal(true);
    });
    it('Returns expected reward after removing top', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_top.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);

      const checkInputs = { user: alice.address, x: 1, y: -1, z: 0 };
      let reward = await missionControl.connect(alice).checkTile(checkInputs);
      expect(reward['bottomAmount'].toNumber() == 1 && reward['topAmount1'].toNumber() == 2).to.equal(true);
    });
  });

  describe('Check tile reverts', function () {
    it('Revert on too large a radius', async function () {
      const checkInputs = { user: owner.address, x: 3, y: -3, z: 0 };

      await expect(missionControl.checkTile(checkInputs)).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on zero radius', async function () {
      const checkInputs = { user: owner.address, x: 0, y: 0, z: 0 };

      await expect(missionControl.checkTile(checkInputs)).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on invalid box coordinates', async function () {
      const checkInputs0 = { user: owner.address, x: 1, y: 1, z: 0 };
      const checkInputs1 = { user: owner.address, x: 1, y: -2, z: 0 };
      const checkInputs2 = { user: owner.address, x: 1, y: 1, z: -1 };

      await expect(missionControl.checkTile(checkInputs0)).to.revertedWith('MC__InvalidCoordinates()');
      await expect(missionControl.checkTile(checkInputs1)).to.revertedWith('MC__InvalidCoordinates()');
      await expect(missionControl.checkTile(checkInputs2)).to.revertedWith('MC__InvalidCoordinates()');
    });
    // it('Revert on no staked token', async function () {
    //   const checkInputs = { user: owner.address, x: 1, y: -1, z: 0 };
    //   await expect(missionControl.checkTile(checkInputs)).to.revertedWith('MISSION_CONTROL: No token staked on coordinates');
    // });
  });

  describe('Collect from tiles', function () {
    let badSig;
    let assetsAdapter: PIXAssetStakeableAdapter;
    let mockAssets: ERC1155MockAssetManager;
    let pixAdapter: ERC721MCStakeableAdapter;
    let pixMock: ERC721MockPIX;
    let roverMock: RoverMock;

    let PIX_WASTE_EV = 12;
    let PIX_WASTE_CAP = 3;
    const month = 2_592_000;
    const hour = 3600;
    const RENT_PER_TILE = 385802469135;

    let aGoldOrders: order[];
    let aGoldLiteOrders: order[];
    let orders: order[];

    beforeEach(async function () {
      aGoldOrders = [
        { x: 0, y: -2, z: 2 },
        { x: 1, y: -2, z: 1 },
        { x: 2, y: -2, z: 0 },
      ];

      aGoldLiteOrders = [
        { x: 2, y: -1, z: -1 },
        { x: 2, y: 0, z: -2 },
        { x: 1, y: 1, z: -2 },
      ];
      orders = [
        { x: 0, y: -2, z: 2 },
        { x: 1, y: -2, z: 1 },
        { x: 2, y: -2, z: 0 },
        { x: 2, y: -1, z: -1 },
        { x: 2, y: 0, z: -2 },
        { x: 1, y: 1, z: -2 },
      ];
      /// RARITY PROVIDER
      let rarityFactory = await ethers.getContractFactory('RarityProviderMock');
      let rarityProvider = await rarityFactory.deploy();

      let qualityKeys = Object.keys(PIXCategory).filter((v) => !isNaN(Number(v)));
      qualityKeys.forEach(async (value) => {
        await rarityProvider.setRarity(value, value);
      });

      /// MOCK 1155
      let mockAssetsFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
      mockAssets = await mockAssetsFactory.deploy('TOY URI');

      /// PIX MOCK 721
      let pixMockFactory = await ethers.getContractFactory('ERC721MockPIX');
      pixMock = await pixMockFactory.deploy('PIX', 'PIX');

      /// 1155 ADAPTER
      let stakeableFactory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
      assetsAdapter = (await upgrades.deployProxy(stakeableFactory, [])) as PIXAssetStakeableAdapter;

      await assetsAdapter.setMissionControl(missionControl.address);
      await assetsAdapter.setERC1155(mockAssets.address);

      /// PIX 721 ADAPTER
      let pixAdapterFactory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
      pixAdapter = (await upgrades.deployProxy(pixAdapterFactory, [])) as ERC721MCStakeableAdapter;

      let roverMockFactory = await ethers.getContractFactory('RoverMock');
      roverMock = (await roverMockFactory.deploy()) as RoverMock;

      let roverAdapterFactory = await ethers.getContractFactory('RoverMCStakeableAdapter');
      let roverAdapter = (await upgrades.deployProxy(roverAdapterFactory, [alice.address])) as RoverMCStakeableAdapter;

      await roverAdapter.setMissionControl(missionControl.address);
      await roverAdapter.setPixAssetAdapter(assetsAdapter.address);
      await roverAdapter.setERC721(roverMock.address);

      await assetsAdapter.setRarityProvider(rarityProvider.address, pixAdapter.address);
      await assetsAdapter.setRover(roverMock.address);
      await assetsAdapter.setRoverAdpater(roverAdapter.address);
      await assetsAdapter.setDroneTier([5, 4, 33], [2, 1, 0]);

      await pixAdapter.setMissionControl(missionControl.address);
      await pixAdapter.setERC721(pixMock.address);

      for (let i = -2; i < 3; i++) {
        for (let j = -2; j < 3; j++) {
          for (let k = -2; k < 3; k++) {
            let sum = i + j + k;
            let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
            if (rad <= 2 && rad > 0 && sum == 0) {
              await assetsAdapter.setTokenInfo(AssetIds.GenesisDrone, true, false, true, 1, 36, AssetIds.Waste, 72, i, j);
              await assetsAdapter.setTokenInfo(AssetIds.FacilityCommon, true, false, true, 2, 1, AssetIds.Energy, 100, i, j);
              await assetsAdapter.setTokenInfo(AssetIds.FacilityRare, true, false, true, 2, 1, AssetIds.Energy, 100, i, j);
              await pixAdapter.setResourceParameters(AssetIds.Waste, PIX_WASTE_EV, PIX_WASTE_CAP, true, true, i, j);
              if (rad == 1) {
                await assetsAdapter.setStructureBonuses(i, j, 10000);
              }
              if (rad == 2) {
                await assetsAdapter.setStructureBonuses(i, j, 11000);
              }
            }
          }
        }
      }

      await assetsAdapter.setFacilityStakeRequirements(AssetIds.FacilityCommon, PIXCategory.Common, true);
      await assetsAdapter.setFacilityStakeRequirements(AssetIds.FacilityRare, PIXCategory.Rare, true);
      let hour = 60 * 60;
      await assetsAdapter.setFacilityStats([8 * hour, 12 * hour, 16 * hour, 20 * hour, 24 * hour], [1, 3, 4, 5, 6]);

      /// Setting up MC
      await missionControl.setWhitelist(assetsAdapter.address, true);
      await missionControl.setWhitelist(pixAdapter.address, true);
      await missionControl.setWhitelist(roverAdapter.address, true);

      /// Minting and approving
      await pixMock.mint(alice.address, 1);
      await pixMock.mint(alice.address, 2);
      await pixMock.mint(alice.address, 3);
      await pixMock.mint(alice.address, 4);
      await pixMock.mint(alice.address, 5);
      await pixMock.connect(alice).setApprovalForAll(pixAdapter.address, true);

      await mockAssets.mint(alice.address, AssetIds.GenesisDrone, 10);
      await mockAssets.mint(alice.address, AssetIds.FacilityLegendary, 10);
      await mockAssets.connect(alice).setApprovalForAll(assetsAdapter.address, true);

      await roverMock.mint(alice.address, 1);
      await roverMock.connect(alice).setApprovalForAll(roverAdapter.address, true);

      badSig = await getCollectDigest(missionControl, alice, alice, BigNumber.from(99));
    });
    it('stakes 2 things in same layer', async () => {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacementtop1: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacementtop2: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: pixAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacementtop1);
      await missionControl.connect(alice).placeNFT(orderPlacementtop2);
    });
    it('Gives expected reward when only base is staked', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: pixAdapter.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);

      await increaseTime(month);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 1, y: -1, z: 0 });
      expect(tileResults[0]).to.equal(PIX_WASTE_CAP);
      expect(tileResults[1]).to.equal(AssetIds.Waste);

      await missionControl.connect(alice).collectFromTiles([{ x: 1, y: -1, z: 0 }], [0], badSig.v, badSig.r, badSig.s);

      expect(await mintable.check_balance(alice.address, AssetIds.Waste)).to.equal(PIX_WASTE_CAP);
    });
    it('Gives expected reward when top is staked on top of base', async () => {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 4, tokenAddress: assetsAdapter.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(48 * hour);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 1, y: -1, z: 0 });
      console.log(tileResults);

      expect(tileResults['topAmount1']).to.equal(72);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);
    });
    it('should pause rewards when their stream ends - LINEAR', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(24 * hour);

      await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);

      await increaseTime(month);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['topAmount1']).to.equal(36);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);
    });
    it('should resume rewards when user re-activates tile- LINEAR', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(24 * hour);

      await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      await increaseTime(month);
      await increaseTime(month);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['topAmount1']).to.equal(72);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);
    });
    it('should pause/resume rewards when stream ends and then restarts - POISSON', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(24 * hour);

      await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      await increaseTime(24 * hour);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['topAmount1']).to.equal(36);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);
    });
    it('should pause reward when the user de-rents a particular tile - LINEAR', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(24 * hour);

      await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);

      await increaseTime(month);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['topAmount1']).to.equal(36);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);
    });
    it('should pause reward when the user de-rents a particular tile - POISSON', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);

      await increaseTime(month * 100);

      console.log(await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 }));
      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['bottomAmount']).to.equal(0);
      expect(tileResults['bottomId']).to.equal(AssetIds.Waste);

      await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [orders[0]], [], 5 * RENT_PER_TILE, 6 * RENT_PER_TILE);
      await increaseTime(month);

      console.log(await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 }));
      const tileResults2 = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults2['bottomAmount']).to.equal(3);
      expect(tileResults2['bottomId']).to.equal(AssetIds.Waste);
    });
    it('should resume reward when user re-rents a particular tile - LINEAR', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(24 * hour);

      await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);

      await increaseTime(month);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['topAmount1']).to.equal(36);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);

      await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [orders[0]], [], 5 * RENT_PER_TILE, 6 * RENT_PER_TILE);
      await increaseTime(12 * hour);

      const tileResults2 = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults2['topAmount1']).to.equal(18);
      expect(tileResults2['topId1']).to.equal(AssetIds.Waste);
    });
    it('should still be paused if user removes a paused top from a base', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(24 * hour);

      await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);

      await increaseTime(month);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['topAmount1']).to.equal(36);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);

      await missionControl.connect(alice).removeNFT({ x: 0, y: -2, z: 2 }, 1);

      await increaseTime(month);

      const tileResults2 = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults2['bottomAmount']).to.equal(0);
      expect(tileResults2['bottomId']).to.equal(AssetIds.Waste);
    });
    it('if user removes a paused top from a base and then unpauses the tile, they should get expected rewards', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(24 * hour);

      await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [], [orders[0]], 6 * RENT_PER_TILE, 5 * RENT_PER_TILE);

      await increaseTime(month);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['topAmount1']).to.equal(36);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);

      await missionControl.connect(alice).removeNFT({ x: 0, y: -2, z: 2 }, 1);
      await missionControl.connect(streamer).updateRentTiles(aGold, alice.address, [orders[0]], [], 5 * RENT_PER_TILE, 6 * RENT_PER_TILE);

      await increaseTime(month);

      const tileResults2 = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults2['bottomAmount']).to.equal(3);
      expect(tileResults2['bottomId']).to.equal(AssetIds.Waste);
    });
    it('should yield expected rewards if user removes a top from a base and then waits.', async () => {
      await missionControl.connect(streamer).createRentTiles(aGold, alice.address, orders, 6 * RENT_PER_TILE);

      const orderPlacement: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacement2: placeOrder = { order: { x: 0, y: -2, z: 2 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };
      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.connect(alice).placeNFT(orderPlacement2);

      await increaseTime(24 * hour);

      const tileResults = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults['topAmount1']).to.equal(36);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);

      await missionControl.connect(alice).removeNFT({ x: 0, y: -2, z: 2 }, 1);

      await increaseTime(month);

      const tileResults2 = await missionControl.checkTile({ user: alice.address, x: 0, y: -2, z: 2 });
      expect(tileResults2['bottomAmount']).to.equal(3);
      expect(tileResults2['bottomId']).to.equal(AssetIds.Waste);
    });
    it('Can collect with valid signature', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: pixAdapter.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.setSignature(true);
      await missionControl.setRaidSigner(owner.address, true);
      let goodSig = await getCollectDigest(missionControl, owner, alice, await missionControl.nonces(alice.address));

      await increaseTime(month);

      await expect(missionControl.connect(alice).collectFromTiles([{ x: 1, y: -1, z: 0 }], [0], goodSig.v, goodSig.r, goodSig.s)).to.not.reverted;

      expect(await mintable.check_balance(await alice.getAddress(), AssetIds.Waste)).to.equal(3);
    });
    it('Gives expected reward when top is staked', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);

      await increaseTime(48 * hour);

      await missionControl.connect(alice).collectFromTiles([{ x: 1, y: -1, z: 0 }], [1], badSig.v, badSig.r, badSig.s);
      expect(await mintable.check_balance(await alice.getAddress(), AssetIds.Waste)).to.equal(72);
    });
    it('Gives expected reward when collecting top, removing top, collecting base', async function () {
      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: AssetIds.GenesisDrone, tokenAddress: assetsAdapter.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);
      await increaseTime(month);
      let tileResults = await missionControl.checkTile({ user: alice.address, x: 1, y: -1, z: 0 });
      expect(tileResults['topAmount1']).to.equal(72);
      expect(tileResults['topId1']).to.equal(AssetIds.Waste);
      await missionControl.connect(alice).collectFromTiles([{ x: 1, y: -1, z: 0 }], [1], badSig.v, badSig.r, badSig.s);
      expect(await mintable.check_balance(await alice.getAddress(), AssetIds.Waste)).to.equal(72);

      const removal = { x: 1, y: -1, z: 0 };
      await missionControl.connect(alice).removeNFT(removal, 1);
      await increaseTime(month);
      tileResults = await missionControl.checkTile({ user: alice.address, x: 1, y: -1, z: 0 });
      expect(tileResults['bottomAmount']).to.equal(3);
      expect(tileResults['bottomId']).to.equal(AssetIds.Waste);
      await missionControl.connect(alice).collectFromTiles([{ x: 1, y: -1, z: 0 }], [0], badSig.v, badSig.r, badSig.s);
      expect(await mintable.check_balance(await alice.getAddress(), AssetIds.Waste)).to.equal(75);
    });
    it('Gets expected reward from facility on regular tile', async () => {
      await mockAssets.mint(alice.address, AssetIds.FacilityCommon, 100);

      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);
      await increaseTime(month * 10);
      let tileResults = await missionControl.checkTile({ user: alice.address, x: 1, y: -1, z: 0 });
      console.log(tileResults);

      // expect(tileResults[0]).to.equal(420);
      // expect(tileResults[1]).to.equal(AssetIds.Energy);
      await missionControl.connect(alice).collectFromTiles([{ x: 1, y: -1, z: 0 }], [1], badSig.v, badSig.r, badSig.s);
      expect(await mintable.check_balance(await alice.getAddress(), AssetIds.Energy)).to.equal(100);
    });
    it('Gets expected reward from facility on contract tile', async () => {
      await mockAssets.mint(alice.address, AssetIds.FacilityCommon, 100);
      await mockAssets.mint(alice.address, AssetIds.FacilityRare, 100);

      await missionControl.connect(streamer).createRentTiles(
        aGold,
        alice.address,
        [
          { x: 2, y: -2, z: 0 },
          { x: 0, y: -2, z: 2 },
        ],
        385802469135 * 2,
      );

      const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
      const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

      const orderPlacementBottom2: placeOrder = { order: { x: 0, y: -1, z: 1 }, tokenId: 1, tokenAddress: pixAdapter.address };
      const orderPlacementTop2: placeOrder = { order: { x: 0, y: -1, z: 1 }, tokenId: AssetIds.FacilityRare, tokenAddress: assetsAdapter.address };

      await missionControl.connect(alice).placeNFT(orderPlacementBottom);
      await missionControl.connect(alice).placeNFT(orderPlacementTop);

      await missionControl.connect(alice).placeNFT(orderPlacementBottom2);
      await missionControl.connect(alice).placeNFT(orderPlacementTop2);

      await increaseTime(month);
      let tileResults = await missionControl.checkTile({ user: alice.address, x: 1, y: -1, z: 0 });
      console.log(tileResults);

      let tileResults2 = await missionControl.checkTile({ user: alice.address, x: 0, y: -1, z: 1 });
      console.log(tileResults2);

      // await missionControl.connect(alice).collectFromTiles([{ x: 2, y: -2, z: 0 }], badSig.v, badSig.r, badSig.s);
      // expect(await mintable.check_balance(await alice.getAddress(), AssetIds.Energy)).to.equal(110);
    });
    describe('Tile Contract Tests', () => {
      let tileContractProvder: TileContractLockProvider;
      beforeEach(async () => {
        await mockAssets.mint(alice.address, AssetIds.FacilityCommon, 100);
        await mockAssets.mint(alice.address, AssetIds.FacilityRare, 100);

        const tileContractProvderFactory = await ethers.getContractFactory('TileContractLockProvider');
        tileContractProvder = (await upgrades.deployProxy(tileContractProvderFactory, [])) as TileContractLockProvider;

        await missionControlStaking.setUnstakeProvider(mockTileContracts.address, [35, 36], tileContractProvder.address);
        await tileContractProvder.setMCStaking(missionControlStaking.address);
        await tileContractProvder.setMissionControl(missionControl.address);

        await tileContractProvder.addCoords(35, [
          { x: 0, y: -1, z: 1 },
          { x: 1, y: -1, z: 0 },
          { x: 1, y: 0, z: -1 },
          { x: 0, y: 1, z: -1 },
          { x: -1, y: 1, z: 0 },
          { x: -1, y: 0, z: 1 },
        ]);

        await tileContractProvder.addCoords(36, [
          { x: 0, y: -2, z: 2 },
          { x: 1, y: -2, z: 1 },
          { x: 2, y: -2, z: 0 },
          { x: 2, y: -1, z: -1 },
          { x: 2, y: 0, z: -2 },
          { x: 1, y: 1, z: -2 },
          { x: 0, y: 2, z: -2 },
          { x: -1, y: 2, z: -1 },
          { x: -2, y: 2, z: 0 },
          { x: -2, y: 1, z: 1 },
          { x: -2, y: 0, z: 2 },
          { x: -1, y: -1, z: 2 },
        ]);
      });
      it('Pauses your rewards if you remove your contract tile', async () => {
        await missionControl.connect(streamer).createRentTiles(
          aGold,
          alice.address,
          [
            { x: 2, y: -2, z: 0 },
            { x: 0, y: -2, z: 2 },
          ],
          385802469135 * 2,
        );

        await missionControl.setTileContractLockProvider(tileContractProvder.address);

        const orderPlacementBottom: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
        const orderPlacementTop: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

        await missionControl.connect(alice).placeNFT(orderPlacementBottom);
        await missionControl.connect(alice).placeNFT(orderPlacementTop);

        await increaseTime(month);

        // await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);
        await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 36, 1, 0);
        await increaseTime(month);

        // await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);

        // await increaseTime(month);

        let tileResults = await missionControl.checkTile({ user: alice.address, x: 2, y: -2, z: 0 });

        expect(tileResults['topAmount1']).to.equal(39);
        console.log(tileResults);
      });
      it('Pauses your rewards if you remove your contract tile, but resumes the reward(starts it from scratch) if you re - stake it and the tile is rented', async () => {
        await missionControl.connect(streamer).createRentTiles(
          aGold,
          alice.address,
          [
            { x: 2, y: -2, z: 0 },
            { x: 0, y: -2, z: 2 },
          ],
          385802469135 * 2,
        );

        await missionControl.setTileContractLockProvider(tileContractProvder.address);

        const orderPlacementBottom: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
        const orderPlacementTop: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

        await missionControl.connect(alice).placeNFT(orderPlacementBottom);
        await missionControl.connect(alice).placeNFT(orderPlacementTop);

        await increaseTime(month);

        // await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);
        await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 36, 1, 0);
        await increaseTime(month);

        await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);

        await increaseTime(month);

        let tileResults = await missionControl.checkTile({ user: alice.address, x: 2, y: -2, z: 0 });

        expect(tileResults['topAmount1']).to.equal(39);
        console.log(tileResults);
      });
      it('Pauses your reward if you remove your contract tile, and does not unpause it if you ended your stream before re-staking', async () => {
        await missionControl.connect(streamer).createRentTiles(
          aGold,
          alice.address,
          [
            { x: 2, y: -2, z: 0 },
            { x: 0, y: -2, z: 2 },
          ],
          385802469135 * 2,
        );

        await missionControl.setTileContractLockProvider(tileContractProvder.address);

        const orderPlacementBottom: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
        const orderPlacementTop: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

        await missionControl.connect(alice).placeNFT(orderPlacementBottom);
        await missionControl.connect(alice).placeNFT(orderPlacementTop);

        await increaseTime(month * 2);

        await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 36, 1, 0);

        await missionControl.connect(streamer).deleteRentTiles(aGold, alice.address);
        await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);

        await increaseTime(month * 3);

        let tileResults = await missionControl.checkTile({ user: alice.address, x: 2, y: -2, z: 0 });

        expect(tileResults['topAmount1']).to.equal(79);
        console.log(tileResults);
      });
      it('removing contract tile for regular tiles', async () => {
        await missionControl.connect(streamer).createRentTiles(
          aGold,
          alice.address,
          [
            { x: 2, y: -2, z: 0 },
            { x: 0, y: -2, z: 2 },
          ],
          385802469135 * 2,
        );

        await missionControl.setTileContractLockProvider(tileContractProvder.address);

        const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
        const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

        await missionControl.connect(alice).placeNFT(orderPlacementBottom);
        await missionControl.connect(alice).placeNFT(orderPlacementTop);

        await increaseTime(month);

        await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 35, 1, 0);
        // await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);

        await increaseTime(month / 2);

        let tileResults = await missionControl.checkTile({ user: alice.address, x: 1, y: -1, z: 0 });

        expect(tileResults['topAmount1']).to.equal(36);
        console.log(tileResults);
      });
      it('removing contract tile for regular tiles and re-adding it', async () => {
        await missionControl.connect(streamer).createRentTiles(
          aGold,
          alice.address,
          [
            { x: 2, y: -2, z: 0 },
            { x: 0, y: -2, z: 2 },
          ],
          385802469135 * 2,
        );

        await missionControl.setTileContractLockProvider(tileContractProvder.address);

        const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
        const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

        await missionControl.connect(alice).placeNFT(orderPlacementBottom);
        await missionControl.connect(alice).placeNFT(orderPlacementTop);

        await increaseTime(month);

        await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 35, 1, 0);
        await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 35, 1, 0);

        await increaseTime(month * 2);

        let tileResults = await missionControl.checkTile({ user: alice.address, x: 1, y: -1, z: 0 });

        expect(tileResults['topAmount1']).to.equal(72);
        console.log(tileResults);
      });
      it('Cant stake on tile without a tile contract: radius 1', async () => {
        await missionControl.connect(streamer).createRentTiles(
          aGold,
          alice.address,
          [
            { x: 2, y: -2, z: 0 },
            { x: 0, y: -2, z: 2 },
          ],
          385802469135 * 2,
        );

        await missionControl.setTileContractLockProvider(tileContractProvder.address);

        await increaseTime(month);
        await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 35, 1, 0);

        const orderPlacementBottom: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
        const orderPlacementTop: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

        await expect(missionControl.connect(alice).placeNFT(orderPlacementBottom)).to.revertedWith('MC__NoTileContractStaked()');
      });
      it('Cant stake on tile without a tile contract: radius 2', async () => {
        await missionControl.connect(streamer).createRentTiles(
          aGold,
          alice.address,
          [
            { x: 2, y: -2, z: 0 },
            { x: 0, y: -2, z: 2 },
          ],
          385802469135 * 2,
        );

        await missionControl.setTileContractLockProvider(tileContractProvder.address);

        await increaseTime(month);
        await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 36, 1, 0);

        const orderPlacementBottom: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };

        await expect(missionControl.connect(alice).placeNFT(orderPlacementBottom)).to.revertedWith('MC__TilePaused()');
      });
      it('Should not pause upon unstaking if tc balance still non zero', async () => {
        await missionControl.connect(streamer).createRentTiles(
          aGold,
          alice.address,
          [
            { x: 2, y: -2, z: 0 },
            { x: 0, y: -2, z: 2 },
          ],
          385802469135 * 2,
        );

        await missionControl.setTileContractLockProvider(tileContractProvder.address);
        await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);

        await increaseTime(month);

        await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 36, 1, 0);

        const orderPlacementBottom: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: 3, tokenAddress: pixAdapter.address };
        const orderPlacementTop: placeOrder = { order: { x: 2, y: -2, z: 0 }, tokenId: AssetIds.FacilityCommon, tokenAddress: assetsAdapter.address };

        await expect(missionControl.connect(alice).placeNFT(orderPlacementBottom)).to.not.reverted;
        await expect(missionControl.connect(alice).placeNFT(orderPlacementTop)).to.not.reverted;

        await increaseTime(month * 2);

        let tileResults = await missionControl.checkTile({ user: alice.address, x: 2, y: -2, z: 0 });

        expect(tileResults['topAmount1']).to.equal(79);
        console.log(tileResults);
      });
      it('NG lock tests', async () => {
        let nglock: NewlandsGenesisLockProvider;
        let ng: Mock1155FT;
        let nglockfactory = await ethers.getContractFactory('NewlandsGenesisLockProvider');
        nglock = (await upgrades.deployProxy(nglockfactory, [])) as NewlandsGenesisLockProvider;

        let mockConnextFactory = await ethers.getContractFactory('MockConnext');
        const mockConnext = await mockConnextFactory.deploy();

        let ngfactory = await ethers.getContractFactory('Mock1155FT');
        ng = await ngfactory.deploy('TOY URI');

        await nglock.setConnext(mockConnext.address);
        await nglock.setMCStaking(missionControlStaking.address);
        await nglock.setTarget(mcCrosschainServices.address);
        await nglock.setDestinationDomain(137);

        await ng.mint(alice.address, 3, 1);
        await ng.connect(alice).setApprovalForAll(missionControlStaking.address, true);

        await missionControlStaking.whitelistTokens(ng.address, [3], true);
        await missionControlStaking.setUnstakeProvider(ng.address, [3], nglock.address);

        await mcCrosschainServices.setConnext(mockConnext.address);
        await mcCrosschainServices.setMissionControl(missionControl.address);
        await mcCrosschainServices.setOriginDomain(1);
        await mcCrosschainServices.setSource(nglock.address);
        await mcCrosschainServices.setAGold(aGold);
        await mcCrosschainServices.setAGoldLite(aGoldLite);

        await mcCrosschainServices.setValidSource(nglock.address, true);
        await mcCrosschainServices.setNLock(nglock.address);

        await missionControlStaking.connect(alice).stakeSemiFungible(ng.address, 3, 1, 15, { value: ethers.utils.parseEther('1') });

        await expect(
          missionControl.connect(streamer).createRentTiles(
            aGold,
            alice.address,
            [
              { x: 2, y: -2, z: 0 },
              { x: 0, y: -2, z: 2 },
            ],
            0,
          ),
        ).to.not.reverted;

        await increaseTime(month);

        await missionControlStaking.connect(alice).unstake(ng.address, 3, 1, 15, { value: ethers.utils.parseEther('1') });
        await expect(await missionControl.getUserRentals(alice.address, aGold)).to.empty;

        await expect(
          missionControl.connect(streamer).createRentTiles(
            aGold,
            alice.address,
            [
              { x: 2, y: -2, z: 0 },
              { x: 0, y: -2, z: 2 },
            ],
            0,
          ),
        ).to.reverted;

        await missionControlStaking.connect(alice).stakeSemiFungible(ng.address, 3, 1, 15, { value: ethers.utils.parseEther('1') });

        await expect(
          missionControl.connect(streamer).createRentTiles(
            aGold,
            alice.address,
            [
              { x: 2, y: -2, z: 0 },
              { x: 0, y: -2, z: 2 },
            ],
            0,
          ),
        ).to.not.reverted;

        await increaseTime(month);
        await missionControlStaking.connect(alice).unstake(ng.address, 3, 1, 15, { value: ethers.utils.parseEther('1') });

        await expect(
          missionControl.connect(streamer).createRentTiles(
            aGold,
            alice.address,
            [
              { x: 2, y: -2, z: 0 },
              { x: 0, y: -2, z: 2 },
            ],
            385802469135 * 2,
          ),
        ).to.not.reverted;
      });
    });
  });

  describe('Collect from tiles reverts', function () {
    let badSig;
    beforeEach(async function () {
      badSig = await getCollectDigest(missionControl, alice, alice, BigNumber.from(99));
    });
    it('Revert on too large a radius', async function () {
      await expect(
        missionControl.collectFromTiles(
          [
            {
              x: 3,
              y: -3,
              z: 0,
            },
          ],
          [0],
          badSig.v,
          badSig.r,
          badSig.s,
        ),
      ).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on zero radius', async function () {
      await expect(
        missionControl.collectFromTiles(
          [
            {
              x: 0,
              y: 0,
              z: 0,
            },
          ],
          [0],
          badSig.v,
          badSig.r,
          badSig.s,
        ),
      ).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Revert on invalid box coordinates', async function () {
      await expect(
        missionControl.collectFromTiles(
          [
            {
              x: 1,
              y: -2,
              z: 0,
            },
          ],
          [0],
          badSig.v,
          badSig.r,
          badSig.s,
        ),
      ).to.revertedWith('MC__InvalidCoordinates()');
      await expect(
        missionControl.collectFromTiles(
          [
            {
              x: 1,
              y: -1,
              z: 1,
            },
          ],
          [0],
          badSig.v,
          badSig.r,
          badSig.s,
        ),
      ).to.revertedWith('MC__InvalidCoordinates()');
      await expect(
        missionControl.collectFromTiles(
          [
            {
              x: 1,
              y: 1,
              z: 1,
            },
          ],
          [0],
          badSig.v,
          badSig.r,
          badSig.s,
        ),
      ).to.revertedWith('MC__InvalidCoordinates()');
    });
    it('Reverts on bad signature if signatures are enabled', async function () {
      const orderPlacement: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: stakeable_base.address };

      await missionControl.connect(alice).placeNFT(orderPlacement);
      await missionControl.setSignature(true);
      await expect(
        missionControl.collectFromTiles(
          [
            {
              x: 1,
              y: -1,
              z: 0,
            },
          ],
          [0],
          badSig.v,
          badSig.r,
          badSig.s,
        ),
      ).to.revertedWith('MC__InvalidSigner()');
    });
    it('Revert on no staked token', async function () {
      await expect(
        missionControl.collectFromTiles(
          [
            {
              x: 1,
              y: -1,
              z: 0,
            },
          ],
          [0],
          badSig.v,
          badSig.r,
          badSig.s,
        ),
      ).to.revertedWith('MC__TileEmpty()');
    });
    /*
        it("Revert on asking for too much", async function () {
            await missionControl.connect(alice).placeNFT(orderPlacement);
            await expect(missionControl.connect(alice).collectFromTiles([{
                x: 1,
                y: -1,
                z: 0,

            }])).to.revertedWith("MISSION_CONTROL: Requested too many tokens!");
        });*/
  });

  // describe('Raiding', function () {
  //   let assetsAdapter: PIXAssetStakeableAdapter;
  //   let mockAssets: ERC1155MockAssetManager;
  //   let pixAdapter: ERC721MCStakeableAdapter;
  //   let pixMock: ERC721MockPIX;

  //   let PIX_WASTE_EV = 12;
  //   let PIX_WASTE_CAP = 3;
  //   const month = 2_592_000;
  //   const RENT_PER_TILE = 385802469135;

  //   let aGoldOrders: order[];
  //   let aGoldLiteOrders: order[];
  //   let orders: order[];
  //   let firstRingOrders: order[];

  //   let startTime;
  //   const raidTimeout = 120;
  //   let badSig;

  //   beforeEach(async function () {
  //     aGoldOrders = [
  //       { x: 0, y: -2, z: 2 },
  //       { x: 1, y: -2, z: 1 },
  //       { x: 2, y: -2, z: 0 },
  //     ];

  //     aGoldLiteOrders = [
  //       { x: 2, y: -1, z: -1 },
  //       { x: 2, y: 0, z: -2 },
  //       { x: 1, y: 1, z: -2 },
  //     ];
  //     orders = [
  //       { x: 0, y: -2, z: 2 },
  //       { x: 1, y: -2, z: 1 },
  //       { x: 2, y: -2, z: 0 },
  //       { x: 2, y: -1, z: -1 },
  //       { x: 2, y: 0, z: -2 },
  //       { x: 1, y: 1, z: -2 },
  //     ];
  //     firstRingOrders = [
  //       { x: 0, y: -1, z: 1 },
  //       { x: 1, y: -1, z: 0 },
  //       { x: 1, y: 0, z: -1 },
  //       { x: 0, y: 1, z: -1 },
  //       { x: -1, y: 1, z: 0 },
  //       { x: -1, y: 0, z: 1 },
  //     ];

  //     /// RARITY PROVIDER
  //     let rarityFactory = await ethers.getContractFactory('RarityProviderMock');
  //     let rarityProvider = await rarityFactory.deploy();

  //     let qualityKeys = Object.keys(PIXCategory).filter((v) => !isNaN(Number(v)));
  //     qualityKeys.forEach(async (value) => {
  //       await rarityProvider.setRarity(value, value);
  //     });

  //     /// MOCK 1155
  //     let mockAssetsFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
  //     mockAssets = await mockAssetsFactory.deploy('TOY URI');

  //     /// PIX MOCK 721
  //     let pixMockFactory = await ethers.getContractFactory('ERC721MockPIX');
  //     pixMock = await pixMockFactory.deploy('PIX', 'PIX');

  //     /// 1155 ADAPTER
  //     let stakeableFactory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
  //     assetsAdapter = (await upgrades.deployProxy(stakeableFactory, [])) as PIXAssetStakeableAdapter;

  //     await assetsAdapter.setRarityProvider(rarityProvider.address, rarityProvider.address);
  //     await assetsAdapter.setMissionControl(missionControl.address);
  //     await assetsAdapter.setERC1155(mockAssets.address);

  //     /// PIX 721 ADAPTER
  //     let pixAdapterFactory = await ethers.getContractFactory('ERC721MCStakeableAdapter');
  //     pixAdapter = (await upgrades.deployProxy(pixAdapterFactory, [])) as ERC721MCStakeableAdapter;

  //     await pixAdapter.setMissionControl(missionControl.address);
  //     await pixAdapter.setERC721(pixMock.address);

  //     for (let i = -2; i < 3; i++) {
  //       for (let j = -2; j < 3; j++) {
  //         for (let k = -2; k < 3; k++) {
  //           let sum = i + j + k;
  //           let rad = (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2;
  //           if (rad <= 2 && rad > 0 && sum == 0) {
  //             // console.log(`Now setting token Info for: ${i}, ${j}, ${k}`);
  //             await assetsAdapter.setTokenInfo(
  //               AssetIds.GenesisDrone,
  //               true,
  //               false,
  //               true,
  //               1, //CLASS.LINEAR
  //               14,
  //               AssetIds.Energy,
  //               28,
  //               i,
  //               j,
  //             );
  //             await pixAdapter.setResourceParameters(AssetIds.Waste, PIX_WASTE_EV, PIX_WASTE_CAP, true, true, i, j);
  //           }
  //         }
  //       }
  //     }

  //     /// Setting up MC
  //     await missionControl.setWhitelist(assetsAdapter.address, true);
  //     await missionControl.setWhitelist(pixAdapter.address, true);

  //     /// Minting and approving
  //     await pixMock.mint(alice.address, 2);
  //     await pixMock.mint(alice.address, 5);
  //     await pixMock.connect(alice).setApprovalForAll(pixAdapter.address, true);

  //     await mockAssets.mint(alice.address, AssetIds.GenesisDrone, 10);
  //     await mockAssets.connect(alice).setApprovalForAll(assetsAdapter.address, true);

  //     await missionControl.setRaidParams(raidTimeout);
  //     const orderPlacementBottom: placeOrder = { order: firstRingOrders[0], tokenId: 5, tokenAddress: pixAdapter.address };
  //     const orderPlacementBottom2: placeOrder = { order: firstRingOrders[1], tokenId: 2, tokenAddress: pixAdapter.address };

  //     await missionControl.connect(alice).placeNFT(orderPlacementBottom);
  //     await missionControl.connect(alice).placeNFT(orderPlacementBottom2);
  //     await missionControl.setRaidSigner(await owner.getAddress(), true);
  //     startTime = (await ethers.provider.getBlock('latest')).timestamp;
  //     badSig = await getCollectDigest(missionControl, alice, alice, BigNumber.from(99));
  //   });

  //   describe('Raiding Passes', () => {
  //     it('Should allow raiding', async function () {
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));
  //       await expect(missionControl.notifyRaided(alice.address, startTime, 1, [firstRingOrders[0]], digest.v, digest.r, digest.s)).to.not.reverted;
  //     });

  //     it('Should allow raiding multiple tiles', async function () {
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));
  //       await expect(missionControl.notifyRaided(alice.address, startTime, 1, [firstRingOrders[0], firstRingOrders[1]], digest.v, digest.r, digest.s));
  //     });

  //     it('Should emit Raid event when raiding', async function () {
  //       await missionControl.setRaidParams(120);

  //       await missionControl.setRaidSigner(await owner.getAddress(), true);
  //       let startTime = (await ethers.provider.getBlock('latest')).timestamp;
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));
  //       await expect(missionControl.notifyRaided(await alice.getAddress(), startTime, 1, [firstRingOrders[0], firstRingOrders[1]], digest.v, digest.r, digest.s))
  //         .to.emit(missionControl, 'Raid')
  //         .withArgs(await alice.getAddress(), 1, 0, startTime);
  //     });
  //   });

  //   describe('Raiding reverts', function () {
  //     it('Should revert on zero radius', async function () {
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));
  //       await expect(missionControl.notifyRaided(alice.address, startTime, 1, [{ x: 0, y: 0, z: 0 }], digest.v, digest.r, digest.s)).to.revertedWith(
  //         'MISSION_CONTROL: Invalid coordinates',
  //       );
  //     });

  //     it('Should revert on too large radius', async function () {
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));
  //       await expect(missionControl.notifyRaided(alice.address, startTime, 1, [{ x: 3, y: -3, z: 0 }], digest.v, digest.r, digest.s)).to.revertedWith(
  //         'MISSION_CONTROL: Invalid coordinates',
  //       );
  //     });

  //     it('Should revert on invalid box coordinates', async function () {
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));
  //       await expect(
  //         missionControl.notifyRaided(
  //           alice.address,
  //           startTime,
  //           1,
  //           [
  //             {
  //               x: 1,
  //               y: -2,
  //               z: 0,
  //             },
  //           ],
  //           digest.v,
  //           digest.r,
  //           digest.s,
  //         ),
  //       ).to.revertedWith('MISSION_CONTROL: No token staked on coordinates');
  //     });

  //     it('Should revert on no staked token', async function () {
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));
  //       await expect(
  //         missionControl.notifyRaided(
  //           await alice.getAddress(),
  //           startTime,
  //           1,
  //           [
  //             {
  //               x: 1,
  //               y: 0,
  //               z: -1,
  //             },
  //           ],
  //           digest.v,
  //           digest.r,
  //           digest.s,
  //         ),
  //       ).to.revertedWith('MISSION_CONTROL: No token staked on coordinates');
  //     });

  //     it('Should revert on timeout', async function () {
  //       await increaseTime(raidTimeout + 1);
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));
  //       await expect(
  //         missionControl.notifyRaided(
  //           await alice.getAddress(),
  //           startTime,
  //           1,
  //           [
  //             {
  //               x: 1,
  //               y: -1,
  //               z: 0,
  //             },
  //           ],
  //           digest.v,
  //           digest.r,
  //           digest.s,
  //         ),
  //       ).to.revertedWith('MISSION_CONTROL: Signature has timed out');
  //     });

  //     it('Should revert on double raid', async function () {
  //       let digest = await getRaidDigest(missionControl, owner, alice, BigNumber.from(startTime), BigNumber.from(1));

  //       await expect(missionControl.notifyRaided(await alice.getAddress(), startTime, 1, [firstRingOrders[0], firstRingOrders[1]], digest.v, digest.r, digest.s)).to.not
  //         .reverted;

  //       await expect(
  //         missionControl.notifyRaided(await alice.getAddress(), startTime, 1, [firstRingOrders[0], firstRingOrders[1]], digest.v, digest.r, digest.s),
  //       ).to.revertedWith('MISSION_CONTROL: Raid already registered');
  //     });
  //   });
  // });

  describe('Events', function () {
    it('Should emit nftPlaced emit on placing NFT', async function () {
      let count = 1;
      for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
          for (let k = -1; k < 2; k++) {
            if (i + j + k == 0 && (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2 == 1) {
              const orderPlacementBottom: placeOrder = { order: { x: i, y: j, z: k }, tokenId: count, tokenAddress: stakeable_base.address };
              const orderPlacementTop: placeOrder = { order: { x: i, y: j, z: k }, tokenId: count, tokenAddress: stakeable_top.address };

              await expect(missionControl.connect(alice).placeNFT(orderPlacementBottom))
                .to.emit(missionControl, 'NFTPlaced')
                .withArgs(await alice.getAddress(), stakeable_base.address, count, i, j);
              await expect(missionControl.connect(alice).placeNFT(orderPlacementTop))
                .to.emit(missionControl, 'NFTPlaced')
                .withArgs(await alice.getAddress(), stakeable_top.address, count, i, j);
              count += 1;
            }
          }
        }
      }
    });

    it('Should emit nftRemoved emit on removing NFT', async function () {
      let count = 1;
      for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
          for (let k = -1; k < 2; k++) {
            if (i + j + k == 0 && (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2 == 1) {
              console.log(`Now Placing on ${i}, ${j}`);
              const orderPlacementBottom: placeOrder = { order: { x: i, y: j, z: k }, tokenId: count, tokenAddress: stakeable_base.address };
              const orderPlacementTop: placeOrder = { order: { x: i, y: j, z: k }, tokenId: count, tokenAddress: stakeable_top.address };
              await missionControl.connect(alice).placeNFT(orderPlacementBottom);
              await missionControl.connect(alice).placeNFT(orderPlacementTop);
              count += 1;
            }
          }
        }
      }
      count = 1;
      for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
          for (let k = -1; k < 2; k++) {
            if (i + j + k == 0 && (Math.abs(i) + Math.abs(j) + Math.abs(k)) / 2 == 1) {
              await expect(missionControl.connect(alice).removeNFT({ x: i, y: j, z: k }, 1))
                .to.emit(missionControl, 'NFTRemoved')
                .withArgs(alice.address, stakeable_top.address, stakeable_base.address, count, count, i, j);
              await expect(missionControl.connect(alice).removeNFT({ x: i, y: j, z: k }, 0))
                .to.emit(missionControl, 'NFTRemoved')
                .withArgs(alice.address, stakeable_base.address, constants.ZERO_ADDRESS, count, 0, i, j);
              count += 1;
            }
          }
        }
      }
    });
    it('Should emit Whitelist set  event when whitelisting', async function () {
      await expect(missionControl.setWhitelist(notWhitelisted.address, true)).to.emit(missionControl, 'WhitelistSet').withArgs(notWhitelisted.address, true);
    });
    it('Should emit MintSet event when setting mint', async function () {
      await expect(missionControl.setMint(mintable.address)).to.emit(missionControl, 'MintSet').withArgs(mintable.address);
    });
    it('Should emit RadiusSet event when setting radius', async function () {
      await expect(missionControl.setRadius(2)).to.emit(missionControl, 'RadiusSet').withArgs(2);
    });
  });

  describe('Administrative', function () {
    it('Allows owner to whitelist', async function () {
      await expect(missionControl.setWhitelist(notWhitelisted.address, true)).to.not.reverted;
      const orderPlacement1: placeOrder = { order: { x: 1, y: -1, z: 0 }, tokenId: 1, tokenAddress: notWhitelisted.address };
      const orderPlacement2: placeOrder = { order: { x: 1, y: 0, z: -1 }, tokenId: 2, tokenAddress: notWhitelisted.address };

      await expect(missionControl.connect(alice).placeNFT(orderPlacement1)).to.not.reverted;
      await expect(missionControl.setWhitelist(notWhitelisted.address, false)).to.not.reverted;
      await expect(missionControl.connect(alice).placeNFT(orderPlacement2)).to.revertedWith('MC__NotWhitelisted()');
    });

    it('Does not allow Alice to whitelist', async function () {
      await expect(missionControl.connect(alice).setWhitelist(notWhitelisted.address, true)).to.revertedWith('Ownable: caller is not the owner');
    });

    it('Allows owner to set mint', async function () {
      await expect(missionControl.setMint(mintable.address)).to.not.reverted;
    });

    it('Does not allow Alice to set mint', async function () {
      await expect(missionControl.connect(alice).setMint(mintable.address)).to.revertedWith('Ownable: caller is not the owner');
    });

    it('Allows owner set radius', async function () {
      await expect(missionControl.setRadius(2)).to.emit(missionControl, 'RadiusSet').withArgs(2);
    });

    it('Does not allow Alice to set radius', async function () {
      await expect(missionControl.connect(alice).setRadius(2)).to.revertedWith('Ownable: caller is not the owner');
    });
  });

  /**

     describe("Flaky tests", function () {

     // Tests that can give a false negatives randomly, though very rarely

        it("Should give approximately the right amount of crates", async function () {
            await missionControl.connect(alice).placeNFT(orderPlacement);
            let N = 2 * 1000
            this.timeout(N * 20)
            let expected = N * 10 * 0.005;
            for (let i = 0; i < N; i++) {
                await missionControl.connect(alice).collectFromTiles([{x: 1, y: -1, z: 0, amount: 10}])
            }
            expect(Math.abs((await mintable.check_balance(await alice.getAddress(), 15)) / expected - 1) < 0.25).equal(true);

        });
    });
     */
});

async function increaseTime(amount) {
  await hre.network.provider.send('evm_increaseTime', [amount]);
  await hre.network.provider.send('evm_mine');
  //console.log("EVM time " + amount + " seconds increased!")
}

const getRaidDigest = async (missionControl: Contract, signer: SignerWithAddress, defender: SignerWithAddress, timestamp: BigNumber, raidId: BigNumber) => {
  const domain = {
    name: 'Mission Control',
    version: '1',
    chainId: (await ethers.provider.getNetwork()).chainId,
    verifyingContract: missionControl.address,
  };

  const types = {
    RaidMessage: [
      { name: '_defender', type: 'address' },
      { name: '_timestamp', type: 'uint256' },
      { name: '_raidId', type: 'uint256' },
    ],
  };

  const value = {
    _defender: defender.address,
    _timestamp: timestamp,
    _raidId: raidId,
  };

  const signature = await signer._signTypedData(domain, types, value);
  return utils.splitSignature(signature);
};

const getCollectDigest = async (missionControl: Contract, signer: SignerWithAddress, user: SignerWithAddress, nonce: BigNumber) => {
  const domain = {
    name: 'Mission Control',
    version: '1',
    chainId: (await ethers.provider.getNetwork()).chainId,
    verifyingContract: missionControl.address,
  };

  const types = {
    CollectMessage: [
      { name: '_user', type: 'address' },
      { name: '_nonce', type: 'uint256' },
    ],
  };

  const value = {
    _user: user.address,
    _nonce: nonce,
  };

  const signature = await signer._signTypedData(domain, types, value);
  return utils.splitSignature(signature);
};
