import { assert, expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { Contract, BigNumber, utils } from 'ethers';
import { parseEther } from 'ethers/lib/utils';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

describe('Gravity Grade Giveaways', () => {
  let gravityGrade: Contract;
  let gravityGradeGiveaways: Contract;

  let zack: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;

  beforeEach(async () => {
    [zack, alice, bob] = await ethers.getSigners();

    let ggFactory = await ethers.getContractFactory('GravityGrade');
    gravityGrade = await upgrades.deployProxy(ggFactory, ['Gravity Grade', 'IX-GG']);

    let ggGiveawayFactory = await ethers.getContractFactory('GravityGradeGiveaways');
    gravityGradeGiveaways = await upgrades.deployProxy(ggGiveawayFactory);

    await gravityGrade.setModerator(alice.address);
    await gravityGrade.connect(alice).setTokenUri(1, 'URI 1');
    await gravityGrade.connect(alice).setTrusted(gravityGradeGiveaways.address, true);

    await gravityGradeGiveaways.setGravityGrade(gravityGrade.address);
    await gravityGradeGiveaways.setSigner(alice.address);
  });
  describe('Claim', () => {
    let giveawayId = 1;
    let tokenId = 1;
    let amount = 5;
    let totalClaims = 100;
    beforeEach(async () => {
      await gravityGradeGiveaways.createGiveaway(tokenId, amount, totalClaims);
    });
    it('Claims as expected', async () => {
      const data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(giveawayId), bob);
      expect(await gravityGradeGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s))
        .to.emit(gravityGradeGiveaways, 'GiveawayClaimed')
        .withArgs(bob.address, giveawayId);
      const info = await gravityGradeGiveaways.getGiveawayInfo(giveawayId);
      expect(info['claimed']).to.equal(1);
      expect(await gravityGradeGiveaways.canClaim(bob.address, giveawayId)).to.false;
      expect(await gravityGrade.balanceOf(bob.address, tokenId)).to.equal(amount);
    });
  });
  describe('Claim reverts', () => {
    let tokenId = 1;
    let giveawayId = 1;
    let amount = 5;
    let totalClaims = 2;
    beforeEach(async () => {
      await gravityGradeGiveaways.createGiveaway(tokenId, amount, totalClaims);
    });
    it('Reverts on trying to claim more than once', async () => {
      let data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(giveawayId), bob);
      await gravityGradeGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s);
      data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(giveawayId), bob);
      await expect(
        gravityGradeGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s),
      ).to.revertedWith(`GG_Giveaway_AlreadyClaimed("${bob.address}")`);
    });
    it('Reverts on giveaway max amount exceeded', async () => {
      let data;
      let wallet;
      for (let i = 0; i < totalClaims; i++) {
        wallet = ethers.Wallet.createRandom();
        wallet = wallet.connect(ethers.provider);
        await zack.sendTransaction({ to: wallet.address, value: ethers.utils.parseEther('1') });
        data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(giveawayId), wallet);
        await gravityGradeGiveaways.connect(wallet).claim(giveawayId, data.v, data.r, data.s);
      }
      wallet = ethers.Wallet.createRandom();
      wallet = wallet.connect(ethers.provider);
      await zack.sendTransaction({ to: wallet.address, value: ethers.utils.parseEther('1') });
      data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(giveawayId), wallet);
      await expect(
        gravityGradeGiveaways.connect(wallet).claim(giveawayId, data.v, data.r, data.s),
      ).to.revertedWith(`GG_Giveaway_MaxAmountExceeded(${giveawayId})`);
    });
    it('Reverts on invalid giveaway id', async () => {
      let invalidId = 999;
      const data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(invalidId), bob);
      await expect(
        gravityGradeGiveaways.connect(bob).claim(invalidId, data.v, data.r, data.s),
      ).to.revertedWith(`GG_Giveaway_InvalidId(${invalidId})`);
    });
    it('Reverts on incorrect signature', async () => {
      const data = await signMessage(
        gravityGradeGiveaways,
        alice,
        BigNumber.from(giveawayId),
        alice,
      );
      await expect(
        gravityGradeGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s),
      ).to.revertedWith(`GG_Giveaway_InvalidSignature()`);
    });
  });
  describe('Create Giveaway', () => {
    let tokenId = 1;
    let amount = 5;
    let totalClaims = 100;
    it('Creates giveaway successfully', async () => {
      await expect(gravityGradeGiveaways.createGiveaway(tokenId, amount, totalClaims))
        .to.emit(gravityGradeGiveaways, 'GiveawayCreated')
        .withArgs(await gravityGradeGiveaways.s_totalGiveaways(), tokenId, amount, totalClaims);
    });
  });
  describe('Create Giveaway Reverts', () => {
    let tokenId = 1;
    let amount = 5;
    let totalClaims = 100;
    it('Reverts on non-owner trying to create giveaway', async () => {
      await expect(
        gravityGradeGiveaways.connect(bob).createGiveaway(tokenId, amount, totalClaims),
      ).to.revertedWith('Ownable: caller is not the owner');
    });
  });
  describe('Update Giveaway', () => {
    let tokenId = 1;
    let giveawayId = 1;
    let amount = 5;
    let totalClaims = 1;
    let newTotalClaims = 2;
    let info;
    
    beforeEach(async () => {
      await gravityGradeGiveaways.createGiveaway(tokenId, amount, totalClaims);
    });
    it('Updates giveaway successfully', async () => {
      await expect(gravityGradeGiveaways.updateGiveaway(giveawayId, tokenId, amount, newTotalClaims))
        .to.emit(gravityGradeGiveaways, 'GiveawayUpdated')
        .withArgs(giveawayId, tokenId, amount, newTotalClaims);
      info = await gravityGradeGiveaways.getGiveawayInfo(giveawayId);
      expect(info['cap']).to.equal(newTotalClaims);
    });

  });
  describe('Delete Giveaway', () => {
    let tokenId = 1;
    let giveawayId = 1;
    let amount = 5;
    let totalClaims = 100;
    beforeEach(async () => {
      await gravityGradeGiveaways.createGiveaway(tokenId, amount, totalClaims);
    });
    it('Deletes giveaway successfully', async () => {
      await expect(gravityGradeGiveaways.deleteGiveaway(giveawayId))
        .to.emit(gravityGradeGiveaways, 'GiveawayDeleted')
        .withArgs(giveawayId);
    });
  });
  describe('Delete Giveaway Reverts', () => {
    let tokenId = 1;
    let giveawayId = 1;
    let amount = 5;
    let totalClaims = 100;
    beforeEach(async () => {
      await gravityGradeGiveaways.createGiveaway(tokenId, amount, totalClaims);
    });
    it('Reverts on non-owner trying to delete giveaway', async () => {
      await expect(gravityGradeGiveaways.connect(bob).deleteGiveaway(giveawayId)).to.revertedWith(
        'Ownable: caller is not the owner',
      );
    });
    it('Reverts on invalid giveaway id', async () => {
      let invalidId = 999;
      await expect(gravityGradeGiveaways.deleteGiveaway(invalidId)).to.revertedWith(
        `GG_Giveaway_InvalidId(${invalidId})`,
      );
    });
    it('Reverts on trying to claim after giveaway deleted', async () => {
      await gravityGradeGiveaways.deleteGiveaway(giveawayId);
      const data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(giveawayId), bob);

      await expect(
        gravityGradeGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s),
      ).to.revertedWith(`GG_Giveaway_InvalidId(${giveawayId})`);
    });
  });
  describe('Set Gravity Grade Contract', () => {
    it('Sets GG contract as expected', async () => {
      await expect(gravityGradeGiveaways.setGravityGrade(gravityGrade.address)).to.not.reverted;
    });
    it('Reverts on non-owner call', async () => {
      await expect(
        gravityGradeGiveaways.connect(bob).setGravityGrade(gravityGrade.address),
      ).to.revertedWith('Ownable: caller is not the owner');
    });
  });
  describe('Set signer address', () => {
    it('Sets signer as expected', async () => {
      await expect(gravityGradeGiveaways.setSigner(alice.address)).to.not.reverted;
    });
    it('Reverts on non-owner call', async () => {
      await expect(gravityGradeGiveaways.connect(bob).setSigner(alice.address)).to.revertedWith(
        'Ownable: caller is not the owner',
      );
    });
  });
  describe('Get Giveaway Info', () => {
    let giveawayId = 1;
    let tokenId = 1;
    let amount = 10;
    let totalClaims = 100;
    let info;
    beforeEach(async () => {
      await gravityGradeGiveaways.createGiveaway(tokenId, amount, totalClaims);
    });
    it('Gets info as expected', async () => {
      info = await gravityGradeGiveaways.getGiveawayInfo(giveawayId);
      expect(info['id']).to.equal(giveawayId);
      expect(info['tokenId']).to.equal(tokenId);
      expect(info['amount']).to.equal(amount);
      expect(info['claimed']).to.equal(0);
      expect(info['cap']).to.equal(totalClaims);
    });
    it('Gets correct info after some airdrops were claimed', async () => {
      let data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(giveawayId), bob);
      await gravityGradeGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s);
      info = await gravityGradeGiveaways.getGiveawayInfo(giveawayId);
      expect(info['id']).to.equal(giveawayId);
      expect(info['tokenId']).to.equal(tokenId);
      expect(info['amount']).to.equal(amount);
      expect(info['claimed']).to.equal(1);
      expect(info['cap']).to.equal(totalClaims);
    });
    it('Reverts on invalid id', async () => {
      let invalidId = 999;
      await expect(gravityGradeGiveaways.getGiveawayInfo(invalidId)).to.reverted;
    });
  });
  describe('Can Claim', () => {
    let giveawayId = 1;
    let tokenId = 1;
    let amount = 10;
    let totalClaims = 100;
    beforeEach(async () => {
      await gravityGradeGiveaways.createGiveaway(tokenId, amount, totalClaims);
    });
    it('User should be able to claim by default', async () => {
      expect(await gravityGradeGiveaways.canClaim(bob.address, giveawayId)).to.true;
    });
    it('Reverts on user trying to claim more than once.', async () => {
      let data = await signMessage(gravityGradeGiveaways, alice, BigNumber.from(giveawayId), bob);
      await gravityGradeGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s);
      expect(await gravityGradeGiveaways.canClaim(bob.address, giveawayId)).to.false;
    });
    it('Reverts on invalid id', async () => {
      let invalidId = 999;
      await expect(gravityGradeGiveaways.canClaim(bob.address, invalidId)).to.reverted;
    });
  });
});

const signMessage = async (
  giveaway: Contract,
  signer: SignerWithAddress,
  id: BigNumber,
  sender: SignerWithAddress,
) => {
  const domain = {
    name: 'GG__Giveaway',
    version: '1',
    chainId: (await ethers.provider.getNetwork()).chainId,
    verifyingContract: giveaway.address,
  };

  const types = {
    AirdropMessage: [
      { name: 'id', type: 'uint256' },
      { name: 'sender', type: 'address' },
      { name: 'nonce', type: 'uint256' },
    ],
  };

  const value = {
    id,
    sender: sender.address,
    nonce: await giveaway.nonces(sender.address, id),
  };

  const signature = await signer._signTypedData(domain, types, value);
  return utils.splitSignature(signature);
};
