import {expect} from 'chai';
import hre, {ethers, upgrades} from 'hardhat';
import {Signer, Contract, BigNumber, providers, utils} from 'ethers';
import {PIXCategory, PIXSize} from './utils';
import {constants} from '@openzeppelin/test-helpers';
import {parseEther} from "ethers/lib/utils";

describe('Oracle manager mock', function () {

    let maxCoin: Contract;
    let shazCoin: Contract;

    let oracle: Contract;

    let owner: Signer;
    let alice_moderator: Signer;
    let bob_player: Signer;

    beforeEach(async function () {
        [owner, alice_moderator, bob_player] = await ethers.getSigners();

        let erc20Factory = await ethers.getContractFactory("ERC20Mock");
        maxCoin = await erc20Factory.deploy("MaxCoin", "MXC", await owner.getAddress(), 0);
        shazCoin = await erc20Factory.deploy("ShazCoin", "SZC", await owner.getAddress(), 0);

        let oracleFactory = await ethers.getContractFactory("OracleManagerMock");
        oracle = await oracleFactory.deploy();
        await oracle.setRatio(constants.ZERO_ADDRESS, shazCoin.address, 1, 10);
        await oracle.setRatio(constants.ZERO_ADDRESS, maxCoin.address, 1, 5);
        await oracle.setRatio(shazCoin.address, maxCoin.address, 10, 5);
    });

    describe("Basic tests", async function () {
        it("Should give me the correct amount of max -> shaz", async function () {
            let num_max_coins = 23
            expect(await oracle.callStatic.getAmountOut(maxCoin.address, shazCoin.address, num_max_coins)).to.equal(BigNumber.from(Math.floor(num_max_coins / 2)))
        })


        it("Should give me the correct amount of shaz -> max", async function () {
            let shaz_coins = 23
            expect(await oracle.callStatic.getAmountOut(shazCoin.address, maxCoin.address, shaz_coins)).to.equal(BigNumber.from(Math.floor(shaz_coins * 2)))
        })

        it("Should give me the correct amount of eth -> max", async function () {
            let eth = 23
            expect(await oracle.callStatic.getAmountOut(constants.ZERO_ADDRESS, maxCoin.address, eth)).to.equal(BigNumber.from(Math.floor(eth / 5)))
        })

        it("Should give me the correct amount of eth -> shaz", async function () {
            let eth = 23
            expect(await oracle.callStatic.getAmountOut(constants.ZERO_ADDRESS, shazCoin.address, eth)).to.equal(BigNumber.from(Math.floor(eth / 10)))
        })

        it("Should give me the correct amount of max -> eth", async function () {
            let max = 23
            expect(await oracle.callStatic.getAmountOut(maxCoin.address, constants.ZERO_ADDRESS, max)).to.equal(BigNumber.from(Math.floor(max * 5)))
        })

        it("Should give me the correct amount of shaz -> eth", async function () {
            let max = 23
            expect(await oracle.callStatic.getAmountOut(shazCoin.address, constants.ZERO_ADDRESS, max)).to.equal(BigNumber.from(Math.floor(max * 10)))
        })

    })


});
