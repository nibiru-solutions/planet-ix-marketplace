import { expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { Signer, Contract, BigNumber, providers, utils } from 'ethers';
import { ERC1155MockAssetManager, ERC721Mock, GravityGrade, GravityGradeMock, PIXAssetsGenericBurn, VRFCoordinatorV2Mock, VRFManager } from '../src/types';
import { getAddress } from 'ethers/lib/utils';

describe('Gravity Grade Generic Burn', function () {
  let gravityGrade: GravityGradeMock;
  let gBurn: PIXAssetsGenericBurn;
  let assets: ERC1155MockAssetManager;
  let erc721Mock: ERC721Mock;

  let owner: Signer;
  let alice: Signer;

  let vrfCoordinatorV2Mock: VRFCoordinatorV2Mock;
  let vrfManager: VRFManager;
  const SUBSCRIPTION_ID = 1;
  const KEYHASH = ethers.utils.defaultAbiCoder.encode(['uint256'], [1]);
  const CALLBACK_GAS_LIMIT = 2_500_000;
  const REQUEST_CONFIRMATIONS = 3;
  const NUM_WORDS = 1;

  beforeEach(async function () {
    [owner, alice] = await ethers.getSigners();

    let VRFCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
    vrfCoordinatorV2Mock = await VRFCoordinatorV2MockFactory.deploy(0, 6);
    await vrfCoordinatorV2Mock.createSubscription();
    await vrfCoordinatorV2Mock.fundSubscription(SUBSCRIPTION_ID, ethers.utils.parseEther('1000000'));

    let VRFManagerFactory = await ethers.getContractFactory('VRFManager');
    vrfManager = await VRFManagerFactory.deploy(vrfCoordinatorV2Mock.address);

    let ggFactory = await ethers.getContractFactory('GravityGradeMock');
    gravityGrade = await ggFactory.deploy();
    let assetFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    assets = await assetFactory.deploy('toy uri');
    let erc721Factory = await ethers.getContractFactory('ERC721Mock');
    erc721Mock = await erc721Factory.deploy('TOYTOKEN', 'TOY');

    let gBurnFactory = await ethers.getContractFactory('PIXAssetsGenericBurn');
    gBurn = (await upgrades.deployProxy(gBurnFactory, [])) as PIXAssetsGenericBurn;

    await gBurn.setPixAssets(assets.address);
    await gBurn.setVRFOracle(vrfManager.address);

    await assets.setTrusted(gBurn.address, true);
    await erc721Mock.setTrusted(gBurn.address, true);

    await vrfManager.setConsumer(gBurn.address, SUBSCRIPTION_ID, KEYHASH, CALLBACK_GAS_LIMIT, REQUEST_CONFIRMATIONS, NUM_WORDS);
  });

  describe('Create content category', function () {
    let tokenId = 1;
    let tokenId2 = 2;
    it('Should emit event', async function () {
      await expect(gBurn.createContentCategory(tokenId)).to.emit(gBurn, 'CategoryCreated').withArgs(tokenId, 1);
    });
    it('Handles creation of multiple categories', async () => {
      await expect(gBurn.createContentCategory(tokenId)).to.emit(gBurn, 'CategoryCreated').withArgs(tokenId, 1);
      await expect(gBurn.createContentCategory(tokenId)).to.emit(gBurn, 'CategoryCreated').withArgs(tokenId, 2);
      await expect(gBurn.createContentCategory(tokenId)).to.emit(gBurn, 'CategoryCreated').withArgs(tokenId, 3);
      await expect(gBurn.createContentCategory(tokenId2)).to.emit(gBurn, 'CategoryCreated').withArgs(tokenId2, 1);
      await expect(gBurn.createContentCategory(tokenId2)).to.emit(gBurn, 'CategoryCreated').withArgs(tokenId2, 2);
      await expect(gBurn.createContentCategory(tokenId2)).to.emit(gBurn, 'CategoryCreated').withArgs(tokenId2, 3);
    });

    it('Should not assign subsequent content categories to same id', async function () {
      expect(true).to.equal(true);
    });
  });

  describe('Create content category reverts', function () {
    let tokenId1 = 1;
    let tokenId2 = 2;
    it('Should revert on non owner call', async function () {
      await expect(gBurn.connect(alice).createContentCategory(tokenId1)).to.revertedWith(`GB__NotGov("${await alice.getAddress()}")`);
    });
  });

  describe('Delete content category', function () {
    let tokenId1 = 1;
    let tokenId2 = 2;
    let categoryId1;
    let categoryId2;
    let categoryId3;
    let categoryId4;
    let categoryId5;
    let categoryId6;
    beforeEach(async () => {
      categoryId1 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);

      categoryId2 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);

      categoryId3 = await gBurn.callStatic.createContentCategory(tokenId2);
      await gBurn.createContentCategory(tokenId2);

      categoryId4 = await gBurn.callStatic.createContentCategory(tokenId2);
      await gBurn.createContentCategory(tokenId2);

      categoryId5 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);

      categoryId6 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
    });
    it('Should emit event', async function () {
      await expect(gBurn.deleteContentCategory(tokenId1, categoryId1)).to.emit(gBurn, 'CategoryDeleted').withArgs(tokenId1, categoryId1);
    });
    it('Should remove content category from getContentCategory call', async function () {
      let categoriesBefore = await gBurn.getContentCategories(tokenId1);
      let lengthBefore = categoriesBefore.length;

      await gBurn.deleteContentCategory(tokenId1, categoryId1);

      let categoriesAfter = await gBurn.getContentCategories(tokenId1);
      expect(categoriesAfter.find((category) => category['id'].toNumber() == categoryId1)).to.undefined;
      expect(categoriesAfter.length).to.equal(lengthBefore - 1);
    });
    it('Deletes all categories from a tokenId successfully', async () => {
      let categoriesBefore = await gBurn.getContentCategories(tokenId1);
      let lengthBefore = categoriesBefore.length;

      await gBurn.deleteContentCategory(tokenId1, categoryId1);
      await gBurn.deleteContentCategory(tokenId1, categoryId2);

      let categoriesAfter = await gBurn.getContentCategories(tokenId1);
      expect(categoriesAfter.find((category) => category['id'].toNumber() == categoryId1)).to.undefined;
      expect(categoriesAfter.find((category) => category['id'].toNumber() == categoryId2)).to.undefined;
      expect(categoriesAfter.length).to.equal(lengthBefore - 2);
    });
  });

  describe('Delete content category reverts', function () {
    let tokenId1 = 1;
    let categoryId1;
    beforeEach(async () => {
      categoryId1 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
    });
    it('Should revert on non owner call', async function () {
      await expect(gBurn.connect(alice).deleteContentCategory(tokenId1, categoryId1)).to.revertedWith(`GB__NotGov("${await alice.getAddress()}")`);
    });
    it('Should revert on invalid category id', async function () {
      await expect(gBurn.deleteContentCategory(tokenId1, 99)).to.revertedWith(`GB__InvalidCategoryId(${99})`);
    });
    it('Should revert on deleted category id', async function () {
      await gBurn.deleteContentCategory(tokenId1, categoryId1);
      await expect(gBurn.deleteContentCategory(tokenId1, categoryId1)).to.revertedWith(`GB__InvalidCategoryId(${categoryId1})`);
    });
  });

  describe('Set content amounts', function () {
    let amounts = [20, 30, 40, 10, 5];
    let amountWeights = [50, 20, 10, 15, 25];
    let categoryId1;
    let tokenId1 = 1;
    beforeEach(async () => {
      await gBurn.setMaxDraws(tokenId1, 40);
      categoryId1 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
    });
    it('Should emit event', async function () {
      await expect(gBurn.setContentAmounts(tokenId1, categoryId1, amounts, amountWeights))
        .to.emit(gBurn, 'ContentAmountsUpdated')
        .withArgs(tokenId1, categoryId1, amounts, amountWeights);
    });
  });

  describe('Set content amounts reverts', function () {
    let amounts = [20, 30, 40, 10, 5];
    let amountWeights = [50, 20, 10, 15, 25];
    let categoryId1;
    let tokenId1 = 1;
    beforeEach(async () => {
      await gBurn.setMaxDraws(tokenId1, 40);
      categoryId1 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
    });
    it('Should revert on non owner call', async function () {
      await expect(gBurn.connect(alice).setContentAmounts(tokenId1, categoryId1, amounts, amountWeights)).to.revertedWith(
        `GB__NotGov("${await alice.getAddress()}")`,
      );
    });
    it('Should revert on zero weight', async function () {
      await expect(gBurn.setContentAmounts(tokenId1, categoryId1, amounts, [50, 20, 10, 0, 25])).to.revertedWith('GB__ZeroWeight()');
    });
    it('Should revert on invalid category id', async function () {
      await expect(gBurn.setContentAmounts(tokenId1, 99, amounts, amountWeights)).to.revertedWith(`GB__InvalidCategoryId(${99})`);
    });
    it('Should revert on argument arrays not being the same lengths', async function () {
      await expect(gBurn.setContentAmounts(tokenId1, categoryId1, amounts, [50, 20, 10, 15, 25, 30])).to.revertedWith('GB__ArraysNotSameLength()');
    });
  });

  describe('Set contents', function () {
    let tokenIds = [1, 2, 3];
    let amounts = [20, 30, 40];
    let amountWeights = [50, 20, 10];
    let categoryId1;
    let tokenId1 = 1;
    beforeEach(async () => {
      categoryId1 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
    });
    it('Should emit event', async function () {
      await expect(gBurn.setContents(tokenId1, categoryId1, [assets.address, assets.address, assets.address], tokenIds, amounts, amountWeights))
        .to.emit(gBurn, 'ContentsUpdated')
        .withArgs(tokenId1, categoryId1, [assets.address, assets.address, assets.address], tokenIds, amounts, amountWeights);
    });
    it('Works with upgradeable contracts', async () => {
      await expect(
        gBurn.setContents(tokenId1, categoryId1, [gravityGrade.address, gravityGrade.address, gravityGrade.address], tokenIds, amounts, amountWeights),
      )
        .to.emit(gBurn, 'ContentsUpdated')
        .withArgs(tokenId1, categoryId1, [gravityGrade.address, gravityGrade.address, gravityGrade.address], tokenIds, amounts, amountWeights);
    });
  });
  describe('Set contents reverts', function () {
    let tokenIds = [1, 2, 3];
    let amounts = [20, 30, 40];
    let amountWeights = [50, 20, 10];
    let categoryId1;
    let tokenId1 = 1;
    beforeEach(async () => {
      categoryId1 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
    });
    it('Should revert on non owner call', async function () {
      await expect(
        gBurn.connect(alice).setContents(tokenId1, categoryId1, [assets.address, assets.address, assets.address], tokenIds, amounts, amountWeights),
      ).to.revertedWith(`GB__NotGov("${await alice.getAddress()}")`);
    });
    it('Should revert on zero weight', async function () {
      await expect(gBurn.setContents(tokenId1, categoryId1, [assets.address, assets.address, assets.address], tokenIds, amounts, [50, 20, 0])).to.revertedWith(
        'GB__ZeroWeight()',
      );
    });
    it('Should revert on zero amount', async function () {
      await expect(
        gBurn.setContents(tokenId1, categoryId1, [assets.address, assets.address, assets.address], tokenIds, [20, 0, 40], amountWeights),
      ).to.revertedWith('GB_ZeroAmount()');
    });
    it('Should revert on invalid category id', async function () {
      await expect(gBurn.setContents(tokenId1, 99, [assets.address, assets.address, assets.address], tokenIds, amounts, amountWeights)).to.revertedWith(
        'GB__InvalidCategoryId(99)',
      );
    });
    it('Should revert on non erc721 or erc1155 being added', async function () {
      await expect(
        gBurn.setContents(
          tokenId1,
          categoryId1,
          [assets.address, '0xa406d6AD2f0713e1E8c7aE5FB6F9F75A52714E89', assets.address],
          tokenIds,
          amounts,
          amountWeights,
        ),
      ).to.revertedWith('GB__NotTrustedMintable("0xa406d6AD2f0713e1E8c7aE5FB6F9F75A52714E89")');
    });
    it('Should revert on argument arrays not being the same lengths', async function () {
      await expect(
        gBurn.setContents(tokenId1, categoryId1, [assets.address, assets.address, assets.address], tokenIds, amounts, [50, 20, 10, 5]),
      ).to.revertedWith('GB__ArraysNotSameLength()');
    });
  });

  describe('Burn pack', function () {
    let burnAmount = 1;
    let tokenId1 = 1;
    let tokens;
    let categoryId1;
    let categoryId2;
    let categoryId3;
    let predetermined_draws;
    let maxDraws = 10;

    beforeEach(async () => {
      tokens = [assets.address, assets.address, assets.address];

      predetermined_draws = 10;
      await gBurn.setMaxDraws(tokenId1, maxDraws);

      categoryId1 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
      await gBurn.setContentAmounts(tokenId1, categoryId1, [predetermined_draws, 2], [10, 10]);
      await gBurn.setContents(tokenId1, categoryId1, [gravityGrade.address], [19], [1], [1]);

      categoryId2 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
      await gBurn.setContentAmounts(tokenId1, categoryId2, [predetermined_draws, 4, 5], [50, 20, 10]);
      await gBurn.setContents(
        tokenId1,
        categoryId2,
        [assets.address, assets.address, assets.address], // token addresses
        [1, 2, 3], // token Ids
        [2, 3, 5], // token amounts
        [40, 20, 50], // token weights
      );

      categoryId3 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
      await gBurn.setContentAmounts(tokenId1, categoryId3, [5, predetermined_draws, 7], [225, 200, 200]);
      await gBurn.setContents(tokenId1, categoryId3, [erc721Mock.address], [0], [1], [1]);

      await gBurn.whitelistToken(tokenId1, true);
      await quickDrop(alice, assets, 1, 100);
    });
    it('Should emit event', async function () {
      let tx = await gBurn.connect(alice).burnPack(tokenId1, 1, false);
      await expect(tx)
        .to.emit(gBurn, 'PackOpened')
        .withArgs(await alice.getAddress(), 1, 1);
    });
    it('Should burn whitelisted token', async function () {
      let tx = await gBurn.connect(alice).burnPack(tokenId1, burnAmount, false);
      expect(await assets.balanceOf(await alice.getAddress(), 1)).to.equal(99);
    });
    it('Should yield expected rewards when deterministic', async function () {
      let tx = await gBurn.connect(alice).burnPack(tokenId1, burnAmount, false);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });
      await printBalances(alice, gravityGrade, assets, erc721Mock);
    });
    it('Should yield expected rewards when conditional categories present', async () => {
      let conditionalFactory = await ethers.getContractFactory('MockConditionalProvider');
      let conditionalProvider = await conditionalFactory.deploy();

      await gBurn.setContentEligibility(tokenId1, categoryId1, conditionalProvider.address);
      await gBurn.setContentEligibility(tokenId1, categoryId2, conditionalProvider.address);

      let tx = await gBurn.connect(alice).burnPack(tokenId1, burnAmount, true);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });
      await printBalances(alice, gravityGrade, assets, erc721Mock);
    });
    it('Todo: Add a lot of various combinations of contents, like 10 of them', async function () {
      const categoryId4 = await gBurn.callStatic.createContentCategory(tokenId1);
      await gBurn.createContentCategory(tokenId1);
      await gBurn.setContentAmounts(tokenId1, categoryId4, [predetermined_draws, 4, 5], [50, 20, 10]);
      await gBurn.setContents(
        tokenId1,
        categoryId4,
        [
          assets.address,
          assets.address,
          assets.address,
          erc721Mock.address,
          erc721Mock.address,
          gravityGrade.address,
          assets.address,
          erc721Mock.address,
          gravityGrade.address,
          gravityGrade.address,
        ], // token addresses
        [1, 2, 3, 0, 0, 19, 4, 0, 20, 21], // token Ids
        [2, 3, 5, 1, 1, 4, 2, 1, 3, 3], // token amounts
        [10, 20, 30, 21, 33, 53, 20, 10, 5, 20], // token weights
      );

      let tx = await gBurn.connect(alice).burnPack(tokenId1, 4, false);
      let tx_fulfillCategory_draws = await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });
      const receipt = await tx_fulfillCategory_draws.wait();
      // console.log(receipt.events);
      await printBalances(alice, gravityGrade, assets, erc721Mock);
    });
  });

  describe('Burn pack reverts', function () {
    beforeEach(async () => {
      await quickDrop(alice, assets, 19, 100);
    });
    it('Should revert on non whitelisted token', async function () {
      await expect(gBurn.connect(alice).burnPack(19, 1, false)).to.revertedWith(`GB__TokenNotWhitelisted(19)`);
    });
  });

  describe('Set gravity grade', function () {
    it('Should emit event', async function () {
      await expect(gBurn.setPixAssets(assets.address)).to.emit(gBurn, 'PIXAssetsSet').withArgs(assets.address);
    });
  });

  describe('Set gravity grade reverts', function () {
    it('Should revert on non owner call', async function () {
      await expect(gBurn.connect(alice).setPixAssets(assets.address)).to.revertedWith(`GB__NotGov("${await alice.getAddress()}")`);
    });
  });
});

async function quickDrop(player: Signer, assets: ERC1155MockAssetManager, tokenId, num = 1) {
  await assets.mint(await player.getAddress(), tokenId, num);
}

async function printBalances(user: Signer, gravityGrade: Contract, assets: Contract, erc721Mock: Contract) {
  const aliceBal = await gravityGrade.balanceOf(await user.getAddress(), 19);
  const aliceBal1 = await assets.balanceOf(await user.getAddress(), 1);
  const aliceBal2 = await assets.balanceOf(await user.getAddress(), 2);
  const aliceBal3 = await assets.balanceOf(await user.getAddress(), 3);
  const aliceBal4 = await erc721Mock.balanceOf(await user.getAddress());
  console.log(`Alice's GG 19 balance: ${aliceBal}`);
  console.log(`Alice's Assets 1 balance: ${aliceBal1}`);
  console.log(`Alice's Assets 2 balance: ${aliceBal2}`);
  console.log(`Alice's Assets 3 balance: ${aliceBal3}`);
  console.log(`Alice's ERC721 balance: ${aliceBal4}`);
}
