import { ethers, upgrades } from 'hardhat';
import {
  PIERCER_DRONE_APOLLO,
  PIERCER_DRONE_ARTEMIS,
  PIERCER_DRONE_CHRONOS,
  PIERCER_DRONE_HELIOS,
  GENESIS_DRONE_APOLLO,
  GENESIS_DRONE_ARTEMIS,
  GENESIS_DRONE_CHRONOS,
  GENESIS_DRONE_HELIOS,
  NIGHT_DRONE_APOLLO,
  NIGHT_DRONE_ARTEMIS,
  NIGHT_DRONE_CHRONOS,
  NIGHT_DRONE_HELIOS,
  OUTLIER_FACILITY,
  COMMON_FACILITY,
  UNCOMMON_FACILITY,
  RARE_FACILITY,
  LEGENDARY_FACILITY,
  SOLAR_GRID2,
  GEOWELL2,
  TIDAL_TURBINES2,
  RADIANT_GRADENS2,
  FUSION_CORE2,
  SOLAR_GRID3,
  GEOWELL3,
  TIDAL_TURBINES3,
  RADIANT_GRADENS3,
  FUSION_CORE3,
  apollo_tiles,
  artemis_tiles,
  chronos_tiles,
  helios_tiles,
} from '../utils/mcResources';

const hre = require('hardhat');
import { Signer, Contract, BigNumber, providers, utils } from 'ethers';
import { AssetIds, PIXCategory, PIXSize, Distribution } from './utils';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { expect } from 'chai';

enum Class {
  Poisson,
  Linear,
  Facility,
}

describe('PIXAssetStakeableAdapter tests', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;
  let erc1155: Contract;
  let rarityProvider: Contract;
  let stakeable: Contract;
  let nonce = 0;
  const facilityEv = 12;
  const facilityCap = 12;
  const droneEv = 14;
  const droneCap = 14;
  const dayInSec = 86400;

  function getNonce() {
    nonce++;
    return nonce;
  }

  beforeEach(async function () {
    this.timeout(19000);
    nonce = 0;
    [owner, alice, bob] = await ethers.getSigners();

    let ercFactory = await ethers.getContractFactory('ERC1155Mock');
    erc1155 = await ercFactory.deploy('');

    let rarityFactory = await ethers.getContractFactory('RarityProviderMock');
    rarityProvider = await rarityFactory.deploy();

    let stakeableFactory = await ethers.getContractFactory('PIXAssetStakeableAdapter');
    stakeable = await upgrades.deployProxy(stakeableFactory);

    // Rarity providers provides a mock address for PIX in these tests
    await stakeable.setRarityProvider(rarityProvider.address, rarityProvider.address);
    await stakeable.setMissionControl(owner.address);
    await stakeable.setERC1155(erc1155.address);

    await erc1155.mint(alice.address, AssetIds.GenesisDrone, 1);
    await erc1155.mint(alice.address, AssetIds.FacilityOutlier, 100);
    await erc1155.mint(alice.address, AssetIds.FacilityCommon, 100);
    await erc1155.mint(alice.address, AssetIds.FacilityUncommon, 100);
    await erc1155.mint(alice.address, AssetIds.FacilityRare, 100);
    await erc1155.mint(alice.address, AssetIds.FacilityLegendary, 100);
    await erc1155.mint(alice.address, AssetIds.BronzeBadge, 1);
    // PIX categories are used in lieu of actual tokens ids in these tests
    let qualityKeys = Object.keys(PIXCategory).filter((v) => !isNaN(Number(v)));
    qualityKeys.forEach(async (value) => {
      await rarityProvider.setRarity(value, value);
    });
    await stakeable.setFacilityStakeRequirements(AssetIds.FacilityOutlier, PIXCategory.Outliers, true);
    await stakeable.setFacilityStakeRequirements(AssetIds.FacilityCommon, PIXCategory.Common, true);
    await stakeable.setFacilityStakeRequirements(AssetIds.FacilityUncommon, PIXCategory.Uncommon, true);
    await stakeable.setFacilityStakeRequirements(AssetIds.FacilityRare, PIXCategory.Rare, true);
    await stakeable.setFacilityStakeRequirements(AssetIds.FacilityLegendary, PIXCategory.Legendary, true);
    let hour = 60 * 60;
    await stakeable.setFacilityStats([8 * hour, 12 * hour, 16 * hour, 20 * hour, 24 * hour], [1, 3, 4, 5, 6]);

    // This is purely for testing

    await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);

    const tx1 = await stakeable.setFacilityStakeRequirements(52, 4, true);
    const tx2 = await stakeable.setFacilityStakeRequirements(53, 3, true);
    const tx3 = await stakeable.setFacilityStakeRequirements(54, 2, true);
    const tx4 = await stakeable.setFacilityStakeRequirements(55, 1, true);
    const tx5 = await stakeable.setFacilityStakeRequirements(56, 0, true);
    const tx6 = await stakeable.setFacilityStakeRequirements(57, 4, true);
    const tx7 = await stakeable.setFacilityStakeRequirements(58, 3, true);
    const tx8 = await stakeable.setFacilityStakeRequirements(59, 2, true);
    const tx9 = await stakeable.setFacilityStakeRequirements(60, 1, true);
    const tx10 = await stakeable.setFacilityStakeRequirements(61, 0, true);

    const ringsTx = await stakeable.setRings([
      { x: apollo_tiles[0].x, y: apollo_tiles[0].y, ring: 0 },
      { x: apollo_tiles[1].x, y: apollo_tiles[1].y, ring: 0 },
      { x: apollo_tiles[2].x, y: apollo_tiles[2].y, ring: 0 },
      { x: apollo_tiles[3].x, y: apollo_tiles[3].y, ring: 0 },
      { x: apollo_tiles[4].x, y: apollo_tiles[4].y, ring: 0 },
      { x: apollo_tiles[5].x, y: apollo_tiles[5].y, ring: 0 },
      { x: artemis_tiles[0].x, y: artemis_tiles[0].y, ring: 1 },
      { x: artemis_tiles[1].x, y: artemis_tiles[1].y, ring: 1 },
      { x: artemis_tiles[2].x, y: artemis_tiles[2].y, ring: 1 },
      { x: artemis_tiles[3].x, y: artemis_tiles[3].y, ring: 1 },
      { x: artemis_tiles[4].x, y: artemis_tiles[4].y, ring: 1 },
      { x: artemis_tiles[5].x, y: artemis_tiles[5].y, ring: 1 },
      { x: artemis_tiles[6].x, y: artemis_tiles[6].y, ring: 1 },
      { x: artemis_tiles[7].x, y: artemis_tiles[7].y, ring: 1 },
      { x: artemis_tiles[8].x, y: artemis_tiles[8].y, ring: 1 },
      { x: artemis_tiles[9].x, y: artemis_tiles[9].y, ring: 1 },
      { x: artemis_tiles[10].x, y: artemis_tiles[10].y, ring: 1 },
      { x: artemis_tiles[11].x, y: artemis_tiles[11].y, ring: 1 },
      { x: chronos_tiles[0].x, y: chronos_tiles[0].y, ring: 2 },
      { x: chronos_tiles[1].x, y: chronos_tiles[1].y, ring: 2 },
      { x: chronos_tiles[2].x, y: chronos_tiles[2].y, ring: 2 },
      { x: chronos_tiles[3].x, y: chronos_tiles[3].y, ring: 2 },
      { x: chronos_tiles[4].x, y: chronos_tiles[4].y, ring: 2 },
      { x: chronos_tiles[5].x, y: chronos_tiles[5].y, ring: 2 },
      { x: chronos_tiles[6].x, y: chronos_tiles[6].y, ring: 2 },
      { x: chronos_tiles[7].x, y: chronos_tiles[7].y, ring: 2 },
      { x: chronos_tiles[8].x, y: chronos_tiles[8].y, ring: 2 },
      { x: chronos_tiles[9].x, y: chronos_tiles[9].y, ring: 2 },
      { x: chronos_tiles[10].x, y: chronos_tiles[10].y, ring: 2 },
      { x: chronos_tiles[11].x, y: chronos_tiles[11].y, ring: 2 },
      { x: helios_tiles[0].x, y: helios_tiles[0].y, ring: 3 },
      { x: helios_tiles[1].x, y: helios_tiles[1].y, ring: 3 },
      { x: helios_tiles[2].x, y: helios_tiles[2].y, ring: 3 },
      { x: helios_tiles[3].x, y: helios_tiles[3].y, ring: 3 },
      { x: helios_tiles[4].x, y: helios_tiles[4].y, ring: 3 },
      { x: helios_tiles[5].x, y: helios_tiles[5].y, ring: 3 },
    ]);

    await setupResources(stakeable);

    let tx = await stakeable.setLeadTimeBonuses([0, 1, 2, 3], [10000, 9000, 9000, 8000]);
  });

  describe('onStaked', function () {
    it('Should allow staking a drone', async function () {
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.GenesisDrone)).to.equal(0);
      await stakeable.onStaked(alice.address, AssetIds.GenesisDrone, getNonce(), rarityProvider.address, PIXCategory.Outliers);
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.GenesisDrone)).to.equal(1);
    });
    it('Should allow staking a facility on correct PIX', async function () {
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.FacilityOutlier)).to.equal(0);
      await stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, getNonce(), rarityProvider.address, PIXCategory.Outliers);
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.FacilityOutlier)).to.equal(1);

      expect(await erc1155.balanceOf(stakeable.address, AssetIds.FacilityLegendary)).to.equal(0);
      await stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, getNonce(), rarityProvider.address, PIXCategory.Legendary);
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.FacilityLegendary)).to.equal(1);
    });
  });

  describe('onStaked reverts', function () {
    beforeEach(async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.setTokenInfo(AssetIds.GenesisDrone, true, false, false, Class.Poisson, 14, AssetIds.Waste, 28);
    });

    it('Should revert on unauthorised transfer', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, false);
      await expect(stakeable.onStaked(alice.address, AssetIds.GenesisDrone, getNonce(), rarityProvider.address, PIXCategory.Outliers)).to.revertedWith('ERC1155: caller is not owner nor approved');
    });

    it('Should revert on unstakeable token', async function () {
      await stakeable.setTokenInfo(AssetIds.FacilityLegendary, false, false, false, Class.Linear, facilityEv, AssetIds.Energy, facilityEv);
      await expect(stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, getNonce(), rarityProvider.address, PIXCategory.Legendary)).to.revertedWith('MCS1155_ADAPTER: Token not stakeable');
    });

    it('Should revert on wrong pix for facility', async function () {
      await expect(stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, getNonce(), rarityProvider.address, PIXCategory.Legendary)).to.revertedWith('MCS1155_ADAPTER: Facility must be staked on correct tier');
    });

    it('Should revert on non MC call', async function () {
      await expect(stakeable.connect(alice).onStaked(alice.address, AssetIds.FacilityOutlier, getNonce(), rarityProvider.address, PIXCategory.Outliers)).to.revertedWith('MCS_ADAPTER: Sender not mission control');
    });
    it('Should revert on reused nonce', async function () {
      let nonce = getNonce();
      await erc1155.mint(alice.address, AssetIds.FacilityOutlier, 1);

      await expect(stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce, rarityProvider.address, PIXCategory.Outliers)).to.not.reverted;
      await expect(stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce, rarityProvider.address, PIXCategory.Outliers)).to.revertedWith('MCS1155_ADAPTER: Nonce already used');
      await expect(stakeable.onStaked(alice.address, AssetIds.GenesisDrone, nonce, rarityProvider.address, PIXCategory.Outliers)).to.revertedWith('MCS1155_ADAPTER: Nonce already used');
    });
  });

  describe('onUnstaked', function () {
    let nonce;
    beforeEach(async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.GenesisDrone)).to.equal(0);
      nonce = getNonce();
      await stakeable.onStaked(alice.address, AssetIds.GenesisDrone, nonce, rarityProvider.address, PIXCategory.Outliers);
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.GenesisDrone)).to.equal(1);
    });
    it('Should allow unstaking a drone', async function () {
      await stakeable.onUnstaked(alice.address, nonce);
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.GenesisDrone)).to.equal(0);
    });
  });

  describe('onUnstaked reverts', function () {
    let nonce;
    beforeEach(async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.GenesisDrone)).to.equal(0);
      nonce = getNonce();
      await stakeable.onStaked(alice.address, AssetIds.GenesisDrone, nonce, rarityProvider.address, PIXCategory.Outliers);
      expect(await erc1155.balanceOf(stakeable.address, AssetIds.GenesisDrone)).to.equal(1);
    });
    it('Should revert on non MC call', async function () {
      await expect(stakeable.connect(alice).onUnstaked(alice.address, nonce)).to.revertedWith('MCS_ADAPTER: Sender not mission control');
    });
    it('Should revert on unused nonce', async function () {
      await expect(stakeable.onUnstaked(alice.address, getNonce())).to.revertedWith('MCS1155_ADAPTER: No such token staked');
    });
  });

  describe('isRaidable', function () {
    beforeEach(async function () {});
    it('returns expected when token is raidable', async function () {
      expect(await stakeable.isRaidable(AssetIds.Waste)).to.equal(true);
    });
    it('returns expected when token is not raidable', async function () {
      expect(await stakeable.isRaidable(AssetIds.GenesisDrone)).to.equal(false);
    });
  });

  describe('isBase', function () {
    beforeEach(async function () {});
    it('returns expected when token is base', async function () {
      expect(await stakeable.isBase(AssetIds.Waste)).to.equal(true);
    });
    it('returns expected when token is not base', async function () {
      expect(await stakeable.isBase(AssetIds.GenesisDrone)).to.equal(false);
    });
  });

  describe('checkTile', function () {
    let nonce1;
    let nonce2;
    beforeEach(async function () {
      nonce1 = getNonce();
      nonce2 = getNonce();
      //   await stakeable.onStaked(alice.address, AssetIds.GenesisDrone, nonce1, rarityProvider.address, PIXCategory.Outliers, ethers.constants.AddressZero, 0, 0, -1, 0);
      await stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce2, rarityProvider.address, PIXCategory.Outliers, ethers.constants.AddressZero, 0, 0, -1, 0);
    });
    it('returns expected resource type', async function () {
      expect((await stakeable.checkTile(alice.address, nonce1))[1]).to.equal(AssetIds.Waste);
      expect((await stakeable.checkTile(alice.address, nonce2))[1]).to.equal(AssetIds.Energy);
    });
    it('returns expected resource amount when deterministic', async function () {
      expect((await stakeable.checkTile(alice.address, nonce1))[0]).to.equal(0);
      let half_day = dayInSec / 2;
      await increaseTime(half_day + 1);
      expect((await stakeable.checkTile(alice.address, nonce1))[0]).to.equal(droneEv / 2);
      await increaseTime(half_day + 1);
      expect((await stakeable.checkTile(alice.address, nonce1))[0]).to.equal(droneEv);
    });
    it('returns expected resource amount when outlier facility on size pix', async function () {
      let period = dayInSec;
      let prod_amt = 1;
      expect((await stakeable.checkTile(alice.address, nonce2, 0, -1, 0))[0]).to.equal(0);
      let half_period = period / 2;
      await increaseTime(half_period + 1);
      expect((await stakeable.checkTile(alice.address, nonce2, 0, -1, 0))[0]).to.equal(0);
      await increaseTime(half_period + 1);
      expect((await stakeable.checkTile(alice.address, nonce2, 0, -1, 0))[0]).to.equal(prod_amt);
      await increaseTime(period + 1);
      expect((await stakeable.checkTile(alice.address, nonce2, 0, -1, 0))[0]).to.equal(2 * prod_amt);
      console.log((await stakeable.checkTile(alice.address, nonce2, 0, -1, 0))[0]);
    });
    it('returns expected resource amount when facility on size pix', async function () {
      let period = 60 * 60 * 8;
      let prod_amt = 6;
      await rarityProvider.setSize(99, PIXSize.Domain);
      await rarityProvider.setRarity(99, PIXCategory.Legendary);
      await erc1155.mint(alice.address, AssetIds.FacilityLegendary, 1);
      let nonce3 = getNonce();
      await stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, nonce3, rarityProvider.address, 99);
      expect((await stakeable.checkTile(alice.address, nonce3))[0]).to.equal(0);
      let half_period = period / 2;
      await increaseTime(half_period + 1);
      expect((await stakeable.checkTile(alice.address, nonce3))[0]).to.equal(0);
      await increaseTime(half_period + 1);
      expect((await stakeable.checkTile(alice.address, nonce3))[0]).to.equal(prod_amt);
      await increaseTime(period + 1);
      expect((await stakeable.checkTile(alice.address, nonce3))[0]).to.equal(2 * prod_amt);
    });
    it('increments by one a.e when Poisson', async function () {
      /*

            No poisson on release

            This is technically a flaky test, however the chance of a false negative is infinitesimal

            This test also takes a long time to run :)

            let num_seconds = dayInSec * 12 / droneEv;
            this.timeout(num_seconds) // this is just some schizo heuristics to avoid timeout
            expect((await stakeable.checkTile(await alice.getAddress(), nonce1))[0]).to.equal(0);
            let counter = 0
            let latest = 0
            let newVal = 0
            let dt = 10
            while (counter < num_seconds && latest < 2) {
                counter += dt
                await increaseTime(dt)
                newVal = (await stakeable.checkTile(await alice.getAddress(), nonce1))[0]
                expect(newVal - latest <= 1).to.equal(true);
                latest = newVal
            }
            expect((await stakeable.checkTile(await alice.getAddress(), nonce1))[0] > 0).to.true;       */
    });
  });

  describe.only('Testing different tiers lead times on different rings', () => {
    const offset = 0.1;

    let nonce1;
    let nonce2;
    let nonce3;
    let nonce4;
    let nonce5;
    let nonce6;
    let nonce7;
    let nonce8;
    let nonce9;
    let nonce10;
    let nonce11;
    let nonce12;
    let nonce13;
    let nonce14;
    let nonce15;
    let nonce16;
    let nonce17;
    let nonce18;
    let nonce19;
    let nonce20;

    beforeEach(async function () {
      nonce1 = getNonce();
      nonce2 = getNonce();
      nonce3 = getNonce();
      nonce4 = getNonce();
      nonce5 = getNonce();
      nonce6 = getNonce();
      nonce7 = getNonce();
      nonce8 = getNonce();
      nonce9 = getNonce();
      nonce10 = getNonce();
      nonce11 = getNonce();
      nonce12 = getNonce();
      nonce13 = getNonce();
      nonce14 = getNonce();
      nonce15 = getNonce();
      nonce16 = getNonce();
      nonce17 = getNonce();
      nonce18 = getNonce();
      nonce19 = getNonce();
      nonce20 = getNonce();

      await stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce1, rarityProvider.address, PIXCategory.Outliers, ethers.constants.AddressZero, 0, 0, apollo_tiles[0].x, apollo_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityCommon, nonce2, rarityProvider.address, PIXCategory.Common, ethers.constants.AddressZero, 0, 0, apollo_tiles[0].x, apollo_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityUncommon, nonce3, rarityProvider.address, PIXCategory.Uncommon, ethers.constants.AddressZero, 0, 0, apollo_tiles[0].x, apollo_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityRare, nonce4, rarityProvider.address, PIXCategory.Rare, ethers.constants.AddressZero, 0, 0, apollo_tiles[0].x, apollo_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, nonce5, rarityProvider.address, PIXCategory.Legendary, ethers.constants.AddressZero, 0, 0, apollo_tiles[0].x, apollo_tiles[0].y);

      await stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce6, rarityProvider.address, PIXCategory.Outliers, ethers.constants.AddressZero, 0, 0, artemis_tiles[0].x, artemis_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityCommon, nonce7, rarityProvider.address, PIXCategory.Common, ethers.constants.AddressZero, 0, 0, artemis_tiles[0].x, artemis_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityUncommon, nonce8, rarityProvider.address, PIXCategory.Uncommon, ethers.constants.AddressZero, 0, 0, artemis_tiles[0].x, artemis_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityRare, nonce9, rarityProvider.address, PIXCategory.Rare, ethers.constants.AddressZero, 0, 0, artemis_tiles[0].x, artemis_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, nonce10, rarityProvider.address, PIXCategory.Legendary, ethers.constants.AddressZero, 0, 0, artemis_tiles[0].x, artemis_tiles[0].y);

      await stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce11, rarityProvider.address, PIXCategory.Outliers, ethers.constants.AddressZero, 0, 0, chronos_tiles[0].x, chronos_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityCommon, nonce12, rarityProvider.address, PIXCategory.Common, ethers.constants.AddressZero, 0, 0, chronos_tiles[0].x, chronos_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityUncommon, nonce13, rarityProvider.address, PIXCategory.Uncommon, ethers.constants.AddressZero, 0, 0, chronos_tiles[0].x, chronos_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityRare, nonce14, rarityProvider.address, PIXCategory.Rare, ethers.constants.AddressZero, 0, 0, chronos_tiles[0].x, chronos_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, nonce15, rarityProvider.address, PIXCategory.Legendary, ethers.constants.AddressZero, 0, 0, chronos_tiles[0].x, chronos_tiles[0].y);

      await stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce16, rarityProvider.address, PIXCategory.Outliers, ethers.constants.AddressZero, 0, 0, helios_tiles[0].x, helios_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityCommon, nonce17, rarityProvider.address, PIXCategory.Common, ethers.constants.AddressZero, 0, 0, helios_tiles[0].x, helios_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityUncommon, nonce18, rarityProvider.address, PIXCategory.Uncommon, ethers.constants.AddressZero, 0, 0, helios_tiles[0].x, helios_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityRare, nonce19, rarityProvider.address, PIXCategory.Rare, ethers.constants.AddressZero, 0, 0, helios_tiles[0].x, helios_tiles[0].y);
      await stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, nonce20, rarityProvider.address, PIXCategory.Legendary, ethers.constants.AddressZero, 0, 0, helios_tiles[0].x, helios_tiles[0].y);
    });
    it('Outlier Facility on Apollo', async function () {
      await increaseTime(hoursToSeconds(24 - offset));
      expect((await stakeable.checkTile(alice.address, nonce1, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce1, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(1);
    });
    it('Outlier Facility on Artemis', async function () {
      await increaseTime(hoursToSeconds(21.6 - offset));
      expect((await stakeable.checkTile(alice.address, nonce6, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce6, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(1);
    });
    it('Outlier Facility on Chronos', async function () {
      await increaseTime(hoursToSeconds(21.6 - offset));
      expect((await stakeable.checkTile(alice.address, nonce11, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce11, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(1);
    });
    it('Outlier Facility on Helios', async function () {
      await increaseTime(hoursToSeconds(19.2 - offset));
      expect((await stakeable.checkTile(alice.address, nonce16, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce16, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(1);
    });
    it('Common Facility on Apollo', async function () {
      await increaseTime(hoursToSeconds(20 - offset));
      expect((await stakeable.checkTile(alice.address, nonce2, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce2, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(1);
    });
    it('Common Facility on Artemis', async function () {
      await increaseTime(hoursToSeconds(18 - offset));
      expect((await stakeable.checkTile(alice.address, nonce7, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce7, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(1);
    });
    it('Common Facility on Chronos', async function () {
      await increaseTime(hoursToSeconds(18 - offset));
      expect((await stakeable.checkTile(alice.address, nonce12, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce12, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(1);
    });
    it('Common Facility on Helios', async function () {
      await increaseTime(hoursToSeconds(16 - offset));
      expect((await stakeable.checkTile(alice.address, nonce17, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce17, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(1);
    });
    it('Uncommon Facility on Apollo', async function () {
      await increaseTime(hoursToSeconds(16 - offset));
      expect((await stakeable.checkTile(alice.address, nonce3, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce3, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(1);
    });
    it('Uncommon Facility on Artemis', async function () {
      await increaseTime(hoursToSeconds(14.4 - offset));
      expect((await stakeable.checkTile(alice.address, nonce8, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce8, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(1);
    });
    it('Uncommon Facility on Chronos', async function () {
      await increaseTime(hoursToSeconds(14.4 - offset));
      expect((await stakeable.checkTile(alice.address, nonce13, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce13, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(1);
    });
    it('Uncommon Facility on Helios', async function () {
      await increaseTime(hoursToSeconds(12.8 - offset));
      expect((await stakeable.checkTile(alice.address, nonce18, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce18, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(1);
    });
    it('Rare Facility on Apollo', async function () {
      await increaseTime(hoursToSeconds(12 - offset));
      expect((await stakeable.checkTile(alice.address, nonce4, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce4, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(1);
    });
    it('Rare Facility on Artemis', async function () {
      await increaseTime(hoursToSeconds(10.8 - offset));
      expect((await stakeable.checkTile(alice.address, nonce9, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce9, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(1);
    });
    it('Rare Facility on Chronos', async function () {
      await increaseTime(hoursToSeconds(10.8 - offset));
      expect((await stakeable.checkTile(alice.address, nonce14, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce14, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(1);
    });
    it('Rare Facility on Helios', async function () {
      await increaseTime(hoursToSeconds(9.6 - offset));
      expect((await stakeable.checkTile(alice.address, nonce19, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce19, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(1);
    });
    it('Legendary Facility on Apollo', async function () {
      await increaseTime(hoursToSeconds(8 - offset));
      expect((await stakeable.checkTile(alice.address, nonce5, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce5, 0, apollo_tiles[0].x, apollo_tiles[0].y))[0]).to.equal(1);
    });
    it('Legendary Facility on Artemis', async function () {
      await increaseTime(hoursToSeconds(7.2 - offset));
      expect((await stakeable.checkTile(alice.address, nonce10, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce10, 0, artemis_tiles[0].x, artemis_tiles[0].y))[0]).to.equal(1);
    });
    it('Legendary Facility on Chronos', async function () {
      await increaseTime(hoursToSeconds(7.2 - offset));
      expect((await stakeable.checkTile(alice.address, nonce15, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce15, 0, chronos_tiles[0].x, chronos_tiles[0].y))[0]).to.equal(1);
    });
    it('Legendary Facility on Helios', async function () {
      await increaseTime(hoursToSeconds(6.4 - offset));
      expect((await stakeable.checkTile(alice.address, nonce20, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(0);
      await increaseTime(hoursToSeconds(offset));
      expect((await stakeable.checkTile(alice.address, nonce20, 0, helios_tiles[0].x, helios_tiles[0].y))[0]).to.equal(1);
    });
  });

  describe('checkTile reverts', function () {
    let nonce1;
    let nonce2;
    beforeEach(async function () {
      nonce1 = getNonce();
      nonce2 = getNonce();
      await stakeable.onStaked(alice.address, AssetIds.GenesisDrone, nonce1, rarityProvider.address, PIXCategory.Outliers);
      await stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce2, rarityProvider.address, PIXCategory.Outliers);
    });

    it('reverts on invalid nonce, deterministic', async function () {
      await expect(stakeable.checkTile(alice.address, getNonce())).to.revertedWith('MCS1155_ADAPTER: No such token staked');
    });
    it('reverts on invalid nonce, Poisson', async function () {
      await expect(stakeable.checkTile(alice.address, getNonce())).to.revertedWith('MCS1155_ADAPTER: No such token staked');
    });
  });

  describe('onCollect', function () {
    let nonce1;
    let nonce2;
    beforeEach(async function () {
      nonce1 = getNonce();
      nonce2 = getNonce();
      await stakeable.onStaked(alice.address, AssetIds.GenesisDrone, nonce1, rarityProvider.address, PIXCategory.Outliers);
      await stakeable.onStaked(alice.address, AssetIds.FacilityOutlier, nonce2, rarityProvider.address, PIXCategory.Outliers);
    });
    it('returns expected resource type', async function () {
      expect((await stakeable.callStatic.onCollect(alice.address, nonce1))[1]).to.equal(AssetIds.Waste);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[1]).to.equal(AssetIds.Energy);
    });
    it('returns expected resource amount when deterministic (STATIC)', async function () {
      expect((await stakeable.callStatic.onCollect(alice.address, nonce1))[0]).to.equal(0);
      let half_day = dayInSec / 2;
      await increaseTime(half_day + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce1))[0]).to.equal(droneEv / 2);
      await increaseTime(half_day + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce1))[0]).to.equal(droneEv);
    });

    it('returns expected resource amount when deterministic ', async function () {
      expect((await stakeable.callStatic.onCollect(alice.address, nonce1))[0]).to.equal(0);
      let half_day = dayInSec / 2;
      await increaseTime(half_day + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce1))[0]).to.equal(droneEv / 2);
      //
      await stakeable.onCollect(alice.address, nonce1);
      await increaseTime(half_day + 120);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce1))[0]).to.equal(droneEv / 2);
    });

    it('collects expected resource amount when outlier facility on size pix (STATIC)', async function () {
      let period = dayInSec;
      let prod_amt = 1;
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(0);
      let half_period = period / 2;
      await increaseTime(half_period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(0);
      await increaseTime(half_period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(prod_amt);
      await increaseTime(period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(2 * prod_amt);
    });

    it('collects expected resource amount when outlier facility on size pix ', async function () {
      let period = dayInSec;
      let prod_amt = 1;
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(0);
      await increaseTime(period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(prod_amt);
      await stakeable.onCollect(alice.address, nonce2);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(0);
      await increaseTime(2 * period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(2 * prod_amt);
      await stakeable.onCollect(alice.address, nonce2);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce2))[0]).to.equal(0);
    });

    it('collects expected resource amount when facility on size pix (STATIC)', async function () {
      let period = 60 * 60 * 8;
      let prod_amt = 6;
      await rarityProvider.setSize(99, PIXSize.Domain);
      await rarityProvider.setRarity(99, PIXCategory.Legendary);
      await erc1155.mint(alice.address, AssetIds.FacilityLegendary, 1);
      let nonce3 = getNonce();
      await stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, nonce3, rarityProvider.address, 99);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(0);
      let half_period = period / 2;
      await increaseTime(half_period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(0);
      await increaseTime(half_period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(prod_amt);
      await increaseTime(period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(2 * prod_amt);
    });

    it('collects expected resource amount when facility on size pix', async function () {
      let period = 60 * 60 * 8;
      let prod_amt = 6;
      await rarityProvider.setSize(99, PIXSize.Domain);
      await rarityProvider.setRarity(99, PIXCategory.Legendary);
      await erc1155.mint(alice.address, AssetIds.FacilityLegendary, 1);
      let nonce3 = getNonce();
      await stakeable.onStaked(alice.address, AssetIds.FacilityLegendary, nonce3, rarityProvider.address, 99);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(0);
      await increaseTime(period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(prod_amt);
      await stakeable.onCollect(alice.address, nonce3);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(0);
      await increaseTime(2 * period + 1);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(2 * prod_amt);
      await stakeable.onCollect(alice.address, nonce3);
      expect((await stakeable.callStatic.onCollect(alice.address, nonce3))[0]).to.equal(0);
    });
  });

  describe('check time left', async function () {
    it('Should be 24h time left until full, after staking drone.', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let nonce = getNonce();
      let tokenId = AssetIds.GenesisDrone;
      await stakeable.onStaked(await alice.getAddress(), tokenId, nonce, rarityProvider.address, 0);
      let timeLeft = await stakeable.timeLeft(await alice.getAddress(), nonce, 1);
      expect(timeLeft.toNumber()).to.approximately(dayInSec, 10);
    });
    it('Should be 3/4day in sec time left until full, after staking drone.', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let nonce = getNonce();
      let tokenId = AssetIds.GenesisDrone;
      await stakeable.onStaked(await alice.getAddress(), tokenId, nonce, rarityProvider.address, 0);
      await increaseTime(dayInSec / 4);
      let timeLeft = await stakeable.timeLeft(await alice.getAddress(), nonce, 1);
      expect(timeLeft.toNumber()).to.approximately(dayInSec * 0.75, 10); // +-10 sec
    });
    it('Should return 0 time left until full, after staking drone.', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let nonce = getNonce();
      let tokenId = AssetIds.GenesisDrone;
      await stakeable.onStaked(await alice.getAddress(), tokenId, nonce, rarityProvider.address, 0);
      await increaseTime(dayInSec * 2);
      let timeLeft = await stakeable.timeLeft(await alice.getAddress(), nonce, 1);
      console.log(timeLeft);
      expect(timeLeft).to.equal(0);
    });
    it('Should return 1/2day in sec time left until full, after staking drone and collecting tile.', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let nonce = getNonce();
      let tokenId = AssetIds.GenesisDrone;
      await stakeable.onStaked(await alice.getAddress(), tokenId, nonce, rarityProvider.address, 0);
      await increaseTime(dayInSec * 0.5);
      await stakeable.onCollect(await alice.getAddress(), 1);
      await increaseTime(dayInSec * 0.5);
      let timeLeft = await stakeable.timeLeft(await alice.getAddress(), nonce, 1);
      expect(timeLeft.toNumber()).to.approximately(dayInSec * 0.5, 10);
    });
    it('Should be 4days in sec time left until full, after staking facility.', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let nonce = getNonce();
      let tokenId = AssetIds.FacilityLegendary;
      await stakeable.onStaked(await alice.getAddress(), tokenId, nonce, rarityProvider.address, 0);
      let timeLeft = await stakeable.timeLeft(await alice.getAddress(), nonce, 1);
      expect(timeLeft).to.equal(dayInSec * 4);
    });
    it('Should return 0 time left until full, after staking facility.', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let nonce = getNonce();
      let tokenId = AssetIds.FacilityLegendary;
      await stakeable.onStaked(await alice.getAddress(), tokenId, nonce, rarityProvider.address, 0);
      await increaseTime(dayInSec * 5);
      let timeLeft = await stakeable.timeLeft(await alice.getAddress(), nonce, 1);
      expect(timeLeft).to.equal(0);
    });
  });

  describe('check timestamp', function () {
    it('should return expected time', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let receipt = await stakeable.onStaked(await alice.getAddress(), AssetIds.GenesisDrone, 1, rarityProvider.address, 0);
      let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      await increaseTime(1000);
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
    });
    it('should return expected time after collecting', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let receipt = await stakeable.onStaked(await alice.getAddress(), AssetIds.GenesisDrone, 1, rarityProvider.address, 0);
      let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      await increaseTime(1000);
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
      receipt = await stakeable.onCollect(await alice.getAddress(), 1);
      timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
    });
    it('should return expected time after resetting', async function () {
      await erc1155.connect(alice).setApprovalForAll(stakeable.address, true);
      let receipt = await stakeable.onStaked(await alice.getAddress(), AssetIds.GenesisDrone, 1, rarityProvider.address, 0);
      let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      await increaseTime(1000);
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
      receipt = await stakeable.reset(await alice.getAddress(), 1);
      timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
    });
  });

  describe('Administrative', function () {
    it('Should allow admin to set token info', async function () {
      await expect(stakeable.setTokenInfo(1, true, false, true, 1, 2, 3, 4)).to.not.reverted;
    });
    it('Should not allow Alice to set token info', async function () {
      await expect(stakeable.connect(alice).setTokenInfo(1, true, false, true, 1, 2, 3, 4)).to.revertedWith('Ownable: caller is not the owner');
    });
    it('Should allow admin to set facility stake requirements', async function () {
      await expect(stakeable.setFacilityStakeRequirements(1, 2, true)).to.not.reverted;
    });
    it('Should not allow Alice to set facility stake requirements', async function () {
      await expect(stakeable.connect(alice).setFacilityStakeRequirements(1, 2, true)).to.revertedWith('Ownable: caller is not the owner');
    });
    it('Should allow admin to set rarity provider', async function () {
      await expect(stakeable.setRarityProvider(alice.address, alice.address)).to.not.reverted;
    });
    it('Should not allow Alice to set rarity provider', async function () {
      await expect(stakeable.connect(alice).setRarityProvider(alice.address, alice.address)).to.revertedWith('Ownable: caller is not the owner');
    });
    it('Should allow admin to set mission control', async function () {
      await expect(stakeable.setMissionControl(alice.address)).to.not.reverted;
    });
    it('Should allow Alice to set mission control', async function () {
      await expect(stakeable.connect(alice).setMissionControl(alice.address)).to.revertedWith('Ownable: caller is not the owner');
    });
    it('Should allow admin to set erc1155', async function () {
      // Implicity handled by init i aint refactoring this rn
    });
    it('Should not allow Alice to set erc1155', async function () {
      await expect(stakeable.connect(alice).setERC1155(alice.address)).to.revertedWith('Ownable: caller is not the owner');
    });
    it('Should not allow admin to set erc1155 twice', async function () {
      await expect(stakeable.setERC1155(alice.address)).to.revertedWith('MC1155_ADAPTER: Erc1155 has already been set');
    });
  });

  describe('Events', function () {
    it('Should emit TokenClaimed on staking', async function () {
      expect(await stakeable.onStaked(alice.address, AssetIds.GenesisDrone, getNonce(), rarityProvider.address, PIXCategory.Outliers))
        .to.emit(stakeable, 'TokenClaimed')
        .withArgs(alice.address, stakeable.address, AssetIds.GenesisDrone);
    });
    it('Should emit TokenReturned on unstaking', async function () {
      let localNonce = getNonce();
      await stakeable.onStaked(alice.address, AssetIds.GenesisDrone, localNonce, rarityProvider.address, PIXCategory.Outliers);
      expect(await stakeable.onUnstaked(alice.address, localNonce))
        .to.emit(stakeable, 'TokenReturned')
        .withArgs(alice.address, stakeable.address, AssetIds.GenesisDrone);
    });
    it('Should emit TokenInfoUpdated when updating token info', async function () {
      expect(await stakeable.setTokenInfo(1, true, false, true, 1, 2, 3, 4))
        .to.emit(stakeable, 'TokenInfoUpdated')
        .withArgs(1, true, false, true, 1, 2, 3, 4);
    });
    it('Should emit FacilityStakeRequirementsUpdated when updating facilitu requirements', async function () {
      expect(await stakeable.setFacilityStakeRequirements(1, 2, true))
        .to.emit(stakeable, 'FacilityStakeRequirementsUpdated')
        .withArgs(1, 2, true);
    });
    it('Should emit RarityProviderUpdated when updating rarity provider', async function () {
      expect(await stakeable.setRarityProvider(alice.address, alice.address))
        .to.emit(stakeable, 'RarityProviderUpdated')
        .withArgs(alice.address, alice.address);
    });
    it('Should emit MissionControlSet when updating mission control', async function () {
      expect(await stakeable.setMissionControl(alice.address))
        .to.emit(stakeable, 'MissionControlSet')
        .withArgs(alice.address);
    });
  });
});

async function increaseTime(amount) {
  await hre.network.provider.send('evm_increaseTime', [amount]);
  await hre.network.provider.send('evm_mine');
  //console.log("EVM time " + amount + " seconds increased!")
}

function hoursToSeconds(hours: number): number {
  const secondsInHour = 60 * 60;
  return Math.round(hours * secondsInHour);
}

async function setupResources(stakeable: Contract) {
  const apolloTx = await stakeable.setTokenInfoBulk([
    {
      tokenId: SOLAR_GRID2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: SOLAR_GRID2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: SOLAR_GRID2.resource_cap,
      ring: 0,
    },
    {
      tokenId: GEOWELL2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: GEOWELL2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: GEOWELL2.resource_cap,
      ring: 0,
    },
    {
      tokenId: TIDAL_TURBINES2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: TIDAL_TURBINES2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: TIDAL_TURBINES2.resource_cap,
      ring: 0,
    },
    {
      tokenId: RADIANT_GRADENS2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RADIANT_GRADENS2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RADIANT_GRADENS2.resource_cap,
      ring: 0,
    },
    {
      tokenId: FUSION_CORE2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: FUSION_CORE2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: FUSION_CORE2.resource_cap,
      ring: 0,
    },
    {
      tokenId: SOLAR_GRID3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: SOLAR_GRID3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: SOLAR_GRID3.resource_cap,
      ring: 0,
    },
    {
      tokenId: GEOWELL3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: GEOWELL3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: GEOWELL3.resource_cap,
      ring: 0,
    },
    {
      tokenId: TIDAL_TURBINES3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: TIDAL_TURBINES3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: TIDAL_TURBINES3.resource_cap,
      ring: 0,
    },
    {
      tokenId: RADIANT_GRADENS3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RADIANT_GRADENS3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RADIANT_GRADENS3.resource_cap,
      ring: 0,
    },
    {
      tokenId: FUSION_CORE3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: FUSION_CORE3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: FUSION_CORE3.resource_cap,
      ring: 0,
    },
    {
      tokenId: OUTLIER_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: OUTLIER_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: OUTLIER_FACILITY.resource_cap,
      ring: 0,
    },
    {
      tokenId: COMMON_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: COMMON_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: COMMON_FACILITY.resource_cap,
      ring: 0,
    },
    {
      tokenId: UNCOMMON_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: UNCOMMON_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: UNCOMMON_FACILITY.resource_cap,
      ring: 0,
    },
    {
      tokenId: RARE_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RARE_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RARE_FACILITY.resource_cap,
      ring: 0,
    },
    {
      tokenId: LEGENDARY_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: LEGENDARY_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: LEGENDARY_FACILITY.resource_cap,
      ring: 0,
    },
    {
      tokenId: PIERCER_DRONE_APOLLO.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: PIERCER_DRONE_APOLLO.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: PIERCER_DRONE_APOLLO.resource_cap,
      ring: 0,
    },
    {
      tokenId: GENESIS_DRONE_APOLLO.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: GENESIS_DRONE_APOLLO.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: GENESIS_DRONE_APOLLO.resource_cap,
      ring: 0,
    },
    {
      tokenId: NIGHT_DRONE_APOLLO.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: NIGHT_DRONE_APOLLO.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: NIGHT_DRONE_APOLLO.resource_cap,
      ring: 0,
    },
  ]);

  const artemisTx = await stakeable.setTokenInfoBulk([
    {
      tokenId: SOLAR_GRID2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: SOLAR_GRID2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: SOLAR_GRID2.resource_cap,
      ring: 1,
    },
    {
      tokenId: GEOWELL2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: GEOWELL2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: GEOWELL2.resource_cap,
      ring: 1,
    },
    {
      tokenId: TIDAL_TURBINES2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: TIDAL_TURBINES2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: TIDAL_TURBINES2.resource_cap,
      ring: 1,
    },
    {
      tokenId: RADIANT_GRADENS2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RADIANT_GRADENS2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RADIANT_GRADENS2.resource_cap,
      ring: 1,
    },
    {
      tokenId: FUSION_CORE2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: FUSION_CORE2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: FUSION_CORE2.resource_cap,
      ring: 1,
    },
    {
      tokenId: SOLAR_GRID3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: SOLAR_GRID3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: SOLAR_GRID3.resource_cap,
      ring: 1,
    },
    {
      tokenId: GEOWELL3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: GEOWELL3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: GEOWELL3.resource_cap,
      ring: 1,
    },
    {
      tokenId: TIDAL_TURBINES3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: TIDAL_TURBINES3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: TIDAL_TURBINES3.resource_cap,
      ring: 1,
    },
    {
      tokenId: RADIANT_GRADENS3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RADIANT_GRADENS3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RADIANT_GRADENS3.resource_cap,
      ring: 1,
    },
    {
      tokenId: FUSION_CORE3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: FUSION_CORE3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: FUSION_CORE3.resource_cap,
      ring: 1,
    },
    {
      tokenId: OUTLIER_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: OUTLIER_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: OUTLIER_FACILITY.resource_cap,
      ring: 1,
    },
    {
      tokenId: COMMON_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: COMMON_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: COMMON_FACILITY.resource_cap,
      ring: 1,
    },
    {
      tokenId: UNCOMMON_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: UNCOMMON_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: UNCOMMON_FACILITY.resource_cap,
      ring: 1,
    },
    {
      tokenId: RARE_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RARE_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RARE_FACILITY.resource_cap,
      ring: 1,
    },
    {
      tokenId: LEGENDARY_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: LEGENDARY_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: LEGENDARY_FACILITY.resource_cap,
      ring: 1,
    },
    {
      tokenId: PIERCER_DRONE_ARTEMIS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: PIERCER_DRONE_ARTEMIS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: PIERCER_DRONE_ARTEMIS.resource_cap,
      ring: 1,
    },
    {
      tokenId: GENESIS_DRONE_ARTEMIS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: GENESIS_DRONE_ARTEMIS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: GENESIS_DRONE_ARTEMIS.resource_cap,
      ring: 1,
    },
    {
      tokenId: NIGHT_DRONE_ARTEMIS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: NIGHT_DRONE_ARTEMIS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: NIGHT_DRONE_ARTEMIS.resource_cap,
      ring: 1,
    },
  ]);

  const chronosTx = await stakeable.setTokenInfoBulk([
    {
      tokenId: SOLAR_GRID2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: SOLAR_GRID2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: SOLAR_GRID2.resource_cap,
      ring: 2,
    },
    {
      tokenId: GEOWELL2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: GEOWELL2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: GEOWELL2.resource_cap,
      ring: 2,
    },
    {
      tokenId: TIDAL_TURBINES2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: TIDAL_TURBINES2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: TIDAL_TURBINES2.resource_cap,
      ring: 2,
    },
    {
      tokenId: RADIANT_GRADENS2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RADIANT_GRADENS2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RADIANT_GRADENS2.resource_cap,
      ring: 2,
    },
    {
      tokenId: FUSION_CORE2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: FUSION_CORE2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: FUSION_CORE2.resource_cap,
      ring: 2,
    },
    {
      tokenId: SOLAR_GRID3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: SOLAR_GRID3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: SOLAR_GRID3.resource_cap,
      ring: 2,
    },
    {
      tokenId: GEOWELL3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: GEOWELL3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: GEOWELL3.resource_cap,
      ring: 2,
    },
    {
      tokenId: TIDAL_TURBINES3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: TIDAL_TURBINES3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: TIDAL_TURBINES3.resource_cap,
      ring: 2,
    },
    {
      tokenId: RADIANT_GRADENS3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RADIANT_GRADENS3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RADIANT_GRADENS3.resource_cap,
      ring: 2,
    },
    {
      tokenId: FUSION_CORE3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: FUSION_CORE3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: FUSION_CORE3.resource_cap,
      ring: 2,
    },
    {
      tokenId: OUTLIER_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: OUTLIER_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: OUTLIER_FACILITY.resource_cap,
      ring: 2,
    },
    {
      tokenId: COMMON_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: COMMON_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: COMMON_FACILITY.resource_cap,
      ring: 2,
    },
    {
      tokenId: UNCOMMON_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: UNCOMMON_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: UNCOMMON_FACILITY.resource_cap,
      ring: 2,
    },
    {
      tokenId: RARE_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RARE_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RARE_FACILITY.resource_cap,
      ring: 2,
    },
    {
      tokenId: LEGENDARY_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: LEGENDARY_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: LEGENDARY_FACILITY.resource_cap,
      ring: 2,
    },
    {
      tokenId: PIERCER_DRONE_CHRONOS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: PIERCER_DRONE_CHRONOS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: PIERCER_DRONE_CHRONOS.resource_cap,
      ring: 2,
    },
    {
      tokenId: GENESIS_DRONE_CHRONOS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: GENESIS_DRONE_CHRONOS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: GENESIS_DRONE_CHRONOS.resource_cap,
      ring: 2,
    },
    {
      tokenId: NIGHT_DRONE_CHRONOS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: NIGHT_DRONE_CHRONOS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: NIGHT_DRONE_CHRONOS.resource_cap,
      ring: 2,
    },
  ]);

  const heliosTx = await stakeable.setTokenInfoBulk([
    {
      tokenId: SOLAR_GRID2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: SOLAR_GRID2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: SOLAR_GRID2.resource_cap,
      ring: 3,
    },
    {
      tokenId: GEOWELL2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: GEOWELL2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: GEOWELL2.resource_cap,
      ring: 3,
    },
    {
      tokenId: TIDAL_TURBINES2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: TIDAL_TURBINES2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: TIDAL_TURBINES2.resource_cap,
      ring: 3,
    },
    {
      tokenId: RADIANT_GRADENS2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RADIANT_GRADENS2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RADIANT_GRADENS2.resource_cap,
      ring: 3,
    },
    {
      tokenId: FUSION_CORE2.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: FUSION_CORE2.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: FUSION_CORE2.resource_cap,
      ring: 3,
    },
    {
      tokenId: SOLAR_GRID3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: SOLAR_GRID3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: SOLAR_GRID3.resource_cap,
      ring: 3,
    },
    {
      tokenId: GEOWELL3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: GEOWELL3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: GEOWELL3.resource_cap,
      ring: 3,
    },
    {
      tokenId: TIDAL_TURBINES3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: TIDAL_TURBINES3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: TIDAL_TURBINES3.resource_cap,
      ring: 3,
    },
    {
      tokenId: RADIANT_GRADENS3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RADIANT_GRADENS3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RADIANT_GRADENS3.resource_cap,
      ring: 3,
    },
    {
      tokenId: FUSION_CORE3.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: FUSION_CORE3.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: FUSION_CORE3.resource_cap,
      ring: 3,
    },
    {
      tokenId: OUTLIER_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: OUTLIER_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: OUTLIER_FACILITY.resource_cap,
      ring: 3,
    },
    {
      tokenId: COMMON_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: COMMON_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: COMMON_FACILITY.resource_cap,
      ring: 3,
    },
    {
      tokenId: UNCOMMON_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: UNCOMMON_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: UNCOMMON_FACILITY.resource_cap,
      ring: 3,
    },
    {
      tokenId: RARE_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: RARE_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: RARE_FACILITY.resource_cap,
      ring: 3,
    },
    {
      tokenId: LEGENDARY_FACILITY.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.Facility,
      resourceEv: LEGENDARY_FACILITY.resource_ev,
      resourceId: AssetIds.Energy,
      resourceCap: LEGENDARY_FACILITY.resource_cap,
      ring: 3,
    },
    {
      tokenId: PIERCER_DRONE_HELIOS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: PIERCER_DRONE_HELIOS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: PIERCER_DRONE_HELIOS.resource_cap,
      ring: 3,
    },
    {
      tokenId: GENESIS_DRONE_HELIOS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: GENESIS_DRONE_HELIOS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: GENESIS_DRONE_HELIOS.resource_cap,
      ring: 3,
    },
    {
      tokenId: NIGHT_DRONE_HELIOS.token_id,
      isStakeable: true,
      isBase: false,
      isRaidable: true,
      class: Distribution.LinearDeterministic,
      resourceEv: NIGHT_DRONE_HELIOS.resource_ev,
      resourceId: AssetIds.Waste,
      resourceCap: NIGHT_DRONE_HELIOS.resource_cap,
      ring: 3,
    },
  ]);
}
