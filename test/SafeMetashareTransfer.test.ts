import { expect } from 'chai';
import hre, { ethers } from 'hardhat';
import { Contract } from 'ethers';

import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

describe('Safe Metashare Transfer tests', function () {
  let assets: Contract;
  let metashareTransferContract: Contract;

  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let maxSupply = 100

  beforeEach(async function () {
    [owner, alice] = await ethers.getSigners();

    let assetFactory = await ethers.getContractFactory('ERC1155MockAssetManager');

    assets = await assetFactory.deploy('test uri');

    let metashareTransferFactory = await ethers.getContractFactory('SafeMetashareTransfer');

    metashareTransferContract = await hre.upgrades.deployProxy(
      metashareTransferFactory,
      [assets.address]
    )
    await metashareTransferContract.setMaxSupply(maxSupply)


    await assets.setTrusted(metashareTransferContract.address, true);
  });

  describe("Setter functions should work", function () {
    it('Should set the tokenId', async function () {
      expect((await metashareTransferContract.tokenId()).toNumber()).to.equal(0);
      await metashareTransferContract.setTokenId(12);
      expect((await metashareTransferContract.tokenId()).toNumber()).to.equal(12);
    })

    it('Should set an admin', async function () {
      const newUser = alice.address;
      const isAdmin = true;

      expect(await metashareTransferContract.admins(newUser)).to.equal(false);
      await metashareTransferContract.setAdmin(newUser, isAdmin);
      expect(await metashareTransferContract.admins(newUser)).to.equal(isAdmin);
    });

  });


  describe("Minting should work", function () {

    it('Should mint with a one-time key', async function () {
      const oneTimeKey = 'unique_key';
      const recipient = alice.address;
      const amount = 10;

      // Ensure the key hasn't been used before
      expect(await metashareTransferContract.usedKeys(oneTimeKey)).to.equal(false);


      await metashareTransferContract.setTokenId(12);
      // Mint with an unused key
      await metashareTransferContract.mintWithOneTimeKey(oneTimeKey, recipient, amount);

      // Ensure the key is marked as used
      expect(await metashareTransferContract.usedKeys(oneTimeKey)).to.equal(true);

      // Check that "Alice" received her desired tokens
      const balance = await assets.balanceOf(alice.address, 12)
      expect(balance.toNumber()).to.equal(amount)

    });

    it('Should work if supply is increased', async function () {

      const tempMaxSupply = 2
      const mintAmount = 1

      await metashareTransferContract.setMaxSupply(tempMaxSupply)

      await metashareTransferContract.mintWithOneTimeKey("oneTimeKey1", alice.address, mintAmount)
      await metashareTransferContract.mintWithOneTimeKey("oneTimeKey2", alice.address, mintAmount)
      await expect(metashareTransferContract.mintWithOneTimeKey("oneTimeKey3", alice.address, mintAmount)).to.revertedWith('Supply limit exceeded')

      await metashareTransferContract.setMaxSupply(tempMaxSupply + 1)

      expect(await metashareTransferContract.mintWithOneTimeKey("oneTimeKey3", alice.address, mintAmount)).to.emit(metashareTransferContract, 'keyUsed').withArgs("oneTimeKey3".toString());

    });
  })

  describe("Minting should revert", function () {

    it('Should revert if key has been used', async function () {
      const oneTimeKey = 'unique_key';
      const amount = 10;

      // Ensure the key hasn't been used before
      expect(await metashareTransferContract.usedKeys(oneTimeKey)).to.equal(false);

      // Mint with an unused key
      await metashareTransferContract.mintWithOneTimeKey(oneTimeKey, alice.address, amount);

      // Ensure the key is marked as used
      expect(await metashareTransferContract.usedKeys(oneTimeKey)).to.equal(true);

      // Revert when trying to mint with same key again.
      await expect(metashareTransferContract.mintWithOneTimeKey(oneTimeKey, alice.address, amount)).to.revertedWith('This key has already been used.')

    });

    it('Should revert if over max supply', async function () {
      const recipient = alice.address;

      const tempMaxSupply = 10
      const tempKey = 11
      const mintAmount = 1

      await metashareTransferContract.setMaxSupply(tempMaxSupply)

      for (let i = 0; i < tempMaxSupply; i++) {
        const oneTimeKey = i.toString();

        expect(await metashareTransferContract.mintWithOneTimeKey(oneTimeKey, recipient, mintAmount)).to.emit(metashareTransferContract, 'keyUsed').withArgs(i.toString());
      }

      await expect(metashareTransferContract.mintWithOneTimeKey(tempKey, recipient, mintAmount)).to.revertedWith('Supply limit exceeded')

    });
  });
});

