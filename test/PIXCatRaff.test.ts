import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { expect } from 'chai';
import { ethers } from 'hardhat';
import { Contract, BigNumber, constants, utils } from 'ethers';
import { increaseTime, advanceBlock } from './utils';

describe('PIXCatRaff', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;
  let carol: SignerWithAddress;
  let pixToken: Contract;
  let vrfCoordinatorV2Mock: Contract;
  let pixCatRaff: Contract;
  let pixCatRaffKeeper: Contract;

  const ticketPrice = utils.parseEther('10');
  const weeklyPrize = utils.parseEther('1200');
  const keyHash = utils.defaultAbiCoder.encode(['uint256'], [1]);
  const subScriptionId = 1;
  const secondsPerWeek = 7 * 24 * 3600;

  beforeEach(async function () {
    [owner, alice, bob, carol] = await ethers.getSigners();

    const PIXTFactory = await ethers.getContractFactory('PIXT');
    pixToken = await PIXTFactory.deploy();
    await pixToken.transfer(alice.address, utils.parseEther('20000'));
    await pixToken.transfer(bob.address, utils.parseEther('20000'));
    await pixToken.transfer(owner.address, utils.parseEther('20000'));

    const VRFCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
    vrfCoordinatorV2Mock = await VRFCoordinatorV2MockFactory.deploy(0, 6);
    await vrfCoordinatorV2Mock.createSubscription();
    await vrfCoordinatorV2Mock.fundSubscription(subScriptionId, utils.parseEther('10'));

    const PIXCatRaffFactory = await ethers.getContractFactory('PIXCatRaff');
    pixCatRaff = await PIXCatRaffFactory.deploy(
      pixToken.address,
      owner.address,
      ticketPrice,
      weeklyPrize,
      vrfCoordinatorV2Mock.address,
      keyHash,
      subScriptionId,
    );
    await pixCatRaff.setCallbackGasLimit(2000000);

    const PIXCatRaffKeeperFactory = await ethers.getContractFactory('PIXCatRaffKeeper');
    pixCatRaffKeeper = await PIXCatRaffKeeperFactory.deploy(pixCatRaff.address);

    await pixCatRaff.setKeeper(pixCatRaffKeeper.address);
    await pixToken.approve(pixCatRaff.address, ethers.constants.MaxUint256);
  });

  describe('#initialize', () => {
    it('check initial values', async function () {
      expect(await pixCatRaff.SECONDS_PER_WEEK()).equal(secondsPerWeek);
      expect(await pixCatRaff.pixToken()).equal(pixToken.address);
      expect(await pixCatRaff.treasury()).equal(owner.address);
      expect(await pixCatRaff.keeper()).equal(pixCatRaffKeeper.address);
      expect(await pixCatRaff.ticketPrice()).equal(ticketPrice);
      expect(await pixCatRaff.weeklyPrize()).equal(weeklyPrize);
      expect(await pixCatRaff.getCurrentWeek()).equal(0);
    });
  });

  describe('#addTickets', () => {
    it('zero tickets', async function () {
      await expect(pixCatRaff.connect(alice).addTickets(0)).to.be.revertedWith(
        'tickets must be > 0',
      );
    });
    it('exceed allowance', async function () {
      await expect(pixCatRaff.connect(alice).addTickets(1)).to.be.revertedWith(
        'ERC20: transfer amount exceeds allowance',
      );
    });
    it('exceed balance', async function () {
      await pixToken.connect(alice).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await expect(pixCatRaff.connect(alice).addTickets(20000)).to.be.revertedWith(
        'ERC20: transfer amount exceeds balance',
      );
    });
    it('add 10 tickets', async function () {
      await pixToken.connect(alice).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(alice).addTickets(10);

      expect(await pixToken.balanceOf(pixCatRaff.address)).equal(utils.parseEther('100'));
      expect(await pixToken.balanceOf(alice.address)).equal(utils.parseEther('19900'));
    });
  });

  describe('#draw', () => {
    let performData = utils.defaultAbiCoder.encode(['uint256'], [0]);
    beforeEach(async function () {
      await pixToken.connect(alice).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(alice).addTickets(10);

      await pixToken.connect(bob).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(bob).addTickets(10);
    });
    it('only keeper', async function () {
      await expect(pixCatRaff.connect(alice).draw(0)).to.be.revertedWith('invalid keeper');
    });
    it('not drawable', async function () {
      await expect(pixCatRaffKeeper.performUpkeep(performData)).to.be.revertedWith('invalid week');
    });
    it('draw', async function () {
      await increaseTime(BigNumber.from(secondsPerWeek));
      await pixCatRaffKeeper.performUpkeep(performData);

      const requestId = 1;
      expect(await pixCatRaff.drawRequests(requestId)).equal(0);

      const tx = await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaff.address, {
        gasLimit: 2000000,
      });
      expect(tx).to.emit(pixCatRaff, 'ResultDrawn').withArgs(0);

      const prize = await pixCatRaff.prizes(alice.address);
      const winnings = await pixCatRaff.winnings(0);
    });
  });

  describe('#addOldTickets', () => {
    let performData = utils.defaultAbiCoder.encode(['uint256'], [0]);
    beforeEach(async function () {
      await pixToken.connect(alice).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(alice).addTickets(10);

      await pixToken.connect(bob).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(bob).addTickets(10);
    });
    it('not drawn past week', async function () {
      await expect(pixCatRaff.connect(alice).addOldTickets(0, 10, false)).to.be.revertedWith(
        'not drawn',
      );
    });
    it('invalid tickets', async function () {
      await increaseTime(BigNumber.from(secondsPerWeek));
      await pixCatRaffKeeper.performUpkeep(performData);

      const requestId = 1;
      expect(await pixCatRaff.drawRequests(requestId)).equal(0);

      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaff.address, {
        gasLimit: 2000000,
      });

      await expect(pixCatRaff.connect(alice).addOldTickets(0, 20, false)).to.be.revertedWith(
        'invalid tickets',
      );
    });
    it('add 3 old tickets', async function () {
      await increaseTime(BigNumber.from(secondsPerWeek));
      await pixCatRaffKeeper.performUpkeep(performData);

      const requestId = 1;
      expect(await pixCatRaff.drawRequests(requestId)).equal(0);

      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaff.address, {
        gasLimit: 2000000,
      });

      const tx = await pixCatRaff.connect(alice).addOldTickets(0, 3, false);
      expect(tx).emit(pixCatRaff, 'TicketsWithdrawn').withArgs([0, alice.address, 3]);
      expect(tx).emit(pixCatRaff, 'TicketsAdded').withArgs([1, alice.address, 3]);
    });

    it('add all old tickets', async function () {
      await increaseTime(BigNumber.from(secondsPerWeek));
      await pixCatRaffKeeper.performUpkeep(performData);

      const requestId = 1;
      expect(await pixCatRaff.drawRequests(requestId)).equal(0);

      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaff.address, {
        gasLimit: 2000000,
      });

      const tx = await pixCatRaff.connect(alice).addOldTickets(0, 3, true);
      expect(tx).emit(pixCatRaff, 'TicketsWithdrawn').withArgs([0, alice.address, 10]);
      expect(tx).emit(pixCatRaff, 'TicketsAdded').withArgs([1, alice.address, 10]);
    });
  });

  describe('#withdrawOldTickets', () => {
    let performData = utils.defaultAbiCoder.encode(['uint256'], [0]);
    beforeEach(async function () {
      await pixToken.connect(alice).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(alice).addTickets(10);

      await pixToken.connect(bob).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(bob).addTickets(10);
    });
    it('not drawn past week', async function () {
      await expect(pixCatRaff.connect(alice).withdrawOldTickets(0, 10, false)).to.be.revertedWith(
        'not drawn',
      );
    });
    it('locked', async function () {
      await increaseTime(BigNumber.from(secondsPerWeek));
      await pixCatRaffKeeper.performUpkeep(performData);

      const requestId = 1;
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaff.address, {
        gasLimit: 2000000,
      });

      await expect(pixCatRaff.connect(alice).withdrawOldTickets(0, 20, false)).to.be.revertedWith(
        'locked',
      );
    });
    it('withdraw 3 old tickets', async function () {
      await increaseTime(BigNumber.from(secondsPerWeek));
      await pixCatRaffKeeper.performUpkeep(performData);

      const requestId = 1;
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaff.address, {
        gasLimit: 2000000,
      });

      await increaseTime(BigNumber.from(secondsPerWeek));

      const tx = await pixCatRaff.connect(alice).withdrawOldTickets(0, 3, false);
      expect(tx).emit(pixCatRaff, 'TicketsWithdrawn').withArgs([0, alice.address, 3]);
    });

    it('withdraw all old tickets', async function () {
      await increaseTime(BigNumber.from(secondsPerWeek));
      await pixCatRaffKeeper.performUpkeep(performData);

      const requestId = 1;
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaff.address, {
        gasLimit: 2000000,
      });

      await increaseTime(BigNumber.from(secondsPerWeek));

      const tx = await pixCatRaff.connect(alice).withdrawOldTickets(0, 3, true);
      expect(tx).emit(pixCatRaff, 'TicketsWithdrawn').withArgs([0, alice.address, 10]);
    });
  });

  describe('#claimPrize', () => {
    let performData = utils.defaultAbiCoder.encode(['uint256'], [0]);
    const requestId = 1;
    beforeEach(async function () {
      await pixToken.connect(alice).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(alice).addTickets(10);

      await pixToken.connect(bob).approve(pixCatRaff.address, ethers.constants.MaxUint256);
      await pixCatRaff.connect(bob).addTickets(10);

      await increaseTime(BigNumber.from(secondsPerWeek));
      await pixCatRaffKeeper.performUpkeep(performData);

      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaff.address, {
        gasLimit: 2000000,
      });

      const winnings = await pixCatRaff.winnings(0);
    });
    it('no prize', async function () {
      await expect(pixCatRaff.connect(carol).claimPrize()).to.be.revertedWith('no prize');
    });
    it('claim alice or bob prize', async function () {
      const alicePrize = await pixCatRaff.prizes(alice.address);
      const bobPrize = await pixCatRaff.prizes(bob.address);

      expect(alicePrize.add(bobPrize)).equal(weeklyPrize);

      if (alicePrize.gt(BigNumber.from(0))) {
        const tx = await pixCatRaff.connect(alice).claimPrize();
        expect(tx).emit(pixCatRaff, 'PrizeClaimed').withArgs([alice.address, alicePrize]);
      }
      if (bobPrize.gt(BigNumber.from(0))) {
        const tx = await pixCatRaff.connect(bob).claimPrize();
        expect(tx).emit(pixCatRaff, 'PrizeClaimed').withArgs([bob.address, bobPrize]);
      }
    });
  });
});
