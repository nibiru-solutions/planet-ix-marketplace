import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Contract, BigNumber, utils, constants, Wallet, BigNumberish } from 'ethers';
import { Address, ecsign } from 'ethereumjs-util';
import { DENOMINATOR, generateRandomAddress, getMerkleTree, PIXCategory, PIXSize } from './utils';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { IPIX, PIX, PIXSaleV2, PIXSaleV2__factory } from '../src/types';
import MerkleTree from 'merkletreejs';
const { keccak256, defaultAbiCoder, toUtf8Bytes, solidityPack } = utils;

type SaleInfo = {
  seller: string;
  executeBySeller: boolean;
  nftToken: string;
  tokenIds: BigNumber[];
  tokenAmounts: BigNumber[];
  hashes: string[];
  minPrice: BigNumber;
  validUntil: BigNumber;
  is721: boolean;
  inLandmark: boolean;
};

type SaleOfferInfo = {
  buyer: string;
  saleSignatures: string[];
  price: BigNumberish;
  validUntil: BigNumberish;
};

type OfferInfo = {
  seller: string;
  buyer: string;
  nftToken: string;
  tokenIds: BigNumberish[];
  tokenAmounts: BigNumberish[];
  hashes: string[];
  price: BigNumberish;
  validUntil: BigNumberish;
  is721: boolean;
  inLandmark: boolean;
};

type PIXInfo = {
  to: string,
  pixId: number,
  category: PIXCategory,
  size: PIXSize
};


describe('PIXSaleV2', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;
  let treasury: string = generateRandomAddress();
  let pixNFT: Contract;
  let saleV2: Contract;
  let pixtToken: Contract;
  let merkleMinter: Contract;

  beforeEach(async function () {
    [owner, alice, bob] = await ethers.getSigners();

    const PIXTFactory = await ethers.getContractFactory('PIXT');
    pixtToken = await PIXTFactory.connect(bob).deploy();

    const MockTokenFactory = await ethers.getContractFactory('MockToken');
    const usdc = await MockTokenFactory.deploy('Mock USDC', 'USDC', 6);

    const PIXFactory = await ethers.getContractFactory('PIX');
    pixNFT = await upgrades.deployProxy(PIXFactory, [pixtToken.address, usdc.address]);

    const PIXMerkleMinterFactory = await ethers.getContractFactory('PIXMerkleMinter');
    merkleMinter = await upgrades.deployProxy(PIXMerkleMinterFactory, [pixNFT.address]);

    const PIXSaleV2Factory = await ethers.getContractFactory('PIXSaleV2');
    saleV2 = await upgrades.deployProxy(PIXSaleV2Factory, [
      pixtToken.address,
      pixNFT.address,
      merkleMinter.address,
    ]);

    await pixNFT.setTrader(saleV2.address, true);
    await pixNFT.setTrader(merkleMinter.address, true);
    await saleV2.setWhitelistedNFTs(pixNFT.address, true);

    await pixNFT.setModerator(merkleMinter.address, true);
    await merkleMinter.setDelegateMinter(saleV2.address, true);
  });

  describe('#buy function', () => {
    const tokenId = 1;
    const price = utils.parseEther('1');

    beforeEach(async () => {
      await pixNFT.safeMint(alice.address, [0, PIXCategory.Rare, PIXSize.Sector]);
      await pixNFT.safeMint(alice.address, [0, PIXCategory.Rare, PIXSize.Sector]);
      await pixNFT.safeMint(alice.address, [0, PIXCategory.Rare, PIXSize.Sector]);
      await pixNFT.connect(alice).setApprovalForAll(saleV2.address, true);
    });

    it('should purchase PIX and send to seller and treasury', async () => {
      await saleV2.setTreasury(treasury, 100, 0, false);
      await pixtToken.connect(bob).approve(saleV2.address, price);

      const aliceBalanceBefore = await pixtToken.balanceOf(alice.address);
      const treasuryBalanceBefore = await pixtToken.balanceOf(treasury);

      const saleInfos: SaleInfo[] = [
        {
          seller: alice.address,
          executeBySeller: false,
          nftToken: pixNFT.address,
          tokenIds: [BigNumber.from(tokenId)],
          tokenAmounts: [BigNumber.from(0)],
          hashes: [],
          minPrice: price,
          validUntil: BigNumber.from('7777777777'),
          is721: true,
          inLandmark: false,
        },
      ];

      const { sig, saleSignatures } = await getDigest(saleV2, alice, saleInfos);

      await saleV2.connect(bob).buy([saleSignatures, 0, saleInfos[0], [], [], [], sig], price);
      expect(await pixNFT.ownerOf(tokenId)).to.be.equal(bob.address);
      const fee = price.mul(BigNumber.from('100')).div(DENOMINATOR);
      expect(await pixtToken.balanceOf(alice.address)).to.be.equal(
        aliceBalanceBefore.add(price).sub(fee),
      );
      expect(await pixtToken.balanceOf(treasury)).to.be.equal(treasuryBalanceBefore.add(fee));
    });

    it('should purchase multiple tokens', async () => {
      await saleV2.setTreasury(treasury, 100, 0, false);
      await pixtToken.connect(bob).approve(saleV2.address, price);

      const aliceBalanceBefore = await pixtToken.balanceOf(alice.address);
      const treasuryBalanceBefore = await pixtToken.balanceOf(treasury);

      let tokenIds = [BigNumber.from(1), BigNumber.from(2), BigNumber.from(3)];

      const saleInfos: SaleInfo[] = [
        {
          seller: alice.address,
          executeBySeller: false,
          nftToken: pixNFT.address,
          tokenIds: tokenIds,
          tokenAmounts: [BigNumber.from(0)],
          hashes: [],
          minPrice: price,
          validUntil: BigNumber.from('7777777777'),
          is721: true,
          inLandmark: false,
        },
      ];

      const { sig, saleSignatures } = await getDigest(saleV2, alice, saleInfos);

      await saleV2.connect(bob).buy([saleSignatures, 0, saleInfos[0], [], [], [], sig], price);

      expect(await pixNFT.balanceOf(bob.address)).to.be.equal(tokenIds.length);
    });

  });

  describe('#executeSale function', () => {

    const tokenId = 1;
    const price = utils.parseEther('1');

    beforeEach(async () => {
      await pixNFT.safeMint(alice.address, [0, PIXCategory.Rare, PIXSize.Sector]);
      await pixNFT.connect(alice).setApprovalForAll(saleV2.address, true);
    });

    it('should sale PIX and send to seller and treasury', async () => {
      await saleV2.setTreasury(treasury, 100, 0, false);
      await pixtToken.connect(bob).approve(saleV2.address, price);

      const aliceBalanceBefore = await pixtToken.balanceOf(alice.address);
      const treasuryBalanceBefore = await pixtToken.balanceOf(treasury);

      const saleInfos: SaleInfo[] = [
        {
          seller: alice.address,
          executeBySeller: true,
          nftToken: pixNFT.address,
          tokenIds: [BigNumber.from(tokenId)],
          tokenAmounts: [BigNumber.from(0)],
          hashes: [],
          minPrice: price,
          validUntil: BigNumber.from('7777777777'),
          is721: true,
          inLandmark: false,
        },
      ];

      const { sig, saleSignatures } = await getDigest(saleV2, alice, saleInfos);

      const SaleParam = {
        saleSignatures: saleSignatures,
        saleIdx: 0,
        saleInfo: saleInfos[0],
        merklePixInfos: [],
        merkleRoot: [],
        merkleProofs: [],
        sig: sig,
      }

      const saleOfferInfos: SaleOfferInfo[] = [
        {
          buyer: bob.address,
          saleSignatures: saleSignatures,
          price: saleInfos[0].minPrice,
          validUntil: saleInfos[0].validUntil,
        }
      ];

      const { offerSig, offerSignatures } = await getSaleOfferSignatures(saleV2, bob, saleOfferInfos);

      const SaleOfferParam = {
        saleOfferSignatures: offerSignatures,
        saleOfferIdx: 0,
        saleOfferInfo: saleOfferInfos[0],
        sig: offerSig,
      }

      await saleV2.connect(alice).executeSale(SaleParam, SaleOfferParam);

      expect(await pixNFT.ownerOf(tokenId)).to.be.equal(bob.address);
      const fee = price.mul(BigNumber.from('100')).div(DENOMINATOR);
      expect(await pixtToken.balanceOf(alice.address)).to.be.equal(
        aliceBalanceBefore.add(price).sub(fee),
      );
      expect(await pixtToken.balanceOf(treasury)).to.be.equal(treasuryBalanceBefore.add(fee));
    });

  });

  describe('#acceptOffer function', () => {

    const tokenId = 1;
    const price = utils.parseEther('1');

    beforeEach(async () => {
      await pixNFT.safeMint(alice.address, [0, PIXCategory.Rare, PIXSize.Sector]);
      await pixNFT.connect(alice).setApprovalForAll(saleV2.address, true);
    });

    it('should accept PIX offer and send to seller and treasury', async () => {
      await saleV2.setTreasury(treasury, 100, 0, false);
      await pixtToken.connect(bob).approve(saleV2.address, price);

      const aliceBalanceBefore = await pixtToken.balanceOf(alice.address);
      const treasuryBalanceBefore = await pixtToken.balanceOf(treasury);

      const offerInfos: OfferInfo[] = [
        {
          seller: alice.address,
          buyer: bob.address,
          nftToken: pixNFT.address,
          tokenIds: [BigNumber.from(tokenId)],
          tokenAmounts: [BigNumber.from(0)],
          hashes:[],
          price: price,
          validUntil: BigNumber.from('7777777777'),
          is721: true,
          inLandmark: false,
        },
      ];

      const { offerSig, offerSignatures } = await getOfferSignatures(saleV2, bob, offerInfos);

      const offerParam = 
      {
        offerSignatures: offerSignatures,
        offerIdx: 0,
        offerInfo: offerInfos[0],
        merklePixInfos: [],
        merkleRoot: [],
        merkleProofs: [],
        sig: offerSig,
      };

      await saleV2.connect(alice).acceptOffer(offerParam);

      expect(await pixNFT.ownerOf(tokenId)).to.be.equal(bob.address);
      const fee = price.mul(BigNumber.from('100')).div(DENOMINATOR);
      expect(await pixtToken.balanceOf(alice.address)).to.be.equal(
        aliceBalanceBefore.add(price).sub(fee),
      );
      expect(await pixtToken.balanceOf(treasury)).to.be.equal(treasuryBalanceBefore.add(fee));
    });

  });

  describe('#mints with merkle minter', () => {
    const tokenId = 1;
    const price = utils.parseEther('1');

    let merkleTreeNodes = 100000;
    let leafNodes, pixes;
    let merkleTree: MerkleTree;

    beforeEach(async () => {
      await pixNFT.safeMint(alice.address, [0, PIXCategory.Rare, PIXSize.Sector]);
      await pixNFT.connect(alice).setApprovalForAll(saleV2.address, true);

      let accounts = [];
      for(let i=0; i<merkleTreeNodes; i++) {
        accounts.push(alice);
      }
      
      let merkleTreeData = getMerkleTree(accounts);
      leafNodes = merkleTreeData.leafNodes;
      merkleTree = merkleTreeData.merkleTree;
      pixes = merkleTreeData.pixes;

      await merkleMinter.setMerkleRoot(merkleTree.getRoot(), true);
    });

    const getPixInfoHashes = async (indexes: number[]) => {
      let hashes = indexes.map( (pixIndex) => 
          utils.solidityKeccak256(
            [
              'address',
              'uint256',
              'uint8',
              'uint8'
            ],
            [
              pixes[pixIndex].to,
              pixes[pixIndex].pixId,
              pixes[pixIndex].category,
              pixes[pixIndex].size
            ]
        )
      );
    
      return hashes;
    }

    it('should mint PIX with merkle minter', async () => {
      await saleV2.setTreasury(treasury, 100, 0, false);
      await pixtToken.connect(bob).approve(saleV2.address, price);

      const aliceBalanceBefore = await pixtToken.balanceOf(alice.address);
      const treasuryBalanceBefore = await pixtToken.balanceOf(treasury);

      let merkleIndexes = [0,1,2];
      let pixInfos = merkleIndexes.map( (pixIndex) => pixes[pixIndex]);
      let pixInfoHashes = await getPixInfoHashes(merkleIndexes);
      let merkeRoots = merkleIndexes.map( () => merkleTree.getRoot());
      let merkleProofs = merkleIndexes.map( (pixIndex) => 
            merkleTree.getHexProof(leafNodes[pixIndex], pixIndex) 
          );

      const saleInfos: SaleInfo[] = [
        {
          seller: alice.address,
          executeBySeller: false,
          nftToken: pixNFT.address,
          tokenIds: [],
          tokenAmounts: [],
          hashes: pixInfoHashes,
          minPrice: price,
          validUntil: BigNumber.from('7777777777'),
          is721: true,
          inLandmark: false,
        },
      ];

      const { sig, saleSignatures } = await getDigest(saleV2, alice, saleInfos);

      await saleV2.connect(bob).buy(
        [
          saleSignatures, 
          0, 
          saleInfos[0], 
          pixInfos, 
          merkeRoots, 
          merkleProofs, 
          sig
        ], 
        price
      );

      expect( await pixNFT.balanceOf(bob.address)).to.be.equal(merkleIndexes.length);
    });


  });
});

const getDigest = async (sale: Contract, seller: SignerWithAddress, saleInfos: SaleInfo[]) => {
  const domain = {
    name: 'PlanetIX',
    version: '2',
    chainId: (await ethers.provider.getNetwork()).chainId,
    verifyingContract: sale.address,
  };

  const types = {
    Sales: [{ name: 'signatures', type: 'bytes32[]' }],
  };

  const signatures = saleInfos.map((info) =>
    utils.solidityKeccak256(
      [
        'address',
        'bool',
        'address',
        'uint256[]',
        'uint256[]',
        'bytes32[]',
        'uint256',
        'uint64',
        'bool',
        'bool',
      ],
      [
        info.seller,
        info.executeBySeller,
        info.nftToken,
        info.tokenIds,
        info.tokenAmounts,
        info.hashes,
        info.minPrice,
        info.validUntil,
        info.is721,
        info.inLandmark,
      ],
    ),
  );

  const value = {
    seller: seller.address,
    signatures,
  };

  const signature = await seller._signTypedData(domain, types, value);
  const split = utils.splitSignature(signature);
  const sig = utils.defaultAbiCoder.encode(
    ['uint8', 'bytes32', 'bytes32'],
    [split.v, split.r, split.s],
  );
  return {
    sig,
    saleSignatures: signatures,
  };
};

const getSaleOfferSignatures = async (sale: Contract, buyer: SignerWithAddress, saleOfferInfos: SaleOfferInfo[]) => {
  const domain = {
    name: 'PlanetIX',
    version: '2',
    chainId: (await ethers.provider.getNetwork()).chainId,
    verifyingContract: sale.address,
  };

  const types = {
    Sales: [{ name: 'signatures', type: 'bytes32[]' }],
  };

  const signatures = saleOfferInfos.map((info) =>
    utils.solidityKeccak256(
      [
        'address',
        'bytes32[]',
        'uint256',
        'uint64',
      ],
      [
        info.buyer,
        info.saleSignatures,
        info.price,
        info.validUntil,
      ],
    ),
  );

  const value = {
    buyer: buyer.address,
    signatures,
  };

  const signature = await buyer._signTypedData(domain, types, value);
  const split = utils.splitSignature(signature);
  const sig = utils.defaultAbiCoder.encode(
    ['uint8', 'bytes32', 'bytes32'],
    [split.v, split.r, split.s],
  );
  return {
    offerSig: sig,
    offerSignatures: signatures,
  };
};

const getOfferSignatures = async (sale: Contract, buyer: SignerWithAddress, saleOfferInfos: OfferInfo[]) => {
  const domain = {
    name: 'PlanetIX',
    version: '2',
    chainId: (await ethers.provider.getNetwork()).chainId,
    verifyingContract: sale.address,
  };

  const types = {
    Sales: [{ name: 'signatures', type: 'bytes32[]' }],
  };

  const signatures = saleOfferInfos.map((info) =>
    utils.solidityKeccak256(
      [
        'address',
        'address',
        'address',
        'uint256[]',
        'uint256[]',
        'bytes32[]',
        'uint256',
        'uint64',
        'bool',
        'bool',
      ],
      [
        info.seller, 
        info.buyer,
        info.nftToken,
        info.tokenIds,
        info.tokenAmounts,
        info.hashes,
        info.price,
        info.validUntil,
        info.is721,
        info.inLandmark
      ],
    ),
  );

  const value = {
    buyer: buyer.address,
    signatures,
  };

  const signature = await buyer._signTypedData(domain, types, value);
  const split = utils.splitSignature(signature);
  const sig = utils.defaultAbiCoder.encode(
    ['uint8', 'bytes32', 'bytes32'],
    [split.v, split.r, split.s],
  );
  return {
    offerSig: sig,
    offerSignatures: signatures,
  };
};

