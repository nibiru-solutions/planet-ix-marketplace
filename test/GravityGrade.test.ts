import { assert, expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { Signer, Contract, BigNumber, providers, utils } from 'ethers';
import { PIXCategory, PIXSize } from './utils';
import { constants } from '@openzeppelin/test-helpers';
import { parseEther } from 'ethers/lib/utils';
import {delay} from "@nomiclabs/hardhat-etherscan/dist/src/etherscan/EtherscanService";

describe('Gravity Grade', function () {
  this.timeout(8000)
  let gravityGrade: Contract;
  let gravityGradeWrapper: Contract;
  let maxCoin: Contract;
  let shazCoin: Contract;

  let oracle: Contract;
  let swapManager: Contract;

  let owner: Signer;
  let alice_moderator: Signer;
  let bob_player: Signer;
  let charles_trusted: Signer;

  let swap_mint_amt = 100;

  beforeEach(async function () {
    [owner, alice_moderator, bob_player, charles_trusted] = await ethers.getSigners();

    let erc20Factory = await ethers.getContractFactory('ERC20Mock');
    maxCoin = await erc20Factory.deploy('MaxCoin', 'MXC', await owner.getAddress(), 0);
    shazCoin = await erc20Factory.deploy('ShazCoin', 'SZC', await owner.getAddress(), 0);

    let oracleFactory = await ethers.getContractFactory('OracleManagerMock');
    oracle = await oracleFactory.deploy();
    let swapFactory = await ethers.getContractFactory('SwapManagerMock');
    swapManager = await swapFactory.deploy(oracle.address);

    await swapManager.payMe({ value: parseEther('50.0') });
    await maxCoin.mint(swapManager.address, swap_mint_amt);
    await shazCoin.mint(swapManager.address, swap_mint_amt);
    await oracle.setRatio(constants.ZERO_ADDRESS, shazCoin.address, 1, 10);
    await oracle.setRatio(constants.ZERO_ADDRESS, maxCoin.address, 1, 5);
    await oracle.setRatio(shazCoin.address, maxCoin.address, 10, 5);

    // INSERT YOUR GG IMPLEMENTATION BELOW
    let ggFactory = await ethers.getContractFactory('GravityGrade');
    gravityGrade = await upgrades.deployProxy(ggFactory, ['Gravity Grade', 'IX-GG']);

    await gravityGrade.setModerator(await alice_moderator.getAddress());
    await gravityGrade.connect(alice_moderator).setTokenUri(1, 'This is a toy URI');
    await gravityGrade.connect(alice_moderator).setSwapManager(swapManager.address);
    await gravityGrade.connect(alice_moderator).setOracleManager(oracle.address);
    await gravityGrade
      .connect(alice_moderator)
      .setTrusted(await charles_trusted.getAddress(), true);
  });

  describe('Create sale', function () {
    it("Should increment saleId's by one", async function () {
      let firstId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(1, 1, 1, 1, maxCoin.address, true);
      gravityGrade.connect(alice_moderator).createNewSale(1, 1, 1, 1, maxCoin.address, true);
      let secondId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(1, 1, 1, 1, maxCoin.address, true);
      expect(secondId - firstId).to.equal(1);
    });
    it('Should emit event', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).createNewSale(1, 1, 2, 3, maxCoin.address, true),
      )
        .to.emit(gravityGrade, 'SaleInfoUpdated')
        .withArgs(0, 1, 1, 2, 3, maxCoin.address);
      await expect(
        gravityGrade.connect(alice_moderator).createNewSale(1, 1, 2, 3, shazCoin.address, true),
      )
        .to.emit(gravityGrade, 'SaleInfoUpdated')
        .withArgs(1, 1, 1, 2, 3, shazCoin.address);
    });
  });

  describe('Create sale reverts', function () {
    it('Should revert on non moderator trying to create sale', async function () {
      await expect(
        gravityGrade.connect(bob_player).createNewSale(1, 1, 1, 1, maxCoin.address, true),
      ).to.revertedWith('GravityGrade__NotGovernance');
    });
    it('Should revert on zero sale cap', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).createNewSale(1, 1, 0, 1, maxCoin.address, true),
      ).to.revertedWith('GravityGrade__ZeroSaleCap');
    });
    it('Should revert on zero user sale cap', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).createNewSale(1, 1, 1, 0, maxCoin.address, true),
      ).to.revertedWith('GravityGrade__ZeroUserSaleCap');
    });
    it('Should revert on non initialised token id', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).createNewSale(2, 1, 1, 1, maxCoin.address, true),
      ).to.revertedWith('GravityGrade__TokenNotSet(2)');
    });
  });

  describe('Delete sale', function () {
    let saleId;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(1, 1, 2, 3, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).createNewSale(1, 1, 2, 3, maxCoin.address, true);
    });
    it('Should emit event on deletion', async function () {
      await expect(gravityGrade.connect(alice_moderator).deleteSale(saleId))
        .to.emit(gravityGrade, 'SaleDeleted')
        .withArgs(saleId);
    });
  });

  describe('Delete sale reverts', function () {
    let saleId;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(1, 1, 2, 3, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).createNewSale(1, 1, 2, 3, maxCoin.address, true);
    });
    it('Should revert on non moderator call', async function () {
      await expect(gravityGrade.connect(bob_player).deleteSale(saleId)).to.revertedWith(
        'GravityGrade__NotGovernance',
      );
    });
    it('Should revert on invalid sale id', async function () {
      await expect(gravityGrade.connect(alice_moderator).deleteSale(999)).to.revertedWith(
        'GravityGrade__NonExistentSale(' + (999).toString() + ')',
      );
    });
    it('Should revert on deleted sale id', async function () {
      await gravityGrade.connect(alice_moderator).deleteSale(saleId);
      await expect(gravityGrade.connect(alice_moderator).deleteSale(saleId)).to.revertedWith(
        'GravityGrade__NonExistentSale(' + saleId.toString() + ')',
      );
    });
  });

  describe('Modify sale', function () {
    let saleId;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(1, 1, 2, 3, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).createNewSale(1, 1, 2, 3, maxCoin.address, true);
    });

    it('Should emit event when modifying sale', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).modifySale(saleId, 2, 3, 4, shazCoin.address, false),
      )
        .to.emit(gravityGrade, 'SaleInfoUpdated')
        .withArgs(saleId, 1, 2, 3, 4, shazCoin.address);
    });
  });

  describe('Modify sale reverts', function () {
    let saleId;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(1, 1, 2, 3, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).createNewSale(1, 1, 2, 3, maxCoin.address, true);
    });

    it('Should revert on non moderator call', async function () {
      await expect(
        gravityGrade.connect(bob_player).modifySale(saleId, 2, 3, 4, shazCoin.address, false),
      ).to.revertedWith('GravityGrade__NotGovernance');
    });
    it('Should revert on invalid sale id', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).modifySale(99999, 2, 1, 4, shazCoin.address, false),
      ).to.revertedWith('GravityGrade__NonExistentSale(99999)');
    });

    it('Should revert on deleted sale', async function () {
      await gravityGrade.connect(alice_moderator).deleteSale(saleId);
      await expect(
        gravityGrade.connect(alice_moderator).modifySale(saleId, 2, 1, 4, shazCoin.address, false),
      ).to.revertedWith(`GravityGrade__NonExistentSale(${saleId})`);
    });

    it('Should revert on zero cap', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).modifySale(saleId, 2, 0, 4, shazCoin.address, false),
      ).to.revertedWith('GravityGrade__ZeroSaleCap');
    });
    it('Should revert on zero user cap', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).modifySale(saleId, 2, 1, 0, shazCoin.address, false),
      ).to.revertedWith('GravityGrade__ZeroUserSaleCap');
    });
  });

  describe('Set Sale State ', function () {
    let saleId;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(1, 1, 2, 3, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).createNewSale(1, 1, 2, 3, maxCoin.address, true);
    });
    it('Should emit event when updating sale state', async function () {
      await expect(gravityGrade.connect(alice_moderator).setSaleState(saleId, true))
        .to.emit(gravityGrade, 'SaleState')
        .withArgs(saleId, true);
      await expect(gravityGrade.connect(alice_moderator).setSaleState(saleId, false))
        .to.emit(gravityGrade, 'SaleState')
        .withArgs(saleId, false);
    });
  });

  describe('Set Sale State reverts', function () {
    let saleId;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(1, 1, 2, 3, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).createNewSale(1, 1, 2, 3, maxCoin.address, true);
    });
    it('Should revert on non moderator call', async function () {
      await expect(gravityGrade.connect(bob_player).setSaleState(saleId, true)).to.revertedWith(
        'GravityGrade__NotGovernance',
      );
    });
    it('Should revert on invalid sale id', async function () {
      await expect(gravityGrade.connect(alice_moderator).setSaleState(9999, true)).to.revertedWith(
        `GravityGrade__NonExistentSale(${9999})`,
      );
    });
    it('Should revert on deleted sale', async function () {
      await gravityGrade.connect(alice_moderator).deleteSale(saleId);
      await expect(
        gravityGrade.connect(alice_moderator).setSaleState(saleId, true),
      ).to.revertedWith(`GravityGrade__NonExistentSale(${saleId})`);
    });
  });

  describe('Buy Pack', function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 10;
    let bob_mint = 100;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
      await gravityGrade.connect(alice_moderator).setPaperCurrency(maxCoin.address);
    });
    it('Allows Bob to buy his pack', async function () {
      await maxCoin.connect(bob_player).approve(gravityGrade.address, price);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(bob_mint - price);
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(1);
    });

    it('Allows Bob (via paper) to buy his pack', async function () {
      await maxCoin.connect(bob_player).approve(gravityGrade.address, price);
      await gravityGrade
        .connect(bob_player)
        .onPaper(await bob_player.getAddress(), constants.ZERO_ADDRESS, 0, 1, saleId);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(bob_mint - price);
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(1);
    });

    it('Allows Bob to buy multiple packs', async function () {
      let numBought = 3;
      await maxCoin.connect(bob_player).approve(gravityGrade.address, numBought * price);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, numBought, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        bob_mint - numBought * price,
      );
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(
        numBought,
      );
    });

    it('Allows Bob (via paper) to buy multiple packs', async function () {
      let numBought = 3;
      await maxCoin.connect(bob_player).approve(gravityGrade.address, numBought * price);
      await gravityGrade
        .connect(bob_player)
        .onPaper(await bob_player.getAddress(), constants.ZERO_ADDRESS, 0, numBought, saleId);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        bob_mint - numBought * price,
      );
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(
        numBought,
      );
    });

    it('Allows Bob to make multiple purchases without unexpected behaviour', async function () {
      await maxCoin.connect(bob_player).approve(gravityGrade.address, bob_mint);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(bob_mint - price);
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(1);
      let numBought = 3;
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, numBought, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        bob_mint - (numBought + 1) * price,
      );
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(
        numBought + 1,
      );
    });

    it('Allows Bob (via paper) to make multiple purchases without unexpected behaviour', async function () {
      await maxCoin.connect(bob_player).approve(gravityGrade.address, bob_mint);
      await gravityGrade
        .connect(bob_player)
        .onPaper(await bob_player.getAddress(), constants.ZERO_ADDRESS, 0, 1, saleId);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(bob_mint - price);
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(1);
      let numBought = 3;
      await gravityGrade
        .connect(bob_player)
        .onPaper(await bob_player.getAddress(), constants.ZERO_ADDRESS, 0, numBought, saleId);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        bob_mint - (numBought + 1) * price,
      );
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(
        numBought + 1,
      );
    });

    it('Allows Bob to purchase with Matic', async function () {
      //console.log('test Allows Bob to purchase with Matic unfinished');
      //expect(false).to.equal(true);

      // This test really needs to be looked over it's really dumb but I couldn't get the exact gas
      let hugePrice = BigNumber.from(parseEther('10.0'));
      let saleId2 = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, hugePrice, cap, playerCap, maxCoin.address, true);
      let preBalance = await bob_player.getBalance();
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, hugePrice, cap, playerCap, constants.ZERO_ADDRESS, true);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId2, 1, 0, constants.ZERO_ADDRESS, constants.ZERO_ADDRESS, {
          value: hugePrice,
        });
      let postBalance = await bob_player.getBalance();
      let diff = preBalance.sub(postBalance);
      expect(diff >= hugePrice).to.equal(true);
      expect(diff <= parseEther('10.001')).to.equal(true);
    });

    it('Allows Bob to purchase with Matic and gives him the correct change', async function () {
      // This test really needs to be looked over it's really dumb but I couldn't get the exact gas
      await gravityGrade
        .connect(alice_moderator)
        .setAllowedPaymentCurrencies(1, [maxCoin.address, constants.ZERO_ADDRESS]);
      let hugePrice = BigNumber.from(parseEther('10.0'));
      let saleId2 = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      let preBalance = await bob_player.getBalance();
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, hugePrice, cap, playerCap, constants.ZERO_ADDRESS, true);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId2, 1, 0, constants.ZERO_ADDRESS, constants.ZERO_ADDRESS, {
          value: parseEther('20.0'),
        });
      let postBalance = await bob_player.getBalance();
      let diff = preBalance.sub(postBalance);
      expect(diff >= hugePrice).to.equal(true);
      expect(diff <= parseEther('10.001')).to.equal(true);
    });

    it('Allows Bob to purchase with shaz coin in a max coin default currency sale', async function () {
      await gravityGrade
        .connect(alice_moderator)
        .setAllowedPaymentCurrencies(saleId, [maxCoin.address, shazCoin.address]);
      let price_in_shazcoins = await oracle.callStatic.getAmountOut(
        maxCoin.address,
        shazCoin.address,
        price,
      );
      await shazCoin.connect(bob_player).approve(gravityGrade.address, price_in_shazcoins);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, shazCoin.address);
      expect(await shazCoin.balanceOf(await bob_player.getAddress())).to.equal(
        bob_mint - price_in_shazcoins,
      );
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(1);
    });

    it('Allows Bob to purchase with shaz coin in a matic default currency sale', async function () {
      let hugePrice = BigNumber.from(parseEther('10.0'));
      let shazPrice = await oracle.callStatic.getAmountOut(
        constants.ZERO_ADDRESS,
        shazCoin.address,
        hugePrice,
      );
      await shazCoin.mint(await bob_player.getAddress(), shazPrice);
      await shazCoin.connect(bob_player).approve(gravityGrade.address, shazPrice);
      let bob_shazBalance_pre = await shazCoin.balanceOf(await bob_player.getAddress());
      let gg_shazBalance_pre = await shazCoin.balanceOf(gravityGrade.address);
      let saleId2 = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, hugePrice, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, hugePrice, cap, playerCap, constants.ZERO_ADDRESS, true);
      await gravityGrade
        .connect(alice_moderator)
        .setAllowedPaymentCurrencies(saleId2, [constants.ZERO_ADDRESS, shazCoin.address]);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId2, 1, 0, constants.ZERO_ADDRESS, shazCoin.address);
      let bob_shazBalance_post = await shazCoin.balanceOf(await bob_player.getAddress());
      let gg_shazBalance_post = await shazCoin.balanceOf(gravityGrade.address);
      expect(bob_shazBalance_pre.sub(bob_shazBalance_post)).to.equal(shazPrice);
      expect(gg_shazBalance_post.sub(gg_shazBalance_pre)).to.equal(shazPrice);
    });

    it('Allows Bob to purchase with matic in a shaz coin default currency sale', async function () {
      let preBalance = await bob_player.getBalance();
      let hugePrice = BigNumber.from(parseEther('10.0'));
      let shazPrice = await oracle.callStatic.getAmountOut(
        constants.ZERO_ADDRESS,
        shazCoin.address,
        hugePrice,
      );
      let saleId2 = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, shazPrice, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, shazPrice, cap, playerCap, shazCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .setAllowedPaymentCurrencies(saleId2, [constants.ZERO_ADDRESS, shazCoin.address]);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId2, 1, 0, constants.ZERO_ADDRESS, constants.ZERO_ADDRESS, {
          value: parseEther('10.0'),
        });
      let postBalance = await bob_player.getBalance();
      let diff = preBalance.sub(postBalance);
      expect(diff >= hugePrice).to.equal(true);
      expect(diff <= parseEther('10.001')).to.equal(true);
    });

    it("Retains the ShazCoin if it's supposed to", async function () {
      await gravityGrade
        .connect(alice_moderator)
        .setAllowedPaymentCurrencies(saleId, [maxCoin.address, shazCoin.address]);
      let price_in_shazcoins = await oracle.callStatic.getAmountOut(
        maxCoin.address,
        shazCoin.address,
        price,
      );
      await shazCoin.connect(bob_player).approve(gravityGrade.address, price_in_shazcoins);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, shazCoin.address);
      expect(await shazCoin.balanceOf(await bob_player.getAddress())).to.equal(
        bob_mint - price_in_shazcoins,
      );
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(1);
      expect(await shazCoin.balanceOf(gravityGrade.address)).to.equal(price_in_shazcoins);
    });

    it("Exchanges the ShazCoins for MaxCoins if it's supposed to", async function () {
      await gravityGrade
        .connect(alice_moderator)
        .modifySale(saleId, price, cap, playerCap, maxCoin.address, false);
      await gravityGrade
        .connect(alice_moderator)
        .setAllowedPaymentCurrencies(saleId, [maxCoin.address, shazCoin.address]);
      let price_in_shazcoins = await oracle.callStatic.getAmountOut(
        maxCoin.address,
        shazCoin.address,
        price,
      );
      await shazCoin.connect(bob_player).approve(gravityGrade.address, price_in_shazcoins);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, shazCoin.address);
      expect(await shazCoin.balanceOf(await bob_player.getAddress())).to.equal(
        bob_mint - price_in_shazcoins,
      );
      expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(1);
      expect(await maxCoin.balanceOf(gravityGrade.address)).to.equal(price);
    });
  });

  describe('Buy Pack reverts', function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 5;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), 100);
      await maxCoin.connect(bob_player).approve(gravityGrade.address, 100);
      await shazCoin.mint(await bob_player.getAddress(), 100);
    });
    it('Should revert on sale inactive', async function () {
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, true);
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.revertedWith('GravityGrade__SaleInactive');
    });

    it('Should revert on invalid sale id', async function () {
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(9999, 1, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.revertedWith('GravityGrade__NonExistentSale(9999)');
    });

    it('Should revert on deleted sale', async function () {
      await gravityGrade.connect(alice_moderator).deleteSale(saleId);
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.revertedWith(`GravityGrade__NonExistentSale(${saleId})`);
    });

    it('Should revert on exceeding per player cap', async function () {
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 6, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.revertedWith('GravityGrade__PurchaseExceedsPlayerMax(0, 5)');
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 5, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.not.reverted; // This is so that we can see that buying 5 + 1 also throws the same error
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.revertedWith('GravityGrade__PurchaseExceedsPlayerMax(5, 5)');
    });

    it('Should revert on exceeding total cap', async function () {
      await maxCoin.mint(await owner.getAddress(), 100);
      await maxCoin.connect(owner).approve(gravityGrade.address, 100);
      await maxCoin.mint(await alice_moderator.getAddress(), 100);
      await maxCoin.connect(alice_moderator).approve(gravityGrade.address, 100);
      await expect(
        gravityGrade.connect(owner).buyPacks(saleId, 5, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.not.reverted;
      await expect(
        gravityGrade
          .connect(alice_moderator)
          .buyPacks(saleId, 4, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.not.reverted;
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 2, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.revertedWith('GravityGrade__PurchaseExceedsTotalMax(9, 10)'); // (5+4) +2 > 10
      await expect(
        gravityGrade
          .connect(alice_moderator)
          .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.not.reverted;
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address),
      ).to.revertedWith('GravityGrade__PurchaseExceedsTotalMax(10, 10)'); // (5+4+1) + 1 > 10
    });

    it('Should revert on non whitelisted currency', async function () {
      let price_in_shazcoins = await oracle.callStatic.getAmountOut(
        maxCoin.address,
        shazCoin.address,
        price,
      );
      await shazCoin.approve(gravityGrade.address, price_in_shazcoins);
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, shazCoin.address),
      ).to.revertedWith(`GravityGrade__CurrencyNotWhitelisted("${shazCoin.address}")`);
    });
    it('Should revert on insufficient value sent when paying with MATIC', async function () {
      let price_in_matic = await oracle.callStatic.getAmountOut(
        maxCoin.address,
        constants.ZERO_ADDRESS,
        price,
      );
      await gravityGrade.setAllowedPaymentCurrencies(saleId, [constants.ZERO_ADDRESS]);
      await expect(
        gravityGrade
          .connect(bob_player)
          .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, constants.ZERO_ADDRESS, {
            value: price_in_matic.sub(BigNumber.from(1)),
          }),
      ).to.revertedWith(
        `GravityGrade__InsufficientEthValue(${price_in_matic.sub(
          BigNumber.from(1),
        )}, ${price_in_matic})`,
      );
    });
  });

  describe('Fee wallets and percentages', function () {
    let saleId;
    let price = 100;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 5;
    let numBought = 5;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), 500);
      await maxCoin.connect(bob_player).approve(gravityGrade.address, 500);
      await shazCoin.mint(await bob_player.getAddress(), 100);
    });
    it('Should allow setting the wallets and percentages', async function () {
      await expect(
        await gravityGrade
          .connect(owner)
          .setFeeWalletsAndPercentages(
            [await owner.getAddress(), await alice_moderator.getAddress()],
            [100, 200],
          ),
      )
        .to.emit(gravityGrade, 'BeneficiariesUpdated')
        .withArgs([await owner.getAddress(), await alice_moderator.getAddress()], [100, 200]);
    });
    it('Should give beneficiaries expected tokens', async function () {
      await gravityGrade
        .connect(owner)
        .setFeeWalletsAndPercentages(
          [await owner.getAddress(), await alice_moderator.getAddress()],
          [100, 200],
        );
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, numBought, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await owner.getAddress())).to.equal(
        (100 / 10000) * (price * numBought),
      );
      expect(await maxCoin.balanceOf(await alice_moderator.getAddress())).to.equal(
        (200 / 10000) * (price * numBought),
      );
    });
    it('Should overwrite previous beneficiaries on repeated calls', async function () {
      await gravityGrade
        .connect(owner)
        .setFeeWalletsAndPercentages(
          [await owner.getAddress(), await alice_moderator.getAddress()],
          [100, 200],
        );
      await gravityGrade
        .connect(owner)
        .setFeeWalletsAndPercentages([await owner.getAddress()], [100]);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, numBought, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await owner.getAddress())).to.equal(
        (100 / 10000) * (price * numBought),
      );
      expect(await maxCoin.balanceOf(await alice_moderator.getAddress())).to.equal(0);
    });
  });

  describe('Fee wallets and percentages reverts', function () {
    it('Should revert on non owner call', async function () {
      await expect(
        gravityGrade
          .connect(alice_moderator)
          .setFeeWalletsAndPercentages(
            [await owner.getAddress(), await alice_moderator.getAddress()],
            [100, 200],
          ),
      ).to.revertedWith('Ownable: caller is not the owner');
    });

    it('Should revert on sum exceeding 100%', async function () {
      await expect(
        gravityGrade
          .connect(owner)
          .setFeeWalletsAndPercentages(
            [await owner.getAddress(), await alice_moderator.getAddress()],
            [9000, 2000],
          ),
      ).to.revertedWith('GravityGrade__ValueTooLarge(11000, 10000)');
    });

    it('Should revert on singular entry exceeding 100%', async function () {
      await expect(
        gravityGrade
          .connect(owner)
          .setFeeWalletsAndPercentages([await owner.getAddress()], [10001]),
      ).to.revertedWith('GravityGrade__ValueTooLarge(10001, 10000)');
    });
  });

  describe('Airdrop', function () {
    it('Should allow trusted party to airdrop tokens', async function () {
      await gravityGrade.setTokenUri(2, 'This is a toy URI 2');
      await gravityGrade
        .connect(charles_trusted)
        .airdrop([await owner.getAddress(), await alice_moderator.getAddress()], [1, 2], [3, 4]);
      expect(await gravityGrade.balanceOf(await owner.getAddress(), 1)).to.equal(3);
      expect(await gravityGrade.balanceOf(await alice_moderator.getAddress(), 2)).to.equal(4);
    });
  });

  describe('Airdrop reverts', function () {
    it('Should not allow untrusted party to airdrop tokens', async function () {
      await gravityGrade.setTokenUri(2, 'This is a toy URI 2');
      await expect(
        gravityGrade
          .connect(owner)
          .airdrop([await owner.getAddress(), await alice_moderator.getAddress()], [1, 2], [3, 4]),
      ).to.revertedWith(`GravityGrade__NotTrusted("${await owner.getAddress()}")`);
    });
    it('Should not allow airdrop of uninitialised token', async function () {
      await expect(
        gravityGrade
          .connect(charles_trusted)
          .airdrop([await owner.getAddress(), await alice_moderator.getAddress()], [1, 2], [3, 4]),
      ).to.revertedWith('GravityGrade__TokenNotSet(2)');
    });
  });

  describe('Bulk Rebates', async function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 10;
    let bob_mint = 100;
    let rebate1 = 1000;
    let rebate2 = 1000;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
      await maxCoin.connect(bob_player).approve(gravityGrade.address, 100);
    });

    it('Should allow the moderator to add a bulk rebate', async function () {
      await expect(gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, rebate1)).to.not
        .reverted;
    });

    it('The bulk rebate should give the expected discount', async function () {
      await expect(gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, rebate1)).to.not
        .reverted;
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - rebate1 / 10000))),
      );
    });

    it('bulk rebate should give the expected discount when paying with matic', async function () {
      //console.log('test Allows Bob to purchase with Matic unfinished');
      //expect(false).to.equal(true);

      // This test really needs to be looked over it's really dumb but I couldn't get the exact gas
      let hugePrice = BigNumber.from(parseEther('10.0'));
      let saleId2 = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, hugePrice, cap, playerCap, maxCoin.address, true);
      let preBalance = await bob_player.getBalance();
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, hugePrice, cap, playerCap, constants.ZERO_ADDRESS, true);
      await expect(gravityGrade.connect(alice_moderator).addBulkDiscount(saleId2, 2, rebate1)).to
        .not.reverted;
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId2, 2, 0, constants.ZERO_ADDRESS, constants.ZERO_ADDRESS, {
          value: hugePrice.mul(2),
        });
      let postBalance = await bob_player.getBalance();
      let diff = preBalance.sub(postBalance);
      expect(
        diff >=
          hugePrice
            .mul(2)
            .mul(10000 - rebate1)
            .div(10000),
      ).to.equal(true);
      expect(
        diff <=
          hugePrice
            .mul(2)
            .mul(10000 - rebate1)
            .div(10000)
            .add(parseEther('0.001')),
      ).to.equal(true);
    });

    it('Adding subsequent rebates at same breakpoint should result in overwriting', async function () {
      await expect(gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, rebate1)).to.not
        .reverted;
      await expect(gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, 3 * rebate1)).to
        .not.reverted;
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - (3 * rebate1) / 10000))),
      );
    });
    it('should count rebates cumulatively', async function () {
      await expect(gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, rebate1)).to.not
        .reverted;
      await expect(gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 10, rebate2)).to
        .not.reverted;
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 10, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 10 * price * (1 - (rebate1 + rebate2) / 10000))),
      );
    });
  });

  describe('Bulk Rebates reverts', async function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 9;
    let bob_mint = 100;
    let rebate1 = 1000;
    let rebate2 = 1000;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
      await maxCoin.connect(bob_player).approve(gravityGrade.address, 100);
    });

    it('Should non allow non moderator to add a bulk rebate', async function () {
      await expect(
        gravityGrade.connect(bob_player).addBulkDiscount(saleId, 5, rebate1),
      ).to.revertedWith('GravityGrade__NotGovernance');
    });

    it('Should not allow adding a sale for an invalid saleId', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addBulkDiscount(99999, 5, rebate1),
      ).to.revertedWith('GravityGrade__NonExistentSale(99999)');
    });
    it('Should not allow adding a sale for a deleted sale', async function () {
      await gravityGrade.connect(alice_moderator).deleteSale(saleId);
      await expect(
        gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, rebate1),
      ).to.revertedWith(`GravityGrade__NonExistentSale(${saleId})`);
    });
    it('Should not allow discount breakpoint to exceed per player cap', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 10, rebate1),
      ).to.revertedWith(`GravityGrade__ValueTooLarge(${10}, ${9}`);
    });
    it('Should not allow discount amount to be exceed 100% ', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, 10001),
      ).to.revertedWith(`GravityGrade__ValueTooLarge(10001, 10000)`);
    });
    it('Should not allow cumulative discount amount to exceed 100% ', async function () {
      await gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 4, 9000);
      await expect(
        gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, 1001),
      ).to.revertedWith(`GravityGrade__ValueTooLarge(10001, 10000)`);
    });

    it('Should not revert when overwriting same sale multiple times', async function () {
      await gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 4, 9000);
      await expect(gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 4, 9000)).to.not
        .reverted;
    });
  });

  describe('Ownership rebates', async function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 10;
    let bob_mint = 100;
    let rebate1 = 1000;
    let rebate2 = 2000;
    let erc721: Contract;
    let erc1155: Contract;

    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
      await maxCoin.connect(bob_player).approve(gravityGrade.address, 100);

      let erc721fac = await ethers.getContractFactory('ERC721Mock');
      erc721 = await erc721fac.deploy('mock 721', 'm721');

      let erc1155Fac = await ethers.getContractFactory('ERC1155Mock');
      erc1155 = await erc1155Fac.deploy('some uri');
    });

    it('Should allow the moderator to add ownership rebate', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(rebate1),
        }),
      ).to.not.reverted;
    });

    it('Should apply rebate when owning the correct erc 721 token', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(rebate1),
        }),
      ).to.not.reverted;
      await erc721.mint(await bob_player.getAddress(), 1);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 0, erc721.address, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - rebate1 / 10000))),
      );
    });

    it('Should give the expected ownership discount when paying with matic', async function () {
      //console.log('test Allows Bob to purchase with Matic unfinished');
      //expect(false).to.equal(true);

      // This test really needs to be looked over it's really dumb but I couldn't get the exact gas
      let hugePrice = BigNumber.from(parseEther('10.0'));
      let saleId2 = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, hugePrice, cap, playerCap, maxCoin.address, true);
      let preBalance = await bob_player.getBalance();
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, hugePrice, cap, playerCap, constants.ZERO_ADDRESS, true);
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId2, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(rebate1),
        }),
      ).to.not.reverted;
      await erc721.mint(await bob_player.getAddress(), 1);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId2, 1, 0, erc721.address, constants.ZERO_ADDRESS, {
          value: hugePrice.mul(1),
        });
      let postBalance = await bob_player.getBalance();
      let diff = preBalance.sub(postBalance);
      expect(
        diff >=
          hugePrice
            .mul(1)
            .mul(10000 - rebate1)
            .div(10000),
      ).to.equal(true);
      expect(
        diff <=
          hugePrice
            .mul(1)
            .mul(10000 - rebate1)
            .div(10000)
            .add(parseEther('0.001')),
      ).to.equal(true);
    });

    it('Should override previous rebate if rebate is added to same token address (ERC721)', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(rebate1),
        }),
      ).to.not.reverted;
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(3 * rebate1),
        }),
      ).to.not.reverted;
      await erc721.mint(await bob_player.getAddress(), 1);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 0, erc721.address, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - (3 * rebate1) / 10000))),
      );
    });

    it('Should apply rebate when owning the correct erc 1155 token', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1, // ERC 721
          tokenAddress: erc1155.address,
          tokenId: 1,
          basisPoints: BigNumber.from(rebate2),
        }),
      ).to.not.reverted;
      await erc1155.mint(await bob_player.getAddress(), 1, 1);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 1, erc1155.address, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - rebate2 / 10000))),
      );
    });

    it('Should apply rebate when owning the correct erc 1155 token and there are multiple rebates on the same contract', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1, // ERC 721
          tokenAddress: erc1155.address,
          tokenId: 1,
          basisPoints: BigNumber.from(rebate2),
        }),
      ).to.not.reverted;
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1, // ERC 721
          tokenAddress: erc1155.address,
          tokenId: 2,
          basisPoints: BigNumber.from(3 * rebate2),
        }),
      ).to.not.reverted;
      await erc1155.mint(await bob_player.getAddress(), 1, 1);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 1, erc1155.address, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - rebate2 / 10000))),
      );
    });

    it('Should apply rebate when owning the correct erc 1155 token and there are multiple rebates on the same contract and there are erc721 rebates', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1,
          tokenAddress: erc1155.address,
          tokenId: 1,
          basisPoints: BigNumber.from(rebate2),
        }),
      ).to.not.reverted;
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1,
          tokenAddress: erc1155.address,
          tokenId: 2,
          basisPoints: BigNumber.from(3 * rebate2),
        }),
      ).to.not.reverted;
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 1,
          basisPoints: BigNumber.from(2 * rebate2),
        }),
      ).to.not.reverted;
      await erc1155.mint(await bob_player.getAddress(), 1, 1);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 1, erc1155.address, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - rebate2 / 10000))),
      );
    });

    it('Should apply rebate when owning the correct erc 721 token and there are multiple erc 1155 rebates', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1,
          tokenAddress: erc1155.address,
          tokenId: 1,
          basisPoints: BigNumber.from(rebate2),
        }),
      ).to.not.reverted;
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1,
          tokenAddress: erc1155.address,
          tokenId: 2,
          basisPoints: BigNumber.from(3 * rebate2),
        }),
      ).to.not.reverted;
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(2 * rebate2),
        }),
      ).to.not.reverted;
      await erc1155.mint(await bob_player.getAddress(), 1, 1);
      await erc721.mint(await bob_player.getAddress(), 4);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 0, erc721.address, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - (2 * rebate2) / 10000))),
      );
    });

    it('Should override previous rebate if rebate is added to same token address + token id (ERC1155)', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1, // ERC 721
          tokenAddress: erc1155.address,
          tokenId: 1,
          basisPoints: BigNumber.from(rebate2),
        }),
      ).to.not.reverted;
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1, // ERC 721
          tokenAddress: erc1155.address,
          tokenId: 1,
          basisPoints: BigNumber.from(3 * rebate2),
        }),
      ).to.not.reverted;
      await erc1155.mint(await bob_player.getAddress(), 1, 1);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 1, erc1155.address, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(Math.floor(bob_mint - 5 * price * (1 - (3 * rebate2) / 10000))),
      );
    });
  });

  describe('Ownership rebates reverts', async function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 10;
    let bob_mint = 100;
    let rebate1 = 1000;
    let rebate2 = 9000;
    let erc721: Contract;
    let erc1155: Contract;

    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
      await maxCoin.connect(bob_player).approve(gravityGrade.address, 100);

      let erc721fac = await ethers.getContractFactory('ERC721Mock');
      erc721 = await erc721fac.deploy('mock 721', 'm721');

      let erc1155Fac = await ethers.getContractFactory('ERC1155Mock');
      erc1155 = await erc1155Fac.deploy('some uri');
    });

    it('Should not allow non moderator to add ownership rebate', async function () {
      await expect(
        gravityGrade.connect(bob_player).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(rebate1),
        }),
      ).to.revertedWith('GravityGrade__NotGovernance()');
    });
    it('Should not allow rebates exceeding 100% (erc721)', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(10001),
        }),
      ).to.revertedWith('GravityGrade__InvalidSaleParameters()');
    });
    it('Should not allow rebates exceeding 100% (erc1155)', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1, // ERC 721
          tokenAddress: erc1155.address,
          tokenId: 1,
          basisPoints: BigNumber.from(10001),
        }),
      ).to.revertedWith('GravityGrade__InvalidSaleParameters()');
    });
    it('Should not allow ERC721s to be passed off as erc 1155', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 1, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 1,
          basisPoints: BigNumber.from(1000),
        }),
      ).to.revertedWith('GravityGrade__InvalidSaleParameters()');
    });
    it('Should not allow ERC1155s to be passed off as erc 721', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc1155.address,
          tokenId: 0,
          basisPoints: BigNumber.from(1000),
        }),
      ).to.revertedWith('GravityGrade__InvalidSaleParameters()');
    });

    it('Should revert if the user buys and does not actually own the token', async function () {
      await gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
        tokenType: 0, // ERC 721
        tokenAddress: erc721.address,
        tokenId: 0,
        basisPoints: BigNumber.from(rebate1),
      });
      await expect(
        gravityGrade.connect(bob_player).buyPacks(saleId, 1, 0, erc721.address, maxCoin.address),
      ).to.revertedWith(`GravityGrade__SenderDoesNotOwnToken("${erc721.address}", 0)`);
    });

    it('Should revert if the user buys and the token is not eligible for a rebate', async function () {
      await expect(
        gravityGrade.connect(bob_player).buyPacks(saleId, 1, 555, erc721.address, maxCoin.address),
      ).to.revertedWith(`GravityGrade__TokenNotEligibleForRebate("${erc721.address}")`);
    });
  });

  describe('Compound rebate tests', async function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 10;
    let bob_mint = 100;
    let rebate1 = 1000;
    let rebate2 = 2000;
    let erc721: Contract;
    let erc1155: Contract;

    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
      await maxCoin.connect(bob_player).approve(gravityGrade.address, 100);

      let erc721fac = await ethers.getContractFactory('ERC721Mock');
      erc721 = await erc721fac.deploy('mock 721', 'm721');

      let erc1155Fac = await ethers.getContractFactory('ERC1155Mock');
      erc1155 = await erc1155Fac.deploy('some uri');
    });

    it('Should apply both bulk and ownership rebate', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(rebate1),
        }),
      ).to.not.reverted;
      await gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 5, rebate2);
      await erc721.mint(await bob_player.getAddress(), 1);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 5, 0, erc721.address, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        BigNumber.from(
          Math.floor(bob_mint - 5 * price * (1 - rebate1 / 10000) * (1 - rebate2 / 10000)),
        ),
      );
    });

    it('Should revert if total bulk discounts and rebate amount effectively gives away the pack for free A', async function () {
      await gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 1, 5000);
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0,
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(8500),
        }),
      ).to.revertedWith(`GravityGrade__RebateTooLarge(${saleId}, ${price}, 5000, 8500)`);
    });
    it('Should revert if total bulk discounts and rebate amount effectively gives away the pack for free B', async function () {
      await gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 1, 9000);
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(5000),
        }),
      ).to.revertedWith(`GravityGrade__RebateTooLarge(${saleId}, ${price}, 9000, 5000)`);
    });
    it('Should revert if total bulk discounts and rebate amount effectively gives away the pack for free C', async function () {
      await gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 1, 1000);
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(9000),
        }),
      ).to.revertedWith(`GravityGrade__RebateTooLarge(${saleId}, ${price}, 1000, 9000)`);
    });
    it('Should revert if total bulk discounts and rebate amount effectively gives away the pack for free D', async function () {
      await gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 1, 500);
      await gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 2, 500);
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(9000),
        }),
      ).to.revertedWith(`GravityGrade__RebateTooLarge(${saleId}, ${price}, 1000, 9000)`);
    });
    it('Should revert if total bulk discounts and rebate amount effectively gives away the pack for free E', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
          tokenType: 0, // ERC 721
          tokenAddress: erc721.address,
          tokenId: 0,
          basisPoints: BigNumber.from(9500),
        }),
      ).to.revertedWith(`GravityGrade__RebateTooLarge(${saleId}, ${price}, 0, 9500)`);
    });
    it('Should revert if total bulk discounts and rebate amount effectively gives away the pack for free F', async function () {
      await expect(
        gravityGrade.connect(alice_moderator).addBulkDiscount(saleId, 1, 9500),
      ).to.revertedWith(`GravityGrade__RebateTooLarge(${saleId}, ${price}, 9500, 0)`);
    });
  });

  describe('withdraw', function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 10;
    let bob_mint = 100;
    beforeEach(async function () {
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
    });
    it('withdraws maxCoin', async function () {
      // Arrange
      await maxCoin.connect(bob_player).approve(gravityGrade.address, price);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address);
      const startingGGBalance = await maxCoin.balanceOf(gravityGrade.address);
      const startingModeratorBalance = await maxCoin.balanceOf(await alice_moderator.getAddress());
      // Act
      await gravityGrade.withdraw(await alice_moderator.getAddress(), maxCoin.address);
      const endingGGBalance = await maxCoin.balanceOf(gravityGrade.address);
      const endingModeratorBalance = await maxCoin.balanceOf(await alice_moderator.getAddress());
      // Assert
      expect(BigNumber.from(0)).to.equal(endingGGBalance);
      expect(BigNumber.from(startingGGBalance)).to.equal(endingModeratorBalance);
    });
    it('withdraws matic', async function () {
      // Arrange
      const sendValue = ethers.utils.parseEther('1');
      await gravityGrade
        .connect(alice_moderator)
        .setAllowedPaymentCurrencies(saleId, [constants.ZERO_ADDRESS]);
      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, constants.ZERO_ADDRESS, {
          value: sendValue,
        });
      const startingGGBalance = await gravityGrade.provider.getBalance(gravityGrade.address);
      console.log(`Starting GG balance: ${startingGGBalance}`);
      const startingModeratorBalance = await gravityGrade.provider.getBalance(
        await alice_moderator.getAddress(),
      );
      // Act
      await gravityGrade.withdraw(await alice_moderator.getAddress(), constants.ZERO_ADDRESS);

      const endingGGBalance = await gravityGrade.provider.getBalance(gravityGrade.address);
      const endingModeratorBalance = await gravityGrade.provider.getBalance(
        await alice_moderator.getAddress(),
      );
      // Assert
      expect(BigNumber.from(0)).to.equal(endingGGBalance);
      assert.equal(
        startingGGBalance.add(startingModeratorBalance).toString(),
        endingModeratorBalance.toString(),
      );
    });
    it('reverts on non gov call', async function () {
      await expect(
        gravityGrade
          .connect(bob_player)
          .withdraw(await alice_moderator.getAddress(), constants.ZERO_ADDRESS),
      ).to.revertedWith('GravityGrade__NotGovernance');
    });
  });

  describe('packClaimable', function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 10;
    let bob_mint = 100;
    let erc721;
    let erc1155;
    beforeEach(async function () {
      saleId = await gravityGrade
          .connect(alice_moderator)
          .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
          .connect(alice_moderator)
          .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
      await gravityGrade.connect(alice_moderator).setPaperCurrency(maxCoin.address);

      let erc721fac = await ethers.getContractFactory('ERC721Mock');
      erc721 = await erc721fac.deploy('mock 721', 'm721');

      let erc1155Fac = await ethers.getContractFactory('ERC1155Mock');
      erc1155 = await erc1155Fac.deploy('some uri');


    });
    it('Says that Bob can buy the pack', async function () {
      expect(await gravityGrade.packClaimable(await bob_player.getAddress(),
          constants.ZERO_ADDRESS, 0, 1, saleId
      )).to.equal(true);
    });

    it('Says that Bob can buy the pack if he owns the erc721 for rebate', async function () {
      await erc721.mint(await bob_player.getAddress(), 1);
      await expect(
          gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
            tokenType: 0, // ERC 721
            tokenAddress: erc721.address,
            tokenId: 0,
            basisPoints: BigNumber.from(1000),
          }),
      ).to.not.reverted;
      expect(await gravityGrade.packClaimable(await bob_player.getAddress(),
          erc721.address, 1, 1, saleId
      )).to.equal(true);
    });

    it('Says that Bob can not buy the pack if he does not owns the erc721 for rebate', async function () {
      await expect(
          gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
            tokenType: 0, // ERC 721
            tokenAddress: erc721.address,
            tokenId: 0,
            basisPoints: BigNumber.from(1000),
          }),
      ).to.not.reverted;
      await erc721.mint(await alice_moderator.getAddress(), 1);
      expect(await gravityGrade.packClaimable(await bob_player.getAddress(),
          erc721.address, 1, 1, saleId
      )).to.equal(false);
    });

    it('Says that Bob can buy the pack if he owns the erc1155 for rebate', async function () {
      await erc1155.mint(await bob_player.getAddress(), 1,1);
      await expect(
          gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
            tokenType: 1, // ERC 1155
            tokenAddress: erc1155.address,
            tokenId: 1,
            basisPoints: BigNumber.from(1000),
          }),
      ).to.not.reverted;
      expect(await gravityGrade.packClaimable(await bob_player.getAddress(),
          erc1155.address, 1, 1, saleId
      )).to.equal(true);
    });

    it('Says that Bob can not buy the pack if he does not owns the erc1155 for rebate', async function () {
      await expect(
          gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
            tokenType: 1, // ERC 1155
            tokenAddress: erc1155.address,
            tokenId: 1,
            basisPoints: BigNumber.from(1000),
          }),
      ).to.not.reverted;
      expect(await gravityGrade.packClaimable(await bob_player.getAddress(),
          erc1155.address, 1, 1, saleId
      )).to.equal(false);
    });

    it('Says that Bob can not exceed the player cap', async function () {
      expect(await gravityGrade.packClaimable(await bob_player.getAddress(),
          constants.ZERO_ADDRESS, 0, playerCap+1, saleId
      )).to.equal(false);
    });

  });

  describe('packClaimable', function () {
    let saleId;
    let price = 10;
    let tokenId = 1;
    let cap = 10;
    let playerCap = 10;
    let bob_mint = 100;
    let erc721;
    let erc1155;
    beforeEach(async function () {
      saleId = await gravityGrade
          .connect(alice_moderator)
          .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
          .connect(alice_moderator)
          .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await shazCoin.mint(await bob_player.getAddress(), bob_mint);
      await gravityGrade.connect(alice_moderator).setPaperCurrency(maxCoin.address);

      let erc721fac = await ethers.getContractFactory('ERC721Mock');
      erc721 = await erc721fac.deploy('mock 721', 'm721');

      let erc1155Fac = await ethers.getContractFactory('ERC1155Mock');
      erc1155 = await erc1155Fac.deploy('some uri');

      await gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
            tokenType: 0, // ERC 721
            tokenAddress: erc721.address,
            tokenId: 0,
            basisPoints: BigNumber.from(1000),
          })

      await gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
        tokenType: 1, // ERC 1155
        tokenAddress: erc1155.address,
        tokenId: 1,
        basisPoints: BigNumber.from(2000),
      })

      await erc721.mint(await bob_player.getAddress(), 1);


    });
    it('Gives Bob the correct price', async function () {
      expect(await gravityGrade.callStatic.checkPrice(constants.ZERO_ADDRESS,
          0, 5, saleId
      )).to.equal(5*price);
    });

    it('Gives Bob the correct price for erc721 rebate', async function () {
      expect(await gravityGrade.callStatic.checkPrice(erc721.address,
          0, 1, saleId
      )).to.equal(9);
    });
    it('Gives Bob the correct price for erc1155 rebate', async function () {
      expect(await gravityGrade.callStatic.checkPrice(erc1155.address,
          1, 1, saleId
      )).to.equal(8);
    });

    it('Does not confuse erc1155 rebates', async function () {
      await gravityGrade.connect(alice_moderator).addOwnershipDiscount(saleId, {
        tokenType: 1, // ERC 1155
        tokenAddress: erc1155.address,
        tokenId: 2,
        basisPoints: BigNumber.from(3000),
      })
      expect(await gravityGrade.callStatic.checkPrice(erc1155.address,
          1, 1, saleId
      )).to.equal(8);
      expect(await gravityGrade.callStatic.checkPrice(erc1155.address,
          2, 1, saleId
      )).to.equal(7);
    });


  });

  describe('Compound & misc. tests', function () {
    // Tests for things that combine multiple functions or don't fit into previous categories

    it('Should update the price correctly when modifying a sale', async function () {
      let saleId;
      let price = 10;
      let tokenId = 1;
      let cap = 10;
      let playerCap = 10;
      let bob_mint = 100;
      saleId = await gravityGrade
        .connect(alice_moderator)
        .callStatic.createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade
        .connect(alice_moderator)
        .createNewSale(tokenId, price, cap, playerCap, maxCoin.address, true);
      await gravityGrade.connect(alice_moderator).setSaleState(saleId, false);
      await maxCoin.mint(await bob_player.getAddress(), bob_mint);
      await maxCoin.connect(bob_player).approve(gravityGrade.address, 100);

      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(bob_mint - price);
      let newPrice = 3 * price;

      await gravityGrade
        .connect(alice_moderator)
        .modifySale(saleId, newPrice, cap, playerCap, maxCoin.address, true);

      await gravityGrade
        .connect(bob_player)
        .buyPacks(saleId, 1, 0, constants.ZERO_ADDRESS, maxCoin.address);
      expect(await maxCoin.balanceOf(await bob_player.getAddress())).to.equal(
        bob_mint - (price + newPrice),
      );
    });

    it('Should remove maxCoin as a whitelisted currency', async function () {
      expect(
        await gravityGrade
          .connect(alice_moderator)
          .setAllowedPaymentCurrencies(1, [maxCoin.address, constants.ZERO_ADDRESS]),
      )
        .to.emit(gravityGrade, 'PaymentCurrenciesSet')
        .withArgs(1, [maxCoin.address, constants.ZERO_ADDRESS]);

      expect(
        await gravityGrade.connect(alice_moderator).removePaymentCurrencies(1, [maxCoin.address]),
      )
        .to.emit(gravityGrade, 'PaymentCurrenciesRevoked')
        .withArgs(1, [maxCoin.address]);
    });
  });
});
