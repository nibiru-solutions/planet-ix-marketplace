import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { BigNumber } from 'ethers';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { EnergyIXTStaking } from '../src/types/EnergyIXTStaking';
import { ERC20Mock } from '../src/types/ERC20Mock';
import { ERC1155MockAssetManager } from '../src/types/ERC1155MockAssetManager';
import { intToBuffer } from 'ethereumjs-util';

describe('ENERGY/IXT STAKING', function () {
  let owner: SignerWithAddress;
  let moderator: SignerWithAddress;
  let alice: SignerWithAddress;
  let lisa: SignerWithAddress;
  let bob: SignerWithAddress;
  let allen_donor: SignerWithAddress;
  let amelia_foundation: SignerWithAddress;
  let energyStaking: EnergyIXTStaking;
  let ixtMock: ERC20Mock;
  let assetManagerMock: ERC1155MockAssetManager;
  const initial_donor_bal = ethers.utils.parseEther('100000');
  const pushRewardNextEpochPercents = [
    2500, 2000, 1500, 1000, 500, 500, 500, 500, 400, 200, 200, 200,
  ];

  beforeEach(async function () {
    [owner, moderator, alice, bob, allen_donor, amelia_foundation, lisa] =
      await ethers.getSigners();

    const ixtFactory = await ethers.getContractFactory('ERC20Mock');
    ixtMock = await ixtFactory.deploy('Planet IX Token', 'IXT', owner.address, 50);
    ixtMock = await ixtFactory.deploy('Reward Token', 'RWD', owner.address, 50);

    const assetManagerFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    assetManagerMock = await assetManagerFactory.deploy('TOY URI');

    const energyStakingFactory = await ethers.getContractFactory('EnergyIXTStaking');
    energyStaking = (await upgrades.deployProxy(energyStakingFactory, [
      ixtMock.address,
      assetManagerMock.address,
    ])) as EnergyIXTStaking;

    await energyStaking.setModerator(owner.address, true);

    await energyStaking.setAmeliaFoundationWallet(amelia_foundation.address);
    await energyStaking.setPushRewardNextEpochsPercentages(pushRewardNextEpochPercents);
    await energyStaking.setRewardDistributor(allen_donor.address);
    await energyStaking.setRewardPercentageDenominator(10000);

    await ixtMock.mint(allen_donor.address, initial_donor_bal);
    await ixtMock.connect(allen_donor).approve(energyStaking.address, initial_donor_bal);

    await assetManagerMock.setTrusted(energyStaking.address, true);
  });

  describe('add rewards', () => {
    let rewardAmount = ethers.utils.parseEther('10000');
    let month = 2_592_000;

    let init_energy_bal = 10000;
    let init_ixt_bal = ethers.utils.parseEther('10000');

    const sevenDays = 7 * 24 * 60 * 60;
    const day = 24 * 60 * 60;
    it('facility calculation gives expected results', async () => {
      let poolShare = await energyStaking.callStatic._facilityCalculation(45000);
      console.log(poolShare);
    });
    it('should add rewards', async () => {
      let tx = await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);

      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      let totalLocked = rewardAmount.sub(ethers.utils.parseEther(poolShare.toString()));

      expect(tx).to.emit(energyStaking, 'RewardAdded').withArgs(rewardAmount, month);
      let donor_bal = await ixtMock.balanceOf(allen_donor.address);
      let contract_bal = await ixtMock.balanceOf(energyStaking.address);

      expect(donor_bal).to.equal(initial_donor_bal.sub(rewardAmount));
      expect(contract_bal).to.equal(rewardAmount);

      for (let i = 1; i < 13; i++) {
        let rewardsPushedToNextEpochs = await energyStaking.getPushedRewards(i + 1);
        let percentRewardsThisEpoch = pushRewardNextEpochPercents[i - 1];
        let rewardsThisEpoch = totalLocked.mul(percentRewardsThisEpoch).div(10000);
        expect(rewardsPushedToNextEpochs).to.equal(rewardsThisEpoch);
      }

      //await printPoolStats();
    });

    it('stake before rewards added', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      let tx = await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);

      let energy_bal = await assetManagerMock.balanceOf(bob.address, 24);
      let ixt_bal = await ixtMock.balanceOf(bob.address);

      expect(energy_bal).to.equal(init_energy_bal - 10);
      expect(ixt_bal).to.equal(init_ixt_bal.sub(ethers.utils.parseEther('10')));

      let energy_bal_contract = await assetManagerMock.balanceOf(energyStaking.address, 24);
      let ixt_bal_contract = await ixtMock.balanceOf(energyStaking.address);

      expect(ixt_bal_contract).to.equal(ethers.utils.parseEther('10').add(rewardAmount));

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      console.log(`poolshare: ${poolShare}`);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.equal(0);
      expect(totalStaked).to.equal(0);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(0);
      expect(userAmeliaStakedEnergy).to.equal(0);

      await printPoolStats();
    });

    it('rewards carry over into next epoch', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);

      console.log('------ADD EPOCH------START OF JANUARY');
      await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      console.log('------STAKE------START OF JANUARY');
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await increaseTime(month);

      console.log('------ADD EPOCH------START OF FEBRUARY');
      await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      console.log('------STAKE------START OF FEBRUARY');

      await energyStaking.connect(bob).stakeEnergy(10, false);

      await increaseTime(month);

      console.log('------CLAIM------END OF FEBRUARY');

      await energyStaking.connect(bob).claim(false);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal)}`);

      await printPoolStats();
    });
    it('staked in first epoch but not second', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);

      console.log('------ADD EPOCH------START OF JANUARY');
      await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      console.log('------STAKE------START OF JANUARY');
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await increaseTime(month);

      console.log('------ADD EPOCH------START OF FEBRUARY');
      await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      console.log('------STAKE------START OF FEBRUARY');
      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, false);

      await increaseTime(month);

      console.log('------CLAIM------END OF FEBRUARY');

      await energyStaking.connect(bob).claim(false);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal)}`);

      await printPoolStats();
    });
    it('staked in second epoch but not first', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);

      console.log('------ADD EPOCH------START OF JANUARY');
      await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      console.log('------STAKE------START OF JANUARY');

      await increaseTime(month);

      console.log('------ADD EPOCH------START OF FEBRUARY');
      await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      console.log('------STAKE------START OF FEBRUARY');

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await increaseTime(month);

      console.log('------CLAIM------END OF FEBRUARY');

      await energyStaking.connect(bob).claim(false);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal)}`);

      await printPoolStats();
    });

    it('stake does not carry over into next epoch', async () => {});

    it('new epoch resets pool', async () => {});

    it('should push rewards to the next epochs if not enough facilities exist', async function () {
      let tx = await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      let totalLocked = rewardAmount.sub(ethers.utils.parseEther(poolShare.toString()));

      console.log(`Pool Share: ${poolShare}`);

      for (let i = 1; i < 13; i++) {
        let rewardsPushedToNextEpochs = await energyStaking.getPushedRewards(i + 1);
        let percentRewardsThisEpoch = pushRewardNextEpochPercents[i - 1];
        let rewardsThisEpoch = totalLocked.mul(percentRewardsThisEpoch).div(10000);
        expect(rewardsPushedToNextEpochs).to.equal(rewardsThisEpoch);
      }

      await testPlowing(totalLocked);
    });

    it('should revert if previous epoch not finished', async function () {
      let tx = await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      await expect(
        energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month),
      ).to.revertedWith('ENERGY STAKING: EPOCH NOT YET FINISHED');
    });

    it('should add second epoch after endTime pass', async function () {
      let tx = await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);

      await ethers.provider.send('evm_increaseTime', [month * 1.5]);
      await ethers.provider.send('evm_mine', []);

      await expect(energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month))
        .to.not.reverted;
    });
  });

  describe('setRewardDistributor', () => {
    it('it should set reward distributor correctly', async () => {
      await expect(energyStaking.setRewardDistributor(allen_donor.address)).to.not.reverted;
    });

    it('it should revert if the caller is not the donor', async () => {
      await expect(energyStaking.connect(bob).setRewardDistributor(bob.address)).to.reverted;
    });

    it('it should revert if the distributor address is zero address', async () => {
      await expect(energyStaking.setRewardDistributor(ethers.constants.AddressZero)).to.reverted;
    });
  });

  describe('stake and unstake', () => {
    let init_energy_bal = 10000;
    let init_ixt_bal = ethers.utils.parseEther('10000');

    let rewardAmount = ethers.utils.parseEther('10000');
    let month = 2_592_000;

    const sevenDays = 7 * 24 * 60 * 60;
    const day = 24 * 60 * 60;

    beforeEach(async () => {
      let tx = await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      await energyStaking.setAmeliaFoundationShare(1000, 10000);
    });
    it('should revert if energy balance insufficient', async () => {
      await expect(energyStaking.connect(bob).stakeIXT(10)).to.reverted;
      await expect(energyStaking.connect(bob).stakeEnergy(10, false)).to.reverted;
    });

    it('should revert if ixt balance insufficient', async () => {
      await assetManagerMock.mint(bob.address, 24, 10000);
      await expect(energyStaking.connect(bob).stakeIXT(10)).to.reverted;
    });
    it('test user individual stake updates', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(15);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(20);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(30);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(40);

      expect((await energyStaking.userIXTStakes(bob.address, 0)).amount).to.equal(
        BigNumber.from(10),
      );
      expect((await energyStaking.userIXTStakes(bob.address, 1)).amount).to.equal(
        BigNumber.from(15),
      );
      expect((await energyStaking.userIXTStakes(bob.address, 2)).amount).to.equal(
        BigNumber.from(20),
      );
      expect((await energyStaking.userIXTStakes(bob.address, 3)).amount).to.equal(
        BigNumber.from(30),
      );
      expect((await energyStaking.userIXTStakes(bob.address, 4)).amount).to.equal(
        BigNumber.from(40),
      );

      let ixt_bal = await ixtMock.balanceOf(bob.address);

      expect(ixt_bal).to.equal(
        init_ixt_bal
          .sub(ethers.utils.parseEther('10'))
          .sub(ethers.utils.parseEther('15'))
          .sub(ethers.utils.parseEther('20'))
          .sub(ethers.utils.parseEther('30'))
          .sub(ethers.utils.parseEther('40')),
      );
    });

    it('should revert if trying to unstake before any of the stakes have passed their lockup period', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(15);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(20);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(30);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(40);

      await expect(energyStaking.connect(bob).unstakeIXT(10)).to.revertedWith(
        'ENERGY STAKING: NOT_ENOUGH_TOKENS_UNLOCKED',
      );
    });

    it('Testing gradual unlocking of user individual stakes A', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(15);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(20);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(30);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(40);

      const ixtbal = ethers.utils.formatEther(
        await (await ixtMock.balanceOf(bob.address)).toString(),
      );
      console.log(ixtbal);

      // console.log(
      //   '-----------------------------------------BEFORE ANY WITHDRAWALS-----------------------------------------',
      // );
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      let firstStake = await energyStaking.userIXTStakes(bob.address, 0);
      let firstDay = firstStake['stakedAt'].toNumber();

      await ethers.provider.send('evm_setNextBlockTimestamp', [firstDay + month]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).unstakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).unstakeIXT(15);

      // console.log(
      //   '-----------------------------------------AFTER WITHDRAWING THE FIRST 25 IN PARTS-----------------------------------------',
      // );
      // console.log(await energyStaking.getUserIXTStakes(bob.address));
      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(3);

      await ethers.provider.send('evm_increaseTime', [day * 80]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).unstakeIXT(80);

      // console.log(
      //   '-----------------------------------------AFTER WITHDRAWING 80-----------------------------------------',
      // );
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();
      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      console.log(`poolshare: ${poolShare}`);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(totalStaked))).to.equal(0);
      expect(ixtStaked).to.equal(10);
      expect(parseFloat(ethers.utils.formatEther(userStakedEnergy))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(userAmeliaStakedEnergy))).to.equal(0);

      // //await printPoolStats();

      const ixtbal2 = ethers.utils.formatEther(
        await (await ixtMock.balanceOf(bob.address)).toString(),
      );
      console.log(ixtbal2);
    });
    it('Testing gradual unlocking of user individual stakes B', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(15);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(20);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(30);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(40);

      // console.log(
      //   '-----------------------------------------BEFORE ANY WITHDRAWS-----------------------------------------',
      // );
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      let firstStake = await energyStaking.userIXTStakes(bob.address, 0);
      let firstDay = firstStake['stakedAt'].toNumber();

      await ethers.provider.send('evm_setNextBlockTimestamp', [firstDay + month]);
      await ethers.provider.send('evm_mine', []);
      // console.log(
      //   '-----------------------------------------WTHDRAW 5-----------------------------------------',
      // );
      await energyStaking.connect(bob).unstakeIXT(5);

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(5);
      expect(stakes[0].amount).to.equal(5);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();
      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      console.log(`poolshare: ${poolShare}`);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(totalStaked))).to.equal(0);
      expect(ixtStaked).to.equal(110);
      expect(parseFloat(ethers.utils.formatEther(userStakedEnergy))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(userAmeliaStakedEnergy))).to.equal(0);

      // //await printPoolStats();
    });
    it('Testing gradual unlocking of user individual stakes C', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(15);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(20);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(30);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(40);

      // console.log(
      //   '-----------------------------------------BEFORE ANY WITHDRAWS-----------------------------------------',
      // );
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      let firstStake = await energyStaking.userIXTStakes(bob.address, 0);
      let firstDay = firstStake['stakedAt'].toNumber();

      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);
      // console.log(
      //   '-----------------------------------------WTHDRAW ALL-----------------------------------------',
      // );
      await energyStaking.connect(bob).unstakeIXT(115);

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(0);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();
      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      console.log(`poolshare: ${poolShare}`);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(totalStaked))).to.equal(0);
      expect(ixtStaked).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(userStakedEnergy))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(userAmeliaStakedEnergy))).to.equal(0);

      // //await printPoolStats();
    });
    it('Testing gradual unlocking of user individual stakes D', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(15);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(20);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(30);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(40);

      // console.log(
      //   '-----------------------------------------BEFORE ANY WITHDRAWS-----------------------------------------',
      // );
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      let firstStake = await energyStaking.userIXTStakes(bob.address, 0);
      let firstDay = firstStake['stakedAt'].toNumber();

      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);
      // console.log(
      //   '-----------------------------------------WTHDRAW HALF-----------------------------------------',
      // );
      await energyStaking.connect(bob).unstakeIXT(60);

      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(2);
      expect(stakes[0].amount).to.equal(15);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();
      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      console.log(`poolshare: ${poolShare}`);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(totalStaked))).to.equal(0);
      expect(ixtStaked).to.equal(55);
      expect(parseFloat(ethers.utils.formatEther(userStakedEnergy))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(userAmeliaStakedEnergy))).to.equal(0);
    });
    it('Testing gradual unlocking of user individual stakes E', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(15);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(20);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(30);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(40);

      // console.log(
      //   '-----------------------------------------BEFORE ANY WITHDRAWS-----------------------------------------',
      // );
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      let firstStake = await energyStaking.userIXTStakes(bob.address, 0);
      let firstDay = firstStake['stakedAt'].toNumber();

      await ethers.provider.send('evm_increaseTime', [day * 26]);
      await ethers.provider.send('evm_mine', []);
      // console.log(
      //   '-----------------------------------------TRY TO WITHDRAW MORE THAN IS UNLOCKED-----------------------------------------',
      // );
      await expect(energyStaking.connect(bob).unstakeIXT(15)).to.reverted;

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(5);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();
      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      // console.log(`poolshare: ${poolShare}`);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(totalStaked))).to.equal(0);
      expect(ixtStaked).to.equal(115);
      expect(parseFloat(ethers.utils.formatEther(userStakedEnergy))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(userAmeliaStakedEnergy))).to.equal(0);
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      // //await printPoolStats();
    });
    it('Testing gradual unlocking of user individual stakes F', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(15);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(20);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(30);

      await ethers.provider.send('evm_increaseTime', [day]);
      await ethers.provider.send('evm_mine', []);
      await energyStaking.connect(bob).stakeIXT(40);

      // console.log(
      //   '-----------------------------------------BEFORE ANY WITHDRAWS-----------------------------------------',
      // );
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      let firstStake = await energyStaking.userIXTStakes(bob.address, 0);
      let firstDay = firstStake['stakedAt'].toNumber();

      await ethers.provider.send('evm_increaseTime', [day * 27]);
      await ethers.provider.send('evm_mine', []);
      // console.log(
      //   '-----------------------------------------TRY TO WITHDRAW MORE THAN IS UNLOCKED-----------------------------------------',
      // );
      await energyStaking.connect(bob).unstakeIXT(20);
      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(4);
      expect(stakes[0].amount).to.equal(5);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();
      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      console.log(`poolshare: ${poolShare}`);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(totalStaked))).to.equal(0);
      expect(ixtStaked).to.equal(95);
      expect(parseFloat(ethers.utils.formatEther(userStakedEnergy))).to.equal(0);
      expect(parseFloat(ethers.utils.formatEther(userAmeliaStakedEnergy))).to.equal(0);
      // console.log(await energyStaking.getUserIXTStakes(bob.address));

      // //await printPoolStats();
    });
    it('should stake', async function () {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      let energy_bal = await assetManagerMock.balanceOf(bob.address, 24);
      let ixt_bal = await ixtMock.balanceOf(bob.address);

      expect(energy_bal).to.equal(init_energy_bal - 10);
      expect(ixt_bal).to.equal(init_ixt_bal.sub(ethers.utils.parseEther('10')));

      let energy_bal_contract = await assetManagerMock.balanceOf(energyStaking.address, 24);
      let ixt_bal_contract = await ixtMock.balanceOf(energyStaking.address);

      expect(ixt_bal_contract).to.equal(ethers.utils.parseEther('10').add(rewardAmount));

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      console.log(`poolshare: ${poolShare}`);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.equal(0);
      expect(totalStaked).to.equal(10);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(10);
      expect(userAmeliaStakedEnergy).to.equal(0);
    });

    it('should unstake', async function () {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      const tx = await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await increaseTime(month);

      await energyStaking.connect(bob).claim(false);

      let reward_bal = await ixtMock.balanceOf(bob.address);

      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal)}`);

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(
        poolShare.toNumber() / 10,
        0.1,
      );
      expect(totalStaked).to.equal(10);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(10);
      expect(userAmeliaStakedEnergy).to.equal(0);
      // expect(ethers.utils.formatEther(reward_bal)).to.be.closeTo(poolShare.toNumber() + 10000, 1);
    });

    it('earned works as expected', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      const tx = await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [2 * day]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).stakeIXT(1);
      await energyStaking.connect(bob).stakeEnergy(1, false);

      const earned = await energyStaking.earned(bob.address, 1);
      console.log(`Earned tokens: ${ethers.utils.formatEther(earned)}`);

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(2);
      expect(stakes[0].amount).to.equal(10);
    });
    it('unstake reverts if amount = 0', async () => {
      await expect(energyStaking.connect(bob).unstakeIXT(0)).to.revertedWith(
        'ENERGY STAKING: CANT UNSTAKE 0',
      );
    });
    it("unstake reverts if user hasn't staked enough tokens", async () => {
      await assetManagerMock.mint(alice.address, 24, init_energy_bal);

      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);

      const tx = await energyStaking.connect(alice).stakeIXT(10);

      await expect(energyStaking.connect(bob).unstakeIXT(10)).to.revertedWith(
        'ENERGY STAKING: INSUFFICIENT AMOUNT STAKED',
      );
    });
    it('Staking to amelia foundation works', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      let energy_bal = await assetManagerMock.balanceOf(bob.address, 24);
      let ixt_bal = await ixtMock.balanceOf(bob.address);

      expect(energy_bal).to.equal(init_energy_bal - 10);
      expect(ixt_bal).to.equal(init_ixt_bal.sub(ethers.utils.parseEther('10')));

      let energy_bal_contract = await assetManagerMock.balanceOf(energyStaking.address, 24);
      let ixt_bal_contract = await ixtMock.balanceOf(energyStaking.address);

      expect(ixt_bal_contract).to.equal(ethers.utils.parseEther('10').add(rewardAmount));

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(0, 0);
      expect(totalStaked).to.equal(10);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(0);
      expect(userAmeliaStakedEnergy).to.equal(10);
      // expect(ethers.utils.formatEther(reward_bal)).to.be.closeTo(poolShare.toNumber() + 10000, 1);
    });
    it('claiming from amelia pool works and distributes rewards to amelia properly', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      const tx = await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await ethers.provider.send('evm_increaseTime', [month]);
      await ethers.provider.send('evm_mine', []);

      const tx2 = await energyStaking.connect(bob).unstakeIXT(10);
      await energyStaking.connect(bob).claim(true);

      let ixt_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal_amelia = await ixtMock.balanceOf(amelia_foundation.address);

      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);
      console.log(
        `Reward balance of Amelia foundation : ${ethers.utils.formatEther(
          reward_bal_amelia.toString(),
        )}`,
      );

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(0);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(
        poolShare.toNumber() / 10,
        0.1,
      );
      expect(totalStaked).to.equal(10);
      expect(ixtStaked).to.equal(0);
      expect(userStakedEnergy).to.equal(0);
      expect(userAmeliaStakedEnergy).to.equal(10);
      // expect(ethers.utils.formatEther(reward_bal)).to.be.closeTo(poolShare.toNumber() + 10000, 1);
    });
    it('several users stake', async () => {
      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, true);

      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await assetManagerMock.mint(allen_donor.address, 24, init_energy_bal);
      await ixtMock.mint(allen_donor.address, init_ixt_bal);
      await ixtMock.connect(allen_donor).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(allen_donor).stakeIXT(10);
      await energyStaking.connect(allen_donor).stakeEnergy(10, true);

      await ethers.provider.send('evm_increaseTime', [month]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(alice).stakeIXT(1);
      await energyStaking.connect(alice).stakeEnergy(1, true);

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(
        poolShare.toNumber() / 30,
        0.1,
      );
      expect(totalStaked).to.equal(31);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(0);
      expect(userAmeliaStakedEnergy).to.equal(10);
    });
    it('several users staking and claiming amelia pool', async () => {
      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, true);

      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await assetManagerMock.mint(allen_donor.address, 24, init_energy_bal);
      await ixtMock.mint(allen_donor.address, init_ixt_bal);
      await ixtMock.connect(allen_donor).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(allen_donor).stakeIXT(10);
      await energyStaking.connect(allen_donor).stakeEnergy(10, true);

      await ethers.provider.send('evm_increaseTime', [month]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(true);
      await energyStaking.connect(alice).claim(true);
      await energyStaking.connect(allen_donor).claim(true);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal_amelia = await ixtMock.balanceOf(amelia_foundation.address);

      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);
      console.log(
        `Amelia foundation balance: ${ethers.utils.formatEther(reward_bal_amelia.toString())}`,
      );

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(
        poolShare.toNumber() / 30,
        0.1,
      );
      expect(totalStaked).to.equal(30);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(0);
      expect(userAmeliaStakedEnergy).to.equal(10);
    });
    it('users staking and claiming from both ixt pool and amelia pool', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, true);

      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, false);

      await assetManagerMock.mint(lisa.address, 24, init_energy_bal);
      await ixtMock.mint(lisa.address, init_ixt_bal);
      await ixtMock.connect(lisa).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(lisa).stakeIXT(10);
      await energyStaking.connect(lisa).stakeEnergy(10, true);

      await energyStaking.connect(lisa).stakeIXT(10);
      await energyStaking.connect(lisa).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(true);
      await energyStaking.connect(alice).claim(true);
      await energyStaking.connect(lisa).claim(true);

      await energyStaking.connect(bob).claim(false);
      await energyStaking.connect(alice).claim(false);
      await energyStaking.connect(lisa).claim(false);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal_alice = await ixtMock.balanceOf(alice.address);
      let reward_bal_allen = await ixtMock.balanceOf(lisa.address);
      let reward_bal_amelia = await ixtMock.balanceOf(amelia_foundation.address);

      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);
      console.log(
        `Reward Balance of alice: ${ethers.utils.formatEther(reward_bal_alice.toString())}`,
      );
      console.log(
        `Reward Balance of allen: ${ethers.utils.formatEther(reward_bal_allen.toString())}`,
      );
      console.log(
        `Amelia foundation balance: ${ethers.utils.formatEther(reward_bal_amelia.toString())}`,
      );

      // //await printPoolStats();

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(2);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(
        poolShare.toNumber() / 60,
        0.1,
      );
      expect(totalStaked).to.equal(60);
      expect(ixtStaked).to.equal(20);
      expect(userStakedEnergy).to.equal(10);
      expect(userAmeliaStakedEnergy).to.equal(10);
    });
    it('users staking and unstaking from both ixt and amelia pool', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, true);

      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, false);

      await assetManagerMock.mint(lisa.address, 24, init_energy_bal);
      await ixtMock.mint(lisa.address, init_ixt_bal);
      await ixtMock.connect(lisa).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(lisa).stakeIXT(10);
      await energyStaking.connect(lisa).stakeEnergy(10, true);

      await energyStaking.connect(lisa).stakeIXT(10);
      await energyStaking.connect(lisa).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month]);
      await ethers.provider.send('evm_mine', []);

      // await energyStaking.connect(bob).unstakeIXT(10)
      await energyStaking.connect(alice).unstakeIXT(10);
      await energyStaking.connect(lisa).unstakeIXT(10);

      await energyStaking.connect(bob).unstakeIXT(10);
      // await energyStaking.connect(alice).unstakeIXT(10)
      await energyStaking.connect(lisa).unstakeIXT(10);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal_alice = await ixtMock.balanceOf(alice.address);
      let reward_bal_lisa = await ixtMock.balanceOf(lisa.address);
      let reward_bal_amelia = await ixtMock.balanceOf(amelia_foundation.address);

      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);
      console.log(
        `Reward Balance of alice: ${ethers.utils.formatEther(reward_bal_alice.toString())}`,
      );
      console.log(
        `Reward Balance of lisa: ${ethers.utils.formatEther(reward_bal_lisa.toString())}`,
      );
      console.log(
        `Amelia foundation balance: ${ethers.utils.formatEther(reward_bal_amelia.toString())}`,
      );

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      // expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(
      //   poolShare.toNumber() / 30,
      //   0.1,
      // );
      expect(totalStaked).to.equal(60);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(10);
      expect(userAmeliaStakedEnergy).to.equal(10);
    });
    it('user staking, unstaking and claiming from both ixt and amelia pools', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, true);

      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, false);

      await assetManagerMock.mint(lisa.address, 24, init_energy_bal);
      await ixtMock.mint(lisa.address, init_ixt_bal);
      await ixtMock.connect(lisa).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(lisa).stakeIXT(10);
      await energyStaking.connect(lisa).stakeEnergy(10, true);

      await energyStaking.connect(lisa).stakeIXT(10);
      await energyStaking.connect(lisa).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).unstakeIXT(10);
      // await energyStaking.connect(alice).stakeIXT(10);
      // await energyStaking.connect(alice).stakeEnergy(10, true);

      await energyStaking.connect(alice).claim(true);
      await energyStaking.connect(lisa).unstakeIXT(10);

      await energyStaking.connect(bob).claim(false);
      await energyStaking.connect(alice).unstakeIXT(10);
      await energyStaking.connect(lisa).unstakeIXT(10);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal_alice = await ixtMock.balanceOf(alice.address);
      let reward_bal_allen = await ixtMock.balanceOf(lisa.address);
      let reward_bal_amelia = await ixtMock.balanceOf(amelia_foundation.address);

      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);
      console.log(
        `Reward Balance of alice: ${ethers.utils.formatEther(reward_bal_alice.toString())}`,
      );
      console.log(
        `Reward Balance of allen: ${ethers.utils.formatEther(reward_bal_allen.toString())}`,
      );
      console.log(
        `Amelia foundation balance: ${ethers.utils.formatEther(reward_bal_amelia.toString())}`,
      );

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(
        poolShare.toNumber() / 60,
        0.1,
      );
      expect(totalStaked).to.equal(60);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(10);
      expect(userAmeliaStakedEnergy).to.equal(10);
    });
    it('should give correct reward when user partially unstakes', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await assetManagerMock.mint(alice.address, 24, init_energy_bal);
      await ixtMock.mint(alice.address, init_ixt_bal);
      await ixtMock.connect(alice).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(alice).stakeIXT(10);
      await energyStaking.connect(alice).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month / 2]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).unstakeIXT(5);

      await ethers.provider.send('evm_increaseTime', [month / 2]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).unstakeIXT(5);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal_amelia = await ixtMock.balanceOf(amelia_foundation.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);

      console.log(
        `Amelia foundation balance: ${ethers.utils.formatEther(reward_bal_amelia.toString())}`,
      );

      //await printPoolStats();
    });
    it('does not revert if user tries to claim after they already unstaked', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month / 2]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).unstakeIXT(10);
      await energyStaking.connect(bob).claim(false);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal_amelia = await ixtMock.balanceOf(amelia_foundation.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);

      //await printPoolStats();
    });
    it('lets user unstake after they already claimed', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month / 2]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);
      await energyStaking.connect(bob).unstakeIXT(10);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);

      //await printPoolStats();
    });
    it('reverts if user tries to unstake twice in a row', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month / 2]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);
      await energyStaking.connect(bob).unstakeIXT(10);
      await expect(energyStaking.connect(bob).unstakeIXT(10)).to.revertedWith(
        'ENERGY STAKING: INSUFFICIENT AMOUNT STAKED',
      );

      let reward_bal = await ixtMock.balanceOf(bob.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);
      //await printPoolStats();
    });
    it('lets user claim twice in a row with expected rewards', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month / 2]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);

      await increaseTime(month / 2);

      await energyStaking.connect(bob).claim(false);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);

      //await printPoolStats();
    });
    it('claiming gradually over different time intervals yield expected rewards', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month / 10]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);

      await ethers.provider.send('evm_increaseTime', [month / 10]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);

      await ethers.provider.send('evm_increaseTime', [month / 10]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);

      await ethers.provider.send('evm_increaseTime', [month / 10]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);

      await ethers.provider.send('evm_increaseTime', [month / 10]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);

      await ethers.provider.send('evm_increaseTime', [month / 10]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);

      await ethers.provider.send('evm_increaseTime', [month / 10]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(bob).claim(false);

      let reward_bal = await ixtMock.balanceOf(bob.address);
      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal.toString())}`);

      let stakes = await energyStaking.getUserIXTStakes(bob.address);
      expect(stakes.length).to.equal(1);
      expect(stakes[0].amount).to.equal(10);

      let rewardRate = await (await energyStaking.getCurrentRewardRate()).toString();
      let rewardPerToken = await energyStaking.getRewardPerToken();
      let totalStaked = await energyStaking.getTotalStakedAmount();

      let ixtStaked = await energyStaking.getUserTotalIXTStaked(bob.address);
      let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
      let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
      let lastUpdateTime = await energyStaking.getLastUpdateTime();
      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);

      expect(parseFloat(ethers.utils.formatEther(rewardRate))).to.equal(
        poolShare.toNumber() / month,
      );
      expect(parseFloat(ethers.utils.formatEther(rewardPerToken))).to.be.closeTo(
        ((poolShare.toNumber() / 10) * 7) / 10,
        0.1,
      );
      expect(totalStaked).to.equal(10);
      expect(ixtStaked).to.equal(10);
      expect(userStakedEnergy).to.equal(10);
      expect(userAmeliaStakedEnergy).to.equal(0);
    });
    it('reverts if user tries to claim from amelia pool when they have nothing staked there', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await expect(energyStaking.connect(bob).claim(true)).to.revertedWith(
        'ENERGY STAKING: INSUFFICIENT AMOUNT COMMITTED',
      );
    });
    it('reverts if user tries to claim from non donation ixt pool when they have nothing staked there', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await expect(energyStaking.connect(bob).claim(false)).to.revertedWith(
        'ENERGY STAKING: INSUFFICIENT AMOUNT STAKED',
      );
    });
    it('reverts if user tries to unstake more old ixt than they own', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await expect(energyStaking.connect(bob).unstakeOld(10, false)).to.revertedWith(
        'ENERGY STAKING: INSUFFICIENT AMOUNT STAKED',
      );
    });
    it('reverts if user tries to unstake more old amelia ixt than they own', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await expect(energyStaking.connect(bob).unstakeOld(10, true)).to.revertedWith(
        'ENERGY STAKING: INSUFFICIENT AMOUNT COMMITTED',
      );
    });
    it('reverts if user tries to unstake more new ixt than they own', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);
      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);
      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await increaseTime(month);

      await expect(energyStaking.connect(bob).unstakeIXT(21)).to.revertedWith(
        'ENERGY STAKING: INSUFFICIENT AMOUNT STAKED',
      );
    });
  });

  describe('claim rewards', () => {
    let init_energy_bal = 10000;
    let init_ixt_bal = ethers.utils.parseEther('10000');

    let rewardAmount = ethers.utils.parseEther('10000');
    let month = 2_592_000;

    const sevenDays = 7 * 24 * 60 * 60;
    const day = 24 * 60 * 60;

    beforeEach(async () => {
      let tx = await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);
      await energyStaking.setAmeliaFoundationShare(1000, 10000);
    });
    it('should claim correct amount', async function () {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await ethers.provider.send('evm_increaseTime', [month * 0.5]);
      await ethers.provider.send('evm_mine', []);

      const tx2 = await energyStaking.connect(bob).claim(false);

      let ixt_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal = await ixtMock.balanceOf(bob.address);

      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      let totalLocked = rewardAmount.sub(ethers.utils.parseEther(poolShare.toString()));

      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal)}`);

      // //await printPoolStats();

      // expect(ethers.utils.formatEther(reward_bal)).to.be.closeTo(poolShare.toNumber() + 10000, 1);
    });

    it('claiming from amelia pool works and rewards are proportioned properly', async () => {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      const tx = await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, true);

      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      const tx2 = await energyStaking.connect(bob).claim(true);

      let ixt_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal = await ixtMock.balanceOf(bob.address);
      let reward_bal_amelia = await ixtMock.balanceOf(amelia_foundation.address);

      let poolShare = await energyStaking.callStatic._facilityCalculation(10000);
      let totalLocked = rewardAmount.sub(ethers.utils.parseEther(poolShare.toString()));

      console.log(`Reward Balance of bob: ${ethers.utils.formatEther(reward_bal)}`);
      console.log(
        `Amelia foundation balance: ${ethers.utils.formatEther(reward_bal_amelia.toString())}`,
      );

      //await printPoolStats();
    });

    it('should revert if user has nothing staked', async function () {
      await assetManagerMock.mint(bob.address, 24, init_energy_bal);

      await ixtMock.mint(bob.address, init_ixt_bal);
      await ixtMock.connect(bob).approve(energyStaking.address, init_ixt_bal);

      await energyStaking.connect(bob).stakeIXT(10);
      await energyStaking.connect(bob).stakeEnergy(10, false);

      await expect(energyStaking.connect(alice).claim(false)).to.revertedWith(
        'ENERGY STAKING: INSUFFICIENT AMOUNT STAKED',
      );
    });

    it('Plowing to next epochs', async () => {
      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      //await printPoolStats();

      let tx = await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);

      //await printPoolStats();

      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      await energyStaking.connect(allen_donor).addCurrentEpochRewards(rewardAmount, month);

      //await printPoolStats();
    });
  });

  const testPlowing = async (totalLocked) => {
    for (let i = 1; i < 13; i++) {
      let rewardsPushedToNextEpochs = await energyStaking.getPushedRewards(i + 1);
      let percentRewardsThisEpoch = pushRewardNextEpochPercents[i - 1];
      let rewardsThisEpoch = totalLocked.mul(percentRewardsThisEpoch).div(10000);
      expect(rewardsPushedToNextEpochs).to.equal(rewardsThisEpoch);
    }
  };

  const increaseTime = async (time) => {
    await ethers.provider.send('evm_increaseTime', [time]);
    await ethers.provider.send('evm_mine', []);
  };

  const printPoolStats = async () => {
    let totalStaked = await energyStaking.getTotalStakedAmount();
    let userStakedEnergy = await energyStaking.userEnergyStaked(bob.address, 1);
    let userAmeliaStakedEnergy = await energyStaking.userEnergyStakedAmelia(bob.address, 1);
    let rewardPerToken = await energyStaking.getRewardPerToken();
    let lastUpdateTime = await energyStaking.getLastUpdateTime();
    let rewardRate = await energyStaking.getCurrentRewardRate();

    console.log(`Bob Staked Amount: ${userStakedEnergy}`);
    console.log(`Bob Amelia Commitment: ${userAmeliaStakedEnergy}`);
    console.log(`Total Energy Staked in ppol: ${totalStaked}`);

    console.log(`Reward Rate: ${ethers.utils.formatEther(rewardRate)}`);
    console.log(`Reward Per Token: ${ethers.utils.formatEther(rewardPerToken)}`);
    console.log(`Last Update Time: ${lastUpdateTime}`);
  };

  const printRewardsPushed = async () => {
    let rewardsPushToNextEpochs1 = await energyStaking.getPushedRewards(1);
    let rewardsPushToNextEpochs2 = await energyStaking.getPushedRewards(2);
    let rewardsPushToNextEpochs3 = await energyStaking.getPushedRewards(3);
    let rewardsPushToNextEpochs4 = await energyStaking.getPushedRewards(4);
    let rewardsPushToNextEpochs5 = await energyStaking.getPushedRewards(5);
    let rewardsPushToNextEpochs6 = await energyStaking.getPushedRewards(6);
    let rewardsPushToNextEpochs7 = await energyStaking.getPushedRewards(7);
    let rewardsPushToNextEpochs8 = await energyStaking.getPushedRewards(8);
    let rewardsPushToNextEpochs9 = await energyStaking.getPushedRewards(9);
    let rewardsPushToNextEpochs10 = await energyStaking.getPushedRewards(10);
    let rewardsPushToNextEpochs11 = await energyStaking.getPushedRewards(11);
    let rewardsPushToNextEpochs12 = await energyStaking.getPushedRewards(12);
    let rewardsPushToNextEpochs13 = await energyStaking.getPushedRewards(13);
    let rewardsPushToNextEpochs14 = await energyStaking.getPushedRewards(14);
    let rewardsPushToNextEpochs15 = await energyStaking.getPushedRewards(15);
    let rewardsPushToNextEpochs16 = await energyStaking.getPushedRewards(16);
    let rewardsPushToNextEpochs17 = await energyStaking.getPushedRewards(17);
    console.log(
      `Reward pushed to next epochs 1: ${ethers.utils.formatEther(rewardsPushToNextEpochs1)}`,
    );
    console.log(
      `Reward pushed to next epochs 2: ${ethers.utils.formatEther(rewardsPushToNextEpochs2)}`,
    );
    console.log(
      `Reward pushed to next epochs 3: ${ethers.utils.formatEther(rewardsPushToNextEpochs3)}`,
    );
    console.log(
      `Reward pushed to next epochs 4: ${ethers.utils.formatEther(rewardsPushToNextEpochs4)}`,
    );
    console.log(
      `Reward pushed to next epochs 5: ${ethers.utils.formatEther(rewardsPushToNextEpochs5)}`,
    );
    console.log(
      `Reward pushed to next epochs 6: ${ethers.utils.formatEther(rewardsPushToNextEpochs6)}`,
    );
    console.log(
      `Reward pushed to next epochs 7: ${ethers.utils.formatEther(rewardsPushToNextEpochs7)}`,
    );
    console.log(
      `Reward pushed to next epochs 8: ${ethers.utils.formatEther(rewardsPushToNextEpochs8)}`,
    );
    console.log(
      `Reward pushed to next epochs 9: ${ethers.utils.formatEther(rewardsPushToNextEpochs9)}`,
    );
    console.log(
      `Reward pushed to next epochs 10: ${ethers.utils.formatEther(rewardsPushToNextEpochs10)}`,
    );
    console.log(
      `Reward pushed to next epochs 11: ${ethers.utils.formatEther(rewardsPushToNextEpochs11)}`,
    );
    console.log(
      `Reward pushed to next epochs 12: ${ethers.utils.formatEther(rewardsPushToNextEpochs12)}`,
    );
    console.log(
      `Reward pushed to next epochs 13: ${ethers.utils.formatEther(rewardsPushToNextEpochs13)}`,
    );
    console.log(
      `Reward pushed to next epochs 14: ${ethers.utils.formatEther(rewardsPushToNextEpochs14)}`,
    );
    console.log(
      `Reward pushed to next epochs 15: ${ethers.utils.formatEther(rewardsPushToNextEpochs15)}`,
    );
    console.log(
      `Reward pushed to next epochs 16: ${ethers.utils.formatEther(rewardsPushToNextEpochs16)}`,
    );
    console.log(
      `Reward pushed to next epochs 17: ${ethers.utils.formatEther(rewardsPushToNextEpochs17)}`,
    );
  };
});
