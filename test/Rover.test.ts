import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Contract, constants, utils } from 'ethers';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { parseEther } from 'ethers/lib/utils';

describe('Rover', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let missionControl: SignerWithAddress;
  let rover: Contract;
  let roverUri = 'ipfs://QmTXCZAqyC3TAomvmdSFP8EC9Tn8GeNSnsB62YfXuFvPWf';
  let oracle: Contract;
  let usdt: Contract;
  let ixt: Contract;
  let wmatic: Contract;
  let astroCredits: Contract;
  let acTokenId = 0;
  let vrfCoordinatorV2Mock: Contract;
  let keyHash = utils.defaultAbiCoder.encode(['uint256'], [1]);
  let callbackGasLimit = 2000000;
  let requestConfirmations = 3;
  let subscriptionId = 1;

  let ixtNamePrice = ethers.utils.parseEther('25');
  let acNamePrice = ethers.utils.parseEther('1000');

  let ixtRepairWornPrice = ethers.utils.parseEther('1');
  let ixtRepairDamagedPrice = ethers.utils.parseEther('2.5');
  let acRepairWornPrice = ethers.utils.parseEther('30');
  let acRepairDamagedPrice = ethers.utils.parseEther('80');

  let purchaseUSDTprice = 10_000_000; // 10 usdt, it has 6 decimals

  beforeEach(async function () {
    [owner, alice, missionControl] = await ethers.getSigners();

    let oracleFactory = await ethers.getContractFactory('OracleManagerMock');
    oracle = await oracleFactory.deploy();

    const VRFCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
    vrfCoordinatorV2Mock = await VRFCoordinatorV2MockFactory.deploy(0, 6);
    await vrfCoordinatorV2Mock.createSubscription();
    await vrfCoordinatorV2Mock.fundSubscription(subscriptionId, utils.parseEther('10'));

    const USDTFactory = await ethers.getContractFactory('MockToken');
    usdt = await USDTFactory.deploy('USDT', 'USDT', 6);

    const IXTFactory = await ethers.getContractFactory('MockToken');
    ixt = await IXTFactory.deploy('IXT', 'IXT', 18);

    const WMATICFactory = await ethers.getContractFactory('MockToken');
    wmatic = await WMATICFactory.deploy('WMATIC', 'WMATIC', 18);

    const ACFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    astroCredits = await ACFactory.deploy('uri');

    const RoverHelerFactory = await ethers.getContractFactory('RoverHelper');
    const roverHelper = await RoverHelerFactory.deploy();

    const RoverFactory = await ethers.getContractFactory('Rover');
    rover = await upgrades.deployProxy(RoverFactory, [
      roverUri,
      missionControl.address,
      oracle.address,
      vrfCoordinatorV2Mock.address,
      usdt.address,
      ixt.address,
      // wmatic.address,
      astroCredits.address,
      acTokenId,
    ]);
    await rover.setSaleOpen(true);
    await rover.setKeyHash(keyHash);
    await rover.setCallbackGasLimit(callbackGasLimit);
    await rover.setRequestConfirmations(requestConfirmations);
    await rover.setSubscriptionId(subscriptionId);
    await vrfCoordinatorV2Mock.addConsumer(subscriptionId, rover.address);

    await rover.setRoverHelper(roverHelper.address);

    await usdt.approve(rover.address, constants.MaxUint256);
    await ixt.approve(rover.address, constants.MaxUint256);
    await wmatic.approve(rover.address, constants.MaxUint256);
    await astroCredits.mint(owner.address, acTokenId, parseEther('1000'));
    await astroCredits.setApprovalForAll(rover.address, true);
    // await astroCredits.mint(alice.address, acTokenId, parseEther('1000'));
    await astroCredits.connect(alice).setApprovalForAll(rover.address, true);

    await usdt.connect(alice).approve(rover.address, constants.MaxUint256);
    await ixt.connect(alice).approve(rover.address, constants.MaxUint256);
    await wmatic.connect(alice).approve(rover.address, constants.MaxUint256);

    // set prices
    const PayableToken = {
      IXT: 0,
      AstroCredits: 1,
      USDT: 2,
      Matic: 3,
    };

    const Status = {
      Pristine: 0,
      Worn: 1,
      Damaged: 2,
      Wrecked: 3,
    };

    await rover.setNamePrice(PayableToken.IXT, ixtNamePrice);
    await rover.setNamePrice(PayableToken.AstroCredits, acNamePrice);
  
    await rover.setRepairPrice(PayableToken.IXT, Status.Worn, ixtRepairWornPrice);
    await rover.setRepairPrice(PayableToken.IXT, Status.Damaged, ixtRepairDamagedPrice);
    await rover.setRepairPrice(PayableToken.AstroCredits, Status.Worn, acRepairWornPrice);
    await rover.setRepairPrice(PayableToken.AstroCredits, Status.Damaged, acRepairDamagedPrice);
  
    await rover.setPurchaseUSDTprice(purchaseUSDTprice);
  });

  describe('#initialize', () => {
    it('check initial values', async function () {
      expect(await rover.name()).equal('Rover');
      expect(await rover.symbol()).equal('ROVER');
      expect(await rover.uris(0)).equal(roverUri);
    });
  });

  describe('vrf request', () => {
    beforeEach(async () => {
      await oracle.setRatio(usdt.address, constants.AddressZero, 1e12, 1);
      await oracle.setRatio(constants.AddressZero, ixt.address, 1, 1);
    });

    it('revert when sale closed', async () => {
      await rover.setSaleOpen(false);
      await expect(rover.purchaseWithUSDT(1)).revertedWith('Rover__SaleNotOpen');
    });

    it('purchase and mint with USDT', async () => {
      const previousBalance = await usdt.balanceOf(await owner.getAddress());
      await rover.connect(owner).purchaseWithUSDT(1);
      const requestId = 1;
      expect(await rover.s_requests(requestId)).equal(await owner.getAddress());

      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });
      expect(await rover.balanceOf(await owner.getAddress())).equal(1);
      const currentBalance = await usdt.balanceOf(await owner.getAddress());
      expect(currentBalance).equal(previousBalance.sub(purchaseUSDTprice));
    });

    it('purchase with IXT', async () => {
      const previousBalance = await ixt.balanceOf(await owner.getAddress());

      await rover.purchaseWithIXT(1);

      const currentBalance = await ixt.balanceOf(await owner.getAddress());
      const difference = previousBalance.sub(currentBalance);

      expect(difference).equal(utils.parseEther('10'));
    });

    it('purchase with MATIC', async () => {
      const provider = ethers.provider;
      const previousBalance = await provider.getBalance(await owner.getAddress());

      await rover.purchaseWithMatic(1, { value: utils.parseEther('10') });

      const currentBalance = await provider.getBalance(await owner.getAddress());
      const difference = previousBalance.sub(currentBalance);

      // account for gas costs
      expect(difference.sub(utils.parseEther('10')).abs()).lt(utils.parseEther('0.001'));
    });

    it('monte carlo purchase', async () => {
      let piercer = 0;
      let genesis = 0;
      let night = 0;
      for (let i = 0; i < 1000; i++) {
        let reqId = await rover.callStatic.purchaseWithUSDT(1);
        await rover.purchaseWithUSDT(1);
        let tx = await vrfCoordinatorV2Mock.fulfillRandomWords(reqId, rover.address, {
          gasLimit: callbackGasLimit,
        });
        let roverInfo = await rover.rovers(i);
        if (roverInfo['color'] == 2) piercer++;
        if (roverInfo['color'] == 1) genesis++;
        if (roverInfo['color'] == 0) night++;
      }

      console.log('piercer count: ', piercer);
      console.log('genesis count: ', genesis);
      console.log('night count: ', night);
    });

    it('cannot purchase with too little MATIC', async () => {
      const provider = ethers.provider;
      const previousBalance = await provider.getBalance(await owner.getAddress());

      await expect(rover.purchaseWithMatic(1, { value: utils.parseEther('9') })).to.be.revertedWith('Rover__PaymentTooLow');
    });
  });

  describe('update uris', () => {
    it('revert if msg.sender is not owner', async () => {
      await expect(rover.connect(alice).addUri('new_uri')).to.be.revertedWith('Ownable: caller is not the owner');
    });

    it('should set base uri by owner', async () => {
      const uri = 'https://planetix.com/rover-nfts/white/christmas';
      await rover.addUri(uri);
      expect(await rover.uris(1)).equal(uri);
    });
  });

  describe('set new skin on rover', () => {
    it('revert if not an available skin', async () => {
      await rover.purchaseWithUSDT(1);
      const requestId = 1;
      expect(await rover.s_requests(requestId)).equal(await owner.getAddress());
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });
      await expect(rover.upgradeSkinIXT(0, 11)).to.be.revertedWith('Rover__InvalidSkin');
    });

    it('revert if cannot afford', async () => {
      await rover.addSkin('ipfs://new_skin', utils.parseEther('12'), utils.parseEther('15'));
      await usdt.connect(owner).transfer(alice.address, purchaseUSDTprice);
      await rover.connect(alice).purchaseWithUSDT(1);
      const requestId = 1;
      expect(await rover.s_requests(requestId)).equal(alice.address);
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });
      await expect(rover.connect(alice).upgradeSkinIXT(0, 1)).to.be.revertedWith('ERC20: transfer amount exceeds balance');
    });

    it('update skin ixt', async () => {
      await rover.purchaseWithUSDT(1);
      const requestId = 1;
      expect(await rover.s_requests(requestId)).equal(await owner.getAddress());
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });
      await rover.addSkin('ipfs://new_skin', utils.parseEther('12'), utils.parseEther('15'));

      const previousBalance = await ixt.balanceOf(await owner.getAddress());
      await rover.upgradeSkinIXT(0, 1);
      const roverInstance = await rover.rovers(0);
      expect(roverInstance.skin).equal(1);
      const currentBalance = await ixt.balanceOf(await owner.getAddress());
      expect(currentBalance).equal(previousBalance.sub(utils.parseEther('12')));
    });

    it('update skin ac', async () => {
      await rover.purchaseWithUSDT(1);
      const requestId = 1;
      expect(await rover.s_requests(requestId)).equal(await owner.getAddress());
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });
      await rover.addSkin('ipfs://new_skin', utils.parseEther('12'), utils.parseEther('15'));

      const previousBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      await rover.upgradeSkinAC(0, 1);
      const roverInstance = await rover.rovers(0);
      expect(roverInstance.skin).equal(1);
      const currentBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      expect(currentBalance).equal(previousBalance.sub(utils.parseEther('15')));
    });
  });

  describe('set name', () => {
    const newName = 'new name';

    it('revert if cannot afford', async () => {
      await usdt.connect(owner).transfer(alice.address, 10_000_000);
      await rover.connect(alice).purchaseWithUSDT(1);
      const requestId = 1;
      expect(await rover.s_requests(requestId)).equal(await alice.getAddress());
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });

      await expect(rover.connect(alice).setNameWithIXT(0, newName)).to.be.revertedWith('ERC20: transfer amount exceeds balance');
      await expect(rover.connect(alice).setNameWithAC(0, newName)).to.be.revertedWith('ERC1155: insufficient balance for transfer');
    });

    it('set name ixt', async () => {
      await rover.purchaseWithUSDT(1);
      const requestId = 1;
      expect(await rover.s_requests(requestId)).equal(await owner.getAddress());
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });

      const previousBalance = await ixt.balanceOf(await owner.getAddress());
      await rover.setNameWithIXT(0, newName);
      const roverInstance = await rover.rovers(0);
      expect(roverInstance.name).equal(newName);
      const currentBalance = await ixt.balanceOf(await owner.getAddress());
      expect(currentBalance).equal(previousBalance.sub(utils.parseEther('25')));
    });

    it('set name ac', async () => {
      await rover.purchaseWithUSDT(1);
      const requestId = 1;
      expect(await rover.s_requests(requestId)).equal(await owner.getAddress());
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });

      const previousBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      await rover.setNameWithAC(0, newName);
      const roverInstance = await rover.rovers(0);
      expect(roverInstance.name).equal(newName);
      const currentBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      expect(currentBalance).equal(previousBalance.sub(utils.parseEther('1000')));
    });
  });

  describe('break down and repair', () => {
    beforeEach(async () => {
      await rover.purchaseWithUSDT(1);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, rover.address, {
        gasLimit: callbackGasLimit,
      });
    });

    it('repairWornIXT', async () => {
      await rover.connect(missionControl)._breakdown(0, 1);
      let roverInstance = await rover.rovers(0);
      expect(roverInstance.status).equal(1);

      const previousBalance = await ixt.balanceOf(await owner.getAddress());
      await rover.repairBatchIXT([0]);
      roverInstance = await rover.rovers(0);
      expect(roverInstance.status).equal(0);
      const currentBalance = await ixt.balanceOf(await owner.getAddress());
      expect(currentBalance).equal(previousBalance.sub(ixtRepairWornPrice));
    });

    it('repairDamagedIXT', async () => {
      await rover.connect(missionControl)._breakdown(0, 2);
      let roverInstance = await rover.rovers(0);
      expect(roverInstance.status).equal(2);

      const previousBalance = await ixt.balanceOf(await owner.getAddress());
      await rover.repairBatchIXT([0]);
      roverInstance = await rover.rovers(0);
      expect(roverInstance.status).equal(0);
      const currentBalance = await ixt.balanceOf(await owner.getAddress());
      expect(currentBalance).equal(previousBalance.sub(ixtRepairDamagedPrice));
    });

    it('repairWornAc', async () => {
      await rover.connect(missionControl)._breakdown(0, 1);
      let roverInstance = await rover.rovers(0);
      expect(roverInstance.status).equal(1);

      const previousBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      await rover.repairBatchAC([0]);
      roverInstance = await rover.rovers(0);
      expect(roverInstance.status).equal(0);
      const currentBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      expect(currentBalance).equal(previousBalance.sub(acRepairWornPrice));
    });

    it('repairDamagedAc', async () => {
      await rover.connect(missionControl)._breakdown(0, 2);
      let roverInstance = await rover.rovers(0);
      expect(roverInstance.status).equal(2);

      const previousBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      await rover.repairBatchAC([0]);
      roverInstance = await rover.rovers(0);
      expect(roverInstance.status).equal(0);
      const currentBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      expect(currentBalance).equal(previousBalance.sub(acRepairDamagedPrice));
    });
  });

  describe('dynamic metadata', () => {
    const newName = 'new name';

    beforeEach(async () => {
      await rover.purchaseWithUSDT(1);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, rover.address, {
        gasLimit: callbackGasLimit,
      });
    });

    it('get dynamic metadata for name', async () => {
      let uri = await rover.tokenURI(0);
      let metadata = JSON.parse(Buffer.from(uri.slice(29), 'base64').toString('ascii'));
      expect(metadata.name).equal('RVR Piercer');
      // console.log(metadata);

      let roverInstance = await rover.rovers(0);
      expect(roverInstance.color).equal(2);
      expect(roverInstance.status).equal(0);

      await rover.setNameWithAC(0, newName);
      uri = await rover.tokenURI(0);
      metadata = JSON.parse(Buffer.from(uri.slice(29), 'base64').toString('ascii'));
      expect(metadata.name).equal(newName);
      expect(metadata.image).equal(roverUri + '/0-2.gif');
      expect(metadata.animation_url).equal(roverUri + '/0-2.mp4');
    });

    it('dynamic metadata for skin', async () => {
      const newSkin = 'ipfs://new_skin';
      await rover.addSkin(newSkin, utils.parseEther('12'), utils.parseEther('15'));
      await rover.upgradeSkinIXT(0, 1);

      let uri = await rover.tokenURI(0);
      let metadata = JSON.parse(Buffer.from(uri.slice(29), 'base64').toString('ascii'));
      expect(metadata.image).equal(newSkin);
    });
  });

  describe('withdraw', () => {
    beforeEach(async () => {
      await oracle.setRatio(usdt.address, constants.AddressZero, 1, 1);
      await oracle.setRatio(constants.AddressZero, ixt.address, 1, 1);
    });

    it('withdraw usdt', async () => {
      await rover.purchaseWithUSDT(1);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, rover.address, {
        gasLimit: callbackGasLimit,
      });
      const previousBalance = await usdt.balanceOf(await owner.getAddress());
      await rover.withdraw();
      const currentBalance = await usdt.balanceOf(await owner.getAddress());
      expect(currentBalance).equal(previousBalance.add(utils.parseUnits('10', 6)));
    });

    it('withdraw matic', async () => {
      const provider = ethers.provider;
      await rover.purchaseWithMatic(1, { value: utils.parseEther('10') });
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, rover.address, {
        gasLimit: callbackGasLimit,
      });
      const previousBalance = await provider.getBalance(await owner.getAddress());
      await rover.withdraw();
      const currentBalance = await provider.getBalance(await owner.getAddress());
      expect(currentBalance.gt(previousBalance.add(utils.parseUnits('9.9')))).equal(true);
    });

    it('withdraw ixt', async () => {
      await rover.purchaseWithIXT(1);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, rover.address, {
        gasLimit: callbackGasLimit,
      });
      const previousBalance = await ixt.balanceOf(await owner.getAddress());
      await rover.withdraw();
      const currentBalance = await ixt.balanceOf(await owner.getAddress());
      expect(currentBalance).equal(previousBalance.add(purchaseUSDTprice));
    });

    it('withdraw astro credits', async () => {
      await rover.purchaseWithUSDT(1);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, rover.address, {
        gasLimit: callbackGasLimit,
      });
      await rover.setNameWithAC(0, 'name');
      const previousBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      await rover.withdraw();
      const currentBalance = await astroCredits.balanceOf(await owner.getAddress(), acTokenId);
      expect(currentBalance).equal(previousBalance.add(utils.parseUnits('1000')));
    });
  });

  describe('burn', () => {
    it('burn rover', async () => {
      await rover.purchaseWithUSDT(1);
      const requestId = 1;
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, rover.address, {
        gasLimit: callbackGasLimit,
      });

      expect(await rover.balanceOf(await owner.getAddress())).equal(1);
      await rover.burn(0);
      expect(await rover.balanceOf(await owner.getAddress())).equal(0);
    });
  });

  describe('trusted mint', () => {
    it('mints a rover', async () => {
      await rover.setTrusted(await alice.getAddress(), true);
      await rover.connect(alice).trustedMint(await alice.getAddress(), 1, 0);
      await rover.connect(alice).trustedMint(await alice.getAddress(), 3, 0);

      expect(await rover.balanceOf(await alice.getAddress())).equal(2);

      let roverInstance = await rover.rovers(0);
      expect(roverInstance.color).equal(0);
      roverInstance = await rover.rovers(1);
      expect(roverInstance.color).equal(2);
    });
    it('does not mint a rover', async () => {
      await expect(rover.connect(alice).trustedMint(await alice.getAddress(), 1, 1)).to.be.revertedWith('TM__NotTrusted');
    });
  });
});
