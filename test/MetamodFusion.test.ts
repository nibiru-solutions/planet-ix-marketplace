import {expect} from 'chai';
import {ethers, upgrades} from 'hardhat';
import {Contract, BigNumber, utils} from 'ethers';
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/signers";
import "hardhat-gas-reporter";
import {describe} from "mocha";
import { getCurrentTime, increaseTime } from './utils';

describe('Metamod Fusion tests', function () {
    let owner: SignerWithAddress;
    let alice: SignerWithAddress;
    let bob: SignerWithAddress;
    let assetManagerMock: Contract;
    let ixt:Contract;
    let metamodFusion: Contract;

    const M3TAM0D = 0;
    const ASTRO_CREDITS = 8;
    const ENERGY = 24;
    const BIOMOD_OUTLIER = 10;
    const BIOMOD_COMMON = 11;
    const BIOMOD_UNCOMMON = 12;
    const BIOMOD_RARE = 13;
    const BIOMOD_LEGENDARY = 14;

    let costTokenIds = [ASTRO_CREDITS, BIOMOD_OUTLIER, BIOMOD_COMMON, BIOMOD_UNCOMMON, BIOMOD_RARE, BIOMOD_LEGENDARY, ENERGY];
    let costAmounts = [3000, 75, 57, 38, 19, 8, 100];

    beforeEach(async function () {
        [owner, alice, bob] = await ethers.getSigners();

        let assetManagerFactory = await ethers.getContractFactory("ERC1155MockAssetManager");
        assetManagerMock = await assetManagerFactory.deploy("mockUri");
        await assetManagerMock.connect(alice).setTrusted(alice.address, true);

        const IXTFactory = await ethers.getContractFactory('ERC20Mock');
        ixt = await IXTFactory.deploy('IXT', 'IXT');

        let metamodFusionFactory = await ethers.getContractFactory("MetamodFusion");
        metamodFusion = await upgrades.deployProxy(metamodFusionFactory, [assetManagerMock.address, ixt.address]);

        await metamodFusion.setFusionCost(costTokenIds, costAmounts);

        await assetManagerMock.setTrusted(metamodFusion.address, true);
        
        await assetManagerMock.mintBatch(
          alice.address, 
          [ASTRO_CREDITS, BIOMOD_OUTLIER, BIOMOD_COMMON, BIOMOD_UNCOMMON, BIOMOD_RARE, BIOMOD_LEGENDARY, ENERGY], 
          [1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000],
          []
        );
        await assetManagerMock.mintBatch(
          bob.address, 
          [ASTRO_CREDITS, BIOMOD_OUTLIER, BIOMOD_COMMON, BIOMOD_UNCOMMON, BIOMOD_RARE, BIOMOD_LEGENDARY, ENERGY], 
          [1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000, 1000000000],
          []
        );

        await ixt.mint(alice.address, utils.parseEther("10000"));
        await ixt.connect(alice).approve(metamodFusion.address, utils.parseEther("10000"));

        await assetManagerMock.connect(alice).setApprovalForAll(metamodFusion.address, true);
        await assetManagerMock.connect(bob).setApprovalForAll(metamodFusion.address, true);
    });

    describe('Place order', () => {
      it('should place fusion order and burn cost tokens', async function () {
        let amountsBefore = [];
        for(let i=0; i<costTokenIds.length; i++) {
          amountsBefore[i] = await assetManagerMock.balanceOf(alice.address, costTokenIds[i]);
        }

        await metamodFusion.connect(alice).placeFusionOrder();
        
        let amountsAfter = [];
        for(let i=0; i<costTokenIds.length; i++) {
          amountsAfter[i] = await assetManagerMock.balanceOf(alice.address, costTokenIds[i]);
          expect(amountsAfter[i]).to.be.equal(amountsBefore[i] - costAmounts[i]);
        }

        expect(await metamodFusion.currentFusionOrder(alice.address)).to.be.equal(1);
        expect((await metamodFusion.getOrders(alice.address)).length).to.be.equal(1);
      });

      it('reverts if you already have upgrading order', async function () {
        await metamodFusion.connect(alice).placeFusionOrder();
        await expect(metamodFusion.connect(alice).placeFusionOrder()).to.be.revertedWith("MetamodFusion__MaxOrdersReached()");
      });

    });

    describe('Claim order', () => {

      let orderTime;
      let metamodsBefore;

      beforeEach(async function () {
        orderTime = await metamodFusion.fusionOrderTime();
        metamodsBefore = await assetManagerMock.balanceOf(alice.address, M3TAM0D);

        await metamodFusion.connect(alice).placeFusionOrder();
      });

      it('Should be able to claim order', async function () {
        await increaseTime(orderTime.add(100));
        await expect(metamodFusion.connect(alice).claimOrder(1)).to.emit(metamodFusion, "ClaimOrder");

        expect(await assetManagerMock.balanceOf(alice.address, M3TAM0D)).to.be.equal(metamodsBefore.add(1));
      });

      it('Should be able to call claim batch order function', async function () {
        await increaseTime(orderTime.add(100));
        await expect(metamodFusion.connect(alice).claimBatchOrder()).to.emit(metamodFusion, "ClaimOrder");

        expect(await assetManagerMock.balanceOf(alice.address, M3TAM0D)).to.be.equal(metamodsBefore.add(1));
      });

      it('Should not be able to call with invalid order id', async function () {
        await metamodFusion.connect(bob).placeFusionOrder();
        await increaseTime(orderTime.add(100));
        await expect(metamodFusion.connect(alice).claimOrder(2)).to.be.revertedWith("MetamodFusion__InvalidOrder(2)");
        await expect(metamodFusion.connect(bob).claimOrder(1)).to.be.revertedWith("MetamodFusion__InvalidOrder(1)");
      });

      it('Should not be able to claim if order is not finished', async function () {
        await expect(metamodFusion.connect(alice).claimOrder(1)).to.be.revertedWith("MetamodFusion__OrderNotFinished()");
      });

      it('Should not be able to claim order twice', async function () {
        await increaseTime(orderTime.add(100));
        await metamodFusion.connect(alice).claimOrder(1);
        await expect(metamodFusion.connect(alice).claimOrder(1)).to.be.revertedWith("MetamodFusion__InvalidOrder(1)");
      });

      it('Should be able to speedup order', async function () {
        await metamodFusion.connect(alice).speedUpOrder(24, 1);
        let order = await metamodFusion.fusionOrders(1);
        expect(order.speedUpDeductedAmount).to.be.above(BigNumber.from(0));
        expect(order.totalCompletionTime).to.be.equal(orderTime.sub(order.speedUpDeductedAmount));

        await expect(metamodFusion.connect(alice).claimOrder(1)).to.be.revertedWith("MetamodFusion__OrderNotFinished()");
        await metamodFusion.connect(alice).speedUpOrder(24*4 - 1, 1);
        await expect(metamodFusion.connect(alice).claimOrder(1)).to.be.revertedWith("MetamodFusion__OrderNotFinished()");
        await metamodFusion.connect(alice).speedUpOrder(1, 1);

        await expect(metamodFusion.connect(alice).claimOrder(1)).to.emit(metamodFusion, "ClaimOrder")
      });

      it('Should be able to speedup order with IXT', async function () {
        await metamodFusion.connect(alice).IXTSpeedUpOrder(24, 1);
        let order = await metamodFusion.fusionOrders(1);
        expect(order.speedUpDeductedAmount).to.be.above(BigNumber.from(0));
        expect(order.totalCompletionTime).to.be.equal(orderTime.sub(order.speedUpDeductedAmount));

        await expect(metamodFusion.connect(alice).claimOrder(1)).to.be.revertedWith("MetamodFusion__OrderNotFinished()");
        await metamodFusion.connect(alice).IXTSpeedUpOrder(24*4 - 1, 1);
        await expect(metamodFusion.connect(alice).claimOrder(1)).to.be.revertedWith("MetamodFusion__OrderNotFinished()");
        await metamodFusion.connect(alice).IXTSpeedUpOrder(1, 1);

        await expect(metamodFusion.connect(alice).claimOrder(1)).to.emit(metamodFusion, "ClaimOrder")
      });
    });
});