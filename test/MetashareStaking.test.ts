import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Signer, Contract, utils } from 'ethers';
import { advanceTime } from './utils';
import { parseEther } from 'ethers/lib/utils';

describe('MetashareStaking', function () {
  let owner: Signer;
  let alice: Signer;
  let bob: Signer;
  let pixToken: Contract;
  let corporations: Contract;
  let shareStaking: Contract;

  beforeEach(async function () {
    [owner, alice, bob] = await ethers.getSigners();

    const PIXTFactory = await ethers.getContractFactory('PIXT');
    pixToken = await PIXTFactory.deploy();

    const CorporationsFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    corporations = await CorporationsFactory.deploy('uri');

    const PIXMetashareStakingFactory = await ethers.getContractFactory('MetashareStaking');
    shareStaking = await upgrades.deployProxy(PIXMetashareStakingFactory, [
      pixToken.address,
      corporations.address,
      corporations.address, // Dummy address - connext
    ]);

    await shareStaking.setStakeableIds([1], true);
    await corporations.connect(alice).mint(await alice.getAddress(), 1, 20);
    await corporations.connect(bob).mint(await bob.getAddress(), 1, 20);
    await pixToken.approve(shareStaking.address, utils.parseEther('1000000'));
    await corporations.connect(alice).setApprovalForAll(shareStaking.address, true);
    await corporations.connect(bob).setApprovalForAll(shareStaking.address, true);
    await shareStaking.setRewardDistributor(await owner.getAddress());
  });

  describe('stake', () => {
    it('cannot stake invalid tokenId', async function () {
      await expect(shareStaking.connect(alice).stake(2, 5)).to.revertedWith('MetashareStaking: INVALID_TOKEN_ID');
    });
    it('should stake an NFT', async function () {
      await shareStaking.connect(alice).stake(1, 5);
      expect(await corporations.balanceOf(await alice.getAddress(), 1)).to.equal(15);
      expect(await shareStaking.getStakedAmounts(await alice.getAddress(), 1)).to.equal(5);
    });
  });

  describe('initializePools', () => {
    it('revert if msg.sender is not reward distributor', async function () {
      await expect(shareStaking.connect(alice).initializePools([1], [1000], 86400)).to.revertedWith('Staking: NON_DISTRIBUTOR');
    });

    it('revert if arguments are invalid', async function () {
      await expect(shareStaking.initializePools([1, 2], [1000], 86400)).to.revertedWith('Staking: INVALID_ARGUMENTS');
    });

    it('should deposit the correct staking rewards', async function () {
      await shareStaking.initializePools([1], [1000], 86400);
      expect(await pixToken.balanceOf(await shareStaking.address)).to.equal(1000);
    });
  });

  describe('claim', () => {
    beforeEach(async function () {
      let monthSeconds = 60 * 60 * 24 * 30;
      let epochTotalReward = parseEther('1000');
      await shareStaking.initializePools([1], [epochTotalReward], monthSeconds);
      // Stake an NFT from Alice
      await shareStaking.connect(alice).stake(1, 5);
    });

    it('rewardPerTokenFromNow is correct', async function () {
      const oneDayFromNow = Math.floor(Date.now() / 1000) + 86400;
      const rpt = await shareStaking.rewardPerTokenFromNow(1, oneDayFromNow);
      expect(rpt.toString().startsWith('6666')).to.equal(true);
      expect(rpt.toString().length).to.equal(19);
    });

    it('rewardPerTokenFromNow is correct with 2 staked users', async function () {
      await shareStaking.connect(bob).stake(1, 5);
      const oneDayFromNow = Math.floor(Date.now() / 1000) + 86400;
      const rpt = await shareStaking.rewardPerTokenFromNow(1, oneDayFromNow);
      expect(rpt.toString().startsWith('333')).to.equal(true);
      expect(rpt.toString().length).to.equal(19);
    });

    it('should return the correct staking amount', async function () {
      expect(await shareStaking.getStakedAmounts(await alice.getAddress(), 1)).to.equal(5);
    });

    it('should claim correct rewards amount', async function () {
      await advanceTime(60 * 60 * 24);
      await shareStaking.connect(alice).claim();
      let expectedDailyReward = parseEther('33.3333333333332928');
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedDailyReward))).to.be.most(1);
    });

    it('should claim all rewards amount after epoch end', async function () {
      await advanceTime(60 * 60 * 24 * 30);
      await shareStaking.connect(alice).claim();
      let expectedMonthlyReward = parseEther('999.999614197529648195');
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedMonthlyReward))).to.be.most(1);
    });

    it('should claim correct amount of rewards after second epoch epoch end', async function () {
      let monthSeconds = 60 * 60 * 24 * 30;
      let epochTotalReward = parseEther('1000');
      await advanceTime(monthSeconds);
      await shareStaking.initializePools([1], [epochTotalReward], monthSeconds);
      await shareStaking.connect(bob).stake(1, 5);
      await advanceTime(monthSeconds);
      await shareStaking.connect(alice).claim();
      await shareStaking.connect(bob).claim();
      let expectedAlicesReward = parseEther('1499.999807098763608090');
      let expectedBobsReward = parseEther('499.999807098764824095');
      expect(Math.abs((await pixToken.balanceOf(await alice.getAddress())).sub(expectedAlicesReward))).to.be.most(1);
      expect(Math.abs((await pixToken.balanceOf(await bob.getAddress())).sub(expectedBobsReward))).to.be.most(1);
    });
  });

  describe('unstake', () => {
    beforeEach(async function () {
      // Stake an NFT from Alice
      await shareStaking.connect(alice).stake(1, 5);
    });

    it('revert if msg.sender is unstaking invalid token', async function () {
      await expect(shareStaking.unstake(0, 10)).to.revertedWith('MetashareStaking: INVALID_TOKEN_ID');
    });

    it('revert if msg.sender is not staker', async function () {
      await expect(shareStaking.unstake(1, 10)).to.revertedWith('MetashareStaking: NOT_ENOUGH');
    });

    it('revert if msg.sender is trying to unstake more than he staked', async function () {
      await expect(shareStaking.connect(alice).unstake(1, 10)).to.revertedWith('MetashareStaking: NOT_ENOUGH');
    });

    it('should return the correct staking amount', async function () {
      await shareStaking.connect(alice).unstake(1, 3);
      expect(await shareStaking.getStakedAmounts(await alice.getAddress(), 1)).to.equal(2);
    });

    it('should stake again', async function () {
      await shareStaking.connect(alice).unstake(1, 5);
      await shareStaking.connect(alice).stake(1, 10);
      expect(await corporations.balanceOf(await alice.getAddress(), 1)).to.equal(10);
    });
  });
});
