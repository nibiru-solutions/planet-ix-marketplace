import { expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { Contract, BigNumber } from 'ethers';
import { PIXCategory, AssetIds } from './utils';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { MCCrosschainServices, MissionControl, MissionControlStaking, Mock1155FT, MockMissionControlMintable } from '../src/types';

interface order {
  x: number;
  y: number;
  z: number;
}
interface placeOrder {
  order: order;
  tokenId: number;
  tokenAddress: string;
}

describe('Mission Control', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let streamer: SignerWithAddress;
  let bob: SignerWithAddress;
  let missionControl: MissionControl;
  let mintable: MockMissionControlMintable;
  let missionControlStaking: MissionControlStaking;
  let mockTileContracts: Mock1155FT;
  let mcCrosschainServices: MCCrosschainServices;

  const aGold = '0x3CAD7147c15C0864B8cF0EcCca43f98735e6e782';
  const aGoldLite = '0x39161eb4Ce381d92a472Dfc88dA033700C14D49D';

  const tile_price = 385802469135;

  const GR_AP = {
    token_id: 1,
    resource_ev: 240,
    resource_id: AssetIds.Waste,
    resource_cap: 240,
  };
  const GR_ART = {
    token_id: 1,
    resource_ev: 288,
    resource_id: AssetIds.Waste,
    resource_cap: 288,
  };
  const GENESIS_ROVER_CHRONOS = {
    token_id: 1,
    resource_ev: 288,
    resource_id: AssetIds.Waste,
    resource_cap: 288,
  };
  const GENESIS_ROVER_HELIOS = {
    token_id: 1,
    resource_ev: 312,
    resource_id: AssetIds.Waste,
    resource_cap: 312,
  };
  const PR_AP = {
    token_id: 2,
    resource_ev: 160,
    resource_id: AssetIds.Waste,
    resource_cap: 160,
  };
  const PR_ART = {
    token_id: 2,
    resource_ev: 192,
    resource_id: AssetIds.Waste,
    resource_cap: 192,
  };
  const PIERCER_ROVER_CHRONOS = {
    token_id: 2,
    resource_ev: 192,
    resource_id: AssetIds.Waste,
    resource_cap: 192,
  };
  const PIERCER_ROVER_HELIOS = {
    token_id: 2,
    resource_ev: 208,
    resource_id: AssetIds.Waste,
    resource_cap: 208,
  };
  const NR_AP = {
    token_id: 0,
    resource_ev: 300,
    resource_id: AssetIds.Waste,
    resource_cap: 300,
  };
  const NR_ART = {
    token_id: 0,
    resource_ev: 360,
    resource_id: AssetIds.Waste,
    resource_cap: 360,
  };
  const NIGHT_ROVER_CHRONOS = {
    token_id: 0,
    resource_ev: 360,
    resource_id: AssetIds.Waste,
    resource_cap: 360,
  };
  const NIGHT_ROVER_HELIOS = {
    token_id: 0,
    resource_ev: 390,
    resource_id: AssetIds.Waste,
    resource_cap: 390,
  };

  const helios_tiles = [
    { x: -3, y: 0, z: 3 },
    { x: -3, y: 3, z: 0 },
    { x: 0, y: -3, z: 3 },
    { x: 0, y: 3, z: -3 },
    { x: 3, y: -3, z: 0 },
    { x: 3, y: 0, z: -3 },
  ];

  const chronos_tile = [
    { x: -3, y: 1, z: 2 },
    { x: -3, y: 2, z: 1 },
    { x: -2, y: -1, z: 3 },
    { x: -2, y: 3, z: -1 },
    { x: -1, y: -2, z: 3 },
    { x: -1, y: 3, z: -2 },
    { x: 1, y: -3, z: 2 },
    { x: 1, y: 2, z: -3 },
    { x: 2, y: -3, z: 1 },
    { x: 2, y: 1, z: -3 },
    { x: 3, y: -2, z: -1 },
    { x: 3, y: -1, z: -2 },
  ];

  const artemis_tile = [
    { x: -2, y: 0, z: 2 },
    { x: -2, y: 1, z: 1 },
    { x: -2, y: 2, z: 0 },
    { x: -1, y: -1, z: 2 },
    { x: -1, y: 2, z: -1 },
    { x: 0, y: -2, z: 2 },
    { x: 0, y: 2, z: -2 },
    { x: 1, y: -2, z: 1 },
    { x: 1, y: 1, z: -2 },
    { x: 2, y: -2, z: 0 },
    { x: 2, y: -1, z: -1 },
    { x: 2, y: 0, z: -2 },
  ];

  const apollo_tile = [
    { x: -1, y: 0, z: 1 },
    { x: -1, y: 1, z: 0 },
    { x: 0, y: -1, z: 1 },
    { x: 0, y: 1, z: -1 },
    { x: 1, y: -1, z: 0 },
    { x: 1, y: 0, z: -1 },
  ];

  enum ROVER_TIERS {
    NIGHT,
    GENESIS,
    PIERCER,
  }

  beforeEach(async function () {
    [owner, alice, bob, streamer] = await ethers.getSigners();

    let mintableFactory = await ethers.getContractFactory('MockMissionControlMintable');
    mintable = await mintableFactory.deploy();
    let mcFactory = await ethers.getContractFactory('MissionControl');
    missionControl = (await upgrades.deployProxy(mcFactory, [3, mintable.address])) as MissionControl;

    await missionControl.setStreamer(streamer.address);

    await missionControl.setAGold(aGold);
    await missionControl.setAGoldLite(aGoldLite);

    let tileContractFactory = await ethers.getContractFactory('Mock1155FT');
    mockTileContracts = await tileContractFactory.deploy('TOY URI');

    mockTileContracts.mint(alice.address, 35, 10000);
    mockTileContracts.mint(alice.address, 36, 10000);
    mockTileContracts.mint(alice.address, 37, 10000);
    mockTileContracts.mint(alice.address, 38, 10000);

    await missionControl.setMissionControlStaking(missionControlStaking.address);
    await missionControl.setTileContracts(mockTileContracts.address);
    await missionControl.setCrosschainServices(mcCrosschainServices.address);
    await mockTileContracts.connect(alice).setApprovalForAll(missionControlStaking.address, true);

    await missionControlStaking.whitelistTokens(mockTileContracts.address, [35, 36, 37, 38], true);
    await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 35, 1, 0);
    await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 36, 1, 0);

    let mockRoverFactory = await ethers.getContractFactory('ERC721Mock');
    let mockRover = await mockRoverFactory.deploy('ROVER', 'ROVER');

    let roverAdapterFactory = await ethers.getContractFactory('RoverMCStakeableAdapter');
    let roverAdapter = await upgrades.deployProxy(roverAdapterFactory, [
      '0x7a1BaC17Ccc5b313516C5E16fb24f7659aA5ebed',
      '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
      0,
      '0x4b09e658ed251bcafeebbc69400383d49f344ace09b9576fe248bb02c003fe9f',
      2500000,
      3,
      2541,
    ]);
    await roverAdapter.deployed();
    console.log('Deployed RoverMCStakeableAdapter at: ', roverAdapter.address);
    await roverAdapter.setMissionControl(missionControl.address);
    // await contract.setPixAssetAdapter(PIX_ASSET_ADAPTER);
    await roverAdapter.setERC721(mockRover.address);

    await roverAdapter.setResourceParametersBulk([
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[0].x, y: apollo_tile[0].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[1].x, y: apollo_tile[1].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[2].x, y: apollo_tile[2].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[3].x, y: apollo_tile[3].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[4].x, y: apollo_tile[4].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[5].x, y: apollo_tile[5].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[0].x, y: apollo_tile[0].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[1].x, y: apollo_tile[1].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[2].x, y: apollo_tile[2].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[3].x, y: apollo_tile[3].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[4].x, y: apollo_tile[4].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[5].x, y: apollo_tile[5].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[0].x, y: apollo_tile[0].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[1].x, y: apollo_tile[1].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[2].x, y: apollo_tile[2].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[3].x, y: apollo_tile[3].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[4].x, y: apollo_tile[4].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: apollo_tile[5].x, y: apollo_tile[5].y },
    ]);
    await roverAdapter.setResourceParametersBulk([
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[0].x, y: artemis_tile[0].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[1].x, y: artemis_tile[1].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[2].x, y: artemis_tile[2].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[3].x, y: artemis_tile[3].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[4].x, y: artemis_tile[4].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[6].x, y: artemis_tile[6].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[7].x, y: artemis_tile[7].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[8].x, y: artemis_tile[8].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[9].x, y: artemis_tile[9].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[10].x, y: artemis_tile[10].y },
      { tier: ROVER_TIERS.PIERCER, resourceEV: PR_AP.resource_ev, resourceId: PR_AP.resource_id, resourceCap: PR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[11].x, y: artemis_tile[11].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[0].x, y: artemis_tile[0].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[1].x, y: artemis_tile[1].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[2].x, y: artemis_tile[2].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[3].x, y: artemis_tile[3].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[4].x, y: artemis_tile[4].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
      { tier: ROVER_TIERS.GENESIS, resourceEV: GR_AP.resource_ev, resourceId: GR_AP.resource_id, resourceCap: GR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[0].x, y: artemis_tile[0].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[1].x, y: artemis_tile[1].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[2].x, y: artemis_tile[2].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[3].x, y: artemis_tile[3].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[4].x, y: artemis_tile[4].y },
      { tier: ROVER_TIERS.NIGHT, resourceEV: NR_AP.resource_ev, resourceId: NR_AP.resource_id, resourceCap: NR_AP.resource_cap, isBase: false, isRaidable: true, x: artemis_tile[5].x, y: artemis_tile[5].y },
    ]);
  });
});

async function increaseTime(amount) {
  await hre.network.provider.send('evm_increaseTime', [amount]);
  await hre.network.provider.send('evm_mine');
  //console.log("EVM time " + amount + " seconds increased!")
}
