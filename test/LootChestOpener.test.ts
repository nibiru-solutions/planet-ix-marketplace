import {expect} from 'chai';
import hre, {ethers, upgrades} from 'hardhat';
import {Signer, Contract, BigNumber, providers} from 'ethers';
import {AssetIds} from './utils';
import {constants} from '@openzeppelin/test-helpers';
import {address} from "hardhat/internal/core/config/config-validation";
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/signers";

describe('Loot chest opener', function () {
    let owner: SignerWithAddress;
    let alice: SignerWithAddress;
    let mockAssets: Contract;
    let opener: Contract;
    let vrf: Contract;
    let assets = [
        AssetIds.Blueprint,
        AssetIds.BioModOutlier,
        AssetIds.BioModCommon,
        AssetIds.BioModUncommon,
        AssetIds.BioModRare,
        AssetIds.BioModLegendary,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
        AssetIds.AstroCredit,
    ];

    // These are not final
    let assetsWeight = [
        1, 20, 15, 10, 5, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2
    ];
    let assetsAmounts = [
        1, 1, 1, 1, 1, 1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29
    ];
    let contents = [2, 3, 4, 5]
    let contentWeights = [2, 5, 2, 1]

    let toyWeight = [10, 20, 40, 80, 160, 320, 640]
    let toyAssets = [
        AssetIds.AstroCredit,
        AssetIds.Blueprint,
        AssetIds.BioModOutlier,
        AssetIds.BioModCommon,
        AssetIds.BioModUncommon,
        AssetIds.BioModRare,
        AssetIds.BioModLegendary]
    let toyAssetAmt = [1, 1, 1, 1, 1, 1, 1]


    beforeEach(async function () {
            [owner, alice] = await ethers.getSigners();
            let ercfac = await ethers.getContractFactory("ERC1155MockAssetManager");
            mockAssets = await ercfac.deploy("Toy uri");
            let burnfac = await ethers.getContractFactory("LootChestOpener");
            opener = await upgrades.deployProxy(
                burnfac, [mockAssets.address, assets, assetsAmounts, assetsWeight, contents, contentWeights]);
            await mockAssets.setTrusted(opener.address, true);
            let vrfFac = await ethers.getContractFactory("VRFManagerMock")
            vrf = await vrfFac.deploy();
            await opener.setVRF(vrf.address);
        }
    )

    describe("Open Chests", function () {
        it("Mints some reward", async function () {
            let num = 4;
            let N = 5;
            let reqId = 0;
            this.timeout(num * N * 100)
            for (let i = 0; i < N; i++) {
                await mockAssets.mint(alice.address, AssetIds.LootCrate, num);
                await opener.connect(alice).openChests(num);
                for (let x = 0; x < num; x++) {
                    await vrf.fulfill(++reqId)
                }
            }

            expect(await getBalancesSum(alice, mockAssets) >= 2 * N * num).to.true;
            //console.log(await getBalances(alice, mockAssets));
        })

        it("Mints specific reward when output is deterministic", async function () {
            let tokenId = 10;
            let amount = 20;
            expect(await mockAssets.balanceOf(alice.address, tokenId)).to.equal(0);
            await opener.updateProbabilities([tokenId], [amount], [10], [1], [10])
            await mockAssets.mint(alice.address, AssetIds.LootCrate, 1);
            await opener.connect(alice).openChests(1)
            await vrf.fulfill(1);
            expect(await mockAssets.balanceOf(alice.address, tokenId)).to.equal(amount);
        })
        it("Burns the chest tokens", async function () {
            let burn_num = 11;
            let mint_num = 20
            await mockAssets.mint(alice.address, AssetIds.LootCrate, mint_num);
            await opener.connect(alice).openChests(burn_num);
            expect(await mockAssets.balanceOf(alice.address, AssetIds.LootCrate)).to.equal(mint_num - burn_num);
        })

        it("Emits event", async function () {
            await mockAssets.mint(alice.address, AssetIds.LootCrate, 1);
            await opener.connect(alice).openChests(1);
            await expect(vrf.fulfill(1)).to.emit(opener, "ChestOpened");
        })
        it("Emits specific event when output is deterministic", async function () {
            let tokenId = 10;
            let amount = 20;
            await opener.updateProbabilities([tokenId], [amount], [10], [1], [10])
            await mockAssets.mint(alice.address, AssetIds.LootCrate, 1);
            await opener.connect(alice).openChests(1)
            await expect(vrf.fulfill(1)).to.emit(opener, "ChestOpened"
            ).withArgs(alice.address, [tokenId], [amount]);
        })
    })

    describe("Misc", function () {
        it("Should revert on non owner call to updateProbabilities", async function () {
            let tokenId = 10;
            let amount = 20;
            await expect(opener.connect(alice)
                .updateProbabilities([tokenId], [amount], [10], [1], [10]))
                .to.revertedWith("Ownable: caller is not the owner");
        })

        it("Should revert on non owner call to set vrf", async function () {
            await expect(opener.connect(alice).setVRF(alice.address)).to.revertedWith("Ownable: caller is not the owner");
        })

        it("Should revert on non oracle call to fulfill", async function () {
            await expect(opener.connect(alice).fulfillRandomWords(1, [1]))
                .to.revertedWith(`OnlyCoordinatorCanFulfill("${alice.address}", "${vrf.address}")`);
        })
    })
})

async function getBalances(user: SignerWithAddress, erc1155: Contract) {
    return [
        await erc1155.balanceOf(user.address, AssetIds.AstroCredit),
        await erc1155.balanceOf(user.address, AssetIds.Blueprint),
        await erc1155.balanceOf(user.address, AssetIds.BioModOutlier),
        await erc1155.balanceOf(user.address, AssetIds.BioModCommon),
        await erc1155.balanceOf(user.address, AssetIds.BioModUncommon),
        await erc1155.balanceOf(user.address, AssetIds.BioModRare),
        await erc1155.balanceOf(user.address, AssetIds.BioModLegendary)
    ]
}

async function getBalancesSum(user: SignerWithAddress, erc1155: Contract) {
    return (await erc1155.balanceOf(user.address, AssetIds.AstroCredit)).add(
        await erc1155.balanceOf(user.address, AssetIds.Blueprint)).add(
        await erc1155.balanceOf(user.address, AssetIds.BioModOutlier)).add(
        await erc1155.balanceOf(user.address, AssetIds.BioModCommon)).add(
        await erc1155.balanceOf(user.address, AssetIds.BioModUncommon)).add(
        await erc1155.balanceOf(user.address, AssetIds.BioModRare)).add(
        await erc1155.balanceOf(user.address, AssetIds.BioModLegendary))

}