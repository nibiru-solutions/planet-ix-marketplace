import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';

const hre = require('hardhat');
import { Signer, Contract, constants, utils } from 'ethers';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { AssetIds } from './utils';
import { Rover, RoverMCStakeableAdapter, VRFCoordinatorV2Mock } from '../src/types';
import { advanceBlock } from 'hardhat-helpers';
import { randomBytes } from 'crypto';

enum ROVER_TIERS {
  NIGHT,
  GENESIS,
  PIERCER,
}

let ROVER_PIERCER_A = {
  tier: ROVER_TIERS.PIERCER,
  resource_ev: 160,
  resource_id: AssetIds.Waste,
  resource_cap: 160,
  is_base: false,
  is_raidable: false,
};
let ROVER_PIERCER_B = {
  tier: ROVER_TIERS.PIERCER,
  resource_ev: 192,
  resource_id: AssetIds.Waste,
  resource_cap: 192,
  is_base: false,
  is_raidable: false,
};

let ROVER_GENESIS_A = {
  tier: ROVER_TIERS.GENESIS,
  resource_ev: 240,
  resource_id: AssetIds.Waste,
  resource_cap: 240,
  is_base: false,
  is_raidable: false,
};
let ROVER_GENESIS_B = {
  tier: ROVER_TIERS.GENESIS,
  resource_ev: 288,
  resource_id: AssetIds.Waste,
  resource_cap: 288,
  is_base: false,
  is_raidable: false,
};

let ROVER_NIGHT_A = {
  tier: ROVER_TIERS.NIGHT,
  resource_ev: 300,
  resource_id: AssetIds.Waste,
  resource_cap: 300,
  is_base: false,
  is_raidable: false,
};
let ROVER_NIGHT_B = {
  tier: ROVER_TIERS.NIGHT,
  resource_ev: 360,
  resource_id: AssetIds.Waste,
  resource_cap: 360,
  is_base: false,
  is_raidable: false,
};

describe('RoverMCStakeableAdapter tests', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: Signer;
  let stakeable: RoverMCStakeableAdapter;
  let id = 1;
  let base = false;

  let missionControl: SignerWithAddress;
  let rover: Rover;
  let roverUri = 'https://bafybeicopzv5xbxqvsgfylzfddfiw53mkmgfg6fvbfavfx4loxrhy2l7me.ipfs.4everland.io';
  let oracle: Contract;
  let usdt: Contract;
  let ixt: Contract;
  let wmatic: Contract;
  let astroCredits: Contract;
  let acTokenId = 0;
  let vrfCoordinatorV2Mock: VRFCoordinatorV2Mock;
  let keyHash = utils.defaultAbiCoder.encode(['uint256'], [1]);
  let callbackGasLimit = 2000000;
  let requestConfirmations = 3;
  let subscriptionId = 1;

  let m3tam0d: Contract;
  let m3tam0dTokenId = 1;

  beforeEach(async function () {
    this.timeout(19000);

    [owner, alice, bob] = await ethers.getSigners();
    missionControl = owner;

    // ROVER NFT
    let oracleFactory = await ethers.getContractFactory('OracleManagerMock');
    oracle = await oracleFactory.deploy();

    const VRFCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
    vrfCoordinatorV2Mock = await VRFCoordinatorV2MockFactory.deploy(0, 6);
    await vrfCoordinatorV2Mock.createSubscription();
    await vrfCoordinatorV2Mock.fundSubscription(subscriptionId, utils.parseEther('10'));

    const USDTFactory = await ethers.getContractFactory('MockToken');
    usdt = await USDTFactory.deploy('USDT', 'USDT', 6);

    const IXTFactory = await ethers.getContractFactory('MockToken');
    ixt = await IXTFactory.deploy('IXT', 'IXT', 18);

    const WMATICFactory = await ethers.getContractFactory('MockToken');
    wmatic = await WMATICFactory.deploy('WMATIC', 'WMATIC', 18);

    const ACFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    astroCredits = await ACFactory.deploy('astrocredits');

    const RoverHelerFactory = await ethers.getContractFactory('RoverHelper');
    const roverHelper = await RoverHelerFactory.deploy();

    const RoverFactory = await ethers.getContractFactory('Rover');
    rover = (await upgrades.deployProxy(RoverFactory, [roverUri, missionControl.address, oracle.address, vrfCoordinatorV2Mock.address, usdt.address, ixt.address, astroCredits.address, acTokenId])) as Rover;
    await rover.setKeyHash(keyHash);
    await rover.setCallbackGasLimit(callbackGasLimit);
    await rover.setRequestConfirmations(requestConfirmations);
    await rover.setSubscriptionId(subscriptionId);
    await rover.setSaleOpen(true);
    await vrfCoordinatorV2Mock.addConsumer(subscriptionId, rover.address);

    await rover.setRoverHelper(roverHelper.address);

    await usdt.approve(rover.address, constants.MaxUint256);
    await usdt.connect(alice).approve(rover.address, constants.MaxUint256);

    // ROVER STAKEABLE ADAPTER
    const M3tam0dFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    m3tam0d = await M3tam0dFactory.deploy('M3taM0d');

    let stakeableFactory = await ethers.getContractFactory('RoverMCStakeableAdapter');
    stakeable = (await upgrades.deployProxy(stakeableFactory, [vrfCoordinatorV2Mock.address, m3tam0d.address, m3tam0dTokenId, keyHash, callbackGasLimit, requestConfirmations, subscriptionId])) as RoverMCStakeableAdapter;

    await stakeable.setERC721(rover.address);
    await stakeable.setMissionControl(await owner.getAddress());
    await stakeable.setResourceParameters(ROVER_PIERCER_A.tier, ROVER_PIERCER_A.resource_ev, ROVER_PIERCER_A.resource_id, ROVER_PIERCER_A.resource_cap, ROVER_PIERCER_A.is_base, ROVER_PIERCER_A.is_raidable, 1);
    await stakeable.setResourceParameters(ROVER_GENESIS_A.tier, ROVER_GENESIS_A.resource_ev, ROVER_GENESIS_A.resource_id, ROVER_GENESIS_A.resource_cap, ROVER_GENESIS_A.is_base, ROVER_GENESIS_A.is_raidable, 1);
    await stakeable.setResourceParameters(ROVER_NIGHT_A.tier, ROVER_NIGHT_A.resource_ev, ROVER_NIGHT_A.resource_id, ROVER_NIGHT_A.resource_cap, ROVER_NIGHT_A.is_base, ROVER_NIGHT_A.is_raidable, 1);

    await stakeable.setRings([{ x: 0, y: 0, ring: 1 }]);

    await m3tam0d.setTrusted(stakeable.address, true);
    await vrfCoordinatorV2Mock.addConsumer(subscriptionId, stakeable.address);

    // Mint Rovers
    await usdt.transfer(await alice.getAddress(), 100_000000);
    await rover.setRoverAdapter(stakeable.address);
    await rover.setMissionControl(stakeable.address);
    await rover.connect(alice).purchaseWithUSDT(2);
    await vrfCoordinatorV2Mock.fulfillRandomWords(1, rover.address, {
      gasLimit: callbackGasLimit,
    });
  });

  describe('onStaked', function () {
    it('Can claim if approved', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      expect(await rover.ownerOf(1)).to.equal(stakeable.address);
    });
  });

  describe('onStaked reverts', function () {
    it('Can not claim token if not approved as spender', async function () {
      await expect(stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0)).to.revertedWith('ERC721: transfer caller is not owner nor approved');
    });

    it('Can not claim token if not MC', async function () {
      await expect(stakeable.connect(alice).onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0)).to.revertedWith('MCRover_ADAPTER: Sender not mission control');
    });
  });

  describe('onUnstaked', function () {
    it('MC can return', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      expect(await rover.ownerOf(1)).to.equal(stakeable.address);
      await stakeable.onUnstaked(await alice.getAddress(), 1, 0);
      expect(await rover.ownerOf(1)).to.equal(await alice.getAddress());
    });
  });

  describe('onUnstaked reverts', function () {
    it('Can not return if not MC', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      expect(await rover.ownerOf(1)).to.equal(stakeable.address);
      await expect(stakeable.connect(bob).onUnstaked(await alice.getAddress(), 1, 0)).to.revertedWith('MCRover_ADAPTER: Sender not mission control');
    });
  });

  describe('reset', function () {
    it('can reset', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      await expect(stakeable.reset(await alice.getAddress(), 1)).to.not.reverted;
    });
  });

  describe('reset reverts', function () {
    it('Should revert on caller not being MC', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      await expect(stakeable.connect(alice).reset(await alice.getAddress(), 1)).to.revertedWith('MCRover_ADAPTER: Sender not mission control');
    });
    it('Should revert on not owning token', async function () {
      await expect(stakeable.reset(await alice.getAddress(), 1)).to.revertedWith('MCRover_ADAPTER: No such token staked');
    });
  });

  describe('checkTile', function () {
    beforeEach(async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
    });

    it('returns zero resources initially', async function () {
      expect((await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0]).to.equal(0);
    });

    it('returns correct tokenId', async function () {
      expect((await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[1]).to.equal(ROVER_PIERCER_A.resource_id);
    });

    it('increments over time', async function () {
      let num_days = 11;
      expect((await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0]).to.equal(0);
      await increaseTime(86400 * num_days);
      // expect((await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0] > 0).to.true;
    });

    it('increments up to max', async function () {
      expect((await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0]).to.equal(0);
      const rateResult = await stakeable.getRoverRate(1, 30);
      const rate = rateResult[0];
      console.log(rate);
      for (let i = 0; i < 10; i++) {
        await increaseTime(86400);
        const newVal = (await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0];
        expect(newVal).to.equal(rate.mul(i + 1));
      }
      for (let i = 0; i < 2; i++) {
        await increaseTime(86400);
        const newVal = (await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0];
        expect(newVal).to.equal(rate.mul(10));
      }
      // expect((await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0] > 0).to.true;
    });
  });

  describe('checkTile reverts', function () {
    beforeEach(async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
    });

    it('should revert if not MC', async function () {
      await expect(stakeable.connect(alice).checkTile(await alice.getAddress(), 1, 0, 0, 0)).to.revertedWith('MCRover_ADAPTER: Sender not mission control');
    });
  });

  describe('collectFromTiles', function () {
    beforeEach(async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
    });

    it('verify with tier', async () => {
      let tier = 2;
      let verify = await stakeable.roverStakedWithTier(await alice.getAddress(), 0, tier);
      expect(verify).to.equal(true);

      tier = 1;
      verify = await stakeable.roverStakedWithTier(await alice.getAddress(), 0, tier);
      expect(verify).to.equal(false);
    });

    it('returns correct tokenId', async function () {
      await increaseTime(86400 * 11);
      expect((await stakeable.callStatic.onCollect(await alice.getAddress(), 1, 0, 0, 0))[1]).to.equal(ROVER_PIERCER_A.resource_id);
    });

    it('increments over time', async function () {
      /*
            This is technically a flaky test, however the chance of a false negative is infinitesimal
             */
      let num_days = 11;
      expect((await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0]).to.equal(0);
      await increaseTime(86400 * num_days);
      await expect(stakeable.callStatic.onCollect(await alice.getAddress(), 1, 0, 0, 0)).to.not.reverted;
    });

    it('should decrease the amount on a tile by the collected amount', async function () {
      /*
            This is technically a flaky test, however the chance of a false negative is infinitesimal

            This test also takes a long time to run :)
             */
      let num_days = 11;
      expect((await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0]).to.equal(0);
      await increaseTime(86400 * num_days);
      let num_before = (await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0];
      await stakeable.onCollect(await alice.getAddress(), 1, 0, 0, 0);
      let num_after = (await stakeable.checkTile(await alice.getAddress(), 1, 0, 0, 0))[0];
      expect(num_before.sub(num_after)).to.equal(num_before);
    });
    it('Collecting from the same rover many time should never make it go past wrecked', async () => {
      await rover.setTrusted(owner.address, true);
      await vrfCoordinatorV2Mock.addConsumer(1, stakeable.address);
      let pristine: number = 0;
      let worn: number = 0;
      let damaged: number = 0;
      let wrecked: number = 0;
      let nonce = 4;
      let tx = await rover.trustedMint(await alice.getAddress(), 1, 1);
      const roverJustMinted = (await tx.wait()).events[0].args.tokenId;
      await stakeable.onStaked(alice.address, roverJustMinted, nonce, ethers.constants.AddressZero, 0, ethers.constants.AddressZero, 0, 0, 0, 0);
      for (let i = 0; i < 100; i++) {
        await increaseTime(60 * 60 * 24 * 10);
        let roverStatus = await rover.rovers(roverJustMinted);
        if (roverStatus['status'] != 3) {
          await stakeable.onCollect(alice.address, 4, 0, 0, 0);
        } else {
          await expect(stakeable.onCollect(alice.address, 4, 0, 0, 0)).to.reverted;
          return;
        }
        const randomNumber = getRandomNumber(); // Generate a random number between 0 and 2^256
        await vrfCoordinatorV2Mock.fulfillRandomWordsWithOverride(i + 2, stakeable.address, [randomNumber], { gasLimit: callbackGasLimit });
        if (roverStatus['status'] == 0) console.log('pristine:', pristine++);
        if (roverStatus['status'] == 1) console.log('worn', worn++);
        if (roverStatus['status'] == 2) console.log('damaged', damaged++);
        if (roverStatus['status'] == 3) console.log('wrecked', wrecked++);
      }
      console.log(`final status ${(await rover.rovers(roverJustMinted))['status']}`);
    });
    it('breakdown monte carlo', async () => {
      await rover.setTrusted(owner.address, true);
      await vrfCoordinatorV2Mock.addConsumer(1, stakeable.address);
      let pristine: number = 0;
      let worn: number = 0;
      let damaged: number = 0;
      let wrecked: number = 0;
      for (let i = 0; i < 1000; i++) {
        let tx = await rover.trustedMint(await alice.getAddress(), 1, 1);
        const roverJustMinted = (await tx.wait()).events[0].args.tokenId;
        let nonce = i + 4;
        await stakeable.onStaked(alice.address, roverJustMinted, nonce, ethers.constants.AddressZero, 0, ethers.constants.AddressZero, 0, 0, 0, 0);
        await increaseTime(60 * 60 * 24 * 10);
        await stakeable.onCollect(alice.address, nonce, 0, 0, 0);
        const randomNumber = getRandomNumber(); // Generate a random number between 0 and 2^256
        await vrfCoordinatorV2Mock.fulfillRandomWordsWithOverride(i + 2, stakeable.address, [randomNumber], { gasLimit: callbackGasLimit });
        let roverStatus = await rover.rovers(roverJustMinted);
        await stakeable.onUnstaked(alice.address, nonce, 0);
        if (roverStatus['status'] == 0) console.log('pristine:', pristine++);
        if (roverStatus['status'] == 1) console.log('worn', worn++);
        if (roverStatus['status'] == 2) console.log('damaged', damaged++);
        if (roverStatus['status'] == 3) console.log('wrecked', wrecked++);
      }
      console.log(`pristine: ${pristine}`);
      console.log(`worn: ${worn}`);
      console.log(`damaged: ${damaged}`);
      console.log(`wrecked: ${wrecked}`);
    });
  });

  describe('isBase', function () {
    it('should return expected boolean', async function () {
      expect(await stakeable.isBase(1, 0, 0)).to.equal(base);
    });
  });

  describe('check timeLeft', function () {
    it('should return 10 days', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      let timeLeft = await stakeable.timeLeft(await alice.getAddress(), 1, 0, 0);
      expect(timeLeft).to.equal(864000);
    });
  });

  describe('check timestamp', function () {
    it('should return expected time', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      let receipt = await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      await increaseTime(1000);
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
    });
    it('should return expected time after collecting', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      let receipt = await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      await increaseTime(86000 * 11);
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
      receipt = await stakeable.onCollect(await alice.getAddress(), 1, 0, 0, 0);
      timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
    });
    it('should return expected time after resetting', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      let receipt = await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      await increaseTime(1000);
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
      receipt = await stakeable.reset(await alice.getAddress(), 1);
      timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
      expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
    });
  });

  describe('Administrative', function () {
    it('should not allow non owner to set ERC721', async function () {
      await expect(stakeable.connect(alice).setERC721(rover.address)).to.revertedWith('Ownable: caller is not the owner');
    });
    it('should allow owner to set MissionControl', async function () {
      await expect(stakeable.setMissionControl(rover.address)).to.not.reverted;
    });
    it('should not allow non owner to set MissionControl', async function () {
      await expect(stakeable.connect(alice).setERC721(rover.address)).to.revertedWith('Ownable: caller is not the owner');
    });
    it('should allow owner to set params', async function () {
      await expect(stakeable.setResourceParameters(1, 0, id, 0, base, false, 0)).to.not.reverted;
    });
    it('should not allow non owner to set params', async function () {
      await expect(stakeable.connect(alice).setResourceParameters(1, 0, id, 0, base, false, 0)).to.revertedWith('Ownable: caller is not the owner');
    });
  });

  describe('Events', function () {
    it('Should emit TokenClaimed on claiming token', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await expect(stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0))
        .to.emit(stakeable, 'TokenClaimed')
        .withArgs(await alice.getAddress(), stakeable.address, 1);
    });
    it('Should emit TokenReturned on returning token', async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.AddressZero, 0, constants.AddressZero, 0, 0, 0, 0);
      expect(await rover.ownerOf(1)).to.equal(stakeable.address);
      await expect(stakeable.onUnstaked(await alice.getAddress(), 1, 0))
        .to.emit(stakeable, 'TokenReturned')
        .withArgs(await alice.getAddress(), stakeable.address, 1);
    });
    it('Should emit ParametersUpdated on updating parameters', async function () {
      await expect(stakeable.setResourceParameters(1, 0, id, 0, base, false, 1)).to.emit(stakeable, 'ParametersUpdated').withArgs(stakeable.address, id, 0, 0, base);
    });
  });

  describe('m3tam0d minting', () => {
    beforeEach(async function () {
      await rover.connect(alice).setApprovalForAll(stakeable.address, true);
      await stakeable.onStaked(await alice.getAddress(), 1, 1, rover.address, 0, constants.AddressZero, 0, 0, 0, 0);
    });

    it('should mint a token', async () => {
      await increaseTime(86400 * 11);
      await stakeable.onCollect(await alice.getAddress(), 1, 0, 0, 0);
      await vrfCoordinatorV2Mock.fulfillRandomWordsWithOverride(2, stakeable.address, [0], {
        gasLimit: callbackGasLimit,
      });
      const balance = await m3tam0d.balanceOf(await alice.getAddress(), m3tam0dTokenId);
      expect(balance).to.equal(1);
    });

    it('should not mint a token', async () => {
      await increaseTime(86400 * 11);
      await stakeable.onCollect(await alice.getAddress(), 1, 0, 0, 0);
      await vrfCoordinatorV2Mock.fulfillRandomWordsWithOverride(2, stakeable.address, [501], {
        gasLimit: callbackGasLimit,
      });
      const balance = await m3tam0d.balanceOf(await alice.getAddress(), m3tam0dTokenId);
      expect(balance).to.equal(0);
    });
  });
});

async function increaseTime(amount) {
  await hre.network.provider.send('evm_increaseTime', [amount]);
  await hre.network.provider.send('evm_mine');
  //console.log("EVM time " + amount + " seconds increased!")
}

function getRandomNumber(): bigint {
  const maxValue = 2n ** 256n; // 2^256 as a bigint
  const bytes = randomBytes(32); // Generate 32 random bytes
  const hexString = bytes.toString('hex'); // Convert the bytes to a hexadecimal string
  const randomNumber = BigInt(`0x${hexString}`); // Parse the hexadecimal string as a bigint
  return randomNumber % maxValue;
}
