import {expect} from 'chai';
import hre, {ethers, upgrades} from 'hardhat';
import {Signer, Contract, BigNumber, providers, utils} from 'ethers';
import {PIXCategory, PIXSize, AssetIds} from './utils';
import {constants} from '@openzeppelin/test-helpers';
import {address} from "hardhat/internal/core/config/config-validation";
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/signers";

function calc_cutoff(cap, ev) {
    return 115293 * (cap / ev) + 17322;
}

describe('Mission Control', function () {
    let owner: SignerWithAddress;
    let alice: SignerWithAddress;
    let bob: Signer;
    let missionControl: Contract;
    let assets: Contract;
    let pix: Contract;
    let pixStakeable: Contract;
    let assetStakeable: Contract;
    let pix_ev = 12;
    let pix_cap = 3;
    let pix_cutoff = calc_cutoff(pix_cap, pix_ev);
    let p_drone_ev = 13;
    let p_drone_cap = 24;
    let p_drone_cutoff = calc_cutoff(p_drone_cap, p_drone_ev);
    let sig;

    beforeEach(async function () {
        this.timeout(4000); // due to token creation loop
        [owner, alice, bob] = await ethers.getSigners();


        let erc721Factory = await ethers.getContractFactory("ERC721PIXMock");
        pix = await erc721Factory.deploy("PIX", "PIX");

        let assetFactory = await ethers.getContractFactory("ERC1155MockAssetManager");
        assets = await assetFactory.deploy("");

        let pixstakeFactory = await ethers.getContractFactory("ERC721MCStakeableAdapter");
        pixStakeable = await upgrades.deployProxy(pixstakeFactory, [AssetIds.Waste, 12, 3, true]);
        pixStakeable.setERC721(pix.address);

        let assetStakeFactory = await ethers.getContractFactory("PIXAssetStakeableAdapter");
        assetStakeable = await upgrades.deployProxy(assetStakeFactory);
        assetStakeable.setERC1155(assets.address);


        let mcFactory = await ethers.getContractFactory("MissionControl");
        missionControl = await upgrades.deployProxy(mcFactory, [1, assets.address]);

        await assetStakeable.setMissionControl(missionControl.address);
        await pixStakeable.setMissionControl(missionControl.address);

        await missionControl.setWhitelist(pixStakeable.address, true);
        await missionControl.setWhitelist(assetStakeable.address, true);
        await assets.setTrusted(missionControl.address, true);
        await assets.setTrusted(owner.address, true);

        await assetStakeable.setTokenInfo(AssetIds.PiercerDrone, true, false, false, 0, p_drone_ev, AssetIds.Waste, p_drone_cap);
        await assetStakeable.setTokenInfo(AssetIds.GenesisDrone, true, false, false, 0, 14, AssetIds.Waste, 24);


        for (let i = 0; i <= 6; i++) {
            pix.mint(alice.address, 5 * i)
            pix.mint(alice.address, 5 * i + 1)
            pix.mint(alice.address, 5 * i + 2)
            pix.mint(alice.address, 5 * i + 3)
            await pix.mint(alice.address, 5 * i + 4)
        }
        await assets.mint(alice.address, AssetIds.PiercerDrone, 6)


        sig = await getCollectDigest(missionControl, alice, alice, BigNumber.from(0));
    });

    describe("Staking", function () {
        beforeEach(async function () {
            pix.connect(alice).setApprovalForAll(pixStakeable.address, true);
        });

        it("stake pix", async function () {
            await missionControl.connect(alice).placeNFT(1, -1, 0, 1, pixStakeable.address);
            await missionControl.connect(alice).removeNFT(1, -1, 0);
            await missionControl.connect(alice).placeNFT(1, -1, 0, 1, pixStakeable.address);
            await missionControl.connect(alice).removeNFT(1, -1, 0);
        });


    });

    describe("Collecting from pix", function () {
        beforeEach(async function () {
            pix.connect(alice).setApprovalForAll(pixStakeable.address, true);
            await missionControl.connect(alice).placeNFT(1, -1, 0, 1, pixStakeable.address);
            await missionControl.connect(alice).placeNFT(-1, 1, 0, 2, pixStakeable.address);
            await missionControl.connect(alice).placeNFT(1, 0, -1, 3, pixStakeable.address);
            await missionControl.connect(alice).placeNFT(-1, 0, 1, 4, pixStakeable.address);
            await missionControl.connect(alice).placeNFT(0, 1, -1, 5, pixStakeable.address);
            await missionControl.connect(alice).placeNFT(0, -1, 1, 6, pixStakeable.address);
        });

        it("Collect from drones", async function () {
            await assets.connect(alice).setApprovalForAll(assetStakeable.address, true);
            await missionControl.connect(alice).placeNFT(1, -1, 0, AssetIds.PiercerDrone, assetStakeable.address);
            await missionControl.connect(alice).placeNFT(-1, 1, 0, AssetIds.PiercerDrone, assetStakeable.address);
            await missionControl.connect(alice).placeNFT(1, 0, -1, AssetIds.PiercerDrone, assetStakeable.address);
            await missionControl.connect(alice).placeNFT(-1, 0, 1, AssetIds.PiercerDrone, assetStakeable.address);
            await missionControl.connect(alice).placeNFT(0, 1, -1, AssetIds.PiercerDrone, assetStakeable.address);
            await missionControl.connect(alice).placeNFT(0, -1, 1, AssetIds.PiercerDrone, assetStakeable.address);

            let continueLoopin = true;
            let waste;
            while (continueLoopin) {
                await increaseTime(6000);
                waste = (await missionControl.connect(alice).checkTile(alice.address, 1, -1, 0))[0];
                continueLoopin = waste < p_drone_cap;

            }
            await missionControl.connect(alice).collectFromTiles([{x: 1, y: -1, z: 0}], sig.v, sig.r, sig.s)
            console.log(await assets.balanceOf(alice.address, AssetIds.Waste))


            continueLoopin = true;
            while (continueLoopin) {
                await increaseTime(6000);
                waste = (await missionControl.connect(alice).checkTile(alice.address, 1, -1, 0))[0];
                continueLoopin = waste < p_drone_cap;
            }
            await missionControl.connect(alice).collectFromTiles([
                {x: 1, y: -1, z: 0},
                {x: -1, y: 1, z: 0},
                {x: 1, y: 0, z: -1},
                {x: -1, y: 0, z: 1},
                {x: 0, y: -1, z: 1},
                {x: 0, y: 1, z: -1},
            ], sig.v, sig.r, sig.s)
            console.log(await assets.balanceOf(alice.address, AssetIds.Waste))

            console.log("Got here")
            console.log(p_drone_cutoff)
            console.log(p_drone_cutoff - 86400)
            await increaseTime(Math.floor(p_drone_cutoff - 86400))
            console.log("Got here 2")
            await missionControl.connect(alice).collectFromTiles([
                {x: 1, y: -1, z: 0},
                {x: -1, y: 1, z: 0},
                {x: 1, y: 0, z: -1},
                {x: -1, y: 0, z: 1},
                {x: 0, y: -1, z: 1},
                {x: 0, y: 1, z: -1},
            ], sig.v, sig.r, sig.s)
            console.log(await assets.balanceOf(alice.address, AssetIds.Waste))
            //await missionControl.connect(alice).collectFromTiles([{x: 1, y: -1, z: 0}])
        });

        it("Collect from pix", async function () {

            let continueLoopin = true;
            let waste;
            while (continueLoopin) {
                await increaseTime(600);
                waste = (await missionControl.connect(alice).checkTile(alice.address, 1, -1, 0))[0];
                continueLoopin = waste < 3;

            }
            //await missionControl.connect(alice).collectFromTiles([{x: 1, y: -1, z: 0, amount: 1}])
            //await missionControl.connect(alice).collectFromTiles([{x: 1, y: -1, z: 0, amount: 1}])
            await missionControl.connect(alice).collectFromTiles([{x: 1, y: -1, z: 0}], sig.v, sig.r, sig.s)
            console.log(await assets.balanceOf(alice.address, AssetIds.Waste))


            continueLoopin = true;
            while (continueLoopin) {
                await increaseTime(600);
                waste = (await missionControl.connect(alice).checkTile(alice.address,1, -1, 0))[0];
                continueLoopin = waste < 3;
            }
            await missionControl.connect(alice).collectFromTiles([
                {x: 1, y: -1, z: 0},
                {x: -1, y: 1, z: 0},
                {x: 1, y: 0, z: -1},
                {x: -1, y: 0, z: 1},
                {x: 0, y: -1, z: 1},
                {x: 0, y: 1, z: -1},
            ], sig.v, sig.r, sig.s)
            console.log(await assets.balanceOf(alice.address, AssetIds.Waste))


            await increaseTime(Math.floor(pix_cutoff - 100))

            await missionControl.connect(alice).collectFromTiles([
                {x: 1, y: -1, z: 0},
                {x: -1, y: 1, z: 0},
                {x: 1, y: 0, z: -1},
                {x: -1, y: 0, z: 1},
                {x: 0, y: -1, z: 1},
                {x: 0, y: 1, z: -1},
            ], sig.v, sig.r, sig.s)
            console.log(await assets.balanceOf(alice.address, AssetIds.Waste))
            //await missionControl.connect(alice).collectFromTiles([{x: 1, y: -1, z: 0}])
        });

    });


});

async function increaseTime(amount) {
    await hre.network.provider.send("evm_increaseTime", [amount])
    await hre.network.provider.send("evm_mine")
    //console.log("EVM time " + amount + " seconds increased!")
}

const getRaidDigest = async (
    missionControl: Contract,
    signer: SignerWithAddress,
    defender: SignerWithAddress,
    timestamp: BigNumber,
    raidId: BigNumber,
) => {
    const domain = {
        name: 'Mission Control',
        version: '1',
        chainId: (await ethers.provider.getNetwork()).chainId,
        verifyingContract: missionControl.address,
    };

    const types = {
        RaidMessage: [
            {name: '_defender', type: 'address'},
            {name: '_timestamp', type: 'uint256'},
            {name: '_raidId', type: 'uint256'}
        ],
    };

    const value = {
        _defender: defender.address,
        _timestamp: timestamp,
        _raidId: raidId
    };

    const signature = await signer._signTypedData(domain, types, value);
    return utils.splitSignature(signature);
};

const getCollectDigest = async (
    missionControl: Contract,
    signer: SignerWithAddress,
    user: SignerWithAddress,
    nonce: BigNumber
) => {
    const domain = {
        name: 'Mission Control',
        version: '1',
        chainId: (await ethers.provider.getNetwork()).chainId,
        verifyingContract: missionControl.address,
    };

    const types = {
        CollectMessage: [
            {name: '_user', type: 'address'},
            {name: '_nonce', type: 'uint256'}
        ],
    };

    const value = {
        _user: user.address,
        _nonce: nonce
    };

    const signature = await signer._signTypedData(domain, types, value);
    return utils.splitSignature(signature);
};