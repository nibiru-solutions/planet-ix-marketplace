import { expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { ERC721Mock, MissionControlStaking, Mock1155FT } from '../src/types';
import { exp } from '@prb/math';

describe('Mission Control Staking', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let streamer: SignerWithAddress;
  let bob: SignerWithAddress;
  let missionControlStaking: MissionControlStaking;
  let mockTileContracts: Mock1155FT;
  let mockPix: ERC721Mock;

  beforeEach(async function () {
    [owner, alice, bob, streamer] = await ethers.getSigners();

    let stakingFactory = await ethers.getContractFactory('MissionControlStaking');
    missionControlStaking = (await upgrades.deployProxy(stakingFactory, [])) as MissionControlStaking;

    let tileContractFactory = await ethers.getContractFactory('Mock1155FT');
    mockTileContracts = await tileContractFactory.deploy('TOY URI');

    let pixMockFactory = await ethers.getContractFactory('ERC721Mock');
    mockPix = await pixMockFactory.deploy('NAME', 'SYMBOL');

    await mockPix.mint(alice.address, 1);
    await mockPix.mint(alice.address, 2);
    await mockPix.mint(alice.address, 3);
    await mockPix.mint(alice.address, 11);
    await mockPix.mint(bob.address, 4);
    await mockPix.mint(bob.address, 5);
    await mockPix.mint(bob.address, 6);
    await mockPix.mint(bob.address, 7);

    await mockPix.connect(alice).setApprovalForAll(missionControlStaking.address, true);

    await mockTileContracts.mint(alice.address, 35, 10000);
    await mockTileContracts.mint(alice.address, 36, 10000);
    await mockTileContracts.mint(alice.address, 38, 10000);

    await mockTileContracts.connect(alice).setApprovalForAll(missionControlStaking.address, true);

    await missionControlStaking.whitelistTokens(mockTileContracts.address, [35], true);
    await missionControlStaking.whitelistTokens(mockTileContracts.address, [36], true);
    await missionControlStaking.whitelistTokens(mockTileContracts.address, [37], true);
    await missionControlStaking.whitelistTokens(mockPix.address, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], true);
  });

  describe('stakeNonFungible', () => {
    it('stake a non-fungible token', async function () {
      await missionControlStaking.connect(alice).stakeNonFungible(mockPix.address, 1, 0, { value: 1 });
      expect(await mockPix.balanceOf(missionControlStaking.address)).to.equal(1);
      expect(await mockPix.balanceOf(alice.address)).to.equal(3);
    });
    it("user should not be able to stake if they don't own the token", async () => {
      await expect(missionControlStaking.connect(alice).stakeNonFungible(mockPix.address, 4, 0, { value: 1 })).to.reverted;
    });
    it('reverts on non whitelisted token', async () => {
      await expect(missionControlStaking.connect(alice).stakeNonFungible(mockPix.address, 11, 0, { value: 1 })).to.reverted;
    });
    it('stake many 721', async () => {
      let aliceBalanceBefore = await (await mockPix.balanceOf(alice.address)).toNumber();
      let stakingBalanceBefore = await (await mockPix.balanceOf(missionControlStaking.address)).toNumber();
      await missionControlStaking.connect(alice).stakeManyNonFungible([mockPix.address, mockPix.address, mockPix.address], [1, 2, 3], 0, { value: 1 });
      expect(await mockPix.balanceOf(alice.address)).to.equal(aliceBalanceBefore - 3);
      expect(await mockPix.balanceOf(missionControlStaking.address)).to.equal(stakingBalanceBefore + 3);
    });
  });
  describe('stakeSemiFungible', () => {
    it('stake a semi-fungible token', async () => {
      await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 35, 10, 0, { value: 1 });
      expect(await mockTileContracts.balanceOf(missionControlStaking.address, 35)).to.equal(10);
      expect(await mockTileContracts.balanceOf(alice.address, 35)).to.equal(9990);
    });
    it('reverts if user tries to stake a token they have none of', async () => {
      await expect(missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 37, 10, 0, { value: 1 })).to.reverted;
    });

    it('reverts on non whitelisted token', async () => {
      await expect(missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 38, 10, 0, { value: 1 })).to.revertedWith(
        'MC_STAKING: TOKEN NOT WHITELISTED',
      );
    });
    it('reverts if user tries to stake too much of a particular tokenid', async () => {
      await expect(missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 35, 1000000, 0, { value: 1 })).to.revertedWith(
        'ERC1155: insufficient balance for transfer',
      );
    });
    it('stake many 1155', async () => {
      let aliceBalanceBefore35 = await (await mockTileContracts.balanceOf(alice.address, 35)).toNumber();
      let stakingBalanceBefore35 = await (await mockTileContracts.balanceOf(missionControlStaking.address, 35)).toNumber();

      let aliceBalanceBefore36 = await (await mockTileContracts.balanceOf(alice.address, 36)).toNumber();
      let stakingBalanceBefore36 = await (await mockTileContracts.balanceOf(missionControlStaking.address, 36)).toNumber();

      await missionControlStaking
        .connect(alice)
        .stakeManySemiFungible([mockTileContracts.address, mockTileContracts.address], [35, 36], [10, 10], 0, { value: 1 });

      expect(await mockTileContracts.balanceOf(alice.address, 35)).to.equal(aliceBalanceBefore35 - 10);
      expect(await mockTileContracts.balanceOf(missionControlStaking.address, 35)).to.equal(stakingBalanceBefore35 + 10);

      expect(await mockTileContracts.balanceOf(alice.address, 36)).to.equal(aliceBalanceBefore36 - 10);
      expect(await mockTileContracts.balanceOf(missionControlStaking.address, 36)).to.equal(stakingBalanceBefore36 + 10);
    });
  });
  describe('unstake', () => {
    beforeEach(async () => {
      await missionControlStaking.connect(alice).stakeNonFungible(mockPix.address, 1, 0, { value: 1 });
      await missionControlStaking.connect(alice).stakeNonFungible(mockPix.address, 2, 0, { value: 1 });
      await missionControlStaking.connect(alice).stakeNonFungible(mockPix.address, 3, 0, { value: 1 });

      await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 35, 10, 0, { value: 1 });
      await missionControlStaking.connect(alice).stakeSemiFungible(mockTileContracts.address, 36, 10, 0, { value: 1 });

      await increaseTime(3000000);
    });
    it('unstakes a 721', async () => {
      let aliceBalanceBefore = await (await mockPix.balanceOf(alice.address)).toNumber();
      let stakingBalanceBefore = await (await mockPix.balanceOf(missionControlStaking.address)).toNumber();
      await missionControlStaking.connect(alice).unstake(mockPix.address, 1, 1, 1, { value: 1 });
      expect(await mockPix.balanceOf(alice.address)).to.equal(aliceBalanceBefore + 1);
      expect(await mockPix.balanceOf(missionControlStaking.address)).to.equal(stakingBalanceBefore - 1);
    });
    it('unstake an 1155', async () => {
      let aliceBalanceBefore = await (await mockTileContracts.balanceOf(alice.address, 35)).toNumber();
      let stakingBalanceBefore = await (await mockTileContracts.balanceOf(missionControlStaking.address, 35)).toNumber();

      await missionControlStaking.connect(alice).unstake(mockTileContracts.address, 35, 1, 1, { value: 1 });

      expect(await mockTileContracts.balanceOf(alice.address, 35)).to.equal(aliceBalanceBefore + 1);
      expect(await mockTileContracts.balanceOf(missionControlStaking.address, 35)).to.equal(stakingBalanceBefore - 1);
    });
    it('unstakes many tokens at once', async () => {
      let aliceBalanceBefore35 = await (await mockTileContracts.balanceOf(alice.address, 35)).toNumber();
      let stakingBalanceBefore35 = await (await mockTileContracts.balanceOf(missionControlStaking.address, 35)).toNumber();

      let aliceBalanceBefore36 = await (await mockTileContracts.balanceOf(alice.address, 36)).toNumber();
      let stakingBalanceBefore36 = await (await mockTileContracts.balanceOf(missionControlStaking.address, 36)).toNumber();

      let aliceBalanceBefore721 = await (await mockPix.balanceOf(alice.address)).toNumber();
      let stakingBalanceBefore721 = await (await mockPix.balanceOf(missionControlStaking.address)).toNumber();

      await missionControlStaking
        .connect(alice)
        .unstakeMany(
          [mockPix.address, mockPix.address, mockPix.address, mockTileContracts.address, mockTileContracts.address],
          [1, 2, 3, 35, 36],
          [1, 1, 1, 10, 10],
          0,
          { value: 1 },
        );

      expect(await mockTileContracts.balanceOf(alice.address, 35)).to.equal(aliceBalanceBefore35 + 10);
      expect(await mockTileContracts.balanceOf(missionControlStaking.address, 35)).to.equal(stakingBalanceBefore35 - 10);

      expect(await mockTileContracts.balanceOf(alice.address, 36)).to.equal(aliceBalanceBefore36 + 10);
      expect(await mockTileContracts.balanceOf(missionControlStaking.address, 36)).to.equal(stakingBalanceBefore36 - 10);

      expect(await mockPix.balanceOf(alice.address)).to.equal(aliceBalanceBefore721 + 3);
      expect(await mockPix.balanceOf(missionControlStaking.address)).to.equal(stakingBalanceBefore721 - 3);
    });
  });
});

async function increaseTime(amount) {
  await hre.network.provider.send('evm_increaseTime', [amount]);
  await hre.network.provider.send('evm_mine');
  //console.log("EVM time " + amount + " seconds increased!")
}
