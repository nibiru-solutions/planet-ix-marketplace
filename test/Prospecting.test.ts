import { network, ethers, upgrades } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { expect } from 'chai';
import { BigNumber, Contract } from 'ethers';
import { exp } from '@prb/math';
import { max } from 'hardhat/internal/util/bigint';
import { describe } from 'mocha';

describe('Prospecting', function () {
  this.timeout(90000);
  let nft: Contract;
  let prospecting: Contract;
  let vrfCoordinatorV2Mock: Contract;
  let vrfManager: Contract;
  let emilCoin: Contract;
  let orderAutomation: Contract;
  let deployer: SignerWithAddress;
  let player: SignerWithAddress;
  let feeWallet: SignerWithAddress;
  let alice: SignerWithAddress;
  const WASTE_ID = 7;
  const ASTRO_CREDITS = 8;
  const LEGENDARY_BIOMOD = 14;
  const RARE_BIOMOD = 13;
  const UNCOMMON_BIOMOD = 12;
  const COMMON_BIOMOD = 11;
  const OUTLIER_BIOMOD = 10;
  const BLUEPRINT = 9;
  const NO_MINT = 0;
  const INITIAL_BALANCE = 500;
  const PROSPECTOR_FEE = 25;
  const PROSPECTOR_TIME = 21600;
  const MAX_ORDERS = 4;
  const SPEEDUP_TIME = 3600;
  const SPEEDUP_COST = 5;
  const PROSPECTOR_TAX = 500;

  const SUBSCRIPTION_ID = 1;
  const KEYHASH = ethers.utils.defaultAbiCoder.encode(['uint256'], [1]);
  const CALLBACK_GAS_LIMIT = 2000000;
  const REQUEST_CONFIRMATIONS = 3;
  const NUM_WORDS = 1;

  let pixtSpeedupSplitBps = 500;
  let pixtSpeedupCost = 500;
  let batchOrderInterval = 60;
  let batchMaxSize = 15;

  const EMIL_COINT_BALANCE = pixtSpeedupCost * 8;

  beforeEach(async () => {
    [deployer, player, feeWallet, alice] = await ethers.getSigners();

    let nftFactory = await ethers.getContractFactory('NFT');
    nft = await upgrades.deployProxy(nftFactory, ['PlanetIX', 'PIX']);

    let prospectingFactory = await ethers.getContractFactory('ProspectingV2');
    prospecting = await upgrades.deployProxy(prospectingFactory, [feeWallet.address, PROSPECTOR_FEE, PROSPECTOR_TIME, MAX_ORDERS, SPEEDUP_TIME, SPEEDUP_COST, PROSPECTOR_TAX]);

    let erc20Factory = await ethers.getContractFactory('ERC20Mock');
    emilCoin = await erc20Factory.deploy('EmilCoin', 'MXC');
    await emilCoin.mint(player.address, EMIL_COINT_BALANCE);

    let vrfFac = await ethers.getContractFactory('VRFManagerMock');
    vrfManager = await vrfFac.deploy();

    let baseLevelFactory = await ethers.getContractFactory('MockBaseLevel');
    let baseLevel = await upgrades.deployProxy(baseLevelFactory);
    await prospecting.setBaseLevelAddress(baseLevel.address);

    let mockStreamFactory = await ethers.getContractFactory('MockSFStream');
    let mockSFStream = await mockStreamFactory.deploy();
    await mockSFStream.setStream(await player.getAddress(), 0);

    let orderAutomationFactory = await ethers.getContractFactory('OrderAutomation');
    orderAutomation = await upgrades.deployProxy(orderAutomationFactory, [batchOrderInterval, batchMaxSize, prospecting.address]);
    await orderAutomation.setOracle(vrfManager.address);
    await prospecting.setOrderAutomation(orderAutomation.address);

    await nft.setTrustedContract(prospecting.address);
    await nft.setTrustedContract(deployer.address);
    await nft.setURI(WASTE_ID, 'TokenURI');
    await nft.setURI(ASTRO_CREDITS, 'TokenURI');
    await nft.setURI(LEGENDARY_BIOMOD, 'TokenURI');
    await nft.setURI(RARE_BIOMOD, 'TokenURI');
    await nft.setURI(UNCOMMON_BIOMOD, 'TokenURI');
    await nft.setURI(COMMON_BIOMOD, 'TokenURI');
    await nft.setURI(OUTLIER_BIOMOD, 'TokenURI');
    await nft.setURI(BLUEPRINT, 'TokenURI');
    await nft.trustedMint(player.address, WASTE_ID, INITIAL_BALANCE);
    await prospecting.setIAssetManager(nft.address);
    await prospecting.setBiomodWeights([LEGENDARY_BIOMOD, RARE_BIOMOD, UNCOMMON_BIOMOD, COMMON_BIOMOD, OUTLIER_BIOMOD, BLUEPRINT, NO_MINT], [20, 50, 100, 150, 200, 10, 300]);
  });

  async function reqRandNumAutomation() {
    await network.provider.send('evm_increaseTime', [batchOrderInterval + 1]);
    await network.provider.send('evm_mine');
    let checkUpkeep = await orderAutomation.checkUpkeep(1);
    //console.log("rett", checkUpkeep.upkeepNeeded)
    if (checkUpkeep.upkeepNeeded) {
      await orderAutomation.performUpkeep(1);
      return true;
    }
  }

  describe('Place Orders', () => {
    let numOrders;
    let creationtime;
    it('Place an order as expected', async () => {
      numOrders = 1;

      await expect(prospecting.connect(player).placeProspectingOrders(numOrders)).to.emit(prospecting, 'ProspectingOrderPlaced').withArgs(player.address, numOrders);

      expect(await nft.balanceOf(player.address, WASTE_ID)).to.equal(INITIAL_BALANCE - PROSPECTOR_FEE);

      expect(await nft.balanceOf(feeWallet.address, WASTE_ID)).to.equal(numOrders);

      let data = await prospecting.getOrders(player.address);
      creationtime = await (await ethers.provider.getBlock(await ethers.provider.getBlockNumber())).timestamp;
      expect(data[0]['createdAt']).to.equal(creationtime);
    });
    it('Place multiple orders as expected', async () => {
      numOrders = 4;

      await expect(prospecting.connect(player).placeProspectingOrders(numOrders)).to.emit(prospecting, 'ProspectingOrderPlaced').withArgs(player.address, numOrders);

      expect(await nft.balanceOf(player.address, WASTE_ID)).to.equal(INITIAL_BALANCE - PROSPECTOR_FEE * numOrders);

      expect(await nft.balanceOf(feeWallet.address, WASTE_ID)).to.equal(numOrders);
    });
    it('Place multiple orders as expected in separate transactions', async () => {
      let numOrders_1 = 2;
      let numOrders_2 = 2;
      await expect(prospecting.connect(player).placeProspectingOrders(numOrders_1)).to.emit(prospecting, 'ProspectingOrderPlaced').withArgs(player.address, numOrders_1);

      await expect(prospecting.connect(player).placeProspectingOrders(numOrders_2)).to.emit(prospecting, 'ProspectingOrderPlaced').withArgs(player.address, numOrders_2);

      expect(await nft.balanceOf(player.address, WASTE_ID)).to.equal(INITIAL_BALANCE - PROSPECTOR_FEE * numOrders);

      expect(await nft.balanceOf(feeWallet.address, WASTE_ID)).to.equal(numOrders);
    });
  });

  describe('Place Order reverts', () => {
    let numOrders;
    it('Reverts when placing too many orders in one transaction', async () => {
      numOrders = 5;
      await expect(prospecting.connect(player).placeProspectingOrders(numOrders)).to.revertedWith(`Prospector__MaxOrdersExceeded(${numOrders})`);
    });
    it('Reverts when submitting amount of orders that would make user go over the limit', async () => {
      let numOrders1 = 3;
      let numOrders2 = 4;
      prospecting.connect(player).placeProspectingOrders(numOrders1);
      await expect(prospecting.connect(player).placeProspectingOrders(numOrders2)).to.revertedWith(`Prospector__MaxOrdersExceeded(${numOrders1 + numOrders2})`);
    });
    it('Reverts when user with 0 WASTE balance tries to place an order', async () => {
      numOrders = 1;
      await expect(prospecting.connect(alice).placeProspectingOrders(numOrders)).to.revertedWith('ERC1155: burn amount exceeds balance');
    });
    it('Reverts when user with low balance tries to place too many orders', async () => {
      numOrders = 3;
      await nft.trustedMint(alice.address, WASTE_ID, 55);
      await expect(prospecting.connect(alice).placeProspectingOrders(numOrders)).to.revertedWith('ERC1155: burn amount exceeds balance');
    });
  });

  describe('Monte carlo claim', () => {
    const weights = [20, 15, 10, 5, 2, 1, 30];
    const sumWeights = weights.reduce((accumulator, current) => {
      return accumulator + current;
    }, 0);
    const tokenIds = [OUTLIER_BIOMOD, COMMON_BIOMOD, UNCOMMON_BIOMOD, RARE_BIOMOD, LEGENDARY_BIOMOD, BLUEPRINT, NO_MINT];
    beforeEach(async () => {
      await nft.trustedMint(player.address, WASTE_ID, 1000000);
      await prospecting.setProspectingTime(PROSPECTOR_TIME);
      await prospecting.setBiomodWeights(tokenIds, weights);
      let rand_seed = Math.floor(Math.random() * (20 - 1 + 1) + 1);
      console.log('random seed: ', rand_seed);
      await vrfManager.setSeed(rand_seed + 1);
    });
    it('n runs should give expected amount for each tokenId', async () => {
      let runs = 1000;

      let i = 0;
      while (i < runs) {
        //should make this async buf fk it.
        await prospecting.connect(player).placeProspectingOrders(1);
        await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME]);
        await network.provider.send('evm_mine');
        await reqRandNumAutomation();
        await vrfManager.fulfill(i + 1);
        await prospecting.connect(player).claimOrder(i);
        i++;
      }
      let claimSum = 0;
      for (let j = 0; j < tokenIds.length - 1; j++) {
        let balance = await nft.balanceOf(player.address, tokenIds[j]);
        claimSum += Number(balance);
        console.log('balance of ', Number(balance), (weights[j] / sumWeights) * runs);
        //expect(Number(balance)).to.approximately((weights[j] / sumWeights) * runs, runs * 0.05);
      }
      expect(claimSum).to.equal(runs - (runs - claimSum));
      console.log('balance of Nothing', runs - claimSum, (weights[weights.length - 1] / sumWeights) * runs);
    });
  });

  describe('Claim Prospecting', () => {
    let numOrders;
    let claimId;
    beforeEach(async () => {
      numOrders = 4;
      await prospecting.setBiomodWeights([LEGENDARY_BIOMOD, RARE_BIOMOD, UNCOMMON_BIOMOD, COMMON_BIOMOD, OUTLIER_BIOMOD, BLUEPRINT, NO_MINT], [20, 5, 10, 15, 2, 1, 30]);
      await prospecting.connect(player).placeProspectingOrders(numOrders);
      await reqRandNumAutomation();
      await vrfManager.setSeed(6);
      await vrfManager.fulfill(1);
      /*for(let i = 0; i <numOrders; i ++){
                await vrfManager.fulfill(i+1);
            }*/
    });
    it('Claims as expected', async () => {
      claimId = 0;
      let mintedBiomod;
      let requestId = 1;

      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME]);
      await network.provider.send('evm_mine');

      let tx = await prospecting.connect(player).claimOrder(claimId);

      await expect(tx).to.emit(prospecting, 'RandomBiomodMinted');

      expect(await nft.balanceOf(player.address, LEGENDARY_BIOMOD)).to.equal(1);
    });
    it('Claims all orders as expected', async () => {
      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME]);
      await network.provider.send('evm_mine');

      await expect(prospecting.connect(player).claimOrder(0)).to.emit(prospecting, 'RandomBiomodMinted').withArgs(player.address, 14, 0); // correct output for verf with seed 4.

      await expect(prospecting.connect(player).claimOrder(1)).to.emit(prospecting, 'RandomBiomodMinted').withArgs(player.address, 11, 1);

      await expect(prospecting.connect(player).claimOrder(2)).to.emit(prospecting, 'RandomBiomodMinted').withArgs(player.address, 14, 2);

      await expect(prospecting.connect(player).claimOrder(3)).to.emit(prospecting, 'RandomBiomodMinted').withArgs(player.address, 0, 3);
    });
  });

  describe('Claim Prospecting Reverts', () => {
    let numOrders;
    let claimId;
    beforeEach(async () => {
      numOrders = 1;
      await prospecting.connect(player).placeProspectingOrders(numOrders);
      await reqRandNumAutomation();
      await vrfManager.fulfill(1);
    });
    it('Reverts if user has placed no orders', async () => {
      claimId = 0;
      await expect(prospecting.connect(alice).claimOrder(0)).to.revertedWith(`Prospector__NoOrders("${alice.address}")`);
    });
    it('Reverts if trying to claim order that has not been placed', async () => {
      claimId = 1;
      await expect(prospecting.connect(player).claimOrder(claimId)).to.revertedWith(`Prospector__InvalidId(${claimId})`);
    });
    it('Reverts if trying to claim order twice', async () => {
      claimId = 0;
      await prospecting.connect(player).placeProspectingOrders(numOrders);
      await reqRandNumAutomation();
      await vrfManager.fulfill(2);
      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME * 2]);
      await prospecting.connect(player).claimOrder(claimId);
      await expect(prospecting.connect(player).claimOrder(claimId)).to.revertedWith(`Prospector__InvalidId(0)`);
    });
    it('Reverts if trying to claim before enough time has passed', async () => {
      claimId = 0;
      await expect(prospecting.connect(player).claimOrder(claimId)).to.revertedWith(`Prospecting__OrderNotYetCompleted("${player.address}", ${claimId})`);
    });
    it('Reverts if no orders left', async () => {
      claimId = 0;

      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME]);
      await network.provider.send('evm_mine');

      await expect(prospecting.connect(player).claimOrder(claimId)).to.emit(prospecting, 'RandomBiomodMinted');

      await expect(prospecting.connect(player).claimOrder(claimId)).to.revertedWith(`Prospector__NoOrders("${player.address}")`);
    });
  });

  describe('Speed up orders', async () => {
    let numOrders;
    let numSpeedups;
    let claimId;
    let creationTime;
    const ASTRO_BALANCE = 100;
    beforeEach(async () => {
      numOrders = 4;

      await nft.trustedMint(player.address, ASTRO_CREDITS, ASTRO_BALANCE);
      await nft.trustedMint(alice.address, ASTRO_CREDITS, ASTRO_BALANCE);
      await prospecting.connect(player).placeProspectingOrders(numOrders);
      await reqRandNumAutomation();
      await vrfManager.fulfill(1);
      /*for(let i = 0; i <numOrders; i ++){
                await vrfManager.fulfill(i+1);
            }*/
    });
    it('Speeds up as expected', async () => {
      numSpeedups = 2;
      claimId = 0;

      console.log('astobefore speedup ', await nft.balanceOf(player.address, ASTRO_CREDITS));
      await expect(prospecting.connect(player).speedUpOrder(numSpeedups, 0));
      await expect(prospecting.connect(player).speedUpOrder(numSpeedups, 1));
      await expect(prospecting.connect(player).speedUpOrder(numSpeedups, 2));
      await expect(prospecting.connect(player).speedUpOrder(numSpeedups, 3)).to.emit(prospecting, 'ProspectingOrderSpedUp');

      let data = await prospecting.getOrders(player.address);
      console.log('astro after speedup ', await nft.balanceOf(player.address, ASTRO_CREDITS));

      let waitTime1 = data[0]['totalCompletionTime']; //await prospecting.getOrderCompletionTime(0, player.address);
      let waitTime2 = data[1]['totalCompletionTime']; //await prospecting.getOrderCompletionTime(1, player.address);
      let waitTime3 = data[2]['totalCompletionTime']; //await prospecting.getOrderCompletionTime(2, player.address);
      let waitTime4 = data[3]['totalCompletionTime']; //await prospecting.getOrderCompletionTime(3, player.address);
      expect(Number(waitTime1)).to.closeTo(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups, 2);
      expect(Number(waitTime2)).to.closeTo(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups, 2);
      expect(Number(waitTime3)).to.closeTo(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups, 2);
      expect(Number(waitTime4)).to.closeTo(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups, 2);

      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups]);
      await network.provider.send('evm_mine');
      expect(await nft.balanceOf(player.address, ASTRO_CREDITS)).to.equal(ASTRO_BALANCE - SPEEDUP_COST * numSpeedups * 4);
    });
    it('Executes multiple speedups as expected', async () => {
      numSpeedups = 2;
      claimId = 0;
      numOrders = 4;
      await prospecting.connect(player).speedUpOrder(numSpeedups, 0);
      await prospecting.connect(player).speedUpOrder(numSpeedups, 1);
      await prospecting.connect(player).speedUpOrder(numSpeedups, 2);
      await expect(prospecting.connect(player).speedUpOrder(numSpeedups, 3)).to.emit(prospecting, 'ProspectingOrderSpedUp');

      let data = await prospecting.getOrders(player.address);
      let waitTime1 = data[0]['totalCompletionTime'];
      let waitTime2 = data[1]['totalCompletionTime'];
      let waitTime3 = data[2]['totalCompletionTime'];
      let waitTime4 = data[3]['totalCompletionTime'];
      expect(waitTime1.toNumber()).to.closeTo(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups, 2);
      expect(waitTime2.toNumber()).to.closeTo(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups, 2);
      expect(waitTime3.toNumber()).to.closeTo(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups, 2);
      expect(waitTime4.toNumber()).to.closeTo(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups, 2);

      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedups]);
      await network.provider.send('evm_mine');

      await expect(prospecting.connect(player).claimOrder(claimId)).to.emit(prospecting, 'RandomBiomodMinted');

      expect(await nft.balanceOf(player.address, ASTRO_CREDITS)).to.equal(ASTRO_BALANCE - SPEEDUP_COST * numSpeedups * numOrders);
    });

    it('Speeds up orders beyond total wait time', async () => {
      numSpeedups = 20;
      claimId = 0;
      await expect(prospecting.connect(player).speedUpOrder(numSpeedups, claimId)).to.emit(prospecting, 'ProspectingOrderSpedUp');
      //.withArgs(player.address, numSpeedups, creationTime - SPEEDUP_TIME * numSpeedups);

      let data = await prospecting.getOrders(player.address);
      let waitTime1 = data[0]['totalCompletionTime'];
      expect(waitTime1).to.equal(0);
      await expect(prospecting.connect(player).claimOrder(claimId)).to.emit(prospecting, 'RandomBiomodMinted');

      expect(await nft.balanceOf(player.address, ASTRO_CREDITS)).to.equal(ASTRO_BALANCE - SPEEDUP_COST * numSpeedups);
    });

    it('should successfully speed up all orders with AstroCredits', async () => {
      const numSpeedUps = 2;
      const totalSpeedupCost = SPEEDUP_COST * numSpeedUps * numOrders;

      const initialAstroCreditsBalance = await nft.balanceOf(player.address, ASTRO_CREDITS);
      await prospecting.connect(player).speedupAllAC(numSpeedUps);

      const newAstroCreditsBalance = await nft.balanceOf(player.address, ASTRO_CREDITS);
      expect(newAstroCreditsBalance).to.equal(initialAstroCreditsBalance - totalSpeedupCost);

      const orders = await prospecting.getOrders(player.address);
      for (let i = 0; i < numOrders; i++) {
        expect(orders[i].speedUpDeductedAmount).to.equal(SPEEDUP_TIME * numSpeedUps);
        expect(orders[i].totalCompletionTime).to.be.at.most(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedUps);
      }
    });

    it('should not allow to speed up orders if there are no AstroCredits', async () => {
      const numSpeedUps = 2;

      console.log('Initial AstroCredits balance:', (await nft.balanceOf(player.address, ASTRO_CREDITS)).toString());

      console.log('Burning all AstroCredits...');
      await nft.trustedBurn(player.address, ASTRO_CREDITS, ASTRO_BALANCE);

      console.log('New AstroCredits balance:', (await nft.balanceOf(player.address, ASTRO_CREDITS)).toString());

      console.log(`Attempting to apply ${numSpeedUps} speed ups to all orders without AstroCredits...`);
      await expect(prospecting.connect(player).speedupAllAC(numSpeedUps)).to.be.revertedWith('ERC1155: burn amount exceeds balance');
    });

    it('should not allow to speed up orders with zero speedup amount', async () => {
      await expect(prospecting.connect(player).speedupAllAC(0)).to.be.revertedWith('Prospector__InvalidSpeedupAmount');
    });

    it('should not allow to speed up orders if the user has no orders', async () => {
      const numSpeedUps = 1;
      await expect(prospecting.connect(alice).speedupAllAC(numSpeedUps)).to.be.revertedWith('Prospector__NoOrders');
    });
  });
  describe('Speed up orders with erc20', async () => {
    let numOrders;
    let numSpeedups;
    let claimId;
    let creationTime;
    beforeEach(async () => {
      numOrders = 4;
      await prospecting.connect(player).placeProspectingOrders(numOrders);
      await prospecting.setIXTSpeedUpParams(
        emilCoin.address,
        emilCoin.address, //isn't used yet.
        pixtSpeedupSplitBps,
        pixtSpeedupCost,
      );
    });
    it('Emits event', async () => {
      numSpeedups = 1;
      claimId = 0;
      let priorBalance = await emilCoin.balanceOf(player.address);
      await emilCoin.connect(player).approve(prospecting.address, numSpeedups * pixtSpeedupCost);
      await expect(prospecting.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.emit(prospecting, 'ProspectingOrderSpedUp').withArgs(player.address, numSpeedups, claimId);
    });
    it('Draws coins as expected', async () => {
      numSpeedups = 1;
      claimId = 0;
      let priorBalance = await emilCoin.balanceOf(player.address);
      await emilCoin.connect(player).approve(prospecting.address, numSpeedups * pixtSpeedupCost);
      await expect(prospecting.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.not.reverted;
      expect(priorBalance - (await emilCoin.balanceOf(player.address))).to.equal(numSpeedups * pixtSpeedupCost);
    });

    it('Deposits expected amount into contract', async () => {
      numSpeedups = 1;
      claimId = 0;
      let priorBalance = await emilCoin.balanceOf(prospecting.address);
      await emilCoin.connect(player).approve(prospecting.address, numSpeedups * pixtSpeedupCost);
      await expect(prospecting.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.not.reverted;
      expect((await emilCoin.balanceOf(prospecting.address)) - priorBalance).to.equal(BigNumber.from(numSpeedups * pixtSpeedupCost * (10000 - pixtSpeedupSplitBps)).div(BigNumber.from(10000)));
    });

    it('Speeds up as expected', async () => {
      numSpeedups = 5;
      claimId = 0;
      let priorOrder = (await prospecting.getOrders(player.address))[0];
      await emilCoin.connect(player).approve(prospecting.address, numSpeedups * pixtSpeedupCost);
      await expect(prospecting.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.not.reverted;
      let posteriorOrder = (await prospecting.getOrders(player.address))[0];
      expect(priorOrder['totalCompletionTime'] - posteriorOrder['totalCompletionTime']).to.equal(numSpeedups * SPEEDUP_TIME);
    });

    it('should not allow to speed up orders if there are no IXT tokens', async () => {
      const numSpeedUps = 2;
      await emilCoin.connect(player).transfer(alice.address, EMIL_COINT_BALANCE);
      await expect(prospecting.connect(player).speedupAllIXT(numSpeedUps)).to.be.revertedWith('ERC20: transfer amount exceeds balance');
    });

    it('should not allow to speed up orders if there are not enough IXT tokens', async () => {
      const numSpeedUps = 2;
      const balance = await emilCoin.balanceOf(player.address);
      await emilCoin.connect(player).transfer(alice.address, balance - EMIL_COINT_BALANCE / 2);
      await expect(prospecting.connect(player).speedupAllIXT(numSpeedUps)).to.be.revertedWith('ERC20: transfer amount exceeds balance');
    });

    it('should not allow to speed up orders with 0 speedups', async () => {
      const numSpeedUps = 0;
      await expect(prospecting.connect(player).speedupAllIXT(numSpeedUps)).to.be.revertedWith('Prospector__InvalidSpeedupAmount()');
    });

    it('should allow to speed up orders with valid parameters', async () => {
      const numSpeedUps = 2;
      const balance = await emilCoin.balanceOf(player.address);
      await emilCoin.connect(player).approve(prospecting.address, balance);
      await prospecting.connect(player).speedupAllIXT(numSpeedUps);

      const orders = await prospecting.getOrders(player.address);
      for (const order of orders) {
        expect(order.speedUpDeductedAmount).to.equal(SPEEDUP_TIME * numSpeedUps);
        expect(order.totalCompletionTime).to.be.at.most(PROSPECTOR_TIME - SPEEDUP_TIME * numSpeedUps);
      }
    });

    it('should burn the correct amount of IXT tokens and transfer to the correct address', async () => {
      const numSpeedUps = 2;
      const balance = await emilCoin.balanceOf(player.address);
      await emilCoin.connect(player).approve(prospecting.address, balance);
      await prospecting.connect(player).speedupAllIXT(numSpeedUps);

      const orders = await prospecting.getOrders(player.address);
      const expectedBurnAmount = (pixtSpeedupCost * numSpeedUps * pixtSpeedupSplitBps * orders.length) / 10000;
      expect(await emilCoin.balanceOf(prospecting.address)).to.equal(pixtSpeedupCost * numSpeedUps * orders.length - expectedBurnAmount);
      expect(await emilCoin.balanceOf(player.address)).to.equal(EMIL_COINT_BALANCE - pixtSpeedupCost * numSpeedUps * orders.length);
    });
  });
  describe('Revert Speed up order with erc20', () => {
    let orderAmount = 50;
    let numSpeedups = 1;
    beforeEach(async () => {
      await prospecting.connect(player).placeProspectingOrders(1);
    });
    it('Revert when IXTParams is not set', async () => {
      await expect(prospecting.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith('IXT address not set');
    });
    it('Revert when not enough erc20 in player wallet', async () => {
      await prospecting.setIXTSpeedUpParams(emilCoin.address, emilCoin.address, pixtSpeedupSplitBps, pixtSpeedupCost);
      await emilCoin.connect(player).transfer(deployer.getAddress(), EMIL_COINT_BALANCE);
      //await expect(prospecting.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith("Transfer of funds failed") // should be this...
      await expect(prospecting.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith('ERC20: transfer amount exceeds balance');
    });
    it('Reverts place order when paused', async () => {
      await prospecting.setPaused(true);
      await expect(prospecting.connect(player).placeProspectingOrders(1)).to.revertedWith(`Prospector__IsPaused()`);
      await prospecting.setPaused(false);
      await prospecting.connect(player).placeProspectingOrders(1);
    });
    it('Reverts when not owner pause', async () => {
      await expect(prospecting.connect(player).setPaused(true)).to.revertedWith('Ownable: caller is not the owner');
    });
  });

  describe('Speed up orders reverts', async () => {
    let numOrders;
    let numSpeedups;
    let claimId;
    const ASTRO_BALANCE = 100;
    beforeEach(async () => {
      numOrders = 4;
      await prospecting.connect(player).placeProspectingOrders(numOrders);
      await nft.trustedMint(player.address, ASTRO_CREDITS, ASTRO_BALANCE);
    });
    it('Reverts on player having no orders', async () => {
      numSpeedups = 1;
      await expect(prospecting.connect(alice).speedUpOrder(numSpeedups, 0)).to.revertedWith(`Prospector__NoOrders("${alice.address}")`);
    });
    it('Reverts on invalid speedup amount', async () => {
      numSpeedups = 0;
      await nft.trustedMint(alice.address, WASTE_ID, INITIAL_BALANCE);
      await nft.trustedMint(alice.address, ASTRO_CREDITS, ASTRO_BALANCE);
      await prospecting.connect(alice).placeProspectingOrders(numOrders);
      await expect(prospecting.connect(alice).speedUpOrder(numSpeedups, 0)).to.revertedWith(`Prospector__InvalidSpeedupAmount()`);
    });
    it('Reverts on player astro credit balance being insufficient', async () => {
      numSpeedups = 1;
      await nft.trustedMint(alice.address, WASTE_ID, INITIAL_BALANCE);
      await prospecting.connect(alice).placeProspectingOrders(numOrders);
      //order id 4,5,6
      for (let i = 4; i < 4 + numOrders - 1; i++) {
        //disgusting
        await expect(prospecting.connect(alice).speedUpOrder(numSpeedups, i));
      }
      //orderid 7
      await expect(prospecting.connect(alice).speedUpOrder(numSpeedups, 4 + 3)).to.revertedWith(`ERC1155: burn amount exceeds balance`);
    });
  });
  describe('Set biomod weights', async () => {
    it('Sets biomod weights as expected', async () => {
      await expect(prospecting.setBiomodWeights([LEGENDARY_BIOMOD, RARE_BIOMOD, UNCOMMON_BIOMOD, COMMON_BIOMOD, OUTLIER_BIOMOD, BLUEPRINT, NO_MINT], [2, 5, 10, 15, 20, 1, 30])).to.not.reverted;
    });
    it('Reverts when array lengths dont match', async () => {
      await expect(prospecting.setBiomodWeights([LEGENDARY_BIOMOD, RARE_BIOMOD, UNCOMMON_BIOMOD, COMMON_BIOMOD, OUTLIER_BIOMOD, BLUEPRINT, NO_MINT], [2, 5, 10, 15, 20])).to.revertedWith(
        'Prospector__NonmatchingArrays()',
      );
    });
    it('Reverts when empty arrays passed', async () => {
      await expect(prospecting.setBiomodWeights([], [])).to.revertedWith(`Prospector__InvalidArray()`);
    });
  });
  describe('OrderAutomation', async () => {
    let numOrders;
    beforeEach(async () => {
      numOrders = 4;
      await nft.trustedMint(alice.address, WASTE_ID, INITIAL_BALANCE);
      //await prospecting.connect(player).placeProspectingOrders(numOrders);
      await nft.trustedMint(player.address, ASTRO_CREDITS, INITIAL_BALANCE);
    });
    it('two player claim order with one vrf call ', async () => {
      await prospecting.connect(player).placeProspectingOrders(1);
      await prospecting.connect(alice).placeProspectingOrders(1);

      await reqRandNumAutomation();
      await vrfManager.fulfill(1);
      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME]);
      await network.provider.send('evm_mine');
      await expect(prospecting.connect(player).claimOrder(0)).to.emit(prospecting, 'RandomBiomodMinted');
      await expect(prospecting.connect(alice).claimOrder(1)).to.emit(prospecting, 'RandomBiomodMinted');
    });
    it('expect correct queue length', async () => {
      await prospecting.connect(player).placeProspectingOrders(1);
      await expect(await orderAutomation.queueLength()).to.equal(1);
      await reqRandNumAutomation();
      await expect(await orderAutomation.queueLength()).to.equal(1);
      await prospecting.connect(alice).placeProspectingOrders(1);
      await expect(await orderAutomation.queueLength()).to.equal(2);
      await prospecting.connect(player).placeProspectingOrders(1);
      await expect(await orderAutomation.queueLength()).to.equal(2);

      await vrfManager.fulfill(1);
      await expect(await orderAutomation.queueLength()).to.equal(1);
    });
    it('expect correct queue length 2', async () => {
      let players = [];
      let batchSize = 15;
      await orderAutomation.setBatchMaxSize(batchSize);
      for (let j = 0; j < batchSize; j++) {
        let tempPlayer;
        tempPlayer = await ethers.Wallet.createRandom();
        tempPlayer = tempPlayer.connect(ethers.provider);
        await nft.trustedMint(tempPlayer.address, WASTE_ID, INITIAL_BALANCE);
        await deployer.sendTransaction({ to: tempPlayer.address, value: ethers.utils.parseEther('1') });
        players.push(tempPlayer);
      }

      for (let k = 0; k < MAX_ORDERS; k++) {
        for (let s = 0; s < players.length; s++) {
          await prospecting.connect(players[s]).placeProspectingOrders(1);
        }
        await expect(await orderAutomation.queueLength()).to.equal(k + 1);
      }

      await reqRandNumAutomation();
      await vrfManager.fulfill(1);
      await expect(await orderAutomation.queueLength()).to.equal(MAX_ORDERS - 1);
    });
    it('expect pending', async () => {
      let i = 0;
      let t;
      let pending;
      let queueOrders;
      await prospecting.connect(player).placeProspectingOrders(1);
      let onceTrue = false;
      for (let i = 0; i < 61; i++) {
        await network.provider.send('evm_increaseTime', [1]);
        await network.provider.send('evm_mine');
        let ret = await reqRandNumAutomation();
        [t, pending, queueOrders] = await orderAutomation.getFirstBatch();
        //expect should perform upkeep to be true once
        if (ret) {
          expect(pending).to.equal(true);
          onceTrue = true;
        }
        if (onceTrue) {
          expect(pending).to.equal(true);
        } else expect(pending).to.equal(false);
      }
      await vrfManager.fulfill(1);
      await prospecting.connect(player).placeProspectingOrders(1);
      [t, pending, queueOrders] = await orderAutomation.getFirstBatch();
      expect(pending).to.equal(false);
    });
    it('Should handle many orders sim', async () => {
      //schizo..
      let i = 0;
      let players = [];
      let vrf = 1;
      orderAutomation.setTimeInterval(600);
      while (i < 120) {
        let tempPlayer;
        tempPlayer = await ethers.Wallet.createRandom();
        //console.log("iteration : ",i, "player: ", tempPlayer.address)
        tempPlayer = tempPlayer.connect(ethers.provider);
        players.push(tempPlayer);
        await nft.trustedMint(tempPlayer.address, WASTE_ID, INITIAL_BALANCE);
        await deployer.sendTransaction({ to: tempPlayer.address, value: ethers.utils.parseEther('1') });
        await prospecting.connect(tempPlayer).placeProspectingOrders(4);
        let performed = await reqRandNumAutomation();
        if (performed) {
          //don't fulfill the last batch
          if (i != 119) {
            await vrfManager.fulfill(vrf);
            vrf++;
            console.log('len after fulfill', await orderAutomation.queueLength());
          }
        }
        i++;
      }
      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME]);
      await network.provider.send('evm_mine');
      let t;
      let pending;
      let queueOrders;
      [t, pending, queueOrders] = await orderAutomation.getFirstBatch();
      let temp = [];
      for (t of queueOrders) {
        temp.push(Number(t));
      }
      queueOrders = temp;
      for (let p = 0; p < players.length; p++) {
        let playerOrders = await prospecting.getOrders(players[p].address);
        let oId = playerOrders[0].orderId;
        if (queueOrders.includes(Number(oId))) {
          await expect(prospecting.connect(players[p]).claimOrder(Number(oId))).to.revertedWith('Random Number not fulfilled');
        } else {
          expect(await prospecting.connect(players[p]).claimOrder(Number(oId))).to.emit(prospecting, 'RandomBiomodMinted');
        }
      }
      console.log('player size: ', players.length);
    });
    it('fulfill gas test', async () => {
      let i = 0;
      let players = [];
      let batchSize = 100;
      await orderAutomation.setBatchMaxSize(batchSize);
      while (i < batchSize) {
        let tempPlayer;
        tempPlayer = await ethers.Wallet.createRandom();
        //console.log("iteration : ",i, "player: ", tempPlayer.address)
        tempPlayer = tempPlayer.connect(ethers.provider);
        players.push(tempPlayer);
        await nft.trustedMint(tempPlayer.address, WASTE_ID, INITIAL_BALANCE);
        await deployer.sendTransaction({ to: tempPlayer.address, value: ethers.utils.parseEther('1') });
        await prospecting.connect(tempPlayer).placeProspectingOrders(1);
        i++;
      }
      //await prospecting.connect(player).placeProspectingOrders(1);
      await reqRandNumAutomation();

      await vrfManager.fulfill(1);
      console.log('len: ', Number(await orderAutomation.queueLength()));
    });
    it('revert when not fulfilled', async () => {
      await prospecting.connect(player).placeProspectingOrders(1);
      await network.provider.send('evm_increaseTime', [PROSPECTOR_TIME]);
      await network.provider.send('evm_mine');
      await expect(prospecting.connect(player).claimOrder(0)).to.revertedWith('Random Number not fulfilled');
      await reqRandNumAutomation();

      await vrfManager.fulfill(1);
      await expect(prospecting.connect(player).claimOrder(0)).to.emit(prospecting, 'RandomBiomodMinted');
    });
  });
});