import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { ERC20Mock, ERC721Mock, LuckyCatGeoLottery, LuckyCatGeoReward, PIXTerritoryStakingMock, VRFCoordinatorV2Mock } from '../src/types';
import { MerkleTree } from 'merkletreejs';
import { keccak256 } from 'ethereumjs-util';

describe('Lucky Cat Geo Lottery', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;
  let vrfCoordinatorV2Mock: VRFCoordinatorV2Mock;
  let geoLottery: LuckyCatGeoLottery;
  let aGoldMock: ERC20Mock;
  let territoryMock: ERC721Mock;
  let rewardMock: LuckyCatGeoReward;
  let territoryStakingMock: PIXTerritoryStakingMock;

  const SUBSCRIPTION_ID = 1;
  const KEYHASH = ethers.utils.defaultAbiCoder.encode(['uint256'], [1]);
  const TICKET_PRICE = ethers.utils.parseEther('10');
  const MAX_ENTRIES = 200;
  const POOL_SHARES = [1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000];


  const toEth = (input: number) => {
    return ethers.utils.parseUnits(input.toString(), "ether");
  }

  beforeEach(async () => {
    [owner, alice, bob] = await ethers.getSigners();

    let VRFCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
    vrfCoordinatorV2Mock = await VRFCoordinatorV2MockFactory.deploy(0, 6);
    await vrfCoordinatorV2Mock.createSubscription();
    await vrfCoordinatorV2Mock.fundSubscription(SUBSCRIPTION_ID, ethers.utils.parseEther('1000000'));

    let AGoldMockFactory = await ethers.getContractFactory('ERC20Mock');
    aGoldMock = await AGoldMockFactory.deploy('AGOLD', 'AGD');

    let TerritoryMockFactory = await ethers.getContractFactory('ERC721Mock');
    territoryMock = await TerritoryMockFactory.deploy('TERRITORY', 'PIX');

    let TerritoryStakingMockFactory = await ethers.getContractFactory('PIXTerritoryStakingMock');
    territoryStakingMock = await TerritoryStakingMockFactory.deploy();

    let GeoRewardMockFactory = await ethers.getContractFactory('LuckyCatGeoReward');
    rewardMock = await upgrades.deployProxy(GeoRewardMockFactory, []) as LuckyCatGeoReward;

    const geoLotteryFactory = await ethers.getContractFactory('LuckyCatGeoLottery');
    geoLottery = (await upgrades.deployProxy(geoLotteryFactory, [vrfCoordinatorV2Mock.address, KEYHASH, SUBSCRIPTION_ID])) as LuckyCatGeoLottery;

    await geoLottery.setAGOLD(aGoldMock.address);
    await geoLottery.setTicketPrice(TICKET_PRICE);
    await geoLottery.setMaxEntries(MAX_ENTRIES);
    await geoLottery.setLuckyCatGeoReward(rewardMock.address);

    await rewardMock.setGeoLottery(geoLottery.address);
    await rewardMock.setAGOLD(aGoldMock.address);

    await vrfCoordinatorV2Mock.addConsumer(SUBSCRIPTION_ID, geoLottery.address);
  });

  describe('Entering lottery normally', () => {
    const lotteryId = 0;
    beforeEach(async () => {
      await aGoldMock.mint(alice.address, ethers.utils.parseEther('100'));
      await territoryMock.mint(alice.address, 100);
    });

    it('Should emit event upon user entering lottery', async () => {
      const _entries = 1;
      await aGoldMock.connect(alice).approve(geoLottery.address, TICKET_PRICE.mul(_entries));
      await expect(geoLottery.connect(alice).enterLottery(_entries)).to.emit(geoLottery, 'UserEntered').withArgs(alice.address, lotteryId, _entries);
    });

    it('Should allow user to enter with more than one entries', async () => {
      const _entries = 3;
      await aGoldMock.connect(alice).approve(geoLottery.address, TICKET_PRICE.mul(_entries));
      await geoLottery.connect(alice).enterLottery(_entries);
      const userEntries = await geoLottery.userEntries(alice.address, lotteryId);
      expect(userEntries).to.equal(_entries);
    });

    it('Should revert if entries exceeds max entries', async () => {
      const _entries = MAX_ENTRIES + 1;
      await aGoldMock.connect(alice).approve(geoLottery.address, TICKET_PRICE.mul(_entries));
      await expect(geoLottery.connect(alice).enterLottery(_entries)).to.be.revertedWith('Max entries exceeded');
    });

    it('Should revert if user has insufficient funds for tickets', async () => {
      const _entries = 100;
      await aGoldMock.connect(alice).approve(geoLottery.address, TICKET_PRICE.mul(_entries));
      await expect(geoLottery.connect(alice).enterLottery(_entries)).to.be.revertedWith('ERC20: transfer amount exceeds balance');
    });

    it('Should revert if lottery paused', async () => {
      await geoLottery.stop();
      const _entries = 1;
      await aGoldMock.connect(alice).approve(geoLottery.address, TICKET_PRICE.mul(_entries));
      await expect(geoLottery.connect(alice).enterLottery(_entries)).to.revertedWith('Pausable: paused');
    });

    it('Should revert if spender not approved', async () => {
      const _entries = 1;
      await expect(geoLottery.connect(alice).enterLottery(_entries)).to.revertedWith('ERC20: transfer amount exceeds allowance');
    });
  });

  describe('Subscribing to lottery entries', () => {
    const lotteryId = 0;
    const flowRate = 10;
    const initialFlowRate = 100;
    const updatedFlowRate = 200;
    const updatedFlowRate2 = 300;

    beforeEach(async () => {
      await geoLottery.setGeoStream(owner.address);
    });
    it('stream started without issues', async () => {
      const beforeTimestamp = (await ethers.provider.getBlock('latest')).timestamp;

      await geoLottery.onStreamStarted(alice.address, flowRate);

      const afterTimestamp = (await ethers.provider.getBlock('latest')).timestamp;

      const updatedAt = await geoLottery.userStreamUpdatedAt(alice.address, lotteryId);
      expect(Number(updatedAt)).to.be.within(beforeTimestamp, afterTimestamp);

      const userFlowRate = await geoLottery.userLotteryRate(alice.address, lotteryId);
      expect(Number(userFlowRate)).to.equal(flowRate);

      const userFlowStatus = await geoLottery.userFlowActive(alice.address);
      expect(userFlowStatus).to.be.true;

      await expect(geoLottery.onStreamStarted(alice.address, flowRate)).to.emit(geoLottery, 'UserStreamStarted');
    });

    it('stream updated without issues after 3 days', async () => {
      // Start the stream
      await geoLottery.onStreamStarted(alice.address, initialFlowRate);
      const startTimestamp = (await ethers.provider.getBlock('latest')).timestamp;
      console.log('Start timestamp:', startTimestamp);

      // Increase the time by 3 days
      await ethers.provider.send('evm_increaseTime', [259200]); // 3 * 24 * 60 * 60
      await ethers.provider.send('evm_mine', []);

      // Update the stream
      await geoLottery.onStreamUpdated(alice.address, updatedFlowRate, initialFlowRate);
      // Get the expected userAmountStreamed
      const timeElapsedSinceLastUpdate = (await ethers.provider.getBlock('latest')).timestamp - startTimestamp;
      console.log('Time elapsed since last update:', timeElapsedSinceLastUpdate);
      const expectedAmountStreamed = timeElapsedSinceLastUpdate * initialFlowRate;
      console.log('Expected amount streamed:', expectedAmountStreamed);

      // Check userStreamUpdatedAt mapping
      const updatedAt = await geoLottery.userStreamUpdatedAt(alice.address, lotteryId);
      console.log('Updated at:', updatedAt);
      expect(Number(updatedAt)).to.equal((await ethers.provider.getBlock('latest')).timestamp);

      // Check userLotteryRate mapping
      const userFlowRate = await geoLottery.userLotteryRate(alice.address, lotteryId);
      console.log('User flow rate:', userFlowRate);
      expect(Number(userFlowRate)).to.equal(updatedFlowRate);

      // Check userAmountStreamed mapping
      const amountStreamed = await geoLottery.userAmountStreamed(alice.address, lotteryId);
      console.log('Amount streamed:', amountStreamed);
      expect(Number(amountStreamed)).to.equal(expectedAmountStreamed);

      // Check emitted event
      await expect(geoLottery.onStreamUpdated(alice.address, updatedFlowRate, initialFlowRate)).to.emit(geoLottery, 'UserStreamUpdated');
    });

    it('stream updated without issues after 3 days, then again after 2 more days', async () => {
      // Start the stream
      await geoLottery.onStreamStarted(alice.address, initialFlowRate);
      const startTimestamp = (await ethers.provider.getBlock('latest')).timestamp;

      // Increase the time by 3 days
      await ethers.provider.send('evm_increaseTime', [259200]); // 3 * 24 * 60 * 60
      await ethers.provider.send('evm_mine', []);

      // First Update the stream
      await geoLottery.onStreamUpdated(alice.address, updatedFlowRate, initialFlowRate);
      const firstUpdateTimestamp = (await ethers.provider.getBlock('latest')).timestamp;

      // Increase the time by 2 days
      await ethers.provider.send('evm_increaseTime', [172800]); // 2 * 24 * 60 * 60
      await ethers.provider.send('evm_mine', []);

      // Second Update the stream
      await geoLottery.onStreamUpdated(alice.address, updatedFlowRate2, updatedFlowRate); // assuming you have a new updated flow rate
      const secondUpdateTimestamp = (await ethers.provider.getBlock('latest')).timestamp;

      // Calculate expected values
      const timeElapsedFirstUpdate = firstUpdateTimestamp - startTimestamp;
      const timeElapsedSecondUpdate = secondUpdateTimestamp - firstUpdateTimestamp;
      const expectedAmountStreamedFirst = timeElapsedFirstUpdate * initialFlowRate;
      const expectedAmountStreamedSecond = expectedAmountStreamedFirst + timeElapsedSecondUpdate * updatedFlowRate;

      // Check userStreamUpdatedAt mapping
      const updatedAt = await geoLottery.userStreamUpdatedAt(alice.address, lotteryId);
      expect(Number(updatedAt)).to.equal(secondUpdateTimestamp);

      // Check userLotteryRate mapping
      const userFlowRate = await geoLottery.userLotteryRate(alice.address, lotteryId);
      expect(Number(userFlowRate)).to.equal(updatedFlowRate2);

      // Check userAmountStreamed mapping
      const amountStreamed = await geoLottery.userAmountStreamed(alice.address, lotteryId);
      expect(Number(amountStreamed)).to.equal(expectedAmountStreamedSecond);

      // Check emitted events
      await expect(geoLottery.onStreamUpdated(alice.address, updatedFlowRate2, updatedFlowRate)).to.emit(geoLottery, 'UserStreamUpdated');
    });

    it('stream ended without issues after 2 days', async () => {
      // Start the stream
      await geoLottery.onStreamStarted(alice.address, initialFlowRate);
      const startTimestamp = (await ethers.provider.getBlock('latest')).timestamp;

      // Increase the time by 2 days
      await ethers.provider.send('evm_increaseTime', [172800]); // 2 * 24 * 60 * 60
      await ethers.provider.send('evm_mine', []);

      // End the stream
      await geoLottery.onStreamEnded(alice.address, initialFlowRate);
      const endTimestamp = (await ethers.provider.getBlock('latest')).timestamp;

      // Calculate expected values
      const timeElapsedSinceLastUpdate = endTimestamp - startTimestamp;
      const expectedAmountStreamed = timeElapsedSinceLastUpdate * initialFlowRate;

      // Check userStreamUpdatedAt mapping
      const updatedAt = await geoLottery.userStreamUpdatedAt(alice.address, lotteryId);
      expect(Number(updatedAt)).to.equal(endTimestamp);

      // Check userLotteryRate mapping, should be 0
      const userFlowRate = await geoLottery.userLotteryRate(alice.address, lotteryId);
      expect(Number(userFlowRate)).to.equal(0);

      // Check userAmountStreamed mapping
      const amountStreamed = await geoLottery.userAmountStreamed(alice.address, lotteryId);
      expect(Number(amountStreamed)).to.equal(expectedAmountStreamed);

      // Check userFlowActive mapping, should be false
      const isUserFlowActive = await geoLottery.userFlowActive(alice.address);
      expect(isUserFlowActive).to.equal(false);

      // Check emitted event
      await expect(geoLottery.onStreamEnded(alice.address, initialFlowRate)).to.emit(geoLottery, 'UserStreamEnded');
    });
  });

  describe('Claims', () => {
    const lotteryId = 0;
    const entries = 1;
    const ticketReward = 10
    const ticketRewardWei = ethers.utils.parseEther(ticketReward.toString())

    let proofs = [];

    beforeEach(async () => {
      proofs = [];
      await geoLottery.setGeoStream(owner.address);
      await aGoldMock.mint(alice.address, ethers.utils.parseEther('10000000000'));
      await aGoldMock.connect(alice).approve(geoLottery.address, ethers.utils.parseEther('10000000000'));
      await aGoldMock.mint(rewardMock.address, ethers.utils.parseEther('10000000000'));
      await geoLottery.setPoolRewards(lotteryId, [ticketRewardWei], [6]);
      await geoLottery.setDefaultThreshold(toEth(0.9))

      for (let pool = 6; pool <= 9; pool++) {
        let proof = await generateMerkleProofForAlice(alice, geoLottery, lotteryId, pool);
        proofs.push(proof);
      }
    });
    it('claim without issues', async () => {
      await geoLottery.connect(alice).enterLottery(entries);
      await geoLottery.setPoolRewards(lotteryId, [ethers.utils.parseEther('10')], [6]);
      await geoLottery.drawWinners();
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, geoLottery.address);

      const tx = await (await geoLottery.connect(alice).claimReward(lotteryId, proofs)).wait();
      const events = tx.events.filter((e) => e.event && e.args);

      let numberValue;
      for (const event of events) {
        if (event.event === 'LogBytes16Value') {
          numberValue = bytes16ToNumber(event.args.value);
          console.log(`description = ${event.args.description} value = ${numberValue}, raw value = ${event.args.value}`);
        } else {
          console.log(`description = ${event.args.description}, value = ${event.args.value}`);
        }
      }
    });

    it('claim when streamed 9.999999999 agold and 1 threshold - should be 0 ticket', async () => {
      const flowRate = ethers.utils.parseEther('4.9999999995'); // Streamed for 2 weeks so * 2
      await geoLottery.setThresholdForId(lotteryId, toEth(1))

      await geoLottery.onStreamStarted(alice.address, flowRate)

      await drawWinners(geoLottery, vrfCoordinatorV2Mock)

      await expect(geoLottery.connect(alice).claimReward(lotteryId, proofs)).to.emit(geoLottery, 'RewardClaimed').withArgs(lotteryId, alice.address, toEth(0).mul(ticketReward).toString())
    });

    it('claim when streamed 0.5 agold and 1 threshold - should be 0 ticket', async () => {
      const flowRate = ethers.utils.parseEther('0.25'); // Streamed for 2 weeks so * 2
      await geoLottery.setThresholdForId(lotteryId, toEth(1))

      await geoLottery.onStreamStarted(alice.address, flowRate)

      await drawWinners(geoLottery, vrfCoordinatorV2Mock)

      await expect(geoLottery.connect(alice).claimReward(lotteryId, proofs)).to.emit(geoLottery, 'RewardClaimed').withArgs(lotteryId, alice.address, toEth(0).mul(ticketReward).toString())
    });

    it('claim when streamed 4.2 agold and 0.5 threshold - should be 0 ticket', async () => {
      const flowRate = ethers.utils.parseEther('2.1'); // Streamed for 2 weeks so * 2
      await geoLottery.setThresholdForId(lotteryId, toEth(0.5))

      await geoLottery.onStreamStarted(alice.address, flowRate)

      await drawWinners(geoLottery, vrfCoordinatorV2Mock)

      await expect(geoLottery.connect(alice).claimReward(lotteryId, proofs)).to.emit(geoLottery, 'RewardClaimed').withArgs(lotteryId, alice.address, toEth(0).mul(ticketReward).toString())
    });

    it('claim when streamed 5 agold and 0.5 threshold - should be 1 ticket', async () => {
      const flowRate = ethers.utils.parseEther('2.5'); // Streamed for 2 weeks so * 2
      await geoLottery.setThresholdForId(lotteryId, toEth(0.5))

      await geoLottery.onStreamStarted(alice.address, flowRate)

      await drawWinners(geoLottery, vrfCoordinatorV2Mock)

      await expect(geoLottery.connect(alice).claimReward(lotteryId, proofs)).to.emit(geoLottery, 'RewardClaimed').withArgs(lotteryId, alice.address, toEth(1).mul(ticketReward).toString())

    });

    // Currently the "luckyCatGeoReward.safeMint(msg.sender, rewardFlowRate);" fails. Test works if you increase the "if (rewardAmount < 100 * 1e18)" to 1000.
    it('claim when streamed 295 agold and 0.6 threshold - should be 29 tickets', async () => {
      const flowRate = ethers.utils.parseEther('147.5'); // Streamed for 2 weeks so * 2

      await geoLottery.setThresholdForId(lotteryId, toEth(0.6))

      await geoLottery.onStreamStarted(alice.address, flowRate)

      await drawWinners(geoLottery, vrfCoordinatorV2Mock)

      await expect(geoLottery.connect(alice).claimReward(lotteryId, proofs)).to.emit(geoLottery, 'RewardClaimed').withArgs(lotteryId, alice.address, toEth(29).mul(ticketReward).toString())
    });

    // Currently the "luckyCatGeoReward.safeMint(msg.sender, rewardFlowRate);" fails. Test works if you increase the "if (rewardAmount < 100 * 1e18)" to 1000.
    it('claim when streamed 290 agold and 0.6 threshold - should be 29 tickets', async () => {
      const flowRate = ethers.utils.parseEther('147.5'); // Streamed for 2 weeks so * 2

      await geoLottery.setThresholdForId(lotteryId, toEth(0.6))

      await geoLottery.onStreamStarted(alice.address, flowRate)

      await drawWinners(geoLottery, vrfCoordinatorV2Mock)

      await expect(geoLottery.connect(alice).claimReward(lotteryId, proofs)).to.emit(geoLottery, 'RewardClaimed').withArgs(lotteryId, alice.address, toEth(29).mul(ticketReward).toString())
    });

    it('claim when streamed 9 agold and 0.9 threshold - should be 1 ticket', async () => {
      const flowRate = ethers.utils.parseEther('4.5'); // Streamed for 2 weeks so * 2

      await geoLottery.setThresholdForId(lotteryId, toEth(0.1))

      await geoLottery.onStreamStarted(alice.address, flowRate)


      await drawWinners(geoLottery, vrfCoordinatorV2Mock)

      await expect(geoLottery.connect(alice).claimReward(lotteryId, proofs)).to.emit(geoLottery, 'RewardClaimed').withArgs(lotteryId, alice.address, toEth(1).mul(ticketReward).toString())
    });
  });

  describe('setting default threshold', () => {
    const lotteryId = 0;
    const ticketReward = 10
    const ticketRewardWei = ethers.utils.parseEther(ticketReward.toString())


    let proofs = [];

    beforeEach(async () => {
      proofs = [];
      await geoLottery.setGeoStream(owner.address);
      await aGoldMock.mint(alice.address, ethers.utils.parseEther('10000000000'));
      await aGoldMock.connect(alice).approve(geoLottery.address, ethers.utils.parseEther('10000000000'));
      await aGoldMock.mint(geoLottery.address, ethers.utils.parseEther('10000000000'));
      await geoLottery.setPoolRewards(lotteryId, [ticketRewardWei], [6]);

      for (let pool = 6; pool <= 9; pool++) {
        let proof = await generateMerkleProofForAlice(alice, geoLottery, lotteryId, pool);
        proofs.push(proof);
      }
    });

    it('setting to high threshold should revert', async function () {
      await expect(geoLottery.setDefaultThreshold(toEth(2))).to.be.revertedWith('Cannot set threshold to be bigger than one eth');
    })

    it('default threshold should be used if setThresholdForId has not been set', async function () {
      await geoLottery.setDefaultThreshold(toEth(1)) // threshold is 1, which means it needs full ticket amount streamed (10)

      const flowRate = ethers.utils.parseEther('4.5'); // 9 streamed agold

      await geoLottery.onStreamStarted(alice.address, flowRate)

      await drawWinners(geoLottery, vrfCoordinatorV2Mock)

      await expect(geoLottery.connect(alice).claimReward(lotteryId, proofs)).to.emit(geoLottery, 'RewardClaimed').withArgs(lotteryId, alice.address, toEth(0).mul(ticketReward).toString())
    })
  })

  describe('rounding number', () => {

    it('should round the 0.9 to 1 with 0.1 threshold', async function () {
      const setThreshold = toEth(0.1)
      const inputValue = toEth(0.9)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(1));
    })
    it('should round the 52.9 to 53 with 0.1 threshold', async function () {
      const setThreshold = toEth(0.1)
      const inputValue = toEth(52.9)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(53));
    })
    it('should round the 102.2 to 103 with 0.1 threshold', async function () {
      const setThreshold = toEth(0.1)
      const inputValue = toEth(102.2)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(103));
    })
    it('should round the 0.9 to 1 with 0.8 threshold', async function () {
      const setThreshold = toEth(0.8)
      const inputValue = toEth(0.9)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(1));
    })
    it('should round the 52.79 to 52 with 0.8 threshold', async function () {
      const setThreshold = toEth(0.8)
      const inputValue = toEth(52.79)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(52))
    })

    it('should round the 0.1 to 1 with 0.1 threshold', async function () {
      const setThreshold = toEth(0.1)
      const inputValue = toEth(0.1)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(1))
    })

    it('should round the 0.5 to 0 with 0.8 threshold', async function () {
      const setThreshold = toEth(0.8)
      const inputValue = toEth(0.5)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(0));
    })

    it('should round the 0.5 to 1 with 0 threshold', async function () {
      const setThreshold = toEth(0)
      const inputValue = toEth(0.5)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(1));
    })
    it('should round the 20 to 20 with 0.1 threshold', async function () {
      const setThreshold = toEth(0.1)
      const inputValue = toEth(20)
      await geoLottery.setDefaultThreshold(setThreshold)
      const threshold = await geoLottery.defaultThreshold()
      const roundedValue = await geoLottery.roundUp(inputValue, threshold)
      expect(roundedValue).to.equal(toEth(20));
    })
  })
  describe('setMerkleRoot', () => {
    it('should allow owner to set merkle roots using a Merkle Tree', async function () {
      // Generate a list of random Ethereum addresses for testing purposes
      const addresses = Array.from({ length: 20 }, () => ethers.Wallet.createRandom().address);

      // console.log('Generated addresses:', addresses);

      // Construct the Merkle Tree using the addresses
      const tree = createMerkleTree(addresses);
      console.log('Constructed Merkle Tree:', tree.toString());

      // Extract the Merkle Root from the tree
      const root = tree.getHexRoot();
      console.log('Extracted Merkle Root:', root);

      // Set the merkle root in the contract
      const _lotteryId = 1;
      const _merkleRoots = [root];
      const _pools = [1];
      await geoLottery.connect(owner).setMerkleRoot(_lotteryId, _merkleRoots, _pools);
      console.log('Merkle root set in contract.');

      // Verify that the merkle root was set correctly in the contract
      const storedMerkleRoot = await geoLottery.merkleRoot(_lotteryId, _pools[0]);
      console.log('Stored Merkle Root in contract:', storedMerkleRoot);
      expect(storedMerkleRoot).to.equal(_merkleRoots[0]);
    });

    it('should allow a user to claim a reward using a Merkle proof', async function () {
      // Generate a list of random Ethereum addresses for testing purposes
      const addresses = Array.from({ length: 20 }, () => ethers.Wallet.createRandom().address);

      // Add Alice's address to the list so she can claim
      addresses.push(alice.address);

      const _lotteryId = 1;
      const _pools = [1]; // Assuming you're using a single pool for this test

      // ABI-encode each address along with the lottery ID and pool ID, then hash the result to get the Merkle leaves
      const leaves = addresses.map((address) => {
        const encodedData = ethers.utils.defaultAbiCoder.encode(['address', 'uint256', 'uint256'], [address, _lotteryId, _pools[0]]);
        return keccak256(Buffer.from(encodedData.slice(2), 'hex')); // Removing the '0x' prefix before hashing
      });

      // Construct the Merkle Tree using the generated leaves
      const tree = new MerkleTree(leaves);
      console.log('Constructed Merkle Tree:', tree.toString());

      // Extract the Merkle Root from the tree
      const root = tree.getHexRoot();
      console.log('Extracted Merkle Root:', root);

      // Set the merkle root in the contract
      const _merkleRoots = [root];
      await geoLottery.connect(owner).setMerkleRoot(_lotteryId, _merkleRoots, _pools);
      console.log('Merkle root set in contract.');

      // Generate a Merkle proof for Alice
      const aliceEncodedData = ethers.utils.defaultAbiCoder.encode(['address', 'uint256', 'uint256'], [alice.address, _lotteryId, _pools[0]]);
      const aliceLeaf = keccak256(Buffer.from(aliceEncodedData.slice(2), 'hex')); // Removing the '0x' prefix before creating a buffer

      const merkleProof = tree.getHexProof(aliceLeaf);
      console.log('Generated Merkle Proof for Alice:', merkleProof);
    });
  });
  describe('Pure functions', () => {
    beforeEach(async () => { });
    it('log10', async () => {
      // const result = await geoLottery.log10(5);
      // console.log(result);
    });
    it('resolveFlowRate', async () => {
      // const tx = await (await geoLottery.resolveFlowRate(ethers.utils.parseEther('10000000'))).wait();
      // const events = tx.events.filter((e) => e.event && e.args);
      // // Log all captured events
      // let numberValue;
      // for (const event of events) {
      //   if (event.event === 'LogBytes16Value') {
      //     numberValue = bytes16ToNumber(event.args.value);
      //     console.log(`description = ${event.args.description} value = ${numberValue}, raw value = ${event.args.value}`);
      //   } else {
      //     console.log(`description = ${event.args.description}, value = ${event.args.value}`);
      //   }
      // }
    });
  });
});

function bytes16ToNumber(quadrupleHex) {
  // Remove '0x' if present
  if (quadrupleHex.startsWith('0x')) {
    quadrupleHex = quadrupleHex.slice(2);
  }

  // Extract the sign bit (1 bit)
  const signBit = parseInt(quadrupleHex.slice(0, 1), 16) >> 3;

  // Extract the exponent (15 bits) and subtract the bias
  const exponentRaw = parseInt(quadrupleHex.slice(0, 4), 16) & 0x7fff;
  const exponent = exponentRaw - 16383;

  // Extract the fraction (112 bits)
  const fractionHex = quadrupleHex.slice(4);
  const fraction = BigInt(`0x${fractionHex}`);

  // If the exponent is all ones, the number is either Infinity or NaN
  if (exponentRaw === 0x7fff) {
    if (fraction === BigInt(0)) {
      return signBit ? -Infinity : Infinity;
    }
    return NaN;
  }

  // Calculate the number using the formula:
  // (-1)^sign * 2^exponent * (1 + fraction / 2^112)
  const normalizedFraction = Number(fraction) / Math.pow(2, 112);
  const number = Math.pow(-1, signBit) * Math.pow(2, exponent) * (1 + normalizedFraction);

  return number;
}

const createMerkleTree = (elements: string[]): MerkleTree => {
  const leaves = elements.map((e) => keccak256(Buffer.from(e.substring(2), 'hex'))); // Convert hex string to Buffer
  const tree = new MerkleTree(leaves, keccak256);
  return tree;
};

const generateMerkleProofForAlice = async (alice, geoLottery, lotteryId, pool) => {
  // Generate a list of random Ethereum addresses for testing purposes
  const addresses = Array.from({ length: 20 }, () => ethers.Wallet.createRandom().address);

  // Add Alice's address to the list so she can claim
  addresses.push(alice.address);

  // ABI-encode each address along with the lottery ID and pool ID, then hash the result to get the Merkle leaves
  const leaves = addresses.map((address) => {
    const encodedData = ethers.utils.defaultAbiCoder.encode(['address', 'uint256', 'uint256'], [address, lotteryId, pool]);
    return keccak256(Buffer.from(encodedData.slice(2), 'hex')); // Removing the '0x' prefix before hashing
  });

  // Construct the Merkle Tree using the generated leaves
  const tree = new MerkleTree(leaves, keccak256, { sortPairs: true });

  // Extract the Merkle Root from the tree
  const root = tree.getHexRoot();
  await geoLottery.setMerkleRoot(lotteryId, [root], [pool]);

  // Generate a Merkle proof for Alice
  const aliceEncodedData = ethers.utils.defaultAbiCoder.encode(['address', 'uint256', 'uint256'], [alice.address, lotteryId, pool]);
  const aliceLeaf = ethers.utils.keccak256(aliceEncodedData);
  const aliceMerkleProof = tree.getHexProof(aliceLeaf);

  return aliceMerkleProof;
};


const drawWinners = async (geoLottery, vrfCoordinatorV2Mock) => {
  await geoLottery.drawWinners();
  await vrfCoordinatorV2Mock.fulfillRandomWords(1, geoLottery.address);
}