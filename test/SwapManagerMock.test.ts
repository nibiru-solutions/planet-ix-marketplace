import {expect} from 'chai';
import hre, {ethers, upgrades} from 'hardhat';
import {Signer, Contract, BigNumber, providers, utils} from 'ethers';
import {PIXCategory, PIXSize} from './utils';
import {constants} from '@openzeppelin/test-helpers';
import {parseEther} from "ethers/lib/utils";

describe('Swap manager mock', function () {
    let maxCoin: Contract;
    let shazCoin: Contract;

    let oracle: Contract;
    let swapManager: Contract;

    let owner: Signer;
    let alice_moderator: Signer;
    let bob_player: Signer;

    let default_coin_amount = 10000;

    beforeEach(async function () {
        [owner, alice_moderator, bob_player] = await ethers.getSigners();

        let erc20Factory = await ethers.getContractFactory("ERC20Mock");
        maxCoin = await erc20Factory.deploy("MaxCoin", "MXC", await owner.getAddress(), 0);
        shazCoin = await erc20Factory.deploy("ShazCoin", "SZC", await owner.getAddress(), 0);

        let oracleFactory = await ethers.getContractFactory("OracleManagerMock");
        oracle = await oracleFactory.deploy();
        let swapFactory = await ethers.getContractFactory("SwapManagerMock");
        swapManager = await swapFactory.deploy(oracle.address);

        await swapManager.payMe({value: parseEther("50.0")});
        await oracle.setRatio(constants.ZERO_ADDRESS, shazCoin.address, 10, 1);
        await oracle.setRatio(constants.ZERO_ADDRESS, maxCoin.address, 5, 1);
        await oracle.setRatio(shazCoin.address, maxCoin.address, 5, 10);

        await maxCoin.mint(swapManager.address, default_coin_amount);
        await shazCoin.mint(swapManager.address, default_coin_amount);


    });

    describe("Basic tests", async function () {

        it("Swaps correct amount max -> shaz", async function () {
            let max = 10;
            await maxCoin.mint(await owner.getAddress(), max);
            await maxCoin.connect(owner).approve(swapManager.address, max);
            await swapManager.swap(maxCoin.address, shazCoin.address, max, await owner.getAddress());
            expect(await maxCoin.balanceOf(await owner.getAddress())).to.equal(0)
            expect(await maxCoin.balanceOf(swapManager.address)).to.equal(default_coin_amount + max);
            expect(await shazCoin.balanceOf(await owner.getAddress())).to.equal(await oracle.callStatic.getAmountOut(maxCoin.address, shazCoin.address, max));
            expect(await shazCoin.balanceOf(swapManager.address)).to.equal(default_coin_amount - (await oracle.callStatic.getAmountOut(maxCoin.address, shazCoin.address, max)));
        })

        it("Swaps correct amount max -> eth", async function () {
            let max = 10;
            await maxCoin.mint(await owner.getAddress(), max);
            await maxCoin.connect(owner).approve(swapManager.address, max);
            let balance_before = await owner.getBalance();
            await swapManager.swap(maxCoin.address, constants.ZERO_ADDRESS, max, await owner.getAddress());
            expect(await maxCoin.balanceOf(await owner.getAddress())).to.equal(0)
            expect(await maxCoin.balanceOf(swapManager.address)).to.equal(default_coin_amount + max);
        })

        it("reverts on wrong value when swapping eth - > max", async function () {
            await expect(swapManager.swap(constants.ZERO_ADDRESS, maxCoin.address,  100, await owner.getAddress(),{value: 1})).revertedWith("mock swap manager: value wrong");
            await expect(swapManager.swap(constants.ZERO_ADDRESS, maxCoin.address,  100, await owner.getAddress())).revertedWith("mock swap manager: value wrong");
        })

        it("Swaps correct amount eth -> max", async function () {
            let eth = 100;
            await swapManager.swap( constants.ZERO_ADDRESS,maxCoin.address, eth, await owner.getAddress(), {value: eth});
            expect(await maxCoin.balanceOf(await owner.getAddress())).to.equal(await oracle.callStatic.getAmountOut(constants.ZERO_ADDRESS, maxCoin.address, eth))
        })


    })


});
