import {expect} from 'chai';
import {ethers, upgrades} from 'hardhat';

const hre = require("hardhat");
import {Signer, Contract, BigNumber, providers, utils} from 'ethers';
import {PIXCategory, PIXSize} from './utils';
import {time, constants} from '@openzeppelin/test-helpers';
import {address} from "hardhat/internal/core/config/config-validation";
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/signers";
import {exp} from "@prb/math";

describe('ERC721MCS tests', function () {
    let owner: SignerWithAddress;
    let alice: Signer;
    let bob: Signer;
    let stakeable: Contract;
    let nft: Contract;
    let id = 1;
    let ev = 12;
    let cap = 3;
    let base = true;

    beforeEach(async function () {
        this.timeout(19000);
        [owner, alice, bob] = await ethers.getSigners();

        let nftFactory = await ethers.getContractFactory("ERC721Mock");
        nft = await nftFactory.deploy("Toy721", "toy")
        await nft.mint(await alice.getAddress(), 1);

        let generatorFactory = await ethers.getContractFactory("ERC721MCStakeableAdapter");
        stakeable = await upgrades.deployProxy(generatorFactory, [id, ev, cap, base]);
        await stakeable.setERC721(nft.address)
        await stakeable.setMissionControl(await owner.getAddress());
    });

    describe("onStaked", function () {
        it("Can claim if approved", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0)
            expect(await nft.ownerOf(1)).to.equal(stakeable.address)
        });
    });

    describe("onStaked reverts", function () {
        it("Can not claim token if not approved as spender", async function () {
            await expect(stakeable.onStaked(
                await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0))
                .to.revertedWith('ERC721: transfer caller is not owner nor approved');
        });

        it("Can not claim token if not MC", async function () {
            await expect(stakeable.connect(alice).onStaked(
                await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0))
                .to.revertedWith("MCS_ADAPTER: Sender not mission control");
        });
    });

    describe("onUnstaked", function () {
        it("MC can return", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
            expect(await nft.ownerOf(1)).to.equal(stakeable.address);
            await stakeable.onUnstaked(await alice.getAddress(), 1);
            expect(await nft.ownerOf(1)).to.equal(await alice.getAddress());
        });
    });

    describe("onUnstaked reverts", function () {
        it("Can not return if not MC", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
            expect(await nft.ownerOf(1)).to.equal(stakeable.address);
            await expect(stakeable.connect(bob).onUnstaked(
                await alice.getAddress(), 1))
                .to.revertedWith("MCS_ADAPTER: Sender not mission control");
        });
    });

    describe("reset", function () {
        it("can reset", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
            await expect(stakeable.reset(await alice.getAddress(), 1)).to.not.reverted;
        });
    });

    describe("reset reverts", function () {
        it("Should revert on caller not being MC", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
            await expect(stakeable.connect(alice).reset(await alice.getAddress(), 1)).to.revertedWith("MCS_ADAPTER: Sender not mission control");
        });
        it("Should revert on not owning token", async function () {
            await expect(stakeable.reset(await alice.getAddress(), 1)).to.revertedWith("MCS_ADAPTER: No such token staked");
        });
    });

    describe("checkTile", function () {
        beforeEach(async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
        });

        it("returns zero resources initially", async function () {
            expect((await stakeable.checkTile(await alice.getAddress(), 1))[0]).to.equal(0);
        });

        it("returns correct tokenId", async function () {
            expect((await stakeable.checkTile(await alice.getAddress(), 1))[1]).to.equal(id);
        });

        it("increments over time", async function () {
            /*
            This is technically a flaky test, however the chance of a false negative is infinitesimal
             */
            let num_days = 12 / ev;
            expect((await stakeable.checkTile(await alice.getAddress(), 1))[0]).to.equal(0);
            await increaseTime(86400 * num_days);
            expect((await stakeable.checkTile(await alice.getAddress(), 1))[0] > 0).to.true;
        });

        it("increments by one a.e", async function () {

            /*
            This is technically a flaky test, however the chance of a false negative is infinitesimal

            This test also takes a long time to run :)
             */
            let num_seconds = 86400 * 12 / ev;
            this.timeout(num_seconds)
            expect((await stakeable.checkTile(await alice.getAddress(), 1))[0]).to.equal(0);
            let counter = 0
            let latest = 0
            let newVal = 0
            let dt = 120
            while (counter < num_seconds && latest < cap) {
                counter += dt
                await increaseTime(dt)
                newVal = (await stakeable.checkTile(await alice.getAddress(), 1))[0]
                expect(newVal - latest <= 1).to.equal(true);
                latest = newVal
            }
            expect((await stakeable.checkTile(await alice.getAddress(), 1))[0] > 0).to.true;

        });
    });

    describe("checkTile reverts", function () {
        beforeEach(async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
        });

        it("should revert if not MC", async function () {
            await expect((stakeable.connect(alice).checkTile(await alice.getAddress(), 1))).to.revertedWith("MCS_ADAPTER: Sender not mission control");
        });

    });

    describe("collectFromTiles", function () {
        beforeEach(async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
        });

        it("returns zero resources initially", async function () {
            expect((await stakeable.callStatic.onCollect(await alice.getAddress(), 1))[0]).to.equal(0);
        });

        it("returns correct tokenId", async function () {
            expect((await stakeable.callStatic.onCollect(await alice.getAddress(), 1))[1]).to.equal(id);
        });

        it("increments over time", async function () {
            /*
            This is technically a flaky test, however the chance of a false negative is infinitesimal
             */
            let num_days = 12 / ev;
            expect((await stakeable.checkTile(await alice.getAddress(), 1))[0]).to.equal(0);
            await increaseTime(86400 * num_days);
            await expect(stakeable.callStatic.onCollect(await alice.getAddress(), 1)).to.not.reverted;
        });
        it("expected waste to not always be 3 on tile after 5 hours, cutoff bug hotfix", async function () {
            let count = {0:0, 1:0, 2:0, 3:0}
            for(let i = 0; i<50; i++){
                await increaseTime((86400/24) * 7);
                let onTile = await stakeable.checkTile(await alice.getAddress(), 1)
                count[onTile[0].toNumber()] = count[onTile[0].toNumber()] + 1
                if(onTile[0].toNumber()>0)
                    await stakeable.onCollect(await alice.getAddress(), 1)
            }
            expect(count[0] + count[1] + count[2] + count[3]).to.not.equal(count[3])
        });

        it("should decrease the amount on a tile by the collected amount", async function () {

            /*
            This is technically a flaky test, however the chance of a false negative is infinitesimal

            This test also takes a long time to run :)
             */
            let num_days = 12 / ev;
            expect((await stakeable.checkTile(await alice.getAddress(), 1))[0]).to.equal(0);
            await increaseTime(86400 * num_days);
            let num_before = (await stakeable.checkTile(await alice.getAddress(), 1))[0];
            await stakeable.onCollect(await alice.getAddress(), 1);
            let num_after = (await stakeable.checkTile(await alice.getAddress(), 1))[0];
            expect(num_before - num_after).to.equal(num_before);

        });
    });

    describe("isBase", function () {
        it("should return expected boolean", async function () {
            expect(await stakeable.isBase(1)).to.equal(base);
        });
    });

    describe("check timeLeft", function () {
        it("should always return -1", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
            let timeLeft = await stakeable.timeLeft(await alice.getAddress(), 1, 1)
            expect(timeLeft).to.equal(-1);
        });
    });

    describe("check timestamp", function () {
        it("should return expected time", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            let receipt = await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0)
            let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
            await increaseTime(1000);
            expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
        });
        it("should return expected time after collecting", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            let receipt = await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0)
            let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
            await increaseTime(1000);
            expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
            receipt = await stakeable.onCollect(await alice.getAddress(), 1);
            timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
            expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);

        });
        it("should return expected time after resetting", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            let receipt = await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0)
            let timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
            await increaseTime(1000);
            expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);
            receipt = await stakeable.reset(await alice.getAddress(), 1);
            timestamp = (await ethers.provider.getBlock(receipt.blockHash)).timestamp;
            expect(await stakeable.checkTimestamp(await alice.getAddress(), 1)).to.equal(timestamp);

        });
    });

    describe("Administrative", function () {
        it("should not allow owner to double set ERC721", async function () {
            await expect(stakeable.setERC721(nft.address)).to.revertedWith("MCS_ADAPTER: ERC721 already set");
        });
        it("should not allow non owner to set ERC721", async function () {
            await expect(stakeable.connect(alice).setERC721(nft.address)).to.revertedWith('Ownable: caller is not the owner');
        });
        it("should allow owner to set MissionControl", async function () {
            await expect(stakeable.setMissionControl(nft.address)).to.not.reverted;
        });
        it("should not allow non owner to set MissionControl", async function () {
            await expect(stakeable.connect(alice).setERC721(nft.address)).to.revertedWith('Ownable: caller is not the owner');
        });
        it("should allow owner to set params", async function () {
            await expect(stakeable.setResourceParameters(1, 1, 1, true)).to.not.reverted;
        });
        it("should not allow non owner to set params", async function () {
            await expect(stakeable.connect(alice).setResourceParameters(1, 1, 1, true)).to.revertedWith('Ownable: caller is not the owner');
        });
    });

    describe("Events", function () {
        it("Should emit TokenClaimed on claiming token", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await expect(stakeable.onStaked(
                await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0)).to.emit(stakeable, "TokenClaimed")
                .withArgs(await alice.getAddress(), stakeable.address, 1)
        });
        it("Should emit TokenReturned on returning token", async function () {
            await nft.connect(alice).setApprovalForAll(stakeable.address, true);
            await stakeable.onStaked(await alice.getAddress(), 1, 1, constants.ZERO_ADDRESS, 0);
            expect(await nft.ownerOf(1)).to.equal(stakeable.address);
            await expect(stakeable.onUnstaked(await alice.getAddress(), 1))
                .to.emit(stakeable, "TokenReturned").withArgs(await alice.getAddress(), stakeable.address, 1);
        });
        it("Should emit ParametersUpdated on updating parameters", async function () {
            await expect(stakeable.setResourceParameters(1, 2, 3, true)).to.emit(stakeable, "ParametersUpdated").withArgs(stakeable.address, 1, 2, 3, true);
        });

    });


});

async function increaseTime(amount) {
    await hre.network.provider.send("evm_increaseTime", [amount])
    await hre.network.provider.send("evm_mine")
    //console.log("EVM time " + amount + " seconds increased!")
}