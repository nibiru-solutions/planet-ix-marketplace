import {assert, expect} from 'chai';
import hre, {ethers, upgrades} from 'hardhat';
import {Signer, Contract, BigNumber, providers, utils} from 'ethers';
import {PIXCategory, PIXSize} from './utils';
import {constants} from '@openzeppelin/test-helpers';
import {parseEther} from 'ethers/lib/utils';
import {delay} from "@nomiclabs/hardhat-etherscan/dist/src/etherscan/EtherscanService";
import {deploy} from "@openzeppelin/hardhat-upgrades/dist/utils";

describe('Gravity Grade', function () {
    this.timeout(8000)
    let gravityGrade: Contract;
    let gravityGradeWrapper: Contract;
    let maxCoin: Contract;
    let shazCoin: Contract;

    let oracle: Contract;
    let swapManager: Contract;

    let owner: Signer;
    let alice_moderator: Signer;
    let bob_player: Signer;
    let charles_trusted: Signer;

    let swap_mint_amt = 100;

    beforeEach(async function () {
        [owner, alice_moderator, bob_player, charles_trusted] = await ethers.getSigners();

        let erc20Factory = await ethers.getContractFactory('ERC20Mock');
        maxCoin = await erc20Factory.deploy('MaxCoin', 'MXC', await owner.getAddress(), 0);
        shazCoin = await erc20Factory.deploy('ShazCoin', 'SZC', await owner.getAddress(), 0);

        let oracleFactory = await ethers.getContractFactory('OracleManagerMock');
        oracle = await oracleFactory.deploy();
        let swapFactory = await ethers.getContractFactory('SwapManagerMock');
        swapManager = await swapFactory.deploy(oracle.address);

        await swapManager.payMe({value: parseEther('50.0')});
        await maxCoin.mint(swapManager.address, swap_mint_amt);
        await shazCoin.mint(swapManager.address, swap_mint_amt);
        await oracle.setRatio(constants.ZERO_ADDRESS, shazCoin.address, 1, 10);
        await oracle.setRatio(constants.ZERO_ADDRESS, maxCoin.address, 1, 5);
        await oracle.setRatio(shazCoin.address, maxCoin.address, 10, 5);

        // INSERT YOUR GG IMPLEMENTATION BELOW
        let ggFactory = await ethers.getContractFactory('GravityGrade');
        gravityGrade = await upgrades.deployProxy(ggFactory, ['Gravity Grade', 'IX-GG']);

        await gravityGrade.setModerator(await alice_moderator.getAddress());
        await gravityGrade.connect(alice_moderator).setTokenUri(1, 'This is a toy URI');
        await gravityGrade.connect(alice_moderator).setTokenUri(2, 'This is a toy URI2');
        await gravityGrade.connect(alice_moderator).setSwapManager(swapManager.address);
        await gravityGrade.connect(alice_moderator).setOracleManager(oracle.address);
        await gravityGrade
            .connect(alice_moderator)
            .setTrusted(await charles_trusted.getAddress(), true);

        let ggWrapperFac = await ethers.getContractFactory('GravityGradeMintWrapper');
        gravityGradeWrapper = await ggWrapperFac.deploy();

        await gravityGradeWrapper.setGravityGradeContract(gravityGrade.address)
        await gravityGrade
            .connect(alice_moderator)
            .setTrusted(gravityGradeWrapper.address, true);

    });

    describe('ggWrapper trusted mint', function () {
        let mintAmount = 4
        let tokenId = 1
        let tokenId2 = 2
        beforeEach(async function () {
            await gravityGradeWrapper.connect(owner).setTrusted(await charles_trusted.getAddress(), true);
        });
        it('Mint when trusted', async function () {
            await gravityGradeWrapper.connect(charles_trusted).trustedMint(await bob_player.getAddress(), tokenId, mintAmount)
            expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(mintAmount)
        }).timeout(5000)
        it('Mint batch when trusted', async function () {
            await gravityGradeWrapper.connect(charles_trusted).trustedBatchMint(await bob_player.getAddress(), [tokenId, tokenId2], [mintAmount, mintAmount])
            expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId)).to.equal(mintAmount)
            expect(await gravityGrade.balanceOf(await bob_player.getAddress(), tokenId2)).to.equal(mintAmount)
        }).timeout(5000)
    }).timeout(5000)
    describe('Revert trusted mint', function () {
        let mintAmount = 4
        let tokenId = 1
        let tokenId2 = 2
        beforeEach(async function () {
            await gravityGradeWrapper.connect(owner).setTrusted(await charles_trusted.getAddress(), true);
        });
        it('Revert mint when not trusted', async function () {
            await expect(gravityGradeWrapper.connect(bob_player).trustedMint(await bob_player.getAddress(), tokenId, mintAmount))
                .to.revertedWith(`GravityGradeMintWrapper__NotTrusted("${await bob_player.getAddress()}")`);
        });
        it('Revert batch mint when lengths not the same', async function () {
            await expect(gravityGradeWrapper.connect(charles_trusted)
                .trustedBatchMint(await bob_player.getAddress(), [tokenId2], [mintAmount, mintAmount]))
                .to.revertedWith(`GravityGradeMintWrapper__TokenIdsAndAmountsNotSameLength()`);
        });
        it('Revert when gravitygrade is not set', async function () {
            let ggWrapperFac2 = await ethers.getContractFactory('GravityGradeMintWrapper');
            let gravityGradeWrapper2 = await ggWrapperFac2.deploy();
            await expect(gravityGradeWrapper2
                .trustedMint(await owner.getAddress(), tokenId2, mintAmount))
                .to.revertedWith(`GravityGradeMintWrapper__GravityGradeNotSet()`);
        });

    }).timeout(5000)
})