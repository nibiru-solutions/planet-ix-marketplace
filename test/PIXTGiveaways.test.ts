import { assert, expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { Contract, BigNumber, utils } from 'ethers';
import { parseEther } from 'ethers/lib/utils';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

describe('IXT Giveaways', () => {
  let ixtToken: Contract;
  let ixtGiveaways: Contract;

  let zack: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;

  beforeEach(async () => {
    [zack, alice, bob] = await ethers.getSigners();

    const PIXTFactory = await ethers.getContractFactory('PIXT');
    ixtToken = await PIXTFactory.deploy();

    let ixtGiveawayFactory = await ethers.getContractFactory('PIXTGiveaways');
    ixtGiveaways = await upgrades.deployProxy(ixtGiveawayFactory, [ixtToken.address]);

    await ixtGiveaways.setSigner(alice.address);
    await ixtToken.transfer(ixtGiveaways.address, utils.parseEther('10000'));


  });
  describe('Claim', () => {
    let giveawayId = 1;
    let amount = 5;
    let totalClaims = 100;
    beforeEach(async () => {
      await ixtGiveaways.createGiveaway(amount, totalClaims);
    });
    it('Claims as expected', async () => {
      const data = await signMessage(ixtGiveaways, alice, BigNumber.from(giveawayId), bob);
      expect(await ixtGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s))
        .to.emit(ixtGiveaways, 'GiveawayClaimed')
        .withArgs(bob.address, giveawayId);
      const info = await ixtGiveaways.getGiveawayInfo(giveawayId);
      expect(info['claimed']).to.equal(1);
      expect(await ixtGiveaways.canClaim(bob.address, giveawayId)).to.false;
      expect(await ixtToken.balanceOf(bob.address)).to.equal(amount);
    });
  });
  describe('Claim reverts', () => {
    let giveawayId = 1;
    let amount = 5;
    let totalClaims = 2;
    beforeEach(async () => {
      await ixtGiveaways.createGiveaway(amount, totalClaims);
    });
    it('Reverts on trying to claim more than once', async () => {
      let data = await signMessage(ixtGiveaways, alice, BigNumber.from(giveawayId), bob);
      await ixtGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s);
      data = await signMessage(ixtGiveaways, alice, BigNumber.from(giveawayId), bob);
      await expect(
        ixtGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s),
      ).to.revertedWith(`IXT_Giveaway_AlreadyClaimed("${bob.address}")`);
    });
    it('Reverts on giveaway max amount exceeded', async () => {
      let data;
      let wallet;
      for (let i = 0; i < totalClaims; i++) {
        wallet = ethers.Wallet.createRandom();
        wallet = wallet.connect(ethers.provider);
        await zack.sendTransaction({ to: wallet.address, value: ethers.utils.parseEther('1') });
        data = await signMessage(ixtGiveaways, alice, BigNumber.from(giveawayId), wallet);
        await ixtGiveaways.connect(wallet).claim(giveawayId, data.v, data.r, data.s);
      }
      wallet = ethers.Wallet.createRandom();
      wallet = wallet.connect(ethers.provider);
      await zack.sendTransaction({ to: wallet.address, value: ethers.utils.parseEther('1') });
      data = await signMessage(ixtGiveaways, alice, BigNumber.from(giveawayId), wallet);
      await expect(
        ixtGiveaways.connect(wallet).claim(giveawayId, data.v, data.r, data.s),
      ).to.revertedWith(`IXT_Giveaway_MaxAmountExceeded(${giveawayId})`);
    });
    it('Reverts on invalid giveaway id', async () => {
      let invalidId = 999;
      const data = await signMessage(ixtGiveaways, alice, BigNumber.from(invalidId), bob);
      await expect(
        ixtGiveaways.connect(bob).claim(invalidId, data.v, data.r, data.s),
      ).to.revertedWith(`_Giveaway_InvalidId(${invalidId})`);
    });
    it('Reverts on incorrect signature', async () => {
      const data = await signMessage(
        ixtGiveaways,
        alice,
        BigNumber.from(giveawayId),
        alice,
      );
      await expect(
        ixtGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s),
      ).to.revertedWith(`IXT_Giveaway_InvalidSignature()`);
    });
  });
  describe('Create Giveaway', () => {
    let amount = 5;
    let totalClaims = 100;
    it('Creates giveaway successfully', async () => {
      await expect(ixtGiveaways.createGiveaway(amount, totalClaims))
        .to.emit(ixtGiveaways, 'GiveawayCreated')
        .withArgs(await ixtGiveaways.s_totalGiveaways(), amount, totalClaims);
    });
  });
  describe('Create Giveaway Reverts', () => {
    let amount = 5;
    let totalClaims = 100;
    it('Reverts on non-owner trying to create giveaway', async () => {
      await expect(
        ixtGiveaways.connect(bob).createGiveaway(amount, totalClaims),
      ).to.revertedWith('Ownable: caller is not the owner');
    });
  });
  describe('Delete Giveaway', () => {
    let giveawayId = 1;
    let amount = 5;
    let totalClaims = 100;
    beforeEach(async () => {
      await ixtGiveaways.createGiveaway(amount, totalClaims);
    });
    it('Deletes giveaway successfully', async () => {
      await expect(ixtGiveaways.deleteGiveaway(giveawayId))
        .to.emit(ixtGiveaways, 'GiveawayDeleted')
        .withArgs(giveawayId);
    });
  });
  describe('Delete Giveaway Reverts', () => {
    let giveawayId = 1;
    let amount = 5;
    let totalClaims = 100;
    beforeEach(async () => {
      await ixtGiveaways.createGiveaway(amount, totalClaims);
    });
    it('Reverts on non-owner trying to delete giveaway', async () => {
      await expect(ixtGiveaways.connect(bob).deleteGiveaway(giveawayId)).to.revertedWith(
        'Ownable: caller is not the owner',
      );
    });
    it('Reverts on invalid giveaway id', async () => {
      let invalidId = 999;
      await expect(ixtGiveaways.deleteGiveaway(invalidId)).to.revertedWith(
        `IXT_Giveaway_InvalidId(${invalidId})`,
      );
    });
    it('Reverts on trying to claim after giveaway deleted', async () => {
      await ixtGiveaways.deleteGiveaway(giveawayId);
      const data = await signMessage(ixtGiveaways, alice, BigNumber.from(giveawayId), bob);

      await expect(
        ixtGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s),
      ).to.revertedWith(`IXT_Giveaway_InvalidId(${giveawayId})`);
    });
  });
  describe('Set signer address', () => {
    it('Sets signer as expected', async () => {
      await expect(ixtGiveaways.setSigner(alice.address)).to.not.reverted;
    });
    it('Reverts on non-owner call', async () => {
      await expect(ixtGiveaways.connect(bob).setSigner(alice.address)).to.revertedWith(
        'Ownable: caller is not the owner',
      );
    });
  });
  describe('Get Giveaway Info', () => {
    let giveawayId = 1;
    let amount = 10;
    let totalClaims = 100;
    let info;
    beforeEach(async () => {
      await ixtGiveaways.createGiveaway(amount, totalClaims);
    });
    it('Gets info as expected', async () => {
      info = await ixtGiveaways.getGiveawayInfo(giveawayId);
      expect(info['id']).to.equal(giveawayId);
      expect(info['amount']).to.equal(amount);
      expect(info['claimed']).to.equal(0);
      expect(info['cap']).to.equal(totalClaims);
    });
    it('Gets correct info after some airdrops were claimed', async () => {
      let data = await signMessage(ixtGiveaways, alice, BigNumber.from(giveawayId), bob);
      await ixtGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s);
      info = await ixtGiveaways.getGiveawayInfo(giveawayId);
      expect(info['id']).to.equal(giveawayId);
      expect(info['amount']).to.equal(amount);
      expect(info['claimed']).to.equal(1);
      expect(info['cap']).to.equal(totalClaims);
    });
    it('Reverts on invalid id', async () => {
      let invalidId = 999;
      await expect(ixtGiveaways.getGiveawayInfo(invalidId)).to.reverted;
    });
  });
  describe('Can Claim', () => {
    let giveawayId = 1;
    let amount = 10;
    let totalClaims = 100;
    beforeEach(async () => {
      await ixtGiveaways.createGiveaway(amount, totalClaims);
    });
    it('User should be able to claim by default', async () => {
      expect(await ixtGiveaways.canClaim(bob.address, giveawayId)).to.true;
    });
    it('Reverts on user trying to claim more than once.', async () => {
      let data = await signMessage(ixtGiveaways, alice, BigNumber.from(giveawayId), bob);
      await ixtGiveaways.connect(bob).claim(giveawayId, data.v, data.r, data.s);
      expect(await ixtGiveaways.canClaim(bob.address, giveawayId)).to.false;
    });
    it('Reverts on invalid id', async () => {
      let invalidId = 999;
      await expect(ixtGiveaways.canClaim(bob.address, invalidId)).to.reverted;
    });
  });
});

const signMessage = async (
  giveaway: Contract,
  signer: SignerWithAddress,
  id: BigNumber,
  sender: SignerWithAddress,
) => {
  const domain = {
    name: 'IXT__Giveaway',
    version: '1',
    chainId: (await ethers.provider.getNetwork()).chainId,
    verifyingContract: giveaway.address,
  };

  const types = {
    AirdropMessage: [
      { name: 'id', type: 'uint256' },
      { name: 'sender', type: 'address' },
      { name: 'nonce', type: 'uint256' },
    ],
  };

  const value = {
    id,
    sender: sender.address,
    nonce: await giveaway.nonces(sender.address, id),
  };

  const signature = await signer._signTypedData(domain, types, value);
  return utils.splitSignature(signature);
};
