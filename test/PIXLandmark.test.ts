import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Contract, constants } from 'ethers';
import { PIXCategory } from './utils';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

describe('PIXLandmark', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let pixLandmark: Contract;

  beforeEach(async function () {
    [owner, alice] = await ethers.getSigners();

    const PIXLandmarkFactory = await ethers.getContractFactory('PIXLandmark');
    pixLandmark = await upgrades.deployProxy(PIXLandmarkFactory);

    const tokensByRarity = [269,189,1,131,186,164];
    const indices = [0,1,2,3,4,5];
    const maxSupplies = [1,7,888,49,343,2401];

    await pixLandmark.setMaxSupplies([269,189,1,131,186,164], [1,7,888,49,343,2401])
    await pixLandmark.setTokensByRarity(tokensByRarity);
    await pixLandmark.setTokenRarityIndices(tokensByRarity, indices);
    await pixLandmark.setTrusted(owner.address, true);
  });

  describe('#initialize', () => {
    it('check initial values', async function () {
      expect(await pixLandmark.isModerator(owner.address)).equal(true);
      expect(await pixLandmark.name()).equal('PIX LANDMARK');
      expect(await pixLandmark.symbol()).equal('PIXL');
    });
  });

  describe('setModerator', () => {
    it('revert if msg.sender is not owner', async () => {
      await expect(pixLandmark.connect(alice).setModerator(alice.address, true)).to.revertedWith(
        'Ownable: caller is not the owner',
      );
    });

    it('revert if moderator is zero address', async () => {
      await expect(pixLandmark.setModerator(constants.AddressZero, true)).to.revertedWith(
        'Landmark: INVALID_MODERATOR',
      );
    });

    it('should set moderator by owner', async () => {
      await pixLandmark.setModerator(alice.address, true);
      expect(await pixLandmark.isModerator(alice.address)).to.be.equal(true);

      await pixLandmark.setModerator(alice.address, false);
      expect(await pixLandmark.isModerator(alice.address)).to.be.equal(false);
    });
  });

  describe('setBaseURI', () => {
    const uri = 'https://planetix.com/land-nfts/';

    it('revert if msg.sender is not owner', async () => {
      await expect(pixLandmark.connect(alice).setBaseURI(uri)).to.revertedWith(
        'Ownable: caller is not the owner',
      );
    });

    it('should set base uri by owner', async () => {
      await pixLandmark.setBaseURI(uri);
      await pixLandmark.setMaxSupplies([1], [1]);
      await pixLandmark.safeMint(alice.address, 1, 1);
      expect(await pixLandmark.uri(1)).to.equal(uri + '1');
    });
  });

  describe('setMaxSupplies', () => {
    it('revert if msg.sender is not owner', async () => {
      await expect(pixLandmark.connect(alice).setMaxSupplies([1], [2])).to.revertedWith(
        'Ownable: caller is not the owner',
      );
    });

    it('revert if arguments are invalid', async () => {
      await expect(pixLandmark.setMaxSupplies([1], [10, 20])).to.revertedWith(
        'Landmark: INVALID_ARGUMENTS',
      );
    });

    it('should set maximum supplies by owner', async () => {
      await pixLandmark.setMaxSupplies([1, 2], [10, 20]);
      expect(await pixLandmark.getMaxSupply(1)).to.equal(10);
    });

    it('revert if new max supply is less then current total supply', async () => {
      await pixLandmark.setMaxSupplies([1], [10]);
      pixLandmark.safeMint(alice.address, 1, 10)
      await expect(pixLandmark.setMaxSupplies([1], [9])).to.revertedWith(
        'Landmark: AMOUNT_TOO_SMALL',
      );
    });
  });

  describe('setCategories', () => {
    it('revert if msg.sender is not owner', async () => {
      await expect(pixLandmark.connect(alice).setCategories([1], [PIXCategory.Uncommon])).to.revertedWith(
        'Ownable: caller is not the owner',
      );
    });

    it('revert if arguments are invalid', async () => {
      await expect(pixLandmark.setCategories([1, 2], [PIXCategory.Uncommon])).to.revertedWith(
        'Landmark: INVALID_ARGUMENTS',
      );
    });

    it('should set categories by owner', async () => {
      await pixLandmark.setCategories([1, 2],[PIXCategory.Common, PIXCategory.Uncommon]);
      expect(await pixLandmark.getLandCategory(1)).to.equal(PIXCategory.Common);
    });
  });

  describe('safeMint', () => {
    it('revert if sender is not moderator', async () => {
      await expect(
        pixLandmark.connect(alice).safeMint(alice.address, 1, 1),
      ).to.revertedWith('Landmark: NON_MODERATOR');
    });

    it('should safe mint', async () => {
      await pixLandmark.setMaxSupplies([1], [1]);
      await pixLandmark.safeMint(alice.address, 1, 1);
      expect(await pixLandmark.totalSupply(1)).to.equal(1);
    });

    it('revert if minting more than max supply', async () => {
      await pixLandmark.setMaxSupplies([1], [1]);
      await expect(
        pixLandmark.safeMint(alice.address, 1, 2),
      ).to.revertedWith('Landmark: AMOUNT_TOO_BIG');
    });
  });

  describe('batchMint', () => {
    it('revert if sender is not moderator', async () => {
      await expect(
        pixLandmark.connect(alice).batchMint([alice.address], [1], [1]),
      ).to.revertedWith('Landmark: NON_MODERATOR');
    });

    it('revert if arguments are invalid', async () => {
      await expect(pixLandmark.batchMint([alice.address], [], [1])).to.revertedWith(
        'Landmark: INVALID_ARGUMENTS',
      );
    });

    it('should safe mint', async () => {
      await pixLandmark.setMaxSupplies([1], [2]);
      await pixLandmark.batchMint([alice.address], [1], [2]);
      expect(await pixLandmark.totalSupply(1)).to.equal(2);
    });
  });

  describe('trustedMint', () => {
    it('mint 10 rarest landmarks', async () => {
      // Tring to mint token ID 269, 8 times. On the last mint the next rarest
      // should be minted, because max supply is reached for 131
      for (let i=0;i<8;i++) {
        await pixLandmark.trustedMint(owner.address, 269, 1);
      }
      let balanceToken269 = await pixLandmark.balanceOf(owner.address,269);
      expect(balanceToken269).to.equal(1);
      let balanceToken189 = await pixLandmark.balanceOf(owner.address, 189);
      expect(balanceToken189).to.equal(7);

      // Tring to mint token ID 131, 50 times. On the last mint the next rarest
      // should be minted, because max supply is reached for 131
      for (let i=0;i<50;i++) {
        await pixLandmark.trustedMint(owner.address, 131, 1);
      }
      let balanceToken131 = await pixLandmark.balanceOf(owner.address, 131);
      expect(balanceToken131).to.equal(49);
      let balanceToken186 = await pixLandmark.balanceOf(owner.address, 186);
      expect(balanceToken186).to.equal(1);
    });
  });
});
