import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import {Contract, utils} from 'ethers';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

import { ERC1155MockAssetManager } from '../src/types';
import { ERC721Mock } from '../src/types';
import { LuckyCatRaffleV3 } from '../src/types';
import { VRFCoordinatorV2Mock } from '../src/types';

import { LuckyCatERC721Treasury } from '../src/types';
import { LuckyCatERC1155Treasury } from '../src/types';

describe('Lucky Cat Raffle V3', function () {
  this.timeout(120000);
  enum AssetIds {
    UNUSED_0, // 0, unused
    GoldBadge, //1
    SilverBadge, //2
    BronzeBadge, // 3
    GenesisDrone, //4
    PiercerDrone, // 5
    YSpaceShare, //6
    Waste, //7
    AstroCredit, // 8
    Blueprint, // 9
    BioModOutlier, // 10
    BioModCommon, //11
    BioModUncommon, // 12
    BioModRare, // 13
    BioModLegendary, // 14
    LootCrate, // 15
    TicketRegular, // 16
    TicketPremium, //17
    TicketGold, // 18
    FacilityOutlier, // 19
    FacilityCommon, // 20
    FacilityUncommon, // 21
    FacilityRare, //22
    FacilityLegendary, // 23,
    Energy, // 24
    LuckyCatShare, // 25,
    GravityGradeShare, // 26
    NetEmpireShare, //27
    NewLandsShare, // 28
    HaveBlueShare, //29
    GlobalWasteSystemsShare, // 30
    EternaLabShare, // 31
  }

  enum PrizeType {
    ERC721,
    ERC1155,
  }

  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;

  let landmarkMock: Contract;
  let ticketMock: Contract;
  let erc721Mock: Contract;

  let erc721Treasury: Contract;
  let erc1155Treasury: Contract;

  let luckyCatRaffle: Contract;
  let vrfCoordinatorV2Mock: Contract;

  const SUBSCRIPTION_ID = 1;
  const KEYHASH = ethers.utils.defaultAbiCoder.encode(['uint256'], [1]);
  beforeEach(async () => {
    [owner, alice, bob] = await ethers.getSigners();

    const landmarkMockFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    landmarkMock = (await landmarkMockFactory.deploy('TOY URI')) as ERC1155MockAssetManager;

    const ticketMockFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    ticketMock = (await ticketMockFactory.deploy('TOY URI')) as ERC1155MockAssetManager;

    const erc721MockFactory = await ethers.getContractFactory('ERC721Mock');
    erc721Mock = (await erc721MockFactory.deploy('Name', 'Symbol')) as ERC721Mock;

    const vrfCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
    vrfCoordinatorV2Mock = await vrfCoordinatorV2MockFactory.deploy(0, 6);
    await vrfCoordinatorV2Mock.createSubscription();
    await vrfCoordinatorV2Mock.fundSubscription(SUBSCRIPTION_ID, utils.parseEther('10'));

    const luckyCatRaffleFactory = await ethers.getContractFactory('LuckyCatRaffleV3');
    luckyCatRaffle = (await upgrades.deployProxy(luckyCatRaffleFactory, [
      landmarkMock.address,
      ticketMock.address,
      vrfCoordinatorV2Mock.address,
      KEYHASH,
      SUBSCRIPTION_ID,
    ])) as LuckyCatRaffleV3;

    const erc721TreasuryFactory = await ethers.getContractFactory('LuckyCatERC721Treasury');
    erc721Treasury = (await upgrades.deployProxy(
      erc721TreasuryFactory,
      [],
    )) as LuckyCatERC721Treasury;

    const erc1155TreasuryFactory = await ethers.getContractFactory('LuckyCatERC1155Treasury');
    erc1155Treasury = (await upgrades.deployProxy(
      erc1155TreasuryFactory,
      [],
    )) as LuckyCatERC1155Treasury;

    await luckyCatRaffle.setERC721Treasury(erc721Treasury.address);
    await luckyCatRaffle.setERC1155Treasury(erc1155Treasury.address);

    await erc1155Treasury.setLuckyCat(luckyCatRaffle.address);
    await erc721Treasury.setLuckyCat(luckyCatRaffle.address);

    await ticketMock.setTrusted(luckyCatRaffle.address, true);

    await vrfCoordinatorV2Mock.addConsumer(SUBSCRIPTION_ID, luckyCatRaffle.address);
  });

  describe('Register Raffle', () => {
    const day = 24 * 60 * 60;
    const month = day * 30;
    const year = month * 12;

    beforeEach(async () => {
      await landmarkMock.mint(owner.address, 7, 2000);
      await landmarkMock.setApprovalForAll(luckyCatRaffle.address, true);
    });
    it('Should create a raffle without issue', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,
        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      let raffleId = await luckyCatRaffle.callStatic.registerRaffle(raffleParams);
      let tx = await luckyCatRaffle.registerRaffle(raffleParams);

      expect(Number(await landmarkMock.balanceOf(erc1155Treasury.address, 7))).to.equal(
        raffleParams.prizeAmounts[0] * raffleParams.totalPrizes,
      );
      expect(tx).to.emit(luckyCatRaffle, 'RaffleRegistered');

      let registeredRaffle = await luckyCatRaffle.getRaffle(raffleId);
      console.log(registeredRaffle);

      console.log(raffleId);
    });
  });
  describe('Register Raffle Reverts', () => {
    const day = 24 * 60 * 60;
    const month = day * 30;
    const year = month * 12;

    beforeEach(async () => {
      await landmarkMock.mint(owner.address, 7, 2000);
      await landmarkMock.setApprovalForAll(luckyCatRaffle.address, true);
    });
    it('should revert if ticket id not recognized', async () => {
      const raffleParams = {
        ticketId: AssetIds.Waste,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.registerRaffle(raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidId',
      );
    });
    it('should revert if prize ids array length unequal to prize amount array length', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2, 1],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.registerRaffle(raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidInputs',
      );
    });
    it('should revert if user min entries exceed user max entries', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 3,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.registerRaffle(raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if user min entries = 0', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 0,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.registerRaffle(raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if user max entries = 0', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 0,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.registerRaffle(raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if user max entries exceed totalEntries', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 10001,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.registerRaffle(raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if user min entries exceed totalEntries', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 10001,
        userMaxEntries: 3,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.registerRaffle(raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if totalEntries = 0', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 0,
        userMinEntries: 1,
        userMaxEntries: 3,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.registerRaffle(raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if non gov tries to call', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 0,
        userMinEntries: 1,
        userMaxEntries: 3,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(
        luckyCatRaffle.connect(alice).registerRaffle(raffleParams),
      ).to.revertedWith('ICatRaffle_3_0__Unauthorized');
    });
  });
  describe('Update Raffle', () => {
    const day = 24 * 60 * 60;
    const month = day * 30;
    const year = month * 12;

    let raffleId;

    beforeEach(async () => {
      await landmarkMock.mint(owner.address, 7, 2000);
      await landmarkMock.setApprovalForAll(luckyCatRaffle.address, true);
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      raffleId = await luckyCatRaffle.callStatic.registerRaffle(raffleParams);
      await luckyCatRaffle.registerRaffle(raffleParams);
    });
    it('Should update a raffle without issue', async () => {
      const updateParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [10],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, updateParams))
        .to.emit(luckyCatRaffle, 'RaffleUpdated')
        .withArgs(raffleId);
    });
  });
  describe('Update Raffle reverts', () => {
    const day = 24 * 60 * 60;
    const month = day * 30;
    const year = month * 12;

    let raffleId;

    beforeEach(async () => {
      await landmarkMock.mint(owner.address, 7, 2000);
      await landmarkMock.setApprovalForAll(luckyCatRaffle.address, true);
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      raffleId = await luckyCatRaffle.callStatic.registerRaffle(raffleParams);
      await luckyCatRaffle.registerRaffle(raffleParams);
    });
    it('should revert if ticket id not recognized', async () => {
      const raffleParams = {
        ticketId: AssetIds.Waste,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, raffleParams)).to.revertedWith(
        `ICatRaffle_3_0__InvalidId(${7})`,
      );
    });
    it('should revert if prize ids array length unequal to prize amount array length', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2, 1],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidInputs',
      );
    });
    it('should revert if user min entries exceed user max entries', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 3,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if user min entries = 0', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 0,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if user max entries = 0', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 0,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntries',
      );
    });
    it('should revert if user max entries exceed totalEntries', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 10001,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__UserMaxExceedsTotal(10001, 10000)',
      );
    });
    it('should revert if user min entries exceed totalEntries', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 10001,
        userMaxEntries: 3,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, raffleParams)).to.revertedWith(
        'ICatRaffle_3_0__InvalidUserEntryLimits(10001, 3',
      );
    });
    it('should revert user max entries is higher than total entries', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 0,
        userMinEntries: 1,
        userMaxEntries: 3,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(luckyCatRaffle.updateRaffle(raffleId, raffleParams)).to.revertedWith(
        "ICatRaffle_3_0__UserMaxExceedsTotal(3, 0)"
      );
    });
    it('should revert if non gov tries to call', async () => {
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 3,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await expect(
        luckyCatRaffle.connect(alice).updateRaffle(raffleId, raffleParams),
      ).to.revertedWith('ICatRaffle_3_0__Unauthorized');
    });
  });
  describe('Enter Raffle', () => {
    const day = 24 * 60 * 60;
    const month = day * 30;
    const year = month * 12;
    beforeEach(async () => {
      await landmarkMock.mint(owner.address, 7, 2000);
      await landmarkMock.setApprovalForAll(luckyCatRaffle.address, true);
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await luckyCatRaffle.registerRaffle(raffleParams);

      await ticketMock.mint(alice.address, AssetIds.TicketRegular, 10000);
      await luckyCatRaffle.setPaused(1, false);
    });
    it('Enters a user into a raffle as expected', async () => {
      await expect(luckyCatRaffle.connect(alice).enterRaffle(1, 2))
        .to.emit(luckyCatRaffle, 'RaffleEntered')
        .withArgs(1, alice.address);

      //expect(await luckyCatRaffle.ticketToUser(1, 1)).to.equal(alice.address);
      //expect(await luckyCatRaffle.ticketToUser(1, 0)).to.equal(alice.address);

      expect(await luckyCatRaffle.userRaffleEntries(alice.address, 1)).to.equal(2);
      //expect(await luckyCatRaffle.raffleEntries(1)).to.equal(2);

      expect(await ticketMock.balanceOf(alice.address, AssetIds.TicketRegular)).to.equal(10000 - 2);
    });
    it('Enter Raffle, draw raffle and try to view winner', async () => {
      await luckyCatRaffle.connect(alice).enterRaffle(1, 2);
      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      let requestId = await luckyCatRaffle.callStatic.drawWinners(1);
      await luckyCatRaffle.drawWinners(1);
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, luckyCatRaffle.address);
      console.log(requestId, await luckyCatRaffle.raffleDrawn(1));
      console.log(await luckyCatRaffle.getCumulativeEntries(1));
      let result = await luckyCatRaffle.getAllWinningAddresses(1);
      console.log(result);
    });
  });
  describe('Enter Raffle Reverts', () => {
    const day = 24 * 60 * 60;
    const month = day * 30;
    const year = month * 12;
    beforeEach(async () => {
      await landmarkMock.mint(owner.address, 7, 2000);
      await landmarkMock.setApprovalForAll(luckyCatRaffle.address, true);
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: 1000,
        totalEntries: 10000,
        userMinEntries: 1,
        userMaxEntries: 2,
        startTime: 0,

        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await luckyCatRaffle.registerRaffle(raffleParams);

      await ticketMock.mint(alice.address, AssetIds.TicketRegular, 10000);
      await ticketMock.mint(bob.address, AssetIds.TicketRegular, 10000);
      await luckyCatRaffle.setPaused(1, false);
    });

    it('should revert if raffle id invalid', async () => {
      await expect(luckyCatRaffle.connect(alice).enterRaffle(99, 2)).to.revertedWith(
        `ICatRaffle_3_0__InvalidRaffle(${99})`,
      );
    });
    it('should revert if raffle duration already over', async () => {
      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      await expect(luckyCatRaffle.connect(alice).enterRaffle(1, 2)).to.revertedWith(
        `ICatRaffle_3_0__InvalidRaffle(${1})`,
      );
    });
    it('should revert if raffle paused', async () => {
      await luckyCatRaffle.setPaused(1, true);
      await expect(luckyCatRaffle.connect(alice).enterRaffle(1, 2)).to.revertedWith(
        `ICatRaffle_3_0__Paused(${1})`,
      );
    });
    it('should revert if raffle already drawn', async () => {
      await luckyCatRaffle.connect(bob).enterRaffle(1, 2);
      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      let requestId = await luckyCatRaffle.callStatic.drawWinners(1);
      await luckyCatRaffle.drawWinners(1);


      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, luckyCatRaffle.address);
      console.log(requestId, await luckyCatRaffle.raffleDrawn(1)) ;
      expect(await luckyCatRaffle.raffleDrawn(1)).to.true;

      await expect(luckyCatRaffle.connect(alice).enterRaffle(1, 2)).to.revertedWith(
        `ICatRaffle_3_0__IsDrawn(${1})`,
      );
    });
    it('should revert if tickets is 0 ', async () => {
      await expect(luckyCatRaffle.connect(alice).enterRaffle(1, 0)).to.revertedWith(
        'ICatRaffle_3_0__ZeroEntries',
      );
    });
  });
  describe('Draw Winners', () => {
    const day = 24 * 60 * 60;
    const month = day * 30;
    const year = month * 12;

    let totalPrizes = 10;
    let maxEntries = 100;
    let totalEntries = 6;
    beforeEach(async () => {
      await landmarkMock.mint(owner.address, 7, 2000);
      await landmarkMock.setApprovalForAll(luckyCatRaffle.address, true);
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: totalPrizes,
        totalEntries: maxEntries,
        userMinEntries: 1,
        userMaxEntries: 1,
        startTime: 0,
        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await luckyCatRaffle.registerRaffle(raffleParams);

      await ticketMock.mint(alice.address, AssetIds.TicketRegular, 10000);
      await luckyCatRaffle.setPaused(1, false);
    });

    it('Winners drawn as expected', async () => {
      let wallet;
      for (let j = 0; j < 10; j++) {
        console.log(`\nSIMULATION ${j}`);
        for (let i = 0; i < totalEntries; i++) {
          wallet = ethers.Wallet.createRandom();
          wallet = wallet.connect(ethers.provider);
          await ticketMock.mint(wallet.address, AssetIds.TicketRegular, 10000);
          await owner.sendTransaction({ to: wallet.address, value: ethers.utils.parseEther('1') });
          await luckyCatRaffle.connect(wallet).enterRaffle(j + 1, 1);
          console.log(wallet.address, ' entered raffle');
        }

        await ethers.provider.send('evm_increaseTime', [month * 2]);
        await ethers.provider.send('evm_mine', []);

        let requestId = await luckyCatRaffle.callStatic.drawWinners(j + 1);
        await luckyCatRaffle.drawWinners(j + 1);

        await vrfCoordinatorV2Mock.fulfillRandomWordsWithOverride(
          requestId,
          luckyCatRaffle.address,
          [getRandomNumber(0, 1000000)],
        );
        expect(await luckyCatRaffle.raffleDrawn(1)).to.true;

        console.log(`\nWinners: ${await luckyCatRaffle.getAllWinningAddresses(j + 1)}`);

        const allWinners = await luckyCatRaffle.getAllWinningAddresses(j + 1);
        const repeatedWinners = findRepeats(allWinners);
        console.log(`Repeats: ${repeatedWinners}`);
        console.log(await luckyCatRaffle.getCumulativeEntries(j + 1));

        const raffleParams = {
          ticketId: AssetIds.TicketRegular,
          prizeAddress: landmarkMock.address,
          prizeType: PrizeType.ERC1155,
          prizeIds: [7],
          prizeAmounts: [2],
          totalPrizes: totalPrizes,
          totalEntries: maxEntries,
          userMinEntries: 1,
          userMaxEntries: 1,
          startTime: 0,
          duration: month,
          raffleTreasury: erc1155Treasury.address,
        };
        await luckyCatRaffle.registerRaffle(raffleParams);

        await ticketMock.mint(alice.address, AssetIds.TicketRegular, 10000);
        await luckyCatRaffle.setPaused(j + 2, false);
      }
    });
    it('Winners can claim as expected', async () => {
      let wallet;
      for (let i = 0; i < 100; i++) {
        wallet = ethers.Wallet.createRandom();
        wallet = wallet.connect(ethers.provider);
        await ticketMock.mint(wallet.address, AssetIds.TicketRegular, 10000);
        await owner.sendTransaction({ to: wallet.address, value: ethers.utils.parseEther('1') });
        await luckyCatRaffle.connect(wallet).enterRaffle(1, 1);
      }

      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      let requestId = await luckyCatRaffle.callStatic.drawWinners(1);
      await luckyCatRaffle.drawWinners(1);

      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, luckyCatRaffle.address);
      expect(await luckyCatRaffle.raffleDrawn(1)).to.true;


      let winners = await luckyCatRaffle.getAllWinningAddresses(1);
      for (let i = 0; i < winners.length; i++) {
        console.log(i)
        await luckyCatRaffle.claimRewards(1, [i]);
        expect(await landmarkMock.balanceOf(winners[i], 7)).to.equal(2);
      }
    });
    it('claiming rewards reverts if user tries to claim twice in a row', async () => {
      let wallet;
      for (let i = 0; i < 100; i++) {
        wallet = ethers.Wallet.createRandom();
        wallet = wallet.connect(ethers.provider);
        await ticketMock.mint(wallet.address, AssetIds.TicketRegular, 10000);
        await owner.sendTransaction({ to: wallet.address, value: ethers.utils.parseEther('1') });
        await luckyCatRaffle.connect(wallet).enterRaffle(1, 1);
      }

      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);

      let requestId = await luckyCatRaffle.callStatic.drawWinners(1);
      await luckyCatRaffle.drawWinners(1);

      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, luckyCatRaffle.address);
      expect(await luckyCatRaffle.raffleDrawn(1)).to.true;


      let winners = await luckyCatRaffle.getAllWinningAddresses(1);
      for (let i = 0; i < winners.length; i++) {
        await luckyCatRaffle.claimRewards(1, [i]);
        // await expect(luckyCatRaffle.claimRewards(1, i)).to.revertedWith('')
        expect(await landmarkMock.balanceOf(winners[i], 7)).to.equal(2);
      }
    });
  });
  describe('Gas tests', () => {
    const day = 24 * 60 * 60;
    const month = day * 30;
    const year = month * 12;
    const entries = 900;
    beforeEach(async () => {
      await landmarkMock.mint(owner.address, 7, 2000000);
      await landmarkMock.setApprovalForAll(luckyCatRaffle.address, true);
      const raffleParams = {
        ticketId: AssetIds.TicketRegular,
        prizeAddress: landmarkMock.address,
        prizeType: PrizeType.ERC1155,
        prizeIds: [7],
        prizeAmounts: [2],
        totalPrizes: entries,
        totalEntries: entries,
        userMinEntries: 1,
        userMaxEntries: entries,
        startTime: 0,
        duration: month,
        raffleTreasury: erc1155Treasury.address,
      };
      await luckyCatRaffle.registerRaffle(raffleParams);

      await ticketMock.mint(alice.address, AssetIds.TicketRegular, 100000);
      await luckyCatRaffle.setPaused(1, false);
    });

    it.only('Winners drawn as expected', async () => {
      const playerEntries = 250
      await luckyCatRaffle.connect(alice).enterRaffle(1, playerEntries);

      await ethers.provider.send('evm_increaseTime', [month * 2]);
      await ethers.provider.send('evm_mine', []);
      let requestId = await luckyCatRaffle.callStatic.drawWinners(1);
      await luckyCatRaffle.drawWinners(1);
      await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, luckyCatRaffle.address);
      expect(await luckyCatRaffle.raffleDrawn(1)).to.true;
      console.log(await luckyCatRaffle.getAllWinningAddresses(1));

      const numbers = [];
      for (let i = 0; i <= playerEntries - 1; i++) {
        numbers.push(i);
      }

      await luckyCatRaffle.connect(alice).claimRewards(1, numbers);
    });
  });
});

function getRandomNumber(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function findRepeats(
  data: string[],
  ignoreAddress = '0x0000000000000000000000000000000000000000',
): string[] {
  let repeats: string[] = [];
  let counts: { [key: string]: number } = {};

  for (let i = 0; i < data.length; i++) {
    let item = data[i];
    if (item !== ignoreAddress) {
      counts[item] = counts[item] ? counts[item] + 1 : 1;
    }
  }

  for (let item in counts) {
    if (counts[item] > 1) {
      repeats.push(item);
    }
  }

  return repeats;
}
