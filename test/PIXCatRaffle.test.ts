import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';
import { Contract, BigNumber, constants, utils } from 'ethers';
import { increaseTime, advanceBlock } from './utils';
import { ERC1155MockAssetManager, IAssetManager, IAssetManager__factory } from '../src/types';

describe('PIXCatRaffle', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let bob: SignerWithAddress;
  let carol: SignerWithAddress;
  let landmark: Contract;
  let ticket: Contract;
  let vrfCoordinatorV2Mock: Contract;
  let pixCatRaffle: Contract;
  let pixCatRaffleKeeper: Contract;

  const maxEntriesPerWallet = 5;
  const maxTotalEntries = 50;
  const keyHash = utils.defaultAbiCoder.encode(['uint256'], [1]);
  const subScriptionId = 1;
  const secondsPerDay = 24 * 3600;

  beforeEach(async function () {
    [owner, alice, bob, carol] = await ethers.getSigners();

    const Mock1155FTFactory = await ethers.getContractFactory('Mock1155FT');
    landmark = await Mock1155FTFactory.deploy('Landmark');
    await landmark.mintBatch(owner.address, [1, 2, 3], [10, 10, 10]);

    const ERC1155MockAssetManagerFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    ticket = await ERC1155MockAssetManagerFactory.deploy('testuri');

    await ticket.mintBatch(alice.address, [1, 2, 3, 16], [10, 10, 10, 10], 0);
    await ticket.mintBatch(bob.address, [1, 2, 3, 16], [10, 10, 10, 10], 0);
    await ticket.mintBatch(carol.address, [1, 2, 3, 16], [10, 10, 10, 10], 0);

    const VRFCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
    vrfCoordinatorV2Mock = await VRFCoordinatorV2MockFactory.deploy(0, 6);
    await vrfCoordinatorV2Mock.createSubscription();
    await vrfCoordinatorV2Mock.fundSubscription(subScriptionId, utils.parseEther('10'));

    const PIXCatRaffleFactory = await ethers.getContractFactory('PIXCatRaffle');
    pixCatRaffle = await upgrades.deployProxy(PIXCatRaffleFactory, [
      landmark.address,
      ticket.address,
      vrfCoordinatorV2Mock.address,
      keyHash,
      subScriptionId,
    ]);
    await vrfCoordinatorV2Mock.addConsumer(subScriptionId, pixCatRaffle.address);

    const PIXCatRaffleKeeperFactory = await ethers.getContractFactory('PIXCatRaffleKeeper');
    pixCatRaffleKeeper = await PIXCatRaffleKeeperFactory.deploy(pixCatRaffle.address);

    await pixCatRaffle.setKeeper(pixCatRaffleKeeper.address);

    await landmark.setApprovalForAll(pixCatRaffle.address, true);
    await ticket.connect(alice).setApprovalForAll(pixCatRaffle.address, true);
    await ticket.connect(bob).setApprovalForAll(pixCatRaffle.address, true);
    await ticket.connect(carol).setApprovalForAll(pixCatRaffle.address, true);

    await ticket.setTrusted(pixCatRaffle.address, true);
  });

  describe('#initialize', () => {
    it('check initial values', async function () {
      expect(await pixCatRaffle.SECONDS_PER_DAY()).equal(secondsPerDay);
      expect(await pixCatRaffle.landmark()).equal(landmark.address);
      expect(await pixCatRaffle.ticket()).equal(ticket.address);
      expect(await pixCatRaffle.keeper()).equal(pixCatRaffleKeeper.address);
      expect(await pixCatRaffle.subscriptionId()).equal(subScriptionId);
      expect(await pixCatRaffle.keyHash()).equal(keyHash);
      expect(await pixCatRaffle.getCurrentDay()).equal(0);
    });
  });

  describe('#set', () => {
    it('setKeyHash by owner', async function () {
      const keyHash = utils.defaultAbiCoder.encode(['uint256'], [2]);

      await expect(pixCatRaffle.connect(alice).setKeyHash(keyHash)).to.be.revertedWith(
        'Ownable: caller is not the owner',
      );

      await pixCatRaffle.setKeyHash(keyHash);
      expect(await pixCatRaffle.keyHash()).equal(keyHash);
    });

    it('setCallbackGasLimit by owner', async function () {
      const callbackGasLimit = 300000;

      await expect(
        pixCatRaffle.connect(alice).setCallbackGasLimit(callbackGasLimit),
      ).to.be.revertedWith('Ownable: caller is not the owner');

      await pixCatRaffle.setCallbackGasLimit(callbackGasLimit);
      expect(await pixCatRaffle.callbackGasLimit()).equal(callbackGasLimit);
    });

    it('setRequestConfirmations by owner', async function () {
      const requestConfirmations = 5;

      await expect(
        pixCatRaffle.connect(alice).setRequestConfirmations(requestConfirmations),
      ).to.be.revertedWith('Ownable: caller is not the owner');

      await pixCatRaffle.setRequestConfirmations(requestConfirmations);
      expect(await pixCatRaffle.requestConfirmations()).equal(requestConfirmations);
    });
  });

  describe('#addPrize', () => {
    const day = 1;
    const ticketId = 16;
    const ids = [1, 2, 3];
    const amounts = [1, 1, 1];

    it('addPrize by owner', async function () {
      await expect(
        pixCatRaffle
          .connect(alice)
          .addPrize(day, ticketId, ids, amounts, maxEntriesPerWallet, maxTotalEntries),
      ).to.be.revertedWith('non moderator');
      await expect(
        pixCatRaffle
          .connect(alice)
          .addPrizeToCurrentDay(ticketId, ids, amounts, maxEntriesPerWallet, maxTotalEntries),
      ).to.be.revertedWith('non moderator');
    });

    it('addPrize with invalid length', async function () {
      await expect(
        pixCatRaffle.addPrize(day, ticketId, ids, [1], maxEntriesPerWallet, maxTotalEntries),
      ).to.be.revertedWith('invalid length');
      await expect(
        pixCatRaffle.addPrizeToCurrentDay(
          ticketId,
          [1],
          amounts,
          maxEntriesPerWallet,
          maxTotalEntries,
        ),
      ).to.be.revertedWith('invalid length');
    });

    it('addPrize with invalid ticketId', async function () {
      const ticketId = 100;

      await expect(
        pixCatRaffle.addPrize(day, ticketId, ids, amounts, maxEntriesPerWallet, maxTotalEntries),
      ).to.be.revertedWith('invalid ticketId');
      await expect(
        pixCatRaffle.addPrizeToCurrentDay(
          ticketId,
          ids,
          amounts,
          maxEntriesPerWallet,
          maxTotalEntries,
        ),
      ).to.be.revertedWith('invalid ticketId');
    });

    it('event', async function () {
      const tx0 = pixCatRaffle.addPrize(
        day,
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );
      await expect(tx0).to.emit(pixCatRaffle, 'PrizeAdded').withArgs(day, ticketId, ids, amounts);

      const tx1 = pixCatRaffle.addPrizeToCurrentDay(
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );
      await expect(tx1).to.emit(pixCatRaffle, 'PrizeAdded').withArgs(0, ticketId, ids, amounts);

      expect(await landmark.balanceOf(pixCatRaffle.address, 1)).equal(2);
      expect(await landmark.balanceOf(pixCatRaffle.address, 2)).equal(2);
      expect(await landmark.balanceOf(pixCatRaffle.address, 3)).equal(2);
    });

    it('dailyTicketInfo', async function () {
      await pixCatRaffle.addPrize(
        day,
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );

      const info = await pixCatRaffle.dailyTickets(day);
      expect(info.ticketId).equal(ticketId);
      expect(info.maxEntriesPerWallet).equal(maxEntriesPerWallet);
      expect(info.maxTotalEntries).equal(maxTotalEntries);
      expect(info.totalTickets).equal(0);
    });

    it('addPrize again', async function () {
      await pixCatRaffle.addPrizeToCurrentDay(
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );

      const tx = pixCatRaffle.addPrize(
        0,
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );
      await expect(tx).to.emit(pixCatRaffle, 'PrizeAdded').withArgs(0, ticketId, ids, amounts);
    });

    it('addPrizeToCurrentDay again', async function () {
      await pixCatRaffle.addPrize(0, ticketId, ids, amounts, maxEntriesPerWallet, maxTotalEntries);

      const tx = pixCatRaffle.addPrizeToCurrentDay(
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );
      await expect(tx).to.emit(pixCatRaffle, 'PrizeAdded').withArgs(0, ticketId, ids, amounts);
    });

    it('reclaim added prize by draw', async function () {
      const day = 0;
      let performData = utils.defaultAbiCoder.encode(['uint256'], [0]);

      await pixCatRaffle.addPrize(
        day,
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );

      await increaseTime(BigNumber.from(secondsPerDay));
      await pixCatRaffleKeeper.performUpkeep(performData);

      expect(await landmark.balanceOf(owner.address, 1)).equal(10);
      expect(await landmark.balanceOf(owner.address, 2)).equal(10);
      expect(await landmark.balanceOf(owner.address, 3)).equal(10);
    });
  });

  describe('#addTickets', () => {
    const day = 0;
    const ticketId = 16;
    const ids = [1, 2, 3];
    const amounts = [1, 1, 1];
    const aliceTickets = 2;
    const bobTickets = 3;
    const maxEntriesPerWallet = 10;
    const maxTotalEntries = 11;

    beforeEach(async function () {
      await pixCatRaffle.addPrize(
        day,
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );
    });

    it('zero tickets', async function () {
      await expect(pixCatRaffle.connect(alice).addTickets(0)).to.be.revertedWith(
        'tickets must be > 0',
      );
    });

    it('no approved/tickets', async function () {
      await expect(pixCatRaffle.addTickets(1)).to.be.revertedWith(
        'ERC1155: burn amount exceeds balance',
      );

      await ticket.setApprovalForAll(pixCatRaffle.address, true);
      await expect(pixCatRaffle.addTickets(1)).to.be.revertedWith(
        'ERC1155: burn amount exceeds balance',
      );
    });

    it('exceed max entries', async function () {
      await expect(
        pixCatRaffle.connect(alice).addTickets(maxEntriesPerWallet + 1),
      ).to.be.revertedWith('exceed max entries per wallet');

      await pixCatRaffle.connect(alice).addTickets(maxEntriesPerWallet);

      await expect(pixCatRaffle.connect(bob).addTickets(maxEntriesPerWallet)).to.be.revertedWith(
        'exceed max total entries',
      );
    });

    it('event', async function () {
      const tx0 = pixCatRaffle.connect(alice).addTickets(aliceTickets);
      await expect(tx0)
        .to.emit(pixCatRaffle, 'TicketsAdded')
        .withArgs(day, alice.address, ticketId, aliceTickets);

      const tx1 = pixCatRaffle.connect(bob).addTickets(bobTickets);
      await expect(tx1)
        .to.emit(pixCatRaffle, 'TicketsAdded')
        .withArgs(day, bob.address, ticketId, bobTickets);
    });

    it('dailyTicketInfo', async function () {
      await pixCatRaffle.connect(alice).addTickets(aliceTickets);
      await pixCatRaffle.connect(bob).addTickets(bobTickets);

      const info = await pixCatRaffle.dailyTickets(day);
      expect(info.ticketId).equal(ticketId);
      expect(info.totalTickets).equal(aliceTickets + bobTickets);
    });

    it('availableTicketsOf', async function () {
      await pixCatRaffle.connect(alice).addTickets(aliceTickets);
      expect(await pixCatRaffle.availableTicketsOf(alice.address)).equal(
        maxEntriesPerWallet - aliceTickets,
      );

      await pixCatRaffle.connect(bob).addTickets(bobTickets);
      expect(await pixCatRaffle.availableTicketsOf(bob.address)).equal(
        maxTotalEntries - aliceTickets - bobTickets,
      );
    });

    it('ticketsOf', async function () {
      await pixCatRaffle.connect(alice).addTickets(aliceTickets);
      await pixCatRaffle.connect(bob).addTickets(bobTickets);

      const aliceInfo = await pixCatRaffle.ticketsOf(day, alice.address);
      expect(aliceInfo[0]).equal(ticketId);
      expect(aliceInfo[1]).equal(aliceTickets);

      const bobInfo = await pixCatRaffle.ticketsOf(day, bob.address);
      expect(bobInfo[0]).equal(ticketId);
      expect(bobInfo[1]).equal(bobTickets);
    });

    it('ticketsOfCurrentDay', async function () {
      await pixCatRaffle.connect(alice).addTickets(aliceTickets);
      await pixCatRaffle.connect(bob).addTickets(bobTickets);

      const aliceInfo = await pixCatRaffle.ticketsOfCurrentDay(alice.address);
      expect(aliceInfo[0]).equal(ticketId);
      expect(aliceInfo[1]).equal(aliceTickets);

      const bobInfo = await pixCatRaffle.ticketsOfCurrentDay(bob.address);
      expect(bobInfo[0]).equal(ticketId);
      expect(bobInfo[1]).equal(bobTickets);
    });

    it('pastTicketsOf', async function () {
      await pixCatRaffle.connect(alice).addTickets(aliceTickets);
      await pixCatRaffle.connect(bob).addTickets(bobTickets);

      const aliceInfo = await pixCatRaffle.pastTicketsOf(alice.address);
      expect(aliceInfo[0]).to.deep.equal([BigNumber.from(ticketId)]);
      expect(aliceInfo[1]).to.deep.equal([BigNumber.from(aliceTickets)]);

      const bobInfo = await pixCatRaffle.pastTicketsOf(bob.address);
      expect(bobInfo[0]).to.deep.equal([BigNumber.from(ticketId)]);
      expect(bobInfo[1]).to.deep.equal([BigNumber.from(bobTickets)]);
    });
  });

  describe('#draw', () => {
    const day = 0;
    const ticketId = 16;
    const ids = [1, 2, 3];
    const amounts = [1, 1, 1];
    const aliceTickets = 2;
    const bobTickets = 2;
    const carolTickets = 2;
    let performData = utils.defaultAbiCoder.encode(['uint256'], [0]);

    beforeEach(async function () {
      await pixCatRaffle.addPrize(
        day,
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );
      await pixCatRaffle.connect(alice).addTickets(aliceTickets);
      await pixCatRaffle.connect(bob).addTickets(bobTickets);
      await pixCatRaffle.connect(carol).addTickets(carolTickets);
    });

    it('only keeper', async function () {
      await expect(pixCatRaffle.connect(alice).draw(0)).to.be.revertedWith('invalid keeper');
    });

    it('not drawable', async function () {
      expect(await pixCatRaffle.drawable(day)).equal(false);
      await expect(pixCatRaffleKeeper.performUpkeep(performData)).to.be.revertedWith('invalid day');
    });

    it('checkUpkeep not drawable', async function () {
      let performData = utils.defaultAbiCoder.encode(['uint256'], [1]);
      await increaseTime(BigNumber.from(secondsPerDay + secondsPerDay));
      const info = await pixCatRaffleKeeper.checkUpkeep(ethers.constants.HashZero);
      expect(info.upkeepNeeded).equal(false);
      expect(info.performData).equal(performData);
    });

    it('checkUpkeep drawable', async function () {
      await increaseTime(BigNumber.from(secondsPerDay));
      const info = await pixCatRaffleKeeper.checkUpkeep(ethers.constants.HashZero);
      expect(info.upkeepNeeded).equal(true);
      expect(info.performData).equal(performData);
    });

    it ('not resolvable without random seed received', async function(){ 
      await increaseTime(BigNumber.from(secondsPerDay));
      await pixCatRaffleKeeper.performUpkeep(performData);

      const requestId = 1;
      expect(await pixCatRaffle.drawRequests(requestId)).equal(0);

      await expect(pixCatRaffle.resolveDay(day)).to.be.revertedWith('random seed not received yet');
    });

    it('draw', async function () {
      await increaseTime(BigNumber.from(secondsPerDay));
      await pixCatRaffleKeeper.performUpkeep(performData);

      const requestId = 1;
      expect(await pixCatRaffle.drawRequests(requestId)).equal(0);

      let tx = await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaffle.address);
      expect(tx).to.emit(pixCatRaffle, 'RandomSeedReceived');

      tx = await pixCatRaffle.resolveDay(day);
      expect(tx).to.emit(pixCatRaffle, 'ResultDrawn').withArgs(day);

      const winners = await pixCatRaffle.winnings(day);
      expect(winners.length).equal(amounts.reduce((sum, amount) => sum + amount));
      console.log('#Winners: ', winners);
    });
  });

  describe('#claim', () => {
    const day = 0;
    const ticketId = 16;
    const ids = [1, 2, 3];
    const amounts = [1, 1, 1];
    const aliceTickets = 2;
    const bobTickets = 2;
    const carolTickets = 2;
    let performData = utils.defaultAbiCoder.encode(['uint256'], [0]);
    let winners = [];

    beforeEach(async function () {
      await pixCatRaffle.addPrize(
        day,
        ticketId,
        ids,
        amounts,
        maxEntriesPerWallet,
        maxTotalEntries,
      );
      await pixCatRaffle.connect(alice).addTickets(aliceTickets);
      await pixCatRaffle.connect(bob).addTickets(bobTickets);
      await pixCatRaffle.connect(carol).addTickets(carolTickets);

      await increaseTime(BigNumber.from(secondsPerDay));
      await pixCatRaffleKeeper.performUpkeep(performData);

      const requestId = 1;
      expect(await pixCatRaffle.drawRequests(requestId)).equal(0);

      let tx = await vrfCoordinatorV2Mock.fulfillRandomWords(requestId, pixCatRaffle.address);
      expect(tx).to.emit(pixCatRaffle, 'RandomSeedReceived');

      tx = await pixCatRaffle.resolveDay(day);
      expect(tx).to.emit(pixCatRaffle, 'ResultDrawn').withArgs(day);

      winners = await pixCatRaffle.winnings(day);
    });

    it('claimable', async function () {
      const info0 = await pixCatRaffle.claimable(winners[0]);
      console.log("#Winner0's claimable: ", info0);
      const info1 = await pixCatRaffle.claimable(winners[1]);
      console.log("#Winner1's claimable: ", info1);
      const info2 = await pixCatRaffle.claimable(winners[2]);
      console.log("#Winner2's claimable: ", info2);
    });

    it('claim', async function () {
      expect(await landmark.balanceOf(alice.address, 1)).equal(0);
      expect(await landmark.balanceOf(alice.address, 2)).equal(0);
      expect(await landmark.balanceOf(alice.address, 3)).equal(0);
      expect(await landmark.balanceOf(bob.address, 1)).equal(0);
      expect(await landmark.balanceOf(bob.address, 2)).equal(0);
      expect(await landmark.balanceOf(bob.address, 3)).equal(0);
      expect(await landmark.balanceOf(carol.address, 1)).equal(0);
      expect(await landmark.balanceOf(carol.address, 2)).equal(0);
      expect(await landmark.balanceOf(carol.address, 3)).equal(0);

      if (winners.indexOf(alice.address) != -1) {
        await pixCatRaffle.connect(alice).claimPrize();
      } else {
        await expect(pixCatRaffle.connect(alice).claimPrize()).to.be.revertedWith('no prize');
      }

      if (winners.indexOf(bob.address) != -1) {
        await pixCatRaffle.connect(bob).claimPrize();
      } else {
        await expect(pixCatRaffle.connect(bob).claimPrize()).to.be.revertedWith('no prize');
      }

      if (winners.indexOf(carol.address) != -1) {
        await pixCatRaffle.connect(carol).claimPrize();
      } else {
        await expect(pixCatRaffle.connect(carol).claimPrize()).to.be.revertedWith('no prize');
      }

      const aliceBalanceOf1 = await landmark.balanceOf(alice.address, 1);
      const aliceBalanceOf2 = await landmark.balanceOf(alice.address, 2);
      const aliceBalanceOf3 = await landmark.balanceOf(alice.address, 3);
      const bobBalanceOf1 = await landmark.balanceOf(bob.address, 1);
      const bobBalanceOf2 = await landmark.balanceOf(bob.address, 2);
      const bobBalanceOf3 = await landmark.balanceOf(bob.address, 3);
      const carolBalanceOf1 = await landmark.balanceOf(carol.address, 1);
      const carolBalanceOf2 = await landmark.balanceOf(carol.address, 2);
      const carolBalanceOf3 = await landmark.balanceOf(carol.address, 3);

      expect(aliceBalanceOf1.add(bobBalanceOf1).add(carolBalanceOf1)).equal(1);
      expect(aliceBalanceOf2.add(bobBalanceOf2).add(carolBalanceOf2)).equal(1);
      expect(aliceBalanceOf3.add(bobBalanceOf3).add(carolBalanceOf3)).equal(1);

      await expect(pixCatRaffle.connect(alice).claimPrize()).to.be.revertedWith('no prize');
      await expect(pixCatRaffle.connect(bob).claimPrize()).to.be.revertedWith('no prize');
      await expect(pixCatRaffle.connect(carol).claimPrize()).to.be.revertedWith('no prize');

      expect(await pixCatRaffle.claimable(alice.address)).to.deep.equal([[], []]);
      expect(await pixCatRaffle.claimable(bob.address)).to.deep.equal([[], []]);
      expect(await pixCatRaffle.claimable(carol.address)).to.deep.equal([[], []]);
    });
  });
});
