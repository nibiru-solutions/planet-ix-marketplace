import { expect } from 'chai';
import { ethers, upgrades } from 'hardhat';

describe('FluencrSale', function () {
  let owner: SignerWithAddress;
  let alice: SignerWithAddress;
  let flunecrAddress: SignerWithAddress;

  let pixAssets: Contract;
  let ixt: Contract;
  let fluencrSale: Contract;

  beforeEach(async function () {
    [owner, alice, flunecrAddress] = await ethers.getSigners();

    let pixAssetsFactory = await ethers.getContractFactory("PIXVouchers");
    pixAssets = await upgrades.deployProxy(pixAssetsFactory, ["",""]);

    let fluencrSaleFactory = await ethers.getContractFactory("FluencrSale");
    fluencrSale = await upgrades.deployProxy(fluencrSaleFactory, [flunecrAddress.address]);

    let ixtFactory = await ethers.getContractFactory("PIXT");
    ixt = await ixtFactory.deploy();

    await pixAssets.setTrustedContract(fluencrSale.address);
    await pixAssets.setURI(1, "uri");
  });

  describe('#create product', () => {
    let price = 10;
    let tokenId = 1;
    let amount = 1;
    let invalidPrice = 0;
    it('is created properly', async function () {
      await fluencrSale.createProduct(30000, "url", price, pixAssets.address, tokenId, amount, "eu");
      expect((await fluencrSale.listProducts("eu")).length).to.be.equal(1);
    });

    it('should revert if it is called by non owner', async function () {
      await expect(fluencrSale.connect(alice).createProduct(0, "url", price, pixAssets.address, tokenId, amount, "eu")).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it('should revert if price is 0', async function () {
      await expect(fluencrSale.createProduct(0, "url", invalidPrice, pixAssets.address, tokenId, amount, "eu")).to.be.revertedWithCustomError(fluencrSale, "ProductPriceInvalid");
    });

    it("should emit correct event upon product creation", async function () {
      expect(await fluencrSale.createProduct(0, "url", price, pixAssets.address, tokenId, amount, "eu")).to.emit(fluencrSale, "ProductCreated");
    });
  });

  describe('#change product', () => {
    let price = 10;
    let tokenId = 1;
    let amount = 1;

    let newTokenId = 2;
    let newAmount = 5;
    let invalidPrice = 0;
    beforeEach(async function () {
      await fluencrSale.createProduct(1, "url", price, pixAssets.address, tokenId, amount, "eu");
      expect((await fluencrSale.listProducts("eu")).length).to.be.equal(1);
    });

    it('should revert if it is called by non owner', async function () {
      await expect(fluencrSale.connect(alice).changeProduct(1, 1, "url", price, pixAssets.address, tokenId, amount, "zh")).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it('should revert if product does not exist', async function () {
      await expect(fluencrSale.changeProduct(2, 2, "url", price, pixAssets.address, tokenId, amount, "zh")).to.be.revertedWithCustomError(fluencrSale, "NonExistentProduct");
    });
    it('should revert if product price is not set', async function () {
      await expect(fluencrSale.changeProduct(1, 1, "url", invalidPrice, pixAssets.address, tokenId, amount, "zh")).to.be.revertedWithCustomError(fluencrSale, "ProductPriceInvalid");
    });
    it("should change the product data", async function () {
      await fluencrSale.changeProduct(1, 1, "new url", price, pixAssets.address, newTokenId, newAmount,"eu");
      await fluencrSale.createProduct(3, "url", price, pixAssets.address, tokenId, amount, "zh");
      await fluencrSale.createProduct(4, "url", price, pixAssets.address, tokenId, amount, "eu");
      //console.log(await fluencrSale.listProducts("zh"));
      //console.log(await fluencrSale.listProducts("eu"));
      expect((await fluencrSale.listProducts("eu")).length).to.be.equal(2);
      expect((await fluencrSale.listProducts("zh")).length).to.be.equal(1);

    })
  });

  describe('#set alternative language', () => {
    let price = 10;
    let tokenId = 1;
    let amount = 1;
    let productId = 1; 
    
    let newTokenId = 2;
    let newAmount = 5;
    beforeEach(async function () {
      await fluencrSale.createProduct(0, "url", price, pixAssets.address, tokenId, amount, "eu");
      expect((await fluencrSale.listProducts("eu")).length).to.be.equal(1);
    });

    it("should return the alternative language", async function () {
      await fluencrSale.createProduct(0, "url", price, pixAssets.address, newTokenId, newAmount, "eu");
      await fluencrSale.setAlternativeLanguageUris(2, ["zh"],["Novi URI"]);
      //console.log(await fluencrSale.listProducts("zh"));
    })
  });

  describe('#sell product', () => {
    let price = 10;
    let tokenId = 1;
    let amount = 1;
    let productId = 1; 
    beforeEach(async function () {
      await fluencrSale.createProduct(1, "url", price, pixAssets.address, tokenId, amount, "eu");
      await fluencrSale.setSaleLimits(productId, amount);
    });
    it('Is created properly', async function () {
      expect((await fluencrSale.listProducts("eu")).length).to.be.equal(1);
    });

    it('should revert if the contract is paused', async function () {
      await expect(fluencrSale.connect(flunecrAddress).sell(alice.address, 1, pixAssets.address)).to.be.revertedWith("Pausable: paused");
    });

    it('should revert if the contract is not called by flunecr contract', async function () {
      await fluencrSale.toggleStatus();
      await expect(fluencrSale.connect(alice).sell(alice.address, 1, pixAssets.address)).to.be.revertedWithCustomError(fluencrSale, "NotAffiliate");
    });

    it('should revert if the product sale limit is reached', async function () {
      await fluencrSale.toggleStatus();
      await fluencrSale.connect(flunecrAddress).sell(alice.address, 1, pixAssets.address);
      await expect(fluencrSale.connect(flunecrAddress).sell(alice.address, 1, ixt.address)).to.be.revertedWithCustomError(fluencrSale, "SaleLimitExceeded");
    });

    it("should update sales variable", async function () {
      await fluencrSale.toggleStatus();
      await fluencrSale.setSaleLimits(productId, 5);
      await fluencrSale.connect(flunecrAddress).sell(alice.address, 1, pixAssets.address);
      await fluencrSale.connect(flunecrAddress).sell(alice.address, 1, pixAssets.address);
      expect(await fluencrSale.sales(1)).to.be.equal(2);
    })

    it("should emit correct event upon product sale", async function () {
      await fluencrSale.toggleStatus();
      expect(await fluencrSale.connect(flunecrAddress).sell(flunecrAddress.address, 1, ixt.address)).to.emit(fluencrSale, "ProductSold");
      expect(await pixAssets.balanceOf(flunecrAddress.address, tokenId)).to.be.equal(1);
    })
  });
});
