import { assert, expect } from 'chai';
import { Contract, Signer } from 'ethers';
import { network, deployments, ethers, getNamedAccounts } from 'hardhat';
import { networkConfig, developmentChains } from '../helper-hardhat-config';
import { NFT } from '../src/types';

 describe('NFT Unit Tests', function () {
      let nft: NFT;
      let deployer: Signer;
      let player: Signer;
      let gwsAccount: Signer;

      beforeEach(async () => {
        const accounts = await ethers.getSigners();
        deployer = accounts[0];
        player = accounts[1];
        gwsAccount = accounts[2];
        await deployments.fixture(['nft']);
        nft = await ethers.getContractAt('NFT', (await deployments.get('NFT')).address);
      });

      it('Is initialised properly', async function () {
        const name = await nft.connect(player).name();
        const symbol = await nft.connect(player).symbol();

        assert.equal(name, 'PlanetIX - Genesis Corporations');
        assert.equal(symbol, 'PIX-GC');
      });

      it('Sets the URI', async function () {
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        await nft.setURI(7, uri);
        const uriExpected = await nft.connect(player).uri(7);
        assert.equal(uri, uriExpected);
      });

      it('mints tokens', async function () {
        // Arrange
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const playerAddress = await player.getAddress();
        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(await player.getAddress());

        // Act
        await nft.connect(player).trustedMint(playerAddress, 8, 1);

        // Assert
        const playerACBalance = await nft.connect(player).balanceOf(playerAddress, 8);
        assert.equal(1, playerACBalance.toNumber());
      });

      it('mints a batch of tokens', async function () {
        // Arrange
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const playerAddress = await player.getAddress();
        const tokens = [7, 8];
        const tokenAmounts = [2, 2];
        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(await player.getAddress());

        // Act
        await nft.connect(player).trustedBatchMint(playerAddress, tokens, tokenAmounts);

        // Assert
        const playerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);
        const playerACBalance = await nft.connect(player).balanceOf(playerAddress, 8);
        assert.equal(2, playerWasteBalance.toNumber());
        assert.equal(2, playerACBalance.toNumber());
      });

      it('burns tokens', async function () {
        // Arrange
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const playerAddress = await player.getAddress();
        await nft.setURI(7, uri);
        await nft.setTrustedContract(await player.getAddress());
        await nft.connect(player).trustedMint(playerAddress, 7, 1);

        // Act
        await nft.connect(player).trustedBurn(playerAddress, 7, 1);

        // Assert
        const playerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);
        assert.equal(0, playerWasteBalance.toNumber());
      });

      it('burns a batch of tokens', async function () {
        // Arrange
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const playerAddress = await player.getAddress();
        const tokens = [7, 8];
        const tokenAmounts = [2, 2];
        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(await player.getAddress());
        await nft.connect(player).trustedBatchMint(playerAddress, tokens, tokenAmounts);

        // Act
        await nft.connect(player).trustedBatchBurn(playerAddress, tokens, tokenAmounts);

        // Assert
        const playerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);
        const playerACBalance = await nft.connect(player).balanceOf(playerAddress, 8);
        assert.equal(0, playerWasteBalance.toNumber());
        assert.equal(0, playerACBalance.toNumber());
      });

      describe('mints with signature', async function() {

        let playerAddress: string;
        let tokenIds: number[];
        let tokenAmounts: number[];
        let signatureNonce: number;
        let validSignature: string;

        beforeEach(async () => {
          await nft.setTrustedContract(await deployer.getAddress());

          playerAddress = await player.getAddress();
          tokenIds = [7, 8];
          tokenAmounts = [2,3];
          signatureNonce = 1;

          let message = ethers.utils.defaultAbiCoder.encode(
            [
              'address',
              'uint256[]',
              'uint256[]',
              'uint256',
            ],
            [playerAddress, tokenIds, tokenAmounts, signatureNonce],
          );
          let hash = ethers.utils.keccak256(message);
          validSignature = await deployer.signMessage(ethers.utils.arrayify(hash));
        });

        it('mints a batch of tokens with signature', async function () {

          await nft.connect(player).mintWithSignature(tokenIds, tokenAmounts, signatureNonce, validSignature);

          for(let i=0; i<tokenIds.length; i++) {
            let balance = await nft.balanceOf(playerAddress, tokenIds[i]);
            assert.equal(balance.toNumber(), tokenAmounts[i]);
          }

        });

        it('reverts on mint with invalid signature', async function () {
          let wrongTokenAmounts = [5,6];
          let message = ethers.utils.defaultAbiCoder.encode(
            [
              'address',
              'uint256[]',
              'uint256[]',
              'uint256',
            ],
            [playerAddress, tokenIds, wrongTokenAmounts, signatureNonce],
          );
          let hash = ethers.utils.keccak256(message);
          let invalidSignature = await deployer.signMessage(ethers.utils.arrayify(hash));

          await expect(nft.connect(player).mintWithSignature(tokenIds, tokenAmounts, signatureNonce, invalidSignature))
                  .to.be.revertedWith("NFT: INVALID_SIGNATURE");

        });

        it('reverts on mint with used nonce', async function () {
          await nft.connect(player).mintWithSignature(tokenIds, tokenAmounts, signatureNonce, validSignature);

          await expect(nft.connect(player).mintWithSignature(tokenIds, tokenAmounts, signatureNonce, validSignature))
                  .to.be.revertedWith("NFT: USED_NONCE");
        });

      });
});
