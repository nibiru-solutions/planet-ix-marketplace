import { expect } from 'chai';
import hre, { ethers, upgrades } from 'hardhat';
import { Signer, Contract, BigNumber, providers, utils } from 'ethers';

import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { ERC1155MockAssetManager } from '../src/types/ERC1155MockAssetManager';
import { ERC721Mock } from '../src/types/ERC721Mock';
import { GravityGradeMock } from '../src/types/GravityGradeMock';
import { BonusConsumerMock } from '../src/types/BonusConsumerMock';
import { SaleBonuses } from '../src/types/SaleBonuses';
import { VRFManager } from '../src/types/VRFManager';

describe('Generic Reward System', function () {
  let gravityGrade: GravityGradeMock;
  let saleBonuses: SaleBonuses;
  let assets: ERC1155MockAssetManager;
  let toyNFT: ERC1155MockAssetManager;
  let erc721Mock: ERC721Mock;
  let bonusConsumer: BonusConsumerMock;

  let owner: SignerWithAddress;
  let alice: SignerWithAddress;

  let vrfCoordinatorV2Mock: Contract;
  let vrfManager: VRFManager;
  const SUBSCRIPTION_ID = 1;
  const KEYHASH = ethers.utils.defaultAbiCoder.encode(['uint256'], [1]);
  const CALLBACK_GAS_LIMIT = 2_500_000;
  const REQUEST_CONFIRMATIONS = 3;
  const NUM_WORDS = 1;

  beforeEach(async function () {
    [owner, alice] = await ethers.getSigners();

    let VRFCoordinatorV2MockFactory = await ethers.getContractFactory('VRFCoordinatorV2Mock');
    vrfCoordinatorV2Mock = await VRFCoordinatorV2MockFactory.deploy(0, 6);
    await vrfCoordinatorV2Mock.createSubscription();
    await vrfCoordinatorV2Mock.fundSubscription(SUBSCRIPTION_ID, ethers.utils.parseEther('1000000'));

    let VRFManagerFactory = await ethers.getContractFactory('VRFManager');
    vrfManager = await VRFManagerFactory.deploy(vrfCoordinatorV2Mock.address);

    let ggFactory = await ethers.getContractFactory('GravityGradeMock');
    gravityGrade = await ggFactory.deploy();
    let assetFactory = await ethers.getContractFactory('ERC1155MockAssetManager');
    assets = await assetFactory.deploy('toy uri');
    toyNFT = await assetFactory.deploy('TOY NFT 1');

    let erc721Factory = await ethers.getContractFactory('ERC721Mock');
    erc721Mock = await erc721Factory.deploy('TOYTOKEN', 'TOY');

    let saleBonusesFactory = await ethers.getContractFactory('SaleBonuses');
    saleBonuses = (await upgrades.deployProxy(saleBonusesFactory, [])) as SaleBonuses;

    let bonusConsumerFactory = await ethers.getContractFactory('BonusConsumerMock');
    bonusConsumer = await bonusConsumerFactory.deploy(saleBonuses.address);

    await saleBonuses.setVRFOracle(vrfManager.address);

    await assets.setTrusted(saleBonuses.address, true);
    await erc721Mock.setTrusted(saleBonuses.address, true);
    await gravityGrade.setTrusted(saleBonuses.address, true);

    await vrfManager.setConsumer(saleBonuses.address);
    await vrfManager.setConsumerParams(saleBonuses.address, SUBSCRIPTION_ID, vrfCoordinatorV2Mock.address, KEYHASH, CALLBACK_GAS_LIMIT, REQUEST_CONFIRMATIONS, NUM_WORDS);
  });

  describe('Create content category', function () {
    let tokenId = 1;
    let tokenId2 = 2;
    it('Should emit event', async function () {
      await expect(saleBonuses.createContentCategory(bonusConsumer.address, tokenId)).to.emit(saleBonuses, 'CategoryCreated').withArgs(bonusConsumer.address, tokenId, 1);
    });
    it('Handles creation of multiple categories', async () => {
      await expect(saleBonuses.createContentCategory(bonusConsumer.address, tokenId)).to.emit(saleBonuses, 'CategoryCreated').withArgs(bonusConsumer.address, tokenId, 1);
      await expect(saleBonuses.createContentCategory(bonusConsumer.address, tokenId)).to.emit(saleBonuses, 'CategoryCreated').withArgs(bonusConsumer.address, tokenId, 2);
      await expect(saleBonuses.createContentCategory(bonusConsumer.address, tokenId)).to.emit(saleBonuses, 'CategoryCreated').withArgs(bonusConsumer.address, tokenId, 3);
      await expect(saleBonuses.createContentCategory(bonusConsumer.address, tokenId2)).to.emit(saleBonuses, 'CategoryCreated').withArgs(bonusConsumer.address, tokenId2, 1);
      await expect(saleBonuses.createContentCategory(bonusConsumer.address, tokenId2)).to.emit(saleBonuses, 'CategoryCreated').withArgs(bonusConsumer.address, tokenId2, 2);
      await expect(saleBonuses.createContentCategory(bonusConsumer.address, tokenId2)).to.emit(saleBonuses, 'CategoryCreated').withArgs(bonusConsumer.address, tokenId2, 3);
    });

    it('Should not assign subsequent content categories to same id', async function () {
      expect(true).to.equal(true);
    });
  });

  describe('Create content category reverts', function () {
    let tokenId1 = 1;
    let tokenId2 = 2;
    it('Should revert on non owner call', async function () {
      await expect(saleBonuses.connect(alice).createContentCategory(bonusConsumer.address, tokenId1)).to.revertedWith(`SB__Unauthorized("${await alice.getAddress()}")`);
    });
  });

  describe('Delete content category', function () {
    let tokenId1 = 1;
    let tokenId2 = 2;
    let categoryId1;
    let categoryId2;
    let categoryId3;
    let categoryId4;
    let categoryId5;
    let categoryId6;
    beforeEach(async () => {
      categoryId1 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);

      categoryId2 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);

      categoryId3 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId2);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId2);

      categoryId4 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId2);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId2);

      categoryId5 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);

      categoryId6 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
    });
    it('Should emit event', async function () {
      await expect(saleBonuses.deleteContentCategory(bonusConsumer.address, tokenId1, categoryId1))
        .to.emit(saleBonuses, 'CategoryDeleted')
        .withArgs(bonusConsumer.address, tokenId1, categoryId1);
    });
    it('Should remove content category from getContentCategory call', async function () {
      let categoriesBefore = await saleBonuses.getContentCategories(bonusConsumer.address, tokenId1);
      let lengthBefore = categoriesBefore.length;

      await saleBonuses.deleteContentCategory(bonusConsumer.address, tokenId1, categoryId1);

      let categoriesAfter = await saleBonuses.getContentCategories(bonusConsumer.address, tokenId1);
      expect(categoriesAfter.find((category) => category['id'].toNumber() == categoryId1)).to.undefined;
      expect(categoriesAfter.length).to.equal(lengthBefore - 1);
    });
    it('Deletes all categories from a tokenId successfully', async () => {
      let categoriesBefore = await saleBonuses.getContentCategories(bonusConsumer.address, tokenId1);
      let lengthBefore = categoriesBefore.length;

      await saleBonuses.deleteContentCategory(bonusConsumer.address, tokenId1, categoryId1);
      await saleBonuses.deleteContentCategory(bonusConsumer.address, tokenId1, categoryId2);

      let categoriesAfter = await saleBonuses.getContentCategories(bonusConsumer.address, tokenId1);
      expect(categoriesAfter.find((category) => category['id'].toNumber() == categoryId1)).to.undefined;
      expect(categoriesAfter.find((category) => category['id'].toNumber() == categoryId2)).to.undefined;
      expect(categoriesAfter.length).to.equal(lengthBefore - 2);
    });
  });

  describe('Delete content category reverts', function () {
    let tokenId1 = 1;
    let categoryId1;
    beforeEach(async () => {
      categoryId1 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
    });
    it('Should revert on non owner call', async function () {
      await expect(saleBonuses.connect(alice).deleteContentCategory(bonusConsumer.address, tokenId1, categoryId1)).to.revertedWith(
        `SB__Unauthorized("${await alice.getAddress()}")`,
      );
    });
    it('Should revert on invalid category id', async function () {
      await expect(saleBonuses.deleteContentCategory(bonusConsumer.address, tokenId1, 99)).to.revertedWith(`SB__InvalidCategoryId(${99})`);
    });
    it('Should revert on deleted category id', async function () {
      await saleBonuses.deleteContentCategory(bonusConsumer.address, tokenId1, categoryId1);
      await expect(saleBonuses.deleteContentCategory(bonusConsumer.address, tokenId1, categoryId1)).to.revertedWith(`SB__InvalidCategoryId(${categoryId1})`);
    });
  });

  describe('Set content amounts', function () {
    let amounts = [20, 30, 40, 10, 5];
    let amountWeights = [50, 20, 10, 15, 25];
    let categoryId1;
    let tokenId1 = 1;
    beforeEach(async () => {
      await saleBonuses.setMaxDraws(bonusConsumer.address, tokenId1, 40);
      categoryId1 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
    });
    it('Should emit event', async function () {
      await expect(saleBonuses.setContentAmounts(bonusConsumer.address, tokenId1, categoryId1, amounts, amountWeights))
        .to.emit(saleBonuses, 'ContentAmountsUpdated')
        .withArgs(bonusConsumer.address, tokenId1, categoryId1, amounts, amountWeights);
    });
  });

  describe('Set content amounts reverts', function () {
    let amounts = [20, 30, 40, 10, 5];
    let amountWeights = [50, 20, 10, 15, 25];
    let categoryId1;
    let tokenId1 = 1;
    beforeEach(async () => {
      await saleBonuses.setMaxDraws(bonusConsumer.address, tokenId1, 40);
      categoryId1 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
    });
    it('Should revert on non owner call', async function () {
      await expect(saleBonuses.connect(alice).setContentAmounts(bonusConsumer.address, tokenId1, categoryId1, amounts, amountWeights)).to.revertedWith(
        `SB__Unauthorized("${await alice.getAddress()}")`,
      );
    });
    it('Should revert on zero weight', async function () {
      await expect(saleBonuses.setContentAmounts(bonusConsumer.address, tokenId1, categoryId1, amounts, [50, 20, 10, 0, 25])).to.revertedWith('SB__ZeroWeight()');
    });
    it('Should revert on invalid category id', async function () {
      await expect(saleBonuses.setContentAmounts(bonusConsumer.address, tokenId1, 99, amounts, amountWeights)).to.revertedWith(`SB__InvalidCategoryId(${99})`);
    });
    it('Should revert on argument arrays not being the same lengths', async function () {
      await expect(saleBonuses.setContentAmounts(bonusConsumer.address, tokenId1, categoryId1, amounts, [50, 20, 10, 15, 25, 30])).to.revertedWith('SB__ArraysNotSameLength()');
    });
  });

  describe('Set contents', function () {
    let tokenIds = [1, 2, 3];
    let amounts = [20, 30, 40];
    let amountWeights = [50, 20, 10];
    let categoryId1;
    let tokenId1 = 1;
    beforeEach(async () => {
      categoryId1 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
    });
    it('Should emit event', async function () {
      await expect(
        saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId1, {
          _tokens: [assets.address, assets.address, assets.address],
          _ids: tokenIds,
          _amounts: amounts,
          _weights: amountWeights,
        }),
      )
        .to.emit(saleBonuses, 'ContentsUpdated')
        .withArgs(bonusConsumer.address, tokenId1, categoryId1, [assets.address, assets.address, assets.address], tokenIds, amounts, amountWeights);
    });
    it('Works with upgradeable contracts', async () => {
      await expect(
        saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId1, {
          _tokens: [gravityGrade.address, gravityGrade.address, gravityGrade.address],
          _ids: tokenIds,
          _amounts: amounts,
          _weights: amountWeights,
        }),
      )
        .to.emit(saleBonuses, 'ContentsUpdated')
        .withArgs(bonusConsumer.address, tokenId1, categoryId1, [gravityGrade.address, gravityGrade.address, gravityGrade.address], tokenIds, amounts, amountWeights);
    });
  });
  describe('Set contents reverts', function () {
    let tokenIds = [1, 2, 3];
    let amounts = [20, 30, 40];
    let amountWeights = [50, 20, 10];
    let categoryId1;
    let tokenId1 = 1;
    beforeEach(async () => {
      categoryId1 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
    });
    it('Should revert on non owner call', async function () {
      await expect(
        saleBonuses.connect(alice).setContents(bonusConsumer.address, tokenId1, categoryId1, {
          _tokens: [assets.address, assets.address, assets.address],
          _ids: tokenIds,
          _amounts: amounts,
          _weights: amountWeights,
        }),
      ).to.revertedWith(`SB__Unauthorized("${await alice.getAddress()}")`);
    });
    it('Should revert on zero weight', async function () {
      await expect(
        saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId1, {
          _tokens: [assets.address, assets.address, assets.address],
          _ids: tokenIds,
          _amounts: amounts,
          _weights: [50, 20, 0],
        }),
      ).to.revertedWith('SB__ZeroWeight()');
    });
    it('Should revert on zero amount', async function () {
      await expect(
        saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId1, {
          _tokens: [assets.address, assets.address, assets.address],
          _ids: tokenIds,
          _amounts: [20, 0, 40],
          _weights: amountWeights,
        }),
      ).to.revertedWith('SB__ZeroAmount()');
    });
    it('Should revert on invalid category id', async function () {
      await expect(
        saleBonuses.setContents(bonusConsumer.address, tokenId1, 99, {
          _tokens: [assets.address, assets.address, assets.address],
          _ids: tokenIds,
          _amounts: amounts,
          _weights: amountWeights,
        }),
      ).to.revertedWith('SB__InvalidCategoryId(99)');
    });
    it('Should revert on non erc721 or erc1155 being added', async function () {
      await expect(
        saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId1, {
          _tokens: [assets.address, '0xa406d6AD2f0713e1E8c7aE5FB6F9F75A52714E89', assets.address],
          _ids: tokenIds,
          _amounts: amounts,
          _weights: amountWeights,
        }),
      ).to.revertedWith('SB__NotERC721or1155("0xa406d6AD2f0713e1E8c7aE5FB6F9F75A52714E89")');
    });
    it('Should revert on argument arrays not being the same lengths', async function () {
      await expect(
        saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId1, {
          _tokens: [assets.address, assets.address, assets.address],
          _ids: tokenIds,
          _amounts: amounts,
          _weights: [50, 20, 10, 5],
        }),
      ).to.revertedWith('SB__ArraysNotSameLength()');
    });
  });

  describe('Claim Reward', function () {
    let burnAmount = 1;
    let tokenId1 = 1;
    let tokens;
    let categoryId1;
    let categoryId2;
    let categoryId3;
    let predetermined_draws;
    let maxDraws = 10;

    beforeEach(async () => {
      tokens = [assets.address, assets.address, assets.address];

      predetermined_draws = 10;
      await saleBonuses.setMaxDraws(bonusConsumer.address, tokenId1, maxDraws);

      categoryId1 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.setContentAmounts(bonusConsumer.address, tokenId1, categoryId1, [predetermined_draws, 2], [10, 10]);
      await saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId1, { _tokens: [assets.address], _ids: [19], _amounts: [1], _weights: [1] });

      categoryId2 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.setContentAmounts(bonusConsumer.address, tokenId1, categoryId2, [predetermined_draws, 4, 5], [50, 20, 10]);
      await saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId2, {
        _tokens: [assets.address, assets.address, assets.address], // token addresses
        _ids: [1, 2, 3], // token Ids
        _amounts: [2, 3, 5], // token amounts
        _weights: [40, 20, 50], // token weights
      });

      categoryId3 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.setContentAmounts(bonusConsumer.address, tokenId1, categoryId3, [5, predetermined_draws, 7], [225, 200, 200]);
      await saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId3, { _tokens: [erc721Mock.address], _ids: [0], _amounts: [1], _weights: [1] });

      let conditionalBonusFactory = await ethers.getContractFactory('MockConditionalProvider');
      let conditionalBonusProvider = await conditionalBonusFactory.deploy();
      await conditionalBonusProvider.setResult(true);

      await saleBonuses.setBonusEligibility(bonusConsumer.address, tokenId1, conditionalBonusProvider.address);
      await saleBonuses.setTrusted(bonusConsumer.address, true);

      await quickDrop(alice, gravityGrade, 1, 100);
    });
    it('Should emit event', async function () {
      await expect(bonusConsumer.claimBonusReward(tokenId1, 1, false, alice.address)).to.not.reverted;
    });
    it('Should yield expected rewards when deterministic', async function () {
      let tx = await bonusConsumer.claimBonusReward(tokenId1, 1, false, alice.address);

      let tx2 = await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });
      let receipt = await tx2.wait();
      // console.log(receipt.events);

      await printBalances(alice, gravityGrade, assets, erc721Mock);
    });
    it('Should yield expected rewards when conditional categories present', async () => {
      let conditionalFactory = await ethers.getContractFactory('MockConditionalProvider');
      let conditionalProvider = await conditionalFactory.deploy();

      await saleBonuses.setContentEligibility(bonusConsumer.address, tokenId1, categoryId1, conditionalProvider.address);
      await saleBonuses.setContentEligibility(bonusConsumer.address, tokenId1, categoryId2, conditionalProvider.address);

      let tx = await bonusConsumer.claimBonusReward(tokenId1, 1, true, alice.address);
      await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });
      await printBalances(alice, gravityGrade, assets, erc721Mock);
    });
    it('Todo: Add a lot of various combinations of contents, like 10 of them', async function () {
      const categoryId4 = await saleBonuses.callStatic.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.createContentCategory(bonusConsumer.address, tokenId1);
      await saleBonuses.setContentAmounts(bonusConsumer.address, tokenId1, categoryId4, [predetermined_draws, 4, 5], [50, 20, 10]);
      await saleBonuses.setContents(bonusConsumer.address, tokenId1, categoryId4, {
        _tokens: [
          assets.address,
          assets.address,
          assets.address,
          erc721Mock.address,
          erc721Mock.address,
          assets.address,
          assets.address,
          erc721Mock.address,
          assets.address,
          assets.address,
        ], // token addresses
        _ids: [1, 2, 3, 0, 0, 19, 4, 0, 19, 19], // token Ids
        _amounts: [2, 3, 5, 1, 1, 4, 2, 1, 3, 3], // token amounts
        _weights: [10, 20, 30, 21, 33, 53, 20, 10, 5, 20], // token weights
      });
      let tx = await bonusConsumer.claimBonusReward(tokenId1, 4, false, alice.address);
      let tx_fulfillCategory_draws = await vrfCoordinatorV2Mock.fulfillRandomWords(1, vrfManager.address, {
        gasLimit: CALLBACK_GAS_LIMIT,
      });
      const receipt = await tx_fulfillCategory_draws.wait();
      // console.log(receipt.events);
      await printBalances(alice, gravityGrade, assets, erc721Mock);
    });
  });
});

async function quickDrop(player: Signer, gg: Contract, tokenId, num = 1) {
  await gg.airdrop([await player.getAddress()], [tokenId], [num]);
}

async function printBalances(user: Signer, gravityGrade: Contract, assets: Contract, erc721Mock: Contract) {
  const aliceBal = await gravityGrade.balanceOf(await user.getAddress(), 19);
  const aliceBal1 = await assets.balanceOf(await user.getAddress(), 1);
  const aliceBal2 = await assets.balanceOf(await user.getAddress(), 2);
  const aliceBal3 = await assets.balanceOf(await user.getAddress(), 3);
  const aliceBal19 = await assets.balanceOf(await user.getAddress(), 19);
  const aliceBal4 = await erc721Mock.balanceOf(await user.getAddress());
  console.log(`Alice's Assets 1 balance: ${aliceBal1}`);
  console.log(`Alice's Assets 2 balance: ${aliceBal2}`);
  console.log(`Alice's Assets 3 balance: ${aliceBal3}`);
  console.log(`Alice's Assets 19 balance: ${aliceBal19}`);
  console.log(`Alice's ERC721 balance: ${aliceBal4}`);
}
