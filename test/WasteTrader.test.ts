import { assert, expect } from 'chai';
import { BigNumber, Contract, Signer } from 'ethers';
import { network, deployments, ethers, getNamedAccounts, upgrades } from 'hardhat';
import "hardhat-gas-reporter";
import { networkConfig, developmentChains } from '../helper-hardhat-config';
import { NFT, WasteTrader } from '../src/types';
import { advanceTime } from './utils';
import {exp} from "@prb/math";
import {describe} from "mocha";

/*!developmentChains.includes(network.name)
  ? describe.skip*/
describe('WasteTrader Unit Tests', function () {
  this.timeout(19000);
  let nft: Contract;
  let wasteTrader: Contract;
  let emilCoin: Contract;
  let deployer: Signer;
  let player: Signer;
  let gwsAccount: Signer;

  const INITIAL_BALANCE = 500;
  const WASTE_ID = 7;
  const ASTRO_CREDITS = 8;
  const LEGENDARY_BIOMOD = 14;
  const RARE_BIOMOD = 13;
  const UNCOMMON_BIOMOD = 12;
  const COMMON_BIOMOD = 11;
  const OUTLIER_BIOMOD = 10;
  const BLUEPRINT = 9;
  const NO_MINT = 0;
  const GWS_MIN_ORDER = 25
  const GWS_MAX_ORDER = 100
  let pixtSpeedupSplitBps = 500;
  let pixtSpeedupCost = 500;
  const EMIL_COINT_BALANCE = pixtSpeedupCost * 6;

  beforeEach(async () => {
    this.timeout(19000);
    const accounts = await ethers.getSigners();
    const nftName = networkConfig[network.name].nftName!;
    const nftSymbol = networkConfig[network.name].nftSymbol!;
    const numerator = networkConfig[network.name].numerator!;
    const denominator = networkConfig[network.name].denominator!;
    const gwsTime = networkConfig[network.name].gwsTime!;
    const speedupTime = networkConfig[network.name].speedupTime!;
    const gwsMinimumOrder = networkConfig[network.name].gwsMinimumOrder!;
    const gwsTax = networkConfig[network.name].gwsTax!;
    const gwsMax = networkConfig[network.name].gwsMax!;
    const gwsSpeedupCost = networkConfig[network.name].gwsSpeedupCost!;
    const nftArgs = [nftName, nftSymbol];

    deployer = accounts[0];
    player = accounts[1];
    gwsAccount = accounts[2];

    const nftContract = await ethers.getContractFactory('NFT');
    nft = await upgrades.deployProxy(nftContract, nftArgs);
    await nft.deployed();

    const wtArgs = [
      nft.address,
      await gwsAccount.getAddress(),
      numerator,
      denominator,
      gwsTime,
      speedupTime,
      gwsMinimumOrder,
      gwsTax,
      gwsMax,
      gwsSpeedupCost,
    ];

    const wasteTraderContract = await ethers.getContractFactory('WasteTrader');
    wasteTrader = await upgrades.deployProxy(wasteTraderContract, wtArgs);
    await wasteTrader.deployed();

    let erc20Factory = await ethers.getContractFactory('ERC20Mock');
    emilCoin = await erc20Factory.deploy('EmilCoin', 'MXC', await deployer.getAddress(), 0);
    await emilCoin.mint(await player.getAddress(), EMIL_COINT_BALANCE);

    let baseLevelFactory = await ethers.getContractFactory('BaseLevel');
    let baseLevel = await upgrades.deployProxy(baseLevelFactory);
    await wasteTrader.setBaseLevelAddress(baseLevel.address);

    let mockStreamFactory = await ethers.getContractFactory("MockSFStream");
    let mockSFStream = await mockStreamFactory.deploy();
    await mockSFStream.setStream(await player.getAddress(), 2)
    //await mockSFStream.setStream(await player.getAddress(), 1)


    await baseLevel.setSuperFlowAddress(mockSFStream.address)
    await baseLevel.setSuperFluidPerLevel(385802469135)
    await baseLevel.setOrderCapacity(wasteTrader.address, 0, 6, 4)
    console.log("level: ", Number(await baseLevel.getBaseLevel(await player.getAddress())))
    //console.log("level: ", Number(await baseLevel.getBaseLevel(await player.getAddress())))

    await nft.setTrustedContract(wasteTrader.address);
    await nft.setTrustedContract(deployer.getAddress());
    await nft.setURI(WASTE_ID, 'TokenURI');
    await nft.setURI(ASTRO_CREDITS, 'TokenURI');
    await nft.setURI(LEGENDARY_BIOMOD, 'TokenURI');
    await nft.setURI(RARE_BIOMOD, 'TokenURI');
    await nft.setURI(UNCOMMON_BIOMOD, 'TokenURI');
    await nft.setURI(COMMON_BIOMOD, 'TokenURI');
    await nft.setURI(OUTLIER_BIOMOD, 'TokenURI');
    await nft.setURI(BLUEPRINT, 'TokenURI');

    await nft.connect(deployer).trustedMint(await player.getAddress(), WASTE_ID, INITIAL_BALANCE);
  });
  describe('Place Orders', () => {
    let creationtime;
    let wasteAmount = 25;
    it('Place an order as expected', async () => {
      await expect(wasteTrader.connect(player).placeWasteOrder(wasteAmount)).to.emit(
          wasteTrader,
          'WasteOrderPlaced',
      ).withArgs(await player.getAddress(), wasteAmount)

      let data = await wasteTrader.getOrders(player.getAddress());
      creationtime = await (
          await ethers.provider.getBlock(await ethers.provider.getBlockNumber())
      ).timestamp;

      expect(data[0]['createdAt']).to.equal(creationtime);
      expect(await nft.balanceOf(player.getAddress(), WASTE_ID)).to.equal(
          INITIAL_BALANCE - wasteAmount,
      );
    });
    it('Place multiple orders as expected in separate transactions', async () => {

      await expect(wasteTrader.connect(player).placeWasteOrder(wasteAmount))
          .to.emit(wasteTrader, 'WasteOrderPlaced')
          .withArgs(await player.getAddress(), wasteAmount);
      await expect(wasteTrader.connect(player).placeWasteOrder(wasteAmount))
          .to.emit(wasteTrader, 'WasteOrderPlaced')
          .withArgs(await player.getAddress(), wasteAmount);

      expect(await nft.balanceOf(player.getAddress(), WASTE_ID)).to.equal(
          INITIAL_BALANCE - (wasteAmount * 2),
      );

      //expect(await nft.balanceOf(feeWallet.address, WASTE_ID)).to.equal(numOrders);
    });
    it('Place max order', async () => {
      await expect(wasteTrader.connect(player).placeWasteOrder(GWS_MAX_ORDER))
          .to.emit(wasteTrader, 'WasteOrderPlaced')
          .withArgs(await player.getAddress(), GWS_MAX_ORDER);

      expect(await nft.balanceOf(player.getAddress(), WASTE_ID)).to.equal(
          INITIAL_BALANCE - GWS_MAX_ORDER
      );

      //expect(await nft.balanceOf(feeWallet.address, WASTE_ID)).to.equal(numOrders);
    });
    it('Place order when wastetrader time is 0', async () => {
      await wasteTrader.setGwsTime(0);

      await expect(wasteTrader.connect(player).placeWasteOrder(GWS_MAX_ORDER))
          .to.emit(wasteTrader, 'MintedMissionControl')
          .withArgs(await player.getAddress(), ASTRO_CREDITS, GWS_MAX_ORDER); //(s_numerator * amount) / s_denominator should be 100??

      expect(await nft.balanceOf(player.getAddress(), WASTE_ID)).to.equal(
          INITIAL_BALANCE - GWS_MAX_ORDER,
      );

      expect(await nft.balanceOf(player.getAddress(), ASTRO_CREDITS)).to.equal(
          GWS_MAX_ORDER - 5, //tax 5, fix this shit
      );
      expect(await wasteTrader.getOrders(player.getAddress())).to.empty;

      //expect(await nft.balanceOf(feeWallet.address, WASTE_ID)).to.equal(numOrders);
    });
  });

  describe('Place Order reverts', () => {
    let overflowWasteAmount = GWS_MAX_ORDER + 1;
    let underflowWasteAmount = GWS_MIN_ORDER - 1;
    it('Reverts when placing too much waste', async () => {
      await expect(wasteTrader.connect(player).placeWasteOrder(overflowWasteAmount)).to.revertedWith(
          `WasteTrader__MaximumOrderAmountExceeded(${overflowWasteAmount})`,
      );
    });
    it('Reverts when placing too little waste', async () => {
      await expect(wasteTrader.connect(player).placeWasteOrder(underflowWasteAmount)).to.revertedWith(
          `WasteTrader__MinimumOrderNotMet(${GWS_MIN_ORDER})`,
      );
    });
    it('Reverts when placing too much waste on multiple transactions', async () => {
      await wasteTrader.connect(player).placeWasteOrder(GWS_MAX_ORDER * 0.8)
      await expect(wasteTrader.connect(player).placeWasteOrder(GWS_MAX_ORDER)).to.revertedWith(
          `WasteTrader__MaximumOrderAmountExceeded(${GWS_MAX_ORDER + (GWS_MAX_ORDER * 0.8)})`,
      );
    });
  });

  describe('Claim wasteorders', () => {
    let orderAmount = 25
    beforeEach(async () => {
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
    });
    it('Claim orders as expected', async () => {
      let gws_time = await wasteTrader.getGwsTime()
      await network.provider.send('evm_increaseTime', [Number(gws_time)]);
      await network.provider.send('evm_mine');
      for (let i = 0; i < 4; i++) {
        await expect(await wasteTrader.connect(player).claimOrder(i))
            .to.emit(wasteTrader, 'WasteOrderCompleted')
            .withArgs(await player.getAddress(), i);
      }
    });
  });
  describe('Claim Batch wasteorders', () => {
    let orderAmount = 25
    beforeEach(async () => {
      await nft.connect(deployer).trustedMint(await player.getAddress(), ASTRO_CREDITS, INITIAL_BALANCE);
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
    });
    it('Claim batch orders as expected', async () => {
      let gws_time = await wasteTrader.getGwsTime()
      await network.provider.send('evm_increaseTime', [Number(gws_time)]);
      await network.provider.send('evm_mine');

      await expect(await wasteTrader.connect(player).claimBatchOrder())
          .to.emit(wasteTrader, 'WasteOrderCompleted');
      let astroBalance = await nft.balanceOf(await player.getAddress(), ASTRO_CREDITS)
      expect(Number(astroBalance)).to.equal(4 * orderAmount + INITIAL_BALANCE - 4); //-4 in tax..
    });
    it('Claim one batch orders as expected', async () => {
      let gws_time = await wasteTrader.getGwsTime()
      await network.provider.send('evm_increaseTime', [Number(gws_time)]);
      await network.provider.send('evm_mine');
      for (let i = 0; i < 4; i++) {
        await expect(await wasteTrader.connect(player).claimOrder(i))
            .to.emit(wasteTrader, 'WasteOrderCompleted')
            .withArgs(await player.getAddress(), i);
      }
      let astroBalance = await nft.balanceOf(await player.getAddress(), ASTRO_CREDITS)
      expect(Number(astroBalance)).to.equal(4 * orderAmount + INITIAL_BALANCE - 4); //-4 in tax..
    });
  });
  describe('Revert claim Batch wasteorders', () => {
    let orderAmount = 25
    beforeEach(async () => {
      //await wasteTrader.connect(player).placeWasteOrder(orderAmount)
    });
    it('revert Claim batch orders as expected', async () => {
      await expect(wasteTrader.connect(deployer).claimBatchOrder()).to.revertedWith(
          `WasteTrader__NoOrdersPlaced("${await deployer.getAddress()}")`,
      );
    });
  })

  describe('Claim Reverts', () => {
    let orderAmount = 25;
    beforeEach(async () => {
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
    });
    it('Reverts if user has placed no orders', async () => {
      await expect(wasteTrader.connect(deployer).claimOrder(0)).to.revertedWith(
          `WasteTrader__NoOrdersPlaced("${await deployer.getAddress()}")`,
      );
    });
    it('Reverts if trying to claim order that has not been placed', async () => {
      let invalidOrderId = 1;
      await expect(wasteTrader.connect(player).claimOrder(invalidOrderId)).to.revertedWith(
          `WasteTrader__InvalidOrderId(${invalidOrderId})`,
      );
    });
    it('Revert when claiming same order twice', async () => {
      let gws_time = await wasteTrader.getGwsTime()
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await network.provider.send('evm_increaseTime', [Number(gws_time)]);
      await network.provider.send('evm_mine');
      let orderId = 0;
      await wasteTrader.connect(player).claimOrder(orderId)
      await expect(wasteTrader.connect(player).claimOrder(orderId)).to.revertedWith(
          `WasteTrader__InvalidOrderId(${orderId})`,
      );
    });
    it('Reverts if trying to claim before enough time has passed', async () => {
      let orderId = 0;
      await expect(wasteTrader.connect(player).claimOrder(orderId)).to.revertedWith(
          `WasteTrader__OrderNotYetCompleted("${await player.getAddress()}", ${orderId})`,
      );
    });
    it('Revert when no orders left', async () => {
      let orderId = 0;
      const playerAddress = await player.getAddress()
      const gws_time = await wasteTrader.getGwsTime()
      await network.provider.send('evm_increaseTime', [Number(gws_time)]);
      await network.provider.send('evm_mine');
      await wasteTrader.connect(player).claimOrder(orderId)

      await expect(wasteTrader.connect(player).claimOrder(orderId)).to.revertedWith(
          `WasteTrader__NoOrdersPlaced("${playerAddress}")`,
      );
    });
    it('Reverts on reported bug #1 ', async () => {
      /*
      Reported bug: have completed order, place new order, claim first order then second becomes instantly claimable
      Had to increase order id a bunch of times
       */
      const gws_time = await wasteTrader.getGwsTime()
      await network.provider.send('evm_increaseTime', [Number(gws_time)]);
      await network.provider.send('evm_mine');
      for(let i = 0; i < 2; i++){
        await wasteTrader.connect(player).placeWasteOrder(25)
      }

      await network.provider.send('evm_increaseTime', [Number(gws_time)]);
      await network.provider.send('evm_mine');
      await wasteTrader.connect(player).claimBatchOrder()

      await wasteTrader.connect(player).placeWasteOrder(50)
      await network.provider.send('evm_increaseTime', [Number(gws_time)]);
      await network.provider.send('evm_mine');
      await wasteTrader.connect(player).placeWasteOrder(50)
      await wasteTrader.connect(player).claimOrder(3)
      await expect(wasteTrader.connect(player).claimOrder(3)).to.reverted

    });

  });
  describe('Speed up order', () => {
    let orderAmount = 50;
    let speedUpTime;
    let speedUpCost;
    let defaultOrderTime;
    beforeEach(async () => {
      await nft.connect(deployer).trustedMint(await player.getAddress(), ASTRO_CREDITS, INITIAL_BALANCE);
      speedUpTime = await wasteTrader.getSpeedupTime()
      speedUpCost = await wasteTrader.getGwsSpeedupCost()
      defaultOrderTime = await wasteTrader.getGwsTime()
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
    });
    it('Speed up as expected', async () => {
      let orderId = 0;
      let numSpeedups = 2;
      let speedUpTime = await wasteTrader.getSpeedupTime()
      let speedUpCost = await wasteTrader.getGwsSpeedupCost()
      let defaultOrderTime = await wasteTrader.getGwsTime()

      await expect(await wasteTrader.connect(player).speedUpOrder(numSpeedups, orderId))
          .to.emit(wasteTrader, 'WasteOrderSpedUp')
          .withArgs(await player.getAddress(), orderAmount, orderId);


      let data = await wasteTrader.getOrders(player.getAddress());
      let astroBalance = await nft.balanceOf(player.getAddress(), ASTRO_CREDITS)
      expect(data[0]['speedUpDeductedAmount']).to.equal(Number(speedUpTime) * numSpeedups);
      expect(data[0]['totalCompletionTime']).to.equal(Number(defaultOrderTime) - (numSpeedups * Number(speedUpTime)));

      expect(data[1]['speedUpDeductedAmount']).to.equal(0); // check that only one order is speed up and not all.
      expect(data[1]['totalCompletionTime']).to.equal(Number(defaultOrderTime));
      expect(Number(astroBalance)).to.equal(INITIAL_BALANCE - (numSpeedups * Number(speedUpCost)));

    });
    it('Speed up order beyond total time', async () => {
      let orderId = 0;
      let numSpeedups = 7;


      await expect(await wasteTrader.connect(player).speedUpOrder(numSpeedups, orderId))
          .to.emit(wasteTrader, 'WasteOrderSpedUp')
          .withArgs(await player.getAddress(), orderAmount, orderId);


      let data = await wasteTrader.getOrders(player.getAddress());
      let astroBalance = await nft.balanceOf(player.getAddress(), ASTRO_CREDITS)

      expect(data[0]['speedUpDeductedAmount']).to.equal(Number(speedUpTime) * numSpeedups);
      expect(data[0]['totalCompletionTime']).to.equal(0);
      expect(Number(astroBalance)).to.equal(INITIAL_BALANCE - (numSpeedups * Number(speedUpCost)));
    });
    it('Speed up order multiple times', async () => {
      let orderId = 0;
      let numSpeedups = 6; // 6 speed-ups to complete order.
      const playerAddress = await player.getAddress()
      for (let i = 0; i < numSpeedups; i++) {
        await expect(wasteTrader.connect(player).speedUpOrder(1, orderId))
            .to.emit(wasteTrader, 'WasteOrderSpedUp')
            .withArgs(playerAddress, orderAmount, orderId);
      }

      let data = await wasteTrader.getOrders(player.getAddress());
      let astroBalance = await nft.balanceOf(player.getAddress(), ASTRO_CREDITS)

      expect(data[0]['speedUpDeductedAmount']).to.equal(Number(speedUpTime) * numSpeedups);
      expect(data[0]['totalCompletionTime']).to.equal(0);
      expect(Number(astroBalance)).to.equal(INITIAL_BALANCE - (numSpeedups * Number(speedUpCost)));
    });
  })
  describe('Revert Speed up order', () => {
    let orderAmount = 40;
    let orderId = 0;
    let speedUpTime;
    let speedUpCost;
    let defaultOrderTime;
    beforeEach(async () => {
      await nft.connect(deployer).trustedMint(await player.getAddress(), ASTRO_CREDITS, INITIAL_BALANCE);
      speedUpTime = await wasteTrader.getSpeedupTime()
      speedUpCost = await wasteTrader.getGwsSpeedupCost()
      defaultOrderTime = await wasteTrader.getGwsTime()
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
    });
    it('Revert speed up order', async () => {
      let numSpeedups = 6;
      await expect(await wasteTrader.connect(player).speedUpOrder(numSpeedups, orderId))
          .to.emit(wasteTrader, 'WasteOrderSpedUp')
          .withArgs(await player.getAddress(), orderAmount, orderId);
      await expect(wasteTrader.connect(player).speedUpOrder(1,orderId)).to.revertedWith(
          `WasteTrader__speedUpNotAvailable(${orderId})`,
      );
    });

    it('Revert speed up order when cost > claim', async () => {
      let numSpeedups = 10;
      await expect(wasteTrader.connect(player).speedUpOrder(numSpeedups,orderId)).to.revertedWith(
          `WasteTrader__SpeedUpUnitsExceedClaimAmount(${orderAmount})`,
      );
    });
  });

  describe('Speed up order with erc20', () => {
    let orderAmount = 50;
    let speedUpTime;
    let speedUpCost;
    let numSpeedups = 1;
    let orderId = 0;
    let defaultOrderTime;
    let playerAddress;
    beforeEach(async () => {
      await nft.connect(deployer).trustedMint(await player.getAddress(), ASTRO_CREDITS, INITIAL_BALANCE);
      speedUpTime = await wasteTrader.getSpeedupTime()
      speedUpCost = await wasteTrader.getGwsSpeedupCost()
      defaultOrderTime = await wasteTrader.getGwsTime()
      playerAddress = await player.getAddress()
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
      await wasteTrader.setIXTSpeedUpParams(
          emilCoin.address,
          emilCoin.address, //isn't used yet.
          pixtSpeedupSplitBps,
          pixtSpeedupCost
      )
    });
    it('Emits event', async () => {
      await emilCoin.connect(player).approve(wasteTrader.address, numSpeedups * pixtSpeedupCost);
      await expect(wasteTrader.connect(player).IXTSpeedUpOrder(numSpeedups, orderId))
          .to.emit(wasteTrader, "WasteOrderSpedUp").withArgs(playerAddress, orderAmount, orderId);

    });
    it('Draws coins as expected', async () => {
      let priorBalance = await emilCoin.balanceOf(playerAddress)
      await emilCoin.connect(player).approve(wasteTrader.address, numSpeedups * pixtSpeedupCost);
      await expect(wasteTrader.connect(player).IXTSpeedUpOrder(numSpeedups, orderId)).to.not.reverted
      expect(priorBalance - (await emilCoin.balanceOf(playerAddress))).to.equal(numSpeedups * pixtSpeedupCost)
    });
    it('Deposits expected amount into contract', async () => {
      let priorBalance = await emilCoin.balanceOf(wasteTrader.address)
      console.log("wastrader bal", priorBalance)
      await emilCoin.connect(player).approve(wasteTrader.address, numSpeedups * pixtSpeedupCost);
      await expect(wasteTrader.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.not.reverted
      console.log("wastrader bal", await emilCoin.balanceOf(wasteTrader.address))
      expect((await emilCoin.balanceOf(wasteTrader.address))).to.equal(BigNumber.from(numSpeedups * pixtSpeedupCost * (10000 - pixtSpeedupSplitBps)).div(BigNumber.from(10000)))
    });
  });
  describe('Revert Speed up order with erc20', () => {
    let orderAmount = 50;
    let speedUpTime = 100
    let numSpeedups = 1;
    let orderId = 0;
    let defaultOrderTime;
    let playerAddress;
    beforeEach(async () => {
      await wasteTrader.connect(player).placeWasteOrder(orderAmount)
    });
    it('Revert when IXTParams is not set', async () => {
      await expect(wasteTrader.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith("IXT address not set")
    });
    it('Revert when not enough erc20 in player wallet', async () => {
      await wasteTrader.setIXTSpeedUpParams(
          emilCoin.address,
          emilCoin.address,
          pixtSpeedupSplitBps,
          pixtSpeedupCost
      )
      await emilCoin.connect(player).transfer(deployer.getAddress(), EMIL_COINT_BALANCE)
      //await expect(wasteTrader.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith("Transfer of funds failed") // should be this...
      await expect(wasteTrader.connect(player).IXTSpeedUpOrder(numSpeedups, 0)).to.revertedWith("ERC20: transfer amount exceeds balance")
    });
  });

      it('Is initialised properly', async function () {
        const gwsWalletAddress = await wasteTrader.connect(player).getFeeWallet();
        const bpsFee = await wasteTrader.connect(player).getGwsTax();

        assert.equal(gwsWalletAddress, await gwsAccount.getAddress());
        expect(BigNumber.from(500)).to.equal(bpsFee);
      });

      it('Checks mint validitiy', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        const payloadHash = ethers.utils.solidityKeccak256(
          ['address', 'uint256', 'string', 'uint256'],
          [playerAddress, 25, 'mission control', 1],
        );

        const signature = await deployer.signMessage(ethers.utils.arrayify(payloadHash));
        const sig = ethers.utils.splitSignature(signature);
        await wasteTrader.setSigner(deployerAddress);

        // Act
        const isValidMinter = await wasteTrader.checkMintValidity(
          payloadHash,
          signature,
          playerAddress,
          25,
          1,
        );

        // Assert
        assert.equal(true, isValidMinter);
      });

      it('Gets the exchange rate', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        // Act
        const rate = await wasteTrader.connect(player).getExchangeRate();

        // Assert
        expect(BigNumber.from(1)).to.equal(rate[0]);
        expect(BigNumber.from(1)).to.equal(rate[1]);
      });

      it('reverts when too many tokens placed for an order', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 500);

        // Assert
        await expect(wasteTrader.connect(player).placeWasteOrder(30)).to.emit(
          wasteTrader,
          'WasteOrderPlaced',
        );
        let orders = await wasteTrader.connect(player).getOrders(playerAddress);
        await network.provider.send('evm_mine', []);
        await expect(wasteTrader.connect(player).placeWasteOrder(75)).to.be.revertedWith(
          'WasteTrader__MaximumOrderAmountExceeded',
        );
      });

      it('reverts when too many tokens placed for an order not yet completed', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 500);

        // Assert
        await expect(wasteTrader.connect(player).placeWasteOrder(30)).to.emit(
          wasteTrader,
          'WasteOrderPlaced',
        );
        await network.provider.send('evm_mine', []);
        await expect(wasteTrader.connect(player).placeWasteOrder(70)).to.emit(
          wasteTrader,
          'WasteOrderPlaced',
        );
        let orders = await wasteTrader.connect(player).getOrders(playerAddress);
        await network.provider.send('evm_mine', []);
        await network.provider.send('evm_increaseTime', [gwsTime.toNumber() - 10]);
        await network.provider.send('evm_mine', []);
        await expect(wasteTrader.connect(player).placeWasteOrder(100)).to.be.revertedWith(
          'WasteTrader__MaximumOrderAmountExceeded',
        );
      });

      it('Places multiple orders', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 500);

        // Assert
        await expect(wasteTrader.connect(player).placeWasteOrder(30)).to.emit(
          wasteTrader,
          'WasteOrderPlaced',
        );
        await network.provider.send('evm_mine', []);

        await expect(wasteTrader.connect(player).placeWasteOrder(30)).to.emit(
          wasteTrader,
          'WasteOrderPlaced',
        );
        await network.provider.send('evm_mine', []);

        await expect(wasteTrader.connect(player).placeWasteOrder(40)).to.emit(
          wasteTrader,
          'WasteOrderPlaced',
        );
        let orders = await wasteTrader.connect(player).getOrders(playerAddress);
      });

      it('places new orders after unclaimed completed orders', async function () {
        // Arrange
        this.timeout(19000);
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 500);
        await nft.connect(player).trustedMint(playerAddress, 8, 100);

        // Act
        wasteTrader.connect(player).placeWasteOrder(90);

        await network.provider.send('evm_mine', []);

        await network.provider.send('evm_increaseTime', [200]);
        await network.provider.send('evm_mine', []);

        await wasteTrader.connect(player).speedUpOrder(6,0);
        let orders = await wasteTrader.connect(player).getOrders(playerAddress);

        await network.provider.send('evm_mine', []);

        await network.provider.send('evm_increaseTime', [200]);
        await network.provider.send('evm_mine', []);

        await wasteTrader.connect(player).placeWasteOrder(60);
        orders = await wasteTrader.connect(player).getOrders(playerAddress);

        await network.provider.send('evm_increaseTime', [200]);
        await network.provider.send('evm_mine', []);

        wasteTrader.connect(player).placeWasteOrder(40);
        orders = await wasteTrader.connect(player).getOrders(playerAddress);


        await network.provider.send('evm_increaseTime', [200]);
        await network.provider.send('evm_mine', []);
        // Assert
        assert.equal(orders.length, 3);
      });

      it('reverts when new orders after unclaimed completed orders exceed max orders', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 500);
        await nft.connect(player).trustedMint(playerAddress, 8, 100);

        // Act

        wasteTrader.connect(player).placeWasteOrder(90);

        await network.provider.send('evm_mine', []);

        await network.provider.send('evm_increaseTime', [200]);
        await network.provider.send('evm_mine', []);

        await wasteTrader.connect(player).speedUpOrder(6,0);
        let orders = await wasteTrader.connect(player).getOrders(playerAddress);

        await network.provider.send('evm_mine', []);

        await network.provider.send('evm_increaseTime', [200]);
        await network.provider.send('evm_mine', []);

        await wasteTrader.connect(player).placeWasteOrder(60);
        orders = await wasteTrader.connect(player).getOrders(playerAddress);

        await network.provider.send('evm_increaseTime', [200]);
        await network.provider.send('evm_mine', []);

        wasteTrader.connect(player).placeWasteOrder(40);
        orders = await wasteTrader.connect(player).getOrders(playerAddress);


        await network.provider.send('evm_increaseTime', [200]);
        await network.provider.send('evm_mine', []);

        // Assert
        await expect(wasteTrader.connect(player).placeWasteOrder(50)).to.be.revertedWith(
          'WasteTrader__MaximumOrderAmountExceeded',
        );
      });

      it('reverts when no orders placed upon speeding up the exchange', async function () {
        // Assert
        await expect(wasteTrader.connect(player).speedUpOrder(1,0)).to.be.revertedWith(
          'WasteTrader__NoOrdersPlaced',
        );
      });

      it('reverts when speeding up waste orders excessively', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 500);
        await nft.connect(player).trustedMint(playerAddress, 8, 100);

        // Act
        await wasteTrader.connect(player).placeWasteOrder(100);
        await wasteTrader.connect(player).speedUpOrder(2,0);
        await wasteTrader.connect(player).speedUpOrder(2,0);

        // Assert
        await expect(wasteTrader.connect(player).speedUpOrder(17,0)).to.be.revertedWith(
          'WasteTrader__SpeedUpUnitsExceedClaimAmount',
        );
      });

      it('reverts when order is finished', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 500);
        await nft.connect(player).trustedMint(playerAddress, 8, 100);

        // Act
        await wasteTrader.connect(player).placeWasteOrder(100);
        await wasteTrader.connect(player).speedUpOrder(10,0);
        // Assert
        await expect(wasteTrader.connect(player).speedUpOrder(1,0)).to.be.revertedWith(
          'WasteTrader__speedUpNotAvailable',
        );
      });

      it('speeds up waste orders', async function () {
        // Arrange
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();
        const feeWallet = await gwsAccount.getAddress();
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 500);
        await nft.connect(player).trustedMint(playerAddress, 8, 100);


        // Act
        await wasteTrader.connect(player).placeWasteOrder(30);
        await wasteTrader.connect(player).placeWasteOrder(70);

        await network.provider.send('evm_mine', []);

        await network.provider.send('evm_increaseTime', [10800]);
        await network.provider.send('evm_mine', []);

        // Assert
        await expect(wasteTrader.connect(player).speedUpOrder(1,0)).to.emit(
          wasteTrader,
          'WasteOrderSpedUp',
        );

        let orders = await wasteTrader.connect(player).getOrders(playerAddress);


        await expect(wasteTrader.connect(player).speedUpOrder(2,0)).to.emit(
          wasteTrader,
          'WasteOrderSpedUp',
        );

        orders = await wasteTrader.connect(player).getOrders(playerAddress);
      });

      it('Trades Waste for Astro Credits immediately', async function () {
        // Arrange
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const feeWallet = await gwsAccount.getAddress();
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();

        await wasteTrader.setGwsTime(0);
        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 25);
        const startingPlayerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);

        // Act
        await wasteTrader.connect(player).placeWasteOrder(25);

        // Assert
        const endingPlayerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);
        const playerACBalance = await nft.connect(player).balanceOf(playerAddress, 8);
        const feeWalletACBalance = await nft.connect(player).balanceOf(feeWallet, 8);

        assert.equal(25+INITIAL_BALANCE, startingPlayerWasteBalance.toNumber());
        assert.equal(0+INITIAL_BALANCE, endingPlayerWasteBalance.toNumber());
        assert.equal(24, playerACBalance.toNumber());
        assert.equal(1, feeWalletACBalance.toNumber());
      });

      it('Claims astro credits', async function () {
        // Arrange
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const feeWallet = await gwsAccount.getAddress();
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 200);
        const startingPlayerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);

        await wasteTrader.connect(player).placeWasteOrder(30);
        await wasteTrader.connect(player).placeWasteOrder(70);

        await network.provider.send('evm_mine', []);
        await network.provider.send('evm_increaseTime', [gwsTime.toNumber() + 1]);

        // Assert
        await expect(wasteTrader.connect(player).claimOrder(0)).to.emit(
          wasteTrader,
          'WasteOrderCompleted',
        );
        const endingPlayerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);
        const playerACBalance = await nft.connect(player).balanceOf(playerAddress, 8);
        const feeWalletACBalance = await nft.connect(player).balanceOf(feeWallet, 8);
        assert.equal(100+ INITIAL_BALANCE, endingPlayerWasteBalance.toNumber());
        assert.equal(29, playerACBalance.toNumber());
        assert.equal(1, feeWalletACBalance.toNumber());
      });

      it('Claims astro credits minus sped up amount', async function () {
        // Arrange
        const uri = 'ipfs://bafkreicpnuy7vzvgsjhkgz2igt74hlllnhovc3m5ey6raaljl5g7skmsua';
        const feeWallet = await gwsAccount.getAddress();
        const deployerAddress = await deployer.getAddress();
        const playerAddress = await player.getAddress();
        const gwsTime = await wasteTrader.connect(player).getGwsTime();

        await nft.setURI(7, uri);
        await nft.setURI(8, uri);
        await nft.setTrustedContract(playerAddress);
        await nft.connect(player).trustedMint(playerAddress, 7, 100);
        await nft.connect(player).trustedMint(playerAddress, 8, 5);
        const startingPlayerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);

        await wasteTrader.connect(player).placeWasteOrder(30);
        await wasteTrader.connect(player).placeWasteOrder(70);

        await network.provider.send('evm_increaseTime', [7200]);
        await network.provider.send('evm_mine', []);
        let orders = await wasteTrader.connect(player).getOrders(playerAddress);

        await wasteTrader.connect(player).speedUpOrder(1,0);
        await network.provider.send('evm_mine', []);

        await network.provider.send('evm_increaseTime', [gwsTime.toNumber() + 1]);
        await network.provider.send('evm_mine', []);

        // Assert
        await expect(wasteTrader.connect(player).claimOrder(1)).to.emit(
          wasteTrader,
          'WasteOrderCompleted',
        );
        const endingPlayerWasteBalance = await nft.connect(player).balanceOf(playerAddress, 7);
        const playerACBalance = await nft.connect(player).balanceOf(playerAddress, 8);
        const feeWalletACBalance = await nft.connect(player).balanceOf(feeWallet, 8);
        assert.equal(100+INITIAL_BALANCE, startingPlayerWasteBalance.toNumber());
        assert.equal(0+INITIAL_BALANCE, endingPlayerWasteBalance.toNumber());
        assert.equal(67, playerACBalance.toNumber());
        assert.equal(3, feeWalletACBalance.toNumber());
      });
    });
