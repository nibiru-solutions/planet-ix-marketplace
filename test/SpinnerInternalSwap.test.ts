import {network, ethers, upgrades} from 'hardhat';
import {SignerWithAddress} from '@nomiclabs/hardhat-ethers/signers';
import {expect} from 'chai';
import {BigNumber, constants, Contract} from 'ethers';
import {exp} from "@prb/math";
import {max} from "hardhat/internal/util/bigint";
import {describe} from "mocha";

describe('Spinner', function () {
    this.timeout(120000);
    let emilCoin: Contract;
    let usdt: Contract;
    let swapManager: Contract;
    let oracle: Contract;
    let spinnerInternalSwapTreasury: Contract;
    let mockFluencrSale: Contract;

    let deployer: SignerWithAddress;
    let player: SignerWithAddress;
    let feeWallet: SignerWithAddress;
    let alice: SignerWithAddress;
    let USDT_BALANCE = 10_000_000;

    beforeEach(async () => {
        [deployer, player, feeWallet, alice] = await ethers.getSigners();
        let erc20Factory = await ethers.getContractFactory('ERC20Mock');
        emilCoin = await erc20Factory.deploy('EmilCoin', 'EC');

        let erc20Factory2 = await ethers.getContractFactory('ERC20Mock');
        usdt = await erc20Factory2.deploy('usdt', 'usdt');
        await usdt.mint(player.address, USDT_BALANCE);

        let swapManagerFactory = await ethers.getContractFactory('SwapManagerMock');
        swapManager = await swapManagerFactory.deploy();
        await emilCoin.mint(swapManager.address, USDT_BALANCE*100);

        let spinnerTreasuryFactory = await ethers.getContractFactory('SpinnerInternalSwapTreasury');
        spinnerInternalSwapTreasury = await upgrades.deployProxy(spinnerTreasuryFactory, [emilCoin.address]);

        let mockFluencrSaleFactory = await ethers.getContractFactory('MockFluencrSale');
        mockFluencrSale = await upgrades.deployProxy(mockFluencrSaleFactory, [spinnerInternalSwapTreasury.address]);


        await spinnerInternalSwapTreasury.setFluencr(mockFluencrSale.address);
        await spinnerInternalSwapTreasury.setSwapManager(swapManager.address);

    });
    async function automationTreasury(){
        let checkUpkeep = await spinnerInternalSwapTreasury.checkUpkeep(1);
        if(checkUpkeep.upkeepNeeded){
            await spinnerInternalSwapTreasury.performUpkeep(1);
            return true;
        }
    }

    describe('Place swapOrder', () => {
        it('expect correct queue len after fluencr sell', async () => {
            await usdt.connect(deployer).mint(mockFluencrSale.address, USDT_BALANCE*10)
            expect(await usdt.balanceOf(mockFluencrSale.address)).to.equal(USDT_BALANCE*10)
            await mockFluencrSale.callSwapOrder(alice.address, USDT_BALANCE, usdt.address)
            await mockFluencrSale.callSwapOrder(player.address, USDT_BALANCE*2, usdt.address)
            expect(await usdt.balanceOf(mockFluencrSale.address)).to.equal(USDT_BALANCE*10-(USDT_BALANCE*3))
            expect(await usdt.balanceOf(spinnerInternalSwapTreasury.address)).to.equal(USDT_BALANCE*3)
            await automationTreasury()
            expect(await emilCoin.balanceOf(alice.address)).to.equal(10000000)
            expect(await emilCoin.balanceOf(player.address)).to.equal(20000000)
        });
        it('expect correct values after n runs', async () => {
            const n = 50
            const maxRandNum = 500
            let players = []
            let randNums = []
            await spinnerInternalSwapTreasury.connect(deployer).setBpsSplits(9500)

            for(let i = 0; i < n; i++){
                let randNum = Math.floor(Math.random() * maxRandNum) + 1;
                let tempPlayer = await ethers.Wallet.createRandom();
                tempPlayer = tempPlayer.connect(ethers.provider)
                players.push(tempPlayer)
                randNums.push(randNum)
                await usdt.connect(deployer).mint(mockFluencrSale.address, randNum)
                await mockFluencrSale.callSwapOrder(tempPlayer.address, randNum, usdt.address)
                if(randNum > maxRandNum * 0.80){
                    await automationTreasury()
                }
            }
            let notConvertedCurrency = 0
            for(let p = 0; p < n; p++){
                let ixtBalance = Number(await emilCoin.balanceOf(players[p].address))
                if (ixtBalance == 0){
                    notConvertedCurrency += randNums[p]
                }
                else{
                    expect(ixtBalance).to.approximately(randNums[p] * 0.95, ixtBalance * 0.1) // spinner gives 95% to user and 10% deviation set because of low number solidity rounding.
                    notConvertedCurrency += randNums[p] * 0.05
                }
            }
            expect(Number(await usdt.balanceOf(spinnerInternalSwapTreasury.address))).to.approximately(notConvertedCurrency, notConvertedCurrency * 0.06)
        });
        it('expect withdraw to work as expected', async () => {
            let withdrawal = 450
            let mintAmount = 500
            await emilCoin.mint(spinnerInternalSwapTreasury.address, mintAmount)
            await expect(spinnerInternalSwapTreasury.connect(player).withdraw(emilCoin.address, 450)).to.revertedWith('Sender not admin')
            expect(Number(await emilCoin.balanceOf(player.address))).to.equal(0)
            await spinnerInternalSwapTreasury.connect(deployer).setAdmin(player.address, true)
            await spinnerInternalSwapTreasury.connect(player).withdraw(emilCoin.address, withdrawal)
            expect(Number(await emilCoin.balanceOf(player.address))).to.equal(withdrawal)
            expect(Number(await emilCoin.balanceOf(spinnerInternalSwapTreasury.address))).to.equal(mintAmount- withdrawal)
        });
    });
});