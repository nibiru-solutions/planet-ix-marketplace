import { AssetIds } from './test/utils';

let treasureAssets = [
  AssetIds.Blueprint,
  AssetIds.BioModOutlier,
  AssetIds.BioModCommon,
  AssetIds.BioModUncommon,
  AssetIds.BioModRare,
  AssetIds.BioModLegendary,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
  AssetIds.AstroCredit,
];

let treasureAssetsWeight = [
  10, 200, 150, 100, 50, 20, 15, 18, 20, 23, 25, 27, 29, 30, 31, 32, 32, 31, 30, 29, 27, 25, 23, 20,
  18, 15,
];

let treasureAssetsAmounts = [
  1, 1, 1, 1, 1, 1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
];
let crateContentCount = [2, 3, 4, 5];
let crateContentWeights = [20, 50, 20, 10];

export interface networkConfigItem {
  nftName?: string;
  nftSymbol?: string;
  ggName?: string;
  ggSymbol?: string;
  ggBurnProbabilities?: Array<number>;
  assetManagerImpl?: string;
  gwsWalletAddress?: string;
  numerator?: number;
  denominator?: number;
  gwsTime?: number;
  speedupTime?: number;
  gwsMinimumOrder?: number;
  gwsTax?: number;
  gwsMax?: number;
  gwsSpeedupCost?: number;
  ethUsdPriceFeedAddress?: string;
  usdcAddress?: string;
  wethAddress?: string;
  swapRouter?: string;
  pixAssets?: string;
  treasureAssets?: number[];
  treasureAssetsAmounts?: number[];
  treasureAssetsWeight?: number[];
  crateContentCount?: number[];
  crateContentWeights?: number[];
  blockConfirmations?: number;
}

export interface networkConfigInfo {
  [key: string]: networkConfigItem;
}

export const networkConfig: networkConfigInfo = {
  localhost: {
    nftName: 'PlanetIX - Genesis Corporations',
    nftSymbol: 'PIX-GC',
    ggName: 'Planet IX - Gravity Grade',
    ggSymbol: 'PIX-GG',
    ggBurnProbabilities: [2000, 2000, 2000, 2000, 2000],
    assetManagerImpl: '',
    gwsWalletAddress: '',
    numerator: 1,
    denominator: 1,
    gwsTime: 21600,
    speedupTime: 3600,
    gwsMinimumOrder: 25,
    gwsTax: 500,
    gwsMax: 100,
    gwsSpeedupCost: 5,
    ethUsdPriceFeedAddress: '0x0715A7794a1dc8e42615F059dD6e406A6594651A',
    usdcAddress: '0x2058A9D7613eEE744279e3856Ef0eAda5FCbaA7e',
    wethAddress: '0xA6FA4fB5f76172d178d61B04b0ecd319C5d1C0aa',
    swapRouter: '0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff',
    pixAssets: '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    treasureAssets: treasureAssets,
    treasureAssetsAmounts: treasureAssetsAmounts,
    treasureAssetsWeight: treasureAssetsWeight,
    crateContentCount: crateContentCount,
    crateContentWeights: crateContentWeights,
    blockConfirmations: 1,
  },
  hardhat: {
    nftName: 'PlanetIX - Genesis Corporations',
    nftSymbol: 'PIX-GC',
    ggName: 'Planet IX - Gravity Grade',
    ggSymbol: 'PIX-GG',
    ggBurnProbabilities: [2000, 2000, 2000, 2000, 2000],
    assetManagerImpl: '',
    gwsWalletAddress: '',
    numerator: 1,
    denominator: 1,
    gwsTime: 21600,
    speedupTime: 3600,
    gwsMinimumOrder: 25,
    gwsTax: 500,
    gwsMax: 100,
    gwsSpeedupCost: 5,
    ethUsdPriceFeedAddress: '0x0715A7794a1dc8e42615F059dD6e406A6594651A',
    usdcAddress: '0x2058A9D7613eEE744279e3856Ef0eAda5FCbaA7e',
    wethAddress: '0xA6FA4fB5f76172d178d61B04b0ecd319C5d1C0aa',
    swapRouter: '0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff',
    pixAssets: '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    treasureAssets: treasureAssets,
    treasureAssetsAmounts: treasureAssetsAmounts,
    treasureAssetsWeight: treasureAssetsWeight,
    crateContentCount: crateContentCount,
    crateContentWeights: crateContentWeights,
    blockConfirmations: 1,
  },
  testnet: {
    nftName: 'PlanetIX - Genesis Corporations',
    nftSymbol: 'PIX-GC',
    ggName: 'Planet IX - Gravity Grade',
    ggSymbol: 'PIX-GG',
    ggBurnProbabilities: [2000, 2000, 2000, 2000, 2000],
    assetManagerImpl: '0x41F84473804b4245559Cb97cC4b5173BDDA2Ab96',
    gwsWalletAddress: '0x9E3dc3b9Eb903E085b84ab53eb72951D3917100B',
    numerator: 1,
    denominator: 1,
    gwsTime: 21600,
    speedupTime: 3600,
    gwsMinimumOrder: 25,
    gwsTax: 500,
    gwsMax: 100,
    gwsSpeedupCost: 5,
    ethUsdPriceFeedAddress: '0x0715A7794a1dc8e42615F059dD6e406A6594651A',
    usdcAddress: '0x2058A9D7613eEE744279e3856Ef0eAda5FCbaA7e',
    wethAddress: '0xA6FA4fB5f76172d178d61B04b0ecd319C5d1C0aa',
    swapRouter: '0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff',
    pixAssets: '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    treasureAssets: treasureAssets,
    treasureAssetsAmounts: treasureAssetsAmounts,
    treasureAssetsWeight: treasureAssetsWeight,
    crateContentCount: crateContentCount,
    crateContentWeights: crateContentWeights,
    blockConfirmations: 1,
  },
  testneteth: {
    nftName: 'PlanetIX - Genesis Corporations',
    nftSymbol: 'PIX-GC',
    ggName: 'Planet IX - Gravity Grade',
    ggSymbol: 'PIX-GG',
    ggBurnProbabilities: [2000, 2000, 2000, 2000, 2000],
    assetManagerImpl: '0x41F84473804b4245559Cb97cC4b5173BDDA2Ab96',
    gwsWalletAddress: '0x9E3dc3b9Eb903E085b84ab53eb72951D3917100B',
    numerator: 1,
    denominator: 1,
    gwsTime: 21600,
    speedupTime: 3600,
    gwsMinimumOrder: 25,
    gwsTax: 500,
    gwsMax: 100,
    gwsSpeedupCost: 5,
    ethUsdPriceFeedAddress: '0x0715A7794a1dc8e42615F059dD6e406A6594651A',
    usdcAddress: '0x2058A9D7613eEE744279e3856Ef0eAda5FCbaA7e',
    wethAddress: '0xA6FA4fB5f76172d178d61B04b0ecd319C5d1C0aa',
    swapRouter: '0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff',
    pixAssets: '0x1317aD91c39fDF49d07B3687c4D42c91E22dcCDc',
    treasureAssets: treasureAssets,
    treasureAssetsAmounts: treasureAssetsAmounts,
    treasureAssetsWeight: treasureAssetsWeight,
    crateContentCount: crateContentCount,
    crateContentWeights: crateContentWeights,
    blockConfirmations: 1,
  },
  mainnet: {
    nftName: 'PlanetIX - Genesis Corporations',
    nftSymbol: 'PIX-GC',
    ggName: 'Planet IX - Gravity Grade',
    ggSymbol: 'PIX-GG',
    ggBurnProbabilities: [450, 4505, 4505, 450, 90],
    assetManagerImpl: '',
    gwsWalletAddress: '',
    numerator: 1,
    denominator: 1,
    gwsTime: 21600,
    speedupTime: 3600,
    gwsMinimumOrder: 25,
    gwsTax: 500,
    gwsMax: 100,
    gwsSpeedupCost: 5,
    ethUsdPriceFeedAddress: '',
    usdcAddress: '',
    wethAddress: '',
    swapRouter: '',
    pixAssets: '',
    treasureAssets: treasureAssets,
    treasureAssetsAmounts: treasureAssetsAmounts,
    treasureAssetsWeight: treasureAssetsWeight,
    crateContentCount: crateContentCount,
    crateContentWeights: crateContentWeights,
    blockConfirmations: 1,
  },
};

export const developmentChains = ['hardhat', 'localhost'];
