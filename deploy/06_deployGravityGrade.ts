import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { networkConfig, developmentChains } from '../helper-hardhat-config';
import { deployments } from 'hardhat';

const deployGravityGrade: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { ethers, upgrades } = require('hardhat');
  const { network } = hre;
  const { deploy, log } = hre.deployments;

  const ggName = networkConfig[network.name].ggName!;
  const ggSymbol = networkConfig[network.name].ggSymbol!;

  log('----------------------------------------------------');
  log('Deploying GravityGrade and waiting for confirmations...');
  const args = [ggName, ggSymbol];

  const GravityGrade = await ethers.getContractFactory('GravityGrade');
  const gravityGrade = await upgrades.deployProxy(GravityGrade, args);
  await gravityGrade.deployed();

  log(`GravityGrade deployed at ${gravityGrade.address}`);
};

module.exports = deployGravityGrade;
module.exports.tags = ['gravityGrade'];
