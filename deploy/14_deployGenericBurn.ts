import hre, { ethers, upgrades } from 'hardhat';

const NFTProxyAddress = '0xba6666B118f8303F990f3519DF07e160227cCE87';
const gravityGradeAddress = '0x3376C61c450359d402F07909Bda979a4c0e6c32F';
const vrfManagerAddress = '';
const SUBSCRIPTION_ID = '';
const KEYHASH = '';
const CALLBACK_GAS_LIMIT = 2_500_000;
const REQUEST_CONFIRMATIONS = 3;
const NUM_WORDS = 1;

let genericBurn;

async function deployGenericBurn() {
  const genericBurnFactory = await ethers.getContractFactory('GravityGradeGenericBurn');
  genericBurn = await upgrades.deployProxy(genericBurnFactory, []);

  await genericBurn.setGravityGrade(gravityGradeAddress);
  await genericBurn.setVRFOracle(vrfManagerAddress);

  await genericBurn.deployed();
  console.log(`generic burn deployed to: ${genericBurn.address}`);
}

async function upgradeAssetContract() {
  const nftFactory = await ethers.getContractFactory('NFT');
  const nft = await upgrades.upgradeProxy(NFTProxyAddress, nftFactory);
  await nft.setTrusted(genericBurn.address, true);

  console.log('Contract upgraded');
}

async function deployGGMintWrapper() {
  const ggMintWrapperFactory = await ethers.getContractFactory('GravityGradeMintWrapper');
  const ggMintWrapper = await ggMintWrapperFactory.deploy();
  await ggMintWrapper.deployed();

  await ggMintWrapper.setGravityGradeContract(gravityGradeAddress);
  await ggMintWrapper.setTrusted(genericBurn.address, true);

  console.log(`mint wrapper deployed to: ${ggMintWrapper.address}`);
}

async function setupVRFForBurn() {
  const vrfManagerFactory = await ethers.getContractFactory('VRFManager');
  const vrfManager = await vrfManagerFactory.attach(vrfManagerAddress);

  vrfManager.setConsumer(
    genericBurn.address,
    SUBSCRIPTION_ID,
    KEYHASH,
    CALLBACK_GAS_LIMIT,
    REQUEST_CONFIRMATIONS,
    NUM_WORDS,
  );
}

async function main() {
  await deployGenericBurn();
  await upgradeAssetContract();
  await deployGGMintWrapper();
  await setupVRFForBurn();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
