import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { networkConfig } from '../helper-hardhat-config';

const deployLootChestOpener: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { network } = hre;
  const { deploy, log } = hre.deployments;
  const { deployer, proxyAdmin } = await hre.getNamedAccounts();

  const pixAssets = networkConfig[network.name].pixAssets!;
  const rewardAssetIds = networkConfig[network.name].treasureAssets!;
  const rewardAssetAmounts = networkConfig[network.name].treasureAssetsAmounts!;
  const assetWeights = networkConfig[network.name].treasureAssetsWeight!;
  const contentAmounts = networkConfig[network.name].crateContentCount!;
  const contentWeights = networkConfig[network.name].crateContentWeights!;

  log('----------------------------------------------------');
  log('Deploying LootChestOpener and waiting for confirmations...');
  const lootChestOpener = await deploy('LootChestOpener', {
    contract: 'LootChestOpener',
    from: deployer,
    args: [],
    log: true,
    // we need to wait if on a live network so we can verify properly
    waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
    deterministicDeployment: false,
    proxy: {
      owner: proxyAdmin,
      proxyContract: 'TransparentUpgradeableProxy',
      execute: {
        init: {
          methodName: 'initialize',
          args: [
            pixAssets,
            rewardAssetIds,
            rewardAssetAmounts,
            assetWeights,
            contentAmounts,
            contentWeights,
          ],
        },
      },
    },
  });
  log(`lootChestOpener deployed at ${lootChestOpener.address}`);
};

module.exports = deployLootChestOpener;
module.exports.tags = ['lootChestOpener'];
