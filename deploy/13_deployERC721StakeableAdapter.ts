import {HardhatRuntimeEnvironment} from 'hardhat/types';
import {DeployFunction} from 'hardhat-deploy/types';
import {networkConfig, developmentChains} from '../helper-hardhat-config';
import {ethers, upgrades} from 'hardhat';
import {AssetIds} from "../test/utils";

async function getImplementationAddress(proxyAddress: string) {
    const implHex = await ethers.provider.getStorageAt(
        proxyAddress,
        '0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc',
    );
    return ethers.utils.hexStripZeros(implHex);
}

const deployERC721StakeableAdapter: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
    const {network} = hre;
    const {deploy, log} = hre.deployments;
    const {deployer, MaxProxyAdmin} = await hre.getNamedAccounts();


    log('----------------------------------------------------');
    log('Deploying ERC721StakeableAdapter and waiting for confirmations...');
    const missionControl = await deploy('ERC721MCStakeableAdapter', {
        contract: 'ERC721MCStakeableAdapter',
        from: deployer,
        args: [],
        log: true,
        // we need to wait if on a live network so we can verify properly
        waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
        deterministicDeployment: false,
        proxy: {
            owner: MaxProxyAdmin,
            proxyContract: 'TransparentUpgradeableProxy',
            execute: {
                init: {
                    methodName: 'initialize',
                    args: [AssetIds.Waste, 12, 3, true],
                },
            },
        },
    });
    log(`erc721 Stakeable adapter deployed at ${missionControl.address}`);
};

module.exports = deployERC721StakeableAdapter;
module.exports.tags = ['erc721StakeableAdapter'];
