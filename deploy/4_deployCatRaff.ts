import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { waitSeconds } from '../helper/utils';
import { ethers } from 'ethers';

const deploy: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
  const { deploy } = hre.deployments;
  const { deployer } = await hre.getNamedAccounts();

  const ticketPrice = ethers.utils.parseEther('5');
  const weeklyPrize = ethers.utils.parseEther('100');

  const mockToken = await deploy('MockToken', {
    from: deployer,
    args: ['Mock IXT', 'IXT', 18],
    log: true,
  });

  // const vrfCoordinatorMock = await deploy('VRFCoordinatorMock', {
  //   from: deployer,
  //   args: [],
  //   log: true,
  // });
  //   await vrfCoordinatorMock.createSubscription();
  //   await vrfCoordinatorMock.fundSubscription(subScriptionId, ethers.utils.parseEther('0.1'));

  const vrfCoordinator = '0x7a1BaC17Ccc5b313516C5E16fb24f7659aA5ebed';
  const keyHash = '0x4b09e658ed251bcafeebbc69400383d49f344ace09b9576fe248bb02c003fe9f';
  const subScriptionId = 776;
  const params = [
    mockToken.address,
    deployer,
    ticketPrice,
    weeklyPrize,
    vrfCoordinator,
    keyHash,
    subScriptionId,
  ];
  const catRaff = await deploy('PIXCatRaff', {
    from: deployer,
    args: params,
    log: true,
  });
  //   await catRaff.setKeeper(deployer);

  const catRaffKeeper = await deploy('PIXCatRaffKeeper', {
    from: deployer,
    args: [catRaff.address],
    log: true,
  });

  await waitSeconds(60);
  console.log('=====> Verifing ....');
  try {
    await hre.run('verify:verify', {
      address: mockToken.address,
      contract: 'contracts/mock/MockToken.sol:MockToken',
      constructorArguments: ['Mock IXT', 'IXT', 18],
    });
  } catch (error) {
    console.error(error);
  }
  // try {
  //   await hre.run('verify:verify', {
  //     address: vrfCoordinatorMock.address,
  //     contract: 'contracts/mock/VRFCoordinatorMock.sol:VRFCoordinatorMock',
  //     constructorArguments: [],
  //   });
  // } catch (_) {}
  try {
    await hre.run('verify:verify', {
      address: catRaff.address,
      contract: 'contracts/staking/PIXCatRaff.sol:PIXCatRaff',
      constructorArguments: params,
    });
  } catch (_) {}
  try {
    await hre.run('verify:verify', {
      address: catRaffKeeper.address,
      contract: 'contracts/staking/PIXCatRaffKeeper.sol:PIXCatRaffKeeper',
      constructorArguments: [catRaff.address],
    });
  } catch (_) {}
};

module.exports = deploy;
module.exports.tags = ['CatRaff'];
