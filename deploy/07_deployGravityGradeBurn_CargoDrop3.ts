import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { networkConfig, developmentChains } from '../helper-hardhat-config';
import { ethers, upgrades } from 'hardhat';

const deployGravityGradeBurn_CargoDrop3: DeployFunction = async function (
  hre: HardhatRuntimeEnvironment,
) {
  const { network } = hre;
  const { deploy, log } = hre.deployments;
  const { deployer, proxyAdmin } = await hre.getNamedAccounts();

  const probabilities = networkConfig[network.name].ggBurnProbabilities!;

  log('----------------------------------------------------');
  log('Deploying GravityGradeBurn_CargoDrop3 and waiting for confirmations...');
  const gravityGradeBurn = await deploy('GravityGradeBurn_CargoDrop3', {
    contract: 'GravityGradeBurn_CargoDrop3',
    from: deployer,
    args: [],
    log: true,
    // we need to wait if on a live network so we can verify properly
    waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
    deterministicDeployment: false,
    proxy: {
      owner: proxyAdmin,
      proxyContract: 'TransparentUpgradeableProxy',
      execute: {
        init: {
          methodName: 'initialize',
          args: [probabilities],
        },
      },
    },
  });
  log(`GravityGradeBurn_CargoDrop3 deployed at ${gravityGradeBurn.address}`);
};

module.exports = deployGravityGradeBurn_CargoDrop3;
module.exports.tags = ['gravityGradeBurn'];
