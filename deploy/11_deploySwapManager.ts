import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { networkConfig, developmentChains } from '../helper-hardhat-config';

const deploySwapManager: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { network } = hre;
  const { deploy, log } = hre.deployments;
  const { deployer } = await hre.getNamedAccounts();

  const swapRouter = networkConfig[network.name].swapRouter!;

  log('----------------------------------------------------');
  log('Deploying SwapManager and waiting for confirmations...');
  const swapManager = await deploy('SwapManager', {
    contract: 'SwapManager',
    from: deployer,
    args: [swapRouter],
    log: true,
    // we need to wait if on a live network so we can verify properly
    waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
    deterministicDeployment: false,
  });
  log(`SwapManager deployed at ${swapManager.address}`);
};

module.exports = deploySwapManager;
module.exports.tags = ['swapManager'];
