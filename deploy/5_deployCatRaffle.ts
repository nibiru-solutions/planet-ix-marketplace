import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { waitSeconds } from '../helper/utils';
import { ethers, upgrades } from 'hardhat';

async function getImplementationAddress(proxyAddress: string) {
  const implHex = await ethers.provider.getStorageAt(
    proxyAddress,
    '0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc',
  );
  return ethers.utils.hexStripZeros(implHex);
}

const deploy: DeployFunction = async (hre: HardhatRuntimeEnvironment) => {
  const { deploy } = hre.deployments;
  const { deployer, proxyAdmin } = await hre.getNamedAccounts();

  const mockLandmark = await deploy('Mock1155FT', {
    from: deployer,
    args: ['landmark'],
    log: true,
  });
  const mockTicket = await deploy('Mock1155FT', {
    from: deployer,
    args: ['ticket'],
    log: true,
  });

  const vrfCoordinator = '0x7a1BaC17Ccc5b313516C5E16fb24f7659aA5ebed';
  const keyHash = '0x4b09e658ed251bcafeebbc69400383d49f344ace09b9576fe248bb02c003fe9f';
  const subScriptionId = 776;
  const params = [
    mockLandmark.address,
    mockTicket.address,
    vrfCoordinator,
    keyHash,
    subScriptionId,
  ];
  const catRaffle = await deploy('PIXCatRaffle', {
    contract: 'PIXCatRaffle',
    from: deployer,
    args: [],
    log: true,
    deterministicDeployment: false,
    proxy: {
      owner: proxyAdmin,
      proxyContract: 'TransparentUpgradeableProxy',
      execute: {
        init: {
          methodName: 'initialize',
          args: params,
        },
      },
    },
  });
  const catRaffleImplementation = await getImplementationAddress(catRaffle.address);

  const catRaffleKeeper = await deploy('PIXCatRaffleKeeper', {
    from: deployer,
    args: [catRaffle.address],
    log: true,
  });

  await waitSeconds(60);
  console.log('=====> Verifing ....');
  try {
    await hre.run('verify:verify', {
      address: mockLandmark.address,
      contract: 'contracts/mock/Mock1155FT.sol:Mock1155FT',
      constructorArguments: ['landmark'],
    });
  } catch (error) {
    console.error(error);
  }
  try {
    await hre.run('verify:verify', {
      address: mockTicket.address,
      contract: 'contracts/mock/Mock1155FT.sol:Mock1155FT',
      constructorArguments: ['ticket'],
    });
  } catch (error) {
    console.error(error);
  }
  try {
    await hre.run('verify:verify', {
      address: catRaffleImplementation,
      contract: 'contracts/staking/PIXCatRaffle.sol:PIXCatRaffle',
      constructorArguments: [],
    });
  } catch (_) {}
  try {
    await hre.run('verify:verify', {
      address: catRaffleKeeper.address,
      contract: 'contracts/staking/PIXCatRaffleKeeper.sol:PIXCatRaffleKeeper',
      constructorArguments: [catRaffle.address],
    });
  } catch (_) {}
};

module.exports = deploy;
module.exports.tags = ['CatRaffle'];
