import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { networkConfig, developmentChains } from '../helper-hardhat-config';

const deployChainlinkOracle: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { network } = hre;
  const { deploy, log } = hre.deployments;
  const { deployer, proxyAdmin } = await hre.getNamedAccounts();

  const ethUsdPriceFeedAddress = networkConfig[network.name].ethUsdPriceFeedAddress!;
  const usdcAddress = networkConfig[network.name].usdcAddress!;
  const wethAddress = networkConfig[network.name].wethAddress!;

  log('----------------------------------------------------');
  log('Deploying ChainlinkOracle and waiting for confirmations...');
  const chainlinkOracle = await deploy('ChainlinkOracle', {
    contract: 'ChainlinkOracle',
    from: deployer,
    args: [wethAddress, usdcAddress, ethUsdPriceFeedAddress],
    log: true,
    // we need to wait if on a live network so we can verify properly
    waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
    deterministicDeployment: false,
  });
  log(`ChainlinkOracle deployed at ${chainlinkOracle.address}`);
};

module.exports = deployChainlinkOracle;
module.exports.tags = ['chainlinkOracle'];
