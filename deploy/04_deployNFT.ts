import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import verify from '../utils/verify';
import { networkConfig } from '../helper-hardhat-config';

const deployNFT: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { network } = hre;
  const { deploy, log } = hre.deployments;
  const { deployer, proxyAdmin } = await hre.getNamedAccounts();
  const chainId: number = network.config.chainId!;

  const nftName = networkConfig[network.name].nftName!;
  const nftSymbol = networkConfig[network.name].nftSymbol!;

  log('----------------------------------------------------');
  log('Deploying NFT and waiting for confirmations...');
  const nft = await deploy('NFT', {
    contract: 'NFT',
    from: deployer,
    args: [],
    log: true,
    // we need to wait if on a live network so we can verify properly
    waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
    deterministicDeployment: false,
    proxy: {
      owner: proxyAdmin,
      proxyContract: 'TransparentUpgradeableProxy',
      execute: {
        init: {
          methodName: 'initialize',
          args: [nftName, nftSymbol],
        },
      },
    },
  });
  log(`NFT deployed at ${nft.address}`);
  //   if (!developmentChains.includes(network.name) && process.env.POLYGONSCAN_TOKEN) {
  //     await verify(wasteTrader.address, []);
  //   }
};

module.exports = deployNFT;
module.exports.tags = ['nft'];
