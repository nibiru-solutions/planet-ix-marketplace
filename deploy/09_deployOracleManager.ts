import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { networkConfig, developmentChains } from '../helper-hardhat-config';
const { verify } = require('../utils/verify');

const deployOracleManager: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { network } = hre;
  const { deploy, log } = hre.deployments;
  const { deployer, proxyAdmin } = await hre.getNamedAccounts();

  log('----------------------------------------------------');
  log('Deploying OracleManager and waiting for confirmations...');
  const oracleManager = await deploy('OracleManager', {
    contract: 'OracleManager',
    from: deployer,
    args: [],
    log: true,
    // we need to wait if on a live network so we can verify properly
    waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
    deterministicDeployment: false,
  });
  log(`OracleManager deployed at ${oracleManager.address}`);
  if (!developmentChains.includes(network.name) && process.env.POLYGONSCAN_TOKEN) {
    await verify(oracleManager.address, []);
  }
  log('----------------------------------------------------');
};

module.exports = deployOracleManager;
module.exports.tags = ['oracleManager'];
