import {HardhatRuntimeEnvironment} from 'hardhat/types';
import {DeployFunction} from 'hardhat-deploy/types';
import {networkConfig, developmentChains} from '../helper-hardhat-config';
import {ethers, upgrades} from 'hardhat';

async function getImplementationAddress(proxyAddress: string) {
    const implHex = await ethers.provider.getStorageAt(
        proxyAddress,
        '0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc',
    );
    return ethers.utils.hexStripZeros(implHex);
}

const deployMissionControl: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
    const {network} = hre;
    const {deploy, log} = hre.deployments;
    const {deployer, MaxProxyAdmin, PIXAssets} = await hre.getNamedAccounts();


    log('----------------------------------------------------');
    log('Deploying MissionControl and waiting for confirmations...');
    const missionControl = await deploy('MissionControl', {
        contract: 'MissionControl',
        from: deployer,
        args: [],
        log: true,
        // we need to wait if on a live network so we can verify properly
        waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
        deterministicDeployment: false,
        proxy: {
            owner: MaxProxyAdmin,
            proxyContract: 'TransparentUpgradeableProxy',
            execute: {
                init: {
                    methodName: 'initialize',
                    args: [1, PIXAssets],
                },
            },
        },
    });
    log(`Mission Control deployed at ${missionControl.address}`);
};

module.exports = deployMissionControl;
module.exports.tags = ['missionControl'];
