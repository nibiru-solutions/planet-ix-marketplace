import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { networkConfig, developmentChains } from '../helper-hardhat-config';
import { ethers, upgrades } from 'hardhat';
import verify from '../utils/verify';

const deployERC1155StoreGeneric: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { network } = hre;
  const { deploy, log } = hre.deployments;
  const { deployer, proxyAdmin } = await hre.getNamedAccounts();

  log('----------------------------------------------------');
  log('Deploying ERC1155StoreGeneric and waiting for confirmations...');
  const erc1155store = await deploy('ERC1155StoreGeneric', {
    contract: 'ERC1155StoreGeneric',
    from: deployer,
    args: [],
    log: true,
    waitConfirmations: networkConfig[network.name].blockConfirmations || 0,
    deterministicDeployment: false,
    proxy: {
      owner: proxyAdmin,
      proxyContract: 'TransparentUpgradeableProxy',
      execute: {
        init: {
          methodName: 'initialize',
          args: [],
        },
      },
    },
  });
  log(`ERC1155StoreGeneric deployed at ${erc1155store.address}`);
  // await verify(erc1155store.address, []);
  await hre.run('verify:verify', {
    address: erc1155store.address,
  });
};

module.exports = deployERC1155StoreGeneric;
module.exports.tags = ['genericStore'];
